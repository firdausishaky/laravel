<?php namespace Larasite\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class undBiometrics extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'biometrics:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//created  08/01/2016 
	$imports = 'Biometrics/import_Biometrics.json';
              $backups = 'Biometrics/backup_Biometrics.json';
              $times   = 'Biometrics/time_Biometrics.json';

              $backup = json_decode(file_get_contents(storage_path($backups)),1);
              $import = json_decode(file_get_contents(storage_path($imports)),1);
              $time   = json_decode(file_get_contents(storage_path($times)),1);
            // return $import;

              $import_server = "";
              $import_username = "";
              $import_password = "";
              $import_share = "";
              $import_domain = "";

              $backup_server = "";
              $backup_username = "";
              $backup_password = "";
              $backup_share = "";
              $backup_domain = "";
              //end open and rean ajson file ftp

              // start check file already exist or not , and read than save value in a variable
              if(file_exists(storage_path($imports)) == true and file_exists(storage_path($backups)) == true and file_exists(storage_path($times)) == true){
                 if($import != null){
                      foreach($import as $data){
                        $import_server = $data['server-import'];
                        $import_username = $data['server-username'];
                        $import_password = $data['password'];
                        $import_share = $data['share'];
                        $import_domain = $data['domain'];
                      }
                 }

                 if($backup != null){


                      foreach($backup as $data){
                        $backup_server = $data['server-backup'];
                        $backup_username = $data['server-username'];
                        $backup_password = $data['password'];
                        $backup_share = $data['share'];
                        $backup_domain = $data['domain'];
                      }
                 }
              // end start check file already exist or not , and read than save value in a variable  
                 if($import != null && $backup != null && $time != null){
                 	$ftp_server = $import_server;
                 	$file_local = "";
                 	$scan = "";
                 	$ftp_conn = ftp_connect($import_server) or die ("could not connect to $ftp_server");
                 	$login  = ftp_login($ftp_conn,$import_username,$import_password);
                 	if(ftp_chdir($ftp_conn,$import_share))
                 	{
                 		$scan = $import_share;
                 	}
                 	$asciiArray = array('txt', 'csv');
					$extension = end(explode('.', $scan));
					    if (in_array($extension, $asciiArray)) {
					        $mode = FTP_ASCII;      
					    } else {
					        $mode = FTP_BINARY;
					        echo "not accepted format";
					        break;
					    }
					    $remote_file = $import_server.\.$import_share.\.$scan;
					    $file_local = fopen(storage_path($scan),'w');
					    if(ftp_get($ftp_connect,$file_local,$remote_file,$mode,0)){
					    	if(ftp_delete($ftp_conn,$remote_file)){
					    		  $csv = array();
						          $header = array();
						          $handle = "";
						          $handle1 = array();
						          if (($handle = fopen($biometrics, "r")) != false)
						              {
						                  $fcsv = fopen(storage_path('Biometrics\file.csv'),'w');
						                  while(($data = fgets($handle,1000)) != false)
						                  {
						                    // $csv_fp = fopen('file.csv');
						                     $csv[] = preg_replace('/\s/',',',$data);
						                      
						                  } 
						                  $count = sizeof($csv);    
						                  for($i = 0; $i < $count-1; $i++)
						                  {
						                        $header[] = substr($csv[0],0,50);

						                  }
						                  if(isset($header)){
						                      for($i = 0; $i < $count-1; $i++)
						                      {
						                            $handle1[$i] = explode(",",$header[0]);

						                      }
						                      for($i = 0; $i<$count-1; $i++){
						                            
						                           //echo $handle1[$i][0];
						                      	  //call procedure
						                            
						                      }

						                     $ftp_conn1 = ftp_connect($backup_server) or die ("could not connect to $ftp_server");
                 						     $login1  = ftp_login($ftp_conn1,$import_username,$import_password);
						                  }
						             
						             						            }
				                }else{
				                	echo "failed to delete file in server";
				                	break;
				                }
					    	}
					    }else{
					    	return "can't download file from server"
					    }







              }
		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		// return [
		// 	['example', InputArgument::REQUIRED, 'An example argument.'],
		// ];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		// return [
		// 	['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		// ];
	}

}
