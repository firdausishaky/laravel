<?php namespace Larasite\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'Larasite\Console\Commands\Inspire',
		
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	// protected function schedule(Schedule $schedule)
	// {
	// 	$schedule->call(function(){
	// 		\DB::SELECT("CALL approve_all_schedule");
	// 	})->monthlyOn(1,'01:00');

	// 	$schedule->call(function(){
	// 	  	 $data = \DB::SELECT("select att_cutoff_from from att_cutoff_period");
	// 		    $date = date("d");
	// 		    $month = date("m");
	// 		    $year = date("Y");
	// 		    $d=cal_days_in_month(CAL_GREGORIAN,10,2005);
	// 		    $yn = "0";
	// 		    foreach($data as $key => $value){
	// 		        if(sub_str($value,5,2) == $date){
	// 		            $yn += 1;
	// 		        }
	// 		    }

	// 		    if($yn >= 1){
	// 		        $date1 = "01/".$month."/".$year."-"."10/".$month."/".$year;
	// 		        $date2 = "11/".$month."/".$year."-"."20/".$month."/".$year;
	// 		        $date3 = "21/".$month."/".$year."-".$d."/".$month."/".$year;

	// 		        $att1to = "10/".$month."/".$year;
	// 		        if(date("l", mktime(0, 0, 0, 10, $month, $year)) == "Sunday" || date("l", mktime(0, 0, 0, 10, $month, $year)) == "Saturday"){
	// 		            $att1to = "09/".$month."/".$year;
	// 		        }else{
	// 		            $att1to = $att1to;
	// 		        }
	// 		        $att2to = "20/".$month."/".$year;
	// 		        if(date("l", mktime(0, 0, 0, 20, $month, $year)) == "Sunday" || date("l", mktime(0, 0, 0, 20, $month, $year)) == "Saturday"){
	// 		            $att2to = "19/".$month."/".$year;
	// 		        }else{
	// 		            $att2to = $att2to;
	// 		        }
	// 		        $att3to = $d."/".$month."/".$year;
	// 		        if(date("l", mktime(0, 0, 0, $d, $month, $year)) == "Sunday" || date("l", mktime(0, 0, 0, $d, $month, $year)) == "Saturday"){
	// 		            $d1 = $d-1;
	// 		            $att2to = $d1."/".$month."/".$year;
	// 		        }else{
	// 		            $att3to = $att3to;
	// 		        }
	// 		        $att1from = "01/".$month."/".$year;
	// 		        $att2from = "11/".$month."/".$year;
	// 		        $att3from = "21/".$month."/".$year;


	// 		    }
	// 	})->monthlyOn(1,'01:00');
	// }

}
