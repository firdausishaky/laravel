<?php namespace Larasite\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	// public function render($request, Exception $e)
	// {
	// 	return parent::render($request, $e);
	// }
// 	public function render($request, Exception $e)
// {
//     if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
//         if ($request->ajax()) {
//             return response(['error' => $e->getMessage()], 400);
//         } else {
//             return $e->getMessage();
//         }
//     }

//     if ($this->isHttpException($e)) {
//         return $this->renderHttpException($e);
//     } else {
//         return parent::render($request, $e);
//     }
// }
public function render($request, Exception $e) {
    if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException)
        return response(view('index'), 404);
        //return response(view('error.404'), 404);

    return parent::render($request, $e);
}


	public function handle($request, Closure $next){ 

   		header("Access-Control-Allow-Origin: *");

 		 // ALLOW OPTIONS METHOD
		  $headers = [
		         'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PUT, DELETE',
		         'Access-Control-Allow-Headers'=> 'Content-Type, X-Auth-Token, Origin'
		     ];
  		if($request->getMethod() == "OPTIONS") {
         	// The client-side application can set only headers allowed in Access-Control-Allow-Headers
        	 return Response::make('OK', 200, $headers);
     	}

     	$response = $next($request);
     	foreach($headers as $key => $value) 
     		$response->header($key, $value);
  			return $response;
 		}

	}
