<?php namespace Larasite\Http\Controllers\Attendance\CutOff;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use League\Csv\Reader;
use Larasite\Library\FuncAccess;
use Illuminate\Http\Request;

class AttendanceCutOffPeriod extends Controller {

    protected $form = "53";

	public function getMessage($message,$status,$access,$data){
		return response()->json(['header' => ["message" => $message, "status" => $status, "access" => $access],"data" => $data],$status);
	}
	
	 public function index()
   	 {
       	 /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
      		if($access[1] == 200){
        		$from = date('Y-m-d', strtotime('first day of January ' . date('Y')));
            $to = date('Y-m-d',strtotime(date('Y') . '-12-31'));
      			$data = [ "from" => $from, "to" => $to];
      			$message = "success";
      			$status = 200; 	
        	}else{
           		$message = $access[0]; $status = $access[1]; $data=$access[2];
        	}
        		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
        	}

  public function dateFunction($arg,$arg1){
      $date =  getdate();
      $year = $date['year'];
      $month =  $date['mon'];
      $day = $date['mday'];

      if(isset($arg)){
        $split = explode("-",$arg);
        $y = $split[0];
        if($y != $year){
          $year = $y;
        }
      }

      //$newdate = $day.'-'.$month.'-'.$year;
      $newdate = $arg;
      $newComparedate = $arg;
      //$newComparedate = '01-01-'.$year;
      $arrFirstDayMonth = [];
      $arrLastDayMonth = [];
      $monthMax = 12;

      for($i = 1; $i <= $monthMax; $i++){
        $width = strlen($i);
        if($width == 1){
          $newMonth = '0'.$i;
        }else{
          $newMonth =  $i;
        }

        $endOfMonth =  cal_days_in_month(CAL_GREGORIAN,$newMonth,$year); 
        $arrFirstDayMonth[] = $year.'-'.$newMonth.'-01';
        $arrFirstDayMonth[] = $year.'-'.$newMonth.'-11';
        $arrFirstDayMonth[] = $year.'-'.$newMonth.'-21';

        $arrLastDayMonth[] = $year.'-'.$newMonth.'-10';
        $arrLastDayMonth[] = $year.'-'.$newMonth.'-20';
        $arrLastDayMonth[] = $year.'-'.$newMonth.'-'.$endOfMonth;

      }
      $data = [];
      foreach ($arrFirstDayMonth as $key => $value) {
        $match = $key+1;
        $data[] = [ "dateFrom" => [],
                    "dateTo" => [],
                    "id" => $match,
                    "payout_date"  => [],
                    "payroll_period" => $arrFirstDayMonth[$key].'-'.$arrLastDayMonth[$key]
                  ];

      }

      return $data;
  }

  
  public function checkfield(){
    /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
      if($access[1] == 200){
        $input = \Input::all();
        $dateinput = $input['payroll_period'];
        $dateinput =  explode("-",$dateinput);
        $from = $dateinput[0]."-".$dateinput[1]."-".$dateinput[2];
        $to = $dateinput[3]."-".$dateinput[4]."-".$dateinput[5];
        
        $id = $input['id'];
        $lastid = $id+1;
        $substract = \DB::SELECT("select att_cutoff_from from att_cutoff_period where id = $lastid ");
        //return $substract[0]->att_cutoff_from;
        if(isset($substract[0]->att_cutoff_from) &&  $substract[0]->att_cutoff_from != "0000-00-00" ){
          $att_cutoff_to = date('Y-m-d',(strtotime ( '-1 day' , strtotime ($substract[0]->att_cutoff_from)))); 
        }else{

          $att_cutoff_to = date('Y-m-d',(strtotime ( '+9 day' , strtotime ($input['dateFrom']))));
        }
        //return  $att_cutoff_to;
        if($att_cutoff_to != "0000-00-00"){

          $weekof = date('l', strtotime($att_cutoff_to));
          if($weekof == "Monday"){
            $payoutdate = date('Y-m-d',(strtotime ( '-3 day' , strtotime ($att_cutoff_to))));
          }else if($weekof == "Sunday"){
            $payoutdate = date('Y-m-d',(strtotime ( '-2 day' , strtotime ($att_cutoff_to))));
          }else if($weekof == "Saturday"){
            $payoutdate = date('Y-m-d',(strtotime ( '-1 day' , strtotime ($att_cutoff_to))));
          }else{
            $payoutdate = date('Y-m-d',(strtotime ( '-1 day' , strtotime ($att_cutoff_to))));
          }
        }else{
          $payoutdate = "0000-00-00";
        }

         $data = ["id" =>$id, "dateFrom" => $input['dateFrom'], "dateTo" => $att_cutoff_to, "payoutDate" => $payoutdate];
         //return $data;
         $status = "200";
         $message = "success";

      }
      else
      {
        $message = $access[0]; $status = $access[1]; $data=$access[2];
      }
        return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
    }
 

  public function search(){
        	 /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
      		if($access[1] == 200){
              $str = \Input::get('from');
              $end = \Input::get('to');
	        		$input = ["from" => $str, "to" => $end ];
	        		$valid = $this->validasi($input);
	        		if($valid != "ok"){
	        			$message = $valid;
	        			$status = 500;
	        			$data = [];
	        		}else{
  	        		$result = \DB::SELECT("call view_cutoff_period('$str','$end') ");
                if($result == null){
                  $data = $this->dateFunction($str,$end);
                  
                }else{
                  $data = $result;
                }
                $status = 200;
	        			$message = "show data record, success";
	        		}
	        	}
	          else
	          {
           		$message = $access[0]; $status = $access[1]; $data=$access[2];
        		}
        		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
        	}

public function edit(){
  /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update');

  if($access[1] == 200){
        $inputx = \Input::get('data');
        $date = \Input::get('date');
        $result = \DB::SELECT("call view_cutoff_period('$date[from]','$date[to]') ");
       // return [$result];
        if($result == null){
            foreach ($inputx as $key => $value) {
                $payroll_period = $value['payroll_period'];
                $from = $value['dateFrom'];
                $to = $value['dateTo'];
                $payout = $value['payout_date'];

                if($from == []){ $from = '0000-00-00';}
                if($to == []){ $to = '0000-00-00';}
                if($payout == []){ $payout = '0000-00-00';}

                //$insert = \DB::SELECT("insert into att_cutoff_period(payroll_period,att_cutoff_from,att_cutoff_to,payout_date) values('$payroll_period','$from','$to','$payout')");
                $t = "insert into att_cutoff_period(payroll_period,att_cutoff_from,att_cutoff_to,payout_date) values('$payroll_period','$from','$to','$payout')";
                $insert = \DB::SELECT($t);
                $message = "success to insert data cut-off period";
                $status = 200;
                $data = \DB::SELECT("call view_cutoff_period('$date[from]','$date[to]') ");
            }
        }else{
        
            foreach($inputx as $key => $value){
            
                $input = ["from" => $value['dateFrom'], "to" => $value['dateTo'], "id" => $value['id']  ];
            
           
                if(strlen($value['payout_date']) < 4 || !isset($value['payout_date']) || $value['payout_date'] == null){
                    $valid = $this->validasi($input);
                    $message = $valid;
                    $status = 500;
                    $data = [];
                    return \Response::json(['header'=>['message'=>'please complete the data on the form ','status'=>$status,"access" => $access[3]],'data'=>$data],$status);
                }else{
                    $valid = $this->validasi($input);
                    if($valid != "ok"){
                        $message = $valid;
                        $status = 500;
                        $data = [];
                    }else{
                        $data_x = \DB::SELECT("call update_att_cutoffperiod($input[id],'$input[from]','$input[to]','$value[payout_date]') ");
                        $status = 200;
                        //$t1 = "call update_att_cutoffperiod($input[id],'$input[from]','$input[to]','$value[payout_date]') ";  
                    }
                }
            }
            
            if($status != 500){
          $message = "success to edit data cut-off period";
          $status = 200;
          $data = \DB::SELECT("call view_cutoff_period('$date[from]','$date[to]') ");
            }
        }
  }else{$message = $access[0]; $status = $access[1]; $data=$access[2]; }
    return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
}

        	public function validasi($input){
        		$validasi = \Validator::make(
        			["from" => $input['from'],
        			 "to" => $input['to'] 
        			],

        			[ "from" => 'required|date',
        			  "to" => "required|date"
        			]
        			);
        		if($validasi->fails())
        			{ $check = $validasi->errors()->all(); return $check;}
        		else
        			{ return "ok";}
        	}

    
}