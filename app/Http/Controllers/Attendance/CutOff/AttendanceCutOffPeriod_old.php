<?php namespace Larasite\Http\Controllers\Attendance\CutOff;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use League\Csv\Reader;
use Larasite\Library\FuncAccess;
use Illuminate\Http\Request;

class AttendanceCutOffPeriod extends Controller {

    protected $form = "53";

	public function getMessage($message,$status,$access,$data){
		return response()->json(['header' => ["message" => $message, "status" => $status, "access" => $access],"data" => $data],$status);
	}
	
	 public function index()
   	 {
       	 /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
      		if($access[1] == 200){
      			$from = date('Y-m-d', strtotime(date('Y-m-d')."-1 month"));
			$to = date('Y-m-d',strtotime($from."+1 year"));
			$data = [ "from" => $from, "to" => $to];
			$message = "success";
			$status = 200; 	
        		}else{
           		$message = $access[0]; $status = $access[1]; $data=$access[2];
        		}
        		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
        	}

        	public function search(){
        	 /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
      		if($access[1] == 200){
	        		$input = ["from" => \Input::get('from'), "to" => \Input::get('to') ];
	        		$valid = $this->validasi($input);
	        		if($valid != "ok"){
	        			$message = $valid;
	        			$status = 500;
	        			$data = [];
	        		}else{
	        			$data = \DB::SELECT("call view_cutoff_period('$input[from]','$input[to]') ");
	        			$status = 200;
	        			$message = "show data record, success";
	        		}
	        	}
	          else
	          {
           		$message = $access[0]; $status = $access[1]; $data=$access[2];
        		}
        		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
        	}

        	public function edit(){
        		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update');
      		if($access[1] == 200){
      			     $inputx = \Input::get('data');
                          $date = \Input::get('date');
                          foreach ($inputx as $key => $value) {

                                        $input = ["from" => $value['dateFrom'], "to" => $value['dateTo'], "id" => $value['id']  ];
                                        $valid = $this->validasi($input);

                                        if($valid != "ok"){
                                          $message = $valid;
                                          $status = 500;
                                          $data = [];
                                        }else{
                                            $data_x = \DB::SELECT("call update_att_cutoffperiod($input[id],'$input[from]','$input[to]','$input[to]') ");
                                        }
                          }
		        			$message = "success to edit data cut-off period";
		        			$status = 200;
		        			$data = \DB::SELECT("call view_cutoff_period('$date[from]','$date[to]') ");
      		}
	          else
	          {
           		$message = $access[0]; $status = $access[1]; $data=$access[2];
        		}
        		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
        	}

        	public function validasi($input){
        		$validasi = \Validator::make(
        			["from" => $input['from'],
        			 "to" => $input['to'] 
        			],

        			[ "from" => 'required|date',
        			  "to" => "required|date"
        			]
        			);
        		if($validasi->fails())
        			{ $check = $validasi->errors()->all(); return $check;}
        		else
        			{ return "ok";}
        	}

    
}