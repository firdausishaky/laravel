<?php namespace Larasite\Http\Controllers\Attendance\CutOff;
set_time_limit(0);
use Larasite\Http\Requests;
use Larasite\Library\calculation;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Attendance\cutOff\Attendance_cutoffrecordModel;
use Illuminate\Http\Request;

use DateTime;
use DatePeriod;
use DateInterval;

use Larasite\Library\FuncAccess;

class AttendanceCutOffRecord extends Controller {
	protected $form = 75;

	// FUNCTION FOR RANGE DATE
	public function GetDays($sStartDate, $sEndDate){
		// Firstly, format the provided dates.
		// This function works best with YYYY-MM-DD
		// but other date formats will work thanks
		// to strtotime().
		$sStartDate = gmdate("Y-m-d", strtotime($sStartDate));
		$sEndDate = gmdate("Y-m-d", strtotime($sEndDate));

		// Start the variable off with the start date
		$aDays[] = $sStartDate;

		// Set a 'temp' variable, sCurrentDate, with
		// the start date - before beginning the loop
		$sCurrentDate = $sStartDate;

		// While the current date is less than the end date
		while($sCurrentDate < $sEndDate){
			// Add a day to the current date
			$sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));

			// Add this new day to the aDays array
			$aDays[] = $sCurrentDate;
		}
		return $aDays;
	}


	protected function findIndex($stack,$arr,$elem){
		$tmp = -1;
		if(count($arr) > 0){
			foreach ($arr as $key => $value) {
				if($value[$elem] == $stack){
					$tmp = $key;
					break;
				}
			}
		}
		return $tmp;
	}
	// FUNCTION FOR BUILD OBJECT TAR, ABS, ND, NDOT, ROT, ROTDO (CUTOFF)
	public function get_cutoff_result($data,$employee_ids,$date1,$date2,$arr,$end_last_date){

		foreach ($data['data'] as $key => $value) {
			$employee_ids[$value['employee_id']] = $value['employee_id'];
		}

		//loop for get employee id
		foreach ($employee_ids as $key => $value) {
			$data_emp[] = $key;
		}

		$range = $this->GetDays($date1,$date2);

		// get all date rh and sh
		$th = date('Y',strtotime($date1));
		/*$dt_holiday = \DB::SELECT("select distinct date , type from attendance_holiday where year(date)=$th");
		$tmp_holiday = [];
		foreach ($dt_holiday as $key_hol => $value_hol) {
			if($value_hol->type == 1){
				$tmp_holiday[$value_hol->date] = "SH";
			}else{
				$tmp_holiday[$value_hol->date] = "RH";
			}
			//$tmp_holiday[$value_hol->date] = $value_hol->type;
		}*/
		
		//create dictionary array alphabet
		$dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];
		$count = count($data_emp);

		//foreach  last_name, first_name,date, date & shift_code, tar, abs, nd,rot,rotdo

		$result = [];
		$test = [];
		$range_field = [];

		for($x = 0; $x < $count; $x++){ // LOOP__3
			
			foreach($data['data'] as $key => $value){ // LOOP__2

				if(isset($data_emp[$x])){ // IF data_emp__1
					if($data_emp[$x] == $value['employee_id']){ // IF employee_id__1
						$counting_late = 0;
						$counting_leave =  0;
						$counting_night = 0;
						$counting_over  = 0;
						$counting_rdo = 0;
						$result[$value['employee_id']]['employee_id']= $value['employee_id'];
						$result[$value['employee_id']]['first_name'] = $value['first_name'];
						$result[$value['employee_id']]['last_name'] =  $value['last_name'];
						$counts = count($range);

						for($y=0; $y < $counts ; $y++) { // LOOP_START__1

							if($value['date'] == $range[$y]){ // IF RANGE__1

								$value['overtimerest_hours'] = 0;
								$value['overtime_hours'] = 0;



								if($value['Late'] != '-'&& $value['colorLate'] != 'green'){ //colorLate__

									if(!strchr($value['Late'],':')){
										$late1 = $value['Late'];
							  		}else{
										$late = explode(':',$value['Late']);
										$late1 = (intval($late[0]) * 60) + intval($late[1]);
							  		}

									if(isset($result[$value['employee_id']]['tar']) && $result[$value['employee_id']]['tar'] != 0){
								   		$result[$value['employee_id']]['tar'] = intval($result[$value['employee_id']]['tar']) + $late1;
							  		}else if(isset($result[$value['employee_id']]['tar']) && $result[$value['employee_id']]['tar'] == 0){
										$result[$value['employee_id']]['tar'] = $late1;

										if(isset($result[$value['employee_id']]['tar_total'])){
											$result[$value['employee_id']]['tar_total']++;;
										}else{
											$result[$value['employee_id']]['tar_total']=1;
										}
									}else{
										$result[$value['employee_id']]['tar'] = $late1;
										if(isset($result[$value['employee_id']]['tar_total'])){
											$result[$value['employee_id']]['tar_total']++;;
										}else{
											$result[$value['employee_id']]['tar_total']=1;
										}
									}
								}else{
									if(isset($result[$value['employee_id']]['tar']) && $result[$value['employee_id']]['tar'] != 0){
								  		$result[$value['employee_id']]['tar'] = intval($result[$value['employee_id']]['tar']) + 0;
								 	}else if(isset($result[$value['employee_id']]['tar']) && $result[$value['employee_id']]['tar'] == 0){
										$result[$value['employee_id']]['tar'] = 0;
										$result[$value['employee_id']]['tar_total'] = 0;
									}else{
								  		$result[$value['employee_id']]['tar'] = 0;
								  		$result[$value['employee_id']]['tar_total'] = 0;
									}
								}//end if colorLate__

								if($value['Late'] != '-' && $value['colorLate'] != 'green'){ // colorLate__1

									if(!strchr($value['Late'],':')){
										$late = $value['Late'];
									}else{
										$late = explode(':',$value['Late']);
										$late = (intval($late[0]) * 60) + intval($late[1]);
									}
						  		}else{ $late = 0; } // end if colorLate__1

								if($value['Short'] != '-'){ $short = 0 ; }else{ $short = 0; }

								if($value['EarlyOut'] != '-' && $value['early_c'] != 'green'){ // early_c__1
								  $result[$value['employee_id']]['tar'] += intval($value['EarlyOut']);
								  $result[$value['employee_id']]['tar_total']++;
								  $early = intval($value['EarlyOut']);
								}else{ $early = 0;	} // // early_c__1


								if($late || $value['EarlyOut']){ // EarlyOut__1

									$total = $late + $early;
									if($total == null || $total <= 0 ){
									  $total = '/';
									}else{
									  $total = $total."m";
									}
								}else{ $total = '/'; } // END EarlyOut__1

								if(strlen($value['schedule']) <= 3){
									$total = $value['schedule'];
								}

								$tmp = ['el','vl','sl','bl','do','ado','ml','pl','sus','be','ma','t','bo'];

								if(!isset($result[$value['employee_id']]['abs'])){
									$result[$value['employee_id']]['abs']=0;
								}

								$condition1 = false;
								if(strlen($value['schedule']) >=2 && ($value['timeOut'] != "-" && $value['timeIn'] != "-") ){ // schedule__1
									
									if($value['timeOut'] != "-" && $value['timeIn'] != "-"){
										if(strlen($value['schedule']) >= 2 && strlen($value['schedule']) <=3){
											
											$timeOuts 		= $value['timeOut'];
											$am_pm_timeout 	= date("a",strtotime($value['timeOut']));

											$timeIn1 		= $value['timeIn'];
											$am_pm_timein 	= date("a",strtotime($timeIn1));

											$sch_timeout 	= $value['timeOut'];

											if($am_pm_timeout == 'pm'){
												if($timeOuts > "22:59:00"){ $condition1 = true;	}
											}else{
												if($value['nextday']){ $condition1 = true; }
											}

											if(!$condition1 && strtotime($timeIn1) >= strtotime("01:00:00") && strtotime($timeIn1) <= strtotime("05:00:00")){
												$condition1 = true;	
											}else if(!$condition1 && $am_pm_timein == 'pm' && strtotime($timeIn1) >= strtotime("22:00:00") ) {
												$condition1 = true;	
											}
											if(strtotime($timeOuts) > strtotime("22:59")){ $condition1 = true;	}
										}else{
											$split_sch 		= explode("-",$value['schedule']);
											$timeOuts 		= $value['timeOut'];
											$am_pm_timeout 	= date("a",strtotime($value['timeOut']));
											$am_pm_timeout_origin 	= date("a",strtotime($split_sch[1]));
											$sch_timeout 	= $split_sch[1];

											if($am_pm_timeout_origin == 'pm'){
												if(strtotime($split_sch[1]) > strtotime("22:59")){
													$condition1 = true;	
												}
												//if($timeOuts > "22:59:00"){ $condition1 = true;	}
											}else{
												if($value['nextday']){ $condition1 = true; }
											}
											if(strtotime($timeOuts) > strtotime("22:59")){ $condition1 = true;	}
										}

										
									}
								} // END schedule__1
								// if($value['employee_id'] == '2018048' && $value['date']=='2019-11-29'){
								// 	return [/*$total_ndot,$total_nd,*/$condition1,(strlen($value['schedule']) >=2 && ($value['timeOut'] != "-" && $value['timeIn'] != "-") ),
								// 				($value['timeOut'] != "-" && $value['timeIn'] != "-"),
								// 				(!$condition1 && strtotime($timeIn1) >= strtotime("01:00:00") && strtotime($timeIn1) <= strtotime("05:00:00")),
								// 				strtotime($timeIn1) , strtotime("01:00:00") , strtotime($timeIn1) , strtotime("05:00:00")
								// 			];
								// }


								$total_nd = 0; $total_ndot = 0; $total_rot = 0;

								// START CAL
								if($condition1 /*&& $value['new_color_overtime'] == 'green'*/){ // condition1__1

									if(strlen($value['schedule']) >= 2 && strlen($value['schedule']) <=3){
										$sch_timein 		= $value['timeIn'];
										$sch_timeout 		= $value['timeOut'];
									}else{									
										$split_sch 			= explode("-",$value['schedule']);
										$sch_timein 		= $split_sch[0];
										$sch_timeout 		= $split_sch[1];
									}

									$am_pm_timein 		= date("a",strtotime($value['timeIn']));
									$am_pm_timeout 		= date("a",strtotime($value['timeOut']));
									$am_pm_timeout_sch 	= date("a",strtotime($sch_timeout));
									$am_pm_timein_sch 	= date("a",strtotime($sch_timein));

									$act_in 			= strtotime($value['date']." ".$sch_timein);
									$nextdate 			= date('d-m-Y', strtotime("+1 day", strtotime($value['date'])));

									if($am_pm_timein_sch == "pm" && $am_pm_timeout_sch == "am"){
										$act_out = strtotime($nextdate." ".$sch_timeout);
									}else{
										$act_out = strtotime($value['date']." ".$sch_timeout);
									}

									if($value['nextday']){
										$time_in 	= strtotime($value['date']." ".$value['timeIn']);
										$time_out 	= strtotime($value['nextday']." ".$value['timeOut']);
									}else{
										$time_in 	= strtotime($value['date']." ".$value['timeIn']);
										$time_out 	= strtotime($value['date']." ".$value['timeOut']);
									}

									$nd_in 	= strtotime($value['date']." 22:00:00");
									$nd_out = strtotime($nextdate." 06:00:00");


									$timespan = Array(
										"timein" => $time_in,
										"actin" => $act_in,
										"actout" => $act_out,
										"ndin" => $nd_in,
										"ndout" => $nd_out,
										"timeout" => $time_out
									);

									uasort($timespan, function ($a,$b){
										if ($a==$b) return 0;
										return ($a<$b)?-1:1;
									});

									$identifier = Array(0,0);
									$temps = min($timespan);
									$results = Array("rot" => 0);

									// LOOP
									foreach ($timespan as $keyss => $values) {

										$ranges = $values - $temps;

										switch (implode($identifier)){
											case "00" : $type = "rot" ;break;
											case "10" : $type = "normal" ;break;
											case "11" : $type = "nd" ;break;
											case "01" : $type = "ndot" ;
										}

										switch ($keyss){
											case "actin" : $identifier[0] = 1;break;
											case "actout" : $identifier[0] = 0;break;
											case "ndin" : $identifier[1] = 1;break;
											case "ndout" : $identifier[1] = 0;
										}

										if (!($values>$time_in)){
											$temps = $values;
											continue;
										}

										if ($values==$time_out){
											if (array_key_exists($type,$results)){
												$results[$type] += $ranges;
											}else{
												$results[$type] = $ranges;
											}
											break;
										};
										$results[$type] = array_key_exists($type,$results) ?
											($results[$type] + $ranges) :
											($ranges);
										($type == "ndot") && ($values<=$act_in) && ($results["rot"] += $ranges) && (!($results["ndot"] = 0));

										$temps = $values;
									}// END LOOP

									extract($results);
									(!isset($normal)) && ($normal = 0);
									(!isset($nd)) && ($nd = 0);
									(!isset($ndot)) && ($ndot = 0);
									(!isset($rot)) && ($rot = 0);

									// TOTAL WAKTU ND
									//$total_nd = (round(abs($nd)/3600)*60);
									$total_nd = round(abs($nd)/3600);

									// if($value['employee_id'] == '2018101' && $value['date'] == '2019-12-02'){
									// 	return [$total_ndot,$total_nd,$total, $condition1,
									// 		$value['OvertimeRestDay'],
									// 		(strtotime($value['timeIn']) <= strtotime('05:59')),


									// 	];
									// }
									// if(strlen($value['schedule']) >=2 && strlen($value['schedule']) <= 3){									
									// 	$bio_ampm_in 	= date("a",strtotime($value['timeIn']));
									// 	$bio_ampm_out 	= date("a",strtotime($value['timeOut']));
									// }

									if($total_nd > 0 || $total_ndot > 0){									
										if($value['new_color_overtime'] == 'green' || $value['OvertimeRestDay_Color'] == 'green'){
											//$total_ndot = (round(abs($ndot)/3600)*60);
											$total_ndot = round(abs($ndot)/3600);
											if($total_ndot > 0){
												$total_ndot--;
											}
										}else{
											$total_ndot = 0;
										}
									}else{
										$total_nd = 0;
									}
									// 	if($value['employee_id'] == '2011008' && $value['date'] == '2019-11-29'){
									// 	return [$result[$value['employee_id']] ,
									// 	$total_nd,$total_ndot
									// 	];
									// }

									// if($value['employee_id'] == "2016018" && $value['date'] == '2019-12-08'){
									// 	return [ [$total_nd,$nd], [$total_ndot,$ndot],$value];
									// }

									$total_rot = round(abs($rot)/3600);

									if($total_nd == 0){
										if($act_in < $nd_in && $act_out < $nd_in){ $total_nd = 0; }
									}

							  		array_push($test,$value['nextday']." ".$value['timeOut']." to ".$value['date']." ".$value['timeIn']." ,schedule : ".$value['schedule']." total_rot : ".$total_rot." ,total_nd : $total_nd, total_ndot : $total_ndot,employee_id :".$value['employee_id']);

									// di pindah ke bawah
									if(!isset($result[$value['employee_id']]['nd'])){
										$result[$value['employee_id']]['nd'] = 0;
									}
									/*if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] != 0){

										try{
											$result[$value['employee_id']]['nd'] += ($total_nd);
									  	}catch(\Exception $e){}

							  		}else if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] == 0){
										$result[$value['employee_id']]['nd'] += ($total_nd);
							  		}else{
										$result[$value['employee_id']]['nd'] = ($total_nd);
							  		}*/

								} // IF condition1__1
								else{
									if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] != 0){
										$result[$value['employee_id']]['nd'] = $result[$value['employee_id']]['nd'] + 0;
									}else if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] == 0){
										$result[$value['employee_id']]['nd'] = 0;
									}else{
										$result[$value['employee_id']]['nd'] = 0;
									}
						  		}// END IF condition1__1

						  // 		if($value['employee_id'] == '2018048' && $value['date']=='2019-11-28'){
								// 	return [$total_ndot,$total_nd,$condition1,$ndot];
								// }



								// if(isset($result[$value['employee_id']]['ndot'])){ // IF NDOT__1
								// 	if($value['new_color_overtime'] == 'green' || $value['OvertimeRestDay_Color'] == 'green'){
								// 		$result[$value['employee_id']]['ndot'] += $total_ndot;
								// 	}
								// }else{
								// 	$result[$value['employee_id']]['ndot'] = $total_ndot;
								// } // END IF NDOT__1

								// if( /*($total_nd > 0 && $value['new_color_overtime'] == 'green') || ( $total_nd > 0 && $value['OvertimeRestDay_Color'] == 'green')*/ 
								// 	$value['date']== '2019-11-30' && $value['employee_id'] == "2016015"){
								// 	return [200,$value,$total_nd,$total_ndot,$total_rot];
								// 	// if($value['employee_id'] == "2019023" && $value['date'] == '2019-11-30'){
								// 							// 	return [200,$value,$current_date_ampm_in, $reduce,$total_nd,$total_ndot];
								// 							// }
								// }


								// OVERTIME ========
								// total_rot untuk ND
								$val_ndot = 0;
								$val_rdnd = 0;
								$ot_total_ot = 0;
								// if($value['employee_id'] == '2016018' && $value['date']=='2019-11-30'){
						  // 			return [300,$value['overtime_hours'],$result[$value['employee_id']],$partial_process

						  // 			];
						  // 		}
								if($value['new_color_overtime'] == 'green'){ // IF OVERTIME__1
									$exp_tot 	= explode(':',$value['total_overtime']);
									$tot1 		= intval($exp_tot[0]) * 60;
									/*$tot2 		= intval($exp_tot[1]);
									$ot_total_ot 	=  round((($tot1 + $tot2)/60),2);*/
									$tot2 		= intval($exp_tot[1]);
									$ot_total_ot 	=  round((($tot1 + 0)/60),2);

									$ot_total_ot += "0.".$tot2;
									

									if(isset($result[$value['employee_id']]['rot'])){
										if($total_rot){
											$value['overtime_hours'] = /*$total_rot*/ $ot_total_ot;
											//$result[$value['employee_id']]['rot'] += /*(($ot_total_ot-$total_rot)*60)*/ /*($ot_total_ot-$total_rot)*/ /*$total_rot*/ $ot_total_ot;
										}else{
											$value['overtime_hours'] = $ot_total_ot;
											//$result[$value['employee_id']]['rot'] += /*($ot_total_ot*60)*/ $ot_total_ot;
										}
									}else{
							 			if($total_rot){
							 				$value['overtime_hours'] = /*$total_rot*/ $ot_total_ot;
											//$result[$value['employee_id']]['rot'] = /*(($ot_total_ot-$total_rot)*60)*/ /*($ot_total_ot-$total_rot)*/ /*$total_rot*/ $ot_total_ot;
										}else{
											$value['overtime_hours'] = $ot_total_ot;
											//$result[$value['employee_id']]['rot'] = /*($ot_total_ot*60)*/ $ot_total_ot;
										}
									}


									if($total_ndot > 0){		
										$val_ndot = $total_ndot;								
										/*if(strtotime($value['timeOut']) > strtotime('22:00')){
											if($ot_total_ot > $total_ndot){
											}else{
												$val_ndot = $total_ndot;
											}
											if($val_ndot >= 8){
												$val_ndot = $total_ndot;
											}
										}else if(strtotime($value['timeOut']) > strtotime('00:00')){
											
											if($ot_total_ot > $total_ndot){
												$val_ndot = $total_ndot;
											}else{
												$val_ndot = $ot_total_ot;
											}
											if($val_ndot >= 8){
												$val_ndot = $total_ndot;
											}
										}*/
									}else{
										if(strtotime($value['timeOut']) > strtotime('22:00')){
												/*$val_ndot = $ot_total_ot;
												if($val_ndot >= 8){
													$val_ndot = 0;
												}*/

											// if($ot_total_ot > $total_ndot){
											// }else{
											// 	$val_ndot_ = $total_ndot;
											// }
										}else if(strtotime($value['timeOut']) > strtotime('00:00') && $value['nextday']){
											/*$val_ndot = $ot_total_ot;
											if($val_ndot >= 8){
												$val_ndot = 0;
											}*/

											// if($ot_total_ot > $total_ndot){
											// 	$val_ndot_ = $total_ndot;
											// }else{
											// 	$val_ndot = $ot_total_ot;
											// }
										}
									}
									
						  		}else{
						  			$value['overtime_hours'] = 0;
									if(isset($result[$value['employee_id']]['rot']) && $result[$value['employee_id']]['rot'] != 0){
							  			$result[$value['employee_id']]['rot'] = floatval($result[$value['employee_id']]['rot']) + 0;
							 		}else if(isset($result[$value['employee_id']]['rot']) && $result[$value['employee_id']]['rot'] == 0){
										$result[$value['employee_id']]['rot'] = 0;
									}else{
							  			$result[$value['employee_id']]['rot'] = 0;
									}

									if(isset($result[$value['employee_id']]['rots']) && $result[$value['employee_id']]['rots'] != 0){
							  			$result[$value['employee_id']]['rots'] = floatval($result[$value['employee_id']]['rots']) + 0;
							 		}else if(isset($result[$value['employee_id']]['rots']) && $result[$value['employee_id']]['rots'] == 0){
										$result[$value['employee_id']]['rots'] = 0;
									}else{
							  			$result[$value['employee_id']]['rots'] = 0;
									}
						  		}// END IF OVERTIME__1

						  		

								if($value['OvertimeRestDay'] != '-' && $value['OvertimeRestDay'] != null && $value['OvertimeRestDay'] != 0 && $value['OvertimeRestDay_Color'] == 'green'){ // IF OVERREST__1
									$exp_tot = explode(':',$value['OvertimeRestDay']);
									$tot1 = intval($exp_tot[0]) * 60;
									$tot2 = intval($exp_tot[1]);
									$ot_total =  round((($tot1 + 0)/60),2);

									$ot_total += "0.".$tot2;
									/*if($ot_total > 0){
										$ot_total += "0.".$tot2;
									}*/

									$value['overtimerest_hours'] = $ot_total;
									if(isset($result[$value['employee_id']]['rotdo'])){
										$result[$value['employee_id']]['rotdo'] += /*($ot_total*60)*/ $ot_total;
									}else{
										$result[$value['employee_id']]['rotdo'] = /*($ot_total*60)*/ $ot_total;
									}
								}else{

									$value['overtimerest_hours'] = 0;
									if(isset($result[$value['employee_id']]['rotdo']) && $result[$value['employee_id']]['rotdo'] != 0){
										$result[$value['employee_id']]['rotdo'] = floatval($result[$value['employee_id']]['rotdo']) + 0;
									}else if(isset($result[$value['employee_id']]['rotdo']) && $result[$value['employee_id']]['rotdo'] == 0){
										$result[$value['employee_id']]['rotdo'] = 0;
									}else{
										$result[$value['employee_id']]['rotdo'] = 0;
									}
								} // END IF OVERREST__1

								$iexp  =  explode('-',$value['date']);

								if(isset($iexp[2])){
									$iexp = $dictionary[intval($iexp[2])];
								}

								$alpha_stat = $dictionary[$y];
								$result[$value['employee_id']][$alpha_stat] =  $total;

								// check hoiday regular/special

								$tmp_field = array_search($alpha_stat,$range_field);

								if(gettype($tmp_field) == "boolean"){
									array_push($range_field, $alpha_stat);
								}

								if($total == "EL"){
									$result[$value['employee_id']]["status".$alpha_stat] = "red";
								}elseif($total == "SL"){
									$result[$value['employee_id']]["status".$alpha_stat] = "red";
						  		}elseif($total == "VL"){
									$result[$value['employee_id']]["status".$alpha_stat] = "green";
						  		}elseif($total == "BL"){
									$result[$value['employee_id']]["status".$alpha_stat] = "red";
						  		}elseif($total == "DO"){
									$result[$value['employee_id']]["status".$alpha_stat] = "white";
						  		}else{
									if($total != '/'){
							   			$result[$value['employee_id']]["status".$alpha_stat] = "yellow";
									}else{

							  			try{ // TRY__1
											if( ($value['timeIn'] != "00:00" && $value['C_TimeIn'] != "red") && ($value['timeOut'] != "00:00" && $value['C_TimeOut'] != "red") ){
												$result[$value['employee_id']][$alpha_stat] =  $total;
											}else{

												if(in_array($value['schedule'], ['BL','T','TB','OB']) || in_array($value['schedule_shift_code'], ['BL','T','TB','OB'])){
													$result[$value['employee_id']][$alpha_stat] = $value['schedule'];
													$result[$value['employee_id']]["status".$alpha_stat] = "red";
												}else{

									  				$result[$value['employee_id']][$alpha_stat] =  "ABS";

									  				if(!isset($result[$value['employee_id']]['abs'])){
														$result[$value['employee_id']]['abs'] = 1;
									  				}else{
														$result[$value['employee_id']]['abs']++;
									  				}
												}
											}

											$result[$value['employee_id']]["status".$alpha_stat] = "white";
							  			}catch (\Exception $e) {
											return [$e->getMessage(),$result[$value['employee_id']]];
							  			}// END TRY__1
									}
						  		}

						  		// ADD INFO DEPARTMENT FOR GROUPING
						  		if(!isset($result[$value['employee_id']]['department_id'])){ $result[$value['employee_id']]['department_id'] = $value['department']; }
						  		if(!isset($result[$value['employee_id']]['department_name'])){ $result[$value['employee_id']]['department_name'] = $value['department_name']; }
						  		if(!isset($result[$value['employee_id']]['employee_name'])){ $result[$value['employee_id']]['employee_name'] = $value['Name']; }
						  		if(!isset($result[$value['employee_id']]['local_it'])){ $result[$value['employee_id']]['local_it'] = $value['local_it']; }

						  		// LEAVE WITH PAY
						  		if(!isset($result[$value['employee_id']]['leave_pay'])){
						  			$result[$value['employee_id']]['leave_pay'] = ["BL"=>0,"MA"=>0,"PL"=>0,"BE"=>0,"total"=>0];
						  		}


						  		// ABSENCE Working
						  		if(!isset($result[$value['employee_id']]['leave_working'])){
						  			$result[$value['employee_id']]['leave_working'] = ['VL'=>0,'EL'=>0,'SL'=>0,'BL'=>0,'BE'=>0,'BEL'=>0,'MAL'=>0,'MA'=>0,'PL'=>0,'ML'=>0,'SUS'=>0,'total'=>0];
						  		}



						  		try{
						  			$leave_green = array_keys($result[$value['employee_id']]['leave_working']);
						  			$leave_red = array_keys($result[$value['employee_id']]['leave_pay']);

						  			// if($value['schedule'] == 'VL'){
						  			// 	return $value;
						  			// }
						  			if(in_array($value['schedule'], $leave_green)){

						  				//if($value['C_TimeIn'] == 'green'){ /*menggunakan waktu libur BL untuk kerja*/

						  					$result[$value['employee_id']]['leave_working'][$total]++;
								  			$result[$value['employee_id']]['leave_working']["total"]++;
						  				//}
						  				// else if($value['C_TimeIn'] == 'red'){

								  		// 	if(in_array($total, $leave_red)){ /*MENGGUNAKAN HAK CUTI*/
								  		// 		$result[$value['employee_id']]['leave_pay'][$total]++;
								  		// 		$result[$value['employee_id']]['leave_pay']["total"]++;
								  		// 	}
						  				// }
									}

									if(in_array($value['schedule'], $leave_green)){
										//if($value['C_TimeIn'] == 'red'){

								  			if(in_array($total, $leave_red)){ /*MENGGUNAKAN HAK CUTI*/
								  				$result[$value['employee_id']]['leave_pay'][$total]++;
								  				$result[$value['employee_id']]['leave_pay']["total"]++;
								  			}
						  				//}
									}

						  		}catch(\Exception $e){}

						  		if(!isset($result[$value['employee_id']]['got'])){ $result[$value['employee_id']]['got'] = 0; }
						  		if( !isset($result[$value['employee_id']]['rots']) ){ $result[$value['employee_id']]['rots'] = 0; }
						  		if( !isset($result[$value['employee_id']]['rot']) ){ $result[$value['employee_id']]['rot'] = 0; }

						  		// Special Holiday
						  		if( !isset($result[$value['employee_id']]['sh']) ){ $result[$value['employee_id']]['sh'] = 0; }
								if( !isset($result[$value['employee_id']]['shot']) ){ $result[$value['employee_id']]['shot'] = 0; }
								if( !isset($result[$value['employee_id']]['shrdot_1st']) ){ $result[$value['employee_id']]['shrdot_1st'] = 0; }
								if( !isset($result[$value['employee_id']]['shrdot_excess']) ){ $result[$value['employee_id']]['shrdot_excess'] = 0; }
								// Regular Holiday
								if( !isset($result[$value['employee_id']]['rh']) ){ $result[$value['employee_id']]['rh'] = 0; }
								if( !isset($result[$value['employee_id']]['rhot']) ){ $result[$value['employee_id']]['rhot'] = 0; }
								if( !isset($result[$value['employee_id']]['rhrdot_1st']) ){ $result[$value['employee_id']]['rhrdot_1st'] = 0; }
								if( !isset($result[$value['employee_id']]['rhrdot_excess']) ){ $result[$value['employee_id']]['rhrdot_excess'] = 0; }
								// Nigth Deferrentials
								if( !isset($result[$value['employee_id']]['rnd']) ){ $result[$value['employee_id']]['rnd'] = 0; }
								if( !isset($result[$value['employee_id']]['rnd']) ){ $result[$value['employee_id']]['rndot'] = 0; }
								if( !isset($result[$value['employee_id']]['ndot']) ){ $result[$value['employee_id']]['ndot'] = 0; }
								if( !isset($result[$value['employee_id']]['rhnd']) ){ $result[$value['employee_id']]['rhnd'] = 0; }
								if( !isset($result[$value['employee_id']]['shnd']) ){ $result[$value['employee_id']]['shnd'] = 0; }

								if( !isset($result[$value['employee_id']]['rhndot']) ){ $result[$value['employee_id']]['rhndot'] = 0; }
								if( !isset($result[$value['employee_id']]['shndot']) ){ $result[$value['employee_id']]['shndot'] = 0; }
								
								if( !isset($result[$value['employee_id']]['rdnd']) ){ $result[$value['employee_id']]['rdnd'] = 0; }
								if( !isset($result[$value['employee_id']]['rdndot']) ){ $result[$value['employee_id']]['rdndot'] = 0; }
								if( !isset($result[$value['employee_id']]['rhrdnd']) ){ $result[$value['employee_id']]['rhrdnd'] = 0; }
								if( !isset($result[$value['employee_id']]['shrdnd']) ){ $result[$value['employee_id']]['shrdnd'] = 0; }
								if( !isset($result[$value['employee_id']]['rhrdndot']) ){ $result[$value['employee_id']]['rhrdndot'] = 0; }
								if( !isset($result[$value['employee_id']]['shrdndot']) ){ $result[$value['employee_id']]['shrdndot'] = 0; }

								if( !isset($result[$value['employee_id']]['working_day']) ){ $result[$value['employee_id']]['working_day'] = 0; }
								if( !isset($result[$value['employee_id']]['diciplin']) ){ $result[$value['employee_id']]['diciplin'] = 0; }
								if( !isset($result[$value['employee_id']]['no_day_off']) ){ $result[$value['employee_id']]['no_day_off'] = 0; }
								if( !isset($result[$value['employee_id']]['lates']) ){ $result[$value['employee_id']]['lates'] = 0; }
								if( !isset($result[$value['employee_id']]['undertime']) ){ $result[$value['employee_id']]['undertime'] = 0; }
								if( !isset($result[$value['employee_id']]['undertime']) ){ $result[$value['employee_id']]['undertime'] = 0; }

								if( !isset($result[$value['employee_id']]['rotdo_1st']) ){ $result[$value['employee_id']]['rotdo_1st'] = 0; }
								if( !isset($result[$value['employee_id']]['rotdo_excess']) ){ $result[$value['employee_id']]['rotdo_excess'] = 0; }
								

								//===================================================================================
								// COUNT ABS
								$result[$value['employee_id']]['disciplin'] = $result[$value['employee_id']]['abs'];

								// COUNT LATE
								if($value['Late'] != '-'  && $value['Late'] != null && $value['Late'] != 0 && $value['colorLate'] != 'green'){
									$result[$value['employee_id']]['lates'] += $value['Late'];
								}

								// COUNT EARLYOUT
								if($value['EarlyOut'] != '-' && $value['EarlyOut'] != null && $value['EarlyOut'] != 0 && $value['early_c'] != 'green' ){
									$result[$value['employee_id']]['undertime'] += $value['EarlyOut'];
								}

								$reduce_sisa = false;
								$count_rh = false;
								$count_sh = false;

								// COUNT OVERTIME REST DAY AND ND.DO
								
								$value['checked'] = false;

								// if($value['employee_id'] == '2016018' && $value['date']=='2019-11-30'){
						  // 			return [300,$value['overtime_hours'],$result[$value['employee_id']],$partial_process

						  // 			];
						  // 		}
								

								if($total == 'DO' || strlen($value['schedule']) <= 3){
									if($value['timeOut'] != "-" && $value['timeIn'] != "-" && $value['OvertimeRestDay_Color'] == 'green'){

										$result[$value['employee_id']]['working_day']++;

										$current_date_ampm_out 	= date("a",strtotime($value['timeOut']));
										$current_date_ampm_in 	= date("a",strtotime($value['timeIn']));

										$ndval_start 		= 0;
										$ndval_end 			= 0;
										$partial_process 	= false;
										
										if( 
											($current_date_ampm_in == 'pm' && $current_date_ampm_out == 'am' && $value['nextday']) || 
											($current_date_ampm_in == 'pm' && $current_date_ampm_out == 'pm' && strtotime($value['timeIn']) >= strtotime('21:00')) || 
											($current_date_ampm_in == 'am' && $current_date_ampm_out == 'pm' && strtotime($value['timeOut']) >= strtotime('21:00'))
										){

											//foreach ($data['data'] as $key1 => $value1) {

												// CHECKING FOR LAST DATE
												/*$value_LAST = false;
												if($value1['employee_id'] == $value['employee_id'] && $value['nextday'] == $end_last_date){
													if($arr["status"]==200){

														foreach ($arr['data'] as $key2 => $value2){
															if($value2['employee_id'] == $value['employee_id'] && $value2['date'] == $value['nextday']){
																$value_LAST = $value2;
															}
														}
													}
												}

												if($value_LAST){
													$value1 = $value_LAST;
												}*/

												//if($value1['employee_id'] == $value['employee_id'] && $value1['date'] == $value['nextday'] && !$value['checked']){

													
													// cek total jam DO sudah melebihi 8 jam
													if($value['nextday']){
														$date_checking_start 	= $value['date']." ".$value['timeIn'];
														$date_checking_end 		= $value['nextday']." ".$value['timeOut'];
													}else{
														$date_checking_start 	= $value['date']." ".$value['timeIn'];
														$date_checking_end 		= $value['date']." ".$value['timeOut'];
													}
													$timeIn_biometrik 		= date_create($date_checking_start);
													$timeOut_biometrik 		= date_create($date_checking_end);

													$diff_check 	= date_diff($timeIn_biometrik,$timeOut_biometrik);
													$total_h 		= (integer)$diff_check->format("%H");
													$increase_8h 	=  date('Y-m-d H:i', strtotime("+8 hour", strtotime($date_checking_start)));
													
													if($total_h >= 8){

														$split_8h 		= explode(" ", $increase_8h);
														$am_check_8h 	= date("a",strtotime($split_8h[1]));

														
														// ND
														$result_nd = 0;
														$result_ndot = 0;

														if($am_check_8h == 'pm' && strtotime($split_8h[1]) >= strtotime("22:00") ){
															
															$nd_start 	= date_create($value['date']." 22:00");
															$nd_end 	= date_create($increase_8h);

															$diff_nd 	= date_diff($nd_start,$nd_end);
															$result_nd  = (integer)$diff_nd->format("%H");

															$ndot_start = date_create($increase_8h);

															if(strtotime($value['timeOut']) > strtotime("22:00") ){
																$ndot_end 	= date_create($value['date']." ".$value['timeOut']);
															}else{
																if($value['nextday']){															
																	if(strtotime($value['timeOut']) <= strtotime("06:00")){
																		$ndot_end 	= date_create($value['nextday']." ".$value['timeOut']);
																	}else{
																		$ndot_end 	= date_create($value['nextday']." 06:00");
																	}
																}
															}

															$diff_ndot 	= date_diff($ndot_start,$ndot_end);
															$result_ndot  = (integer)$diff_ndot->format("%H");
															$partial_process = true;
															//return [$result_nd,$result_ndot,$value,1];

														}else if($am_check_8h == 'am' && strtotime($split_8h[1]) <= strtotime("06:00") ){
															$nd_start 	= date_create($value['date']." 22:00");
															$nd_end 	= date_create($increase_8h);

															$diff_nd 	= date_diff($nd_start,$nd_end);
															$result_nd  = (integer)$diff_nd->format("%H");

															$ndot_start = date_create($increase_8h);
															if(strtotime($value['timeOut']) <= strtotime("06:00")){
																$ndot_end 	= date_create($value['nextday']." ".$value['timeOut']);
															}else{
																$ndot_end 	= date_create($value['nextday']." 06:00");
															}

															$diff_ndot 	= date_diff($ndot_start,$ndot_end);
															$result_ndot  = (integer)$diff_ndot->format("%H");
															$partial_process = true;
														//	return [$result_nd,$result_ndot,$value,2,$increase_8h,$value['nextday']." ".$value['timeOut']];

														}else if($am_check_8h == 'pm' && strtotime($split_8h[1]) < strtotime("22:00")){

															$ndot_start = date_create(/*$increase_8h*/ $value['date']." 22:00" );

															if(strtotime($value['timeOut']) > strtotime("22:00") ){
																$ndot_end 	= date_create($value['date']." ".$value['timeOut']);
															}else{
																if($value['nextday']){															
																	if(strtotime($value['timeOut']) <= strtotime("06:00")){
																		$ndot_end 	= date_create($value['nextday']." ".$value['timeOut']);
																	}else{
																		$ndot_end 	= date_create($value['nextday']." 06:00");
																	}
																}
															}

															if(isset($ndot_end)){															
																$diff_ndot 	= date_diff($ndot_start,$ndot_end);
																$result_ndot  = (integer)$diff_ndot->format("%H");
																$partial_process = true;
															}
															//return [$result_nd,$result_ndot,$value,3];

															
														}
													}


													if($value['check_holiday'] == '(RH)'){
														$result[$value['employee_id']]['rhrdnd'] += $result_nd;
														$result[$value['employee_id']]['rhrdndot'] += $result_ndot;

													}else if($value['check_holiday'] == '(SH)'){
														$result[$value['employee_id']]['shrdnd'] += $result_nd;
														$result[$value['employee_id']]['shrdndot'] += $result_ndot;
													}else{
														$result[$value['employee_id']]['rdnd'] += $result_nd;
														$result[$value['employee_id']]['rdndot'] += $result_ndot;
													}
													


													// if($total_nd > 0){

													// 	if($current_date_ampm_in == 'pm' && $current_date_ampm_out == 'am'){

													// 		if(strtotime($value['timeIn']) < strtotime("22:00")){
													// 			$date_ot_in1 = date_create($value['date']." 22:00");
													// 		}else{
													// 			$date_ot_in1 = date_create($value['date']." ".$value['timeIn']);
													// 		}

													// 		$date_ot_out1 = date_create($value['nextday']." 00:00");
													// 		$diff1 				= date_diff($date_ot_in1,$date_ot_out1);
													// 		$ndval_start 	= (integer)$diff1->format("%H");

													// 		if(strtotime($value['timeOut']) > strtotime("06:00")){
													// 			$date_ot_in2 = date_create($value['nextday']." 06:00");
													// 		}else{
													// 			$date_ot_in2 = date_create($value['nextday']." ".$value['timeOut']);
													// 		}

													// 		$date_ot_out2 = date_create($value['nextday']." 00:00");
													// 		$diff2 				= date_diff($date_ot_in2,$date_ot_out2);
													// 		$ndval_end 	= (integer)$diff2->format("%H");


													// 	}elseif($current_date_ampm_in == 'pm' && $current_date_ampm_out == 'pm' && strtotime($value['timeIn']) >= strtotime('21:00') ){
													// 		if(strtotime($value['timeIn']) < strtotime("22:00")){
													// 			$date_ot_in1 = date_create($value['date']." 22:00");
													// 		}else{
													// 			$date_ot_in1 = date_create($value['date']." ".$value['timeIn']);
													// 		}

													// 		$date_ot_out1 = date_create($value['nextday']." 00:00");
													// 		$diff1 				= date_diff($date_ot_in1,$date_ot_out1);
													// 		$ndval_start 	= (integer)$diff1->format("%H");															

													// 		if(strtotime($value['timeOut']) > strtotime("06:00")){
													// 			$date_ot_in2 = date_create($value['nextday']." 06:00");
													// 		}else{
													// 			$date_ot_in2 = date_create($value['nextday']." ".$value['timeOut']);
													// 		}

													// 		$date_ot_out2 = date_create($value['nextday']." 00:00");
													// 		$diff2 				= date_diff($date_ot_in2,$date_ot_out2);
													// 		$ndval_end 	= (integer)$diff2->format("%H");
													// 	}

														
													// 	if($value['check_holiday'] == null && $value1['check_holiday'] == null){
													// 		$result[$value['employee_id']]['rdnd'] 	+= $ndval_start;
													// 		if($value['schedule'] != 'DO'){ $result[$value['employee_id']]['nd'] += $ndval_end; }
													// 		$value['checked'] = true;
															
													// 	}else if($value['check_holiday'] == null && $value1['check_holiday'] == '(RH)'){
													// 		$result[$value['employee_id']]['rdnd'] 	+= $ndval_start;
													// 		$result[$value['employee_id']]['rhrdnd'] 	+= $ndval_end;
													// 		$value['checked'] = true;
														
													// 	}else if($value['check_holiday'] == null && $value1['check_holiday'] == '(SH)'){
													// 		$result[$value['employee_id']]['rdnd'] 	+= $ndval_start;
													// 		$result[$value['employee_id']]['shrdnd'] 	+= $ndval_end;
													// 		$value['checked'] = true;
														
													// 	}else if($value['check_holiday'] == '(RH)' && $value1['check_holiday'] == null){
													// 		$result[$value['employee_id']]['rhrdnd'] 	+= $ndval_start;
															
													// 		if($value['schedule'] != 'DO'){ $result[$value['employee_id']]['nd'] 	+= $ndval_end; }
													// 		else{ $result[$value['employee_id']]['rdnd'] 	+= $ndval_end; }
															
													// 		$value['checked'] = true;
														
													// 	}else if($value['check_holiday'] == '(RH)' && $value1['check_holiday'] == "(SH)"){
													// 		$result[$value['employee_id']]['rhrdnd'] 	+= $ndval_start;
													// 		$result[$value['employee_id']]['shrdnd'] 	+= $ndval_end;
													// 		$value['checked'] = true;
														
													// 	}else if($value['check_holiday'] == '(SH)' && $value1['check_holiday'] == null){
													// 		$result[$value['employee_id']]['shrdnd'] 	+= $ndval_start;
															
													// 		if($value['schedule'] != 'DO'){ $result[$value['employee_id']]['nd'] 	+= $ndval_end; }
													// 		else{ $result[$value['employee_id']]['rdnd'] 	+= $ndval_end; }

													// 		$value['checked'] = true;
														
													// 	}else if($value['check_holiday'] == '(SH)' && $value1['check_holiday'] == "(RH)"){
													// 		$result[$value['employee_id']]['shrdnd'] 	+= $ndval_start;
													// 		$result[$value['employee_id']]['rhrdnd'] 	+= $ndval_end;
													// 		$value['checked'] = true;
													// 	}
													// 	$partial_process = true;
													// }// ND

												//}// MATCH
											//}// end FOR
										}

										if($value['OvertimeRestDay_Color'] == 'green'){

											// CEK HOLIDAY
											if($value['check_holiday'] == '(RH)'){

												// reduce nextday for RH dan SH

												
												// COUNT RH
												$result[$value['employee_id']]['rh']++;
												
												if($value['overtimerest_hours'] > 8){

													$result[$value['employee_id']]['rhrdot_1st'] += 8;
													$result[$value['employee_id']]['rhrdot_excess'] += $value['overtimerest_hours']-8;
												}else{

													$result[$value['employee_id']]['rhrdot_1st'] += $value['overtimerest_hours'];
												}
											}
											else if($value['check_holiday'] == '(SH)'){
												
												$result[$value['employee_id']]['sh']++;

												if($value['overtimerest_hours'] > 8){

													$result[$value['employee_id']]['shrdot_1st'] += 8;
													$result[$value['employee_id']]['shrdot_excess'] += $value['overtimerest_hours']-8;
												}else{
													
													$result[$value['employee_id']]['shrdot_1st'] += $value['overtimerest_hours'];
												}
											}else{ // NO HOLIDAY

												if($result[$value['employee_id']]['rotdo'] > 8){
													
													$result[$value['employee_id']]['rotdo_1st'] += 8;
													$result[$value['employee_id']]['rotdo_excess'] += ($value['overtimerest_hours'] - 8);
												}else{

													$result[$value['employee_id']]['rotdo_1st'] += $value['overtimerest_hours'];
												}
											}
										}

										// ND
										if(!$partial_process){ // waktu malam
											$result_ndot = 0;
											if($current_date_ampm_in == 'am' && $current_date_ampm_out == 'am'){
												if(strtotime($value['timeIn']) <= strtotime("06:00") && strtotime($value['timeOut']) <= strtotime("06:00")){
													$nd_start 	= date_create($value['date']." ".$value['timeIn']);
													$nd_end 	= date_create($value['date']." ".$value['timeOut']);

													$diff_ndot 	= date_diff($nd_start,$nd_end);
													$result_ndot  = (integer)$diff_ndot->format("%H");
												}else if(strtotime($value['timeIn']) <= strtotime("06:00") && strtotime($value['timeOut']) > strtotime("06:00")){
													$nd_start 	= date_create($value['date']." ".$value['timeIn']);
													$nd_end 	= date_create($value['date']." 06:00");

													$diff_ndot 	= date_diff($nd_start,$nd_end);
													$result_ndot  = (integer)$diff_ndot->format("%H");
												}
											}else{
												if($current_date_ampm_in == 'am' && $current_date_ampm_out == 'pm'){
													if(strtotime($value['timeIn']) <= strtotime("06:00") && strtotime($value['timeOut']) >= strtotime("06:00")){
														$nd_start 	= date_create($value['date']." ".$value['timeIn']);
														$nd_end 	= date_create($value['date']." 06:00");

														$diff_ndot 	= date_diff($nd_start,$nd_end);
														$result_ndot  = (integer)$diff_ndot->format("%H");
													}
												}
											}


											if($value['check_holiday'] == '(RH)'){
												//$result[$value['employee_id']]['rhrdnd'] += $result_nd;
												$result[$value['employee_id']]['rhrdndot'] += $result_ndot;

											}else if($value['check_holiday'] == '(SH)'){
												//$result[$value['employee_id']]['shrdnd'] += $result_nd;
												$result[$value['employee_id']]['shrdndot'] += $result_ndot;
											}else{
												//$result[$value['employee_id']]['rdnd'] += $result_nd;
												$result[$value['employee_id']]['rdndot'] += $result_ndot;
											}
											


											/*$val_rdnd = 0;
											if($value['OvertimeRestDay_Color'] == 'green'){											
												if(strtotime($value['timeOut']) >= strtotime("22:00")){
													$val_rdnd = $total_nd;
												}elseif(strtotime($value['timeOut']) <> strtotime("06:00")) {
													if($total_nd <= $value['overtimerest_hours']){
														$val_rdnd = $total_nd;
													}else{
														$val_rdnd = $value['overtimerest_hours'];
													}
												}
											}

											if($value['check_holiday'] == '(RH)'){

												$result[$value['employee_id']]['rh']++;
												$result[$value['employee_id']]['rhrdnd'] += $val_rdnd;

											}else if($value['check_holiday'] == '(SH)'){
												
												$result[$value['employee_id']]['sh']++;
												$result[$value['employee_id']]['shrdnd'] += $val_rdnd;

											}else{

												$result[$value['employee_id']]['rdnd'] += $val_rdnd;
											}*/

										}

										// if($value['employee_id'] == '2011008' && $value['date'] == '2019-11-30'){
										// 	return [$result[$value['employee_id']] , $ndval_end, $ndval_start, $partial_process,
										// 	($current_date_ampm_in == 'pm' && $current_date_ampm_out == 'am' && $value['nextday']),
										// 	($current_date_ampm_in == 'pm' && $current_date_ampm_out == 'pm' && strtotime($value['timeIn']) >= strtotime('21:00')),
										// 	$total_nd,$total_ndot
										// 	];
										// }

									}else{
										$result[$value['employee_id']]['no_day_off']++;
									}
								}else{

									// COUNT OVERTIME AND ND

									if($value['timeOut'] != "-" && $value['timeIn'] != "-"){
										
										$result[$value['employee_id']]['working_day']++;

										$time_sch = explode("-",$value['schedule']);
										try {
											
											$sch_date_ampm_out 		= date("a",strtotime($time_sch[1]));
											$sch_date_ampm_in 		= date("a",strtotime($time_sch[0]));
										} catch (\Exception $e) {
											return [$e, $value['schedule']];
										}
										$current_date_ampm_out 	= date("a",strtotime($value['timeOut']));
										$current_date_ampm_in 	= date("a",strtotime($value['timeIn']));

										$ndval_start 		= 0;
										$ndval_end 			= 0;
										$partial_process 	= false;
										
										if( 
											($sch_date_ampm_in == 'pm' && $sch_date_ampm_out == 'am' && $value['nextday']) || 
											($sch_date_ampm_in == 'pm' && $sch_date_ampm_out == 'pm' && strtotime($time_sch[1]) >= strtotime('21:00') && $value['nextday']) ){

											foreach ($data['data'] as $key1 => $value1) {

												// CHECKING FOR LAST DATE
												$value_LAST = false;
												if($value1['employee_id'] == $value['employee_id'] && $value['nextday'] == $end_last_date){
													if($arr["status"]==200){

														foreach ($arr['data'] as $key2 => $value2){
															if($value2['employee_id'] == $value['employee_id'] && $value2['date'] == $value['nextday']){
																$value_LAST = $value2;
															}
														}
													}
												}

												if($value_LAST){
													$value1 = $value_LAST;
												}

												if($value1['employee_id'] == $value['employee_id'] && $value1['date'] == $value['nextday'] && !$value['checked']){


													if($total_nd == 0 && $total_ndot == 0 && $value['new_color_overtime'] == 'green'){

														// COMPARE NDOT VALUE WITH OT
														if(strtotime($time_sch[1]) > strtotime("22:00")){
															$date_ot_in = date_create($value['date']." ".$time_sch[1]);
														}else{
															$date_ot_in = date_create($value['date']." 22:00");
														}

														if(strtotime($value['timeOut']) > strtotime("06:00")){
															$date_ot_out = date_create($value['nextday']." 06:00");
														}else{
															$date_ot_out = date_create($value['nextday']." ".$value['timeOut']);
														}

														$diff 		= date_diff($date_ot_in,$date_ot_out);
														$ndotval 	= (integer)$diff->format("%H");

														if($value['check_holiday'] == '(RH)'){
															$result[$value['employee_id']]['rhndot'] += $ndotval;
														}else if($value['check_holiday'] == '(SH)'){
															$result[$value['employee_id']]['shndot'] += $ndotval;
														}else{
															$result[$value['employee_id']]['ndot'] += $ndotval;
														}

													}


													if($total_nd > 1 && $total_ndot == 0){

														if(strtotime($value['timeIn']) < strtotime($time_sch[0]) ){
															if(strtotime($time_sch[0]) < strtotime("22:00")){
																$date_ot_in = date_create($value['date']." 22:00");
															}else{
																$date_ot_in = date_create($value['date']." ".$time_sch[0]);
															}
														}else{
															if(strtotime($value['timeIn']) < strtotime("22:00")){
																$date_ot_in = date_create($value['date']." 22:00");
															}else{
																$date_ot_in = date_create($value['date']." ".$value['timeIn']);
															}
														}

														$date_ot_out = date_create($value['nextday']." 00:00");
														$diff 				= date_diff($date_ot_in,$date_ot_out);
														$ndval_start 	= (integer)$diff->format("%H");
														$ndval_end 		= $total_nd - $ndval_start;

														
														if($value['check_holiday'] == null){
															$result[$value['employee_id']]['nd'] += $total_nd;
															$value['checked'] = true;
															
														}else if($value['check_holiday'] == '(RH)'){
															$result[$value['employee_id']]['rhnd'] 	+= $total_nd;
															$value['checked'] = true;
														
														}else if($value['check_holiday'] == '(SH)'){
															$result[$value['employee_id']]['shnd'] 	+= $total_nd;
															$value['checked'] = true;
														}
														$partial_process = true;
													}// ND

													// timeOut > 22:00
													if($total_nd > 0 && $total_ndot > 0 && $value['new_color_overtime'] == 'green'){
																													
														if(strtotime($value['timeIn']) < strtotime($time_sch[0]) ){
															if(strtotime($time_sch[0]) < strtotime("22:00")){
																$date_ot_in = date_create($value['date']." 22:00");
															}else{
																$date_ot_in = date_create($value['date']." ".$time_sch[0]);
															}
														}else{
															if(strtotime($value['timeIn']) < strtotime("22:00")){
																$date_ot_in = date_create($value['date']." 22:00");
															}else{
																$date_ot_in = date_create($value['date']." ".$value['timeIn']);
															}
														}

														$date_ot_out = date_create($value['nextday']." 00:00");
														$diff 				= date_diff($date_ot_in,$date_ot_out);
														$ndval_ot_start 	= (integer)$diff->format("%H");
														$ndval_ot_end 		= $total_ndot - $ndval_ot_start;

														if($value['check_holiday'] == null){

															if($val_ndot > 0){ $result[$value['employee_id']]['ndot'] 	+= $val_ndot; }
															else{ $result[$value['employee_id']]['ndot'] 	+= $total_ndot; }
															$result[$value['employee_id']]['nd'] 	+= $total_nd;

															$value['checked'] = true;
															
														}else if($value['check_holiday'] == '(RH)'){

															if($val_ndot > 0){ $result[$value['employee_id']]['rhnd'] 	+= $val_ndot; }
															else{ $result[$value['employee_id']]['rhndot'] 	+= $total_ndot; }
															$result[$value['employee_id']]['rhnd'] 	+= $total_nd;
															$value['checked'] = true;
														
														}else if($value['check_holiday'] == '(SH)'){

															if($val_ndot > 0){ $result[$value['employee_id']]['shnd'] 	+= $val_ndot; }
															else{ $result[$value['employee_id']]['shndot'] 	+= $total_ndot; }
															$result[$value['employee_id']]['shnd'] 	+= $total_nd;
															$value['checked'] = true;
														
														}
														$partial_process = true;
													}// NDOT



													if($value['new_color_overtime'] == 'green'){

														if( 
															(strtotime($time_sch[1]) > strtotime("00:00") && strtotime($time_sch[1]) < strtotime("03:00")) || 
															strtotime($time_sch[1]) > strtotime("23:59")
														){
															$rotval_end 		= $value['overtime_hours'];	

															if($value['check_holiday'] == "(RH)"){
																$partial_process 	= true;
																$result[$value['employee_id']]['rhot'] += $rotval_end;

															}else if($value['check_holiday'] == "(SH)"){
																$partial_process 	= true;
																$result[$value['employee_id']]['shot'] += $rotval_end;
															}else{
																$partial_process 	= true;
																$result[$value['employee_id']]['rots'] += $rotval_end;
																$result[$value['employee_id']]['rot'] = $result[$value['employee_id']]['rots'];
															}
														}else{

															$date_rot_in = date_create($value['date']." ".$time_sch[1]);
															$date_rot_out = date_create($value['nextday']." 00:00");
															$diff_rot 				= date_diff($date_rot_in,$date_rot_out);
															$rotval_start 	= (integer)$diff_rot->format("%H.%i");
															$rotval_end 		= $value['overtime_hours'] - $rotval_start;

															

															
															if($value['check_holiday'] == null){

																$result[$value['employee_id']]['rots'] = $value['overtime_hours'];
																$result[$value['employee_id']]['rot'] += $result[$value['employee_id']]['rots'];
																$partial_process 	= true;
															
															}else if($value['check_holiday'] == "(RH)"){

																$result[$value['employee_id']]['rhot'] += $value['overtime_hours'];
																$partial_process 	= true;

															}else if($value['check_holiday'] == "(SH)"){
															
																$result[$value['employee_id']]['shot'] += $value['overtime_hours'];
																$partial_process 	= true;
															}
														}// end condition timout origin > 00:00

													}

												}// MATCH
											}// end FOR
										}

										// if($value['employee_id'] == '2011008' && $value['date'] == '2019-12-08'){
										// 	return ["ketemu",$result[$value['employee_id']], $t,$arr['data']];
										// }



										// ND
										if($partial_process ==  false){										
											
											if($value['new_color_overtime'] == 'green'){

												if($value['check_holiday'] == '(RH)'){

													$result[$value['employee_id']]['rh']++;
													$result[$value['employee_id']]['rhot'] += $value['overtime_hours'];

												}else if($value['check_holiday'] == '(SH)'){

													$result[$value['employee_id']]['sh']++;
													$result[$value['employee_id']]['shot'] += $value['overtime_hours'];

												}else{
													
													$result[$value['employee_id']]['rots'] += $value['overtime_hours'];
													$result[$value['employee_id']]['rot'] = $result[$value['employee_id']]['rots'];
												}
											}

											if($total_nd > 0){ // waktu malam


												if($value['check_holiday'] == '(RH)'){

													$result[$value['employee_id']]['rh']++;
													if($val_ndot > 0){
														$result[$value['employee_id']]['rhnd'] += $total_nd;
													}else{
														$result[$value['employee_id']]['rhnd'] += $total_nd;
													}

												}else if($value['check_holiday'] == '(SH)'){

													$result[$value['employee_id']]['sh']++;
													if($val_ndot > 0){
														$result[$value['employee_id']]['shnd'] += $total_nd;
													}else{
														$result[$value['employee_id']]['shnd'] += $total_nd;
													}

												}else{
													if($val_ndot > 0){
														$result[$value['employee_id']]['ndot'] += $val_ndot;
														$result[$value['employee_id']]['nd'] += $total_nd;
													}else{
														$result[$value['employee_id']]['nd'] += $total_nd;
													}
												}

											}else if($val_ndot > 0){
												if($value['check_holiday'] == '(RH)'){

													$result[$value['employee_id']]['rh']++;
													if($val_ndot > 0){
														$result[$value['employee_id']]['rhndot'] += $val_ndot;
													}else{
														$result[$value['employee_id']]['rhnd'] += $total_nd;
													}

												}else if($value['check_holiday'] == '(SH)'){

													$result[$value['employee_id']]['sh']++;
													if($val_ndot > 0){
														$result[$value['employee_id']]['shndot'] += $val_ndot;
													}else{
														$result[$value['employee_id']]['shnd'] += $total_nd;
													}

												}else{
													if($val_ndot > 0){
														$result[$value['employee_id']]['ndot'] += $val_ndot;
														$result[$value['employee_id']]['nd'] += $total_nd;
													}else{
														$result[$value['employee_id']]['nd'] += $total_nd;
													}
												}											
											}
										}
									
									}
								}


								// this count SH RH
								if($count_rh){
									$result[$value['employee_id']]['rh']++;
								}else if($count_sh){
									$result[$value['employee_id']]['sh']++;
								}

								// if($value['employee_id'] == '2016018' && $value['date']=='2019-11-30'){
						  // 			return [300,$value['overtime_hours'],$result[$value['employee_id']],$partial_process

						  // 			];
						  // 		}





							}else{
								//return [$range[$y],$value['date'],$value['employee_id']];
								/*$iexp  =  explode('-',$value['date']);
								$iexp = $dictionary[intval($iexp[2])];
								$alpha_stat = $dictionary[$y];test

								$result[$value['employee_id']][$alpha_stat] = '-';
								$result[$value['employee_id']]["status".$alpha_stat] = "white";*/
							}// END IF RANGE__1
						} // LOOP_START__1
					}// END IF employee_id__1
				} // IF data_emp__1
			}// END LOOP__2
		}// END LOOP__3

		$final = [];
		$ix = 0;

		for($k = 0; $k < $count; $k++){
			foreach ($result as $key => $value) {
				if(isset($data_emp[$k])){
					if($data_emp[$k] ==  $key){
		  				$final[$ix] =  $value;
		  				$ix = $ix + 1;
					}
				}
			}
		}

		foreach ($range as $key => $value) {
			$fin[] = ["date" => $value];
		}

		$finally = array_merge_recursive([$final],["date" => $fin],["comment" => []]);

		$group = [];

		foreach($finally[0] as $key => $value){
			$depart = $value['department_name'];

			//$find = $this->findIndex($depart,$group,'department_name');

			// backup 2019-11-20
			if($value['working_day'] > 0){
				/*$got = ($value['working_day'] - $value['no_day_off'] - $value['leave_working']['total']);
				if($got <= 0){
					$value['got'] = 0;	
				}else{
					$value['got'] = $got*2;
				}*/
			}

			// if($value['working_day'] > 0){
			// 	$got = ($value['working_day']-$value['no_day_off']-$value['abs']);
			// 	$value['got'] = $got*2;
			// }			

			$group[] = [
						'employee_id'=> $value['employee_id'],
						'employee_name' => $value['employee_name'],
						'department_id' => $value['department_id'],
						'department_name' => $value['department_name'],
						'local_it' => $value['local_it'],
						'working_day' => $value['working_day'],
						'no_day_off' => $value['no_day_off'],
						'absences_of_days'=>$value['leave_working'],
						'disciplin'=>$value['disciplin'],
						'abs'=>$value['abs'],
						'leave_with_pay'=>$value['leave_pay'],
						'holiday_work'=>[
							'rh'=> $value['rh'],
							'sh'=> $value['sh']
						],
						'overtime_of_hours'=>[
							'got'=>/*$value['got']*/0,
							'rot'=>$value['rots'],
							//'rots'=>$value['rots'],
							'rhot'=>$value['rhot'],
							'shot'=>$value['shot'],
							'rotdo_1st'=>$value['rotdo_1st'],
							'rotdo_excess'=>$value['rotdo_excess'],
							'rhrdot_1st'=>$value['rhrdot_1st'],
							'rhrdot_excess'=>$value['rhrdot_excess'],
							'shrdot_1st'=>$value['shrdot_1st'],
							'shrdot_excess'=>$value['shrdot_excess']
						],
						'night_differential'=>[
							'nd'=>$value['nd'],
							'ndot'=>$value['ndot'],
							'rhnd'=>$value['rhnd'],
							'shnd'=>$value['shnd'],
							'rhndot'=>$value['rhndot'],
							'shndot'=>$value['shndot'],
							'rdnd'=>$value['rdnd'],
							'rdndot'=>$value['rdndot'],
							'rhrdnd'=>$value['rhrdnd'],
							'rhrdndot'=>$value['rhrdndot'],
							'shrdnd'=>$value['shrdnd'],
							'shrdndot'=>$value['shrdndot'],
						],
						'earlyout'=>$value['undertime'],
						'tardiness'=>[
							'late'=>$value['lates'],
							'undertime'=>$value['undertime'],
							'total'=>$value['tar_total']
						]
					];

		}

		return $group;
	}

	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
				return $att_record-> getMessageAccess("Unauthorized",500,[],$access[3]);
			}else{


				$counter = \DB::SELECT("select start_ , from_ from cutoff_date  ");
				$count =  count($counter);

				$q= "select att_cutoff_from,att_cutoff_to, payroll_period, payout_date from att_cutoff_period where att_cutoff_from != '0000-00-00'
										and att_cutoff_to != '0000-00-00' ";
				$data_ex = \DB::SELECT($q);

				$arr =[];
				$x = 0;
				if($data_ex != null){
					foreach ($data_ex as $key => $value) {
						if($count > 0){
							for($i = 0; $i  < $count; $i++){
								if($value->att_cutoff_from == $counter[$i]->start_ && $value->att_cutoff_to == $counter[$i]->from_){
									//unset($data_ex[$key]);
								}
								// else{
								// 	$date1 = explode("-",$value->att_cutoff_from);
								// 	$date2 = explode("-",$value->att_cutoff_to);
								// 	$arr[] = ["periode" => $date1[0].'/'.$date2[1].'/'.$date1[2].'-'.$date2[0].'/'.$date2[1].'/'.$date2[2]];
								// }
							}
						}
					}
				}

					$result = [];
					foreach ($data_ex as $key => $value) {
							$date1 = explode("-",$data_ex[$key]->att_cutoff_from);
							$date2 = explode("-",$data_ex[$key]->att_cutoff_to);

							// $period_payroll = explode('-',$data_ex[$key]->payroll_period);
							// $t = $period_payroll[0].'/'.$period_payroll[1].'/'.$period_payroll[2].'-'.$period_payroll[3].'/'.$period_payroll[4].'/'.$period_payroll[5];

							$attendance = $date1[0].'/'.$date1[1].'/'.$date1[2].'-'.$date2[0].'/'.$date2[1].'/'.$date2[2];

							//$result[] = ["periode" => $date1[0].'/'.$date1[1].'/'.$date1[2].'-'.$date2[0].'/'.$date2[1].'/'.$date2[2]];
							$result[] = ["periode" => $attendance, "payroll_period"=> $data_ex[$key]->payroll_period, "payout_date"=> $data_ex[$key]->payout_date];
							//$res1[] = [$t=>$date1[0].'/'.$date1[1].'/'.$date1[2].'-'.$date2[0].'/'.$date2[1].'/'.$date2[2]];
					}



				$department = \DB::SELECT("select * from department where id not in(0,1)");
				//$department_ex = array_merge($department,[["id" => 0, "name" => "All"]]);
				$job = \DB::SELECT("select id,title from job where id not in(0)");
				//$job_ex = array_merge($job,[["id" => 0, "title" => "All"]]);
				$data = ["department" => $department, "job" => $job, "period" => $result];
				return $att_record->indexAcess($data,$access[3]);
			}

		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

	}

	public function getName()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
				$att_record->getMessage("Unauthorized",500,[],$access[3]);
			}else{
				$input = \Input::all();
				$name = \Input::get('name');
				$rule = [
						'name' => 'required|Regex:/^[A-Za-z]+$/'
						];
				$validation = \Validator::make($input,$rule);
				$data = \DB::SELECT("CALL Search_emp_by_name('$name')");
				return $att_record->search($validation,$data,$access[3]);
			}

		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function cuttingOff()
	{


		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$key = \Input::get("key");
			$encode = base64_decode($key);
			$explode = explode("-",$encode);
			$token = $explode[1];

			$attendance =  \Input::get('form');

			$department  = (!isset($attendance['department']) ? 0  :   $attendance['department']);
			$payroll_period  = (!isset($attendance['payroll_period']) ? 0  :   $attendance['payroll_period']);
			$payout_date  = (!isset($attendance['payout_date']) ? 0  :   $attendance['payout_date']);
			$job  = (!isset($attendance['job']) ? 0  :   $attendance['job']);
			$local  = (!isset($attendance['local']) ? 0  :  $attendance['local']);
			$attendance  = (!isset($attendance['attendance']) ? 0  :  $attendance['attendance']);

			$short  =  \Input::get('short_person');
			if(isset($short)){
				$short =  json_encode(\Input::get('short_person'));
			}else{
				$short =  null;
			}

			$cutoff_per_payroll  =  \Input::get('cutoff_per_payroll');
			if(isset($cutoff_per_payroll)){
				$cutoff_per_payroll =  json_encode($cutoff_per_payroll, JSON_UNESCAPED_UNICODE);
			}else{
				$cutoff_per_payroll =  null;
			}


		 	$now  =   date('Y-m-d');
			$explode =  explode('-',$attendance);

			$explode1 = explode('/',$explode[0]);
			$explodev1 =  $explode1[0].'-'.$explode1[1].'-'.$explode1[2];
			$explode2 = explode('/',$explode[1]);
			$explodev2 =  $explode2[0].'-'.$explode2[1].'-'.$explode2[2];
			// if(strpos($cutoff_per_payroll,"2016039")){
			// 	return [$cutoff_per_payroll,12];
			// }
			$insert = \DB::SELECT("insert into cutoff_date(start_,from_,user,cutoff_at,department_id,job_title_id,cutoff_detail_json,local_it_user,cutoff_per_payroll, payroll_period, payout_date)
				values('$explodev1','$explodev2','$token','$now',$department,$job,'$short',$local,'$cutoff_per_payroll','$payroll_period','$payout_date')");
			$message = 'Success CutOff '; $status = 200; $data=[];
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// CUTOFF FOR STORE TAR, ABS, ND, NDOT, ROT, ROTDO
	public function cuttingOff2(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		return 1;
		if($access[1] == 200){
			$tmp_cal_cutoff = \Input::get('tmp_cal_cutoff');
			$attendance 	= \Input::get('attendance');
			$department_id 	= \Input::get('department');
			$explode  		= explode('-',$attendance);
			$start_date  	= $explode[0];
			$end_date  		= $explode[1];

			for ($i=0; $i < count($tmp_cal_cutoff); $i++) {
				try {

					$tar 	= $tmp_cal_cutoff[$i]['tar'];
					$abs 	= $tmp_cal_cutoff[$i]['abs'];
					$nd 	= $tmp_cal_cutoff[$i]['nd'];
					$ndot 	= $tmp_cal_cutoff[$i]['ndot'];
					$rot 	= $tmp_cal_cutoff[$i]['rot'];
					$rotdo 	= $tmp_cal_cutoff[$i]['rotdo'];

					$cal_emp = $tmp_cal_cutoff[$i]['employee_id'];
					$a = \DB::SELECT("UPDATE cutoff_date SET `tar`=$tar, `abs`=$abs, `rot`=$rot, `rotdo`=$rotdo, `nd`=$nd, `ndot`=$ndot where substr(cutoff_detail_json, (POSITION(':' IN cutoff_detail_json)+1),9) like '%$cal_emp%' and (start_ = '$start_date' and from_ = '$end_date') and department_id = $department");
					return \Response::json(['header'=>['message'=>'cut2','status'=>200],'data'=>null],200);
					//return [$a,"select * from cutoff_date where substr(cutoff_detail_json, (POSITION(':' IN cutoff_detail_json)+1),9) like '%$cal_emp%' and (start_ = '$start_date' and from_ = '$end_date')"];
				} catch (\Exception $e) {
					return \Response::json(['header'=>['message'=>'test','status'=>500],'data'=>$e],500);
				}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}


	public function search_payroll(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		$LIB_ATT = new calculation;
		if($access[1] == 200){
			$i   =  \Input::all();

			$cutoff_start 		= $i['cutoff_start'];
			$cutoff_to 			= $i['cutoff_to'];
			$query_department 	= \Request::get('department_id');
			$query_radio 		= /*\Request::get('local_it')*/1;
			$emp 			= $i['employee_cutoff'];

			$job = \DB::SELECT("select job from job_history where employee_id = '$emp'");
			if(count($job) > 0){
				$query_job = $job[0]->job;
			}

			$valid = \Validator::make([
					'cutoff_start' 		=> $cutoff_start,
					'cutoff_to' 		=> $cutoff_to,
					'department' 		=> $query_department,
					'local_it' 			=> $query_radio,
					'employee_cutoff' 	=> $emp
				],[
					'cutoff_start' 		=> 'date|required',
					'cutoff_to' 		=> 'date|required',
					'department' 		=> 'numeric|required',
					'local_it' 			=> 'numeric|required',
					'employee_cutoff' 	=> 'numeric|required'
				]);

			if($valid->fails()){
				$val = $valid->errors()->all();
				return response()->json(['header' => ['message' => $val[0], 'status' => 500 ], 'data' => null], 500 );
			}else{


				$query_radio  	 	=  " and  t2.local_it in ($query_radio)";
				//$query_job  		=  "and  t11.job = $query_job ";
				$query_job = "";
				$query_department = "";
				//$query_department 	=  " and t2.department = $query_department ";
				$query_emp   		=  [$emp,"t1.date >=  '$cutoff_start' and  t1.date <= '$cutoff_to'"];



				$data = $LIB_ATT->att('cut-off-payroll',$query_emp,$query_job,$query_department,$query_radio);
				return [$data];
			}

		$query_order   =  "and t1.status = 2 order by t1.date ASC";



		//$data = $LIB_ATT->att('cut-off-record',$query_emp,$query_job,$query_department,$query_radio);
		return [$data];
		$tmp2 = [];
		if(count($data['data'])){
			$cutting = \DB::SELECT("select cutoff_detail_json, department_id, job_title_id, local_it_user from cutoff_date where start_ = '$start_date' and from_ = '$end_date'");

			if(count($cutting) > 0){
				foreach ($data['data'] as $key => $value) {
					foreach ($cutting as $cutting_key => $cutting_value) {

						$json = json_decode($cutting_value->cutoff_detail_json,1);

						if($value['employee_id'] == $json['employee_id']){
							unset($data['data'][$key]);
						}
					}
				}

			}
		}

		if(count($data['data']) == 0){
			$data['data'] = [];
			$data['message'] = 'data not exist';
			$data['status'] = 500;
		}

		if(isset($data['status']) && $data['status'] == 200){
              $data = $data;
              return $data;
        }else{

			return \Response::json(['header'=>['message'=>$data['message'],'status'=>$data['status']],'data'=>$data['data']],$data['status']);
        }

        // ######################################## END ##############################


		//$query = \DB::SELECT($base_query);
		$training_temp = [];
		if($data['data'] !=  null){
			//$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
			return ['data'=>$data['data'], 'status'=>200, 'message'=>'View record data'];
		//if($query !=  null){

			foreach ($query as $key => $value) {
				$date_changeShift  =  '';
				if($value->date_changeShift  !=   '99:99:99'){
						$employee_q  =  $value->employee_id;
						$date_changeS =  $value->date_changeShift;
						$db = \DB::SELECT("select concat(substring(aws._from,1,5),'-',substring(aws._to,1,5)) as data from att_schedule_request as asr ,att_change_shift as acs, 	attendance_work_shifts as aws
						 	where asr.update_at = '$date_changeS'
							and asr.request_id = acs.id
						 	and acs.new_shift = aws.shift_id
						 	and asr.employee_id  = '$employee_q'
						 	and  asr.type_id = 5
						 	and asr.status_id =  2");

						if($db != null){
					   		$date_changeShift = $db[0]->data;
						}else{
							$date_changeShift = '99:99:99';
						}

				}else{
					$date_changeShift  =  '99:99:99';
				}

				$query[$key]->schedule_changeShift =   $date_changeShift;

				/** schedule  **/

				$schedule  = '';

				if($query[$key]->schedule_changeShift !=  '99:99:99'  and  $query[$key]->date_changeShift != '99:99:99' ){
					$schedule   =  $query[$key]->schedule_changeShift;
					$exp_sch = explode("-",$schedule);
					$tox 	= $exp_sch[1].":00";
					$fromx	= $exp_sch[0].":00";
				}
				else if($value->date_swapShift != '99:99:99' and  $value->schedule_swapShift  !=  '99:99:99' ){
					$schedule  =  $value->schedule_swapShift;
					$exp_sch = explode("-",$schedule);
					$tox 	= $exp_sch[1].":00";
					$fromx	= $exp_sch[0].":00";
				}else{
					$schedule  =  substr($value->fromx,0,5).'-'.substr($value->tox,0,5);
					if(isset($value->$schedule)){
						if($value->$schedule != "99:99:99" && $value->$schedule == $schedule){
							$exp_sch = explode("-",$schedule);
							$tox 	= $exp_sch[1].":00";
							$fromx	= $exp_sch[0].":00";
						}else{
							$exp_sch = explode("-",$value->$schedule);
							$tox 	= $exp_sch[1].":00";
							$fromx	= $exp_sch[0].":00";
						}
					}else{
						$exp_sch = explode("-",$schedule);
						$tox 	= $exp_sch[1].":00";
						$fromx	= $exp_sch[0].":00";
					}
				}

				$query[$key]->schedule =  $schedule;

				/*
				* TRAINING
				*/
				if($value->start_date_training != "99:99:99" || $value->shift_code == 'T'){
					$a = strtotime($query[$key]->date_swapShift);
					$allow = true;
					if($query[$key]->date_changeShift != '99:99:99'){
						$b = strtotime($query[$key]->date_changeShift);
						if($a < $b){ $allow = false; }
					}elseif($query[$key]->date_swapShift != '99:99:99'){
						$b = strtotime($query[$key]->date_swapShift);
						if($a < $b){ $allow = false; }
					}
					if($value->shift_code == 'T'){
						$training_temp[] = $value->date;
						$allow = false;
					}

					if($allow){
						$begin = new DateTime($value->start_date_training);
						$end = new DateTime($value->end_date_training);
						$end = $end->modify( '+1 day' );

						$interval = new DateInterval('P1D');
						$daterange = new DatePeriod($begin, $interval ,$end);

						foreach($daterange as $date){
							$training_temp[] = $date->format("Y-m-d");
						}
					}

				}

				if(count($training_temp) > 0){
					$a =  array_search($value->date, $training_temp);
					if(gettype($a) == 'integer'){
						$query[$key]->training_req =  $training_temp[$a];
					}else{
						$query[$key]->training_req = false;
					}
				}else{
					$query[$key]->training_req = false;
				}
				// get data and time
				$sch_date[$key] = [
					'date' => $value->date,
					'time' => [
						'in' => $fromx,
						'out'=> $tox,
						'timeIn'=>$value->timeIn,
						'timeOut'=>$value->timeOut
					]
				];
				// END TRAINING SECTION >>

				/** workhour  **/

				/*$workhours  = '';*/
				$workhours  = null;
				$timeIn =  $value->timeIn;
				$fromx  =   $value->fromx;
				// ADD NEW >>
				$total_menit_workhours = null;
				$short_fix = null;
				$late_fix=null;
				$earlyOut_fix=null;
				$eot_fix2=null;
				// $Eout = '';
				// OLD >>
				// $tox =  $value->tox;
				// $fromx  =  $value->fromx;

				if($value->timeIn != '-' and  $value->timeOut !=  '-' || $value->timeIn != null and  $value->timeOut !=  null){

					$timeIn  					.= ':00';
					$fromx2    					= $fromx;
					$timeOuts 					= $value->timeOut;
					$timeOuts 					.= ":00";
					$date_inquire_absen 		= $value->date_inquire_absen;
					$dates 						= $value->date;
					$dates_Biometric 			= $value->date_absen;
					$strtotime_TimeInInquire 	= strtotime("$value->date_inquire_absen $timeIn");
					$strtotime_TimeInBiometric 	= strtotime("$value->date_absen $timeIn");

					$strtotime_formx 			= strtotime("$value->date $fromx2");
					$strtotime_tox 				= strtotime("$value->date $tox");

					$strtotime_TimeOutBiometric = strtotime("$value->date_absen $timeOuts");
					$strtotime_TimeOutInquire 	= strtotime("$value->date_inquire_absen $timeOuts");

					// ######################################################################################
					$am_pm_form 				= date("a",$strtotime_formx);
					$am_pm_to 					= date("a",$strtotime_tox);

					if($value->date_inquire_absen){
						$am_pm_deviceFrom 				= date("a",$strtotime_TimeInInquire);
						$am_pm_deviceTo					= date("a",$strtotime_TimeOutBiometric);
					}else{
						$am_pm_deviceFrom 				= date("a",$strtotime_TimeInBiometric);
						$am_pm_deviceTo					= date("a",$strtotime_TimeOutBiometric);
					}

					$splitOut_time 		= explode(":", $timeOuts);
					$splitOut_time_h 	= (integer)$splitOut_time[0];
					$splitOut_time_m 	= (integer)$splitOut_time[1];

					// CARI WORKHOURS DEVICE

					if($am_pm_form == 'pm' && $am_pm_to == "am"){
						$strtotime_tox 	= strtotime('+1 day' ,strtotime("$value->date $tox"));
					}



					// NEW WITH FUNCTION

					$LIB_CAL_ATT = $LIB_ATT->calculation_attendance([
						'value'							=> $value,
						'fromx2'						=> $fromx2,
						'tox'							=> $tox,
						'strtotime_formx'				=> $strtotime_formx,
						'strtotime_tox'					=> $strtotime_tox,
						'timeIn' 						=> $timeIn,
						'timeOuts' 						=> $timeOuts,
						'strtotime_TimeInInquire' 		=> $strtotime_TimeInInquire,
						"strtotime_TimeInBiometric" 	=> $strtotime_TimeInBiometric,
						"strtotime_TimeOutInquire" 		=> $strtotime_TimeOutInquire,
						"strtotime_TimeOutBiometric" 	=> $strtotime_TimeOutBiometric,
						"late_fix" 						=> $late_fix,
						"eot_fix2" 						=> $eot_fix2
						//"total_device_workhours" 		=> $total_device_workhours
					]);

					$value 							= $LIB_CAL_ATT['value'];
					$fromx2 						= $LIB_CAL_ATT['fromx2'];
					$tox 							= $LIB_CAL_ATT['tox'];
					$strtotime_formx 				= $LIB_CAL_ATT['strtotime_formx'];
					$strtotime_tox 					= $LIB_CAL_ATT['strtotime_tox'];
					$strtotime_TimeInInquire 		= $LIB_CAL_ATT['strtotime_TimeInInquire'];
					$strtotime_TimeInBiometric 		= $LIB_CAL_ATT["strtotime_TimeInBiometric"];
					$strtotime_TimeOutInquire 		= $LIB_CAL_ATT["strtotime_TimeOutInquire"];
					$strtotime_TimeOutBiometric 	= $LIB_CAL_ATT["strtotime_TimeOutBiometric"];
					$late_fix 						= $LIB_CAL_ATT["late_fix"];
					$eot_fix2 						= $LIB_CAL_ATT["eot_fix2"];
					$total_device_workhours 		= $LIB_CAL_ATT["total_device_workhours"];


					$total_schedule_workhours 	= $strtotime_tox - $strtotime_formx;

					// schedule work
					$total_schedule_minutes 	= $total_schedule_workhours / 60;
					$split_schedule_hours 		= floor($total_schedule_minutes / 60);
					$split_schedule_minutes 	= ($total_schedule_minutes % 60);
					// device work
					$total_device_minutes 		= $total_device_workhours / 60;
					$split_device_minutes 		= ($total_device_minutes % 60);
					$split_device_hours 		= floor($total_device_minutes / 60);



					// OVERTIME
					$tmp_m = 0;$tmp_h = 0;
					if($split_schedule_hours < $split_device_hours){
						if($split_schedule_minutes < $split_device_minutes){
							$tmp_m = $split_device_minutes - $split_schedule_minutes;
						}else{
							$tmp_m = $split_schedule_minutes - $split_device_minutes;
						}
						$tmp_h = $split_device_hours - $split_schedule_hours;
						if($tmp_m < 10){ $tmp_m = "0$tmp_m"; }
						if($tmp_h < 10){ $tmp_h = "0$tmp_h"; }

						$query[$key]->total_overtime = "$tmp_h:$tmp_m";
						if("$tmp_h:$tmp_m" == "00:00"){
							$query[$key]->total_overtime = "-";
						}
					}else{
						if($split_schedule_minutes < $split_device_minutes){ $tmp_m = $split_device_minutes - $split_schedule_minutes; }
						else{ $tmp_m = $split_schedule_minutes - $split_device_minutes; }
						if($tmp_m < 10){ $tmp_m = "0$tmp_m"; }
						if($tmp_h < 10){ $tmp_h = "0$tmp_h"; }

						$query[$key]->total_overtime = "$tmp_h:$tmp_m";
						if("$tmp_h:$tmp_m" == "00:00"){
							$query[$key]->total_overtime = "-";
						}
					}

					$time_work	= null;
					if($split_device_hours < 10){ $time_work = "0$split_device_hours:"; }
					else{ $time_work = "$split_device_hours:"; }

					if($split_device_minutes < 10){ $time_work .= "0$split_device_minutes"; }
					else{ $time_work .= "$split_device_minutes"; }



					// CARI TIMEOUT DEVICE FIX INQUERY
					$timeOut_device_fix 	= strtotime("+".$split_schedule_hours." hour",$strtotime_TimeInBiometric);
					// CARI WAKTU PULANG SESUAI WORKHOURS SCHEDULE
					if($value->date_inquire_absen){




						// CARI LATE INQUERY
						if($strtotime_formx < $strtotime_TimeInInquire){
							/*$mktimes_late_fix 	= ($strtotime_TimeInInquire - $strtotime_formx)/60;
							$late_fix 			= floor($mktimes_late_fix);*/
							$mktimes_late_fix 	= mktime(0,($strtotime_TimeInInquire - $strtotime_formx)/60);
							$late_fix 			= date('H:i',$mktimes_late_fix);
						}else{ $late_fix 		= null; }

					}else{
						// CARI TIMEOUT DEVICE FIX


						// CARI LATE
						if($strtotime_formx < $strtotime_TimeInBiometric){
							$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
							$late_fix 		= date("H:i",$mktimes_late_fix);
						}else{ $late_fix 	= null; }

					}

					/*
					* CARI EARLYOUT
					* earlyout not working training and dayoff
					*/
					if($timeOut_device_fix > $strtotime_TimeOutBiometric){
						$mktimes_earlyOut_fix 	= mktime(0,($timeOut_device_fix - $strtotime_TimeOutBiometric)/60);
						$earlyOut_fix 			= date("H:i",$mktimes_earlyOut_fix);
					}else{ $earlyOut_fix 	= null; }


					/* CARI SHORT
					*  short not working dayoff and training
					*/
					if($total_device_workhours < $total_schedule_workhours){
						$mktimes_short_fix = mktime(0,($total_schedule_workhours - $total_device_workhours)/60);
						$short_fix = date('H:i',$mktimes_short_fix);
					}else{ $short_fix = null; }

					// OVERTIME FIX
					if($total_schedule_workhours < $total_device_workhours){
						$mktimes_overtime_fix = mktime(0,($total_device_workhours - $total_schedule_workhours)/60);
						$total_overtime_fix = date("H:i",$mktimes_overtime_fix);
					}else{ $total_overtime_fix = null; }
					$query[$key]->total_overtime = $total_overtime_fix;


					// #######################################################################################


					$exp_strtotime_TimeInInquire 	= explode(":", $strtotime_TimeInInquire);
					$exp_strtotime_TimeInBiometric 	= explode(":", $strtotime_TimeInBiometric);
					$exp_strtotime_formx 			= explode(":", $strtotime_formx);
					$exp_strtotime_TimeOutBiometric = explode(":", $strtotime_TimeOutBiometric);
					$exp_timein 					= explode(":",$timeIn);
					$late_time 						= null;

					$mktimes_workhours_device 	= mktime(0,($total_device_workhours/60));

					$workhours 					= /*date('H:i',$mktimes_workhours_device)*/ $time_work;



						if(strtotime($workhours) > strtotime(date('H:i',(strtotime($tox) -  strtotime($fromx)))) )
						{ $Eout =  0; }
						else
						{
							$timeOut  = $value->timeOut;
							$timeOut  = $timeOut.':00';

							$time1 =  substr(($query[$key]->schedule),6,5).':00';

							if(strtotime($time1) < strtotime($value->timeOut)){
								$Eout =  0;
							}else{
								$Eout = date('H:i', strtotime($time1) - strtotime($value->timeOut));
							}

						}

				}else{
					$Eout = '-';
					$workhours  =  null;
				}



				$query[$key]->WorkHours =  $workhours;
				$query[$key]->WorkHours =  $workhours;
				if($query[$key]->shift_code == 'DO'){
					$query[$key]->OvertimeRestDay = $workhours;
				}

				 //short




				/*$query[$key]->Short  =  $shortx;*/
				$query[$key]->Short  =  $short_fix;

				//schedule mod
				// $query[$key]->schedule_mod =  concat((substring((select schedule),1,5)),':00');
				$query[$key]->schedule_mod = substr($query[$key]->schedule,0,5).':00';

				//late;

				// $late  =  0;
				if(strtotime($query[$key]->schedule_mod) < strtotime($value->timeIn) and $value->timeOut != null)
					{ $late = date('H:i',(strtotime($value->timeIn) -  strtotime($query[$key]->schedule_mod))); }
				else
					{ $late  = 0;  }

				/*$query[$key]->Late  =  $late;*/
				$query[$key]->Late = $late_fix;

				//earlyOut
				$query[$key]->EarlyOut = $Eout;

			}
		}

	   $schedule=null;
		$max_count  =  count($query);
		if($query){
			$count  =   count($query) - 1;
			$c = count($query);
			$mod = $c % 10;
			$count = $c - $mod;
			// if($mod <= 10){
			// 	return $data = ['data'=>$query, 'status'=>500, 'message'=>'Record data is less than 1 month'];
			// }
			$j = 0 ;
			$last = $c - 20;

			if(isset($query[$j]->fromx) && isset($query[$j]->tox)){
				$h_formx = substr($query[$j]->fromx, 0,5);
				$h_tox = substr($query[$j]->tox, 0,5);
			}

			for ($i = 0; $i < $c; ) { // $c = 57 / $i until 56
				// $dt = substr($dt, 8);
				// first period

				if ($i <= 9){
					// if(!isset($query[$j+8]->date)){
					// 	return $query;
					// }


					$between = $query[$j]->date.' <=> '.$query[$max_count-1]->date;
					$between = $between;
				}
				// second period
				else if ($i >= 10 && $i <= 19){

					$between2 = $query[$j+(10-$j)]->date.' <=> '.$query[$max_count-1]->date;

					$between = $between2;
				}
				// third period
				else{
					$date = date("d");

					$between3 = $query[$j+(20-$j)]->date.' <=> '.$query[$max_count-1]->date;
					$between = $between3;
				}


				if ($i >= $count-1){ // if $i = 50 then $i - $c-1
					$between = $query[$i]->date.'<==>'.$query[$c-1]->date;
					$a = $mod;
				}
				else{
					$between = $query[$i]->date.'<==>'.$query[$max_count-1]->date;
					$a = $count;
				}
				for ($j=1; $j <= $a; $j++) {
					$schedule = $query[$i]->schedule;

					$name = $query[$i]->employee_id;
					// create a mark, yes for workHour < hour_per_day, and no for other
					if ($query[$i]->WorkHours != null){
						// if($query[]->date  == '2017-09-02'){
						// 	return $arr  = [$query[$i]->WorkHours,$query[$i]->hour_per_day];
						// }
						if ($query[$i]->WorkHours < $query[$i]->hour_per_day) {

							$emp_over  =  $query[$i]->employee_id;
							$date_str =  $query[$i]->date;


							$check_overtime = \DB::SELECT("select * from att_overtime as ao,att_schedule_request as asr where ao.employee_id =  '$emp_over' and ao.date_str  = '$date_str' and ao.id  = asr.request_id and asr.status_id  = 2  ");


							if($check_overtime != null){
								$workStatus = "no";
							}else{
								$workStatus = "yes";
							}
						}else{
							$emp_over  =  $query[$i]->employee_id;
							$date_str =  $query[$i]->date;
							$check_overtime = \DB::SELECT("select * from att_overtime as ao,att_schedule_request as asr where ao.employee_id =  '$emp_over' and ao.date_str  = '$date_str' and ao.id  = asr.request_id and asr.status_id  = 2  ");


							if($check_overtime != null){
								$workStatus = "no";
							}else{
								$workStatus = "yes";
							}
						}
					}
					else{
						$workStatus = "no";
					}
					/*
					//	Change the view to DO if schedule is 00:00-00:00
					//	Change the view to "-" if schedule is null
					*/

					if ($query[$i]->schedule == "00:00-00:00") {
						$date_modify = $query[$i]->date;
						$emp_modify = $query[$i]->employee_id;

						$give =\DB::SELECT("select aws.shift_code from att_schedule ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify'
							               and ash.date = '$date_modify' and  ash.shift_id = aws.shift_id  ");
						//$check_workshift = \DB::SELECT("select * from att_schedule where ")

						if($query[$i]->date_changeShift !=  '99:99:99' and $query[$i]->status ==  2){

							$give   = \DB::SELECT("select aws.shift_code from att_change_shift ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify'
							               and ash.date = '$date_modify' and  ash.new_shift = aws.shift_id ");
							$schedule = $give[0]->shift_code;
						}else{
							$give   = \DB::SELECT("select aws.shift_code from att_schedule ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify'
							               and ash.date = '$date_modify' and  ash.shift_id = aws.shift_id ");
							if(count($give) > 0){
								$schedule = $give[0]->shift_code;
							}
						}

						if ($query[$i]->timeIn == null AND $query[$i]->timeOut == null) {
							$timeIn = "-";
							$timeOut = "-";
							$noTimeInStatus = "yes";
							$noTimeInStatus = "yes";
						}
						else if ($query[$i]->timeIn == null AND $query[$i]->timeOut != null) {
							$timeIn = "-";
							$timeOut = $query[$i]->timeOut;
							$noTimeInStatus = "yes";
							$noTimeInStatus = "no";
						}
						else if ($query[$i]->timeIn != null AND $query[$i]->timeOut != null) {
							$timeIn = $query[$i]->timeIn;
							$timeOut = $query[$i]->timeOut;
							$noTimeInStatus = "no";
							$noTimeInStatus = "no";
						}
						else {
							$timeIn = $query[$i]->timeIn;
							$timeOut = "-";
							$noTimeInStatus = "no";
							$noTimeInStatus = "yes";
						}
					}
					else {
						$schedule = $query[$i]->schedule;
						if ($query[$i]->timeIn == null) {
							$timeIn = "-";
							$noTimeInStatus = "yes";
						}
						else {
							$timeIn = $query[$i]->timeIn;
							$noTimeInStatus = "no";
						}

						if ($query[$i]->timeOut == null) {
							$timeOut = "-";
							$noTimeOutStatus = "yes";
						}
						else {
							$timeOut = $query[$i]->timeOut;
							$noTimeOutStatus = "no";
						}

					}



					if ($query[$i]->total_overtime == null) {
						$timeIns = strtotime($query[$i]->timeOut);
						$explode = explode('-',$query[$i]->schedule);
						$arg2    = strtotime($explode[1]);
						if($timeIns > $arg2){
							if($schedule == 'DO'){
								$totalOver = '-';

								//ini masalahnya
								$mass = "yess";
							}else{
								if($query[$i]->timeOut == "00:00"){
									$query[$i]->timeOut = "24:00";
								}
								$time1x  =   explode(':',$query[$i]->timeOut);
								$time2x =  explode(':',$explode[1]);

								$time1xr  =   ($time1x[0] * 60) + $time1x[1];
								$time2xr  =   ($time2x[0] * 60) + $time2x[1];

								if(strlen(($time1xr - $time2xr) % 60) ==  1){
									$m	 = '0'.(($time1xr - $time2xr) % 60);
								}else{
									$m   = (($time1xr - $time2xr) % 60);
								}
								/*$totalOver =  floor(($time1xr - $time2xr) / 60).' : '.$m;*/
								$totalOver =  /*floor(($time1xr - $time2xr) / 60).' : '.$m*/ $query[$i]->total_overtime;

							}

						$makeOver =  'yes';
						}else{
							$makeOver = 'no';
							$totalOver = "-";
						}
					}else {

						$floor   = floor($query[$i]->total_overtime);
						$decimal = $query[$i]->total_overtime - $floor;
						if($decimal != 0){
							$decimal = (60 * (10 * $decimal))/10;
						}else{
							$decimal = "00";
						}
						$ax = strlen($floor);

						$ax = ($ax == 1 ? '0'.$floor : $floor);
						/*$totalOver = $floor.':'.$decimal;*/
						$totalOver = /*$floor.':'.$decimal*/ $query[$i]->total_overtime;
						$makeOver  ='no';
					}


					if ($query[$i]->WorkHours == '00:00:00' || $query[$i]->WorkHours == null){
						 $workHour = "-"; $totalOver = "-";

					}
					else{

						if(strchr($query[$i]->WorkHours,'-')){

							$exp_schedule = explode('-', $query[$i]->schedule);
							$ts1 = strtotime($exp_schedule[0]);
							$ts2 = strtotime($exp_schedule[1]);
							$diff = abs($ts1 - $ts2) / 3600;
							if(strpos($diff,'.')){
								$e_menit = explode('.', "$diff");
								$tmp = "0.".$e_menit[1]; // build menit
								$b_menit = round(floatval($tmp) * 60 );
								if(strlen("$b_menit") == 2){
									$workHour =  $e_menit[0].":$b_menit";
								}else{
									$workHour =  $e_menit[0].":0$b_menit";
								}
							}else{
								$t = $diff/60;
								if(strchr("$t",".")){
									$diff = $diff/2;
								}
								$workHour = round($diff).":00";
							}
						}else{
							 $workHour = $query[$i]->WorkHours;
							 $explode = explode(":",$workHour);
							 $hi = $explode[0];
							 if(substr($hi,0,1) == 0){
							 	$workHour =  substr($hi,1,1).':'.$explode[1];
							 }else{
							 	$workHour =  $hi.':'.$explode[1];
							 }
						}
					}



					if($totalOver != "-" and $schedule == 'yes'){
						$timeIns = strtotime($query[$i]->timeOut);
						$explode = explode('-',$query[$i]->schedule);
						$arg2    = strtotime($explode[1]);
						$overRest = date('H:i',($timeIns - $arg2));
					}else if($query[$i]->timeIn and $query[$i]->timeOut){


						//$overRest =
					}else{
						$overRest = "-";
					}

					// if($totalOver ==  "-"&& $overRest !="-"){
					// 	$totalOver = "-";
					// }

					if ($query[$i]->OvertimeRestDay == NULL){
						if(isset($mass) && $mass == "yess"){
							if($schedule == "DO"){
								$timeInss = strtotime($query[$i]->timeOut);
								$timeOuts = strtotime($query[$i]->timeIn);
								$overRest = date('H:i',($timeInss - $timeOuts));

								if($timeIns == false && $timeOuts == false){
									$overRest = "-";
								}else{
									$workHour = $overRest;
								}
							}
						}else{
							$overRest = "-";
						}

					}else{
						$overRest = $query[$i]->OvertimeRestDay;
						$explode =  explode('.',$query[$i]->OvertimeRestDay);
						$floor =  floor($overRest);
						$check_floor = $overRest -  $floor;
						if($check_floor != 0){
							$overRest_decimal = (60 * (10 * $decimal))/10;
							$overRest = $explode[0].':'.$overRest_decimal;
						}else{
							$overRest = ((int)$explode[0]+1).':'.'00';
						}
					}


					if ($query[$i]->Short == null) {$short = "-";}
					else{
						$time1 = intval(substr($query[$i]->Short,0,2));
						$time2 = intval(substr($query[$i]->Short,3,2));
						$short = ($time1 * 60) + $time2;
						if($short == 0){
							$short = '-';
						}else{
							$short =  $short;
						}

					}


					if ($query[$i]->ApproveStatus == null || $query[$i]->ApproveStatus == 0 ){ $status = "no"; }
					elseif ($query[$i]->ApproveStatus == 1) {$status = "yes";}
					else{ $status = "no";}



					if ($query[$i]->Late == null || $query[$i]->Late == "0"){
						//if($query[$i]->date == "2018-05-01"){ return (array)($query[$i]->Late == 0); }
						$late = "-";
					}else{

						$date_late 	= $query[$i]->date;
						//$chkLate  	= \DB::SELECT("select * from att_schedule_request where employee_id  = '$name' and availment_date = '$date_late' and type_id  = 7 and status_id  = 2 order by id desc  limit 1");
						$chkLate 	= \DB::SELECT("select asr.date_request, asr.availment_date, asr.approver, al.late from att_schedule_request as asr, att_late as al where asr.employee_id  = '$name' and asr.availment_date = '$date_late' and asr.type_id  = 7 and asr.status_id  = 2 and al.late_id = asr.request_id order by asr.id desc  limit 1");
						if($chkLate ==  null){
							$colorLate  = 'red';
						}elseif(isset($chkLate[0])){

							if($chkLate[0]->approver ==  null){
								$colorLate  = 'orange';
							}else{

								$split_time_sch  	= explode(":",$query[$i]->fromx);
								$split_late			= explode(":",$chkLate[0]->late);

								$split_late_h = (integer)$split_late[0];
								$split_late_m = (integer)$split_late[1];
								$split_time_sch_h = (integer)$split_time_sch[0];
								$split_time_sch_m = (integer)$split_time_sch[1];

								if($split_late_h == 0){
									$tmp = $split_time_sch_m + $split_late_m;
									if($tmp > 60){
										$split_time_sch_h = $split_late_h + 1;
										$split_time_sch_m = $tmp-60;
									}else{
										$split_time_sch_m = $tmp;
									}
								}else{
									$tmp_h = $split_time_sch_h + $split_late_h;
									if($tmp_h >= 24){
										$split_time_sch_h = $tmp_h - 24;
									}else{
										$split_time_sch_h = $tmp_h;
									}

									$tmp_m = $split_time_sch_m + $split_late_m;
									if($tmp_m > 60){
										$split_time_sch_h = $split_late_h + 1;
										$split_time_sch_m = $tmp_m-60;
									}else{
										$split_time_sch_m = $tmp_m;
									}
								}

								if($split_time_sch_h < 10){
									$build = "0$split_time_sch_h:$split_time_sch_m:00";
								}else{
									$build = "$split_time_sch_h:$split_time_sch_m:00";
								}
								$date_com 		= $query[$i]->date;
								$time_device 	= $query[$i]->timeIn;

								$com_late = strtotime("$date_com $build");
								$com_device = strtotime("$date_com $time_device:00");

								if($com_device <= $com_late){
									$colorLate  	= 'green';
								}else{
									$colorLate  	= 'orange';
								}

							}
						}else{
							$colorLate  = 'green';
						}
							//if($value->date == "2018-05-01"){ return $query[$i]->Late; }
						 	$late = $query[$i]->Late;
						//}
					}

					if ($query[$i]->EarlyOut == null){ $earlyOut = "-";}
					else{



						$explode = explode(":",$query[$i]->EarlyOut);
						 	$earlyOut = strlen($explode[0]);
						 	if($earlyOut == 2 and $earlyOut != '00'){
						 		$ho = (int)$explode[0];
						 		$mi = (int)$explode[1];
						 		$ho = $ho * 60;
						 		$earlyOut = $ho + $mi ;
						 	}else if($earlyOut == 2 and $earlyOut == '00'){
						 		$mi = (int)$explode[1];
						 		$earlyOut =   $mi;
						 	}else{
						 		$earlyOut = "-";
						 	}
						if($earlyOut != '-'){
							//return var_dump($query[$i]);
						}
					}
					$h = substr($schedule, 6, strpos($schedule,":"));
					$m = substr($schedule, 9, strpos($schedule,10));
					//print_r($h."\n");
					$h1 = substr($timeOut, 0, strpos($timeOut,":"));
					$m1 = substr($timeOut, 3, strpos($timeOut,4));
					// create mark for the record that has been changed, yes for changed record
					if ($totalOver != null){
						if($makeOver == 'yes'){
							$overStatus = ( $overRest == "-" ?  "yes" : "no");
							if(isset($mass) && $mass == "yess"){
								$overStatus = "yes";
							}
							// $overStatus = "yes";
						}elseif($makeOver == "no") {
							$overStatus = "no";
						}
					}
					else
						$overStatus = "no";
						if($overRest !=  null && isset($query[$i]->RestDay_App)){
							if($query[$i]->RestDay_App == 2){
								$OTR_color =  'yes';
							}else{
								$OTR_color =  'no';
							}

						}else{
							$OTR_color = "-";
						}


					// Compare time in and time out between attendance record and request
					$emp = $query[$i]->employee_id;
					$dt = $query[$i]->date;
					$_timeIN = $query[$i]->timeIn.':00';
					$_timeOUT = $query[$i]->timeOut.':00';

					// create mark for the record that has been changed, yes for changed record
					$tm = \DB::select("CALL View_TimeInOut_byDate_employee('$emp', '$dt')");
					$test[] = $dt;
					if ($tm != null) {
						if ($tm[0]->req_in != $_timeIN)
							$timeInStatus = "yes";
						else
							$timeInStatus = "no";

						if ($tm[0]->req_out != $_timeOUT)
							$timeOutStatus = "yes";
						else
							$timeOutStatus = "no";
					}
					else {
						$timeInStatus = "no";
						$timeOutStatus = "no";
					}
					if ($i == $c) {

						$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
						return $data;
					}else{
						if($totalOver != '-'){

							if($workHour != "0:00"){
								// if($query[$i]->date  ==   '2017-08-22'){
								// 		return $totalOver;
								// }

								$totalOvers =  $totalOver;
							}else{

								$totalOvers = '-';
							}

							// if($totalOvers ==  '00:00' || $workHour == "0:00"){
							// 	$totalOvers = '-';
							// }
						}else{
							$totalOvers = '-';
						}


						if($overRest != "-"){
							$short = '-';
							$late = '-';
							$earlyOut = '-';
						}

						if(strlen($schedule) ==  2){
							$short = '-';
							$late = '-';
							$earlyOut = '-';
							$totalOvers = '-';
							$overRest = '-';

						}

						$empx = $query[$i]->employee_id;
						$date_request = $query[$i]->date;
						$latest_modified  =  \DB::SELECT("select from_,to_,leave_code from leave_request,leave_type where employee_id  = '$empx' and leave_request.leave_type = leave_type.id and leave_code != '' and leave_request.status_id = 2");
						$latest_request  =  \DB::SELECT("select * from att_schedule_request as asr, att_training as atr where asr.status_id = 2 and asr.employee_id  = '$empx' and asr.request_id = atr.id and '$date_request' >= atr.start_ and '$date_request' <= atr.end and asr.type_id = 1");
						$latest_request1  =  \DB::SELECT("select * from att_schedule_request as asr, att_training  as atr where asr.status_id = 2 and asr.employee_id  = '$empx' and asr.type_id = 2 and asr.request_id  = atr.id and '$date_request' >= atr.start_ and '$date_request' <= atr.end limit 1");
						$terminated = \DB::SELECT("select * from emp_termination_reason where employee_id  = '$empx' ");
						//if($latest_request != null){
						//$continue_training  = \Db::SELECT("select * from ")

							if(isset($latest_request[0]->type_id)  ){
								$schedule =   'T';
							}
							if(isset($latest_request1[0]->type_id)  ){
								$schedule =   'OB';
							}
						//}
						if($latest_modified != null){
							foreach ($latest_modified as $key => $value) {
								if($value->from_ <= $query[$i]->date && $query[$i]->date <= $value->to_){
									$schedule =   $value->leave_code;
								}
							}
						}


						$holiday = \DB::SELECT("select * from attendance_holiday");
							foreach ($holiday as $key => $value) {
								$holiday_date  =  $query[$i]->date;
								if($holiday != null){
									if($holiday[$key]->repeat_annually == 1){
										if($holiday[$key]->day == null && $holiday[$key]->week == null){
											if($holiday[$key]->date == $holiday_date ){
												$schedule = 'H';
											}
										}
										if($holiday[$key]->day != null && $holiday[$key]->week != null){

											$holiday[$key];
											$year =  date('Y');
											$ass_hole =  explode('-',$holiday[$key]->date);
											$month_name  =  date("F", mktime(0, 0, 0,(intval($ass_hole[1])), 10));
											$ass_day =  $holiday[$key]->day;

											if($holiday[$key]->week == 'first'){
												$week_day =  '+1';
											}elseif($holiday[$key]->week == 'second'){
												$week_day =  '+2';
											}elseif ( $holiday[$key]->week == 'third'){
												$week_day = '+3';
											}elseif($holiday[$key]->week == 'fourth'){
												$week_day = '+4';
											}else{
											  $week_day = '+4';
											}
											$get_new_ass_hole  = date('Y-m-d',strtotime("$week_day week $ass_day $month_name $year"));

											if($holiday_date == $get_new_ass_hole){
										 		$schedule = 'H';

										 	}
										}
									}else{
										if($holiday[$key]->date == $query[$i]->date){
											$schedule = 'H';
										}
									}
								}
							}


						if($terminated != null){
							$schedule = 'TER';
						}
						if(!isset($colorLate)){
							$colorLate = 'red';
						}

						if($schedule  == 'DO'){
							$ti =  strtotime($timeIn.':00');
							$to =  strtotime($timeOut.':00');

							$overRest  = date('H:i',$to - $ti);
							if($overRest == '00:00'){
								$overRest  =   '-';
							}
						}else{
							$overRest = '-';
						}

						if($workStatus ==  'no'and  $schedule == 'DO'){
							$OTR_color = 'yes';
						}else{
							$OTR_color = 'no';
						}
						$date_c = $query[$i]->date;
						$emp_c  = $query[$i]->employee_id;

						// ORIGINAL


						if($earlyOut  != null){
							$emp_c  = $query[$i]->employee_id;
							$date_c = $query[$i]->date;
							// print_r("select * from  att_late, att_schedule_request where  att_late.type_id = 7 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id and att_schedule_request.approver = 2");
							$early_c =  \DB::SELECT("select att_schedule_request.status_id from  att_late, att_schedule_request where  att_late.type_id = 8 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id");

							if($early_c != null){
								$query[$i]->early_c =  'green';
							}else{
								$query[$i]->early_c =  'red';
							}
						}else{
							$query[$i]->early_c =  'red';
						}

						if(strlen($schedule) >  4){
							$turns =  explode('-',$schedule);
							if(!isset($turns[1])){
							  $workStatus  =  "no";
							}else{
							$base_date1 =  $turns[0].':00';
							$base_date2 =  $turns[1].':00';

							$substract_1 =  $timeIn.':00';
							$substract_2 =  $timeOut.':00';

						 	$get_tot_date1   = strtotime($base_date2) -  strtotime($base_date1);
							$get_tot_date2   = strtotime($substract_2) - strtotime($substract_1);

								if($get_tot_date1 <= $get_tot_date2  || $get_tot_date1 == $get_tot_date2){
									$workStatus  =  "no";
								}
							}
						}

						//check color  time  in   out
					 	// par timeInStatusC  and timeOutStatusC

						//if($query[$i]->date  ===  '2017-09-10'){
							if($timeIn   ==   '-'){
								$date_timeIn   = $query[$i]->date;
								$date_timeIn_employee  =  $query[$i]->employee_id;

								$check_time_orange  = \DB::SELECT("select t2.status_id, t1.req_in from  att_time_in_out_req as t1,att_schedule_request
																	as t2,pool_request as t3
																	where t1.date  =   '$date_timeIn'
																	and   t1.req_in_out_id =  t2.request_id and  t1.type_id = t2.type_id
																	and   t2.id = t3.id_req  and  t2.type_id   =  t3.type_id and t3.master_type = 1
																	and  t1.employee_id = '$date_timeIn_employee' order  by  t1.req_in_out_id DESC limit 1 ");

								// OLD

								if($check_time_orange  != null){
									///return  $check_time_orange;
									if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  ==  1){

											$timeInStatusC = 'orange';
											$timeIn  =   substr($check_time_orange[0]->req_in,0,5);
									}else{
										$timeInStatusC = 'green';
									}
								}else{
									$timeInStatusC = 'red';
								}
							}else{ $timeInStatusC = 'green'; }
						//}

						if($timeOut   ==   '-'){
							$date_timeOut   = $query[$i]->date;
							$date_timeOut_employee  =  $query[$i]->employee_id;
							$check_time_orange  = \DB::SELECT("select t2.status_id,t1.req_out from  att_time_in_out_req as t1,att_schedule_request
																as t2,pool_request as t3
																where t1.date  =   '$date_timeOut'
																and   t1.req_in_out_id =  t2.request_id and  t1.type_id = t2.type_id
																and   t2.id = t3.id_req  and  t2.type_id   =  t3.type_id and t3.master_type = 1
																and  t1.employee_id = '$date_timeOut_employee' order  by  t1.req_in_out_id DESC limit 1 ");

							//OLD

							if($check_time_orange  != null){
								if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  == 1){
										$timeOutStatusC = 'orange';
										$timeOut  =   substr($check_time_orange[0]->req_out,0,5);
								}else{
									$timeOutStatusC = 'green';
								}
							}else{
								$timeOutStatusC = 'red';
							}
						}else{ $timeOutStatusC = 'green'; }

						//check_color_orange

						//set color to  oramg  for  overtime and overtime dso
						//if  already input for  overtime  rest day parameter $OTR_color
						//If  already  input for  overtime   no  do    parameter $overtime_non_do


						//return $totalOvers;
						if($totalOvers != null){
							$date_overtime  =  $query[$i]->date;
							$employee_overtime = $query[$i]->employee_id ;

							$search  = \DB::SELECT("select * from att_overtime as t1, att_schedule_request as t2
													where t1.employee_id  =  '$employee_overtime'
													and  t1.date_str =  '$date_overtime'
													and  t1.type_id =  t2.type_id
													and  t1.id = t2.request_id
													order by t1.id desc limit 1
													");

							if($search  != null){
								if($search[0]->status_id == 1){
									$overtime_non_do  =  'orange';
								}else{
									$overtime_non_do  =  'green';
								}


							}else{
									 $overtime_non_do =   'red';
							}


						}else{
							$overtime_non_do = 'red';
						}

						if($overRest != null){
							$date_overtime  =  $query[$i]->date;
							$employee_overtime = $query[$i]->employee_id ;

							$search  = \DB::SELECT("select * from att_overtime as t1, att_schedule_request as t2
													where t1.employee_id  =  '$employee_overtime'
													and  t1.date_str =  '$date_overtime'
													and  t1.type_id =  t2.type_id
													and  t1.id = t2.request_id
													order by t1.id desc limit 1
													");

							if($search  != null){
								if($search[0]->status_id == 1){
									$OTR_color  =  'orange';
								}else{
									$OTR_color  =  'green';
								}


							}else{
									$OTR_color =   'red';
							}

						}else{
							$OTR_color  = 'red';
						}

						//check  changeshift
						if(isset($query[$i]->date) ){

							$date_changeshift  =  $query[$i]->date;
							$employee_changeshift = $query[$i]->employee_id ;


							$check_exist_change_shift   =  \DB::SELECT("select * from  att_change_shift as t1 ,  att_schedule_request  as t2,
																		attendance_work_shifts as  t3
																	    where t1.employee_id = '$employee_changeshift' and  date = '$date_changeshift'
																	    and  t1.type_id  =  t2.type_id
																	    and  t1.id  = t2.request_id
																	    and  t1.new_shift  =  t3.shift_id
																	    and t2.status_id  =  2 order  by  t1.id  desc limit 1");
							if($check_exist_change_shift != null){
								$list_code = ['DO','ADO','VL','BL','TB','SL','EL','BE','ML','PL','MA','OB',];



								if(in_array($check_exist_change_shift[0]->shift_code,$list_code)){
									$schedule =  $check_exist_change_shift[0]->shift_code;
									if($query[$i]->date == "2017-09-09" && $query[$i]->employee_id == "2015037"){
										return [$schedule,(array)$query[$i],"TEST1"];
									}
								}else{
									$schedule  =  substr($check_exist_change_shift[0]->_from,0,5).'-'. substr($check_exist_change_shift[0]->_to,0,5);
									if($query[$i]->date == "2017-09-09" && $query[$i]->employee_id == "2015037"){
										return [$schedule,(array)$query[$i],"TEST2"];
									}
								}
							}

						}

						$attendance =  \Input::get('attendance');
						$explode =  explode('-',$attendance);

						$explode1 = explode('/',$explode[0]);


						$exp_sch  =  explode('-', $schedule);


						try{
							if($query[$i]->training_req){
								$schedule = 'T';
								$late = '-';
							}

							if($query[$i]->OvertimeRestDay != '99:99:99'){
								$overRest = $query[$i]->OvertimeRestDay;
							}

						}catch(\Exception $e){
							return [$e,$query[$i]];
						}



						if($pointer == 0){


								$temp[] = [
									'early_c' => $query[$i]->early_c,
									'date' => $query[$i]->date,
									'colorLate' => $colorLate,
									'employee_id' => $query[$i]->employee_id,
									'Name' => $query[$i]->Name,
									'schedule' => $schedule,
									'timeIn' => $timeIn,
									'timeOut' => $timeOut,
									'noTimeIn' => $noTimeInStatus,
									'noTimeOut' => "yes",//$noTimeOutStatus,
									'WorkHours' => $workHour,
									'workStatus' => $workStatus,
									'total_overtime' => $totalOvers,
									'new_color_overtime' => $overtime_non_do,
									'overStatus' => $overStatus,
									'status' => $status,
									'OvertimeRestDay' => $overRest,
									'OvertimeRestDay_Color' => $OTR_color,
									'timeOutStatus' => $timeOutStatus,
									'timeInStatus' => $timeInStatus,
									'C_TimeIn' => $timeInStatusC,
									'C_TimeOut' => $timeOutStatusC,
									'Short' => $short,
									'Late' => /*$query[$i]->Late*/ $late,
									'EarlyOut' =>  ($earlyOut == 0 ? $earlyOut = '-' : $earlyOut = $earlyOut  ),
									'between' => $between
								];
							//}
						}else{

							//$get_date_basic  =   explode('-',$query[$i]->date);

							//if($pointer  ==  $get_date_basic[2] ){
								$temp[] = [
									'early_c' => $query[$i]->early_c,
									'date' => $query[$i]->date,
									'colorLate' => $colorLate,
									'employee_id' => $query[$i]->employee_id,
									'Name' => $query[$i]->Name,
									'schedule' => $schedule,
									'timeIn' => $timeIn,
									'timeOut' => $timeOut,
									'noTimeIn' => $noTimeInStatus,
									'noTimeOut' => "yes",//$noTimeOutStatus,
									'WorkHours' => $workHour,
									'workStatus' => $workStatus,
									'total_overtime' => $totalOvers,
									'new_color_overtime' => $overtime_non_do,
									'overStatus' => $overStatus,
									'status' => $status,
									'OvertimeRestDay' => $overRest,
									'OvertimeRestDay_Color' => $OTR_color,
									'timeOutStatus' => $timeOutStatus,
									'timeInStatus' => $timeInStatus,
									'C_TimeIn' => $timeInStatusC,
									'C_TimeOut' => $timeOutStatusC,
									'Short' => $short,
									'Late' => $query[$i]->Late,
									'EarlyOut' =>  ($earlyOut == 0 ? $earlyOut = '-' : $earlyOut = $earlyOut  ),
									'between' => $between
								];
							//}
						}





						$i++;
					}
				}
			}




			$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
		}
		else{
			$data = ['data'=>[], 'status'=>500, 'message'=>'Record data is empty'];
		}
		return $data;
		}
	}

	// NEW CUTOFF ADD OBJECT TAR, ABS, ND, NDOT, ROT, ROTDO
	public function search(){

			ini_set('max_execution_time', 600);
			/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

			// declare validasi
			$reg = [
			'num'=>'Regex:/^[0-9-\! -\^]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];

			$rule = [
				"attendance" =>"required|".$reg['num']."|max:21",
				"department" => "required|numeric",
				"payroll_period" => "required|".$reg['num']."|max:21",
				"payout_date" => "required|date",
				"radio" => "numeric"
			];

			$valid = \Validator::make(\Input::all(),$rule);

			if($valid->fails()){
				$message='Required Input.'; $status=500; $data=null;
				return response()->json(['header'=>['message' => 'data not found', 'status' => 500],'data' => []],500);
			}

			$LIB_ATT = new calculation;
			if($access[1] == 200){
			$i   =  \Input::all();

			if(isset($i['tabActive']) && $i['tabActive']  != null){
				$pointer  =   $i['tabActive'];
			}else{
				$pointer  =  0;
			}

			$check_attendance =   (!isset($i['attendance']) ? null : $i['attendance'] );
			if($check_attendance == null)
			{
				return response()->json(['header' =>['message' => 'please choose  one of the item  in attendance cut off' ]]);
			}else{
				$explode  =   explode('-',$i['attendance']);
				$start_date  =  $explode[0];
				$end_date  	= $explode[1];

				$period = new DatePeriod(
				 new DateTime($start_date),
				 new DateInterval('P1D'),
				 new DateTime($end_date)
				);

				$rangeDt = "";
				foreach ($period as $keys1 => $values1) {
				  if(strlen($rangeDt) == 0){
					$rangeDt = "'".$values1->format('Y-m-d')."'";
				  }else{
					$rangeDt .= ",'".$values1->format('Y-m-d')."'";
				  }
				  //$value->format('Y-m-d')
				}

				$data2a = str_replace("/", "-", $end_date);
				$rangeDt .= ", '".$data2a."'";
			}



			if(!isset($start_date) && !isset($end_date)){
				return response()->json(['header'=>['message' => 'please choose date   in Attendance Cut-off Periode', 'status' => 500],'data' => []],500);
			}else{
				// $get_employee_name =  \DB::SELECT("select * from  att_schedule where  date >=  '$start_date' and  date <= '$end_date' 	group by employee_id ");
				// if($get_employee_name != null){
				// 	foreach ($get_employee_name as $key => $value) {
				// 		$arr_emp_list[] = $value->employee_id;
				// 	}
				// 	$explore_emp_new =  implode(',',$arr_emp_list);
					$query_emp   =  "t1.date >=  '$start_date' and  t1.date <= '$end_date'";
				// }else{
				// 	return response()->json(['header' => ['message' => 'data not found','status'  => 500],'data' => []],500);
				// }

			}

			if(isset($i['radio']) and  $i['radio'] != null)
			{
				$radio  =   $i['radio'];
				if($radio  == 1){
					$query_radio   = " and  t2.local_it = 1 ";
				}

				if($radio  == 2){
					$query_radio   =  " and  t2.local_it in (2,3)";
				}

				if($radio  == 3){
					$query_radio  = " and  t2.local_it in (1,2,3)";
				}

			}else{
				$query_radio  = " and  t2.local_it in (1,2,3)";
			}


			if(isset($i['department']) and $i['department'] != null){
				$department  =  $i['department'];
				$query_department  =   " and t2.department = '$department' and t1.employee_id NOT IN(SELECT SUBSTR(ct.cutoff_per_payroll,17,7) AS emp_id FROM cutoff_date ct where ct.start_='$start_date' AND ct.from_='$end_date' AND ct.department_id = '$department' ) ";
			}else{
				$query_department = null;
			}


			if(isset($i['job']) and $i['job'] != null)
			{
				$job  = $i['job'];
				$query_job  =   "and  t11.job = $job ";
			}else{
				$query_job = null;
			}

			$query_order   =  "and t1.status = 2 order by t1.date ASC";



			// INCREASE END DATE FOR END CUTOFF DATE
			$increase_end_date 	= date('Y/m/d', strtotime("+1 day", strtotime($end_date)));
			$increase_end_date2 = date('Y-m-d', strtotime("+1 day", strtotime($end_date)));
			$query_emp   =  "t1.date >=  '$start_date' and  t1.date <= '$increase_end_date'";

			$data = $LIB_ATT->att('cut-off-record',$query_emp,$query_job,$query_department,$query_radio);

			//$query_increase_end_date   			=  "t1.date >= '$increase_end_date' and t1.date <= '$increase_end_date'";
			//$arr_res = $LIB_ATT->att('cut-off-record',$query_increase_end_date,$query_job,$query_department,$query_radio);


			//declare variable ==============================================
			$tar = [];
			$abs = [];
			$nd = [];
			$rot = [];
			$rotdo = [];
			$employee_ids = [];
			
			if($data['status']==500){
				return \Response::json(['header'=>['message'=>$data['message'],'status'=>$data['status']],'data'=>$data['data']],$data['status']);
			}

			$arr_periode = ["data"=>[]];
			$arr_periode_increase = ["data"=>[],"status"=>$data['status']];

			if(count($data['data'])){
				foreach ($data['data'] as $key2 => $value2) {
					if($value2['date'] == $increase_end_date2){
						array_push($arr_periode_increase['data'],$value2);
					}else{
						array_push($arr_periode['data'],$value2);
					}
				}
			}

			$data['data'] = $arr_periode['data'];

			$tmp_cal_cutoff = $this->get_cutoff_result($data,$employee_ids,$start_date,$end_date,$arr_periode_increase,$increase_end_date2);
			$data['cutoff_calculate'] = $tmp_cal_cutoff;


			// SEARCHING ===================================
			$tmp2 = []; $hasbeen = false;
			if(count($data['data'])){
				$cutting = \DB::SELECT("select cutoff_detail_json, department_id, job_title_id, local_it_user from cutoff_date where start_ = '$start_date' and from_ = '$end_date'");

				if(count($cutting) > 0){
					foreach ($data['data'] as $key => $value) {
						foreach ($cutting as $cutting_key => $cutting_value) {

							$json = json_decode($cutting_value->cutoff_detail_json,1);
							//return [$json,$cutting];
							if($value['employee_id'] == $json['employee_id']){
								unset($data['data'][$key]);
								$hasbeen = true;
							}else{
								//array_push($tmp2,$value);
							}
						}
					}
					//$data['data'] = $tmp2;
				}
			}

			if($hasbeen){
				foreach ($data['data'] as $key => $value) {
					array_push($tmp2, $value);
					$data['data'] = $tmp2;
				}
			}

			if(count($data['data']) == 0){
				$data['data'] = [];
				$data['cutoff_calculate'] = null;
				$data['message'] = 'data not exist';
				if($hasbeen){
					$data['message'] = "Some employees from this department have been cutoff";
				}
				$data['status'] = 200;
			}

			if(isset($data['status']) && $data['status'] == 200){
				//$data = $data;
				return $data;
	    }else{
				return \Response::json(['header'=>['message'=>$data['message'],'status'=>$data['status']],'data'=>$data['data']],$data['status']);
	    }
			//return \Response::json(['header'=>['message'=>$data['message'],'status'=>$data['status']],'data'=>$data['data']],$data['status']);

	        // ######################################## END ##############################


			//$query = \DB::SELECT($base_query);
			$training_temp = [];
			if($data['data'] !=  null){
				//$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
				return ['data'=>$data['data'], 'status'=>200, 'message'=>'View record data'];
			//if($query !=  null){

				foreach ($query as $key => $value) {
					$date_changeShift  =  '';
					if($value->date_changeShift  !=   '99:99:99'){
							$employee_q  =  $value->employee_id;
							$date_changeS =  $value->date_changeShift;
							$db = \DB::SELECT("select concat(substring(aws._from,1,5),'-',substring(aws._to,1,5)) as data from att_schedule_request as asr ,att_change_shift as acs, 	attendance_work_shifts as aws
							 	where asr.update_at = '$date_changeS'
								and asr.request_id = acs.id
							 	and acs.new_shift = aws.shift_id
							 	and asr.employee_id  = '$employee_q'
							 	and  asr.type_id = 5
							 	and asr.status_id =  2");

							if($db != null){
						   		$date_changeShift = $db[0]->data;
							}else{
								$date_changeShift = '99:99:99';
							}

					}else{
						$date_changeShift  =  '99:99:99';
					}

					$query[$key]->schedule_changeShift =   $date_changeShift;

					/** schedule  **/

					$schedule  = '';
					// OLD>>
					// if($query[$key]->schedule_changeShift !=  '99:99:99'  and  $query[$key]->date_changeShift != '99:99:99' )
					// 	{ $schedule   =  $query[$key]->schedule_changeShift;    }
					// else if($value->date_swapShift != '99:99:99' and  $value->schedule_swapShift  !=  '99:99:99' )
					// 	{ $schedule  =  $value->schedule_swapShift; }
					// else
					// 	{ $schedule  =  substr($value->fromx,0,5).'-'.substr($value->tox,0,5); }

					if($query[$key]->schedule_changeShift !=  '99:99:99'  and  $query[$key]->date_changeShift != '99:99:99' ){
						$schedule   =  $query[$key]->schedule_changeShift;
						$exp_sch = explode("-",$schedule);
						$tox 	= $exp_sch[1].":00";
						$fromx	= $exp_sch[0].":00";
					}
					else if($value->date_swapShift != '99:99:99' and  $value->schedule_swapShift  !=  '99:99:99' ){
						$schedule  =  $value->schedule_swapShift;
						$exp_sch = explode("-",$schedule);
						$tox 	= $exp_sch[1].":00";
						$fromx	= $exp_sch[0].":00";
					}else{
						$schedule  =  substr($value->fromx,0,5).'-'.substr($value->tox,0,5);
						if(isset($value->$schedule)){
							if($value->$schedule != "99:99:99" && $value->$schedule == $schedule){
								$exp_sch = explode("-",$schedule);
								$tox 	= $exp_sch[1].":00";
								$fromx	= $exp_sch[0].":00";
							}else{
								$exp_sch = explode("-",$value->$schedule);
								$tox 	= $exp_sch[1].":00";
								$fromx	= $exp_sch[0].":00";
							}
						}else{
							$exp_sch = explode("-",$schedule);
							$tox 	= $exp_sch[1].":00";
							$fromx	= $exp_sch[0].":00";
						}
					}

					$query[$key]->schedule =  $schedule;

					/*
					* TRAINING
					*/
					if($value->start_date_training != "99:99:99" || $value->shift_code == 'T'){
						$a = strtotime($query[$key]->date_swapShift);
						$allow = true;
						if($query[$key]->date_changeShift != '99:99:99'){
							$b = strtotime($query[$key]->date_changeShift);
							if($a < $b){ $allow = false; }
						}elseif($query[$key]->date_swapShift != '99:99:99'){
							$b = strtotime($query[$key]->date_swapShift);
							if($a < $b){ $allow = false; }
						}
						if($value->shift_code == 'T'){
							$training_temp[] = $value->date;
							$allow = false;
						}

						if($allow){
							$begin = new DateTime($value->start_date_training);
							$end = new DateTime($value->end_date_training);
							$end = $end->modify( '+1 day' );

							$interval = new DateInterval('P1D');
							$daterange = new DatePeriod($begin, $interval ,$end);

							foreach($daterange as $date){
								$training_temp[] = $date->format("Y-m-d");
							}
						}

					}

					if(count($training_temp) > 0){
						$a =  array_search($value->date, $training_temp);
						if(gettype($a) == 'integer'){
							$query[$key]->training_req =  $training_temp[$a];
						}else{
							$query[$key]->training_req = false;
						}
					}else{
						$query[$key]->training_req = false;
					}
					// get data and time
					$sch_date[$key] = [
						'date' => $value->date,
						'time' => [
							'in' => $fromx,
							'out'=> $tox,
							'timeIn'=>$value->timeIn,
							'timeOut'=>$value->timeOut
						]
					];
					// END TRAINING SECTION >>

					/** workhour  **/

					/*$workhours  = '';*/
					$workhours  = null;
					$timeIn =  $value->timeIn;
					$fromx  =   $value->fromx;
					// ADD NEW >>
					$total_menit_workhours = null;
					$short_fix = null;
					$late_fix=null;
					$earlyOut_fix=null;
					$eot_fix2=null;
					// $Eout = '';
					// OLD >>
					// $tox =  $value->tox;
					// $fromx  =  $value->fromx;

					if($value->timeIn != '-' and  $value->timeOut !=  '-' || $value->timeIn != null and  $value->timeOut !=  null){

						$timeIn  					.= ':00';
						$fromx2    					= $fromx;
						$timeOuts 					= $value->timeOut;
						$timeOuts 					.= ":00";
						$date_inquire_absen 		= $value->date_inquire_absen;
						$dates 						= $value->date;
						$dates_Biometric 			= $value->date_absen;
						$strtotime_TimeInInquire 	= strtotime("$value->date_inquire_absen $timeIn");
						$strtotime_TimeInBiometric 	= strtotime("$value->date_absen $timeIn");

						$strtotime_formx 			= strtotime("$value->date $fromx2");
						$strtotime_tox 				= strtotime("$value->date $tox");

						$strtotime_TimeOutBiometric = strtotime("$value->date_absen $timeOuts");
						$strtotime_TimeOutInquire 	= strtotime("$value->date_inquire_absen $timeOuts");

						// ######################################################################################
						$am_pm_form 				= date("a",$strtotime_formx);
						$am_pm_to 					= date("a",$strtotime_tox);

						if($value->date_inquire_absen){
							$am_pm_deviceFrom 				= date("a",$strtotime_TimeInInquire);
							$am_pm_deviceTo					= date("a",$strtotime_TimeOutBiometric);
						}else{
							$am_pm_deviceFrom 				= date("a",$strtotime_TimeInBiometric);
							$am_pm_deviceTo					= date("a",$strtotime_TimeOutBiometric);
						}

						$splitOut_time 		= explode(":", $timeOuts);
						$splitOut_time_h 	= (integer)$splitOut_time[0];
						$splitOut_time_m 	= (integer)$splitOut_time[1];

						// CARI WORKHOURS DEVICE

						if($am_pm_form == 'pm' && $am_pm_to == "am"){
							$strtotime_tox 	= strtotime('+1 day' ,strtotime("$value->date $tox"));
						}

						// OLD
						// if($value->date_inquire_absen){

						// 	if( ($am_pm_deviceFrom == 'am' && $am_pm_deviceTo == "am") || ($am_pm_deviceFrom == 'pm' && $am_pm_deviceTo == "pm")){
						// 		$total_device_workhours 	= $strtotime_TimeOutBiometric - $strtotime_TimeInBiometric;
						// 	}else{
						// 		if($am_pm_deviceTo == "pm" && ( ($splitOut_time_h == 24 && $splitOut_time_m == 0) || ($splitOut_time_h > 12 && $splitOut_time_h < 24) ) ){
						// 			$total_device_workhours 	= $strtotime_TimeOutBiometric - $strtotime_TimeInBiometric;
						// 		}else{
						// 			$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInBiometric;
						// 		}
						// 	}
						// }else{
						// 	if( ($am_pm_deviceFrom == 'am' && $am_pm_deviceTo == "am") || ($am_pm_deviceFrom == 'pm' && $am_pm_deviceTo == "pm")){
						// 		$total_device_workhours 	= $strtotime_TimeOutBiometric - $strtotime_TimeInBiometric;
						// 	}else{
						// 		if($am_pm_deviceTo == "pm" && ( ($splitOut_time_h == 24 && $plitOut_time_m == 0) || ($splitOut_time_h > 12 && $splitOut_time_h < 24) ) ){
						// 			$total_device_workhours 	= $strtotime_TimeOutBiometric - $strtotime_TimeInBiometric;
						// 		}else{
						// 			$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInBiometric;
						// 		}
						// 	}
						// }

						// NEW WITH FUNCTION

						$LIB_CAL_ATT = $LIB_ATT->calculation_attendance([
							'value'							=> $value,
							'fromx2'						=> $fromx2,
							'tox'							=> $tox,
							'strtotime_formx'				=> $strtotime_formx,
							'strtotime_tox'					=> $strtotime_tox,
							'timeIn' 						=> $timeIn,
							'timeOuts' 						=> $timeOuts,
							'strtotime_TimeInInquire' 		=> $strtotime_TimeInInquire,
							"strtotime_TimeInBiometric" 	=> $strtotime_TimeInBiometric,
							"strtotime_TimeOutInquire" 		=> $strtotime_TimeOutInquire,
							"strtotime_TimeOutBiometric" 	=> $strtotime_TimeOutBiometric,
							"late_fix" 						=> $late_fix,
							"eot_fix2" 						=> $eot_fix2
							//"total_device_workhours" 		=> $total_device_workhours
						]);

						$value 							= $LIB_CAL_ATT['value'];
						$fromx2 						= $LIB_CAL_ATT['fromx2'];
						$tox 							= $LIB_CAL_ATT['tox'];
						$strtotime_formx 				= $LIB_CAL_ATT['strtotime_formx'];
						$strtotime_tox 					= $LIB_CAL_ATT['strtotime_tox'];
						$strtotime_TimeInInquire 		= $LIB_CAL_ATT['strtotime_TimeInInquire'];
						$strtotime_TimeInBiometric 		= $LIB_CAL_ATT["strtotime_TimeInBiometric"];
						$strtotime_TimeOutInquire 		= $LIB_CAL_ATT["strtotime_TimeOutInquire"];
						$strtotime_TimeOutBiometric 	= $LIB_CAL_ATT["strtotime_TimeOutBiometric"];
						$late_fix 						= $LIB_CAL_ATT["late_fix"];
						$eot_fix2 						= $LIB_CAL_ATT["eot_fix2"];
						$total_device_workhours 		= $LIB_CAL_ATT["total_device_workhours"];


						$total_schedule_workhours 	= $strtotime_tox - $strtotime_formx;

						// schedule work
						$total_schedule_minutes 	= $total_schedule_workhours / 60;
						$split_schedule_hours 		= floor($total_schedule_minutes / 60);
						$split_schedule_minutes 	= ($total_schedule_minutes % 60);
						// device work
						$total_device_minutes 		= $total_device_workhours / 60;
						$split_device_minutes 		= ($total_device_minutes % 60);
						$split_device_hours 		= floor($total_device_minutes / 60);



						// OVERTIME
						$tmp_m = 0;$tmp_h = 0;
						if($split_schedule_hours < $split_device_hours){
							if($split_schedule_minutes < $split_device_minutes){
								$tmp_m = $split_device_minutes - $split_schedule_minutes;
							}else{
								$tmp_m = $split_schedule_minutes - $split_device_minutes;
							}
							$tmp_h = $split_device_hours - $split_schedule_hours;
							if($tmp_m < 10){ $tmp_m = "0$tmp_m"; }
							if($tmp_h < 10){ $tmp_h = "0$tmp_h"; }

							$query[$key]->total_overtime = "$tmp_h:$tmp_m";
							if("$tmp_h:$tmp_m" == "00:00"){
								$query[$key]->total_overtime = "-";
							}
						}else{
							if($split_schedule_minutes < $split_device_minutes){ $tmp_m = $split_device_minutes - $split_schedule_minutes; }
							else{ $tmp_m = $split_schedule_minutes - $split_device_minutes; }
							if($tmp_m < 10){ $tmp_m = "0$tmp_m"; }
							if($tmp_h < 10){ $tmp_h = "0$tmp_h"; }

							$query[$key]->total_overtime = "$tmp_h:$tmp_m";
							if("$tmp_h:$tmp_m" == "00:00"){
								$query[$key]->total_overtime = "-";
							}
						}

						$time_work	= null;
						if($split_device_hours < 10){ $time_work = "0$split_device_hours:"; }
						else{ $time_work = "$split_device_hours:"; }

						if($split_device_minutes < 10){ $time_work .= "0$split_device_minutes"; }
						else{ $time_work .= "$split_device_minutes"; }



						// CARI TIMEOUT DEVICE FIX INQUERY
						$timeOut_device_fix 	= strtotime("+".$split_schedule_hours." hour",$strtotime_TimeInBiometric);
						// CARI WAKTU PULANG SESUAI WORKHOURS SCHEDULE
						if($value->date_inquire_absen){

							// $dt_timeoutfix 			= date("Y-m-d H:i:s",$timeOut_device_fix);
							// $dt_timeoutbiometric 	= date("Y-m-d H:i:s",$strtotime_TimeOutInquire);
							// $a1 = new DateTime($dt_timeoutfix);
							// $a2 = new DateTime($dt_timeoutbiometric);
							// $s 	= $a2->diff($a1);
							// $overtime_fix = $s->format("%H:%I");


							// CARI LATE INQUERY
							if($strtotime_formx < $strtotime_TimeInInquire){
								/*$mktimes_late_fix 	= ($strtotime_TimeInInquire - $strtotime_formx)/60;
								$late_fix 			= floor($mktimes_late_fix);*/
								$mktimes_late_fix 	= mktime(0,($strtotime_TimeInInquire - $strtotime_formx)/60);
								$late_fix 			= date('H:i',$mktimes_late_fix);
							}else{ $late_fix 		= null; }

						}else{
							// CARI TIMEOUT DEVICE FIX
							// $dt_timeoutfix 			= date("Y-m-d H:i:s",$timeOut_device_fix);
							// $dt_timeoutbiometric 	= date("Y-m-d H:i:s",$strtotime_TimeOutBiometric);
							// $a1 = new DateTime($dt_timeoutfix);
							// $a2 = new DateTime($dt_timeoutbiometric);
							// $s 	= $a2->diff($a1);
							// $overtime_fix = $s->format("%H:%I");

							// CARI LATE
							if($strtotime_formx < $strtotime_TimeInBiometric){
								$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
								$late_fix 		= date("H:i",$mktimes_late_fix);
							}else{ $late_fix 	= null; }

						}

						/*
						* CARI EARLYOUT
						* earlyout not working training and dayoff
						*/
						if($timeOut_device_fix > $strtotime_TimeOutBiometric){
							$mktimes_earlyOut_fix 	= mktime(0,($timeOut_device_fix - $strtotime_TimeOutBiometric)/60);
							$earlyOut_fix 			= date("H:i",$mktimes_earlyOut_fix);
						}else{ $earlyOut_fix 	= null; }


						/* CARI SHORT
						*  short not working dayoff and training
						*/
						if($total_device_workhours < $total_schedule_workhours){
							$mktimes_short_fix = mktime(0,($total_schedule_workhours - $total_device_workhours)/60);
							$short_fix = date('H:i',$mktimes_short_fix);
						}else{ $short_fix = null; }

						// OVERTIME FIX
						if($total_schedule_workhours < $total_device_workhours){
							$mktimes_overtime_fix = mktime(0,($total_device_workhours - $total_schedule_workhours)/60);
							$total_overtime_fix = date("H:i",$mktimes_overtime_fix);
						}else{ $total_overtime_fix = null; }
						$query[$key]->total_overtime = $total_overtime_fix;


						// $query[$key]['late_fix'] = $late_fix;
						// $query[$key]['short_fix'] = $short_fix;
						// $query[$key]['earlyout_fix'] = $earlyout_fix;
						// #######################################################################################


						$exp_strtotime_TimeInInquire 	= explode(":", $strtotime_TimeInInquire);
						$exp_strtotime_TimeInBiometric 	= explode(":", $strtotime_TimeInBiometric);
						$exp_strtotime_formx 			= explode(":", $strtotime_formx);
						$exp_strtotime_TimeOutBiometric = explode(":", $strtotime_TimeOutBiometric);
						$exp_timein 					= explode(":",$timeIn);
						$late_time 						= null;

						$mktimes_workhours_device 	= mktime(0,($total_device_workhours/60));
						// if($value->date == "2018-05-10"){
						// 	return $total_device_workhours;
						// }
						$workhours 					= /*date('H:i',$mktimes_workhours_device)*/ $time_work;

						// if(strtotime($timeIn) < strtotime($fromx)){
						// 	$workhours  =  date('H:i',strtotime($value->timeOut) - strtotime($value->fromx));
						// }else{
						// 	$workhours  =  date('H:i',strtotime($value->timeOut) - strtotime($value->timeIn));
						// }

						//$workhours .=  ':00';


						// if($value->date ==  "2017-09-08"){
						// 	return $workhours;
						// }

							if(strtotime($workhours) > strtotime(date('H:i',(strtotime($tox) -  strtotime($fromx)))) )
							{ $Eout =  0; }
							else
							{
								$timeOut  = $value->timeOut;
								$timeOut  = $timeOut.':00';

								$time1 =  substr(($query[$key]->schedule),6,5).':00';

								if(strtotime($time1) < strtotime($value->timeOut)){
									$Eout =  0;
								}else{
									$Eout = date('H:i', strtotime($time1) - strtotime($value->timeOut));
								}

							}

					}else{
						$Eout = '-';
						$workhours  =  null;
					}



					$query[$key]->WorkHours =  $workhours;
					$query[$key]->WorkHours =  $workhours;
					if($query[$key]->shift_code == 'DO'){
						$query[$key]->OvertimeRestDay = $workhours;
					}

					 //short
					// if($workhours  != null){
					// 	//if($value->date ==   '2017-09-01'){
					// 		$diff = date('H:i:s',(strtotime($tox) -  strtotime($fromx)));

					// 		$arr  = [$workhours,$tox,$fromx,$diff];

					// 		if(strtotime($workhours) > strtotime($diff)){
					// 			$shortx  =  0;
					// 		}else{
					// 			$shortx =  date('H:i:s',strtotime($diff) -  strtotime($workhours));
					// 		}


					// }else{
					// 	$shortx  = "null";
					// }



					/*$query[$key]->Short  =  $shortx;*/
					$query[$key]->Short  =  $short_fix;

					//schedule mod
					// $query[$key]->schedule_mod =  concat((substring((select schedule),1,5)),':00');
					$query[$key]->schedule_mod = substr($query[$key]->schedule,0,5).':00';

					//late;

					// $late  =  0;
					if(strtotime($query[$key]->schedule_mod) < strtotime($value->timeIn) and $value->timeOut != null)
						{ $late = date('H:i',(strtotime($value->timeIn) -  strtotime($query[$key]->schedule_mod))); }
					else
						{ $late  = 0;  }

					/*$query[$key]->Late  =  $late;*/
					$query[$key]->Late = $late_fix;

					//earlyOut
					$query[$key]->EarlyOut = $Eout;

				}
			}

		   $schedule=null;
			$max_count  =  count($query);
			if($query){
				$count  =   count($query) - 1;
				$c = count($query);
				$mod = $c % 10;
				$count = $c - $mod;
				// if($mod <= 10){
				// 	return $data = ['data'=>$query, 'status'=>500, 'message'=>'Record data is less than 1 month'];
				// }
				$j = 0 ;
				$last = $c - 20;

				if(isset($query[$j]->fromx) && isset($query[$j]->tox)){
					$h_formx = substr($query[$j]->fromx, 0,5);
					$h_tox = substr($query[$j]->tox, 0,5);
				}

				for ($i = 0; $i < $c; ) { // $c = 57 / $i until 56
					// $dt = substr($dt, 8);
					// first period

					if ($i <= 9){
						// if(!isset($query[$j+8]->date)){
						// 	return $query;
						// }


						$between = $query[$j]->date.' <=> '.$query[$max_count-1]->date;
						$between = $between;
					}
					// second period
					else if ($i >= 10 && $i <= 19){

						$between2 = $query[$j+(10-$j)]->date.' <=> '.$query[$max_count-1]->date;

						$between = $between2;
					}
					// third period
					else{
						$date = date("d");

						$between3 = $query[$j+(20-$j)]->date.' <=> '.$query[$max_count-1]->date;
						$between = $between3;
					}


					if ($i >= $count-1){ // if $i = 50 then $i - $c-1
						$between = $query[$i]->date.'<==>'.$query[$c-1]->date;
						$a = $mod;
					}
					else{
						$between = $query[$i]->date.'<==>'.$query[$max_count-1]->date;
						$a = $count;
					}
					for ($j=1; $j <= $a; $j++) {
						$schedule = $query[$i]->schedule;

						$name = $query[$i]->employee_id;
						// create a mark, yes for workHour < hour_per_day, and no for other
						if ($query[$i]->WorkHours != null){
							// if($query[]->date  == '2017-09-02'){
							// 	return $arr  = [$query[$i]->WorkHours,$query[$i]->hour_per_day];
							// }
							if ($query[$i]->WorkHours < $query[$i]->hour_per_day) {

								$emp_over  =  $query[$i]->employee_id;
								$date_str =  $query[$i]->date;


								$check_overtime = \DB::SELECT("select * from att_overtime as ao,att_schedule_request as asr where ao.employee_id =  '$emp_over' and ao.date_str  = '$date_str' and ao.id  = asr.request_id and asr.status_id  = 2  ");


								if($check_overtime != null){
									$workStatus = "no";
								}else{
									$workStatus = "yes";
								}
							}else{
								$emp_over  =  $query[$i]->employee_id;
								$date_str =  $query[$i]->date;
								$check_overtime = \DB::SELECT("select * from att_overtime as ao,att_schedule_request as asr where ao.employee_id =  '$emp_over' and ao.date_str  = '$date_str' and ao.id  = asr.request_id and asr.status_id  = 2  ");


								if($check_overtime != null){
									$workStatus = "no";
								}else{
									$workStatus = "yes";
								}
							}
						}
						else{
							$workStatus = "no";
						}
						/*
						//	Change the view to DO if schedule is 00:00-00:00
						//	Change the view to "-" if schedule is null
						*/

						if ($query[$i]->schedule == "00:00-00:00") {
							$date_modify = $query[$i]->date;
							$emp_modify = $query[$i]->employee_id;

							$give =\DB::SELECT("select aws.shift_code from att_schedule ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify'
								               and ash.date = '$date_modify' and  ash.shift_id = aws.shift_id  ");
							//$check_workshift = \DB::SELECT("select * from att_schedule where ")

							if($query[$i]->date_changeShift !=  '99:99:99' and $query[$i]->status ==  2){

								$give   = \DB::SELECT("select aws.shift_code from att_change_shift ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify'
								               and ash.date = '$date_modify' and  ash.new_shift = aws.shift_id ");
								$schedule = $give[0]->shift_code;
							}else{
								$give   = \DB::SELECT("select aws.shift_code from att_schedule ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify'
								               and ash.date = '$date_modify' and  ash.shift_id = aws.shift_id ");
								if(count($give) > 0){
									$schedule = $give[0]->shift_code;
								}
							}

							if ($query[$i]->timeIn == null AND $query[$i]->timeOut == null) {
								$timeIn = "-";
								$timeOut = "-";
								$noTimeInStatus = "yes";
								$noTimeInStatus = "yes";
							}
							else if ($query[$i]->timeIn == null AND $query[$i]->timeOut != null) {
								$timeIn = "-";
								$timeOut = $query[$i]->timeOut;
								$noTimeInStatus = "yes";
								$noTimeInStatus = "no";
							}
							else if ($query[$i]->timeIn != null AND $query[$i]->timeOut != null) {
								$timeIn = $query[$i]->timeIn;
								$timeOut = $query[$i]->timeOut;
								$noTimeInStatus = "no";
								$noTimeInStatus = "no";
							}
							else {
								$timeIn = $query[$i]->timeIn;
								$timeOut = "-";
								$noTimeInStatus = "no";
								$noTimeInStatus = "yes";
							}
						}
						else {
							$schedule = $query[$i]->schedule;
							if ($query[$i]->timeIn == null) {
								$timeIn = "-";
								$noTimeInStatus = "yes";
							}
							else {
								$timeIn = $query[$i]->timeIn;
								$noTimeInStatus = "no";
							}

							if ($query[$i]->timeOut == null) {
								$timeOut = "-";
								$noTimeOutStatus = "yes";
							}
							else {
								$timeOut = $query[$i]->timeOut;
								$noTimeOutStatus = "no";
							}

						}



						if ($query[$i]->total_overtime == null) {
							$timeIns = strtotime($query[$i]->timeOut);
							$explode = explode('-',$query[$i]->schedule);
							$arg2    = strtotime($explode[1]);
							if($timeIns > $arg2){
								if($schedule == 'DO'){
									$totalOver = '-';

									//ini masalahnya
									$mass = "yess";
								}else{
									if($query[$i]->timeOut == "00:00"){
										$query[$i]->timeOut = "24:00";
									}
									$time1x  =   explode(':',$query[$i]->timeOut);
									$time2x =  explode(':',$explode[1]);

									$time1xr  =   ($time1x[0] * 60) + $time1x[1];
									$time2xr  =   ($time2x[0] * 60) + $time2x[1];

									if(strlen(($time1xr - $time2xr) % 60) ==  1){
										$m	 = '0'.(($time1xr - $time2xr) % 60);
									}else{
										$m   = (($time1xr - $time2xr) % 60);
									}
									/*$totalOver =  floor(($time1xr - $time2xr) / 60).' : '.$m;*/
									$totalOver =  /*floor(($time1xr - $time2xr) / 60).' : '.$m*/ $query[$i]->total_overtime;

								}

							$makeOver =  'yes';
							}else{
								$makeOver = 'no';
								$totalOver = "-";
							}
						}else {

							$floor   = floor($query[$i]->total_overtime);
							$decimal = $query[$i]->total_overtime - $floor;
							if($decimal != 0){
								$decimal = (60 * (10 * $decimal))/10;
							}else{
								$decimal = "00";
							}
							$ax = strlen($floor);

							$ax = ($ax == 1 ? '0'.$floor : $floor);
							/*$totalOver = $floor.':'.$decimal;*/
							$totalOver = /*$floor.':'.$decimal*/ $query[$i]->total_overtime;
							$makeOver  ='no';
						}


						if ($query[$i]->WorkHours == '00:00:00' || $query[$i]->WorkHours == null){
							 $workHour = "-"; $totalOver = "-";

						}
						else{

							if(strchr($query[$i]->WorkHours,'-')){

								$exp_schedule = explode('-', $query[$i]->schedule);
								$ts1 = strtotime($exp_schedule[0]);
								$ts2 = strtotime($exp_schedule[1]);
								$diff = abs($ts1 - $ts2) / 3600;
								if(strpos($diff,'.')){
									$e_menit = explode('.', "$diff");
									$tmp = "0.".$e_menit[1]; // build menit
									$b_menit = round(floatval($tmp) * 60 );
									if(strlen("$b_menit") == 2){
										$workHour =  $e_menit[0].":$b_menit";
									}else{
										$workHour =  $e_menit[0].":0$b_menit";
									}
								}else{
									$t = $diff/60;
									if(strchr("$t",".")){
										$diff = $diff/2;
									}
									$workHour = round($diff).":00";
								}
							}else{
								 $workHour = $query[$i]->WorkHours;
								 $explode = explode(":",$workHour);
								 $hi = $explode[0];
								 if(substr($hi,0,1) == 0){
								 	$workHour =  substr($hi,1,1).':'.$explode[1];
								 }else{
								 	$workHour =  $hi.':'.$explode[1];
								 }
							}
						}



						if($totalOver != "-" and $schedule == 'yes'){
							$timeIns = strtotime($query[$i]->timeOut);
							$explode = explode('-',$query[$i]->schedule);
							$arg2    = strtotime($explode[1]);
							$overRest = date('H:i',($timeIns - $arg2));
						}else if($query[$i]->timeIn and $query[$i]->timeOut){


							//$overRest =
						}else{
							$overRest = "-";
						}

						// if($totalOver ==  "-"&& $overRest !="-"){
						// 	$totalOver = "-";
						// }

						if ($query[$i]->OvertimeRestDay == NULL){
							if(isset($mass) && $mass == "yess"){
								if($schedule == "DO"){
									$timeInss = strtotime($query[$i]->timeOut);
									$timeOuts = strtotime($query[$i]->timeIn);
									$overRest = date('H:i',($timeInss - $timeOuts));

									if($timeIns == false && $timeOuts == false){
										$overRest = "-";
									}else{
										$workHour = $overRest;
									}
								}
							}else{
								$overRest = "-";
							}

						}else{
							$overRest = $query[$i]->OvertimeRestDay;
							$explode =  explode('.',$query[$i]->OvertimeRestDay);
							$floor =  floor($overRest);
							$check_floor = $overRest -  $floor;
							if($check_floor != 0){
								$overRest_decimal = (60 * (10 * $decimal))/10;
								$overRest = $explode[0].':'.$overRest_decimal;
							}else{
								$overRest = ((int)$explode[0]+1).':'.'00';
							}
						}


						if ($query[$i]->Short == null) {$short = "-";}
						else{
							$time1 = intval(substr($query[$i]->Short,0,2));
							$time2 = intval(substr($query[$i]->Short,3,2));
							$short = ($time1 * 60) + $time2;
							if($short == 0){
								$short = '-';
							}else{
								$short =  $short;
							}

						}


						if ($query[$i]->ApproveStatus == null || $query[$i]->ApproveStatus == 0 ){ $status = "no"; }
						elseif ($query[$i]->ApproveStatus == 1) {$status = "yes";}
						else{ $status = "no";}

						/*if ($query[$i]->Late == null || $query[$i]->Late == 0){
							// if($query[$i]->employee_id ==  '2016054' and $query[$i]->date == "2017-11-12"){
							// 	return $query[$i]->Late;
							// }
							$colorLate  = "grey";
						}else{
							$date_late = $query[$i]->date;
							$name   =  $query[$i]->employee_id;
							$chkLate  = \DB::SELECT("select status_id from att_schedule_request where employee_id  = '$name' and availment_date = '$date_late' and type_id  = 7 order by id desc  limit 1");
							if($chkLate ==  null){
								$colorLate  = 'purple';
							}else{
								if($chkLate[0]->status_id == 1){
									$colorLate  = "orange";
								}elseif($chkLate[0]->status_id == 2){
									$colorLate  = "green";
								}elseif($chkLate[0]->status_id == 3){
									$colorLate  = "red";
								}elseif($chkLate[0]->status_id == 4){
									$colorLate  = "purple";
								}else{
									//KUDOS
								}
							}
							 	$late = $query[$i]->Late;
						}*/

						if ($query[$i]->Late == null || $query[$i]->Late == "0"){
							//if($query[$i]->date == "2018-05-01"){ return (array)($query[$i]->Late == 0); }
							$late = "-";
						}else{

							$date_late 	= $query[$i]->date;
							//$chkLate  	= \DB::SELECT("select * from att_schedule_request where employee_id  = '$name' and availment_date = '$date_late' and type_id  = 7 and status_id  = 2 order by id desc  limit 1");
							$chkLate 	= \DB::SELECT("select asr.date_request, asr.availment_date, asr.approver, al.late from att_schedule_request as asr, att_late as al where asr.employee_id  = '$name' and asr.availment_date = '$date_late' and asr.type_id  = 7 and asr.status_id  = 2 and al.late_id = asr.request_id order by asr.id desc  limit 1");
							if($chkLate ==  null){
								$colorLate  = 'red';
							}elseif(isset($chkLate[0])){

								if($chkLate[0]->approver ==  null){
									$colorLate  = 'orange';
								}else{

									$split_time_sch  	= explode(":",$query[$i]->fromx);
									$split_late			= explode(":",$chkLate[0]->late);

									$split_late_h = (integer)$split_late[0];
									$split_late_m = (integer)$split_late[1];
									$split_time_sch_h = (integer)$split_time_sch[0];
									$split_time_sch_m = (integer)$split_time_sch[1];

									if($split_late_h == 0){
										$tmp = $split_time_sch_m + $split_late_m;
										if($tmp > 60){
											$split_time_sch_h = $split_late_h + 1;
											$split_time_sch_m = $tmp-60;
										}else{
											$split_time_sch_m = $tmp;
										}
									}else{
										$tmp_h = $split_time_sch_h + $split_late_h;
										if($tmp_h >= 24){
											$split_time_sch_h = $tmp_h - 24;
										}else{
											$split_time_sch_h = $tmp_h;
										}

										$tmp_m = $split_time_sch_m + $split_late_m;
										if($tmp_m > 60){
											$split_time_sch_h = $split_late_h + 1;
											$split_time_sch_m = $tmp_m-60;
										}else{
											$split_time_sch_m = $tmp_m;
										}
									}

									if($split_time_sch_h < 10){
										$build = "0$split_time_sch_h:$split_time_sch_m:00";
									}else{
										$build = "$split_time_sch_h:$split_time_sch_m:00";
									}
									$date_com 		= $query[$i]->date;
									$time_device 	= $query[$i]->timeIn;

									$com_late = strtotime("$date_com $build");
									$com_device = strtotime("$date_com $time_device:00");

									if($com_device <= $com_late){
										$colorLate  	= 'green';
									}else{
										$colorLate  	= 'orange';
									}

								}
							}else{
								$colorLate  = 'green';
							}
								//if($value->date == "2018-05-01"){ return $query[$i]->Late; }
							 	$late = $query[$i]->Late;
							//}
						}

						if ($query[$i]->EarlyOut == null){ $earlyOut = "-";}
						else{

							// $a1 = new \DateTime("17:00:00.000");
							// $a2 = new \DateTime("18:55:00.000");
							// $b 	= date_diff($a2,$a1);
							// $str =$b->format('%H:%I');

							$explode = explode(":",$query[$i]->EarlyOut);
							 	$earlyOut = strlen($explode[0]);
							 	if($earlyOut == 2 and $earlyOut != '00'){
							 		$ho = (int)$explode[0];
							 		$mi = (int)$explode[1];
							 		$ho = $ho * 60;
							 		$earlyOut = $ho + $mi ;
							 	}else if($earlyOut == 2 and $earlyOut == '00'){
							 		$mi = (int)$explode[1];
							 		$earlyOut =   $mi;
							 	}else{
							 		$earlyOut = "-";
							 	}
							if($earlyOut != '-'){
								//return var_dump($query[$i]);
							}
						}
						$h = substr($schedule, 6, strpos($schedule,":"));
						$m = substr($schedule, 9, strpos($schedule,10));
						//print_r($h."\n");
						$h1 = substr($timeOut, 0, strpos($timeOut,":"));
						$m1 = substr($timeOut, 3, strpos($timeOut,4));
						// create mark for the record that has been changed, yes for changed record
						if ($totalOver != null){
							if($makeOver == 'yes'){
								$overStatus = ( $overRest == "-" ?  "yes" : "no");
								if(isset($mass) && $mass == "yess"){
									$overStatus = "yes";
								}
								// $overStatus = "yes";
							}elseif($makeOver == "no") {
								$overStatus = "no";
							}
						}
						else
							$overStatus = "no";
							if($overRest !=  null && isset($query[$i]->RestDay_App)){
								if($query[$i]->RestDay_App == 2){
									$OTR_color =  'yes';
								}else{
									$OTR_color =  'no';
								}

							}else{
								$OTR_color = "-";
							}


						// Compare time in and time out between attendance record and request
						$emp = $query[$i]->employee_id;
						$dt = $query[$i]->date;
						$_timeIN = $query[$i]->timeIn.':00';
						$_timeOUT = $query[$i]->timeOut.':00';

						// create mark for the record that has been changed, yes for changed record
						$tm = \DB::select("CALL View_TimeInOut_byDate_employee('$emp', '$dt')");
						$test[] = $dt;
						if ($tm != null) {
							if ($tm[0]->req_in != $_timeIN)
								$timeInStatus = "yes";
							else
								$timeInStatus = "no";

							if ($tm[0]->req_out != $_timeOUT)
								$timeOutStatus = "yes";
							else
								$timeOutStatus = "no";
						}
						else {
							$timeInStatus = "no";
							$timeOutStatus = "no";
						}
						if ($i == $c) {

							$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
							return $data;
						}else{
							if($totalOver != '-'){

								if($workHour != "0:00"){
									// if($query[$i]->date  ==   '2017-08-22'){
									// 		return $totalOver;
									// }

									$totalOvers =  $totalOver;
								}else{

									$totalOvers = '-';
								}

								// if($totalOvers ==  '00:00' || $workHour == "0:00"){
								// 	$totalOvers = '-';
								// }
							}else{
								$totalOvers = '-';
							}


							if($overRest != "-"){
								$short = '-';
								$late = '-';
								$earlyOut = '-';
							}

							if(strlen($schedule) ==  2){
								$short = '-';
								$late = '-';
								$earlyOut = '-';
								$totalOvers = '-';
								$overRest = '-';

							}

							$empx = $query[$i]->employee_id;
							$date_request = $query[$i]->date;
							$latest_modified  =  \DB::SELECT("select from_,to_,leave_code from leave_request,leave_type where employee_id  = '$empx' and leave_request.leave_type = leave_type.id and leave_code != '' and leave_request.status_id = 2");
							$latest_request  =  \DB::SELECT("select * from att_schedule_request as asr, att_training as atr where asr.status_id = 2 and asr.employee_id  = '$empx' and asr.request_id = atr.id and '$date_request' >= atr.start_ and '$date_request' <= atr.end and asr.type_id = 1");
							$latest_request1  =  \DB::SELECT("select * from att_schedule_request as asr, att_training  as atr where asr.status_id = 2 and asr.employee_id  = '$empx' and asr.type_id = 2 and asr.request_id  = atr.id and '$date_request' >= atr.start_ and '$date_request' <= atr.end limit 1");
							$terminated = \DB::SELECT("select * from emp_termination_reason where employee_id  = '$empx' ");
							//if($latest_request != null){
							//$continue_training  = \Db::SELECT("select * from ")

								if(isset($latest_request[0]->type_id)  ){
									$schedule =   'T';
								}
								if(isset($latest_request1[0]->type_id)  ){
									$schedule =   'OB';
								}
							//}
							if($latest_modified != null){
								foreach ($latest_modified as $key => $value) {
									if($value->from_ <= $query[$i]->date && $query[$i]->date <= $value->to_){
										$schedule =   $value->leave_code;
									}
								}
							}


							$holiday = \DB::SELECT("select * from attendance_holiday");
								foreach ($holiday as $key => $value) {
									$holiday_date  =  $query[$i]->date;
									if($holiday != null){
										if($holiday[$key]->repeat_annually == 1){
											if($holiday[$key]->day == null && $holiday[$key]->week == null){
												if($holiday[$key]->date == $holiday_date ){
													$schedule = 'H';
												}
											}
											if($holiday[$key]->day != null && $holiday[$key]->week != null){

												$holiday[$key];
												$year =  date('Y');
												$ass_hole =  explode('-',$holiday[$key]->date);
												$month_name  =  date("F", mktime(0, 0, 0,(intval($ass_hole[1])), 10));
												$ass_day =  $holiday[$key]->day;

												if($holiday[$key]->week == 'first'){
													$week_day =  '+1';
												}elseif($holiday[$key]->week == 'second'){
													$week_day =  '+2';
												}elseif ( $holiday[$key]->week == 'third'){
													$week_day = '+3';
												}elseif($holiday[$key]->week == 'fourth'){
													$week_day = '+4';
												}else{
												  $week_day = '+4';
												}
												$get_new_ass_hole  = date('Y-m-d',strtotime("$week_day week $ass_day $month_name $year"));

												if($holiday_date == $get_new_ass_hole){
											 		$schedule = 'H';

											 	}
											}
										}else{
											if($holiday[$key]->date == $query[$i]->date){
												$schedule = 'H';
											}
										}
									}
								}


							if($terminated != null){
								$schedule = 'TER';
							}
							if(!isset($colorLate)){
								$colorLate = 'red';
							}

							if($schedule  == 'DO'){
								$ti =  strtotime($timeIn.':00');
								$to =  strtotime($timeOut.':00');

								$overRest  = date('H:i',$to - $ti);
								if($overRest == '00:00'){
									$overRest  =   '-';
								}
							}else{
								$overRest = '-';
							}

							if($workStatus ==  'no'and  $schedule == 'DO'){
								$OTR_color = 'yes';
							}else{
								$OTR_color = 'no';
							}
							$date_c = $query[$i]->date;
							$emp_c  = $query[$i]->employee_id;

							// ORIGINAL
							/*if($earlyOut  != null){
								$emp_c  = $query[$i]->employee_id;
								$date_c = $query[$i]->date;
								// print_r("select * from  att_late, att_schedule_request where  att_late.type_id = 7 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id and att_schedule_request.approver = 2");
								$early_c =  \DB::SELECT("select att_schedule_request.status_id from  att_late, att_schedule_request where  att_late.type_id = 8 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id");

								if($early_c != null){
									if($early_c[0]->status_id ==  1){
										$query[$i]->early_c  = "orange";
									}elseif($query[$i]->early_c ==  2){
										$query[$i]->early_c  = "green";
									}elseif($query[$i]->early_c ==  3){
										$query[$i]->early_c  = "red";
									}elseif($query[$i]->early_c ==  4){
										$query[$i]->early_c  = "purple";
									}else{
										//KUDOS
									}

								}else{
									$query[$i]->early_c =  'purple';
								}
							}else{
								$query[$i]->early_c =  'grey';
							}*/

							if($earlyOut  != null){
								$emp_c  = $query[$i]->employee_id;
								$date_c = $query[$i]->date;
								// print_r("select * from  att_late, att_schedule_request where  att_late.type_id = 7 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id and att_schedule_request.approver = 2");
								$early_c =  \DB::SELECT("select att_schedule_request.status_id from  att_late, att_schedule_request where  att_late.type_id = 8 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id");

								if($early_c != null){
									$query[$i]->early_c =  'green';
								}else{
									$query[$i]->early_c =  'red';
								}
							}else{
								$query[$i]->early_c =  'red';
							}

							if(strlen($schedule) >  4){
								$turns =  explode('-',$schedule);
								if(!isset($turns[1])){
								  $workStatus  =  "no";
								}else{
								$base_date1 =  $turns[0].':00';
								$base_date2 =  $turns[1].':00';

								$substract_1 =  $timeIn.':00';
								$substract_2 =  $timeOut.':00';

							 	$get_tot_date1   = strtotime($base_date2) -  strtotime($base_date1);
								$get_tot_date2   = strtotime($substract_2) - strtotime($substract_1);

									if($get_tot_date1 <= $get_tot_date2  || $get_tot_date1 == $get_tot_date2){
										$workStatus  =  "no";
									}
								}
							}

							//check color  time  in   out
						 	// par timeInStatusC  and timeOutStatusC

							//if($query[$i]->date  ===  '2017-09-10'){
								if($timeIn   ==   '-'){
									$date_timeIn   = $query[$i]->date;
									$date_timeIn_employee  =  $query[$i]->employee_id;

									$check_time_orange  = \DB::SELECT("select t2.status_id, t1.req_in from  att_time_in_out_req as t1,att_schedule_request
																		as t2,pool_request as t3
																		where t1.date  =   '$date_timeIn'
																		and   t1.req_in_out_id =  t2.request_id and  t1.type_id = t2.type_id
																		and   t2.id = t3.id_req  and  t2.type_id   =  t3.type_id and t3.master_type = 1
																		and  t1.employee_id = '$date_timeIn_employee' order  by  t1.req_in_out_id DESC limit 1 ");

									// OLD
									// if($check_time_orange  != null){
									// 	///return  $check_time_orange;
									// 	if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  ==  1){
									// 			$timeInStatusC = 'orange';
									// 			$timeIn  =   substr($check_time_orange[0]->req_in,0,5);
									// 	}elseif($check_time_orange[0]->status_id  ==  4){
									// 		$timeInStatusC = 'purple';
									// 	}elseif($check_time_orange[0]->status_id  ==  3){
									// 		$timeInStatusC = 'red';
									// 	}elseif($check_time_orange[0]->status_id  ==  2){
									// 		$timeInStatusC = 'green';
									// 	}else{
									// 		// ASS HOLE ALWAYS CHANGE
									// 	}
									// }else{
									// 	$timeInStatusC = 'purple';
									// }
									if($check_time_orange  != null){
										///return  $check_time_orange;
										if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  ==  1){

												$timeInStatusC = 'orange';
												$timeIn  =   substr($check_time_orange[0]->req_in,0,5);
										}else{
											$timeInStatusC = 'green';
										}
									}else{
										$timeInStatusC = 'red';
									}
								}else{ $timeInStatusC = 'green'; }
							//}

							if($timeOut   ==   '-'){
								$date_timeOut   = $query[$i]->date;
								$date_timeOut_employee  =  $query[$i]->employee_id;
								$check_time_orange  = \DB::SELECT("select t2.status_id,t1.req_out from  att_time_in_out_req as t1,att_schedule_request
																	as t2,pool_request as t3
																	where t1.date  =   '$date_timeOut'
																	and   t1.req_in_out_id =  t2.request_id and  t1.type_id = t2.type_id
																	and   t2.id = t3.id_req  and  t2.type_id   =  t3.type_id and t3.master_type = 1
																	and  t1.employee_id = '$date_timeOut_employee' order  by  t1.req_in_out_id DESC limit 1 ");

								//OLD
								// if($check_time_orange  != null){
								// 	if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  == 1){
								// 			$timeOutStatusC = 'orange';
								// 			$timeOut  =   substr($check_time_orange[0]->req_out,0,5);
								// 	}elseif($check_time_orange[0]->status_id  ==  4){
								// 			$timeInStatusC = 'purple';
								// 	}elseif($check_time_orange[0]->status_id  ==  3){
								// 			$timeInStatusC = 'red';
								// 	}elseif($check_time_orange[0]->status_id  ==  2){
								// 			$timeInStatusC = 'green';
								// 	}else{
								// 			// ASS HOLE ALWAYS CHANGE
								// 	}
								// }else{
								// 	$timeOutStatusC = 'purple';
								// }
								if($check_time_orange  != null){
									if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  == 1){
											$timeOutStatusC = 'orange';
											$timeOut  =   substr($check_time_orange[0]->req_out,0,5);
									}else{
										$timeOutStatusC = 'green';
									}
								}else{
									$timeOutStatusC = 'red';
								}
							}else{ $timeOutStatusC = 'green'; }

							//check_color_orange

							//check colcor late
							// if($colorLate == 'red'){
							// 	if($query[$i]->date ==  '2017-03-05'){

							// 		$date_late = $query[$i]->date;
							// 		$nameX  =  $query[$i]->employee_id;
							// 		$chkLate  = \DB::SELECT("select * from att_schedule_request where employee_id  = '$nameX' and availment_date = '$date_late' and type_id  = 7 and  status_id  = 1 order by id desc  limit 1");

							// 		if($chkLate  !=  null){
							// 			//if($chkLate[0]->approver  ==   null){
							// 				$colorLate  = 'orange';
							// 			//}
							// 		}
							// 	}
							// }

							//check color early  out
							// if($query[$i]->early_c =  'red'){
							// $early_c =  \DB::SELECT("select * from  att_late, att_schedule_request where  att_late.type_id = 8 and date  = '$date_c' and 	att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id and att_schedule_request.status_id = 1");

							// 	if($early_c != null){
							// 		$query[$i]->early_c =  'orange';
							// 	}
							// }

							//set color to  oramg  for  overtime and overtime dso
							//if  already input for  overtime  rest day parameter $OTR_color
							//If  already  input for  overtime   no  do    parameter $overtime_non_do


							//return $totalOvers;
							if($totalOvers != null){
								$date_overtime  =  $query[$i]->date;
								$employee_overtime = $query[$i]->employee_id ;

								$search  = \DB::SELECT("select * from att_overtime as t1, att_schedule_request as t2
														where t1.employee_id  =  '$employee_overtime'
														and  t1.date_str =  '$date_overtime'
														and  t1.type_id =  t2.type_id
														and  t1.id = t2.request_id
														order by t1.id desc limit 1
														");
								// if($search  != null){
								// 	if($search[0]->status_id == 1){
								// 		$overtime_non_do  =  'orange';
								// 	}elseif($search[0]->status_id == 2){
								// 		$overtime_non_do  =  'green';
								// 	}elseif($search[0]->status_id == 3){
								// 		$overtime_non_do  =  'red';
								// 	}elseif($search[0]->status_id == 4){
								// 		$overtime_non_do  =  'purple';
								// 	}else{
								// 		//
								// 	}


								// }else{
								// 	$overtime_non_do =   'purple';
								// }
								if($search  != null){
									if($search[0]->status_id == 1){
										$overtime_non_do  =  'orange';
									}else{
										$overtime_non_do  =  'green';
									}


								}else{
										 $overtime_non_do =   'red';
								}


							}else{
								$overtime_non_do = 'red';
							}

							if($overRest != null){
								$date_overtime  =  $query[$i]->date;
								$employee_overtime = $query[$i]->employee_id ;

								$search  = \DB::SELECT("select * from att_overtime as t1, att_schedule_request as t2
														where t1.employee_id  =  '$employee_overtime'
														and  t1.date_str =  '$date_overtime'
														and  t1.type_id =  t2.type_id
														and  t1.id = t2.request_id
														order by t1.id desc limit 1
														");
								// if($search  != null){
								// 	if($search[0]->status_id == 1){
								// 		$OTR_color  =  'orange';
								// 	}elseif($search[0]->status_id == 2){
								// 		$OTR_color  =  'green';
								// 	}elseif($search[0]->status_id == 3){
								// 		$OTR_color  =  'red';
								// 	}elseif($search[0]->status_id == 4){
								// 		$OTR_color  =  'purple';
								// 	}else{
								// 		// KUDOS
								// 	}


								// }else{
								// 		$OTR_color =   'purple';
								// }
								if($search  != null){
									if($search[0]->status_id == 1){
										$OTR_color  =  'orange';
									}else{
										$OTR_color  =  'green';
									}


								}else{
										$OTR_color =   'red';
								}

							}else{
								$OTR_color  = 'red';
							}

							//check  changeshift
							if(isset($query[$i]->date) ){
								// if($query[$i]->date ==  '2017-09-14'){
								// $date_changeshift  =  $query[$i]->date;
								// $employee_changeshift = $query[$i]->employee_id ;

								// return $check_exist_change_shift   =  \DB::SELECT("select * from  att_change_shift as t1 ,  att_schedule_request  as t2,
								// 											attendance_work_shifts as  t3
								// 										    where t1.employee_id = '$employee_changeshift' and  date = '$date_changeshift'
								// 										    and  t1.type_id  =  t2.type_id
								// 										    and  t1.id  = t2.request_id
								// 										    and  t1.new_shift  =  t3.shift_id
								// 										    and t2.status_id  =  2 order  by  t1.id  desc limit 1");

								// return   $check_exist_change_shift;

								// }
								$date_changeshift  =  $query[$i]->date;
								$employee_changeshift = $query[$i]->employee_id ;


								$check_exist_change_shift   =  \DB::SELECT("select * from  att_change_shift as t1 ,  att_schedule_request  as t2,
																			attendance_work_shifts as  t3
																		    where t1.employee_id = '$employee_changeshift' and  date = '$date_changeshift'
																		    and  t1.type_id  =  t2.type_id
																		    and  t1.id  = t2.request_id
																		    and  t1.new_shift  =  t3.shift_id
																		    and t2.status_id  =  2 order  by  t1.id  desc limit 1");
								if($check_exist_change_shift != null){
									$list_code = ['DO','ADO','VL','BL','TB','SL','EL','BE','ML','PL','MA','OB',];



									if(in_array($check_exist_change_shift[0]->shift_code,$list_code)){
										$schedule =  $check_exist_change_shift[0]->shift_code;
										if($query[$i]->date == "2017-09-09" && $query[$i]->employee_id == "2015037"){
											return [$schedule,(array)$query[$i],"TEST1"];
										}
									}else{
										$schedule  =  substr($check_exist_change_shift[0]->_from,0,5).'-'. substr($check_exist_change_shift[0]->_to,0,5);
										if($query[$i]->date == "2017-09-09" && $query[$i]->employee_id == "2015037"){
											return [$schedule,(array)$query[$i],"TEST2"];
										}
									}
								}

							}

							$attendance =  \Input::get('attendance');
							$explode =  explode('-',$attendance);

							$explode1 = explode('/',$explode[0]);


							$exp_sch  =  explode('-', $schedule);
							// if(!isset($exp_sch[1]) || $query[$i]->Late  ==  0 ){

							// 	$query[$i]->Late  = '-';
							// }

							try{
								if($query[$i]->training_req){
									$schedule = 'T';
									$late = '-';
								}

								if($query[$i]->OvertimeRestDay != '99:99:99'){
									$overRest = $query[$i]->OvertimeRestDay;
								}

							}catch(\Exception $e){
								return [$e,$query[$i]];
							}


							// if($query[$i]->date == "2017-09-09" && $query[$i]->employee_id == "2015037"){
							// 				return [$schedule,(array)$query[$i],"TEST2"];
							// 			}
							if($pointer == 0){

								//$get_date_basic  =   explode('-',$query[$i]->date);

								//if($explode1[2]  ==  $get_date_basic[2] ){
									$temp[] = [
										'early_c' => $query[$i]->early_c,
										'date' => $query[$i]->date,
										'colorLate' => $colorLate,
										'employee_id' => $query[$i]->employee_id,
										'Name' => $query[$i]->Name,
										'schedule' => $schedule,
										'timeIn' => $timeIn,
										'timeOut' => $timeOut,
										'noTimeIn' => $noTimeInStatus,
										'noTimeOut' => "yes",//$noTimeOutStatus,
										'WorkHours' => $workHour,
										'workStatus' => $workStatus,
										'total_overtime' => $totalOvers,
										'new_color_overtime' => $overtime_non_do,
										'overStatus' => $overStatus,
										'status' => $status,
										'OvertimeRestDay' => $overRest,
										'OvertimeRestDay_Color' => $OTR_color,
										'timeOutStatus' => $timeOutStatus,
										'timeInStatus' => $timeInStatus,
										'C_TimeIn' => $timeInStatusC,
										'C_TimeOut' => $timeOutStatusC,
										'Short' => $short,
										'Late' => /*$query[$i]->Late*/ $late,
										'EarlyOut' =>  ($earlyOut == 0 ? $earlyOut = '-' : $earlyOut = $earlyOut  ),
										'between' => $between
									];
								//}
							}else{

								//$get_date_basic  =   explode('-',$query[$i]->date);

								//if($pointer  ==  $get_date_basic[2] ){
									$temp[] = [
										'early_c' => $query[$i]->early_c,
										'date' => $query[$i]->date,
										'colorLate' => $colorLate,
										'employee_id' => $query[$i]->employee_id,
										'Name' => $query[$i]->Name,
										'schedule' => $schedule,
										'timeIn' => $timeIn,
										'timeOut' => $timeOut,
										'noTimeIn' => $noTimeInStatus,
										'noTimeOut' => "yes",//$noTimeOutStatus,
										'WorkHours' => $workHour,
										'workStatus' => $workStatus,
										'total_overtime' => $totalOvers,
										'new_color_overtime' => $overtime_non_do,
										'overStatus' => $overStatus,
										'status' => $status,
										'OvertimeRestDay' => $overRest,
										'OvertimeRestDay_Color' => $OTR_color,
										'timeOutStatus' => $timeOutStatus,
										'timeInStatus' => $timeInStatus,
										'C_TimeIn' => $timeInStatusC,
										'C_TimeOut' => $timeOutStatusC,
										'Short' => $short,
										'Late' => $query[$i]->Late,
										'EarlyOut' =>  ($earlyOut == 0 ? $earlyOut = '-' : $earlyOut = $earlyOut  ),
										'between' => $between
									];
								//}
							}





							$i++;
						}
					}
				}


				// $count_temp  =  count($queryx);

				// if($count_temp  > 9){
				// 	$mod  =  $count_temp   % 10;
				// 	if($mod  != 0){
				// 		$get_paging  =  ($count_temp -  $mod) / 10;
				// 		$get_paging  += 1;
				// 	}else{
				// 		$get_paging  =  $count_temp  / 10;
				// 	} 'paging' => $get_paging
				// }


				$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
			}
			else{
				$data = ['data'=>[], 'status'=>500, 'message'=>'Record data is empty'];
			}
			return $data;
			}
	}

}
