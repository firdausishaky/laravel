<?php namespace Larasite\Http\Controllers\Attendance\CutOff;
set_time_limit(0);
use Larasite\Http\Requests;
use Larasite\Library\calculation;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Attendance\cutOff\Attendance_cutoffrecordModel;
use Illuminate\Http\Request;

use Larasite\Library\FuncAccess;

class AttendanceCutOffRecord extends Controller {
	protected $form = 75;

	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		
		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
				return $att_record-> getMessageAccess("Unauthorized",500,[],$access[3]);
			}else{
			
				
				$counter = \DB::SELECT("select start_ , from_ from cutoff_date  ");
				$count =  count($counter);
				
				$data_ex = \DB::SELECT("select att_cutoff_from,att_cutoff_to from att_cutoff_period where att_cutoff_from != '0000-00-00'
										and att_cutoff_to != '0000-00-00' ");
				
				$arr =[];
				$x = 0;
				if($data_ex != null){
					foreach ($data_ex as $key => $value) {
						if($count > 0){
							for($i = 0; $i  < $count; $i++){
								if($value->att_cutoff_from == $counter[$i]->start_ && $value->att_cutoff_to == $counter[$i]->from_){
									//unset($data_ex[$key]);
								}
								// else{
								// 	$date1 = explode("-",$value->att_cutoff_from);
								// 	$date2 = explode("-",$value->att_cutoff_to);
								// 	$arr[] = ["periode" => $date1[0].'/'.$date2[1].'/'.$date1[2].'-'.$date2[0].'/'.$date2[1].'/'.$date2[2]];
								// }
							}
						}
					}
				}

					$result = [];
					foreach ($data_ex as $key => $value) {
							$date1 = explode("-",$data_ex[$key]->att_cutoff_from);
							$date2 = explode("-",$data_ex[$key]->att_cutoff_to);
							$result[] = ["periode" => $date1[0].'/'.$date1[1].'/'.$date1[2].'-'.$date2[0].'/'.$date2[1].'/'.$date2[2]];
					}

					

				$department = \DB::SELECT("select * from department where id>1");
				//$department_ex = array_merge($department,[["id" => 0, "name" => "All"]]);
				$job = \DB::SELECT("select id,title from job");
				//$job_ex = array_merge($job,[["id" => 0, "title" => "All"]]);
				$data = ["department" => $department, "job" => $job, "period" => $result];
				return $att_record->indexAcess($data,$access[3]);
			}

		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

	}

	public function getName()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
				$att_record->getMessage("Unauthorized",500,[],$access[3]);
			}else{
				$input = \Input::all();
				$name = \Input::get('name');
				$rule = [
						'name' => 'required|Regex:/^[A-Za-z]+$/'
						];
				$validation = \Validator::make($input,$rule);
				$data = \DB::SELECT("CALL Search_emp_by_name('$name')");
				return $att_record->search($validation,$data,$access[3]);
			}

		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function cuttingOff()
	{
		

		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$key = \Input::get("key");
			$encode = base64_decode($key);
			$explode = explode("-",$encode);
			$token = $explode[1];
			
			$attendance =  \Input::get('form');

			$department  = (!isset($attendance['department']) ? 0  :   $attendance['department']);
			$job  = (!isset($attendance['job']) ? 0  :   $attendance['job']);	
			$local  = (!isset($attendance['local']) ? 0  :  $attendance['local']);			
			$attendance  = (!isset($attendance['attendance']) ? 0  :  $attendance['attendance']);
			
			$short  =  \Input::get('short_person');
			if(isset($short)){
				$short =  json_encode(\Input::get('short_person')); 		
			}else{
				$short =  null;	
			}


		 	$now  =   date('Y-m-d');
			$explode =  explode('-',$attendance);		

			$explode1 = explode('/',$explode[0]);
			$explodev1 =  $explode1[0].'-'.$explode1[1].'-'.$explode1[2];
			$explode2 = explode('/',$explode[1]);
			$explodev2 =  $explode2[0].'-'.$explode2[1].'-'.$explode2[2];

			$insert = \DB::SELECT("insert into cutoff_date(start_,from_,user,cutoff_at,department_id,job_title_id,cutoff_detail_json,local_it_user) values('$explodev1','$explodev2','$token','$now',$department,$job,'$short',$local)");
			$message = 'Success CutOff '; $status = 200; $data=[];
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function search(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		$LIB_ATT = new calculation;
		if($access[1] == 200){
		$i   =  \Input::all();
		
		if(isset($i['tabActive']) && $i['tabActive']  != null){
			$pointer  =   $i['tabActive'];
		}else{
			$pointer  =  0;
		}

		$check_attendance =   (!isset($i['attendance']) ? null : $i['attendance'] );
		if($check_attendance == null)
		{
			return response()->json(['header' =>['message' => 'please choose  one of the item  in attendance cut off' ]]);
		}else{
			$explode  =   explode('-',$i['attendance']);
			$start_date  =  $explode[0];
			$end_date  	= $explode[1];
		}
		


		if(!isset($start_date) && !isset($end_date)){
			return response()->json(['header'=>['message' => 'please choose date   in Attendance Cut-off Periode', 'status' => 500],'data' => []],500);
		}else{
			// $get_employee_name =  \DB::SELECT("select * from  att_schedule where  date >=  '$start_date' and  date <= '$end_date' 	group by employee_id ");
			// if($get_employee_name != null){
			// 	foreach ($get_employee_name as $key => $value) {
			// 		$arr_emp_list[] = $value->employee_id;
			// 	}
			// 	$explore_emp_new =  implode(',',$arr_emp_list);
				$query_emp   =  "t1.date >=  '$start_date' and  t1.date <= '$end_date'";
			// }else{
			// 	return response()->json(['header' => ['message' => 'data not found','status'  => 500],'data' => []],500);
			// }

		} 	

		if(isset($i['radio']) and  $i['radio'] != null)
		{
			$radio  =   $i['radio'];
			if($radio  == 1){
				$query_radio   = " and  t2.local_it = 1 ";
			}

			if($radio  == 2){
				$query_radio   =  " and  t2.local_it in (2,3)";
			}

			if($radio  == 3){
				$query_radio  = " and  t2.local_it in (1,2,3)";
			}	

		}else{
			$query_radio  = " and  t2.local_it in (1,2,3)";
		}


		if(isset($i['department']) and $i['department'] != null){
			$department  =  $i['department'];
			$query_department  =   " and t2.department = '$department' ";
		}else{
			$query_department = null;
		}	


		if(isset($i['job']) and $i['job'] != null)
		{
			$job  = $i['job'];
			$query_job  =   "and  t11.job = $job ";
		}else{
			$query_job = null;
		}

		$query_order   =  "and t1.status = 2 order by t1.date ASC";

		

		$data = $LIB_ATT->att('cut-off-record',$query_emp,$query_job,$query_department,$query_radio);
		//return $data;
		$tmp2 = [];
		if(count($data['data'])){		
			$cutting = \DB::SELECT("select cutoff_detail_json, department_id, job_title_id, local_it_user from cutoff_date where start_ = '$start_date' and from_ = '$end_date'");
			
			if(count($cutting) > 0){
				foreach ($data['data'] as $key => $value) {
					foreach ($cutting as $cutting_key => $cutting_value) {
						
						$json = json_decode($cutting_value->cutoff_detail_json,1);
						//return [$json,$cutting];
						if($value['employee_id'] == $json['employee_id']){
							unset($data['data'][$key]);
							//array_push($tmp2,$value);
						}
					}		
				}
				//$data['data'] = $tmp2;
			}
		}

		if(count($data['data']) == 0){
			$data['data'] = [];
			$data['message'] = 'data not exist';
			$data['status'] = 500;
		}

		if(isset($data['status']) && $data['status'] == 200){
              $data = $data;
              return $data;
        }else{
         
			return \Response::json(['header'=>['message'=>$data['message'],'status'=>$data['status']],'data'=>$data['data']],$data['status']);
        }
        
        // ######################################## END ##############################


		//$query = \DB::SELECT($base_query);
		$training_temp = [];
		if($data['data'] !=  null){
			//$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
			return ['data'=>$data['data'], 'status'=>200, 'message'=>'View record data'];	
		//if($query !=  null){

			foreach ($query as $key => $value) {
				$date_changeShift  =  '';
				if($value->date_changeShift  !=   '99:99:99'){
						$employee_q  =  $value->employee_id;
						$date_changeS =  $value->date_changeShift;
						$db = \DB::SELECT("select concat(substring(aws._from,1,5),'-',substring(aws._to,1,5)) as data from att_schedule_request as asr ,att_change_shift as acs, 	attendance_work_shifts as aws  
						 	where asr.update_at = '$date_changeS' 
							and asr.request_id = acs.id 
						 	and acs.new_shift = aws.shift_id
						 	and asr.employee_id  = '$employee_q'
						 	and  asr.type_id = 5 
						 	and asr.status_id =  2");

						if($db != null){
					   		$date_changeShift = $db[0]->data;
						}else{
							$date_changeShift = '99:99:99';
						}

				}else{
					$date_changeShift  =  '99:99:99';
				}

				$query[$key]->schedule_changeShift =   $date_changeShift;

				/** schedule  **/

				$schedule  = '';
				// OLD>>
				// if($query[$key]->schedule_changeShift !=  '99:99:99'  and  $query[$key]->date_changeShift != '99:99:99' )
				// 	{ $schedule   =  $query[$key]->schedule_changeShift;    }
				// else if($value->date_swapShift != '99:99:99' and  $value->schedule_swapShift  !=  '99:99:99' ) 
				// 	{ $schedule  =  $value->schedule_swapShift; }
				// else
				// 	{ $schedule  =  substr($value->fromx,0,5).'-'.substr($value->tox,0,5); }

				if($query[$key]->schedule_changeShift !=  '99:99:99'  and  $query[$key]->date_changeShift != '99:99:99' ){ 
					$schedule   =  $query[$key]->schedule_changeShift;
					$exp_sch = explode("-",$schedule);
					$tox 	= $exp_sch[1].":00";
					$fromx	= $exp_sch[0].":00";
				}
				else if($value->date_swapShift != '99:99:99' and  $value->schedule_swapShift  !=  '99:99:99' ){ 
					$schedule  =  $value->schedule_swapShift; 
					$exp_sch = explode("-",$schedule);
					$tox 	= $exp_sch[1].":00";
					$fromx	= $exp_sch[0].":00";
				}else{ 
					$schedule  =  substr($value->fromx,0,5).'-'.substr($value->tox,0,5); 
					if(isset($value->$schedule)){
						if($value->$schedule != "99:99:99" && $value->$schedule == $schedule){
							$exp_sch = explode("-",$schedule);
							$tox 	= $exp_sch[1].":00";
							$fromx	= $exp_sch[0].":00";
						}else{
							$exp_sch = explode("-",$value->$schedule);
							$tox 	= $exp_sch[1].":00";
							$fromx	= $exp_sch[0].":00";
						}
					}else{
						$exp_sch = explode("-",$schedule);
						$tox 	= $exp_sch[1].":00";
						$fromx	= $exp_sch[0].":00";
					}
				}

				$query[$key]->schedule =  $schedule;

				/*
				* TRAINING
				*/
				if($value->start_date_training != "99:99:99" || $value->shift_code == 'T'){
					$a = strtotime($query[$key]->date_swapShift);
					$allow = true;
					if($query[$key]->date_changeShift != '99:99:99'){
						$b = strtotime($query[$key]->date_changeShift);
						if($a < $b){ $allow = false; }
					}elseif($query[$key]->date_swapShift != '99:99:99'){
						$b = strtotime($query[$key]->date_swapShift);
						if($a < $b){ $allow = false; }
					}
					if($value->shift_code == 'T'){
						$training_temp[] = $value->date;
						$allow = false;
					}					

					if($allow){					
						$begin = new DateTime($value->start_date_training);
						$end = new DateTime($value->end_date_training);
						$end = $end->modify( '+1 day' ); 

						$interval = new DateInterval('P1D');
						$daterange = new DatePeriod($begin, $interval ,$end);

						foreach($daterange as $date){	
							$training_temp[] = $date->format("Y-m-d");
						}
					}
					
				}

				if(count($training_temp) > 0){
					$a =  array_search($value->date, $training_temp);
					if(gettype($a) == 'integer'){
						$query[$key]->training_req =  $training_temp[$a];
					}else{
						$query[$key]->training_req = false;
					}
				}else{
					$query[$key]->training_req = false;
				}
				// get data and time
				$sch_date[$key] = [
					'date' => $value->date,
					'time' => [
						'in' => $fromx,
						'out'=> $tox,
						'timeIn'=>$value->timeIn,
						'timeOut'=>$value->timeOut
					]
				];
				// END TRAINING SECTION >>

				/** workhour  **/

				/*$workhours  = '';*/
				$workhours  = null;
				$timeIn =  $value->timeIn;
				$fromx  =   $value->fromx;
				// ADD NEW >>
				$total_menit_workhours = null;
				$short_fix = null;
				$late_fix=null;
				$earlyOut_fix=null;
				$eot_fix2=null;
				// $Eout = '';
				// OLD >>
				// $tox =  $value->tox;
				// $fromx  =  $value->fromx;

				if($value->timeIn != '-' and  $value->timeOut !=  '-' || $value->timeIn != null and  $value->timeOut !=  null){
				
					$timeIn  					.= ':00';		
					$fromx2    					= $fromx;
					$timeOuts 					= $value->timeOut;
					$timeOuts 					.= ":00";
					$date_inquire_absen 		= $value->date_inquire_absen;
					$dates 						= $value->date;
					$dates_Biometric 			= $value->date_absen;
					$strtotime_TimeInInquire 	= strtotime("$value->date_inquire_absen $timeIn");
					$strtotime_TimeInBiometric 	= strtotime("$value->date_absen $timeIn");

					$strtotime_formx 			= strtotime("$value->date $fromx2");
					$strtotime_tox 				= strtotime("$value->date $tox");

					$strtotime_TimeOutBiometric = strtotime("$value->date_absen $timeOuts");
					$strtotime_TimeOutInquire 	= strtotime("$value->date_inquire_absen $timeOuts");

					// ######################################################################################
					$am_pm_form 				= date("a",$strtotime_formx);
					$am_pm_to 					= date("a",$strtotime_tox);
					
					if($value->date_inquire_absen){
						$am_pm_deviceFrom 				= date("a",$strtotime_TimeInInquire);
						$am_pm_deviceTo					= date("a",$strtotime_TimeOutBiometric);
					}else{
						$am_pm_deviceFrom 				= date("a",$strtotime_TimeInBiometric);
						$am_pm_deviceTo					= date("a",$strtotime_TimeOutBiometric);
					}

					$splitOut_time 		= explode(":", $timeOuts);
					$splitOut_time_h 	= (integer)$splitOut_time[0];
					$splitOut_time_m 	= (integer)$splitOut_time[1];

					// CARI WORKHOURS DEVICE
					
					if($am_pm_form == 'pm' && $am_pm_to == "am"){
						$strtotime_tox 	= strtotime('+1 day' ,strtotime("$value->date $tox"));
					}

					// OLD
					// if($value->date_inquire_absen){

					// 	if( ($am_pm_deviceFrom == 'am' && $am_pm_deviceTo == "am") || ($am_pm_deviceFrom == 'pm' && $am_pm_deviceTo == "pm")){
					// 		$total_device_workhours 	= $strtotime_TimeOutBiometric - $strtotime_TimeInBiometric;
					// 	}else{
					// 		if($am_pm_deviceTo == "pm" && ( ($splitOut_time_h == 24 && $splitOut_time_m == 0) || ($splitOut_time_h > 12 && $splitOut_time_h < 24) ) ){
					// 			$total_device_workhours 	= $strtotime_TimeOutBiometric - $strtotime_TimeInBiometric;	
					// 		}else{
					// 			$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInBiometric;
					// 		}
					// 	}
					// }else{
					// 	if( ($am_pm_deviceFrom == 'am' && $am_pm_deviceTo == "am") || ($am_pm_deviceFrom == 'pm' && $am_pm_deviceTo == "pm")){
					// 		$total_device_workhours 	= $strtotime_TimeOutBiometric - $strtotime_TimeInBiometric;
					// 	}else{
					// 		if($am_pm_deviceTo == "pm" && ( ($splitOut_time_h == 24 && $plitOut_time_m == 0) || ($splitOut_time_h > 12 && $splitOut_time_h < 24) ) ){
					// 			$total_device_workhours 	= $strtotime_TimeOutBiometric - $strtotime_TimeInBiometric;	
					// 		}else{
					// 			$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInBiometric;
					// 		}
					// 	}
					// }

					// NEW WITH FUNCTION
					
					$LIB_CAL_ATT = $LIB_ATT->calculation_attendance([
						'value'							=> $value,
						'fromx2'						=> $fromx2,
						'tox'							=> $tox,
						'strtotime_formx'				=> $strtotime_formx,
						'strtotime_tox'					=> $strtotime_tox,
						'timeIn' 						=> $timeIn,
						'timeOuts' 						=> $timeOuts,
						'strtotime_TimeInInquire' 		=> $strtotime_TimeInInquire,
						"strtotime_TimeInBiometric" 	=> $strtotime_TimeInBiometric,
						"strtotime_TimeOutInquire" 		=> $strtotime_TimeOutInquire,
						"strtotime_TimeOutBiometric" 	=> $strtotime_TimeOutBiometric,
						"late_fix" 						=> $late_fix,
						"eot_fix2" 						=> $eot_fix2
						//"total_device_workhours" 		=> $total_device_workhours
					]);

					$value 							= $LIB_CAL_ATT['value'];
					$fromx2 						= $LIB_CAL_ATT['fromx2'];
					$tox 							= $LIB_CAL_ATT['tox'];
					$strtotime_formx 				= $LIB_CAL_ATT['strtotime_formx'];
					$strtotime_tox 					= $LIB_CAL_ATT['strtotime_tox'];
					$strtotime_TimeInInquire 		= $LIB_CAL_ATT['strtotime_TimeInInquire'];
					$strtotime_TimeInBiometric 		= $LIB_CAL_ATT["strtotime_TimeInBiometric"];
					$strtotime_TimeOutInquire 		= $LIB_CAL_ATT["strtotime_TimeOutInquire"];
					$strtotime_TimeOutBiometric 	= $LIB_CAL_ATT["strtotime_TimeOutBiometric"];
					$late_fix 						= $LIB_CAL_ATT["late_fix"];
					$eot_fix2 						= $LIB_CAL_ATT["eot_fix2"];
					$total_device_workhours 		= $LIB_CAL_ATT["total_device_workhours"];

					
					$total_schedule_workhours 	= $strtotime_tox - $strtotime_formx;

					// schedule work
					$total_schedule_minutes 	= $total_schedule_workhours / 60;
					$split_schedule_hours 		= floor($total_schedule_minutes / 60);
					$split_schedule_minutes 	= ($total_schedule_minutes % 60);
					// device work
					$total_device_minutes 		= $total_device_workhours / 60;
					$split_device_minutes 		= ($total_device_minutes % 60);
					$split_device_hours 		= floor($total_device_minutes / 60);
					


					// OVERTIME
					$tmp_m = 0;$tmp_h = 0;
					if($split_schedule_hours < $split_device_hours){
						if($split_schedule_minutes < $split_device_minutes){
							$tmp_m = $split_device_minutes - $split_schedule_minutes;
						}else{
							$tmp_m = $split_schedule_minutes - $split_device_minutes;
						}
						$tmp_h = $split_device_hours - $split_schedule_hours;
						if($tmp_m < 10){ $tmp_m = "0$tmp_m"; }
						if($tmp_h < 10){ $tmp_h = "0$tmp_h"; }
						
						$query[$key]->total_overtime = "$tmp_h:$tmp_m";
						if("$tmp_h:$tmp_m" == "00:00"){
							$query[$key]->total_overtime = "-";
						}
					}else{
						if($split_schedule_minutes < $split_device_minutes){ $tmp_m = $split_device_minutes - $split_schedule_minutes; }
						else{ $tmp_m = $split_schedule_minutes - $split_device_minutes; }
						if($tmp_m < 10){ $tmp_m = "0$tmp_m"; }
						if($tmp_h < 10){ $tmp_h = "0$tmp_h"; }
						
						$query[$key]->total_overtime = "$tmp_h:$tmp_m";
						if("$tmp_h:$tmp_m" == "00:00"){
							$query[$key]->total_overtime = "-";
						}
					}

					$time_work	= null;
					if($split_device_hours < 10){ $time_work = "0$split_device_hours:"; }
					else{ $time_work = "$split_device_hours:"; }

					if($split_device_minutes < 10){ $time_work .= "0$split_device_minutes"; }
					else{ $time_work .= "$split_device_minutes"; }

					

					// CARI TIMEOUT DEVICE FIX INQUERY
					$timeOut_device_fix 	= strtotime("+".$split_schedule_hours." hour",$strtotime_TimeInBiometric);
					// CARI WAKTU PULANG SESUAI WORKHOURS SCHEDULE
					if($value->date_inquire_absen){
						
						// $dt_timeoutfix 			= date("Y-m-d H:i:s",$timeOut_device_fix);
						// $dt_timeoutbiometric 	= date("Y-m-d H:i:s",$strtotime_TimeOutInquire);
						// $a1 = new DateTime($dt_timeoutfix);
						// $a2 = new DateTime($dt_timeoutbiometric);
						// $s 	= $a2->diff($a1);
						// $overtime_fix = $s->format("%H:%I");

						
						// CARI LATE INQUERY
						if($strtotime_formx < $strtotime_TimeInInquire){
							/*$mktimes_late_fix 	= ($strtotime_TimeInInquire - $strtotime_formx)/60;
							$late_fix 			= floor($mktimes_late_fix);*/
							$mktimes_late_fix 	= mktime(0,($strtotime_TimeInInquire - $strtotime_formx)/60);
							$late_fix 			= date('H:i',$mktimes_late_fix);
						}else{ $late_fix 		= null; }

					}else{
						// CARI TIMEOUT DEVICE FIX
						// $dt_timeoutfix 			= date("Y-m-d H:i:s",$timeOut_device_fix);
						// $dt_timeoutbiometric 	= date("Y-m-d H:i:s",$strtotime_TimeOutBiometric);
						// $a1 = new DateTime($dt_timeoutfix);
						// $a2 = new DateTime($dt_timeoutbiometric);
						// $s 	= $a2->diff($a1);
						// $overtime_fix = $s->format("%H:%I");

						// CARI LATE
						if($strtotime_formx < $strtotime_TimeInBiometric){
							$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
							$late_fix 		= date("H:i",$mktimes_late_fix);
						}else{ $late_fix 	= null; }

					}

					/*
					* CARI EARLYOUT
					* earlyout not working training and dayoff
					*/
					if($timeOut_device_fix > $strtotime_TimeOutBiometric){
						$mktimes_earlyOut_fix 	= mktime(0,($timeOut_device_fix - $strtotime_TimeOutBiometric)/60);
						$earlyOut_fix 			= date("H:i",$mktimes_earlyOut_fix);
					}else{ $earlyOut_fix 	= null; }


					/* CARI SHORT
					*  short not working dayoff and training 
					*/
					if($total_device_workhours < $total_schedule_workhours){
						$mktimes_short_fix = mktime(0,($total_schedule_workhours - $total_device_workhours)/60);
						$short_fix = date('H:i',$mktimes_short_fix);
					}else{ $short_fix = null; }

					// OVERTIME FIX
					if($total_schedule_workhours < $total_device_workhours){
						$mktimes_overtime_fix = mktime(0,($total_device_workhours - $total_schedule_workhours)/60);
						$total_overtime_fix = date("H:i",$mktimes_overtime_fix);
					}else{ $total_overtime_fix = null; }
					$query[$key]->total_overtime = $total_overtime_fix;


					// $query[$key]['late_fix'] = $late_fix;
					// $query[$key]['short_fix'] = $short_fix;
					// $query[$key]['earlyout_fix'] = $earlyout_fix;
					// #######################################################################################
					

					$exp_strtotime_TimeInInquire 	= explode(":", $strtotime_TimeInInquire);
					$exp_strtotime_TimeInBiometric 	= explode(":", $strtotime_TimeInBiometric);
					$exp_strtotime_formx 			= explode(":", $strtotime_formx);
					$exp_strtotime_TimeOutBiometric = explode(":", $strtotime_TimeOutBiometric);
					$exp_timein 					= explode(":",$timeIn);
					$late_time 						= null;

					$mktimes_workhours_device 	= mktime(0,($total_device_workhours/60));
					// if($value->date == "2018-05-10"){
					// 	return $total_device_workhours;
					// }
					$workhours 					= /*date('H:i',$mktimes_workhours_device)*/ $time_work;

					// if(strtotime($timeIn) < strtotime($fromx)){
					// 	$workhours  =  date('H:i',strtotime($value->timeOut) - strtotime($value->fromx)); 
					// }else{
					// 	$workhours  =  date('H:i',strtotime($value->timeOut) - strtotime($value->timeIn));
					// }

					//$workhours .=  ':00';


					// if($value->date ==  "2017-09-08"){
					// 	return $workhours;
					// }

						if(strtotime($workhours) > strtotime(date('H:i',(strtotime($tox) -  strtotime($fromx)))) )
						{ $Eout =  0; }
						else
						{ 
							$timeOut  = $value->timeOut;
							$timeOut  = $timeOut.':00';

							$time1 =  substr(($query[$key]->schedule),6,5).':00';

							if(strtotime($time1) < strtotime($value->timeOut)){
								$Eout =  0;	
							}else{
								$Eout = date('H:i', strtotime($time1) - strtotime($value->timeOut));
							}

						}
				
				}else{
					$Eout = '-';
					$workhours  =  null;
				}



				$query[$key]->WorkHours =  $workhours;
				$query[$key]->WorkHours =  $workhours;
				if($query[$key]->shift_code == 'DO'){
					$query[$key]->OvertimeRestDay = $workhours;
				}

				 //short
				// if($workhours  != null){
				// 	//if($value->date ==   '2017-09-01'){
				// 		$diff = date('H:i:s',(strtotime($tox) -  strtotime($fromx)));

				// 		$arr  = [$workhours,$tox,$fromx,$diff];

				// 		if(strtotime($workhours) > strtotime($diff)){
				// 			$shortx  =  0;
				// 		}else{
				// 			$shortx =  date('H:i:s',strtotime($diff) -  strtotime($workhours));
				// 		}

						
				// }else{
				// 	$shortx  = "null";
				// }

				

				/*$query[$key]->Short  =  $shortx;*/
				$query[$key]->Short  =  $short_fix;

				//schedule mod
				// $query[$key]->schedule_mod =  concat((substring((select schedule),1,5)),':00');	
				$query[$key]->schedule_mod = substr($query[$key]->schedule,0,5).':00';

				//late; 
			
				// $late  =  0;
				if(strtotime($query[$key]->schedule_mod) < strtotime($value->timeIn) and $value->timeOut != null)
					{ $late = date('H:i',(strtotime($value->timeIn) -  strtotime($query[$key]->schedule_mod))); }
				else
					{ $late  = 0;  }

				/*$query[$key]->Late  =  $late;*/
				$query[$key]->Late = $late_fix;

				//earlyOut  
				$query[$key]->EarlyOut = $Eout;	
				
			}
		}

	   $schedule=null;
		$max_count  =  count($query);
		if($query){
			$count  =   count($query) - 1;
			$c = count($query);
			$mod = $c % 10;
			$count = $c - $mod;
			// if($mod <= 10){
			// 	return $data = ['data'=>$query, 'status'=>500, 'message'=>'Record data is less than 1 month'];
			// }
			$j = 0 ;
			$last = $c - 20;

			if(isset($query[$j]->fromx) && isset($query[$j]->tox)){										
				$h_formx = substr($query[$j]->fromx, 0,5);
				$h_tox = substr($query[$j]->tox, 0,5);
			}

			for ($i = 0; $i < $c; ) { // $c = 57 / $i until 56
				// $dt = substr($dt, 8);
				// first period

				if ($i <= 9){
					// if(!isset($query[$j+8]->date)){
					// 	return $query;
					// }
					
			
					$between = $query[$j]->date.' <=> '.$query[$max_count-1]->date;
					$between = $between;
				}
				// second period
				else if ($i >= 10 && $i <= 19){

					$between2 = $query[$j+(10-$j)]->date.' <=> '.$query[$max_count-1]->date;

					$between = $between2;
				}
				// third period
				else{
					$date = date("d");

					$between3 = $query[$j+(20-$j)]->date.' <=> '.$query[$max_count-1]->date;
					$between = $between3;
				}


				if ($i >= $count-1){ // if $i = 50 then $i - $c-1
					$between = $query[$i]->date.'<==>'.$query[$c-1]->date;
					$a = $mod;
				}
				else{
					$between = $query[$i]->date.'<==>'.$query[$max_count-1]->date;
					$a = $count;
				}
				for ($j=1; $j <= $a; $j++) {
					$schedule = $query[$i]->schedule;
					
					$name = $query[$i]->employee_id;
					// create a mark, yes for workHour < hour_per_day, and no for other
					if ($query[$i]->WorkHours != null){
						// if($query[]->date  == '2017-09-02'){
						// 	return $arr  = [$query[$i]->WorkHours,$query[$i]->hour_per_day];
						// }	
						if ($query[$i]->WorkHours < $query[$i]->hour_per_day) {

							$emp_over  =  $query[$i]->employee_id;
							$date_str =  $query[$i]->date;
 
					
							$check_overtime = \DB::SELECT("select * from att_overtime as ao,att_schedule_request as asr where ao.employee_id =  '$emp_over' and ao.date_str  = '$date_str' and ao.id  = asr.request_id and asr.status_id  = 2  ");

							
							if($check_overtime != null){
								$workStatus = "no";
							}else{
								$workStatus = "yes";
							} 
						}else{
							$emp_over  =  $query[$i]->employee_id;
							$date_str =  $query[$i]->date;
							$check_overtime = \DB::SELECT("select * from att_overtime as ao,att_schedule_request as asr where ao.employee_id =  '$emp_over' and ao.date_str  = '$date_str' and ao.id  = asr.request_id and asr.status_id  = 2  ");

							
							if($check_overtime != null){
								$workStatus = "no";
							}else{
								$workStatus = "yes";
							} 
						}  
					}
					else{
						$workStatus = "no";
					}
					/*
					//	Change the view to DO if schedule is 00:00-00:00
					//	Change the view to "-" if schedule is null
					*/
					
					if ($query[$i]->schedule == "00:00-00:00") {
						$date_modify = $query[$i]->date;
						$emp_modify = $query[$i]->employee_id;

						$give =\DB::SELECT("select aws.shift_code from att_schedule ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify' 
							               and ash.date = '$date_modify' and  ash.shift_id = aws.shift_id  ");
						//$check_workshift = \DB::SELECT("select * from att_schedule where ")
						
						if($query[$i]->date_changeShift !=  '99:99:99' and $query[$i]->status ==  2){
							
							$give   = \DB::SELECT("select aws.shift_code from att_change_shift ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify' 
							               and ash.date = '$date_modify' and  ash.new_shift = aws.shift_id ");  
							$schedule = $give[0]->shift_code;
						}else{
							$give   = \DB::SELECT("select aws.shift_code from att_schedule ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify' 
							               and ash.date = '$date_modify' and  ash.shift_id = aws.shift_id ");
							if(count($give) > 0){
								$schedule = $give[0]->shift_code;
							}
						}

						if ($query[$i]->timeIn == null AND $query[$i]->timeOut == null) {
							$timeIn = "-";
							$timeOut = "-";
							$noTimeInStatus = "yes";
							$noTimeInStatus = "yes";
						}
						else if ($query[$i]->timeIn == null AND $query[$i]->timeOut != null) {
							$timeIn = "-";
							$timeOut = $query[$i]->timeOut;
							$noTimeInStatus = "yes";
							$noTimeInStatus = "no";
						}
						else if ($query[$i]->timeIn != null AND $query[$i]->timeOut != null) {
							$timeIn = $query[$i]->timeIn;
							$timeOut = $query[$i]->timeOut;
							$noTimeInStatus = "no";
							$noTimeInStatus = "no";
						}
						else {
							$timeIn = $query[$i]->timeIn;
							$timeOut = "-";
							$noTimeInStatus = "no";
							$noTimeInStatus = "yes";
						}
					}
					else {
						$schedule = $query[$i]->schedule;
						if ($query[$i]->timeIn == null) {
							$timeIn = "-";
							$noTimeInStatus = "yes";
						}
						else {
							$timeIn = $query[$i]->timeIn;
							$noTimeInStatus = "no";
						}

						if ($query[$i]->timeOut == null) {
							$timeOut = "-";
							$noTimeOutStatus = "yes";
						}
						else {
							$timeOut = $query[$i]->timeOut;
							$noTimeOutStatus = "no";
						}

					}

					
					
					if ($query[$i]->total_overtime == null) {
						$timeIns = strtotime($query[$i]->timeOut);
						$explode = explode('-',$query[$i]->schedule);
						$arg2    = strtotime($explode[1]);
						if($timeIns > $arg2){
							if($schedule == 'DO'){
								$totalOver = '-';
								
								//ini masalahnya   
								$mass = "yess";
							}else{
								if($query[$i]->timeOut == "00:00"){
									$query[$i]->timeOut = "24:00";
								}
								$time1x  =   explode(':',$query[$i]->timeOut);
								$time2x =  explode(':',$explode[1]);

								$time1xr  =   ($time1x[0] * 60) + $time1x[1];
								$time2xr  =   ($time2x[0] * 60) + $time2x[1];
								
								if(strlen(($time1xr - $time2xr) % 60) ==  1){
									$m	 = '0'.(($time1xr - $time2xr) % 60);
								}else{
									$m   = (($time1xr - $time2xr) % 60);
								}
								/*$totalOver =  floor(($time1xr - $time2xr) / 60).' : '.$m;*/
								$totalOver =  /*floor(($time1xr - $time2xr) / 60).' : '.$m*/ $query[$i]->total_overtime;
							
							}

						$makeOver =  'yes';
						}else{
							$makeOver = 'no';
							$totalOver = "-";
						}
					}else {

						$floor   = floor($query[$i]->total_overtime);
						$decimal = $query[$i]->total_overtime - $floor;
						if($decimal != 0){
							$decimal = (60 * (10 * $decimal))/10;
						}else{
							$decimal = "00";
						}
						$ax = strlen($floor);

						$ax = ($ax == 1 ? '0'.$floor : $floor);
						/*$totalOver = $floor.':'.$decimal;*/
						$totalOver = /*$floor.':'.$decimal*/ $query[$i]->total_overtime;
						$makeOver  ='no';
					}


					if ($query[$i]->WorkHours == '00:00:00' || $query[$i]->WorkHours == null){
						 $workHour = "-"; $totalOver = "-"; 

					}
					else{

						if(strchr($query[$i]->WorkHours,'-')){
							
							$exp_schedule = explode('-', $query[$i]->schedule);
							$ts1 = strtotime($exp_schedule[0]);
							$ts2 = strtotime($exp_schedule[1]);
							$diff = abs($ts1 - $ts2) / 3600;
							if(strpos($diff,'.')){
								$e_menit = explode('.', "$diff");
								$tmp = "0.".$e_menit[1]; // build menit
								$b_menit = round(floatval($tmp) * 60 );
								if(strlen("$b_menit") == 2){
									$workHour =  $e_menit[0].":$b_menit";
								}else{
									$workHour =  $e_menit[0].":0$b_menit";
								}
							}else{
								$t = $diff/60;
								if(strchr("$t",".")){
									$diff = $diff/2;
								}
								$workHour = round($diff).":00";
							}
						}else{						
							 $workHour = $query[$i]->WorkHours;
							 $explode = explode(":",$workHour);
							 $hi = $explode[0];
							 if(substr($hi,0,1) == 0){
							 	$workHour =  substr($hi,1,1).':'.$explode[1];
							 }else{
							 	$workHour =  $hi.':'.$explode[1];
							 }
						}
					}		

					

					if($totalOver != "-" and $schedule == 'yes'){
						$timeIns = strtotime($query[$i]->timeOut);
						$explode = explode('-',$query[$i]->schedule);
						$arg2    = strtotime($explode[1]);
						$overRest = date('H:i',($timeIns - $arg2));
					}else if($query[$i]->timeIn and $query[$i]->timeOut){
						

						//$overRest = 
					}else{
						$overRest = "-";
					}

					// if($totalOver ==  "-"&& $overRest !="-"){
					// 	$totalOver = "-";
					// }

					if ($query[$i]->OvertimeRestDay == NULL){ 
						if(isset($mass) && $mass == "yess"){
							if($schedule == "DO"){
								$timeInss = strtotime($query[$i]->timeOut);
								$timeOuts = strtotime($query[$i]->timeIn);
								$overRest = date('H:i',($timeInss - $timeOuts));
								
								if($timeIns == false && $timeOuts == false){
									$overRest = "-";
								}else{
									$workHour = $overRest;
								}
							}
						}else{
							$overRest = "-";	
						}
						
					}else{
						$overRest = $query[$i]->OvertimeRestDay;
						$explode =  explode('.',$query[$i]->OvertimeRestDay);
						$floor =  floor($overRest);
						$check_floor = $overRest -  $floor;
						if($check_floor != 0){
							$overRest_decimal = (60 * (10 * $decimal))/10;
							$overRest = $explode[0].':'.$overRest_decimal;
						}else{
							$overRest = ((int)$explode[0]+1).':'.'00'; 
						}		
					}
					

					if ($query[$i]->Short == null) {$short = "-";}
					else{
						$time1 = intval(substr($query[$i]->Short,0,2));
						$time2 = intval(substr($query[$i]->Short,3,2));
						$short = ($time1 * 60) + $time2;
						if($short == 0){
							$short = '-';
						}else{
							$short =  $short;
						}

					}
					

					if ($query[$i]->ApproveStatus == null || $query[$i]->ApproveStatus == 0 ){ $status = "no"; }
					elseif ($query[$i]->ApproveStatus == 1) {$status = "yes";}
					else{ $status = "no";}

					/*if ($query[$i]->Late == null || $query[$i]->Late == 0){
						// if($query[$i]->employee_id ==  '2016054' and $query[$i]->date == "2017-11-12"){
						// 	return $query[$i]->Late;
						// } 
						$colorLate  = "grey";
					}else{
						$date_late = $query[$i]->date;
						$name   =  $query[$i]->employee_id;
						$chkLate  = \DB::SELECT("select status_id from att_schedule_request where employee_id  = '$name' and availment_date = '$date_late' and type_id  = 7 order by id desc  limit 1");
						if($chkLate ==  null){
							$colorLate  = 'purple';
						}else{
							if($chkLate[0]->status_id == 1){
								$colorLate  = "orange";
							}elseif($chkLate[0]->status_id == 2){
								$colorLate  = "green";
							}elseif($chkLate[0]->status_id == 3){
								$colorLate  = "red";
							}elseif($chkLate[0]->status_id == 4){
								$colorLate  = "purple";
							}else{
								//KUDOS
							}
						}
						 	$late = $query[$i]->Late;
					}*/

					if ($query[$i]->Late == null || $query[$i]->Late == "0"){ 
						//if($query[$i]->date == "2018-05-01"){ return (array)($query[$i]->Late == 0); }
						$late = "-"; 
					}else{
							
						$date_late 	= $query[$i]->date;
						//$chkLate  	= \DB::SELECT("select * from att_schedule_request where employee_id  = '$name' and availment_date = '$date_late' and type_id  = 7 and status_id  = 2 order by id desc  limit 1");
						$chkLate 	= \DB::SELECT("select asr.date_request, asr.availment_date, asr.approver, al.late from att_schedule_request as asr, att_late as al where asr.employee_id  = '$name' and asr.availment_date = '$date_late' and asr.type_id  = 7 and asr.status_id  = 2 and al.late_id = asr.request_id order by asr.id desc  limit 1");
						if($chkLate ==  null){
							$colorLate  = 'red';
						}elseif(isset($chkLate[0])){
		
							if($chkLate[0]->approver ==  null){
								$colorLate  = 'orange';
							}else{
								
								$split_time_sch  	= explode(":",$query[$i]->fromx);
								$split_late			= explode(":",$chkLate[0]->late);

								$split_late_h = (integer)$split_late[0];
								$split_late_m = (integer)$split_late[1];
								$split_time_sch_h = (integer)$split_time_sch[0];
								$split_time_sch_m = (integer)$split_time_sch[1];

								if($split_late_h == 0){
									$tmp = $split_time_sch_m + $split_late_m;
									if($tmp > 60){
										$split_time_sch_h = $split_late_h + 1;
										$split_time_sch_m = $tmp-60;
									}else{
										$split_time_sch_m = $tmp;
									}
								}else{
									$tmp_h = $split_time_sch_h + $split_late_h;
									if($tmp_h >= 24){
										$split_time_sch_h = $tmp_h - 24;
									}else{
										$split_time_sch_h = $tmp_h;
									}
									
									$tmp_m = $split_time_sch_m + $split_late_m;
									if($tmp_m > 60){
										$split_time_sch_h = $split_late_h + 1;
										$split_time_sch_m = $tmp_m-60;
									}else{
										$split_time_sch_m = $tmp_m;
									}
								}

								if($split_time_sch_h < 10){
									$build = "0$split_time_sch_h:$split_time_sch_m:00";
								}else{
									$build = "$split_time_sch_h:$split_time_sch_m:00";
								}
								$date_com 		= $query[$i]->date;
								$time_device 	= $query[$i]->timeIn;

								$com_late = strtotime("$date_com $build");
								$com_device = strtotime("$date_com $time_device:00");

								if($com_device <= $com_late){
									$colorLate  	= 'green';
								}else{
									$colorLate  	= 'orange';
								}
								
							}
						}else{
							$colorLate  = 'green';
						}
							//if($value->date == "2018-05-01"){ return $query[$i]->Late; }
						 	$late = $query[$i]->Late;
						//}
					}

					if ($query[$i]->EarlyOut == null){ $earlyOut = "-";}
					else{ 

						// $a1 = new \DateTime("17:00:00.000");
						// $a2 = new \DateTime("18:55:00.000");
						// $b 	= date_diff($a2,$a1); 
						// $str =$b->format('%H:%I');
						
						$explode = explode(":",$query[$i]->EarlyOut);
						 	$earlyOut = strlen($explode[0]);
						 	if($earlyOut == 2 and $earlyOut != '00'){
						 		$ho = (int)$explode[0];
						 		$mi = (int)$explode[1];
						 		$ho = $ho * 60;
						 		$earlyOut = $ho + $mi ;
						 	}else if($earlyOut == 2 and $earlyOut == '00'){
						 		$mi = (int)$explode[1];
						 		$earlyOut =   $mi;
						 	}else{
						 		$earlyOut = "-";
						 	}
						if($earlyOut != '-'){
							//return var_dump($query[$i]);
						}
					}
					$h = substr($schedule, 6, strpos($schedule,":"));
					$m = substr($schedule, 9, strpos($schedule,10));
					//print_r($h."\n");
					$h1 = substr($timeOut, 0, strpos($timeOut,":"));
					$m1 = substr($timeOut, 3, strpos($timeOut,4));
					// create mark for the record that has been changed, yes for changed record
					if ($totalOver != null){
						if($makeOver == 'yes'){
							$overStatus = ( $overRest == "-" ?  "yes" : "no");
							if(isset($mass) && $mass == "yess"){
								$overStatus = "yes";
							}
							// $overStatus = "yes";
						}elseif($makeOver == "no") {
							$overStatus = "no";
						}
					}
					else
						$overStatus = "no";
						if($overRest !=  null && isset($query[$i]->RestDay_App)){
							if($query[$i]->RestDay_App == 2){
								$OTR_color =  'yes';	
							}else{
								$OTR_color =  'no';
							}
							
						}else{
							$OTR_color = "-";
						}


					// Compare time in and time out between attendance record and request
					$emp = $query[$i]->employee_id;
					$dt = $query[$i]->date;
					$_timeIN = $query[$i]->timeIn.':00';
					$_timeOUT = $query[$i]->timeOut.':00';

					// create mark for the record that has been changed, yes for changed record
					$tm = \DB::select("CALL View_TimeInOut_byDate_employee('$emp', '$dt')");
					$test[] = $dt;
					if ($tm != null) {
						if ($tm[0]->req_in != $_timeIN)
							$timeInStatus = "yes";
						else
							$timeInStatus = "no";

						if ($tm[0]->req_out != $_timeOUT)
							$timeOutStatus = "yes";
						else
							$timeOutStatus = "no";
					}
					else {
						$timeInStatus = "no";
						$timeOutStatus = "no";
					}
					if ($i == $c) {

						$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
						return $data;
					}else{
						if($totalOver != '-'){
						
							if($workHour != "0:00"){
								// if($query[$i]->date  ==   '2017-08-22'){
								// 		return $totalOver;
								// }

								$totalOvers =  $totalOver;
							}else{
								
								$totalOvers = '-';
							}

							// if($totalOvers ==  '00:00' || $workHour == "0:00"){
							// 	$totalOvers = '-';
							// }
						}else{
							$totalOvers = '-';
						}


						if($overRest != "-"){
							$short = '-';
							$late = '-';
							$earlyOut = '-';
						}

						if(strlen($schedule) ==  2){
							$short = '-';
							$late = '-';
							$earlyOut = '-';
							$totalOvers = '-';
							$overRest = '-';

						}

						$empx = $query[$i]->employee_id;
						$date_request = $query[$i]->date;
						$latest_modified  =  \DB::SELECT("select from_,to_,leave_code from leave_request,leave_type where employee_id  = '$empx' and leave_request.leave_type = leave_type.id and leave_code != '' and leave_request.status_id = 2");
						$latest_request  =  \DB::SELECT("select * from att_schedule_request as asr, att_training as atr where asr.status_id = 2 and asr.employee_id  = '$empx' and asr.request_id = atr.id and '$date_request' >= atr.start_ and '$date_request' <= atr.end and asr.type_id = 1");
						$latest_request1  =  \DB::SELECT("select * from att_schedule_request as asr, att_training  as atr where asr.status_id = 2 and asr.employee_id  = '$empx' and asr.type_id = 2 and asr.request_id  = atr.id and '$date_request' >= atr.start_ and '$date_request' <= atr.end limit 1");
						$terminated = \DB::SELECT("select * from emp_termination_reason where employee_id  = '$empx' ");
						//if($latest_request != null){
						//$continue_training  = \Db::SELECT("select * from ")
						
							if(isset($latest_request[0]->type_id)  ){
								$schedule =   'T';
							}
							if(isset($latest_request1[0]->type_id)  ){
								$schedule =   'OB';	
							}
						//}
						if($latest_modified != null){
							foreach ($latest_modified as $key => $value) {
								if($value->from_ <= $query[$i]->date && $query[$i]->date <= $value->to_){
									$schedule =   $value->leave_code;
								}	
							}
						}

						
						$holiday = \DB::SELECT("select * from attendance_holiday");
							foreach ($holiday as $key => $value) {
								$holiday_date  =  $query[$i]->date;
								if($holiday != null){
									if($holiday[$key]->repeat_annually == 1){
										if($holiday[$key]->day == null && $holiday[$key]->week == null){
											if($holiday[$key]->date == $holiday_date ){
												$schedule = 'H';
											}
										}
										if($holiday[$key]->day != null && $holiday[$key]->week != null){
											
											$holiday[$key];
											$year =  date('Y');
											$ass_hole =  explode('-',$holiday[$key]->date);
											$month_name  =  date("F", mktime(0, 0, 0,(intval($ass_hole[1])), 10));
											$ass_day =  $holiday[$key]->day;

											if($holiday[$key]->week == 'first'){
												$week_day =  '+1';	
											}elseif($holiday[$key]->week == 'second'){
												$week_day =  '+2';
											}elseif ( $holiday[$key]->week == 'third'){
												$week_day = '+3';
											}elseif($holiday[$key]->week == 'fourth'){
												$week_day = '+4';
											}else{
											  $week_day = '+4';
											}
											$get_new_ass_hole  = date('Y-m-d',strtotime("$week_day week $ass_day $month_name $year"));
											
											if($holiday_date == $get_new_ass_hole){
										 		$schedule = 'H';
												
										 	}
										}		
									}else{
										if($holiday[$key]->date == $query[$i]->date){
											$schedule = 'H';
										}
									}
								}
							}
						

						if($terminated != null){
							$schedule = 'TER';
						}			
						if(!isset($colorLate)){
							$colorLate = 'red';
						}

						if($schedule  == 'DO'){
							$ti =  strtotime($timeIn.':00');
							$to =  strtotime($timeOut.':00');

							$overRest  = date('H:i',$to - $ti);
							if($overRest == '00:00'){
								$overRest  =   '-';
							}
						}else{
							$overRest = '-';
						}

						if($workStatus ==  'no'and  $schedule == 'DO'){
							$OTR_color = 'yes';							
						}else{
							$OTR_color = 'no';
						}
						$date_c = $query[$i]->date;
						$emp_c  = $query[$i]->employee_id;

						// ORIGINAL
						/*if($earlyOut  != null){
							$emp_c  = $query[$i]->employee_id;
							$date_c = $query[$i]->date;  
							// print_r("select * from  att_late, att_schedule_request where  att_late.type_id = 7 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id and att_schedule_request.approver = 2");
							$early_c =  \DB::SELECT("select att_schedule_request.status_id from  att_late, att_schedule_request where  att_late.type_id = 8 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id");

							if($early_c != null){
								if($early_c[0]->status_id ==  1){
									$query[$i]->early_c  = "orange";
								}elseif($query[$i]->early_c ==  2){
									$query[$i]->early_c  = "green";
								}elseif($query[$i]->early_c ==  3){
									$query[$i]->early_c  = "red";
								}elseif($query[$i]->early_c ==  4){
									$query[$i]->early_c  = "purple";
								}else{
									//KUDOS
								}
								
							}else{
								$query[$i]->early_c =  'purple';
							}
						}else{
							$query[$i]->early_c =  'grey';
						}*/

						if($earlyOut  != null){
							$emp_c  = $query[$i]->employee_id;
							$date_c = $query[$i]->date;  
							// print_r("select * from  att_late, att_schedule_request where  att_late.type_id = 7 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id and att_schedule_request.approver = 2");
							$early_c =  \DB::SELECT("select att_schedule_request.status_id from  att_late, att_schedule_request where  att_late.type_id = 8 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id");

							if($early_c != null){
								$query[$i]->early_c =  'green';
							}else{
								$query[$i]->early_c =  'red';
							}
						}else{
							$query[$i]->early_c =  'red';
						}

						if(strlen($schedule) >  4){
							$turns =  explode('-',$schedule);
							if(!isset($turns[1])){
							  $workStatus  =  "no";
							}else{
							$base_date1 =  $turns[0].':00';
							$base_date2 =  $turns[1].':00';

							$substract_1 =  $timeIn.':00';
							$substract_2 =  $timeOut.':00'; 

						 	$get_tot_date1   = strtotime($base_date2) -  strtotime($base_date1);
							$get_tot_date2   = strtotime($substract_2) - strtotime($substract_1); 
							
								if($get_tot_date1 <= $get_tot_date2  || $get_tot_date1 == $get_tot_date2){
									$workStatus  =  "no";
								}
							}
						}	

						//check color  time  in   out 
					 	// par timeInStatusC  and timeOutStatusC
						
						//if($query[$i]->date  ===  '2017-09-10'){
							if($timeIn   ==   '-'){
								$date_timeIn   = $query[$i]->date;
								$date_timeIn_employee  =  $query[$i]->employee_id;
							
								$check_time_orange  = \DB::SELECT("select t2.status_id, t1.req_in from  att_time_in_out_req as t1,att_schedule_request 
																	as t2,pool_request as t3 
																	where t1.date  =   '$date_timeIn'
																	and   t1.req_in_out_id =  t2.request_id and  t1.type_id = t2.type_id
																	and   t2.id = t3.id_req  and  t2.type_id   =  t3.type_id and t3.master_type = 1
																	and  t1.employee_id = '$date_timeIn_employee' order  by  t1.req_in_out_id DESC limit 1 ");

								// OLD
								// if($check_time_orange  != null){
								// 	///return  $check_time_orange;
								// 	if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  ==  1){
								// 			$timeInStatusC = 'orange';
								// 			$timeIn  =   substr($check_time_orange[0]->req_in,0,5); 
								// 	}elseif($check_time_orange[0]->status_id  ==  4){
								// 		$timeInStatusC = 'purple';
								// 	}elseif($check_time_orange[0]->status_id  ==  3){
								// 		$timeInStatusC = 'red';
								// 	}elseif($check_time_orange[0]->status_id  ==  2){
								// 		$timeInStatusC = 'green';
								// 	}else{
								// 		// ASS HOLE ALWAYS CHANGE 
								// 	}
								// }else{
								// 	$timeInStatusC = 'purple';
								// }
								if($check_time_orange  != null){
									///return  $check_time_orange;
									if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  ==  1){
										
											$timeInStatusC = 'orange';
											$timeIn  =   substr($check_time_orange[0]->req_in,0,5); 
									}else{
										$timeInStatusC = 'green';
									}
								}else{
									$timeInStatusC = 'red';
								}
							}else{ $timeInStatusC = 'green'; }
						//}

						if($timeOut   ==   '-'){
							$date_timeOut   = $query[$i]->date;
							$date_timeOut_employee  =  $query[$i]->employee_id;
							$check_time_orange  = \DB::SELECT("select t2.status_id,t1.req_out from  att_time_in_out_req as t1,att_schedule_request 
																as t2,pool_request as t3 
																where t1.date  =   '$date_timeOut'
																and   t1.req_in_out_id =  t2.request_id and  t1.type_id = t2.type_id
																and   t2.id = t3.id_req  and  t2.type_id   =  t3.type_id and t3.master_type = 1
																and  t1.employee_id = '$date_timeOut_employee' order  by  t1.req_in_out_id DESC limit 1 ");

							//OLD
							// if($check_time_orange  != null){
							// 	if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  == 1){
							// 			$timeOutStatusC = 'orange';
							// 			$timeOut  =   substr($check_time_orange[0]->req_out,0,5); 
							// 	}elseif($check_time_orange[0]->status_id  ==  4){
							// 			$timeInStatusC = 'purple';
							// 	}elseif($check_time_orange[0]->status_id  ==  3){
							// 			$timeInStatusC = 'red';
							// 	}elseif($check_time_orange[0]->status_id  ==  2){
							// 			$timeInStatusC = 'green';
							// 	}else{
							// 			// ASS HOLE ALWAYS CHANGE 
							// 	}
							// }else{
							// 	$timeOutStatusC = 'purple';
							// }
							if($check_time_orange  != null){
								if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  == 1){
										$timeOutStatusC = 'orange';
										$timeOut  =   substr($check_time_orange[0]->req_out,0,5); 
								}else{
									$timeOutStatusC = 'green';
								}
							}else{
								$timeOutStatusC = 'red';
							}
						}else{ $timeOutStatusC = 'green'; }

						//check_color_orange 

						//check colcor late
						// if($colorLate == 'red'){
						// 	if($query[$i]->date ==  '2017-03-05'){
						
						// 		$date_late = $query[$i]->date;
						// 		$nameX  =  $query[$i]->employee_id;
						// 		$chkLate  = \DB::SELECT("select * from att_schedule_request where employee_id  = '$nameX' and availment_date = '$date_late' and type_id  = 7 and  status_id  = 1 order by id desc  limit 1");

						// 		if($chkLate  !=  null){
						// 			//if($chkLate[0]->approver  ==   null){
						// 				$colorLate  = 'orange';
						// 			//}
						// 		}
						// 	}
						// }

						//check color early  out   
						// if($query[$i]->early_c =  'red'){
						// $early_c =  \DB::SELECT("select * from  att_late, att_schedule_request where  att_late.type_id = 8 and date  = '$date_c' and 	att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id and att_schedule_request.status_id = 1");

						// 	if($early_c != null){
						// 		$query[$i]->early_c =  'orange';
						// 	}
						// }

						//set color to  oramg  for  overtime and overtime dso
						//if  already input for  overtime  rest day parameter $OTR_color
						//If  already  input for  overtime   no  do    parameter $overtime_non_do


						//return $totalOvers;
						if($totalOvers != null){
							$date_overtime  =  $query[$i]->date;
							$employee_overtime = $query[$i]->employee_id ;

							$search  = \DB::SELECT("select * from att_overtime as t1, att_schedule_request as t2
													where t1.employee_id  =  '$employee_overtime' 
													and  t1.date_str =  '$date_overtime' 
													and  t1.type_id =  t2.type_id  
													and  t1.id = t2.request_id
													order by t1.id desc limit 1 
													");	
							// if($search  != null){
							// 	if($search[0]->status_id == 1){
							// 		$overtime_non_do  =  'orange';
							// 	}elseif($search[0]->status_id == 2){
							// 		$overtime_non_do  =  'green';
							// 	}elseif($search[0]->status_id == 3){
							// 		$overtime_non_do  =  'red';
							// 	}elseif($search[0]->status_id == 4){
							// 		$overtime_non_do  =  'purple';
							// 	}else{
							// 		//
							// 	}


							// }else{
							// 	$overtime_non_do =   'purple';
							// }
							if($search  != null){
								if($search[0]->status_id == 1){
									$overtime_non_do  =  'orange';
								}else{
									$overtime_non_do  =  'green';
								}


							}else{
									 $overtime_non_do =   'red';
							}


						}else{
							$overtime_non_do = 'red';
						}

						if($overRest != null){
							$date_overtime  =  $query[$i]->date;
							$employee_overtime = $query[$i]->employee_id ;

							$search  = \DB::SELECT("select * from att_overtime as t1, att_schedule_request as t2
													where t1.employee_id  =  '$employee_overtime' 
													and  t1.date_str =  '$date_overtime' 
													and  t1.type_id =  t2.type_id  
													and  t1.id = t2.request_id
													order by t1.id desc limit 1 
													");	
							// if($search  != null){
							// 	if($search[0]->status_id == 1){
							// 		$OTR_color  =  'orange';
							// 	}elseif($search[0]->status_id == 2){
							// 		$OTR_color  =  'green';
							// 	}elseif($search[0]->status_id == 3){
							// 		$OTR_color  =  'red';
							// 	}elseif($search[0]->status_id == 4){
							// 		$OTR_color  =  'purple';
							// 	}else{
							// 		// KUDOS
							// 	}


							// }else{
							// 		$OTR_color =   'purple';
							// }
							if($search  != null){
								if($search[0]->status_id == 1){
									$OTR_color  =  'orange';
								}else{
									$OTR_color  =  'green';
								}


							}else{
									$OTR_color =   'red';
							}

						}else{
							$OTR_color  = 'red';
						}

						//check  changeshift
						if(isset($query[$i]->date) ){
							// if($query[$i]->date ==  '2017-09-14'){
							// $date_changeshift  =  $query[$i]->date;
							// $employee_changeshift = $query[$i]->employee_id ;

							// return $check_exist_change_shift   =  \DB::SELECT("select * from  att_change_shift as t1 ,  att_schedule_request  as t2,
							// 											attendance_work_shifts as  t3
							// 										    where t1.employee_id = '$employee_changeshift' and  date = '$date_changeshift'
							// 										    and  t1.type_id  =  t2.type_id 
							// 										    and  t1.id  = t2.request_id  
							// 										    and  t1.new_shift  =  t3.shift_id
							// 										    and t2.status_id  =  2 order  by  t1.id  desc limit 1");

							// return   $check_exist_change_shift;

							// }
							$date_changeshift  =  $query[$i]->date;
							$employee_changeshift = $query[$i]->employee_id ;

							
							$check_exist_change_shift   =  \DB::SELECT("select * from  att_change_shift as t1 ,  att_schedule_request  as t2,
																		attendance_work_shifts as  t3
																	    where t1.employee_id = '$employee_changeshift' and  date = '$date_changeshift'
																	    and  t1.type_id  =  t2.type_id 
																	    and  t1.id  = t2.request_id  
																	    and  t1.new_shift  =  t3.shift_id
																	    and t2.status_id  =  2 order  by  t1.id  desc limit 1");
							if($check_exist_change_shift != null){
								$list_code = ['DO','ADO','VL','BL','TB','SL','EL','BE','ML','PL','MA','OB',];



								if(in_array($check_exist_change_shift[0]->shift_code,$list_code)){
									$schedule =  $check_exist_change_shift[0]->shift_code;
									if($query[$i]->date == "2017-09-09" && $query[$i]->employee_id == "2015037"){
										return [$schedule,(array)$query[$i],"TEST1"];
									}
								}else{
									$schedule  =  substr($check_exist_change_shift[0]->_from,0,5).'-'. substr($check_exist_change_shift[0]->_to,0,5);
									if($query[$i]->date == "2017-09-09" && $query[$i]->employee_id == "2015037"){
										return [$schedule,(array)$query[$i],"TEST2"];
									}
								}
							}

						}

						$attendance =  \Input::get('attendance');
						$explode =  explode('-',$attendance);

						$explode1 = explode('/',$explode[0]);


						$exp_sch  =  explode('-', $schedule);
						// if(!isset($exp_sch[1]) || $query[$i]->Late  ==  0 ){
							
						// 	$query[$i]->Late  = '-';
						// }			

						try{
							if($query[$i]->training_req){
								$schedule = 'T';
								$late = '-';
							}

							if($query[$i]->OvertimeRestDay != '99:99:99'){
								$overRest = $query[$i]->OvertimeRestDay;
							}
							
						}catch(\Exception $e){
							return [$e,$query[$i]];
						}
						
					
						// if($query[$i]->date == "2017-09-09" && $query[$i]->employee_id == "2015037"){
						// 				return [$schedule,(array)$query[$i],"TEST2"];
						// 			}
						if($pointer == 0){

							//$get_date_basic  =   explode('-',$query[$i]->date);

							//if($explode1[2]  ==  $get_date_basic[2] ){
								$temp[] = [
									'early_c' => $query[$i]->early_c,   
									'date' => $query[$i]->date,
									'colorLate' => $colorLate,
									'employee_id' => $query[$i]->employee_id,
									'Name' => $query[$i]->Name,
									'schedule' => $schedule,
									'timeIn' => $timeIn,
									'timeOut' => $timeOut,
									'noTimeIn' => $noTimeInStatus,
									'noTimeOut' => "yes",//$noTimeOutStatus,
									'WorkHours' => $workHour,
									'workStatus' => $workStatus,
									'total_overtime' => $totalOvers,
									'new_color_overtime' => $overtime_non_do,
									'overStatus' => $overStatus,
									'status' => $status,
									'OvertimeRestDay' => $overRest,
									'OvertimeRestDay_Color' => $OTR_color,
									'timeOutStatus' => $timeOutStatus,
									'timeInStatus' => $timeInStatus,
									'C_TimeIn' => $timeInStatusC,
									'C_TimeOut' => $timeOutStatusC,
									'Short' => $short,
									'Late' => /*$query[$i]->Late*/ $late,
									'EarlyOut' =>  ($earlyOut == 0 ? $earlyOut = '-' : $earlyOut = $earlyOut  ),
									'between' => $between
								];
							//}
						}else{
							
							//$get_date_basic  =   explode('-',$query[$i]->date);

							//if($pointer  ==  $get_date_basic[2] ){
								$temp[] = [
									'early_c' => $query[$i]->early_c,   
									'date' => $query[$i]->date,
									'colorLate' => $colorLate,
									'employee_id' => $query[$i]->employee_id,
									'Name' => $query[$i]->Name,
									'schedule' => $schedule,
									'timeIn' => $timeIn,
									'timeOut' => $timeOut,
									'noTimeIn' => $noTimeInStatus,
									'noTimeOut' => "yes",//$noTimeOutStatus,
									'WorkHours' => $workHour,
									'workStatus' => $workStatus,
									'total_overtime' => $totalOvers,
									'new_color_overtime' => $overtime_non_do,
									'overStatus' => $overStatus,
									'status' => $status,
									'OvertimeRestDay' => $overRest,
									'OvertimeRestDay_Color' => $OTR_color,
									'timeOutStatus' => $timeOutStatus,
									'timeInStatus' => $timeInStatus,
									'C_TimeIn' => $timeInStatusC,
									'C_TimeOut' => $timeOutStatusC,
									'Short' => $short,
									'Late' => $query[$i]->Late,
									'EarlyOut' =>  ($earlyOut == 0 ? $earlyOut = '-' : $earlyOut = $earlyOut  ),
									'between' => $between
								];
							//}
						}

						


					
						$i++;
					}
				}
			}

		
			// $count_temp  =  count($queryx);

			// if($count_temp  > 9){
			// 	$mod  =  $count_temp   % 10;
			// 	if($mod  != 0){
			// 		$get_paging  =  ($count_temp -  $mod) / 10;
			// 		$get_paging  += 1;   
			// 	}else{
			// 		$get_paging  =  $count_temp  / 10;
			// 	} 'paging' => $get_paging
			// }


			$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
		}
		else{
			$data = ['data'=>[], 'status'=>500, 'message'=>'Record data is empty'];
		}
		return $data;
		}
	}

	// public function cuttingOff(){
	// 	return 1;
	// }

}



