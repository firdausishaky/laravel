<?php namespace Larasite\Http\Controllers\Attendance\CutOff;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Model\Attendance\cutOff\Attendance_cutoffrecordModel;
use Larasite\Library\calculation;
use DateTime;
use DatePeriod;
use DateInterval;

class Attendance_CutOff extends Controller {

	protected $form = 74;

	public function get_cutoff_period()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
				return $att_record-> getMessageAccess("Unauthorized",500,[],$access[3]);
			}else{
					 
					 $emp = \Request::get('employee_id');
					 $department_id = \Request::get('department_id');
					 $role_id = \Request::get('role_id');
					 if(!$department_id || !$emp){
					 	return \Response::json(['header'=>['message'=>"data not found",'status'=>500],'data'=>null],500);
					 }

					  $data_ex = \DB::SELECT("CALL cutoff_date_period__GET('$emp',$department_id)");
		
					  $periode =[];
					  if($data_ex != null){
						foreach ($data_ex as $key => $value) {
						  $date1 = explode("-",$value->from_);
						  $date2 = explode("-",$value->to_);
						  $periode[] = ["periode" => $date1[0].'/'.$date1[1].'/'.$date1[2].'-'.$date2[0].'/'.$date2[1].'/'.$date2[2]]; 
						}
					  }

						//return $periode;
						return \Response::json(['header'=>['message'=>"View Record.",'status'=>200, 'access'=>$access[3]],'data'=>$periode],200);
					  
				  }

		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

	}

	public function periode_date_by_month()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
				return $att_record-> getMessageAccess("Unauthorized",500,[],$access[3]);
			}else{
					 $data_ex = \DB::SELECT("CALL cutoff_periode_date__GET()");

					return \Response::json(['header'=>['message'=>"View Record.",'status'=>200, 'access'=>$access[3]],'data'=>$data_ex],200);
					  
				  }

		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

	}

	public function cutoff_by_month()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
				return $att_record-> getMessageAccess("Unauthorized",500,[],$access[3]);
			}else{
				$emp_id = \Input::get('employee');
				$dep_id = \Input::get('department');
				$job_id = \Input::get('job');
				$month = \Input::get('month');
				$year = \Input::get('year');
				$per_cutoff = \Input::get('cutoff_date');

				$extra_q = "";

				if($month){
					$extra_q .= "month = $month ";
					
					if($year){
						$extra_q .= " and year = $year ";
					}				
				}

				if(!$month && $year){
					$extra_q .= " year = $year ";
				}

				if($per_cutoff){
					$extra_q .= $per_cutoff; 					
				}
				if($emp_id && $emp_id !=0){
					$extra_q .= " and employee_id = '$emp_id' ";
				}
				if($dep_id && $dep_id != 0){
					$extra_q .= " and department_id = '$dep_id' ";
				}
				if($job_id && $job_id != 0){
					$extra_q .= " and job_id = '$job_id' ";
				}	
					//$t = "select * from cutoff_data_by_month where ".$extra_q;
					//return $t;
					 $data_ex = \DB::SELECT("select * from cutoff_data_by_month where ".$extra_q);

					return \Response::json(['header'=>['message'=>"View Record.",'status'=>200, 'access'=>$access[3]],'data'=>$data_ex],200);
					  
				  }

		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

	}

	public function view_cutoff_month_years()
	{
		// return 1;
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
				return $att_record->getMessageAccess("Unauthorized",500,[],$access[3]);
			}else{
				$y = []; $m = []; $payout = [];
				$q = \DB::SELECT('call view_cutoff_month_years()');
				if(count($q)>0){
					foreach ($q as $key => $value) {

						$arr = explode('-',$value->years_month);

						if($y){
							$tmp_y = in_array($arr[0],$y);
							if(!$tmp_y){ $y[] = $arr[0]; }
						}else{ $y[] = $arr[0]; }

						if($m){
							$tmp_m = in_array($arr[1],$m);
							//$tmp_m = array_search($arr[1],$m);
							if(!$tmp_m){ $m[] = $arr[1]; }
						}else{ $m[] = $arr[1];	 }

						$payout[] = $value->start.' - '.$value->end;
					}
					$data = ["year"=>$y,"month"=>$m,"payout"=>$payout];
					return \Response::json(['header'=>['message'=>"View Record.",'status'=>200, 'access'=>$access[3]],'data'=>$data],200);

				}else{
					return \Response::json(['header'=>['message'=>"data is empty",'status'=>200, 'access'=>$access[3]],'data'=>null],200);				
				}
			}
		}else{
			return \Response::json(['header'=>['message'=>"data is empty",'status'=>200, 'access'=>$access[3]],'data'=>null],200);				
		}

	}

	public function view_emp_terminate()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
				return $att_record-> getMessageAccess("Unauthorized",500,[],$access[3]);
			}else{

			// 12 = resign
			$q = \DB::SELECT('call view_emp_resign();');
			$data = $q;
			if(count($q) > 0){
				return \Response::json(['header'=>['message'=>"View Record.",'status'=>200, 'access'=>$access[3]],'data'=>$q],200);
			}
				return \Response::json(['header'=>['message'=>"Data is empty.",'status'=>200, 'access'=>$access[3]],'data'=>null],200); 
			}

		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

	}



   // $periode = \DB::SELECT("select payroll_period as periode from att_cutoff_period");
   //         $department = \DB::SELECT("select * from department where id<>1");
   //         $department_ex = array_merge($department,[["id" => 0, "name" => "All"]]);
   //         $job = \DB::SELECT("select id,title from job");
   //         $job_ex = array_merge($job,[["id" => 0, "title" => "All"]]);
   //         $data = ["department" => $department_ex, "job" => $job_ex, "period" => $periode];
   //         return $att_record->indexAcess($data,$access[3]);
	public function generate(){
	  /*
	  // $phpWord = new \PhpOffice\PhpWord\PhpWord();

	  // $center = \PhpOffice\PhpWord\SimpleType\JcTable::CENTER;

	  // $section = $phpWord->addSection();
	  // $fancyTableStyleName = 'Fancy Table';
	  // $fancyTableStyle = array('borderSize' => 6, 'borderColor' => '000000', 'cellMargin' => 80, 'alignment' => $center);
	  // $footerTableStyle = array('borderSize' => 0, 'borderColor' => 'ffffff', 'cellMargin' => 80, 'alignment' => $center, 'valign'=>'center');
	  // $fancyTableFirstRowStyle = array('borderBottomSize' => 18, 'borderBottomColor' => '0000FF', 'bgColor' => '66BBFF');
	  // $fancyTableCellStyle = array('valign' => 'center');
	  // $fancyTableCellBtlrStyle = array('valign' => 'center', 'textDirection' => \PhpOffice\PhpWord\Style\Cell::TEXT_DIR_BTLR);
	  // $fancyTableFontStyle = array('bold' => true);
	  
	  // $fontStyle = new \PhpOffice\PhpWord\Style\Font();
	  // $fontStyle->setBold(true);
	  // $fontStyle->setName('Tahoma');
	  // $fontStyle->setSize(13);
	  // $fontStyle->setParagraph(['alignment'=>'center']);
	  // $Y = Date('Y');
	  
	  // $section->addText("LEEKIE ENTERPRISES INC \n SCHEDULE OF ATTENDANCE CUT-OFFS \n PAYROLL PERIOD COVERING JANUARY - DECEMBER $Y ",
	  //   ['bold'=>true],['alignment'=>'center']);
	  // // $T2 = $section->addText("SCHEDULE OF ATTENDANCE CUT-OFFS");
	  // // $T3->setFontStyle($fontStyle);

	  // $phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle, $fancyTableFirstRowStyle);
	  // $table = $section->addTable($fancyTableStyleName);


	  // $table->addRow();
	  // $table->addCell(3000, $fancyTableCellStyle)->addText('PAY-OUT-DATE', $fancyTableFontStyle);
	  // $table->addCell(3000, $fancyTableCellStyle)->addText('PAYROLL PERIOD', $fancyTableFontStyle);
	  // $table->addCell(3000, $fancyTableCellStyle)->addText('ATTENDANCE CUT-OFF', $fancyTableFontStyle);

	  // $data = \Input::get('data');
	  // foreach ($data as $key) {
	  //   $explode_payroll = explode("-", $key['payroll_period']);
	  //   $payroll = $explode_payroll[1]."/".$explode_payroll[2]."-".$explode_payroll[5]."/".$explode_payroll[3];
	  //   $table->addRow();
	  //     $table->addCell(3000)->addText("$key[payout_date]");
	  //     $table->addCell(3000)->addText("$payroll");
	  //     $table->addCell(3000)->addText("$key[dateTo]");
	  // }
	  // $table->addRow();
	  // $table->addCell(3000,$footerTableStyle)->addText("");
	  // $table->addCell(3000,$footerTableStyle)->addText("");
	  // $table->addCell(3000,$footerTableStyle)->addText("");
	  // $table->addRow();
	  // $table->addCell(3000,$footerTableStyle)->addText("CHECKED BY : ");
	  // $table->addCell(3000,$footerTableStyle)->addText("");
	  // $table->addCell(3000,$footerTableStyle)->addText("NOTED BY : ");
	  

	  //   // Saving the document as OOXML file...
	  //     $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
	  //     $path = storage_path("word_cutoff.docx");
	  //     $objWriter->save($path);
	  //     $file = \File::get($path);$type = \File::mimeType($path);
	  //     return \Response::make($file,200,['Content-Type'=>$type]);
	*/
	}
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
				return $att_record-> getMessageAccess("Unauthorized",500,[],$access[3]);
			}else{
				  // $first_date = \DB::SELECT("select date from att_schedule order by date asc limit 1");
				  //   $last_date = \DB::SELECT("select date from att_schedule order by date desc limit 1");
				  //   if($first_date != null && $last_date != null){
				  //   $first = $first_date[0]->date;
				  //   $last = $last_date[0]->date;

				  //   $step = '+9 day';
				  //       $format = 'Y/m/d';
				  //       $dates = array();
				  //       $current = strtotime( $first );
				  //       $last = strtotime( $last );

				  //        while( $current <= $last ) {

				  //           $dates[] = date( $format, $current );
				  //           $current = strtotime( $step, $current );
				  //        }

				  //       $dante = [];

				  //       $count = count($dates);
				  //       for($i = 1; $i < $count; $i++){
				  //           if($i % 2 == 0){
				  //               $dante[] = $dates[$i];
				  //           }else{
				  //               $dantex[] = $dates[$i];
				  //           }
				  //       }

				  //       $count1 = count($dante)-1;
				  //      $date_ex = "";
				  //      for($i = 0; $i < $count1; $i++){
				  //           $date_ex .= ",".$dantex[$i]." - ".$dante[$i];
				  //      }
				  //       $trimmed = ltrim($date_ex, ",");
				  //      $explode = explode(",", $trimmed);
				  //      foreach ($explode as $key => $value) {
				  //       $periode[] = ["periode" => $value];
					 
					  $data_ex = \DB::SELECT("select distinct start_ as att_cutoff_from , from_ as att_cutoff_to from cutoff_date  ");
					  // $data_ex = \DB::SELECT("select att_cutoff_from,att_cutoff_to from att_cutoff_period where att_cutoff_from != '0000-00-00'
					  //    and att_cutoff_to != '0000-00-00' ");
		
					  $periode =[];
					  if($data_ex != null){
						foreach ($data_ex as $key => $value) {
						  $date1 = explode("-",$value->att_cutoff_from);
						  $date2 = explode("-",$value->att_cutoff_to);
						  $periode[] = ["periode" => $date1[0].'/'.$date1[1].'/'.$date1[2].'-'.$date2[0].'/'.$date2[1].'/'.$date2[2]]; 
						}
					  }

						//return $periode;
						return \Response::json(['header'=>['message'=>"View Record.",'status'=>200, 'access'=>$access[3]],'data'=>$periode],200);
					  
				  }
			   // $data = \DB::SELECT("select payroll_period as periode from att_cutoff_period");
			//     if($periode != null ){
			//         $data =  $att_record->indexAcess($periode,$access[3]);
			//     }else{
			//          // $data = [];
			//         $data =  $att_record->indexAcess($periode,$access[3]);
			//     }
			//     return \Response::json(['header'=>['message'=>"View Record.",'status'=>200, 'access'=>$access[3]],'data'=>$data],200);

			

		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

	}


		public function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) {
				$dates = array();
				$current = strtotime($first);
				$last = strtotime($last);

			while( $current <= $last ) {
				$dates[] = date($output_format, $current);
			}
			return $dates;
		}

	public function GetDays($sStartDate, $sEndDate){
	  // Firstly, format the provided dates.
	  // This function works best with YYYY-MM-DD
	  // but other date formats will work thanks
	  // to strtotime().
	  $sStartDate = gmdate("Y-m-d", strtotime($sStartDate));
	  $sEndDate = gmdate("Y-m-d", strtotime($sEndDate));

	  // Start the variable off with the start date
	 $aDays[] = $sStartDate;

	 // Set a 'temp' variable, sCurrentDate, with
	 // the start date - before beginning the loop
	 $sCurrentDate = $sStartDate;

	 // While the current date is less than the end date
	 while($sCurrentDate < $sEndDate){
	   // Add a day to the current date
	   $sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));

	   // Add this new day to the aDays array
	   $aDays[] = $sCurrentDate;
	 }

	 // Once the loop has finished, return the
	 // array of days.
	 return $aDays;
   }
	public function view(){
	  
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){

			$att_record = new  Attendance_cutoffrecordModel;
			$cutOff = new Attendance_cutoffrecordModel;
			$LIB_ATT = new calculation;

			$key = \Input::get("key");
			$encode = base64_decode($key);
			$explode = explode("-",$encode);
			$token = $explode[1];

			//return model

			$attendance  = \Input::get('attendance');
			$job =  \Input::get('job');
			$department =  \Input::get('department');
			$localit =  \Input::get('radio');

			// DECLARE VALIDASI
			$reg = [
			'num'=>'Regex:/^[0-9-\! -\^]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];

			$rule = [
				"attendance" =>"required|".$reg['num']."|max:21",
				"department" => "required|numeric",
				"job" => "numeric",
				"radio" => "numeric"
			];

			$valid = \Validator::make(\Input::all(),$rule);

			if($valid->fails()){
				$message='Required Input.'; $status=500; $data=null;
				return response()->json(['header'=>['message' => 'data not found', 'status' => 500],'data' => []],500);
			}

			$explode = explode("-",$attendance);
			$date1 = $explode[0];
			$date2 = $explode[1];

			$data3a = str_replace("/", "-", $date1);
			$data3b = str_replace("/", "-", $date2);
			$checks = \DB::select("select id from cutoff_date where start_ = '$data3a' and from_ = '$data3b' and department_id = $department");

			if(count($checks) == 0){
				return response()->json(['header'=>['message' => 'data not found', 'status' => 200],'data' => []],200);	
			}
			//call procedure
			//$data = \DB::SELECT("CALL view_cutoff_record('$date1','$date2') ");

		   /* if($localit == 3){
				$localit = '1,2,3';
			}else if($localit == 2){
				$localit =  '2,3';
			}else{
				$localit = '1,2';
			}*/
			
			if(isset($localit) and  $localit != null)
			{
			  $radio  =   $localit;
			  if($radio  == 1){
				$query_radio   = " and  t2.local_it = 1 ";
			  }

			  if($radio  == 2){
				$query_radio   =  " and  t2.local_it in (2,3) ";
			  }

			  if($radio  == 3){
				$query_radio  = " and  t2.local_it in (1,2,3) ";
			  } 

			}else{
			  $query_radio  = " and  t2.local_it in (1,2,3) ";
			}

			 if(isset($department) and $department != null){
			  $query_department  =   " and t2.department = $department and t1.employee_id IN(SELECT SUBSTR(ct.cutoff_per_payroll,17,7) AS emp_id FROM cutoff_date ct where ct.start_='$data3a' AND ct.from_='$data3b' AND ct.department_id = '$department' ) ";
			}else{
			  $query_department = null;
			}

			if(isset($job) and $job != null)
			{
			  $query_job  =   "and  t11.job = $job ";
			}else{
			  $query_job = null;
			}
			
			$period = new DatePeriod(
			 new DateTime($date1),
			 new DateInterval('P1D'),
			 new DateTime($date2)
			);

			$rangeDt = "";
			foreach ($period as $keys => $values) {
			  if(strlen($rangeDt) == 0){
				$rangeDt = "'".$values->format('Y-m-d')."'";
			  }else{
				$rangeDt .= ",'".$values->format('Y-m-d')."'";
			  }
			  //$value->format('Y-m-d')       
			}

			$data2a = str_replace("/", "-", $date2);
			$rangeDt .= ", '".$data2a."'";
			

			$data = $LIB_ATT->att('cut-off',$rangeDt,$query_job,$query_department,$query_radio);


			if(isset($data['status']) && $data['status'] == 200){
			  $data = $data;
			}else{
			  return $data;
			}
			//$data = $this->getDetail($date1,$date2,$job,$department,$localit);
			
			// check if procedure
			if($data == null){
				  return $att_record-> getMessageAccess("Data not exist",200,[],$access[3]);
			}

			//declare variable
			$tar = [];
			$abs = [];
			$nd = [];
			$rot = [];
			$rotdo = [];
			$employee_ids = [];

			//loop data employee in one array
			
			if(!isset($data['data']) || $data['data']  == null ){
				return $att_record->getMessageAccess("Data not exist",200,[],$access[3]);
			}

			foreach ($data['data'] as $key => $value) {
			 
				$employee_ids[$value['employee_id']] = $value['employee_id'];
			}

			//loop for get employee id
			foreach ($employee_ids as $key => $value) {
				$data_emp[] = $key;
			}

			$range = $this->GetDays($date1,$date2);
			//$range = $this->GetDays("2017/03/30","2017/03/31");

			//create dictionary array alphabet
			$dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];
			$count = count($data_emp);

			//foreach  last_name, first_name,date, date & shift_code, tar, abs, nd,rot,rotdo
			//return  $data['data'];
			$result = [];
			$test = [];
			$range_field = [];
			//return $employee_ids;
			for($x = 0; $x < $count; $x++){
				foreach($data['data'] as $key => $value){
					/*   if($value['date'] == '2018-10-24' && $value['employee_id'] == '2018045'){
						return $value;
					}*/
				  if(isset($data_emp[$x])){
					  if($data_emp[$x] == $value['employee_id']){
					  $counting_late = 0;
					  $counting_leave =  0;
					  $counting_night = 0;
					  $counting_over  = 0;
					  $counting_rdo = 0;
					  $result[$value['employee_id']]['employee_id']= $value['employee_id'];
					  $result[$value['employee_id']]['first_name'] = $value['first_name'];
					  $result[$value['employee_id']]['last_name'] =  $value['last_name'];
					  
					  $counts = count($range);
						for ($y=0; $y < $counts ; $y++) { 

							if($value['date'] == $range[$y]){
							  //return $range[$y];
								// testing
								// if($value['date'] == '2018-10-24' && $value['employee_id'] == '2018045'){
								// 	$value['timeOut'] = "23:00";

								// 	$split_sch = explode("-",$value['schedule']);
								// 	$sch_out = date("a",strtotime($split_sch[1]));
								// 	$sch_in = date("a",strtotime($split_sch[0]));

								// 	$nextday_sch = false;
								// 	if($sch_in == 'pm' && $sch_in == 'am'){ $nextday_sch = true; }

								// 	if($value['nextday']){
																				
								// 	}

								// }


							  if($value['Late'] != '-'&& $value['colorLate'] != 'green'){
								  
								  if(!strchr($value['Late'],':')){
									$late1 = $value['Late'];
								  }else{
									$late = explode(':',$value['Late']);
									$late1 = (intval($late[0]) * 60) + intval($late[1]);  
								  }
								  
								  if(isset($result[$value['employee_id']]['tar']) && $result[$value['employee_id']]['tar'] != 0){
									   $result[$value['employee_id']]['tar'] = intval($result[$value['employee_id']]['tar']) + $late1;
								  }else if(isset($result[$value['employee_id']]['tar']) && $result[$value['employee_id']]['tar'] == 0){
										$result[$value['employee_id']]['tar'] = $late1;
								  }else{
									$result[$value['employee_id']]['tar'] = $late1;
								  }  
							  }else{
								if(isset($result[$value['employee_id']]['tar']) && $result[$value['employee_id']]['tar'] != 0){
								  $result[$value['employee_id']]['tar'] = intval($result[$value['employee_id']]['tar']) + 0;
								 }else if(isset($result[$value['employee_id']]['tar']) && $result[$value['employee_id']]['tar'] == 0){
										$result[$value['employee_id']]['tar'] = 0;
								}else{
								  $result[$value['employee_id']]['tar'] = 0;
								}
							  }
							  if($value['Late'] != '-' && $value['colorLate'] != 'green'){
								  
								  if(!strchr($value['Late'],':')){
									$late = $value['Late'];
								  }else{
									$late = explode(':',$value['Late']);
									$late = (intval($late[0]) * 60) + intval($late[1]);  
								  }
								  /*$explode_late  =  explode(':',$value['Late']);
								  $arg1 = intval($explode_late[0]) * 60;
								  $arg2 = intval($explode_late[1]);
								  $late =  intval($arg1) + intval($arg2);   */
							  }else{
								$late = 0;
							  }

							  if($value['Short'] != '-'){
								  $short = /*intval($value['Short'])*/ 0 ;
								  //$result[$value['employee_id']]['tar'] += $short;
							  }else{
								$short = 0;
							  }
							  if($value['EarlyOut'] != '-' && $value['early_c'] != 'green'){
								  $result[$value['employee_id']]['tar'] += intval($value['EarlyOut']);
								  $early = intval($value['EarlyOut']);
							  }else{
								$early = 0;
							  }
							  
							  
							  if($late || $value['EarlyOut']){

								//$total = $late + $short.'m';
								// if($value['employee_id'] == '2016015'){

								//   return $value;
								// }
								$total = $late + $early;
								if($total == null || $total <= 0 ){
								  $total = '/';
								}else{
								  $total = $total."m";
								}
							  }else{
								$total = '/';
							  }

							  if(strlen($value['schedule']) <= 3){
								  $total = $value['schedule']; 
							  }
 
							  // if($value['schedule'] == 'DO'){
							  //   $total = 'DO';
							  // }
							  // if($value['schedule'] == 'BL'){
							  //   $total = 'BL';
							  // }
							  // if($value['schedule'] == 'VL'){
							  //   $total = 'VL';
							  // }
							  // if($value['schedule'] == 'EL'){
							  //   $total = 'EL';
							  // }


							  $tmp = ['el','vl','sl','bl','do','ado','ml','pl','sus','be','ma','t','bo'];
							  if(!isset($result[$value['employee_id']]['abs'])){
							  	$result[$value['employee_id']]['abs']=0;
							  }
							 //  if( $value['timeIn'] ==  '-' && $value['timeOut'] == '-' ){
								//   if($total == 'EL' || $total == 'VL' || $total == 'SL' || $total == 'ML' || $total == 'PL' /*|| $total == 'BL'*/ || $total == 'ML' || $total == 'SUS' || $total == 'BE' || $total == 'MA' || $total == 'DO'){
								// 	if(isset($result[$value['employee_id']]['abs']) && $result[$value['employee_id']]['abs'] != 0){
								// 	  $result[$value['employee_id']]['abs'] = intval($result[$value['employee_id']]['abs']) + 1;
								// 	}else if(isset($result[$value['employee_id']]['abs']) && $result[$value['employee_id']]['abs'] == 0){
								// 	  $result[$value['employee_id']]['abs'] = $result[$value['employee_id']]['abs'];
								// 	}else{
								// 	  $result[$value['employee_id']]['abs'] = 1;
								// 	}
								//   }  
							 //  }else{
								// if(isset($result[$value['employee_id']]['abs']) && $result[$value['employee_id']]['abs'] != 0){
								// 	$result[$value['employee_id']]['abs'] = $result[$value['employee_id']]['abs'];
								// }else if(isset($result[$value['employee_id']]['abs']) && $result[$value['employee_id']]['abs'] == 0){
								// 	$result[$value['employee_id']]['abs'] = 0;
								// }else{
								// 	$result[$value['employee_id']]['abs'] = 0;
								// }
							 //  }


							  //if($value['timeIn'] >=  '22:00:00' && $value['timeIn'] <= '00:00:00' && $value['timeIn'] >=  '01:00:00' && $value['timeIn'] <= '06:00:00'){
							  
							  /*if($value['timeIn'] >=  '22:00:00' && $value['timeIn'] <= '23:59:00' ){
								  $condition1 = true;
							  }else{
								 $condition1 =  false;
							  }

							  //if($value['timeOut'] >=  '22:00:00' && $value['timeOut'] <= '00:00:00' && $value['timeOut'] >=  '01:00:00' && $value['timeOut'] <= '06:00:00'){
							  if($value['timeOut'] >= '06:00:00' && $value['timeOut'] <= '06:59:00'){
								  if($condition1 == false){
									  $condition1 = false;
								  }else{
									  $value['timeOut'] = '06:00:00';
									  $condition1 = true;
								  }
							  }*/
							   $condition1 = false;
							  if(strlen($value['schedule']) > 4){
								if($value['timeOut'] != "-" && $value['timeIn'] != "-"){
								  $split_sch = explode("-",$value['schedule']);
								  $timeOuts = $value['timeOut'];
								  $am_pm_timeout = date("a",strtotime($value['timeOut']));
								  $sch_timeout = $split_sch[1];
								  if($am_pm_timeout == 'pm'){
								  	if($timeOuts > "22:59:00"){
										$condition1 = true;	
								  	}
								  }else{
								  	/*if($sch_timeout == "00:00:00"){
									  $sch_timeout = "24:00:00";
									}*/
									if($value['nextday']){
										$condition1 = true;
									}
								  }
								  // harus lebih dari 22:00:00
								  /*if(($sch_timeout == "00:00:00" || $sch_timeout > "01:00:00") && $value['nextday']){
									if($sch_timeout == "00:00:00"){
									  $sch_timeout = "24:00:00";
									}
									$condition1 = true;
								  }else if($sch_timeout >= "22:59:00"){
								  	$condition1 = true;
								  }*/
								}
							  }
							  

							  $total_nd = 0;
							  $total_ndot = 0;
							  $total_rot = 0;

							  if($condition1 /*&& $value['new_color_overtime'] == 'green'*/){
																			  
							  	  $split_sch = explode("-",$value['schedule']);
							  	  $sch_timein = $split_sch[0];
							  	  $sch_timeout = $split_sch[1];

								  $am_pm_timein = date("a",strtotime($value['timeIn']));
								  $am_pm_timeout = date("a",strtotime($value['timeOut']));
								  $am_pm_timeout_sch = date("a",strtotime($sch_timeout));
								  $am_pm_timein_sch = date("a",strtotime($sch_timein));

								  $act_in = strtotime($value['date']." ".$sch_timein);
								  
								  $nextdate = date('d-m-Y', strtotime("+1 day", strtotime($value['date'])));
								  if($am_pm_timein_sch == "pm" && $am_pm_timeout_sch == "am"){
								  	$act_out = strtotime($nextdate." ".$sch_timeout);
								  }else{
								  	$act_out = strtotime($value['date']." ".$sch_timeout);
								  }

								  if($value['nextday']){
								  	$time_in = strtotime($value['date']." ".$value['timeIn']);
								  	$time_out = strtotime($value['nextday']." ".$value['timeOut']);
								  }else{
								  	$time_in = strtotime($value['date']." ".$value['timeIn']);
								  	$time_out = strtotime($value['date']." ".$value['timeOut']);
								  }

								  $nd_in = strtotime($value['date']." 22:00:00");
								  $nd_out = strtotime($nextdate." 06:00:00");
								  

									$timespan = Array(
										"timein" => $time_in,
										"actin" => $act_in,
										"actout" => $act_out,
										"ndin" => $nd_in,
										"ndout" => $nd_out,
										"timeout" => $time_out
									);

									uasort($timespan, function ($a,$b){
										if ($a==$b) return 0;
										return ($a<$b)?-1:1;
									});

									$identifier = Array(0,0);
									$temps = min($timespan);
									$results = Array("rot" => 0);

									foreach ($timespan as $keyss => $values) {

										$ranges = $values - $temps;

										switch (implode($identifier)){
											case "00" : $type = "rot" ;break;
											case "10" : $type = "normal" ;break;
											case "11" : $type = "nd" ;break;
											case "01" : $type = "ndot" ;
										}
										
										switch ($keyss){
											case "actin" : $identifier[0] = 1;break;
											case "actout" : $identifier[0] = 0;break;
											case "ndin" : $identifier[1] = 1;break;
											case "ndout" : $identifier[1] = 0;
										}

										if (!($values>$time_in)){
											$temps = $values;
											continue;
										}

										if ($values==$time_out){
											if (array_key_exists($type,$results)){
												$results[$type] += $ranges;
											}else{
												$results[$type] = $ranges;
											}
											break;
										};
										$results[$type] = array_key_exists($type,$results) ?
											($results[$type] + $ranges) :
											($ranges);
										($type == "ndot") && ($values<=$act_in) && ($results["rot"] += $ranges) && (!($results["ndot"] = 0));

										$temps = $values;
									}

									extract($results);
									(!isset($normal)) && ($normal = 0);
									(!isset($nd)) && ($nd = 0);
									(!isset($ndot)) && ($ndot = 0);
									(!isset($rot)) && ($rot = 0);

									//var_dump(["nd"=>$nd/3600,"normal"=>$normal/3600,"ndot"=>$ndot/3600,"rot"=>$rot/3600]);

									$total_nd = round(abs($nd)/3600);
									if($value['new_color_overtime'] == 'green'){
										$total_ndot = (round(abs($ndot)/3600)*60);
									}else{
										$total_ndot = 0;
									}
									$total_rot = round(abs($rot)/3600);

									if($act_in < $nd_in && $act_out < $nd_in){
										$total_nd = 0;
									}

									// if($act_out <= $nd_out){
									// 	$total_rot = 0;
									// }
									// if($value['date'] == '2018-10-24' && $value['employee_id'] == "2018045"){
									// 	return 1;
									// }
								  array_push($test,$value['nextday']." ".$value['timeOut']." to ".$value['date']." ".$value['timeIn']." ,schedule : ".$value['schedule']." total_rot : ".$total_rot." ,total_nd : $total_nd, total_ndot : $total_ndot,employee_id :".$value['employee_id']);
								  if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] != 0){
									  
									  try{
										//$split_h = explode(":", $value['WorkHours']);
										//$result[$value['employee_id']]['nd'] += $total_nd;
										$result[$value['employee_id']]['nd'] += ($total_nd);
									  }catch(\Exception $e){
										//$result[$value['employee_id']]['nd'] = $result[$value['employee_id']]['nd'] + $total_nd;
									  }
								  }else if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] == 0){
										//$result[$value['employee_id']]['nd'] += $total_nd;
										$result[$value['employee_id']]['nd'] += ($total_nd);
								  }else{
								  	//$result[$value['employee_id']]['nd'] = $total_nd;
										$result[$value['employee_id']]['nd'] = ($total_nd);
									 //$result[$value['employee_id']]['nd'] = $total_nd;
								  } 

								  //$result[$value['employee_id']]['nd'] =  date('H:i',$result[$value['employee_id']]['nd']); 
							  }else{
								if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] != 0){
								  $result[$value['employee_id']]['nd'] = $result[$value['employee_id']]['nd'] + 0;
								 }else if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] == 0){
										$result[$value['employee_id']]['nd'] = 0;
								}else{
								  $result[$value['employee_id']]['nd'] = 0;
								}
							  }
							  // if($condition1 == true){
							  //     /*$arg = strtotime($value['timeOut']);
							  //     $arg1 = strtotime($value['timeIn']);

							  //     $total_arg =  abs($arg - $arg1) ;*/ 
							  //     /*$arg = strtotime($value['nextday']." 06:00");
							  //     $arg1 = strtotime($value['date']." ".$value['timeIn']);*/
								  
							  //     $am_pm_timeout = date("a",strtotime($value['timeOut']));
							  //     $am_pm_timeout_sch = date("a",strtotime($sch_timeout));
							  //     $am_pm_timeout_sch = date("a",strtotime($sch_timeout));

							  //     if($value['nextday'] && $am_pm_timein == 'pm'){
							  //       if($value['timeIn'] <= '22:00'){
							  //         $arg1 = strtotime($value['date']." 22:00");
							  //       }else{
							  //         $arg1 = strtotime($value['date']." ".$value['timeIn']);
							  //       }
							  //     }else{
							  //       $arg1 = strtotime($value['nextday']." ".$value['timeIn']);
							  //     }

							  //     if($sch_timeout <= "24:00:00" && $sch_timeout > "22:00:00"){
									
							  //       // actual time out > 22:00 and time out < 
							  //       if($am_pm_timeout == "pm"){
							  //         if( $value['timeOut'] <= "24:00:00"){
							  //           if($value['timeOut'] > $sch_timeout){
							  //             $arg = strtotime($value['date']." ".$sch_timeout);
							  //             $total_ndot = round(abs(strtotime($value['date']." ".$value['timeOut']) - strtotime($value['date']." ".$sch_timeout))/3600);
							  //             //return [$value['date']." ".$sch_timeout];
							  //           }else{
							  //             $arg = strtotime($value['date']." ".$value['timeOut']);
							  //             //return 2;
							  //           }
							  //         //  return [( $value['timeOut'] <= "24:00:00"),($value['timeOut'] > $sch_timeout),$arg];
							  //         }

							  //       }else{ // am
							  //         if($value['timeOut'] > "00:00:00"){
							  //           if($value['timeOut'] > $sch_timeout){
							  //             $total_ndot = round(abs(strtotime($value['date']." ".$value['timeOut']) - strtotime($value['date']." ".$sch_timeout))/3600);
							  //             $arg = strtotime($value['date']." ".$sch_timeout);
							  //           }else{
							  //             $arg = strtotime($value['date']." ".$value['timeOut']);  
							  //           }
							  //         }
							  //       }

							  //     }else if($sch_timeout > "00:00:00"){
							  //       // actual time out > 22:00 and time out
							  //       if($am_pm_timeout == "pm"){
							  //         if($value['timeOut'] <= "24:00:00"){
							  //           $arg = strtotime($value['date']." ".$value['timeOut']);
							  //         }
							  //       }else{

							  //         if($value['timeOut'] > "00:00:00"){
							  //           if($value['timeOut'] > $sch_timeout){                                      
							  //             $total_ndot = round(abs(strtotime($value['nextday']." ".$value['timeOut']) - strtotime($value['nextday']." ".$sch_timeout))/3600);
							  //             $arg = strtotime($value['nextday']." ".$sch_timeout);
							  //           }else{
							  //             $total_ndot = round(abs(strtotime($value['nextday']." ".$value['timeOut']) - strtotime($value['nextday']." ".$sch_timeout))/3600);
							  //             $arg = strtotime($value['nextday']." ".$value['timeOut']);
							  //           }

							  //         }
							  //       }

							  //     }

							  //     if(isset($arg) && isset($arg1)){

							  //       $total_nd =  round(abs($arg - $arg1)/3600); 
							  //       array_push($test,$value['nextday']." ".$value['timeOut']." to ".$value['date']." ".$value['timeIn']." total : $total_nd, employee_id :".$value['employee_id']);
									
							  //       if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] != 0){
							  //           $result[$value['employee_id']]['nd'] += $total_nd;
							  //          /* try{
							  //             $split_h = explode(":", $value['WorkHours']);
							  //             $result[$value['employee_id']]['nd'] = (intval($split_h[0])*60)+intval($split_h[1]);
							  //           }catch(\Exception $e){
							  //             //$result[$value['employee_id']]['nd'] = $result[$value['employee_id']]['nd'] + $total_nd;
							  //           }*/
							  //       }else if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] == 0){
							  //             $result[$value['employee_id']]['nd'] += $total_nd;
							  //       }else{
							  //          $result[$value['employee_id']]['nd'] = $total_nd;
							  //       } 
							  //     }
								  

							  //     //$result[$value['employee_id']]['nd'] =  date('H:i',$result[$value['employee_id']]['nd']); 
							  // }else{
							  //   if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] != 0){
							  //     $result[$value['employee_id']]['nd'] = $result[$value['employee_id']]['nd'] + 0;
							  //    }else if(isset($result[$value['employee_id']]['nd']) && $result[$value['employee_id']]['nd'] == 0){
							  //           $result[$value['employee_id']]['nd'] = 0;
							  //   }else{
							  //     $result[$value['employee_id']]['nd'] = 0;
							  //   }
							  // }
							  	if(isset($result[$value['employee_id']]['ndot'])){
							  		if($value['new_color_overtime'] == 'green'){
										$result[$value['employee_id']]['ndot'] += $total_ndot;
							  		}
							  	}else{
							  		//if($value['new_color_overtime'] == 'green'){
							  			$result[$value['employee_id']]['ndot'] = $total_ndot;
							  		//}
							  	}


							  if($value['total_overtime'] != '-'  && $value['total_overtime'] != null && $value['total_overtime'] != 0 && $value['new_color_overtime'] == 'green'){
								 $exp_tot = explode(':',$value['total_overtime']);
								 $tot1 = intval($exp_tot[0]) * 60;
								 $tot2 = intval($exp_tot[1]) ;
								 $ot_total =  round((($tot1 + $tot2)/60),2);
								 if(isset($result[$value['employee_id']]['rot'])){
									//return $ot_total;
									if($total_rot){
										$result[$value['employee_id']]['rot'] += (($ot_total-$total_rot)*60);
									}else{
										$result[$value['employee_id']]['rot'] += ($ot_total*60);
									}
									/*if($total_ndot > 0){
										if($ot_total > $total_ndot){
											$result[$value['employee_id']]['rot'] += ($ot_total-$total_ndot);
										}else{
											$result[$value['employee_id']]['rot'] += ($total_ndot-$ot_total);
										}
									}*/
								 }else{
								 	if($total_rot){
										$result[$value['employee_id']]['rot'] = (($ot_total-$total_rot)*60);
									}else{
										$result[$value['employee_id']]['rot'] = ($ot_total*60);
									}
								 // 	$result[$value['employee_id']]['rot'] += $ot_total;
									// $result[$value['employee_id']]['rot'] = $total_rot;
								 }
								 /*$tot1 = intval($exp_tot[0]) * 60;
								 $tot2 = intval($exp_tot[1]) ;
								 $ot_total =  $tot1 + $tot2;
								  if(isset($result[$value['employee_id']]['rot']) && $result[$value['employee_id']]['rot'] != 0){
									   $result[$value['employee_id']]['rot'] = intval($result[$value['employee_id']]['rot']) + $ot_total;
								  }else if(isset($result[$value['employee_id']]['rot']) && $result[$value['employee_id']]['rot'] == 0){
										$result[$value['employee_id']]['rot'] = $ot_total;
								  }else{
									$result[$value['employee_id']]['rot'] = $ot_total;
								  }*/  
							  }else{
								if(isset($result[$value['employee_id']]['rot']) && $result[$value['employee_id']]['rot'] != 0){
								  $result[$value['employee_id']]['rot'] = intval($result[$value['employee_id']]['rot']) + 0;
								 }else if(isset($result[$value['employee_id']]['rot']) && $result[$value['employee_id']]['rot'] == 0){
										$result[$value['employee_id']]['rot'] = 0;
								}else{
								  $result[$value['employee_id']]['rot'] = 0;
								}
							  }

							   if($value['OvertimeRestDay'] != '-' && $value['OvertimeRestDay'] != null && $value['OvertimeRestDay'] != 0 && $value['OvertimeRestDay_Color'] == 'green'){
								 //$value['OvertimeRestDay'];
								 $exp_tot = explode(':',$value['OvertimeRestDay']);
								 $tot1 = intval($exp_tot[0]) * 60;
								 $tot2 = intval($exp_tot[1]) ;
								 $ot_total =  round((($tot1 + $tot2)/60),2);
								 if(isset($result[$value['employee_id']]['rotdo'])){
									//return $ot_total;
									$result[$value['employee_id']]['rotdo'] += ($ot_total*60);
								 }else{
									$result[$value['employee_id']]['rotdo'] = ($ot_total*60);
								 }

								/*if(isset($result[$value['employee_id']]['rotdo']) && $result[$value['employee_id']]['rotdo'] != 0){
									   $result[$value['employee_id']]['rotdo'] = intval($result[$value['employee_id']]['rotdo']) + $ot_total;
								  }else if(isset($result[$value['employee_id']]['rotdo']) && $result[$value['employee_id']]['rotdo'] == 0){
										$result[$value['employee_id']]['rotdo'] = $ot_total;
								  }else{
									$result[$value['employee_id']]['rotdo'] = $ot_total;
								  }  */
							  }else{
								if(isset($result[$value['employee_id']]['rotdo']) && $result[$value['employee_id']]['rotdo'] != 0){
								  $result[$value['employee_id']]['rotdo'] = intval($result[$value['employee_id']]['rotdo']) + 0;
								 }else if(isset($result[$value['employee_id']]['rotdo']) && $result[$value['employee_id']]['rotdo'] == 0){
										$result[$value['employee_id']]['rotdo'] = 0;
								}else{
								  $result[$value['employee_id']]['rotdo'] = 0;
								}
							  }

							  $iexp  =  explode('-',$value['date']);
							  if(isset($iexp[2])){
							  	$iexp = $dictionary[intval($iexp[2])];
							  }
							  $alpha_stat = $dictionary[$y];
							  $result[$value['employee_id']][$alpha_stat] =  $total;

							  $tmp_field = array_search($alpha_stat,$range_field);
							  if(gettype($tmp_field) == "boolean"){
								array_push($range_field, $alpha_stat);
							  }
							  //return [$result,$y];
							  if($total == "EL"){
								$result[$value['employee_id']]["status".$alpha_stat] = "red";
							  }elseif($total == "SL"){
								$result[$value['employee_id']]["status".$alpha_stat] = "red";
							  }elseif($total == "VL"){
								$result[$value['employee_id']]["status".$alpha_stat] = "green";
							  }elseif($total == "BL"){
								$result[$value['employee_id']]["status".$alpha_stat] = "red";
							  }elseif($total == "DO"){
								$result[$value['employee_id']]["status".$alpha_stat] = "white";
							  }else{
								if($total != '/'){
								   $result[$value['employee_id']]["status".$alpha_stat] = "yellow";
								}else{
								  try {
									if( ($value['timeIn'] != "00:00" && $value['C_TimeIn'] != "red") && ($value['timeOut'] != "00:00" && $value['C_TimeOut'] != "red") ){
										/*if($value['date'] == "2018-10-01" && $value['employee_id'] == "2011006"){
										  return $value;
										}*/
									  $result[$value['employee_id']][$alpha_stat] =  $total;
									}else{               
										if(in_array($value['schedule'], ['BL','T','TB','OB']) || in_array($value['schedule_shift_code'], ['BL','T','TB','OB'])){
											$result[$value['employee_id']][$alpha_stat] = $value['schedule'];
											$result[$value['employee_id']]["status".$alpha_stat] = "red";
										}else{

										  $result[$value['employee_id']][$alpha_stat] =  "ABS";
										  if(!isset($result[$value['employee_id']]['abs'])){
											$result[$value['employee_id']]['abs'] = 1;  
										  }else{
											$result[$value['employee_id']]['abs']++;
										  }
										}
									}

									$result[$value['employee_id']]["status".$alpha_stat] = "white";
								  } catch (\Exception $e) {
									return [$e->getMessage(),$result[$value['employee_id']]];
								  }
								}
							  }
							  if($value['check_holiday']){
							  	$result[$value['employee_id']][$alpha_stat] .= " - ".$value['check_holiday'];
							  }
							 // return  $counting_late;
							  
							}else{
							  //return [$range[$y],$value['date'],$value['employee_id']];
							  /*$iexp  =  explode('-',$value['date']);
							  $iexp = $dictionary[intval($iexp[2])];
							  $alpha_stat = $dictionary[$y];
							  
							  $result[$value['employee_id']][$alpha_stat] = '-';
							  $result[$value['employee_id']]["status".$alpha_stat] = "white";*/
							}
						  }
					  }
				   }  
				}
			}

		   $final = [];
		   $ix = 0;
		   for($k = 0; $k < $count; $k++){
			  foreach ($result as $key => $value) {
				if(isset($data_emp[$k])){
					if($data_emp[$k] ==  $key){
					  $final[$ix] =  $value;
					  $ix = $ix + 1;
				   }
				} 
			  }    
		   }
		  
		  for ($i=0; $i < count($final); $i++) { 
			foreach ($final[$i] as $key => $value) {
			  for($j=0; $j < count($range_field); $j++){
				$field = $range_field[$j];
				if(!isset($final[$i][$field])){
				  $final[$i][$field] = "-"; $final[$i]["status$field"] = 'white';
				}
			  }
/*              if(!isset($final[$i]['a'])){ $final[$i]['a'] = "\\"; $final[$i]['statusa'] = 'white'; }
			  if(!isset($final[$i]['b'])){ $final[$i]['b'] = "\\"; $final[$i]['statusb'] = 'white'; }
			  if(!isset($final[$i]['c'])){ $final[$i]['c'] = "\\"; $final[$i]['statusc'] = 'white'; }
			  if(!isset($final[$i]['d'])){ $final[$i]['d'] = "\\"; $final[$i]['statusd'] = 'white'; }
			  if(!isset($final[$i]['e'])){ $final[$i]['e'] = "\\"; $final[$i]['statuse'] = 'white'; }
			  if(!isset($final[$i]['f'])){ $final[$i]['f'] = "\\"; $final[$i]['statusf'] = 'white'; }
			  if(!isset($final[$i]['g'])){ $final[$i]['g'] = "\\"; $final[$i]['statusg'] = 'white'; }
			  if(!isset($final[$i]['h'])){ $final[$i]['h'] = "\\"; $final[$i]['statush'] = 'white'; }
			  if(!isset($final[$i]['i'])){ $final[$i]['i'] = "\\"; $final[$i]['statusi'] = 'white'; }
			  if(!isset($final[$i]['j'])){ $final[$i]['j'] = "\\"; $final[$i]['statusj'] = 'white'; }
			  if(!isset($final[$i]['k'])){ $final[$i]['k'] = "\\"; $final[$i]['statusk'] = 'white'; }
			  if(!isset($final[$i]['l'])){ $final[$i]['l'] = "\\"; $final[$i]['statusl'] = 'white'; }*/
			}
		   }

		   	
					   foreach ($range as $key => $value) {
						   $fin[] = ["date" => $value];
					   }
					// $comment = \DB::SELECT("select * from att_cutoff_record where cutoff_periot='$attendance' ");
					// ($comment == null ? $comment = [] : $comment = $comment);
				  $finally = array_merge_recursive([$final],["date" => $fin],["comment" => []]);
				// if(count($test) > 0){
				//   return [$test];
				// }
				 return $cutOff->indexAcess($finally,$access[3]);
				 // if(count($test) > 0){
				//   return [$test];
				// }

		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access" => $access[3]],$status);
		}
	}


	public function getDetail($f,$t,$job,$department,$localit)
   {

	
	if($localit == 3){
		$localit = '1,2,3';
	}else if($localit == 2){
		$localit =  '2,3';
	}else{
		$localit = '1,2';
	}

	$period = new DatePeriod(
	 new DateTime($f),
	 new DateInterval('P1D'),
	 new DateTime($t)
	);

	$rangeDt = "";
	foreach ($period as $keys => $values) {
	  if(strlen($rangeDt) == 0){
		$rangeDt = "'".$values->format('Y-m-d')."'";
	  }else{
		$rangeDt .= ",'".$values->format('Y-m-d')."'";
	  }
	  //$value->format('Y-m-d')       
	}
	$rangeDt .= ", '".$t."'";
	//$query = \DB::SELECT("CALL View_Attendance_record('$name')");
	$query = "SELECT distinct t2.employee_id as employee_id, date_format(t1.date,'%Y-%m-%d') as date,concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name) as Name, t2.first_name as first_name, t2.last_name  as last_name, t3._from as fromx, t3._to as tox,
COALESCE((select concat(date_format(attendance_work_shifts._from,'%H:%i'),'-',date_format(attendance_work_shifts._to,'%H:%i')) from attendance_work_shifts, att_change_shift where att_change_shift.employee_id = (t2.employee_id)  and att_change_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_change_shift.status_id = 2 and att_change_shift.new_shift = attendance_work_shifts.shift_id),'99:99:99') as value, 
COALESCE((select att_change_shift.created_at from attendance_work_shifts, att_change_shift where att_change_shift.employee_id = (t2.employee_id)  and att_change_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_change_shift.status_id = 2 and att_change_shift.new_shift = attendance_work_shifts.shift_id),'99:99:99') as date_changeShift,
COALESCE((select att_swap_shift.created_at from att_schedule_request,att_swap_shift,attendance_work_shifts where att_swap_shift.employee_id = (t2.employee_id) 
	   and att_swap_shift.employee_id  = att_schedule_request.employee_id   
	   and att_swap_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 group by att_swap_shift.created_at limit 1 ),'99:99:99') as date_swapShift,
COALESCE((select concat(time_format(attendance_work_shifts._from,'%H:%i'),'-',time_format(attendance_work_shifts._to,'%H:%i')) from att_schedule_request,att_swap_shift,attendance_work_shifts where att_swap_shift.employee_id = (t2.employee_id) 
	   and att_swap_shift.employee_id  = att_schedule_request.employee_id
	   and att_swap_shift.old_shift_id = attendance_work_shifts.shift_id      
	   and att_swap_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 group by att_swap_shift.created_at limit 1 ),'99:99:99') as schedule_swapShift,
	
		  (concat(time_format((select fromx),'%H:%i'),'-',time_format((select tox),'%H:%i')))
		as schedule,
		 date_format(t10.time_in,'%H:%i')as timeIn,date_format(t10.time_out,'%H:%i')as timeOut,
		 (
		  case 
			when date_format(t8.time_in,'%H:%i') >  date_format(t3._from,'%H:%i')
			then (timediff(date_format(t10.time_out,'%H:%i'),date_format(t10.time_in,'%H:%i')))
			else
				  (timediff(date_format(t10.time_out,'%H:%i'),date_format(t3._from,'%H:%i')))
			end
		 )as WorkHours,
		 t3.hour_per_day,t5.total_overtime,t6.total_overtime as OvertimeRestDay, t6.status_id as RestDay_App ,
		 (
		  case
			when (select WorkHours) >  TIMEDIFF(date_format(t3._to,'%H:%i'),date_format(t3._from,'%H:%i'))
			then
			  0
			else
			  timediff(timediff(date_format(t3._to,'%H:%i'),date_format(t3._from,'%H:%i')),(select WorkHours))
		  end
		 ) as Short,
		 concat((substring((select schedule),1,5)),':00') as schedule_mod,  
		(
		case
		when (select schedule_mod) > (select timeIn)
		then
		  0
		when (select schedule_mod) < (select timeIn)
		then 
		  date_format(timediff((select timeIn),(select schedule_mod)),'%H:%i')  
		else
		  0
		end     
		)as Late,
		 (
		  case
			when (select WorkHours) >  TIMEDIFF(date_format(t3._to,'%H:%i'),date_format(t3._from,'%H:%i'))
			then
			  0
			else
			  timediff(date_format(t3._to,'%H:%i'),date_format(t10.time_out,'%H:%i'))
		  end
		 ) as EarlyOut,
		 t1.status,t4.input_ as ApproveStatus
		 FROM att_schedule t1
		 left join emp t2 on t2.employee_id=t1.employee_id
		 left join job_history t40 on t40.employee_id  =   t2.employee_id
		 left join attendance_work_shifts t3 on t3.shift_id=t1.shift_id
		 left join biometrics_device t10 on t10.employee_id=t1.employee_id and t10.date=t1.date
		 left join biometrics_device t4 on t4.employee_id=t1.employee_id and t4.date=t1.date /* and t3._from !='00:00:00' and t3._from !='00:00:00' */
		 left join att_overtime t5 on t5.employee_id=t1.employee_id and t5.date_str=t1.date and t5.status_id=2 and t3.shift_code !='DO'
		 left join att_overtime t6 on t6.employee_id=t1.employee_id and t6.date_str=t1.date and t6.status_id=2 and t3.shift_code ='DO'
		 left join biometrics_device t7 on t7.employee_id=t1.employee_id and t7.date=t1.date and t3._from !='00:00:00' and t3._to !='00:00:00' and date_format(str_to_date(time_to_sec(timediff(t7.time_out,t7.time_in))/60/60,'%l.%i'),'%i') !='00'
		 left join biometrics_device t8 on t8.employee_id=t1.employee_id and t3._from !='00:00:00'  and t8.time_in > t3._from and t8.date=t1.date 
		 left join biometrics_device t9 on t9.employee_id=t1.employee_id and t3._to !='00:00:00'and t9.time_out<t3._to  and t9.date=t1.date
		 where t1.date in ($rangeDt) and t1.status = 2 and  t2.local_it in ($localit)";

		if($job  != null){
			$query .= "and t40.job = $job ";    
		}

		if($department != null){
		  $query .=  "and t2.department = $department";
		}

		$query .=  " group by t1.date,t2.employee_id ASC";

	$query = \DB::SELECT($query);
	// $data = ['data'=>$query, 'status'=>200, 'message'=>'View record data'];
	// return $data;
	if($query){
	  $c = count($query);
	  $mod = $c % 10;
	  $count = $c - $mod;
	  // if($mod <= 10){
	  //  return $data = ['data'=>$query, 'status'=>500, 'message'=>'Record data is less than 1 month'];
	  // }
	  $j = 0 ;
	  $last = $c - 20;

	  for ($i = 0; $i < $c; ) { // $c = 57 / $i until 56
		// $dt = substr($dt, 8);
		// first period
		// if ($i <= 9 ){
		//   $between = $query[$j]->date.' <=> '.$query[$j+(($c-1) -$j)]->date;
		//   $between = $between;
		// }
		// // second period
		// else if ($i >= 10 && $i <= 19){

		//   $between2 = $query[$j+(10-$j)]->date.' <=> '.$query[$j+(19-$j)]->date;

		//   $between = $between2;
		// }
		// // third period
		// else{
		//   $date = date("d");

		//   $between3 = $query[$j+(20-$j)]->date.' <=> '.$query[$j+(($c-1)-$j)]->date;
		//   $between = $between3;
		// }


		if ($i >= $count-1){ // if $i = 50 then $i - $c-1
		  $between = $query[$i]->date.'<==>'.$query[$c-1]->date;
		  $a = $mod;
		}
		else{
		  $between = $query[$i]->date.'<==>'.$query[$i+9]->date;
		  $a = 10;
		}
		for ($j=1; $j <= $a; $j++) {
		  // create a mark, yes for workHour < hour_per_day, and no for other
		  if ($query[$i]->WorkHours != null){
			if ($query[$i]->WorkHours < $query[$i]->hour_per_day) $workStatus = "yes";
			else $workStatus = "no";
		  }
		  else{
			$workStatus = "no";
		  }
		  /*
		  //  Change the view to DO if schedule is 00:00-00:00
		  //  Change the view to "-" if schedule is null
		  */
		  
		  if ($query[$i]->schedule == "00:00-00:00") {
			$date_modify = $query[$i]->date;
			$emp_modify = $query[$i]->employee_id;

			$give =\DB::SELECT("select aws.shift_code from att_schedule ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify' 
							 and ash.date = '$date_modify' and  ash.shift_id = aws.shift_id order by aws.shift_code ASC limit 1 ");
			//$check_workshift = \DB::SELECT("select * from att_schedule where ")
			//return $give[0]->shift_code;
			if(!isset($give[0]->shift_code)){
				$schedule = '-';
			}else{
				$schedule = $give[0]->shift_code;
			}
			/*  if there is no time in or time out recorded,
			//  the text will be colored red
			//  $noTimeInStatus = "yes" means it will be colored red
			*/
			if ($query[$i]->timeIn == null AND $query[$i]->timeOut == null) {
			  $timeIn = "-";
			  $timeOut = "-";
			  $noTimeInStatus = "yes";
			  $noTimeInStatus = "yes";
			}
			else if ($query[$i]->timeIn == null AND $query[$i]->timeOut != null) {
			  $timeIn = "-";
			  $timeOut = $query[$i]->timeOut;
			  $noTimeInStatus = "yes";
			  $noTimeInStatus = "no";
			}
			else if ($query[$i]->timeIn != null AND $query[$i]->timeOut != null) {
			  $timeIn = $query[$i]->timeIn;
			  $timeOut = $query[$i]->timeOut;
			  $noTimeInStatus = "no";
			  $noTimeInStatus = "no";
			}
			else {
			  $timeIn = $query[$i]->timeIn;
			  $timeOut = "-";
			  $noTimeInStatus = "no";
			  $noTimeInStatus = "yes";
			}
		  }
		  else {
			$schedule = $query[$i]->schedule;
			if ($query[$i]->timeIn == null) {
			  $timeIn = "-";
			  $noTimeInStatus = "yes";
			}
			else {
			  $timeIn = $query[$i]->timeIn;
			  $noTimeInStatus = "no";
			}

			if ($query[$i]->timeOut == null) {
			  $timeOut = "-";
			  $noTimeOutStatus = "yes";
			}
			else {
			  $timeOut = $query[$i]->timeOut;
			  $noTimeOutStatus = "no";
			}
		  }

		  if ($query[$i]->total_overtime == null) {
			$timeIns = strtotime($query[$i]->timeOut);
			$explode = explode('-',$query[$i]->schedule);
			$arg2    = strtotime($explode[1]);
			if($timeIns > $arg2){
			  if($schedule == 'DO'){
				$totalOver = '-';
				$mass = "yes";
			  }else{
				$totalOver = date('H:i',($timeIns - $arg2));// $hox.':'.$mox;//intval($ho).':'.$mi;
			  }
			  // $exp =  explode(':',$totalOvers);
			  // $totalOver =  intval($explode[0]).':'.$explode[1];
			  $makeOver =  'yes';
			}else{
			  $makeOver = 'no';
			  $totalOver = "-";
			}
		  }
		  else {
			$floor   = floor($query[$i]->total_overtime);
			$decimal = $query[$i]->total_overtime - $floor;
			if($decimal != 0){
			  $decimal = (60 * (10 * $decimal))/10;
			}else{
			  $decimal = "00";
			}
			$ax = strlen($floor);

			$ax = ($ax == 1 ? '0'.$floor : $floor);
			return $totalOver = $floor.':'.$decimal;
			$makeOver  ='no';

			
		
		  }

		  // if ($query[$i]->timeIn == null) {
		  //  $timeIn = "-";
		  //  $noTimeInStatus = "yes";
		  // }
		  // else {
		  //  $timeIn = $query[$i]->timeIn;
		  //  $noTimeInStatus = "no";
		  // }

		  // if ($query[$i]->timeOut == null) {
		  //  $timeOut = "-";
		  //  $noTimeOutStatus = "yes";
		  // }
		  // else {
		  //  $timeOut = $query[$i]->timeOut;
		  //  $noTimeOutStatus = "no";
		  // }

		  if ($query[$i]->WorkHours == '00:00:00' || $query[$i]->WorkHours == null){ $workHour = "-"; $totalOver = "-"; }
		  else{

			if(strchr($query[$i]->WorkHours,'-')){
			  
			  $exp_schedule = explode('-', $query[$i]->schedule);
			  $ts1 = strtotime($exp_schedule[0]);
			  $ts2 = strtotime($exp_schedule[1]);
			  $diff = abs($ts1 - $ts2) / 3600;
			  if(strpos($diff,'.')){
				$e_menit = explode('.', "$diff");
				$tmp = "0.".$e_menit[1]; // build menit
				$b_menit = round(floatval($tmp) * 60 );
				if(strlen("$b_menit") == 2){
				  $workHour =  $e_menit[0].":$b_menit";
				}else{
				  $workHour =  $e_menit[0].":0$b_menit";
				}
			  }else{
				$t = $diff/60;
				if(strchr("$t",".")){
				  $diff = $diff/2;
				}
				$workHour = "$diff:00";
			  }
			}else{            
			   $workHour = $query[$i]->WorkHours;
			   $explode = explode(":",$workHour);
			   $hi = $explode[0];
			   if(substr($hi,0,1) == 0){
				$workHour =  substr($hi,1,1).':'.$explode[1];
			   }else{
				$workHour =  $hi.':'.$explode[1];
			   }
			}
		  }

		  if($totalOver != "-" and $schedule == 'yes'){
			$timeIns = strtotime($query[$i]->timeOut);
			$explode = explode('-',$query[$i]->schedule);
			$arg2    = strtotime($explode[1]);
			$overRest = date('H:i',($timeIns - $arg2));

		  }else{
			$overRest = "-";
		  }

		  // if($totalOver ==  "-"&& $overRest !="-"){
		  //  $totalOver = "-";
		  // }

		  if ($query[$i]->OvertimeRestDay == NULL){ 
			if(isset($mass) && $mass == "yess"){
			  if($schedule == "DO"){
				$timeInss = strtotime($query[$i]->timeOut);
				$timeOuts = strtotime($query[$i]->timeIn);
				$overRest = date('H:i',($timeInss - $timeOuts));
				
				if($timeIns == false && $timeOuts == false){
				  $overRest = "-";
				}else{
				  $workHour = $overRest;
				}
			  }
			}else{
			  $overRest = "-";  
			}
			
		  }else{
			$overRest = $query[$i]->OvertimeRestDay;
			$explode =  explode('.',$query[$i]->OvertimeRestDay);
			$floor =  floor($overRest);
			$check_floor = $overRest -  $floor;
			if($check_floor != 0){
			  $overRest_decimal = (60 * (10 * $decimal))/10;
			  $overRest = $explode[0].':'.$overRest_decimal;
			}else{
			  $overRest = ((int)$explode[0]+1).':'.'00'; 
			}   
		  }
		  

		  if ($query[$i]->Short == null) {$short = "-";}
		  else{
			$time1 = intval(substr($query[$i]->Short,0,2));
			$time2 = intval(substr($query[$i]->Short,3,2));
			$short = ($time1 * 60) + $time2;
			if($short == 0){
			  $short = '-';
			}else{
			  $short =  $short;
			}

		  }
		  

		  if ($query[$i]->ApproveStatus == null || $query[$i]->ApproveStatus == 0 ){ $status = "no"; }
		  elseif ($query[$i]->ApproveStatus == 1) {$status = "yes";}
		  else{ $status = "no";}

		  if ($query[$i]->Late == null || $query[$i]->Late == "0"){ $late = "-"; }
		  else{
			// if($query[$i]->Late == "0" || $query[$i]->Late == 0){
			//  $late = '-';
			// }else{
			  $late = $query[$i]->Late;
			//}
		  }
		  if ($query[$i]->EarlyOut == null){ $earlyOut = "-";}
		  else{ 

			$a1 = new \DateTime("17:00:00.000");
			$a2 = new \DateTime("18:55:00.000");
			$b  = date_diff($a2,$a1); 
			$str =$b->format('%H:%I');
			
			$explode = explode(":",$query[$i]->EarlyOut);
			  $earlyOut = strlen($explode[0]);
			  if($earlyOut == 2){
				$ho = (int)$explode[0];
				$mi = (int)$explode[1];
				$ho = $ho * 60;
				$earlyOut = $ho + $mi ;
			  }else{
				$earlyOut = "-";
			  }
			if($earlyOut != '-'){
			  //return var_dump($query[$i]);
			}
		  }
		  $h = substr($schedule, 6, strpos($schedule,":"));
		  $m = substr($schedule, 9, strpos($schedule,10));
		  //print_r($h."\n");
		  $h1 = substr($timeOut, 0, strpos($timeOut,":"));
		  $m1 = substr($timeOut, 3, strpos($timeOut,4));
		  //print_r($h1."\n".'---');
		  // create mark for the record that has been changed, yes for changed record
		  if ($totalOver != null){
			if($makeOver == 'yes'){
			  $overStatus = ( $overRest == "-" ?  "yes" : "no");
			  if(isset($mass) && $mass == "yess"){
				$overStatus = "yes";
			  }
			  // $overStatus = "yes";
			}elseif($makeOver == "no") {
			  $overStatus = "no";
			}
			// if ($m1 > $m){
			//  if ($totalOver == "-")
			//    $overStatus = "yes";
			//  else
			//    $overStatus = "no";
			// }
			// else{
			//  if ($totalOver == "-")
			//    $overStatus = "yes";
			//  else
			//    $overStatus = "no";
			// }
		  }
		  else
			$overStatus = "no";

		  if($overRest !=  null && isset($query[$i]->RestDay_App)){
			if($query[$i]->RestDay_App == 2){
			  $OTR_color =  'yes';  
			}else{
			  $OTR_color =  'no';
			}
			
		  }else{
			$OTR_color = "-";
		  }
		  // Compare time in and time out between attendance record and request
		  $emp = $query[$i]->employee_id;
		  $dt = $query[$i]->date;
		  $_timeIN = $query[$i]->timeIn.':00';
		  $_timeOUT = $query[$i]->timeOut.':00';

		  // create mark for the record that has been changed, yes for changed record
		  $tm = \DB::select("CALL View_TimeInOut_byDate_employee('$emp', '$dt')");
		  $test[] = $dt;
		  if ($tm != null) {
			if ($tm[0]->req_in != $_timeIN)
			  $timeInStatus = "yes";
			else
			  $timeInStatus = "no";

			if ($tm[0]->req_out != $_timeOUT)
			  $timeOutStatus = "yes";
			else
			  $timeOutStatus = "no";
		  }
		  else {
			$timeInStatus = "no";
			$timeOutStatus = "no";
		  }
		  if ($i == $c) {
			$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
			return $data;
		  }
		  else{
			if($totalOver != '-'){
			

			  if($workHour != "0:00"){
				
			   $exp_schedule = explode('-', $schedule);
			   if(!isset($atap)){
				  $ts1 = 0;
			   }else{
				  $ts1 = strtotime($exp_schedule[1]); 
			   }

			   $ts2 = strtotime($timeOut);
			   $totalOvers =  date("h:i",($ts1 - $ts2)) ;
			   // //return  $diff = abs($ts1 - $ts2) / 60 / 60;
			   //  if(strpos($diff,'.')){
			   //    $e_menit = explode('.',$diff);
			   //    $tmp = "0.".$e_menit[1]; // build menit
			   //    $b_menit = round(floatval($tmp) * 60 );
			   //    if(strlen("$b_menit") == 2){
			   //      return $e_menit[0];
			   //      return $totalOvers =  $e_menit[0].":$b_menit";
			   //    }else{
			   //      return $totalOvers =  $e_menit[0].":0$b_menit";
			   //    }
			   //  }else{
			   //       $totalOvers = "$diff:00";
			   //  }

			   // return $totalOvers;
			  }else{
				// if($workHour == '0:00'){
				//  $exp_schedule = explode('-', $schedule);
				//  $exp_sch_in = explode(':', $exp_schedule[0]);
				//  $exp_sch_out = explode(':', $exp_schedule[1]);
				//  if(intval($exp_sch_in[1]) > 30){ $a = intval($exp_sch_in[0]) + 1; }
				//  if(intval($exp_sch_out[1]) > 30){ $b = intval($exp_sch_out[0]) + 1; }
				//  if(intval($exp_sch_in[1]) > intval($exp_sch_out[1])){
				//    $c =
				//  }
				// }
				//$workHour = "-";
				$totalOvers = "-";
			  }

			  // if($totalOvers ==  '00:00' || $workHour == "0:00"){
			  //  $totalOvers = '-';
			  // }
			}else{
			  $totalOvers = '-';
			}
			if($overRest != "-"){
			  $short = '-';
			  $late = '-';
			  $earlyOut = '-';
			}

			$temp[] = [
			  'date' => $query[$i]->date,
			  'employee_id' => $query[$i]->employee_id,
			  'Name' => $query[$i]->Name,
			  'schedule' => $schedule,
			  'timeIn' => $timeIn,
			  'timeOut' => $timeOut,
			  'noTimeIn' => $noTimeInStatus,
			  'noTimeOut' => "yes",//$noTimeOutStatus,
			  'WorkHours' => $workHour,
			  'workStatus' => $workStatus,
			  'total_overtime' => $totalOvers,
			  'overStatus' => $overStatus,
			  'status' => $status,
			  'OvertimeRestDay' => $overRest,
			  'OvertimeRestDay_Color' => $OTR_color,
			  'timeOutStatus' => $timeOutStatus,
			  'timeInStatus' => $timeInStatus,
			  'Short' => $short,
			  'Late' => $late,
			  'EarlyOut' => $earlyOut,
			  'first_name' =>$query[$i]->first_name,
			  'last_name' =>$query[$i]->last_name,
			  'between' => $between
			];

			$i++;
		  }
		}
	  }
	  $data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
	}
	else{
	  $data = ['data'=>null, 'status'=>500, 'message'=>'Record data is empty'];
	}
	return $data;
  }


	public function insert_comment(){
			  /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			$cutOff = new Attendance_cutoffrecordModel;
			$input = \Input::all();
			$attendance = \Input::get("attendance");
			$comment = \input::get("comment");

			$rule = [ "attendance" => 'required', "comment" => "required"];
			$validation = \Validator::make($input, $rule);

			if($validation->fails()){
					$check = $validation->errors()->all();
					  return $att_record-> getMessageAccess($check,500,[],$access[3]);
			}else{
					$db = \DB::SELECT("CALL insert_att_cutoff_record('$attendance','$comment')");
					if( isset($db)){
						return $att_record-> getMessageAccess("Insert data comment success ",200,[],$access[3]);
					}else{
						return $att_record-> getMessageAccess("Failed to insert data comment",500,[],$access[3]);
					}
			}
		   }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access" => $access[3]],$status);
		}

	}

}
