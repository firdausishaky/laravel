<?php namespace Larasite\Http\Controllers\Attendance\CutOff;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Model\Attendance\cutOff\Attendance_cutoffrecordModel;

class Attendance_CutOff extends Controller {

    protected $form = 74;

    public function index()
    {
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

        if($access[1] == 200){
            $att_record = new  Attendance_cutoffrecordModel;
            if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
                return $att_record-> getMessageAccess("Unauthorized",500,[],$access[3]);
            }else{
                  $first_date = \DB::SELECT("select date from att_schedule order by date asc limit 1");
                    $last_date = \DB::SELECT("select date from att_schedule order by date desc limit 1");
                    $first = $first_date[0]->date;
                    $last = $last_date[0]->date;
                    $step = '+9 day';
                        $format = 'Y/m/d';
                        $dates = array();
                        $current = strtotime( $first );
                        $last = strtotime( $last );

                         while( $current <= $last ) {

                            $dates[] = date( $format, $current );
                            $current = strtotime( $step, $current );
                         }

                        $dante = [];

                        $count = count($dates);
                        for($i = 1; $i < $count; $i++){
                            if($i % 2 == 0){
                                $dante[] = $dates[$i];
                            }else{
                                $dantex[] = $dates[$i];
                            }
                        }

                        $count1 = count($dante)-1;
                       $date_ex = "";
                       for($i = 0; $i < $count1; $i++){
                            $date_ex .= ",".$dantex[$i]." - ".$dante[$i];
                       }
                        $trimmed = ltrim($date_ex, ",");
                       $explode = explode(",", $trimmed);
                       foreach ($explode as $key => $value) {
                        $periode[] = ["periode" => $value];
                      }

                return $att_record->indexAcess($periode,$access[3]);
            }

        }else{
            $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }

    }


        public function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) {
                $dates = array();
                $current = strtotime($first);
                $last = strtotime($last);

            while( $current <= $last ) {
                $dates[] = date($output_format, $current);
            }
            return $dates;
        }

    public function GetDays($sStartDate, $sEndDate){  
      // Firstly, format the provided dates.  
      // This function works best with YYYY-MM-DD  
      // but other date formats will work thanks  
      // to strtotime().  
      $sStartDate = gmdate("Y-m-d", strtotime($sStartDate));  
      $sEndDate = gmdate("Y-m-d", strtotime($sEndDate));  

      // Start the variable off with the start date  
     $aDays[] = $sStartDate;  

     // Set a 'temp' variable, sCurrentDate, with  
     // the start date - before beginning the loop  
     $sCurrentDate = $sStartDate;  

     // While the current date is less than the end date  
     while($sCurrentDate < $sEndDate){  
       // Add a day to the current date  
       $sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  

       // Add this new day to the aDays array  
       $aDays[] = $sCurrentDate;  
     }  

     // Once the loop has finished, return the  
     // array of days.  
     return $aDays;  
   }  
    public function view(){

        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){

            //return model 
            $att_record = new  Attendance_cutoffrecordModel;
            $cutOff = new Attendance_cutoffrecordModel;

            $attendance  = \Input::get('attendance');
            $explode = explode("-",$attendance);
            $date1 = $explode[0];
            $date2 = $explode[1];

            //call procedure
            $data = \DB::SELECT("CALL view_cutoff_record('$date1','$date2') ");
            // check if procedure 
            if($data == null){
                    return $att_record-> getMessageAccess("Data not exist",200,[],$access[3]);
            }

            //declare variable
            $tar = [];
            $abs = [];
            $nd = [];
            $rot = [];
            $rotdo = [];
            $employee_ids = [];

            //loop data employee in one array
            foreach ($data as $key => $value) {
                $employee_ids[$value->employee_id] = $value->employee_id;
            }

            //loop for get employee id
            foreach ($employee_ids as $key => $value) {
                $data_emp[] = $key;
            }

            //create dictionary array alphabet
            $dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];
            $count = count($data_emp)-1;

            //foreach  last_name, first_name,date, date & shift_code, tar, abs, nd,rot,rotdo
            foreach($data as $key => $value){

                $last_name[$value->employee_id][] = $value->last_name;
                $first_name[$value->employee_id][] =$value->first_name;
                ($value->shift_code == "AC" ? $shift_codex = "/" : $shift_codex = $value->shift_code);
                $date[$value->employee_id][] =$value->date;
                $data[$value->employee_id][] = ["date" =>$value->date,"shift_code" => $shift_codex, "time_in" => $value->time_in, "time_out" => $value->time_out, "from" => $value->_from, "to" => $value->_to];
                $tar[$value->employee_id][] = $value->TAR;
                $abs[$value->employee_id][] = $value->ABS;
                $nd[$value->employee_id][] = $value->ND;
                $rot[$value->employee_id][] = $value->ROT;
                $rotdo[$value->employee_id][] = $value->ROTDO;
            }

            //foreach for create condition looping by key
             foreach ($tar as $key => $value) {
                    $arr_key[] = $key;

                    //summary element in array
                    $tar_x[$key] = array_sum($tar[$key]);
                    $abs_x[$key] = array_sum($abs[$key]);
                    $nd_x[$key] = array_sum($nd[$key]);
                    $rot_x[$key] = array_sum($rot[$key]);
                    $rotdo_x[$key] = array_sum($rotdo[$key]);
                }

                foreach ($tar_x as $key => $value) {
                    $count = count($data);


                    for($i = 0 ; $i < $count; $i++){
                      $count = count($date[$key])-1;
                      for($i = 0; $i <= $count; $i++){
                        $index = (integer)substr($date[$key][$i], 8, 2);
                        $j = $dictionary[$index-1];
                        $month = (integer)substr($data[$key][$i]['date'], 5, 2);
                        $year = (integer)substr($data[$key][$i]['date'], 0, 4);
                        $md = (integer)substr($data[$key][$i]['date'], 8, 2);
                        $date_sisx[$key][] = $index;
                        $dx=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));

                              if($data[$key][$i]['shift_code'] == "EL"){
                                    $data_status[$key]["status".$j] = "red";
                            }elseif($data[$key][$i]['shift_code'] == "SL"){
                                    $data_status[$key]["status".$j] = "red";
                            }elseif($data[$key][$i]['shift_code'] == "VL"){
                                    $data_status[$key]["status".$j] = "green";
                             }elseif($data[$key][$i]['date'] == "VL"){
                                    $data_status[$key]["status".$j] = "green";
                            }else{
                                    $data_status[$key]["status".$j] = "white";
                            }

                          
                            if($data[$key][$i]['time_in'] > $data[$key][$i]['from']){
                                    $clock = $data[$key][$i]['time_in']-$data[$key][$i]['from'];
                                    $data_er[$key][$j] = $clock."m";
                                     $data_status[$key]["status".$j] = "yellow";
                            }elseif($data[$key][$i]['to'] > $data[$key][$i]['time_out']){
                                    $clock = $data[$key][$i]['to']-$data[$key][$i]['time_out'];
                                    $data_er[$key][$j] = $clock."m";
                                    $data_status[$key]["status".$j] = "yellow";
                            }  elseif($data[$key][$i]['time_in'] == "00:00:00" || $data[$key][$i]['time_out'] == "00:00:00"){
                                    $data_er[$key][$j]= "DO";
                                    $data_status[$key]["status".$j] = "white";
                            }else{
                                  $data_er[$key][$j]= $data[$key][$i]['shift_code'];
                            }

                            if(date("l", mktime(0, 0, 0, $month,$md, $year)) == "Sunday"){
                                         $data_status[$key]["status".$j] = "brown";
                            }
                        }
                    }

                    

                    foreach ($data_er as $key => $value) {
                                $dat[] = $value;
                    }

                     foreach ($data_status as $key => $value) {
                                $status[] = $value;
                    }
                }

                $count_ex = count($data_emp)-1;
              
                for($i = 0; $i <= $count_ex; $i++){
                  $datap[] = ["tar" => $tar_x[$arr_key[$i]], "abs" => $abs_x[$arr_key[$i]], "nd" => $nd_x[$arr_key[$i]][0], "rot" => $rot_x[$arr_key[$i]], "rotdo" => $rotdo_x[$arr_key[$i]], "last_name" => $last_name[$arr_key[$i]][0], "first_name" => $first_name[$arr_key[$i]][0]];
                    $y[] = array_merge_recursive($datap[$i],$dat[$i]);
                    $x[] = array_merge($y[$i],$status[$i]);
                }

                        $between = $this->GetDays($date1,$date2);
                       foreach ($between as $key => $value) {
                           $fin[] = ["date" => $value];
                       }
                    $comment = \DB::SELECT("select * from att_cutoff_record where cutoff_periot='$attendance' ");
                    ($comment == null ? $comment = [] : $comment = $comment);
                    $finally = array_merge_recursive([$x],["date" => $fin],["comment" => $comment]);
                  
                 return $cutOff->indexAcess($finally,$access[3]);

        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access" => $access[3]],$status);
        }
    }


    public function insert_comment(){
              /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $att_record = new  Attendance_cutoffrecordModel;
            $cutOff = new Attendance_cutoffrecordModel;
            $input = \Input::all();
            $attendance = \Input::get("attendance");
            $comment = \input::get("comment");

            $rule = [ "attendance" => 'required', "comment" => "required"];
            $validation = \Validator::make($input, $rule);
 
            if($validation->fails()){
                    $check = $validation->errors()->all();
                      return $att_record-> getMessageAccess($check,500,[],$access[3]);
            }else{
                    $db = \DB::SELECT("CALL insert_att_cutoff_record('$attendance','$comment')");
                    if( isset($db)){
                        return $att_record-> getMessageAccess("Insert data comment success ",200,[],$access[3]);
                    }else{
                        return $att_record-> getMessageAccess("Failed to insert data comment",500,[],$access[3]);
                    }
            }
           }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access" => $access[3]],$status);
        }

    }

}
