<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Attendance\cutOff\Attendance_cutoffrecordModel;
use Larasite\Model\fixedschedule_list_Model;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;

class FixedSchedule_Ctrl extends Controller {
	protected $form = "";

	public function __construct(){
		$key = \Input::get('key');
		$keys= base64_decode($key);
		$test = explode('-',$keys);
		$data = $test[1];

		$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
		$db_data =  $db[0]->local_it;

			if($db_data == 1){
				return $this->form = 62;
			}else{
				return $this->form = 63;
			}
	}

	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			$fix = new fixedschedule_list_Model;
			$department = \DB::SELECT("select * from department where id<>1");
			$job = \DB::SELECT("select id,title from job");
			$data = ["department" => array_merge($department,[["id" => 0, "name" => "All","description" => ""]]), "job" => array_merge($job,[["id" => 0, "title" => "All"]]) ];
			return  $fix->indexAcess($data,$access[3]);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
		}


	}

	public function index_department()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			$fix = new fixedschedule_list_Model;
			$department = \DB::SELECT("select id,name from department where id<>1");
			//$job = \DB::SELECT("select id,title from job");
			//$data = ["department" => array_merge($department,[["id" => 0, "name" => "All","description" => ""]]), "job" => array_merge($job,[["id" => 0, "title" => "All"]]) ];
			return  $fix->indexAcess($department,$access[3]);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
		}


	}

	public function index_shift()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			$fix = new fixedschedule_list_Model;
			$shift = \DB::SELECT("CALL view_workshift_fixed_schedule");
			//return $shift;
			foreach($shift as $key => $value){
				$data[] =  ["id" => $value->shift_id, "shif_code" => $value->shift];
			}

			return  $fix->indexAcess($data,$access);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
		}


	}
	// FOR VIEW LEGENDS :: SCHEDULE LIST
	public function index_shiftV2_legends()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			$fix = new fixedschedule_list_Model;
			$department = \Input::get('Department');
			$year = \Input::get('years');
			$month  = \Input::get('month')+1;
			$search = \DB::SELECT("SELECT distinct t1.employee_id, concat(t1.first_name,' ',t1.middle_name, t1.last_name) as employee, t3.date, t2.shift_code,t5.name as department
						  FROM emp t1
						  LEFT JOIN department t5 ON t5.id = t1.department
						  LEFT JOIN att_schedule t3 ON t3.employee_id = t1.employee_id
						  LEFT JOIN attendance_work_shifts t2 ON t2.shift_id = t3.shift_id
						  WHERE t1.department = $department
						  AND YEAR( t3.DATE ) =  '$year'
					     AND month( t3.DATE ) = '$month'
						  AND t3.status = 2");
			
			if($search != null){
			
				foreach ($search as $key => $value) {
					$arr[$value->shift_code][] = $value->shift_code;
				}

				foreach ($arr as $key => $value) {
					$arr1[] = $key;
				}

				$arr_string = implode("','",$arr1);

				$data = \DB::SELECT("select concat(shift_code,' : ',_from,' - ',_to) as shif_code from attendance_work_shifts where shift_code in('$arr_string')");
			}else{
				$data = [];
			}
			return  $fix->indexAcess($data,$access);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
		}


	}

	public function index_shiftV2()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			$fix = new fixedschedule_list_Model;
			$department = \Input::get('Department');
			$year = \Input::get('years');
			$month  = \Input::get('month')+1;
			$search = \DB::SELECT("SELECT distinct t1.employee_id, concat(t1.first_name,' ',t1.middle_name, t1.last_name) as employee, t3.date, t2.shift_code,t5.name as department
						  FROM emp t1
						  LEFT JOIN department t5 ON t5.id = t1.department
						  LEFT JOIN att_schedule t3 ON t3.employee_id = t1.employee_id
						  LEFT JOIN attendance_work_shifts t2 ON t2.shift_id = t3.shift_id
						  WHERE t1.department = $department
						  AND YEAR( t3.DATE ) =  '$year'
					     AND month( t3.DATE ) = '$month'
						  AND t3.status = 1");
			
			if($search != null){
			
				foreach ($search as $key => $value) {
					$arr[$value->shift_code][] = $value->shift_code;
				}

				foreach ($arr as $key => $value) {
					$arr1[] = $key;
				}

				$arr_string = implode("','",$arr1);

				$data = \DB::SELECT("select concat(shift_code,' : ',_from,' - ',_to) as shif_code from attendance_work_shifts where shift_code in('$arr_string')");
			}else{
				$data = [];
			}
			return  $fix->indexAcess($data,$access);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
		}


	}


	public function getName()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$att_record = new  Attendance_cutoffrecordModel;
			$fix = new fixedschedule_list_Model;
			$input = \Input::all();
			$name = \Input::get('name');
			$rule = [
					'name' => 'required|Regex:/^[A-Za-z]+$/'
					];
			$validation = \Validator::make($input,$rule);
			$data = \DB::SELECT("CALL Search_emp_by_name('$name')");
			return $fix->searchAcess($validation,$data,$access[3]);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
	}



	public function insert_fix(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$fix = new fixedschedule_list_Model;

			$input = \Input::all();
			$employee = \Input::get('Employee');
			//$check_exist_emp = \DB::SELECT("select * from att_fixed_schedule")
			$department = \Input::get('Department');
			$json = \input::json('Days');

			///check emp 
			$check_exist_emp = \DB::SELECT("select department from emp where employee_id = '$employee' ");
			$check_emp_date = \DB::SELECt("select date from att_fixed_schedule where employee_id = '$employee' order by employee_id desc limit 1");
			if($check_emp_date != null){
				$check_month = $check_emp_date[0]->date;
				$check_month= substr($check_month,5,2);		
			}else{
				$check_month = 0;
			}
		
			if($check_exist_emp[0]->department == null){
				return \Response::json(['header'=>['message'=>"please fill department for employee you selected",'status'=>500],'data'=>[], "access"=>$access],500);
			}elseif($check_month == date('m')){
				return \Response::json(['header'=>['message'=>"employee schedule for this month already exist",'status'=>500],'data'=>[], "access"=>$access],500);
			}else{
						//$rule = ['Employee' => 'required|integer', 'Department' => 'required|integer'];
				foreach ($json as $key => $value) {
					$i = 1;
					$data = cal_days_in_month(CAL_GREGORIAN, date('m')+1,date("y"));
					for($i; $i <= $data; $i++){
						$days= date("l", mktime(0, 0, 0, date('m')+1, $i, date('Y')));
						if($value['day'] == $days){
                            $shiftcode = $value['shiftcode'];
                            
                            $date_m = date('m')+1;
                            /*if($date_m < 10){
                            	$date_m = "Y-0$date_m-$i";
                            }else{
                            	$date_m = "Y-$date_m-$i";
                            }*/
                            $date = date("Y-$date_m-$i");

                            $data_ex[] = ["employee" => $employee, "schedule" => $value['shiftcode'], "date" => $date, "day" => $days];
                        }
							//$data = \DB::SELECT("call insert_att_fixed_schedule('$employee',$department,$shiftcode,'$date','$days')");

					}
				}

				$count = count($data_ex);
				//return [date('m')+1,$data,$input,$data_ex];
				for($i = 0; $i < $count; $i++){
					$date = $data_ex[$i]['date'];
					$day = $data_ex[$i]['day'];
					//$department = $data_ex[$i]['department'];
					$employee = $data_ex[$i]['employee'];
					$schedule = $data_ex[$i]['schedule'];
					$ji_sheet =  \DB::SELECT("select *  from att_schedule where employee_id = '$employee'  and  date='$date' and shift_id=$schedule");
					if($ji_sheet != null){
						$message = 'already insert schedule'; $status = 200; $data=[];
						return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
					}else{
						$data = \DB::SELECT("call insert_att_fixed_schedule('$employee',$schedule,'$date','$day')");	
					}

				}

				$arr = [];
				foreach ($data_ex as $key => $value) {
					$arr[$value['day']][] = $value;
				}

				$datap = [];

				foreach ($arr as $key => $value){
					$shiftcodex = $value[0]['schedule'];
					$db = \DB::SELECT("call view_workshift_fixed_schedule_id($shiftcodex)");
					$datap =array_merge($datap,[$key => $db[0]->shift]) ;
				}

				$name_employee = \DB::SELECT("call search_employee_by_id('$employee')");
				$name_department = \DB::SELECT("select department.name from att_fixed_schedule,department where att_fixed_schedule.employee_id='$employee' and att_fixed_schedule.department=department.id");
				$explode = explode(" ",$name_employee[0]->name);
				if(!isset($explode[2]) || $explode[2] == null){
					$explode[2] = "-";
				}
				$datax = ["last" => $explode[2], "firstname" => $explode[0],  "department" => $name_department[0]->name, "employee_id" => $employee] ;
				$dataxp = [array_merge($datap, $datax)];
				return  $fix->indexAcess($dataxp,$access[3]);
				}
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
		}

	}

	public function update_fix(){
		set_time_limit(0);
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
			$fix = new fixedschedule_list_Model;

			$input = \Input::all();
			// $employee = \Input::get('Employee');
			// $department = \Input::get('Department');
			$json = \input::get('data');

			//$rule = ['Employee' => 'required|integer', 'Department' => 'required|integer'];
			$count = count($json);

	//		$data = [];
			//for($j = 0; $j <= $count )

		// for($j = 0; $j < $count; $i++){
		// 	$ccount_r = count($json)
			for($i = 0; $i <= 6; $i++){
				// foreach ($json[$i] as $key ) {
			  // return $key;
					// foreach ($value as $keys => $values) {
					// 	# code...
					// }

 					$Monday = explode(":", $json['Monday']);
					$Tuesday = explode(":", $json['Tuesday']);
					$Wednesday = explode(":", $json['Wednesday']);
					$Thursday = explode(":", $json['Thursday']);
					$Friday = explode(":", $json['Friday']);
					$Saturday = explode(":", $json['Saturday']);
					$Sunday = explode(":", $json['Sunday']);
					$data = [['last'  => $json['last'],
						"firstname" => $json['firstname'],
						"department" => $json['department'],
						"employee" => $json['employee_id'],
						"date" => [
								[
								    "days" => "Monday",
								    "shift_code" => $Monday[0]
								],
								[
								    "days" => "Tuesday",
								    "shift_code" => $Tuesday[0]
								],
								[
								    "days" => "Wednesday",
								    "shift_code" => $Wednesday[0]
								],
								[
								    "days" => "Thursday",
								    "shift_code" => $Thursday[0]
								],
								[
								    "days" => "Friday",
								    "shift_code" => $Friday[0]
								],
								[
								    "days" => "Saturday",
								    "shift_code" => $Saturday[0]
								],
								[
								    "days" => "Sunday",
								    "shift_code" => $Sunday[0]
								]
							]
						]];
				//}

			
			//foreach ($data as $key => $value) {
				$countr =  count($data)-1;

				// for($i = 0; $i <= $countr; $i++){
				//
				// 	$count_ex = count($data[$i]['date']);

					for($k = 0; $k <= 6; $k++){

						$department     = $data[0]['department'];
						$employee       = $data[0]['employee'];
						$day            = $data[0]['date'][$k]['days'];
						$shiftcode      = $data[0]['date'][$k]['shift_code'];

						$data_shiftcode = \DB::SELECT("CALL View_WorkSift_byName('$shiftcode') ");
						$shift_idx      = $data_shiftcode[0]->shift_id;
						$department_ex  = \DB::SELECT("select id from department where name='$department' ");
						$department_id  = $department_ex[0]->id;

						//return $department_id.$shift_idx;
						$f[] = [$employee,$day,$department_id,$shift_idx];

						$fix_update = \DB::SELECT("CALL update_att_fixed_schedule('$employee','$day',$department_id,$shift_idx)");
					    if( isset($fix_update) and $fix_update[0]->status == "no"){

					    	$message = "Already approved can't update "; $status = 500; $data=[];
							return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
					    }else{
					    	$countx = count($fix_update);
					    	for($s = 0; $s < $countx; $s++){
					    		$date_new =  $fix_update[$s]->status;
					    		$update_att_schedule  =  \DB::SELECT("update att_schedule set shift_id=$shift_idx where employee_id='$employee' and status = 1 and date = '$date_new'");
					    	}
					    }
					}
				//}
		//	}

		}

				return  $fix->indexAcess($json,$access[3]);

		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
		}
	}

	public function delete_fix($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
		$fix = new fixedschedule_list_Model;
		if($id != null){
			$explode_id = explode(",",$id);
			$count = count($explode_id);

			foreach ($explode_id as $key => $value) {
				$db = \DB::SELECT("CALL delete_fixed_schedule('$value')");
			}
			return $fix->getMessageAccess("Success",200,[], $access[3]);

		}

		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
	}

	public function getView(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

		if($access[1] == 200){
//			$att_record = new  Attendance_cutoffrecordModel;
			$fix = new fixedschedule_list_Model;
			$input = \Input::all();
			$emp = \Input::get('Employee');
			$department = \Input::get('Department');
			$job = \Input::get('jobTitle');

			if(!isset($department)){
				$department = 0;
			}

			if(!isset($job)){
				$job = 0;
			}

			if(!isset($emp)){
				$emp = 0;
			}

			if(isset($department) == 0  && isset($job) == 0){
				return $fix->getMessageAccess("Please not choose all for department or job title in the same request  ",200,[], $access[3]);
			}

			$data = \DB::SELECT("CALL view_fixed_schedule_new('$emp',$department,$job)");
			$count =  count($data);
			$arr = [];
			foreach ($data as $key => $value) {
				$arr[$value->employee_id][] = [$value->day  => $value->schedule];
			}

			$arr_emp = [];
			foreach ($data as $key => $value) {
				$arr_emp[$value->employee_id][] = ["last" => $value->last , "first_name" => $value->first_name, "department" => $value->department, "employee_id" => $value->employee_id];
			}

			
			if($arr_emp == null){
				$data_ex = null;
			}else{
				foreach($arr_emp as $key => $value){
					$data_ex[] = [
							'employee_id'  => $arr_emp[$key][0]['employee_id'],
							'last'  => $arr_emp[$key][0]['last'],
							"firstname" => $arr_emp[$key][0]['first_name'],
							"department" => $arr_emp[$key][0]['department'],
							"Monday" => $arr[$key][0]["Monday"],
							"Tuesday" => $arr[$key][1]["Tuesday"],
							"Wednesday" => $arr[$key][2]["Wednesday"],
							"Thursday" => $arr[$key][3]["Thursday"],
							"Friday" => $arr[$key][4]["Friday"],
							"Saturday" => $arr[$key][5]["Saturday"],
							"Sunday" => $arr[$key][6]["Sunday"]
						          ];
				}
			}
			if($data_ex == null){
				$data_ex = [];
			}
			return $fix->indexAcess($data_ex, $access[3]);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access],'data'=>$data],$status);
	}
}
