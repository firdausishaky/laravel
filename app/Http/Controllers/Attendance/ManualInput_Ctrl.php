<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilage;
use Larasite\Model\Attendance\Schedule\manualInput_att_model;
use Larasite\Library\FuncAccess;
class ManualInput_Ctrl extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     **/

    protected $form = "";

    public function __construct(){
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->form = "63";
		}else{
			return $this->form = "62";
		}
      }

    public function index(){

    	       /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); 
       	      if($access[1] == 200){
       	      
                       $att_record = new manualInput_att_model;
                       return $att_record->get($access[3]);
                 }else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
         return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
     }
 
    
    public function search_data(){
    	      /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
       	      if($access[1] == 200){
	                $att_record = new manualInput_att_model;

	                $input = \Input::all();

	                $date = \Input::get('date');
	                $employee = \Input::get('employee');
	                $department = \Input::get('department');
	                $shift_code = \Input::get('shift_code');
	                $job_title= \Input::get('job_title');

	                if(!isset($department)){
	                	$department =0;
	                }
	                 if(!isset($shift_code)){
	                	$shift_code=0;
	                }

	                 if(!isset($job_title)){
	                	$job_title=0;
	                }

	                 if(!isset($employee)){
	                	$employee=0;
	                }

	                 if(!isset($date)){
	                	$date= ' ';
	                }

	                $filename ='app/config_manual.json';
		     		$fp = fopen(storage_path($filename),'w');
	                $datas[] = ['date' => $date];
	                fwrite($fp, json_encode($datas));
	                fclose($fp);

	                //$db = \DB::SELECT("call view_manual_input_attRecord('$date','$employee',$department,$shift_code,$job_title)");
	                //return $date;
	                $arr = ['department','shift_code','job_title','employee','date'];

	                $depart = "and t2.department =  $department ";	
	                $shift = "and t3.shift_code =  $shift_code ";
	                $job = "and t7.job =  $job_title ";
	                $employ = "and t2.employee_id =  '$employee' ";
	                $dat = "and t5.date =  '$date' ";
	                $base_db = "select concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name) as Name ,
							t5.date as date,
							t5.status as status,
							concat(t3.shift_code,' : ',time_format(t3._from,'%k:%i'),' - ',time_format(t3._to,'%k:%i'))as Schedule,
							t4.name as department,
							t8.title,
							null as timeIn, 
							null as timeOut
							from emp t2
							left join att_schedule t5 on t5.employee_id=t2.employee_id 
							left join attendance_work_shifts t3 on t3.shift_id=t5.shift_id
							left join department t4 on t2.department =  t4.id
							left join job_history t7 on t7.employee_id=t2.employee_id
							left join job t8 on t8.id = t7.job
							where t5.date not in (select date from biometrics_device)
							";
					$end_db = "and t5.status = 2 order by t5.date ASC";

	                foreach ($arr as $key => $value) {
	                	if($value  == "department" &&  $$value != 0){
	                	$base_db .= $depart;
	                	}
	                	if($value  == "$shift_code" &&  $$value != 0){
	                	$base_db .= $shift;
	                	}
	                	if($value  == "job_title" &&  $$value != 0){
	                	$base_db .= $job;
	                	}
	                	if($value  == "employee" &&  $$value != 0){
	                	$base_db .= $employ;
	                	}
	                	if($value  == "date" &&  $$value != ' '){
	                	$base_db .= $dat;
	                	}
	                }

	                //return $base_db;
	                $db = \DB::SELECT("$base_db");
	                return $att_record->index($db, $access[3]); 
	       }else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
         return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
     }
    public function insert_data(){
	    $att_record = new manualInput_att_model;
	    $filename ='app/config_manual.json';
	    $data_date = json_decode(file_get_contents(storage_path($filename)),1);
	    $data_date = $data_date[0]['date'];
    	$key = \Input::get('key');
    	$key_ex = $att_record->key($key);

    	$input_json = \Input::json('data');
    	$count = count($input_json);
	    	  // return "test";


	    	foreach ($input_json as $key => $value) {

		    	$employee = $value['Name'];
		    	$timeIn = $value['timeIn'];
		    	$timeOut = $value['timeOut'];

				$employee_ex =  (bool)ctype_alpha($employee);
			 	$timeIn_ex = (bool)preg_match('/[0-2][0-9]:[0-5][0-9]/',$timeIn);
			  	$timeOut_ex =(bool)preg_match("/[0-2][0-9]:[0-5][0-9]/", $timeOut);

				if($employee != null &&  $timeIn != null  && $timeOut != null && $data_date != null ){
					
					$data_emp = $att_record->search_employee($employee);
					$chekcHR = \DB::SELECT("select  role.role_name from ldap,role where ldap.employee_id = '$key_ex'  and  ldap.role_id = role.role_id");

					$exploded = explode(' ',$chekcHR[0]->role_name);
					//print_r($exploded);
					$count  = count($exploded);
					$arr  = ['human','human resource','resource','hr','superuser'];
					if($count > 1){
					  	for($i = 0 ; $i < $count; $i++){
					  		if(in_array(strtolower($exploded[$i]),$arr))
					  			{ $role_data = "hr"; }
					  	}
					  	
					  	if(!isset($role_data) || $role_data == null){
					  		$role_data =  'notuser';
					  	}
					  	print_r('3');
					}else if(in_array(strtolower($exploded[0]),$arr)){ 
						$role_data = "hr";
						print_r('2');
					}else{ 
						if(isset($chekcHR[0]->role_name)){
							$role_data = $chekcHR[0]->role_name;	
						}else{
							$role_data = 'notuser';
						}
						print_r('1');
					}
				    
				    if($role_data == "hr"){
					    $data = \DB::select("select id,time_in,time_out from biometrics_device where  employee_id='$data_emp' and  date='$data_date' ");
						$check = \DB::SELECT("select req_in_out_id from att_time_in_out_req where employee_id='$data_emp' and status_id = 1 and date = '$data_date'");
						if($check != null){
							return $att_record->getMessage("failed already request , please rejext last request to add new reques for same date",500, null);
						}
							if(!$data){
								\DB::SELECT("insert into att_time_in_out_req(employee_id,date,req_in,req_out,type_id,created_at) values ('$data_emp','$data_date','$timeIn','$timeOut',9,now())");
								//if($ins1){								
								\DB::SELECT("insert into att_schedule_request (date_request,availment_date,employee_id,type_id,request_id) VALUES (now(),'$data_date','$data_emp',9,
										(select req_in_out_id from att_time_in_out_req where employee_id='$data_emp' and date ='$data_date' and type_id = 9 limit 1))");
								//}
								//$db = \DB::SELECT("call insert_manual_attRecord('$data_date','$data_emp','$timeIn','$timeOut')");
							}
					   //  	try{
						  //   	return $data;
								// if($data[0]->time_in == null  && $data[0]->time_out == null){
						  //       }
					   //  	}catch(Exception $e){

					   //  	}
					}else{ return $att_record->getMessage("unauthorized user, need HR for execute",500, null);}

				}else{
				return $att_record->getMessage("failed please check it with carefully your input",500, null);
				}
			}
			return $att_record->getMessage("success",200,[]);
	}
}