<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use League\Csv\Reader;
use Larasite\Library\FuncAccess;
use Illuminate\Http\Request;
use Exception;

class ScheduleImport_Ctrl extends Controller {

          protected $form = "";

         public function __construct(){

            //give access permission
            $key = \Input::get('key');
            $keys= base64_decode($key);
            $test = explode('-',$keys);
            $data = $test[1];

            $db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
            $db_data =  $db[0]->local_it;

               if($db_data == 1 && $db_data = 3){
                  return $this->form = "57";
               }else{
                  return $this->form = "58";
               }
         }

         public function getfile(){



            $ext = '.xlsx';
             $files = 'xls_schedule';
            $path = storage_path()."/".$files.$ext;
            $file = \File::get($path);
            $type = \File::mimeType($path);
            return \Response::make($file,200,['Content-Type'=>$type]);

        }

         public function api_permission(){
               /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
             if($access[1] == 200){
                        if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0 ){
                                 return \Response::json(['header'=>['message'=>"Unauthorized",'status'=>500,"access" => $access[3]],'data'=>[]],500);
                        }else{
                                  $key = \Input::get('key');
                                   $keys= base64_decode($key);
                                   $test = explode('-',$keys);
                                   $data = $test[1];
                                    $data_ex = \DB::SELECT("CALL view_department1");
                                 return \Response::json(['header'=>['message'=>"Success",'status'=>200,"access" => $access[3]],'data'=>$data_ex],200);
                        }
              }
            else{
               $message = $access[0]; $status = $access[1]; $data=$access[2];
            }
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
         }


  public function index()
  {
    /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
    if($access[1] == 200){
      $data = \Input::json('data');
      if ($data['name'] == ""){
        return \Response::json(['header'=>['message'=>'Can not import file cause an empty name field' ,'status'=>500],'data'=>null],500);
      }
      foreach ($data as $key => $value) {
        $insertSchedule = \DB::select("");
        if ($insertSchedule[0]->Status != "OK")
          return \Response::json(['header'=>['message'=>'Can not import file cause an empty name field' ,'status'=>500],'data'=>null],500);
        else
          return \Response::json(['header'=>['message'=>'Import new schedule success' ,'status'=>200, 'access'=>$access[3]],'data'=>null],200);
      }
    }
    else{
      $message = $access[0]; $status = $access[1]; $data=$access[2];
    }
    return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
  }

   // import schedule list from excel
  public function import()
  {
      /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'store');
       if($access[1] == 200){
       set_time_limit(0);

       $data_record = \Input::get('data');
       if(isset($data_record[0]['date1']) && $data_record[0]['date1'] != null ){
           $monthx = explode("-",$data_record[0]['date1']);
           $monthx = $monthx[1]; 
        }else{
           $monthx = date('m');
        }    
       $scheduleLength = cal_days_in_month(CAL_GREGORIAN, $monthx,date("Y"));
      
       $count = count($data_record);
      \DB::beginTransaction();
      try{
          for ($i=0; $i < $count; $i++) {
             for ($j=0; $j < $scheduleLength; $j++) {
                     $dictionary = ["b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];
                     $key = $j+1;

                     $dt = $data_record[$i]['date'.$key];
                     $empId = $data_record[$i]['employee_id'];
                     $temp3 = $data_record[$i][$dictionary[$j]];

                     //if($count < ($scheduleLength + 1))throw new \Exception("error : schedule days for this month less than ".count($data_record[$i]));
                     if($temp3 == null) throw new \Exception("error : please fill all blank schedule or check total days for schedule month right");
                $shiftID = \DB::select("SELECT shift_id FROM attendance_work_shifts WHERE shift_code = '$temp3' ");
               if ($shiftID == null) return $temp3;
               $shiftID = $shiftID[0]->shift_id;
               $temp[] = [$empId, $dt, $temp3];
               // save the schedule to database

              $chek_exist = \DB::SELECT("select * from att_schedule where employee_id = '$empId' and date = '$dt' and status != 2 ");
              if($chek_exist != null){
                   if($chek_exist[0]->status != 2){
                      \DB::select("UPDATE att_schedule SET shift_id = $shiftID, status = 1 where  employee_id = '$empId' and date = '$dt' ");
                      \DB::select("UPDATE att_schedule_slave SET shift_id = $shiftID where  employee_id = '$empId' and date = '$dt' ");
                    }else{
                      return "cant insert already insert and approved";
                    }
              }else{
                  $team_building =  \DB::SELECT("select * from attendance_work_shifts where shift_id = $shiftID");
                  if($team_building !=  null){
                    if($team_building[0]->shift_code  == 'TB' || $team_building[0]->shift_code  == 'tb'){

                      $from  =  $team_building[0]->_from;
                      $to  =  $team_building[0]->_to;
                      \DB::select("INSERT INTO biometrics_device (device_number,date, time_in,time_out,employee_id,authen_id,input_) VALUES ('001','$dt','$from','$to','$empId',0,1)");
                    }
                  }

                  $select   =  \DB::SELECT("select date_of_birth from emp where employee_id = '$empId' ");
                  $date_of_birth  =   $select[0]->date_of_birth;

                  $explode_birth  = explode('-',$date_of_birth);
                  $explode_dt  = explode('-',$dt);

                  if($explode_birth[1] ==  $explode_dt[1] &&  $explode_birth[2] == $explode_dt[2]){
                    \DB::select("INSERT INTO att_schedule (employee_id, date, shift_id,status) VALUES ('$empId','$dt',45,1)");
                  }else{
                    \DB::select("INSERT INTO att_schedule (employee_id, date, shift_id,status) VALUES ('$empId','$dt',$shiftID,1)");
                  }


                  
                  \DB::select("INSERT INTO att_schedule_slave (employee_id, date, shift_id) VALUES ('$empId','$dt',$shiftID)");
              }  
            }
         }
         \DB::commit();
         return \Response::json(['header'=>['message'=>'Sucessfully import new schedule' ,'status'=>200, 'access'=>$access[3]],'data'=>null],200);
      }catch(\Exception $e){
         \DB::rollBack();
         if(gettype($e) == "object"){
            $explode =  explode("'",$e);
            $e = $explode[3]; 
         }
         return \Response::json(['header'=>['message'=>$e ,'status'=>500, 'access'=>$access[3]],'data'=>[]],500);
      }
   }
      else{
         $message = $access[0]; $status = $access[1]; $data=$access[2];
      }
      return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
   }

   public function createDepartment()
   {
      $rule = ['name' => 'regex:/(^[A-Za-z0-9\'\",.-\/\(\) ]+$)+/'];
      $validator = \Validator::make($name,$rule);
      if ($validator->fails()) {
         $val = $validator->errors()->all();
         return \Response::json(['header'=>['message'=>$val[0] ,'status'=>500, 'access'=>$access[3]],'data'=>null],500);
      }
      else{
         $createDep = \DB::select("INSERT INTO department (name) VALUES ('$name')");
      }
   }

   public function insertData()
   {
      $insert = \DB::select("CALL Insert_ScheduleList('$name', '$date', $shift_id)");
      if ($insert){
         return \Response::json(['header'=>['message'=>"Import schedule success" ,'status'=>200],'data'=>$insert[0]],200);
      }
      else{
         return \Response::json(['header'=>['message'=>"Can not import schedule" ,'status'=>500],'data'=>null],500);
      }
   }

  public function  importDataXLS(){
    $data = \Input::get('data');
    $departmen_emp = \Input::get('department');
    $implode_depart = implode(",",$departmen_emp);
    if($implode_depart == null){
      return \Response::json(['header'=>['message'=>"Please select one or many items in departments, before upload ",'status'=>500],'data'=>null],500);
    }
    
    $now_month = substr($data[0]['date1'],5,2);
    $now_year = substr($data[0]['date1'],0,4);

    $date_no = "";
    $dictionary = ["b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];

    
    $emp_exist = \DB::SELECT("select employee_id from att_fixed_schedule group by employee_id");


    //CHECK EMPLOYEE
    foreach ($data as $key => $value) {
      $emp = $value['employee_id'];
      $db_arr = \DB::SELECT(" select employee_id from emp where employee_id='$emp' and department in($implode_depart) ");
      if($db_arr != null){
        $arr_id_emp[] = $value['employee_id'];
        $arr_emp[] = $data[$key];
        $arr_empx[] = $data[$key];
      }else{
        $message_x[] = [ "employee_id" => $value['employee_id'], "date" => null,  "note"  => "employee not exist , in line ".($key+1) ];
        $arr_non[] = $data[$key]['employee_id'];
      }
    }

    // CHECK EMPLOYEE FROM EXCEL IF NOT FOUND IN DATABSE
    if(!isset($arr_emp)){
      return \Response::json(['header'=>['message'=>"Employee not available with selected department",'status'=>500],'data'=>[]],500);
    }

    $count = count($arr_emp);
    $date = cal_days_in_month(CAL_GREGORIAN, $now_month,$now_year);
    $e_max = $dictionary[$date-1];
    $message = "";
    
    for($i = 0; $i <=($count-1); $i++) {
      for ($j = 0; $j <=($date-1); $j++){
          $key = $j+1;

            $date_m = $arr_emp[$i]['date'.$key];      
            $emp_id = $arr_emp[$i]['employee_id'];
            $db = \DB::SELECT("select schedule_id from att_fixed_schedule where employee_id='$emp_id' and date='$date_m' ");

            if($db != null){
              if ($arr_emp[$i][$dictionary[$j]] == NULL){
                $depart = \DB::SELECT("select department.name from emp,department where emp.employee_id='$emp_id' and department.id=emp.department ");
                $department = "";
                if($depart != null){
                  $department= $depart[0]->name;
                }else{
                  $department = null;
                }

                $t = strlen($j);
                if($t == 1){
                  $k = "0".$j+1;
                }else{
                  $k = $j +1;
                }

                $d_date = $now_year.'-'.$now_month.'-'.$k;
                $d_db = \DB::SELECT("select attendance_work_shifts.shift_code as shift_code from att_fixed_schedule,attendance_work_shifts where att_fixed_schedule.employee_id='$emp_id' and att_fixed_schedule.date='$d_date' and attendance_work_shifts.shift_id=att_fixed_schedule.schedule_id ");
                $arr_emp[$i][$dictionary[$j]] = $d_db[0]->shift_code;
              }
              if($arr_emp[$i][$dictionary[$j]] != NULL){
                $depart = \DB::SELECT("select department.name from emp,department where emp.employee_id='$emp_id' and department.id=emp.department ");
                $department = "";
                if($depart != null){
                  $department= $depart[0]->name;
                }else{
                  $department = null;
                }
                
                $arr_emp[$i][$dictionary[$j]] =  $arr_emp[$i][$dictionary[$j]];
              }
            }else{
              $depart = \DB::SELECT("select department.name from emp,department where emp.employee_id='$emp_id' and department.id=emp.department ");
              $department = "";
              if($depart != null){
                  $department= $depart[0]->name;
              }else{
                 $department = null;
              }
              $work_s = $arr_emp[$i][$dictionary[$j]];
              $SDC = \DB::SELECT("select * from attendance_work_shifts where shift_code='$work_s' ");
              if($SDC != null){
                $arr_emp[$i][$dictionary[$j]] =  $arr_emp[$i][$dictionary[$j]];
              }else{
                $arr_emp[$i][$dictionary[$j]] = null;
              }
            }       
        }
        $date_mag[$emp_id] = array_merge($arr_emp[$i],["department" => $department]);
      }
    
      foreach ($date_mag as $key => $value) {
        $finale[] = $value;
      }
      
      if(!isset($message_x)){
        $message = $message;
      }else{
        if($message == null){
          $message = $message_x;
        }else{
          $message = array_merge($message, $message_x);
        }
      }
       
      if(isset($arr_non)){
        $data = implode(",",$arr_non);
      }

      $month = $now_month;
      $from = $now_year."-".$now_month."-01";
      $to  = $now_year."-".$now_month."-".$date;
      $arr_id_emp = implode(",",$arr_id_emp);
      $arr_id_emp = "'".$arr_id_emp."'";
      $emp_db = \DB::SELECT("select  att_fixed_schedule.employee_id, department.name as department, att_fixed_schedule.date, attendance_work_shifts.shift_code  from att_fixed_schedule,department,attendance_work_shifts where date between '$from' and '$to' and  att_fixed_schedule.employee_id not in($arr_id_emp) and att_fixed_schedule.department=department.id and att_fixed_schedule.schedule_id=attendance_work_shifts.shift_id order by date ASC");

      foreach ($emp_db as $key => $value) {
        $emp_arr_data[$value->employee_id][] = ["employee_id" => $value->employee_id, "date" => $value->date, "department" => $value->department, "shift_code" => $value->shift_code];
      }

      if(!isset($emp_arr_data)){
        $finale = $finale;
      }else{
        foreach ($emp_arr_data as $key => $value) {
          $data_emp_ext[] = $key;
        }
      }
     

      if($message == null){
        $message ="success";
      }else{
        foreach ($message as $key => $value) {
          $arr[$value['employee_id']][] =["employee_id" => $value['employee_id'], "date" => $value['date'], 'note' => $value['note'] ];
        }

        foreach ($arr as $key => $value) {
          if($value[0]['date']  != null){
              $count = count($value)-1;
              $dt[] = ["employee_id" => $key, "date" => $value[0]['date'].'-'.$value[$count]['date'], "note" => "please make a sure shift code data with range data for ".$value[0]['date'].' to '.$value[$count]['date']." same as with data in database" ];
              }else{
                $dt[] = ["employee_id" => $key, "date" => "-", "note" => "please make a sure employee id or department of employee id ".$key." , employee id or department not found" ];
              }
        }

        foreach ($arr as $key => $value) {
          $text[] = $key;
        }
      }


      if(isset($data)){
        if(!isset($dt)){
          $dt ="success"; 
        }
          return \Response::json(['header'=>['message'=>$dt,'status'=>200],'data'=>$finale],200);
      }else{
        return \Response::json(['header'=>['message'=>"Success",'status'=>200],'data'=>$finale],200);
      }
   }
}

