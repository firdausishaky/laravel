<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Attendance\Schedule\unapproved_schedule_list;
use Larasite\Model\Attendance\Request\ScheduleRequestList_model as requestList;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;

class ScheduleList_Ctrl extends Controller {



	protected $form = "";

	public function __construct(){

		//give access permission
		$key = \Input::get('key');
		$keys= base64_decode($key);
		$test = explode('-',$keys);
		$data = $test[1];

		$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
		$db_data =  $db[0]->local_it;

			if($db_data == 1 && $db_data = 3){
				return $this->form = "60";
			}else{
				return $this->form = "59";
			}
	}


	public function index(){
		  /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
       		 if($access[1] == 200){
       		 	$unapproved = new unapproved_schedule_list;
       		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				return $unapproved->getMessage('Unauthorized',500,[], $access[3]);
			}else{
				$unapproved = new unapproved_schedule_list;
				$model = new requestList;
				$input = \Input::get("key");
				$explode = explode("-",base64_decode($input));
				$empID = $explode[1];
				$hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$empID' AND t2.role_name = 'hr'");
					$adminex = \DB::SELECT("select role.role_name from ldap,role where ldap.employee_id=$empID and ldap.role_id=role.role_id ");	
					if($hr != null){
						$data = \DB::SELECT("select name,id from department where id<>1 ");
					}elseif($adminex[0]->role_name == "adminEx"){
						$data = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");
					}elseif ($adminex[0]->role_name == "SuperUser") {
						$data = \DB::SELECT("select id,department.name from department where id <>1 ");
					}elseif($adminex[0]->role_name == "admin"){
						$data = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");	
					}elseif($adminex[0]->role_name == "user"){
						$data = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");	
					}elseif($adminex[0]->role_name == "adminIt"){
						$data = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");	
					}else{
						$data = null;
					}			$date = \DB::SELECT("select date from att_schedule");

				if($date != null){
				foreach($date as $value){
					$data_year = $value->date;
					$year = substr($data_year,0,4);
					   $datax[$value->date][]= $value->date;
				}

				foreach($datax as $key => $value){
					$datex[] = substr($key,0,4);
				}

				foreach($datex as $value){
					$date_datex[$value] =$value;
				}

				$unique = array_unique($date_datex);

				foreach ($unique as $key => $value) {
					$test[] = ["year" => $value];
				}
			}else{
				$test = null;
			}
				$record = ["department" => $data, "year" => $test];
				return $unapproved->getMessage("success",200,$record, $access[3]);
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
	}

	public  function index_shift(){
	 /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
   		 	if($access[1] == 200){
   		 	$unapproved = new unapproved_schedule_list;
       		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				return $unapproved->getMessage('Unauthorized',500,[], $access[3]);
			}else{
				$db = \DB::SELECT("CALL View_WorkShift");
				foreach ($db as $key => $value) {
					$db_ext[] = ["shift_id" => $value->shift_id, "shift_code" => $value->shift_code];
				}

				return $unapproved->index($db_ext, $access[3]);

			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
	}

	public  function index_shiftcode(){
	 /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
   		 	if($access[1] == 200){
   		 	$unapproved = new unapproved_schedule_list;
       		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				return $unapproved->getMessage('Unauthorized',500,[], $access[3]);
			}else{
				$db = \DB::SELECT("CALL View_WorkShift");
				foreach ($db as $key => $value) {
					$db_ext[] = ["shift_data" => $value->shift_code." : ".$value->_from." - ".$value->_to];
				}

				return $unapproved->index($db_ext, $access[3]);

			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
	}

	public function search(){
		  /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		 if($access[1] == 200){
       		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				$data = []; $message = 'Unauthorized'; $status = 200;
			}else{

				$unapproved = new unapproved_schedule_list;
				$input = \Input::all();
				$department = \Input::get('Department');
				$year = \Input::get('years');
				$month  = \Input::get('month')+1;

				if($department == null ){
					return $unapproved->getMessage('Please choose one of the item department in combo box',500,[], $access[3]);
				}
				if($year == null){
					return $unapproved->getMessage('Please choose one of the item year in combo box',500,[], $access[3]);
				}
				if(!isset($month)){
					return $unapproved->getMessage('Please choose one of the item month in combo box',500,[], $access[3]);
				}

				$rule = [ 'Department' => 'required|integer', 'years' => 'required|integer', 'month' => 'required|integer' ];
				$validation = \Validator::make($input, $rule);
				$search = \DB::SELECT("call View_ScheduleUnapproved($department,'$year','$month')");

				if($search == null){
					return $unapproved->getMessage("Data not exist",200,[], $access[3]);
				}
				$data = [];
				foreach ($search as $key => $value){
					$data[$value->employee_id][] = ["date" =>$value->date,"shift_code" => $value->shift_code,"employee_id" =>$value->employee_id,"employee" => $value->employee, "department" => $value->department ];
				}


				$arr_x=[];
				foreach ($search as $key => $value){
					$arr_x[$value->employee_id][] = $value->date;
				}

				 $dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];
				//$kodok = "";
				$arr = [];

				//manipulation  shift code
				foreach ($arr_x as $key => $value) {
						$count = count($arr_x[$key])-1;
						for($i = 0; $i <= $count; $i++){
							$index = (integer)substr($arr_x[$key][$i], 8, 2);
							$j = $dictionary[$index-1];
							$monthx = (integer)substr($data[$key][$i]['date'], 5, 2);
							$yearx = (integer)substr($data[$key][$i]['date'], 0, 4);
							$date_sisx[$key][] = $index;
							$dx=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
							$data_tanggal[$key][$j]= $data[$key][$i]['shift_code'];
						}
				}

				//end manipulation

				$keyr = [];
				$d_date = [];

				// date manipulation
				foreach ($data as $key => $value) {
						$keyr[] = $key;
						$count = count($data[$key])-1;
						for($i = 0; $i <= $count; $i++){
						$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
						$month = substr($data[$key][$i]['date'], 5, 2);
						$year = (integer)substr($data[$key][$i]['date'], 0, 4);
						$jp = $dictionary[$indexp-1];
						$date_sisa[$key][] = $indexp;
						$d=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
						$data_tanggax[$key]["date".$jp] =$data[$key][$i]['date'];
						}
				}

				foreach ($date_sisa as $key => $value) {
					for($i = 0; $i <= $d; $i++){
								$df = $i+1;
								$sd = strlen($df);
								($sd == 1 ? $sdx = "0".$df : $sdx = $df);
								$data_tanggax[$key]["date".$dictionary[$i]] = date($year)."-".date($month)."-".date($sdx);


					}
				}

				//end date manipulation


				foreach ($data as $key => $value) {
						$count = count($data[$key])-1;

						for($i = 0; $i <= $count; $i++){
						$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
							$jp = $dictionary[$indexp-1];
							$data_tang[$key]= [$data[$key][$i]['employee'],$data[$key][$i]['employee_id'],$data[$key][$i]['department']];
						}
				}

			foreach ($data_tanggax as $key => $value) {
				$data_er[] = array_merge_recursive($value,$data_tanggal[$key],["employee" => $data_tang[$key][0]],["employee_id" => $data_tang[$key][1]],["department" => $data_tang[$key][2]]) ;
			}

			foreach ($data_er as $key => $value) {
						$data_r[] =array_merge_recursive($value,["name" => "Employee"],["dep" => "Department"]);
			}
			return $unapproved->index($data_r, $access[3]);

		}

			// 	if($search == null){
			// 		return $unapproved->getMessage("Data not exist",200,[], $access[3]);
			// 	}
			// 	$data = [];

			// 	foreach ($search as $key => $value){
			// 		$data[$value->employee_id][] = ["date" =>$value->date,"shift_code" => $value->shift_code,"employee_id" =>$value->employee_id,"employee" => $value->employee, "department" => $value->department ];
			// 	}

			// 	$arr_x=[];
			// 	foreach ($search as $key => $value){
			// 		$arr_x[$value->employee_id][] = $value->date;
			// 	}

			// 	 $dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","aa","ab","ac","ad","ae","af","ag","ah"];
			// 	//$kodok = "";
			// 	$arr = [];

			// 	foreach ($arr_x as $key => $value) {
			// 			$count = count($arr_x[$key])-1;
			// 			for($i = 0; $i <= $count; $i++){
			// 				$index = (integer)substr($arr_x[$key][$i], 8, 2);
			// 				$j = $dictionary[$index-1];
			// 				$data_tanggal[$key][$j]= $data[$key][$i]['shift_code'];
			// 			}
			// 	}

			// 	$keyr = [];
			// 	foreach ($data as $key => $value) {
			// 			$keyr[] = $key;
			// 			$count = count($data[$key])-1;
			// 			for($i = 0; $i <= $count; $i++){
			// 			$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
			// 			$jp = $dictionary[$indexp-1];

			// 			$data_tanggax[$key]["date".$jp] =$data[$key][$i]['date'];
			// 			}
			// 	}

			// 	foreach ($data as $key => $value) {
			// 			$count = count($data[$key])-1;
			// 			for($i = 0; $i <= $count; $i++){
			// 			$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
			// 				$jp = $dictionary[$indexp-1];
			// 				$data_tang[$key]= [$data[$key][$i]['employee'],$data[$key][$i]['employee_id'],$data[$key][$i]['department']];
			// 			}
			// 	}

			// 	foreach ($data_tanggax as $key => $value) {
			// 		$data_er[] = array_merge_recursive($value,$data_tanggal[$key],["employee" => $data_tang[$key][0]],["employee_id" => $data_tang[$key][1]],["department" => $data_tang[$key][2]]) ;
			// 	}

			// 	foreach ($data_er as $key => $value) {
			// 		$data_r[] =array_merge_recursive($value,["name" => "Employee"],["dep" => "Department"]);
			// 	}

			// 	return $unapproved->index($data_r, $access[3]);
			// }
					}else{
		$message = $access[0]; $status = $access[1]; $data=$access[2];
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
	}

	public function change_workshift(){
		 /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
   		 	if($access[1] == 200){
		$unapproved = new unapproved_schedule_list;
		$input = \Input::all();
		$shift = \Input::get("shift");
		$name = \Input::get("employee_id");
		$date = \Input::get("date");

		$rule = ["shift" => "required|string", "employee_id" => "required|Regex:/^[A-Za-z0-9 ]+$/", "date" => "required|date" ];
		$validation  = \Validator::make($input, $rule);


		if($validation->fails()){
			$check = $validation->errors()->all();
			return $unapproved->getMessage("invalidated input format", 500, $check);
		}else{
			$data_shift = \DB::SELECT("CALL View_WorkSift_byName('$shift') ");
			$data_shift_id = $data_shift[0]->shift_id;
			$db_search_data = \DB::SELECT("select * from  att_schedule where employee_id='$name' and date='$date'  ");
			if($db_search_data != null){
				$change_workshift = \DB::SELECT("call Update_ChangeWorkShift($data_shift_id,'$name','$date')");
			}else{
				$insert_workshift= \DB::SELECT("insert into att_schedule(employee_id,date,shift_id,status) values ('$name','$date',$data_shift_id,1)");
			}
			if(isset($change_workshift) || isset($insert_workshift)){


				return $unapproved->getMessage("success", 200,[],$access[3]);
			}else{
				return $unapproved->getMessage("failed",500,[],$access[3]);
			}
		}

		}else{
		$message = $access[0]; $status = $access[1]; $data=$access[2];
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
	 }

	 public function viewScheduleList(){
	 	$unapproved = new unapproved_schedule_list;
		$input = \Input::all();
		$department = \Input::get('Department');
		$year = \Input::get('years');
		$month  = \Input::get('month');
		$rule = [ 'Department' => 'required|integer', 'years' => 'required|integer', 'month' => 'required|integer' ];
		$validation = \Validator::make($input, $rule);
		$search = \DB::SELECT("call View_View_ScheduleList($department,'$year','$month')");
		foreach ($search as $key => $value){
			$data[] = [ "date" =>$value->date];
			$data1[] =["number" => $value->employee_id, "shift_code" => $value->shift_code,  "first_name" => $value->first_name, "last_name" =>$value->last_name, "employee_id" => $value->employee_id];

		}

		return $unapproved->index($validation,$data2);
	 }

	 // public function approve_all(){
		//  /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
  //  		 	if($access[1] == 200){
  //  		 		if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
		// 			$data = []; $message = 'Unauthorized'; $status = 200;
		// 		}else{



	 public function approve_all(){
		 /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
   		 	if($access[1] == 200){
   		 		if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$data = []; $message = 'Unauthorized'; $status = 200;
				}else{
					$month = \Input::get('month')+1;
					$department = \Input::get('Department');
					$year = \Input::get('years');

					if($month != null ){
						$unapproved = new unapproved_schedule_list;
						$data = \DB::SELECT("CALL update_attschedule_unapprove_month($department,'$year','$month')");
						if(isset($data)){
							return $unapproved->getMessage("Success approved data in ".date('$month'), 200,[],$access[3]);
						}
					}else{
						return $unapproved->getMessage("Failed to approve data, month value for request not found", 200,[],$access[3]);
					}
				}
   			}else{
		$message = $access[0]; $status = $access[1]; $data=$access[2];
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
	 }

}
