<?php namespace Larasite\Http\Controllers\Attendance\ScheduleRequest;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Attendance\Request\SwapChangeShift_model as shiftModel;
use Illuminate\Http\Request;
use Larasite\Library\FuncAccess;
use Larasite\Library\calculation;
use Larasite\Model\Attendance\Request\ScheduleRequestList_model as requestList;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;

class SwapShift_Ctrl extends Controller {
	protected $form = 66;

	// get current logged
	/*public function swap_value()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
				$name = \Input::get('arg1');
				$nameswap = \Input::get('arg2');

				if($name == null and $nameswap == null ){
					//return $nameswap;
					return response()->json(["header" => ["message" => "undefined ID, please refresh first", "status" => 500, 'status' => $access[3]],"data" => []],500);
				}else{

					//$db = \DB::SELECT("call view_att_swapshift_request_formonth('$name','$nameswap') ");
					//$db  = \DB::SELECT("select * from att_schedule where employee_id =  '$name' and employee_id = '$nameswap' and date =   ");
					//$db  = \DB::SELECT("select * from view_nonactive_login as vnl, emp_supervisor as emps where vnl.employee_id in ($name,$nameswap) and (vnl.role_name like '%regular employee user%' or  vnl.role_name like '%user%' or vnl.role_name like '%regular employee%')");
					$db  = \DB::SELECT("select * from view_nonactive_login as vnl, emp_supervisor as emps where vnl.employee_id in ($name,$nameswap)");
				    $get_month = (int)date('m');
				    $get_year = (int)date('Y');
					$dbx = \DB::SELECT("select count(date_request) as count from  att_schedule_request where employee_id = '$name' and month(date_request) = $get_month and year(date_request) = $get_year and type_id = 6");
					if($db != null and $dbx[0]->count <= 2){
						$message = "Success";
						$status = 200;
						$data = ['result' => false];
					}else if($db != null and $dbx[0]->count > 2){
						$message = "Request swap shift only it requested twice in a month";
						$status = 500;
						$data = ['result' => true];
					}else{
						$message = "Failed, data not exist";
						$status = 500;
						$data = ['result' => true];
					}
				}



		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);

	}*/

	public function swap_value()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
				$name = \Input::get('arg1');
				$nameswap = \Input::get('arg2');

				if($name == null and $nameswap == null ){
					//return $nameswap;
					return response()->json(["header" => ["message" => "undefined ID, please refresh first", "status" => 500, 'status' => $access[3]],"data" => []],500);
				}else{

					//$db = \DB::SELECT("call view_att_swapshift_request_formonth('$name','$nameswap') ");
					//$db  = \DB::SELECT("select * from att_schedule where employee_id =  '$name' and employee_id = '$nameswap' and date =   ");
					//$db  = \DB::SELECT("select * from view_nonactive_login as vnl, emp_supervisor as emps where vnl.employee_id in ($name,$nameswap) and (vnl.role_name like '%regular employee user%' or  vnl.role_name like '%user%' or vnl.role_name like '%regular employee%')");
					$db  = \DB::SELECT("select * from view_nonactive_login as vnl, emp_supervisor as emps where vnl.employee_id in ($name,$nameswap)");
				    //$get_month = (int)date('m');
				    //$get_year = (int)date('Y');
					//$dbx = \DB::SELECT("select count(date_request) as count from  att_schedule_request where employee_id = '$name' and month(date_request) = $get_month and year(date_request) = $get_year and type_id = 6");
					if($db != null /*and $dbx[0]->count <= 2*/){
						$message = "Success";
						$status = 200;
						$data = ['result' => false];
					/*}
					else if($db != null and $dbx[0]->count > 2){
						$message = "Request swap shift only it requested twice in a month";
						$status = 500;
						$data = ['result' => true];*/
					}else{
						$message = "Failed, data not exist";
						$status = 500;
						$data = ['result' => true];
					}
				}



		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);

	}

	public function index()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		$models = new requestList;
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
				$model = new shiftModel;
				
				$decode = base64_decode(\Request::get('key'));
				$nameDefault = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
				$checkRole = $models->checkRole($nameDefault);
				$empName = $models->getName($nameDefault);
				if (\Input::get('name')){
					$name = \Input::get('name');
				}
				else{
					$name = $nameDefault;
				}

				$index = $model->index($name);
				
				$index['data']['role'] = ['role' => $checkRole];
				$index['data']['name'] = $empName[0]->name;

				return response()->json(['header' => ['message' => $index['message'], 'status' => $index['status'] , 'access'=>$access[3]], 'data' =>$index['data']], $index['status']);
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);

	}

	// BACKUP CHANGE SWAPSHIFT 25/02/2019
	/*public function changeSwapShift()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
		$model = new shiftModel;
		$request = new leaverequest_Model;

		if($access[1] == 200){

			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				$data = []; $message = 'Unauthorized'; $status = 200;
			}
			else{

				$key = \Input::get("key");
				$encode = base64_decode($key);
				$explode = explode("-",$encode);
				$explode_id = $explode[1];

					$json = \Input::get('data');
					if($json == null){
						
						$name = \Input::json('name');
						if(!isset($name)){
							$name = \Input::get('arg1');
						}
						$nameSwap = \Input::json('nameSwap');
						if($nameSwap){
							$nameSwap = \Input::json('nameSwap');
						}else{
							$nameSwap = "'";
						}

						$requestType = \Input::json('requestType');
						$i = \Input::json('subChange');
						$j = \Input::get('subSwap');
						
						if (\Input::json('comment')){$comment = \Input::json('comment');}
						else{ return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500); }
				
						if ($name == "" OR $requestType == ""){
							return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
						}
						else{
							$empID = $model->getEmployeeId($name);
							if ($empID == null){ return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
							else{
								$type = $model->typeId($requestType);
								if ($type == null){
									return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
								}
								// check the request type, change shift or swap shift
								if ($requestType == "Change Shift"){
									$path = 'null';
									$image = 'null';
									$type = 5;
									
									if($comment == null){ 					
										return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
									}
								
									//check if true
									

									if($explode_id != $name){

										$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' ");
										if($db != null){
											$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' and employee_id = '$name' ");
											if($db != null){
												$check_sup = true;
											}else{
												$check_sup = false;
											}
										}else{
											return false;	
										}
									}else{
										$db = \DB::SELECT("select * from view_active_login where employee_id =  '$name' and (lower(role_name) like '%regular employee user%') ");
										if($db == null){
											$check_sup = false;
										}else{
											$check_sup =  true;
										}
									}
									if($check_sup == false){
										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}else{
										$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
										if($emp == null){
											return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
										}
									}
									$change = $model->changeShift($i,$name,$type,$comment,$image,$path,$explode_id);
									

									return response()->json(['header' => ['message' => $change['message'], 'status' => $change['status'] ], 'data' => $change['data']], $change['status']);
								
								}else{
								
									if ($nameSwap == ""){
										return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
									}
									$path = "null";
									$image = "null";
									//return $arr = [$j,$name,$type,$nameSwap,$comment,$image,$path];
									// if($check_sup != null){
									// 	//KUDOS
									// }else{
									$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
									if($emp == null){
										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}
									//}
									$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' and e.local_it != 1  limit 1");
									if($emp == null){
											return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}

									if($emp[0]->local_it == 1){
										return \Response::json(['header'=>['message'=>'only for no expat employee ','status'=>500],'data'=>null],500);	
									}else{
										$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$path,$image,$explode_id);
									}
									return response()->json(['header' => ['message' => $swap['message'], 'status' => $swap['status'] ], 'data' => $swap['data']], $swap['status']);
								}
							}
						}
					}else{
						$file = \Input::file('file');
						$data = json_decode($json,1);

						
						if(isset($data['data'])){
							if(isset($data['data']['arg1'])){
								$data['data']['name'] = $data['data']['arg1'];
							}
						}
						if(!isset($data['data']['name'])){	
							return response()->json(['header' => ['message' => 'name employee  not found' , 'status' => 500],'data' =>[]],500);
							//$name = $data['data']['arg1'];
						}else{
							$name = $data['data']['name'];
						}

						


						if(isset( $data['data']['nameSwap'])){
							$nameSwap = $data['data']['nameSwap'];
						}else{
							$nameSwap = "";
						}
						//$nameSwap = $data['data']['nameSwap'];
						$requestType = $data['data']['requestType'];
						if(isset($data['data']['subChange'])){
							$i = $data['data']['subChange'];
						}else{
							$i = "";
						}

						if(isset($data['data']['subSwap'])){
							$j = $data['data']['subSwap'];
						}else{
							$j = "";
						}

						if (isset($data['data']['comment'])){ $comment = $data['data']['comment'];
						}else{ 					
							return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
						}

						if ($name == "" OR $requestType == ""){
						
							return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
						}else{
							$empID = $model->getEmployeeId($name);
							if ($empID == null){
								return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
							else{
								$image = \Input::file('file');
									if (isset($image)){
										$imageName = $image->getClientOriginalName();
										$ext = \Input::file('file')->getClientOriginalExtension();
										$path = "hrms_upload/swap_shift";
										$size = $image->getSize();

										$rename = "SwapShift_".str_random(6).".".$image->getClientOriginalExtension();

							  			//if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
											if($size > 104857600){
												return \Response::json(['header'=>['message'=>'Image size only support up to 10 MB','status'=>500],'data'=>null],500);
											}else{
												\Input::file('file')->move(storage_path($path),$rename);
											}
										// }else{
										// 		return \Response::json(['header'=>['message'=>'Unsupported format file','status'=>500],'data'=>null],500);
										// }

									}
								$type = $model->typeId($requestType);

								if ($type == null){
									return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
								}
								// check the request type, change shift or swap shift
								if ($requestType == "Change Shift"){

									$change = $model->changeShift($i,$name,$type,$comment,$rename,$path,$explode_id);
									return response()->json(['header' => ['message' => $change['message'], 'status' => $change['status'] ], 'data' => $change['data']], $change['status']);
								}else{
									if ($nameSwap == ""){
										//return 1;
										return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
									}
									$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$rename,$path,$explode_id);
									return response()->json(['header' => ['message' => $swap['message'], 'status' => $swap['status'] ], 'data' => $swap['data']], $swap['status']);
								}
							}
						}
					}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}*/

	/*public function changeSwapShift()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
		$model = new shiftModel;
		$request = new leaverequest_Model;

		if($access[1] == 200){

			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				$data = []; $message = 'Unauthorized'; $status = 200;
			}
			else{

				$key = \Input::get("key");
				$encode = base64_decode($key);
				$explode = explode("-",$encode);
				$explode_id = $explode[1];

					$json = \Input::get('data');
					if($json == null){
						
						$name = \Input::json('name');
						if(!isset($name)){
							$name = \Input::get('arg1');
						}
						$nameSwap = \Input::json('nameSwap');
						if($nameSwap){
							$nameSwap = \Input::json('nameSwap');
						}else{
							$nameSwap = "'";
						}

						$requestType = \Input::json('requestType');
						$i = \Input::json('subChange');
						$j = \Input::get('subSwap');
						
						if (\Input::json('comment')){$comment = \Input::json('comment');}
						else{ return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500); }
				
						if ($name == "" OR $requestType == ""){
							return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
						}
						else{
							$empID = $model->getEmployeeId($name);
							if ($empID == null){ return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
							else{
								$type = $model->typeId($requestType);
								if ($type == null){
									return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
								}
								// check the request type, change shift or swap shift
								if ($requestType == "Change Shift"){
									$path = 'null';
									$image = 'null';
									$type = 5;
									
									if($comment == null){ 					
										return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
									}
								
									//check if true
									

									if($explode_id != $name){

										//$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' ");
										//if($db != null){
											$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' and employee_id = '$name' ");
											if(count($db) > 0){
												$check_sup = true;
											}else{
												$check_sup = false;
											}
										//}else{
										//	$check_sup = false;
										//}
									}else{
										$db = \DB::SELECT("select * from view_active_login where employee_id =  '$name' and (lower(role_name) like '%regular employee user%') ");
										if(count($db) == 0){
											$check_sup = false;
										}else{
											$check_sup =  true;
										}
									}
									if($check_sup == false){
										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}else{
										$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
										if($emp == null){
											return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
										}
									}
									$change = $model->changeShift($i,$name,$type,$comment,$image,$path,$explode_id);
									

									return response()->json(['header' => ['message' => $change['message'], 'status' => $change['status'] ], 'data' => $change['data']], $change['status']);
								
								}else{
								
									if ($nameSwap == ""){
										return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
									}
									$path = "null";
									$image = "null";
									//return $arr = [$j,$name,$type,$nameSwap,$comment,$image,$path];
									// if($check_sup != null){
									// 	//KUDOS
									// }else{
									$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
									if(count($emp) == 0){
										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}
									//}
									$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' and e.local_it != 1  limit 1");
									if(count($emp) == 0){
											return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}

									if($emp[0]->local_it == 1){
										return \Response::json(['header'=>['message'=>'only for no expat employee ','status'=>500],'data'=>null],500);	
									}else{
										$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$path,$image,$explode_id);
									}
									return response()->json(['header' => ['message' => $swap['message'], 'status' => $swap['status'] ], 'data' => $swap['data']], $swap['status']);
								}
							}
						}
					}else{
						$file = \Input::file('file');
						$data = json_decode($json,1);

						if(isset($data['data'])){
							if(isset($data['data']['arg1'])){
								$data['data']['name'] = $data['data']['arg1'];
							}
						}
						
						if(!isset($data['data']['name'])){	
							return response()->json(['header' => ['message' => 'name employee  not found' , 'status' => 500],'data' =>[]],500);
							//$name = $data['data']['arg1'];
						}else{
							$name = $data['data']['name'];
						}

						


						if(isset( $data['data']['nameSwap'])){
							$nameSwap = $data['data']['nameSwap'];
						}else{
							$nameSwap = "";
						}
						//$nameSwap = $data['data']['nameSwap'];
						$requestType = $data['data']['requestType'];
						if(isset($data['data']['subChange'])){
							$i = $data['data']['subChange'];
						}else{
							$i = "";
						}

						if(isset($data['data']['subSwap'])){
							$j = $data['data']['subSwap'];
						}else{
							$j = "";
						}

						if (isset($data['data']['comment'])){ $comment = $data['data']['comment'];
						}else{ 					
							return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
						}


						// PRILAKU
						$empID = $model->getEmployeeId($name);
							if ($empID == null){ return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
							else{
								$type = $model->typeId($requestType);
								if ($type == null){
									return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
								}
								// check the request type, change shift or swap shift
								if ($requestType == "Change Shift"){
									$path = 'null';
									$image = 'null';
									$type = 5;
									
									if($comment == null){ 					
										return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
									}
								
									//check if true
									

									if($explode_id != $name){

										//$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' ");
										//if($db != null){
											$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' and employee_id = '$name' ");
											if(count($db) > 0){
												$check_sup = true;
											}else{
												$check_sup = false;
											}
										//}else{
										//	$check_sup = false;
										//}
									}else{
										$db = \DB::SELECT("select * from view_active_login where employee_id =  '$name' and (lower(role_name) like '%regular employee user%') ");
										if(count($db) == 0){
											$check_sup = false;
										}else{
											$check_sup =  true;
										}
									}
									if($check_sup == false){
										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}else{
										$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
										if($emp == null){
											return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
										}
									}
									//return "lost1";
								}else{
								
									if ($nameSwap == ""){
										return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
									}
									$path = "null";
									$image = "null";
									//return $arr = [$j,$name,$type,$nameSwap,$comment,$image,$path];
									// if($check_sup != null){
									// 	//KUDOS
									// }else{
									$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
									if(count($emp) == 0){
										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}
									//}
									$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' and e.local_it != 1  limit 1");
									if(count($emp) == 0){
											return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}

									if($emp[0]->local_it == 1){
										return \Response::json(['header'=>['message'=>'only for no expat employee ','status'=>500],'data'=>null],500);	
									}else{
										//return "lost12";
									}
								}
							}

						if ($name == "" OR $requestType == ""){
						
							return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
						}else{

							$empID = $model->getEmployeeId($name);
							if ($empID == null){
								return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
							else{
								$image = \Input::file('file');
									if (isset($image)){
										$imageName = $image->getClientOriginalName();
										$ext = \Input::file('file')->getClientOriginalExtension();
										$path = "hrms_upload/swap_shift";
										$size = $image->getSize();

										$rename = "SwapShift_".str_random(6).".".$image->getClientOriginalExtension();

							  			//if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
											if($size > 104857600){
												return \Response::json(['header'=>['message'=>'Image size only support up to 10 MB','status'=>500],'data'=>null],500);
											}else{
												\Input::file('file')->move(storage_path($path),$rename);
											}
										// }else{
										// 		return \Response::json(['header'=>['message'=>'Unsupported format file','status'=>500],'data'=>null],500);
										// }

									}
								$type = $model->typeId($requestType);

								if ($type == null){
									return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
								}
								// check the request type, change shift or swap shift
								if ($requestType == "Change Shift"){
									
									$change = $model->changeShift($i,$name,$type,$comment,$rename,$path,$explode_id);
									return response()->json(['header' => ['message' => $change['message'], 'status' => $change['status'] ], 'data' => $change['data']], $change['status']);
								}else{
									if ($nameSwap == ""){
										//return 1;
										return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
									}
									$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$rename,$path,$explode_id);
									return response()->json(['header' => ['message' => $swap['message'], 'status' => $swap['status'] ], 'data' => $swap['data']], $swap['status']);
								}
							}
						}
					}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}*/

	// public function changeSwapShift()
	// {
	// 	$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
	// 	$model = new shiftModel;
	// 	$request = new leaverequest_Model;

	// 	if($access[1] == 200){

	// 		if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
	// 			$data = []; $message = 'Unauthorized'; $status = 200;
	// 		}
	// 		else{

	// 			$key = \Input::get("key");
	// 			$encode = base64_decode($key);
	// 			$explode = explode("-",$encode);
	// 			$explode_id = $explode[1];

	// 			//return [\Input::get('arg1')];

	// 				$json = \Input::get('data');
	// 				if($json == null){
						
	// 					$name = \Input::json('name');
	// 					if(!isset($name)){
	// 						$name = \Input::get('arg1');
	// 					}
	// 					$nameSwap = \Input::json('nameSwap');
	// 					if($nameSwap){
	// 						$nameSwap = \Input::json('nameSwap');
	// 					}else{
	// 						$nameSwap = "'";
	// 					}

	// 					$arg1 = \Input::get('arg1');
	// 					$arg2 = \Input::get('arg2');
	// 					$name1 = \Input::get('name');
	// 					$name_swap = \Input::get('nameSwap');
	// 					if($arg1 == $explode_id && $name != $explode_id && $name_swap != $explode_id){
	// 						$name 		= $name1;
	// 						$nameSwap 	= $name_swap;
	// 						$arg1 		= $name;
	// 					}

	// 					$requestType = \Input::json('requestType');
	// 					$i = \Input::json('subChange');
	// 					$j = \Input::get('subSwap');
						
	// 					if (\Input::json('comment')){$comment = \Input::json('comment');}
	// 					else{ return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500); }
				
	// 					if ($name == "" OR $requestType == ""){
	// 						return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
	// 					}
	// 					else{
	// 						$empID = $model->getEmployeeId($name);
	// 						if ($empID == null){ return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
	// 						else{
	// 							$type = $model->typeId($requestType);
	// 							if ($type == null){
	// 								return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
	// 							}
	// 							// check the request type, change shift or swap shift
	// 							if ($requestType == "Change Shift"){
	// 								$path = 'null';
	// 								$image = 'null';
	// 								$type = 5;
									
	// 								if($comment == null){ 					
	// 									return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
	// 								}
								
	// 								//check if true
									

	// 								if($explode_id != $name){

	// 									//$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' ");
	// 									//if($db != null){
	// 										$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' and employee_id = '$name' ");
	// 										if(count($db) > 0){
	// 											$check_sup = true;
	// 										}else{
	// 											$check_sup = false;
	// 										}
	// 									//}else{
	// 									//	$check_sup = false;
	// 									//}
	// 								}else{
	// 									$check_sup =  true;
	// 									// $db = \DB::SELECT("select * from view_active_login where employee_id =  '$name' and (lower(role_name) like '%regular employee user%') ");
	// 									// if(count($db) == 0){
	// 									// 	$check_sup = false;
	// 									// }else{
	// 									// 	$check_sup =  true;
	// 									// }
	// 								}
	// 								if($check_sup == false){
	// 									return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}else{
	// 									$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
	// 									if($emp == null){
	// 										if($explode_id == $name){
	// 											$emp1 = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%human%' or '%hr%') and  e.employee_id = '$name' limit 1");
	// 											if(count($emp1) == 0){
	// 												return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 											}
	// 										}
	// 									}
	// 								}
	// 								$change = $model->changeShift($i,$name,$type,$comment,$image,$path,$explode_id);
									

	// 								return response()->json(['header' => ['message' => $change['message'], 'status' => $change['status'] ], 'data' => $change['data']], $change['status']);
								
	// 							}else{
								
	// 								if ($nameSwap == ""){
	// 									return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
	// 								}
	// 								$path = "null";
	// 								$image = "null";
	// 								//return $arr = [$j,$name,$type,$nameSwap,$comment,$image,$path];
	// 								// if($check_sup != null){
	// 								// 	//KUDOS
	// 								// }else{
	// 								/*$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
	// 								if(count($emp) == 0){
	// 									return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}
	// 								//}
	// 								$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' and e.local_it != 1  limit 1");
	// 								if(count($emp) == 0){
	// 										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}

	// 								if($emp[0]->local_it == 1){
	// 									return \Response::json(['header'=>['message'=>'only for no expat employee ','status'=>500],'data'=>null],500);	
	// 								}else{
	// 									$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$path,$image,$explode_id);
	// 								}*/
									
	// 								$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$path,$image,$explode_id);
	// 								return response()->json(['header' => ['message' => $swap['message'], 'status' => $swap['status'] ], 'data' => $swap['data']], $swap['status']);
	// 							}
	// 						}
	// 					}
	// 				}else{
	// 					$file = \Input::file('file');
	// 					$data = json_decode($json,1);

	// 					if(isset($data['data'])){
	// 						if(isset($data['data']['arg1'])){
	// 							if($data['data']['arg1'] == $explode_id && $data['data']['name'] != $explode_id && $data['data']['nameSwap'] != $explode_id){
	// 								$data['data']['arg1'] 	= $data['data']['name'];
	// 							}
	// 							$data['data']['name'] = $data['data']['arg1'];
	// 						}
	// 					}
						
	// 					if(!isset($data['data']['name'])){	
	// 						return response()->json(['header' => ['message' => 'name employee  not found' , 'status' => 500],'data' =>[]],500);
	// 						//$name = $data['data']['arg1'];
	// 					}else{
	// 						$name = $data['data']['name'];
	// 					}

						


	// 					if(isset( $data['data']['nameSwap'])){
	// 						$nameSwap = $data['data']['nameSwap'];
	// 					}else{
	// 						$nameSwap = "";
	// 					}
	// 					//$nameSwap = $data['data']['nameSwap'];
	// 					$requestType = $data['data']['requestType'];
	// 					if(isset($data['data']['subChange'])){
	// 						$i = $data['data']['subChange'];
	// 					}else{
	// 						$i = "";
	// 					}

	// 					if(isset($data['data']['subSwap'])){
	// 						$j = $data['data']['subSwap'];
	// 					}else{
	// 						$j = "";
	// 					}



	// 					if (isset($data['data']['comment'])){ $comment = $data['data']['comment'];
	// 					}else{ 					
	// 						return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
	// 					}


	// 					// PRILAKU
	// 					$empID = $model->getEmployeeId($name);
	// 						if ($empID == null){ return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
	// 						else{
	// 							$type = $model->typeId($requestType);
	// 							if ($type == null){
	// 								return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
	// 							}
	// 							// check the request type, change shift or swap shift
	// 							if ($requestType == "Change Shift"){
	// 								$path = 'null';
	// 								$image = 'null';
	// 								$type = 5;
									
	// 								if($comment == null){ 					
	// 									return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
	// 								}
								
	// 								//check if true
									

	// 								if($explode_id != $name){

	// 									//$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' ");
	// 									//if($db != null){
	// 										$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' and employee_id = '$name' ");
	// 										if(count($db) > 0){
	// 											$check_sup = true;
	// 										}else{
	// 											$check_sup = false;
	// 										}
	// 									//}else{
	// 									//	$check_sup = false;
	// 									//}
	// 								}else{
	// 									$db = \DB::SELECT("select * from view_active_login where employee_id =  '$name' and (lower(role_name) like '%regular employee user%') ");
	// 									if(count($db) == 0){
	// 										$check_sup = false;
	// 									}else{
	// 										$check_sup =  true;
	// 									}
	// 								}
	// 								/*if($check_sup == false){
	// 									return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}else{
	// 									$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
	// 									if($emp == null){
	// 										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 									}
	// 								}*/
	// 								//return "lost1";
	// 							}else{
								
	// 								if ($nameSwap == ""){
	// 									return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
	// 								}
	// 								$path = "null";
	// 								$image = "null";
	// 								//return $arr = [$j,$name,$type,$nameSwap,$comment,$image,$path];
	// 								// if($check_sup != null){
	// 								// 	//KUDOS
	// 								// }else{
	// 								/*$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
	// 								if(count($emp) == 0){
	// 									return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}
	// 								//}
	// 								$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' and e.local_it != 1  limit 1");
	// 								if(count($emp) == 0){
	// 										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}

	// 								if($emp[0]->local_it == 1){
	// 									return \Response::json(['header'=>['message'=>'only for no expat employee ','status'=>500],'data'=>null],500);	
	// 								}else{
	// 									//return "lost12";
	// 								}*/
	// 							}
	// 						}

	// 					if ($name == "" OR $requestType == ""){
						
	// 						return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
	// 					}else{

	// 						$empID = $model->getEmployeeId($name);
	// 						if ($empID == null){
	// 							return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
	// 						else{
	// 							$image = \Input::file('file');
	// 								if (isset($image)){
	// 									$imageName = $image->getClientOriginalName();
	// 									$ext = \Input::file('file')->getClientOriginalExtension();
	// 									$path = "hrms_upload/swap_shift";
	// 									$size = $image->getSize();

	// 									$rename = "SwapShift_".str_random(6).".".$image->getClientOriginalExtension();

	// 						  			//if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
	// 										if($size > 104857600){
	// 											return \Response::json(['header'=>['message'=>'Image size only support up to 10 MB','status'=>500],'data'=>null],500);
	// 										}else{
	// 											\Input::file('file')->move(storage_path($path),$rename);
	// 										}
	// 									// }else{
	// 									// 		return \Response::json(['header'=>['message'=>'Unsupported format file','status'=>500],'data'=>null],500);
	// 									// }

	// 								}
	// 							$type = $model->typeId($requestType);

	// 							if ($type == null){
	// 								return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
	// 							}
	// 							// check the request type, change shift or swap shift
	// 							if ($requestType == "Change Shift"){
									
	// 								$change = $model->changeShift($i,$name,$type,$comment,$rename,$path,$explode_id);
	// 								return response()->json(['header' => ['message' => $change['message'], 'status' => $change['status'] ], 'data' => $change['data']], $change['status']);
	// 							}else{
	// 								if ($nameSwap == ""){
	// 									//return 1;
	// 									return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
	// 								}

	// 								$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$rename,$path,$explode_id);
	// 								return response()->json(['header' => ['message' => $swap['message'], 'status' => $swap['status'] ], 'data' => $swap['data']], $swap['status']);
	// 							}
	// 						}
	// 					}
	// 				}
	// 		}
	// 	}else{
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 	}
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	// }

	// public function changeSwapShift()
	// {
	// 	$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
	// 	$model = new shiftModel;
	// 	$request = new leaverequest_Model;

	// 	if($access[1] == 200){

	// 		if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
	// 			$data = []; $message = 'Unauthorized'; $status = 200;
	// 		}
	// 		else{
	// 			return \Response::json(['header'=>['message'=>'Sorry, this request still under maintenance','status'=>500],'data'=>null],500);
	// 			$key = \Input::get("key");
	// 			$encode = base64_decode($key);
	// 			$explode = explode("-",$encode);
	// 			$explode_id = $explode[1];
	// 			$hr = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%human%' or '%hr%') and  e.employee_id = '$explode_id' limit 1");
	// 			$it_admin = \DB::SELECT("select r.role_name from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%admin%') and  e.employee_id = '$explode_id' limit 1");
	// 			$comment = null;
	// 			//return [\Input::get('arg1')];

	// 				$json = \Input::get('data');
	// 				if($json == null){
						
	// 					$name = \Input::json('name');
	// 					if(!isset($name)){
	// 						$name = \Input::get('arg1');
	// 					}
	// 					$nameSwap = \Input::json('nameSwap');
	// 					if($nameSwap){
	// 						$nameSwap = \Input::json('nameSwap');
	// 					}else{
	// 						$nameSwap = "'";
	// 					}

	// 					$arg1 = \Input::get('arg1');
	// 					$arg2 = \Input::get('arg2');
	// 					$name1 = \Input::get('name');
	// 					$name_swap = \Input::get('nameSwap');
	// 					if($arg1 == $explode_id && $name != $explode_id && $name_swap != $explode_id){
	// 						$name 		= $name1;
	// 						$nameSwap 	= $name_swap;
	// 						$arg1 		= $name;
	// 					}

	// 					$requestType = \Input::json('requestType');
	// 					$i = \Input::json('subChange');
	// 					$j = \Input::get('subSwap');
						
	// 					if (\Input::json('comment')){$comment = \Input::json('comment');}
	// 					//else{ return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500); }
				
	// 					if ($name == "" OR $requestType == ""){
	// 						return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
	// 					}
	// 					else{
	// 						$empID = $model->getEmployeeId($name);
	// 						if ($empID == null){ return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
	// 						else{
	// 							$type = $model->typeId($requestType);
	// 							if ($type == null){
	// 								return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
	// 							}
	// 							// check the request type, change shift or swap shift
	// 							if ($requestType == "Change Shift"){
	// 								$path = 'null';
	// 								$image = 'null';
	// 								$type = 5;
									
	// 								// if($comment == null){ 					
	// 								// 	return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
	// 								// }
								
	// 								//check if true
									

	// 								// if($explode_id != $name){

	// 								// 	//$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' ");
	// 								// 	//if($db != null){
	// 								// 		$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' and employee_id = '$name' ");
	// 								// 		if(count($db) > 0){
	// 								// 			$check_sup = true;
	// 								// 		}else{
	// 								// 			$check_sup = false;
	// 								// 		}
	// 								// 	//}else{
	// 								// 	//	$check_sup = false;
	// 								// 	//}
	// 								// }else{

	// 								// 	$check_sup =  true;
	// 								// 	// $db = \DB::SELECT("select * from view_active_login where employee_id =  '$name' and (lower(role_name) like '%regular employee user%') ");
	// 								// 	// if(count($db) == 0){
	// 								// 	// 	$check_sup = false;
	// 								// 	// }else{
	// 								// 	// 	$check_sup =  true;
	// 								// 	// }
	// 								// }

	// 								if($explode_id != $name){

	// 									//$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' ");
	// 									//if($db != null){
	// 										$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' and employee_id = '$name' ");
	// 										if(count($db) > 0){
	// 											$check_sup = true;
	// 										}else{
												
	// 											if(count($hr) > 0 ){
	// 												$check_sup = true;
	// 											}else if(count($it_admin) > 0){
	// 												$check_sup = true;
	// 											}else{
	// 												$check_sup = false;	
	// 											}
	// 										}
	// 									//}else{
	// 									//	$check_sup = false;
	// 									//}
	// 								}else{

	// 									$check_sup =  true;
	// 									// $db = \DB::SELECT("select * from view_active_login where employee_id =  '$name' and (lower(role_name) like '%regular employee user%') ");
	// 									// if(count($db) == 0){
	// 									// 	$check_sup = false;
	// 									// }else{
	// 									// 	$check_sup =  true;
	// 									// }
	// 								}
	// 								if(!$check_sup){
	// 									return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}

	// 								// if(count($hr) == 0){
	// 								// 	if($check_sup == false){
	// 								// 		return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								// 	}else{
	// 								// 		$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
	// 								// 		if($emp == null){
	// 								// 			if($explode_id == $name){
	// 								// 				/*$emp1 = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%human%' or '%hr%') and  e.employee_id = '$name' limit 1");
	// 								// 				if(count($emp1) == 0){
	// 								// 				}*/
	// 								// 				return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								// 			}
	// 								// 		}
	// 								// 	}
	// 								// }

	// 								$change = $model->changeShift($i,$name,$type,$comment,$image,$path,$explode_id);
									

	// 								return response()->json(['header' => ['message' => $change['message'], 'status' => $change['status'] ], 'data' => $change['data']], $change['status']);
								
	// 							}else{
								
	// 								if ($nameSwap == ""){
	// 									return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
	// 								}
	// 								$path = "null";
	// 								$image = "null";
	// 								//return $arr = [$j,$name,$type,$nameSwap,$comment,$image,$path];
	// 								// if($check_sup != null){
	// 								// 	//KUDOS
	// 								// }else{
	// 								/*$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
	// 								if(count($emp) == 0){
	// 									return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}
	// 								//}
	// 								$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' and e.local_it != 1  limit 1");
	// 								if(count($emp) == 0){
	// 										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}

	// 								if($emp[0]->local_it == 1){
	// 									return \Response::json(['header'=>['message'=>'only for no expat employee ','status'=>500],'data'=>null],500);	
	// 								}else{
	// 									$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$path,$image,$explode_id);
	// 								}*/
									
	// 								$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$path,$image,$explode_id);
	// 								return response()->json(['header' => ['message' => $swap['message'], 'status' => $swap['status'] ], 'data' => $swap['data']], $swap['status']);
	// 							}
	// 						}
	// 					}
	// 				}else{
	// 					$file = \Input::file('file');
	// 					$data = json_decode($json,1);

	// 					if(isset($data['data'])){
	// 						if(isset($data['data']['arg1'])){
	// 							if($data['data']['arg1'] == $explode_id && $data['data']['name'] != $explode_id && $data['data']['nameSwap'] != $explode_id){
	// 								$data['data']['arg1'] 	= $data['data']['name'];
	// 							}
	// 							$data['data']['name'] = $data['data']['arg1'];
	// 						}
	// 					}
						
	// 					if(!isset($data['data']['name'])){	
	// 						return response()->json(['header' => ['message' => 'name employee  not found' , 'status' => 500],'data' =>[]],500);
	// 						//$name = $data['data']['arg1'];
	// 					}else{
	// 						$name = $data['data']['name'];
	// 					}

						


	// 					if(isset( $data['data']['nameSwap'])){
	// 						$nameSwap = $data['data']['nameSwap'];
	// 					}else{
	// 						$nameSwap = "";
	// 					}
	// 					//$nameSwap = $data['data']['nameSwap'];
	// 					$requestType = $data['data']['requestType'];
	// 					if(isset($data['data']['subChange'])){
	// 						$i = $data['data']['subChange'];
	// 					}else{
	// 						$i = "";
	// 					}

	// 					if(isset($data['data']['subSwap'])){
	// 						$j = $data['data']['subSwap'];
	// 					}else{
	// 						$j = "";
	// 					}



	// 					if (isset($data['data']['comment'])){ $comment = $data['data']['comment'];}
	// 					// else{ 					
	// 					// 	return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
	// 					// }


	// 					// PRILAKU
	// 					$empID = $model->getEmployeeId($name);
	// 						if ($empID == null){ return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
	// 						else{
	// 							$type = $model->typeId($requestType);
	// 							if ($type == null){
	// 								return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
	// 							}
	// 							// check the request type, change shift or swap shift
	// 							if ($requestType == "Change Shift"){
	// 								$path = 'null';
	// 								$image = 'null';
	// 								$type = 5;
									
	// 								// if($comment == null){ 					
	// 								// 	return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
	// 								// }
								
	// 								//check if true
									

	// 								if($explode_id != $name){

	// 									//$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' ");
	// 									//if($db != null){
	// 										$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' and employee_id = '$name' ");
	// 										if(count($db) > 0){
	// 											$check_sup = true;
	// 										}else{
												
	// 											if(count($hr) > 0 ){
	// 												$check_sup = true;
	// 											}else if(count($it_admin) > 0){
	// 												$check_sup = true;
	// 											}else{
	// 												$check_sup = false;	
	// 											}
	// 										}
	// 									//}else{
	// 									//	$check_sup = false;
	// 									//}
	// 								}else{

	// 									$check_sup =  true;
	// 									// $db = \DB::SELECT("select * from view_active_login where employee_id =  '$name' and (lower(role_name) like '%regular employee user%') ");
	// 									// if(count($db) == 0){
	// 									// 	$check_sup = false;
	// 									// }else{
	// 									// 	$check_sup =  true;
	// 									// }
	// 								}
	// 								if(!$check_sup){
	// 									return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}
	// 								/*if($check_sup == false){
	// 									return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}else{
	// 									$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
	// 									if($emp == null){
	// 										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 									}
	// 								}*/
	// 								//return "lost1";
	// 							}else{
								
	// 								if ($nameSwap == ""){
	// 									return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
	// 								}
	// 								$path = "null";
	// 								$image = "null";
	// 								//return $arr = [$j,$name,$type,$nameSwap,$comment,$image,$path];
	// 								// if($check_sup != null){
	// 								// 	//KUDOS
	// 								// }else{
	// 								/*$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
	// 								if(count($emp) == 0){
	// 									return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}
	// 								//}
	// 								$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' and e.local_it != 1  limit 1");
	// 								if(count($emp) == 0){
	// 										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
	// 								}

	// 								if($emp[0]->local_it == 1){
	// 									return \Response::json(['header'=>['message'=>'only for no expat employee ','status'=>500],'data'=>null],500);	
	// 								}else{
	// 									//return "lost12";
	// 								}*/
	// 							}
	// 						}

	// 					if ($name == "" OR $requestType == ""){
						
	// 						return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
	// 					}else{

	// 						$empID = $model->getEmployeeId($name);
	// 						if ($empID == null){
	// 							return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
	// 						else{
	// 							$image = \Input::file('file');
	// 								if (isset($image)){
	// 									$imageName = $image->getClientOriginalName();
	// 									$ext = \Input::file('file')->getClientOriginalExtension();
	// 									$path = "hrms_upload/swap_shift";
	// 									$size = $image->getSize();

	// 									$rename = "SwapShift_".str_random(6).".".$image->getClientOriginalExtension();

	// 						  			//if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
	// 										if($size > 104857600){
	// 											return \Response::json(['header'=>['message'=>'Image size only support up to 10 MB','status'=>500],'data'=>null],500);
	// 										}else{
	// 											\Input::file('file')->move(storage_path($path),$rename);
	// 										}
	// 									// }else{
	// 									// 		return \Response::json(['header'=>['message'=>'Unsupported format file','status'=>500],'data'=>null],500);
	// 									// }

	// 								}
	// 							$type = $model->typeId($requestType);

	// 							if ($type == null){
	// 								return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
	// 							}
	// 							// check the request type, change shift or swap shift
	// 							if ($requestType == "Change Shift"){
									
	// 								$change = $model->changeShift($i,$name,$type,$comment,$rename,$path,$explode_id);
	// 								return response()->json(['header' => ['message' => $change['message'], 'status' => $change['status'] ], 'data' => $change['data']], $change['status']);
	// 							}else{
	// 								if ($nameSwap == ""){
	// 									//return 1;
	// 									return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
	// 								}

	// 								$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$rename,$path,$explode_id);
	// 								return response()->json(['header' => ['message' => $swap['message'], 'status' => $swap['status'] ], 'data' => $swap['data']], $swap['status']);
	// 							}
	// 						}
	// 					}
	// 				}
	// 		}
	// 	}else{
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 	}
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	// }

	public function changeSwapShift()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
		$model = new shiftModel;
		$request = new leaverequest_Model;

		if($access[1] == 200){

			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				$data = []; $message = 'Unauthorized'; $status = 200;
			}
			else{

				$key = \Input::get("key");
				$encode = base64_decode($key);
				$explode = explode("-",$encode);
				$explode_id = $explode[1];
				$hr = \DB::SELECT("select r.role_name from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%human%' or '%hr%') and  e.employee_id = '$explode_id' limit 1");
				$it_admin = \DB::SELECT("select r.role_name from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%admin%') and  e.employee_id = '$explode_id' limit 1");
				//return [\Input::get('arg1')];\
				
				$comment = null;

					$json = \Input::get('data');
					if($json == null){
						
						$name = \Input::json('name');
						if(!isset($name)){
							$name = \Input::get('arg1');
						}
						$nameSwap = \Input::json('nameSwap');
						if($nameSwap){
							$nameSwap = \Input::json('nameSwap');
						}else{
							$nameSwap = "'";
						}

						$arg1 = \Input::get('arg1');
						$arg2 = \Input::get('arg2');
						$name1 = \Input::get('name');
						$name_swap = \Input::get('nameSwap');
						if($arg1 == $explode_id && $name != $explode_id && $name_swap != $explode_id){
							$name 		= $name1;
							$nameSwap 	= $name_swap;
							$arg1 		= $name;
						}

						$requestType = \Input::json('requestType');
						$i = \Input::json('subChange');
						$j = \Input::get('subSwap');
						
						if (\Input::json('comment')){$comment = \Input::json('comment');}
						/*else{ return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500); }*/
				
						if ($name == "" OR $requestType == ""){
							return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
						}
						else{
							$empID = $model->getEmployeeId($name);
							if ($empID == null){ return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
							else{
								$type = $model->typeId($requestType);
								if ($type == null){
									return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
								}
								// check the request type, change shift or swap shift
								if ($requestType == "Change Shift"){
									$path = 'null';
									$image = 'null';
									$type = 5;
									
									/*if($comment == null){ 					
										return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
									}*/
								
									//check if true
									

									if($explode_id != $name){

										//$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' ");
										//if($db != null){
											$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' and employee_id = '$name' ");
											if(count($db) > 0){
												$check_sup = true;
											}else{
												
												if(count($hr) > 0 ){
													$check_sup = true;
												}else if(count($it_admin) > 0){
													$check_sup = true;
												}else{
													$check_sup = false;	
												}
											}
										//}else{
										//	$check_sup = false;
										//}
									}else{

										$check_sup =  true;
										// $db = \DB::SELECT("select * from view_active_login where employee_id =  '$name' and (lower(role_name) like '%regular employee user%') ");
										// if(count($db) == 0){
										// 	$check_sup = false;
										// }else{
										// 	$check_sup =  true;
										// }
									}
									if(!$check_sup){
										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}
									// return [$hr,$it_admin,$check_sup];
									// if(count($hr) == 0){
									// 	if($check_sup == false){
									// 		if (count($it_admin) == 0) {
									// 			return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									// 		}
									// 	}else{
									// 		$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
									// 		if($emp == null){
									// 			if($explode_id == $name){
									// 				/*$emp1 = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%human%' or '%hr%') and  e.employee_id = '$name' limit 1");
									// 				if(count($emp1) == 0){
									// 				}*/
									// 				return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									// 			}
									// 		}
									// 	}
									// }

									$tmp = [];
									foreach ($i as $key => $value) {
										if(count($tmp) > 0){
											if(in_array($value['date'],$tmp)){
												return response()->json(['header' => ['message' => "error duplicate date, ".$value['date'], 'status' => 500 ], 'data' => null], 500);
											}else{
												$tmp[] = $value['date'];
											}
										}else{
											$tmp[] = $value['date'];
										}
									}
									$change = $model->changeShift($i,$name,$type,$comment,$image,$path,$explode_id);
									

									return response()->json(['header' => ['message' => $change['message'], 'status' => $change['status'] ], 'data' => $change['data']], $change['status']);
								
								}else{
								
									if ($nameSwap == ""){
										return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
									}
									$path = "null";
									$image = "null";
									//return $arr = [$j,$name,$type,$nameSwap,$comment,$image,$path];
									// if($check_sup != null){
									// 	//KUDOS
									// }else{
									/*$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
									if(count($emp) == 0){
										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}
									//}
									$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' and e.local_it != 1  limit 1");
									if(count($emp) == 0){
											return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}

									if($emp[0]->local_it == 1){
										return \Response::json(['header'=>['message'=>'only for no expat employee ','status'=>500],'data'=>null],500);	
									}else{
										$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$path,$image,$explode_id);
									}*/
									
									$tmp = [];
									foreach ($j as $key => $value) {
										if(count($tmp) > 0){
											if(in_array($value['date'],$tmp)){
												return response()->json(['header' => ['message' => "error duplicate date, ".$value['date'], 'status' => 500 ], 'data' => null], 500);
											}else{
												$tmp[] = $value['date'];
											}
										}else{
											$tmp[] = $value['date'];
										}
									}
									$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$path,$image,$explode_id);
									return response()->json(['header' => ['message' => $swap['message'], 'status' => $swap['status'] ], 'data' => $swap['data']], $swap['status']);
								}
							}
						}
					}else{
						$file = \Input::file('file');
						$data = json_decode($json,1);

						if(isset($data['data'])){
							if(isset($data['data']['arg1'])){
								if($data['data']['arg1'] == $explode_id && $data['data']['name'] != $explode_id && $data['data']['nameSwap'] != $explode_id){
									$data['data']['arg1'] 	= $data['data']['name'];
								}
								$data['data']['name'] = $data['data']['arg1'];
							}
						}
						
						if(!isset($data['data']['name'])){	
							return response()->json(['header' => ['message' => 'name employee  not found' , 'status' => 500],'data' =>[]],500);
							//$name = $data['data']['arg1'];
						}else{
							$name = $data['data']['name'];
						}

						


						if(isset( $data['data']['nameSwap'])){
							$nameSwap = $data['data']['nameSwap'];
						}else{
							$nameSwap = "";
						}
						//$nameSwap = $data['data']['nameSwap'];
						$requestType = $data['data']['requestType'];
						if(isset($data['data']['subChange'])){
							$i = $data['data']['subChange'];
						}else{
							$i = "";
						}

						if(isset($data['data']['subSwap'])){
							$j = $data['data']['subSwap'];
						}else{
							$j = "";
						}



						if (isset($data['data']['comment'])){ $comment = $data['data']['comment'];
						}else{ 					
							//return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
						}


						// PRILAKU
						$empID = $model->getEmployeeId($name);
							if ($empID == null){ return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
							else{
								$type = $model->typeId($requestType);
								if ($type == null){
									return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
								}
								// check the request type, change shift or swap shift
								if ($requestType == "Change Shift"){
									$path = 'null';
									$image = 'null';
									$type = 5;
									
									/*if($comment == null){ 					
										return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
									}*/
								
									//check if true
									

									if($explode_id != $name){

										//$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' ");
										//if($db != null){
											$db = \DB::SELECT("select * from emp_supervisor where supervisor = '$explode_id' and employee_id = '$name' ");
											if(count($db) > 0){
												$check_sup = true;
											}else{
												
												if(count($hr) > 0 ){
													$check_sup = true;
												}else if(count($it_admin) > 0){
													$check_sup = true;
												}else{
													$check_sup = false;	
												}
											}
										//}else{
										//	$check_sup = false;
										//}
									}else{

										$check_sup =  true;
										// $db = \DB::SELECT("select * from view_active_login where employee_id =  '$name' and (lower(role_name) like '%regular employee user%') ");
										// if(count($db) == 0){
										// 	$check_sup = false;
										// }else{
										// 	$check_sup =  true;
										// }
									}
									if(!$check_sup){
										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}
									/*if($check_sup == false){
										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}else{
										$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
										if($emp == null){
											return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
										}
									}*/
									//return "lost1";
								}else{
								
									if ($nameSwap == ""){
										return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
									}
									$path = "null";
									$image = "null";
									//return $arr = [$j,$name,$type,$nameSwap,$comment,$image,$path];
									// if($check_sup != null){
									// 	//KUDOS
									// }else{
									/*$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' limit 1");
									if(count($emp) == 0){
										return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}
									//}
									$emp = \DB::SELECT("select * from emp as e,ldap as l,role as r where e.employee_id =  l.employee_id and l.role_id = r.role_id and (lower(r.role_name) like '%user%' or '%employee_user%' or '%regular employee_user%' or '%superuser%') and  e.employee_id = '$name' and e.local_it != 1  limit 1");
									if(count($emp) == 0){
											return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat and supervisor ','status'=>500],'data'=>null],500);
									}

									if($emp[0]->local_it == 1){
										return \Response::json(['header'=>['message'=>'only for no expat employee ','status'=>500],'data'=>null],500);	
									}else{
										//return "lost12";
									}*/
								}
							}

						if ($name == "" OR $requestType == ""){
						
							return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
						}else{

							$empID = $model->getEmployeeId($name);
							if ($empID == null){
								return \Response::json(['header'=>['message'=>'No employee '.$name.' found','status'=>200],'data'=>null],200);}
							else{
								$image = \Input::file('file');
									if (isset($image)){
										$imageName = $image->getClientOriginalName();
										$ext = \Input::file('file')->getClientOriginalExtension();
										$path = "hrms_upload/swap_shift";
										$size = $image->getSize();

										$rename = "SwapShift_".str_random(6).".".$image->getClientOriginalExtension();

							  			//if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
											if($size > 104857600){
												return \Response::json(['header'=>['message'=>'Image size only support up to 10 MB','status'=>500],'data'=>null],500);
											}else{
												\Input::file('file')->move(storage_path($path),$rename);
											}
										// }else{
										// 		return \Response::json(['header'=>['message'=>'Unsupported format file','status'=>500],'data'=>null],500);
										// }

									}
								$type = $model->typeId($requestType);

								if ($type == null){
									return \Response::json(['header'=>['message'=>'No type '.$requestType.' found','status'=>500],'data'=>null],500);
								}
								// check the request type, change shift or swap shift
								if ($requestType == "Change Shift"){
									
									$tmp = [];
									foreach ($i as $key => $value) {
										if(count($tmp) > 0){
											if(in_array($value['date'],$tmp)){
												return response()->json(['header' => ['message' => "error duplicate date, ".$value['date'], 'status' => 500 ], 'data' => null], 500);
											}else{
												$tmp[] = $value['date'];
											}
										}else{
											$tmp[] = $value['date'];
										}
									}
									$change = $model->changeShift($i,$name,$type,$comment,$rename,$path,$explode_id);
									return response()->json(['header' => ['message' => $change['message'], 'status' => $change['status'] ], 'data' => $change['data']], $change['status']);
								}else{
									if ($nameSwap == ""){
										//return 1;
										return \Response::json(['header'=>['message'=>'Please fill out the required field','status'=>500],'data'=>null],500);
									}

									$tmp = [];
									foreach ($j as $key => $value) {
										if(count($tmp) > 0){
											if(strpos($tmp,$value->date)){
												return response()->json(['header' => ['message' => "error duplicate date, ".$value->date, 'status' => 500 ], 'data' => null], 500);
											}
										}
									}

									$swap = $model->swapShift($j,$name,$type,$nameSwap,$comment,$rename,$path,$explode_id);
									return response()->json(['header' => ['message' => $swap['message'], 'status' => $swap['status'] ], 'data' => $swap['data']], $swap['status']);
								}
							}
						}
					}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}




	// find original work hour by name and date request
	public function work_old()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
		$model = new shiftModel;
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				$data = []; $message = 'Unauthorized'; $status = 200;
			}else{

				$viewWorkShifts = \DB::select("CALL View_WorkShift");
				if (!$viewWorkShifts){
					return \Response::json(['header'=>['message'=>'No work hour found','status'=>500],'data'=>null],500);
				}

				$name = \Input::json('name');
				$nameSwap = \Input::json('nameSwap');
				$requestType = \Input::json('requestType');
				$date = \Input::json('date');
				$temp = [];

				if(isset($nameSwap) && $requestType == "Swap Shift / Day Off"){
					// for change
					// for origin
					$findWorkHour1 = \DB::select("select t1.shift_id,t1.shift_code,
							concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original,
							t1.hour_per_day as duration
							from attendance_work_shifts t1
							left join att_schedule t2 on t2.shift_id=t1.shift_id
							left join emp t3 on t3.employee_id=t2.employee_id
							where t3.employee_id='$nameSwap' and t2.date='$date' order by t2.schedule_id  desc limit 1");
					if(count($findWorkHour1)==0){
						return \Response::json(['header'=>['message'=>'No work hour found on '.$date.' for employee '.$nameSwap,'status'=>500],'data'=>null],500);
					}

					$findWorkHour1extend  = \DB::SELECT("select 
														concat(t3.shift_code,' : ',t3._from,' - 	',t3._to) as Original, t3.shift_id as shift_id,
														t2.update_at
														from att_change_shift as  t1, att_schedule_request as t2,attendance_work_shifts as t3
														where t1.id = t2.request_id  
														and  t1.type_id  = t2.type_id
														and t1.date= '$date' 
														and t1.employee_id  =  '$nameSwap' 
														and t2.status_id = 2
														and t3.shift_id = t1.new_shift 
														order by t1.id desc limit 1");
					// for swap
					$findWorkHour2extend  = \DB::SELECT("select t1.* from att_swap_shift as  t1
														where 
														(t1.employee_id  =  '$nameSwap' or t1.swap_with  =  '$nameSwap' )
														and t1.date= '$date' 
														and t1.status_id = 2
														order by t1.swap_id desc limit 1");

					$change_dt 	= $findWorkHour1extend;
					$swap_dt 	= $findWorkHour2extend;
					$origin_dt	= $findWorkHour1;

					if(count($change_dt) > 0 || count($swap_dt) > 0){
						if(count($change_dt) > 0 && count($swap_dt) > 0){
							if($change_dt[0]->updated_at > $swap_dt[0]->updated_at){
								$pure 		=  $change_dt[0]->Original;
								$shifts 	=  $change_dt[0]->shift_id;	
							}else{
								$pure 		=  $swap_dt[0]->Original;
								$shifts 	=  $swap_dt[0]->shift_id;
							}							
						}else if(count($change_dt) > 0){
							$pure 		=  $change_dt[0]->Original;
							$shifts 	=  $change_dt[0]->shift_id;	
						}else if(count($swap_dt) > 0){
							if($swap_dt[0]->employee_id == $nameSwap){
								$newid = $swap_dt[0]->new_shift_id;
								$getsch = \DB::select("select t1.shift_id, t1.shift_code, t1.hour_per_day as duration,concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original from attendance_work_shifts as t1 where t1.shift_id = $newid");

								$pure 	=  $getsch[0]->Original;
								$shifts =  $getsch[0]->shift_id;

							}else if($swap_dt[0]->swap_with == $nameSwap){
								$oldid = $swap_dt[0]->old_shift_id;
								//return 100;
								$getsch = \DB::select("select t1.shift_id, t1.shift_code, t1.hour_per_day as duration,concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original from attendance_work_shifts as t1 where t1.shift_id = $oldid");

								$pure 	=  $getsch[0]->Original;
								$shifts =  $getsch[0]->shift_id;
							}
						}
					}else{
						$pure 		=  $origin_dt[0]->Original;
						$shifts 	=  $origin_dt[0]->shift_id;
					}
					
					$obj_swap = ["pure"=>$pure,"shifts"=>$shifts];
				}

				// for origin
				$findWorkHour1 = \DB::select("select t1.shift_id,t1.shift_code,
						concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original,
						t1.hour_per_day as duration
						from attendance_work_shifts t1
						left join att_schedule t2 on t2.shift_id=t1.shift_id
						left join emp t3 on t3.employee_id=t2.employee_id
						where t3.employee_id='$name' and t2.date='$date' order by t2.schedule_id  desc limit 1");
				if(count($findWorkHour1)==0){
					return \Response::json(['header'=>['message'=>'No work hour found on '.$date.' for employee '.$name,'status'=>500],'data'=>null],500);
				}

				// for change
				$findWorkHour1extend  = \DB::SELECT("select 
													concat(t3.shift_code,' : ',t3._from,' - 	',t3._to) as Original, t3.shift_id as shift_id,  
													t2.update_at
													from att_change_shift as  t1, att_schedule_request as t2,attendance_work_shifts as t3
													where t1.id = t2.request_id  
													and  t1.type_id  = t2.type_id
													and t1.date= '$date' 
													and t1.employee_id  =  '$name' 
													and t2.status_id = 2
													and t3.shift_id = t1.new_shift 
													order by t1.id desc limit 1");
				// for swap
				$findWorkHour2extend  = \DB::SELECT("select t1.* from att_swap_shift as  t1
													where 
													(t1.employee_id  =  '$name' or t1.swap_with  =  '$name' )
													and t1.date= '$date' 
													and t1.status_id = 2
													order by t1.swap_id desc limit 1");

				$change_dt 	= $findWorkHour1extend;
				$swap_dt 	= $findWorkHour2extend;
				$origin_dt	= $findWorkHour1;

				if(count($change_dt) > 0 || count($swap_dt) > 0){
					if(count($change_dt) > 0 && count($swap_dt) > 0){
						if($change_dt[0]->updated_at > $swap_dt[0]->updated_at){
							$pure 		=  $change_dt[0]->Original;
							$shifts 	=  $change_dt[0]->shift_id;	
						}else{
							$pure 		=  $swap_dt[0]->Original;
							$shifts 	=  $swap_dt[0]->shift_id;
						}							
					}else if(count($change_dt) > 0){
						$pure 		=  $change_dt[0]->Original;
						$shifts 	=  $change_dt[0]->shift_id;	
					}else if(count($swap_dt) > 0){


						if($swap_dt[0]->employee_id == $name){
							$newid = $swap_dt[0]->new_shift_id;
							$getsch = \DB::select("select t1.shift_id, t1.shift_code, t1.hour_per_day as duration,concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original from attendance_work_shifts as t1 where t1.shift_id = $newid");

							$pure 	=  $getsch[0]->Original;
							$shifts =  $getsch[0]->shift_id;

						}else if($swap_dt[0]->swap_with === $name){
							$balik = true;
							$oldid = $swap_dt[0]->new_shift_id;
							$getsch = \DB::select("select t1.shift_id, t1.shift_code, t1.hour_per_day as duration,concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original from attendance_work_shifts as t1 where t1.shift_id = $oldid");

							$pure 	=  $getsch[0]->Original;
							//return [$pure,$obj_swap['pure']];
							$shifts =  $getsch[0]->shift_id;
						}
					}
				}else{
					$pure 		=  $origin_dt[0]->Original;
					$shifts 	=  $origin_dt[0]->shift_id;
				}

				if(isset($nameSwap) && $requestType == "Swap Shift / Day Off"){
					$dt = ["new"=>$obj_swap['pure'],"original"=>$pure];
					if(isset($balik)){
						$dt = ["new"=>$pure,"original"=>$obj_swap['pure']];
					}
					return \Response::json(['header'=>['message'=>'Show work shift record','status'=>200],'data'=>$dt],200);
				}else{
					$c = count($viewWorkShifts);
					for ($i=0; $i < $c; $i++) {
						$temp1 = $viewWorkShifts[$i]->shift_code.' : '.$viewWorkShifts[$i]->_from.' - '.$viewWorkShifts[$i]->_to;
						$temp[] = ['new'=>$temp1, 'shift_id'=>$viewWorkShifts[$i]->shift_id];
					}
					$dt = ["new"=>$temp,"original"=>$pure,"shift_id"=>$shifts];
					return \Response::json(['header'=>['message'=>'Show work shift record','status'=>200],'data'=>$dt],200);
				}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}

	public function work()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
		$model = new shiftModel;
		$LIB_ATT = new calculation;
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				$data = []; $message = 'Unauthorized'; $status = 200;
			}else{

				$viewWorkShifts = \DB::select("CALL View_WorkShift");
				if (!$viewWorkShifts){
					return \Response::json(['header'=>['message'=>'No work hour found','status'=>500],'data'=>null],500);
				}

				$name = \Input::json('name');
				$nameSwap = \Input::json('nameSwap');
				$requestType = \Input::json('requestType');
				$date = \Input::json('date');
				$temp = [];


				if($requestType == 'Change Shift'){
					try {
						$c = count($viewWorkShifts);
						
						$dt = $LIB_ATT->att_perdate('cut-off',$date,null," and t1.employee_id='".$name."' ",null);
						$name1 =  "".$dt['data'][0]['schedule_shift_code'].":".$dt['data'][0]['schedule'];

						for ($i=0; $i < $c; $i++) {
							if($dt['data'][0]['schedule_shift_code'] == $viewWorkShifts[$i]->shift_code){
								$shift_id = $viewWorkShifts[$i]->shift_id;
							}
							$temp1 = $viewWorkShifts[$i]->shift_code.' : '.$viewWorkShifts[$i]->_from.' - '.$viewWorkShifts[$i]->_to;
							$temp[] = ['new'=>$temp1, 'shift_id'=>$viewWorkShifts[$i]->shift_id];
						}
						

						$dt2 = ["new"=>$temp,"original"=>$name1,"shift_id"=>$shift_id];
					} catch (\Exception $e) {
						return \Response::json(['header'=>['message'=>"No work hour found for employee_id $name",'status'=>500],'data'=>null],500);
					}
				}else{
					try {
						$dt = $LIB_ATT->att_perdate('cut-off',$date,null," and t1.employee_id='".$nameSwap."' ",null);
						$name2 =  "".$dt['data'][0]['schedule_shift_code'].":".$dt['data'][0]['schedule'];

						$dt = $LIB_ATT->att_perdate('cut-off',$date,null," and t1.employee_id='".$name."' ",null);
						$name1 =  "".$dt['data'][0]['schedule_shift_code'].":".$dt['data'][0]['schedule'];

						$dt2 = ["new"=>$name2,"original"=>$name1];
					} catch (\Exception $e) {
						return \Response::json(['header'=>['message'=>"No work hour found for employee_id $nameSwap",'status'=>500],'data'=>null],500);
					}
				}

				return \Response::json(['header'=>['message'=>'Show work shift record','status'=>200],'data'=>$dt2],200);
				//$dt2 = ["new"=>$temp,"original"=>$pure,"shift_id"=>$shifts];
				//return $dt;
				
				if(isset($nameSwap) && $requestType == "Swap Shift / Day Off"){
					// for change
					// for origin
					$findWorkHour1 = \DB::select("select t1.shift_id,t1.shift_code,
							concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original,
							t1.hour_per_day as duration
							from attendance_work_shifts t1
							left join att_schedule t2 on t2.shift_id=t1.shift_id
							left join emp t3 on t3.employee_id=t2.employee_id
							where t3.employee_id='$nameSwap' and t2.date='$date' order by t2.schedule_id  desc limit 1");
					if(count($findWorkHour1)==0){
						return \Response::json(['header'=>['message'=>'No work hour found on '.$date.' for employee '.$nameSwap,'status'=>500],'data'=>null],500);
					}

					$findWorkHour1extend  = \DB::SELECT("select 
														concat(t3.shift_code,' : ',t3._from,' - 	',t3._to) as Original, t3.shift_id as shift_id,
														t2.update_at
														from att_change_shift as  t1, att_schedule_request as t2,attendance_work_shifts as t3
														where t1.id = t2.request_id  
														and  t1.type_id  = t2.type_id
														and t1.date= '$date' 
														and t1.employee_id  =  '$nameSwap' 
														and t2.status_id = 2
														and t3.shift_id = t1.new_shift 
														order by t1.id desc limit 1");
					// for swap
					$findWorkHour2extend  = \DB::SELECT("select t1.* from att_swap_shift as  t1
														where 
														(t1.employee_id  =  '$nameSwap' or t1.swap_with  =  '$nameSwap' )
														and t1.date= '$date' 
														and t1.status_id = 2
														order by t1.swap_id desc limit 1");

					$change_dt 	= $findWorkHour1extend;
					$swap_dt 	= $findWorkHour2extend;
					$origin_dt	= $findWorkHour1;

					if(count($change_dt) > 0 || count($swap_dt) > 0){
						if(count($change_dt) > 0 && count($swap_dt) > 0){
							if($change_dt[0]->updated_at > $swap_dt[0]->updated_at){
								$pure 		=  $change_dt[0]->Original;
								$shifts 	=  $change_dt[0]->shift_id;	
							}else{
								$pure 		=  $swap_dt[0]->Original;
								$shifts 	=  $swap_dt[0]->shift_id;
							}							
						}else if(count($change_dt) > 0){
							$pure 		=  $change_dt[0]->Original;
							$shifts 	=  $change_dt[0]->shift_id;	
						}else if(count($swap_dt) > 0){
							if($swap_dt[0]->employee_id == $nameSwap){
								$newid = $swap_dt[0]->new_shift_id;
								$getsch = \DB::select("select t1.shift_id, t1.shift_code, t1.hour_per_day as duration,concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original from attendance_work_shifts as t1 where t1.shift_id = $newid");

								$pure 	=  $getsch[0]->Original;
								$shifts =  $getsch[0]->shift_id;

							}else if($swap_dt[0]->swap_with == $nameSwap){
								$oldid = $swap_dt[0]->old_shift_id;
								//return 100;
								$getsch = \DB::select("select t1.shift_id, t1.shift_code, t1.hour_per_day as duration,concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original from attendance_work_shifts as t1 where t1.shift_id = $oldid");

								$pure 	=  $getsch[0]->Original;
								$shifts =  $getsch[0]->shift_id;
							}
						}
					}else{
						$pure 		=  $origin_dt[0]->Original;
						$shifts 	=  $origin_dt[0]->shift_id;
					}
					
					$obj_swap = ["pure"=>$pure,"shifts"=>$shifts];
				}

				// for origin
				$findWorkHour1 = \DB::select("select t1.shift_id,t1.shift_code,
						concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original,
						t1.hour_per_day as duration
						from attendance_work_shifts t1
						left join att_schedule t2 on t2.shift_id=t1.shift_id
						left join emp t3 on t3.employee_id=t2.employee_id
						where t3.employee_id='$name' and t2.date='$date' order by t2.schedule_id  desc limit 1");
				if(count($findWorkHour1)==0){
					return \Response::json(['header'=>['message'=>'No work hour found on '.$date.' for employee '.$name,'status'=>500],'data'=>null],500);
				}

				// for change
				$findWorkHour1extend  = \DB::SELECT("select 
													concat(t3.shift_code,' : ',t3._from,' - 	',t3._to) as Original, t3.shift_id as shift_id,  
													t2.update_at
													from att_change_shift as  t1, att_schedule_request as t2,attendance_work_shifts as t3
													where t1.id = t2.request_id  
													and  t1.type_id  = t2.type_id
													and t1.date= '$date' 
													and t1.employee_id  =  '$name' 
													and t2.status_id = 2
													and t3.shift_id = t1.new_shift 
													order by t1.id desc limit 1");
				// for swap
				$findWorkHour2extend  = \DB::SELECT("select t1.* from att_swap_shift as  t1
													where 
													(t1.employee_id  =  '$name' or t1.swap_with  =  '$name' )
													and t1.date= '$date' 
													and t1.status_id = 2
													order by t1.swap_id desc limit 1");

				$change_dt 	= $findWorkHour1extend;
				$swap_dt 	= $findWorkHour2extend;
				$origin_dt	= $findWorkHour1;

				if(count($change_dt) > 0 || count($swap_dt) > 0){
					if(count($change_dt) > 0 && count($swap_dt) > 0){
						if($change_dt[0]->updated_at > $swap_dt[0]->updated_at){
							$pure 		=  $change_dt[0]->Original;
							$shifts 	=  $change_dt[0]->shift_id;	
						}else{
							$pure 		=  $swap_dt[0]->Original;
							$shifts 	=  $swap_dt[0]->shift_id;
						}							
					}else if(count($change_dt) > 0){
						$pure 		=  $change_dt[0]->Original;
						$shifts 	=  $change_dt[0]->shift_id;	
					}else if(count($swap_dt) > 0){


						if($swap_dt[0]->employee_id == $name){
							$newid = $swap_dt[0]->new_shift_id;
							$getsch = \DB::select("select t1.shift_id, t1.shift_code, t1.hour_per_day as duration,concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original from attendance_work_shifts as t1 where t1.shift_id = $newid");

							$pure 	=  $getsch[0]->Original;
							$shifts =  $getsch[0]->shift_id;

						}else if($swap_dt[0]->swap_with === $name){
							$balik = true;
							$oldid = $swap_dt[0]->new_shift_id;
							$getsch = \DB::select("select t1.shift_id, t1.shift_code, t1.hour_per_day as duration,concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original from attendance_work_shifts as t1 where t1.shift_id = $oldid");

							$pure 	=  $getsch[0]->Original;
							//return [$pure,$obj_swap['pure']];
							$shifts =  $getsch[0]->shift_id;
						}
					}
				}else{
					$pure 		=  $origin_dt[0]->Original;
					$shifts 	=  $origin_dt[0]->shift_id;
				}

				if(isset($nameSwap) && $requestType == "Swap Shift / Day Off"){
					$dt = ["new"=>$obj_swap['pure'],"original"=>$pure];
					if(isset($balik)){
						$dt = ["new"=>$pure,"original"=>$obj_swap['pure']];
					}
					return \Response::json(['header'=>['message'=>'Show work shift record','status'=>200],'data'=>$dt],200);
				}else{
					$c = count($viewWorkShifts);
					for ($i=0; $i < $c; $i++) {
						$temp1 = $viewWorkShifts[$i]->shift_code.' : '.$viewWorkShifts[$i]->_from.' - '.$viewWorkShifts[$i]->_to;
						$temp[] = ['new'=>$temp1, 'shift_id'=>$viewWorkShifts[$i]->shift_id];
					}
					$dt = ["new"=>$temp,"original"=>$pure,"shift_id"=>$shifts];
					return \Response::json(['header'=>['message'=>'Show work shift record','status'=>200],'data'=>$dt],200);
				}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}



	// public function work()
	// {
	// 	$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
	// 	$model = new shiftModel;
	// 	if($access[1] == 200){
	// 		if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
	// 			$data = []; $message = 'Unauthorized'; $status = 200;
	// 		}else{
	// 				$name = \Input::json('name');
	// 				$nameSwap = \Input::json('nameSwap');
	// 				$requestType = \Input::json('requestType');
	// 				$date = \Input::json('date');
	// 				$temp = [];

	// 				//$findWorkHour1 = \DB::select("CALL View_WorkShift_by_emp('$name','$date','change')");
	// 				//return $arr  = [$nameSwap,$date];
	// 				// $findWorkHour1extend  = \DB::SELECT("select concat(attendance_work_shifts.shift_code,' : ',attendance_work_shifts._from,' - ',attendance_work_shifts._to) as Original, attendance_work_shifts.shift_id as shift_id  from att_change_shift, att_schedule_request,attendance_work_shifts
	// 				// 									where att_change_shift.id  = att_schedule_request.request_id  
	// 				// 									and att_change_shift.date = att_schedule_request.availment_date
	// 				// 									and att_change_shift.availment_date = '$date' 
	// 				// 									and att_schedule_request.employee_id  =  '$name' 
	// 				// 									and att_schedule_request.status_id = 2
	// 				// 									and attendance_work_shifts.shift_id = att_change_shift.new_shift 
	// 				// 									and year(att_change_shift.date) = '2017'
	// 				// 									order by att_schedule_request.id desc limit 1");

	// 					if($date ==  '2017-12-05'){
	// 						$var =   "select t1.shift_id,t1.shift_code,
	// 						concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original,
	// 						t1.hour_per_day as duration 
	// 						from attendance_work_shifts t1
	// 						left join att_schedule t2 on t2.shift_id=t1.shift_id
	// 						left join emp t3 on t3.employee_id=t2.employee_id
	// 						where t3.employee_id='$name' and t2.date='$date'";

	// 						return  $var;
	// 					}
	// 					$findWorkHour1extend  = \DB::SELECT("select concat(t3.shift_code,' : ',t3._from,' - 	',t3._to) as Original, t3.shift_id as shift_id  from att_change_shift as  t1, att_schedule_request as t2,attendance_work_shifts as t3
	// 													where t1.id = t2.request_id  
	// 													and  t1.type_id  = t2.type_id
	// 													and t1.date= '$date' 
	// 													and t1.employee_id  =  '$name' 
	// 													and t2.status_id = 2
	// 													and t3.shift_id = t1.new_shift 
	// 													order by t1.id desc limit 1");

	// 				 $findWorkHour1 = \DB::select("select t1.shift_id,t1.shift_code,
	// 						concat(t1.shift_code,' : ',date_format(t1._from,'%H:%i'),' - ',date_format(t1._to,'%H:%i')) as Original,
	// 						t1.hour_per_day as duration 
	// 						from attendance_work_shifts t1
	// 						left join att_schedule t2 on t2.shift_id=t1.shift_id
	// 						left join emp t3 on t3.employee_id=t2.employee_id
	// 						where t3.employee_id='$name' and t2.date='$date' order by t2.schedule_id  desc limit 1");

	// 				if(count($findWorkHour1) > 0){
	// 					$pure =  $findWorkHour1[0]->Original;
	// 					$shifts  =   $findWorkHour1[0]->shift_id;
	// 				}else if(count($findWorkHour1extend) > 0){
	// 					$pure =  $findWorkHour1extend[0]->Original;
	// 					$shifts =  $findWorkHour1extend[0]->shift_id;
	// 				}
	// 				$findWorkHour2 = \DB::select("CALL View_WorkShift_by_emp('$nameSwap','$date','change')");
	// 				$viewWorkShifts = \DB::select("CALL View_WorkShift");

	// 				// $counter = \DB::select("SELECT distinct date,employee_id FROM att_change_shift");
	// 				// return $counter;
	// 				// return $date;
	// 				// $date = substr($date, start);
	// 				// $date = substr($date,strpos($date,'-')+1,strpos($date,'-'));
	// 				if (!$viewWorkShifts){
	// 					return \Response::json(['header'=>['message'=>'No work hour found','status'=>500],'data'=>null],500);
	// 				}
	// 				if ($findWorkHour1 == null and $name ==  null){
						
	// 					return \Response::json(['header'=>['message'=>'No work hour found on '.$date.' for employee '.$name,'status'=>500],'data'=>null],500);
	// 				}else if(!isset($findWorkHour1[0]->Original)  and  !isset($findWorkHour1[0]->shift_id)){
	// 					return \Response::json(['header'=>['message'=>'No work hour found on '.$date.' for employee '.$name,'status'=>500],'data'=>null],500);
	// 				}else{
						
	// 					$data1 = ['original'=>$pure, 'shift_id'=>$findWorkHour1[0]->shift_id];
	// 				}

	// 				$c = count($viewWorkShifts);
	// 				for ($i=0; $i < $c; $i++) {
	// 					$temp1 = $viewWorkShifts[$i]->shift_code.' : '.$viewWorkShifts[$i]->_from.' - '.$viewWorkShifts[$i]->_to;
	// 					$temp[] = ['new'=>$temp1, 'shift_id'=>$viewWorkShifts[$i]->shift_id];
	// 				}

	// 				if ($requestType == "Change Shift")
	// 					return \Response::json(['header'=>['message'=>'Show work shift record','status'=>200],'data'=>['original'=>$pure, 'shift_id'=>$shifts, 'new'=>$temp]],200);
	// 				else{
	// 					if (!$findWorkHour2){
	// 						return \Response::json(['header'=>['message'=>'No work hour found on '.$date.' for employee '.$nameSwap,'status'=>500],'data'=>null],500);
	// 					}
	// 					else{
	// 						return \Response::json(['header'=>['message'=>'Show work shift record','status'=>200],'data'=>['original'=>$findWorkHour1[0]->Original, 'new'=>$findWorkHour2[0]->Original]],200);
	// 					}
	// 				}
	// 			}
	// 		}else{
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 	}
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	// }

	// find original work hour by current date
	public function WorkHourCurrentDate()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
		$model = new shiftModel;
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
				$name = \Input::get('name');
				$swapWith = \Input::get('swapWith');
				$requestType = \Input::get('requestType');

				$findWorkHour1 = \DB::select("CALL View_WorkShift_by_curdate('$name')");
				$findWorkHour2 = \DB::select("CALL View_WorkShift_by_curdate('$swapWith')");
				$viewWorkShifts = \DB::select("CALL View_WorkShift");
				if ($findWorkHour1 == null){
					return \Response::json(['header'=>['message'=>'No work hour found on current date','status'=>500],'data'=>null],500);
				}
				if ($requestType == "Change Shift"){
					if (!$findWorkHour1){
						return \Response::json(['header'=>['message'=>'No work hour found on current date','status'=>500],'data'=>null],500);
					}
					else{
						$data1 = $findWorkHour1[0]->Original;
					}
					return \Response::json(['header'=>['message'=>'Success','status'=>200],'data'=>['original'=>$data1, 'new'=>$viewWorkShifts]],200);
				}
				else{
					if (!$findWorkHour2){
						return \Response::json(['header'=>['message'=>'No work hour found on current date','status'=>500],'data'=>null],500);
					}
					else{
						$data1 = $findWorkHour1[0]->Original;
						$data2 = $findWorkHour2[0]->Original;
					}
					return \Response::json(['header'=>['message'=>'Success','status'=>200],'data'=>['employee'=>$data1, 'swapWith'=>$data2]],200);
				}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}
}
