<?php namespace Larasite\Http\Controllers\Attendance\ScheduleRequest;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
use Larasite\Model\Attendance\Request\TrainingRequest;
use Larasite\Http\Controllers\Attendance\notificationV3;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;

class TrainingRequest_Ctrl extends Controller {
	protected $form = 64;

	function setup_input($io){
		$decode = json_decode($io);
		foreach ($decode as $value => $key){
			$req['name'] = $key->name;
			$req['start'] = $key->start;
			$req['end'] = $key->end;
			$req['type'] = $key->type;
			$req['comment'] = $key->comment;
		}
		return $req;
	}

	public function createTrainingRequest()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); //$model = new TrainingRequest;
		// return $access[1];
		if($access[1] == 200){
			$request = new leaverequest_Model;
		    $check_exist  =  \Input::get('data');
			$input  =  \Input::all();
			$decode = base64_decode(\Request::get('key'));
			$nameDefault = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
			
			//check request can't lowrer ttah current day
			if(isset($input)){
				if(isset($input['data'])){
					$data =  json_decode($input['data']);
					$date_now =  date('Y-m-d');
					
					 ;
					if(isset($data->data->end)){
						if($data->data->end < $date_now){
							return response()->json(['header'=>['message'=>"request can't lower than ".$date_now,'status'=>500],'data'=>null],500);
						}
					}
					if(isset($data->data->start)){
						if($data->data->start < $date_now){
							return response()->json(['header'=>['message'=>"request can't lower than ".$date_now,'status'=>500],'data'=>null],500);
						}	
					}

					if(isset($data->data->start) && isset($data->data->end)){
						if($data->data->start > $data->data->end){
							return response()->json(['header'=>['message'=>"Error : Date end can't lower than Date End",'status'=>500],'data'=>null],500);
						}
					}
					
				}else{
					$date_now =  date('Y-m-d');
					if(isset($input['end'])){
						if($input['end'] < $date_now){
							return response()->json(['header'=>['message'=>"request can't lower than current date",'status'=>500],'data'=>null],500);
						}
					}

					if(!isset($input['type'])){
						// if($input['end'] < $date_now){
							return response()->json(['header'=>['message'=>"type is required",'status'=>500],'data'=>null],500);
						//}
					} 

					if(isset($input['start'])){
						if($input['start'] < $date_now){
							return response()->json(['header'=>['message'=>"request can't lower than current date",'status'=>500],'data'=>null],500);
						}	
					}

					if(isset($input['start']) && isset($input['end'])){
						if($input['start'] > $input['end']){
							return response()->json(['header'=>['message'=>"Error : Date end can't lower than Date End",'status'=>500],'data'=>null],500);
						}
					}
				}
			}

			
			 $emp = \DB::SELECT("select * from view_nonactive_login where employee_id = '$nameDefault' and  (lower(role_name) like '%user%' or lower(role_name) like  '%employee_user%' or lower(role_name) like '%regular employee_user%' or lower(role_name) like  '%superuser%' or lower(role_name) like  '%human resource%' or lower(role_name) like  '%hr%')  limit 1");
			if($emp == null){
				return \Response::json(['header'=>['message'=>'Requestor only employee for local and expat','status'=>500],'data'=>null],500);
			}


			if(!isset($check_exist)){
				//$input = $this->setup_input(\Input::get('data'));
				
				// return $employee_id;

				// $name = \Input::get('name');
				if (isset($input['name'])){ $name = $input['name']; }else{ $name = $nameDefault; }

				//check if employee
				

				$start = $input['start'];
				$type = $input['type'];
				if(!isset($start)){ $start = null; }else{ $start = $start; }

				$end = $input['end'];
				if(!isset($end)){ $end = null; }else{ $end = $end; }

				$comment = $input['comment'];
				
				if(!isset($comment)){ return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500); }


				// $reg = ['text_num'=>'Regex:/^[ñA-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
				// 		'text_only'=>'Regex:/^[ñA-Za\-! ,\'\"\/@\.:\(\)]+$/',
				// 		'twit'=>'regex:/^[ñA-Za-z0-9_]{1,15}$/'];
				// $rule =  [
				// 	'type'=>'required'
				// ];
				//$validator = \Validator::make(\Input::all(),$rule);

				if (!isset($input['type'])){
				//	$message = $validator->errors()->all();
					return \Response::json(['header'=>['message'=>'Type is required.','status'=>500],'data'=>null],500);
				}
				else{
					// find request type id by name
					$findType = \DB::select("CALL Search_type_by_name('$type')");
					if (!$findType){
						return \Response::json(['header'=>['message'=>'No type found','status'=>500],'data'=>null],500);
					}
					else{
						$type = $findType[0]->type_id;
					}
					// find employee name by id
					$userID = \DB::select("CALL search_employee_by_id('$name')");
					if (!$userID){
						return \Response::json(['header'=>['message'=>'No employee','status'=>200],'data'=>null],200);}
					else{
						if(!isset($comment)){
							return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
						}					}
						// if user id is found, insert trainig request
						$check_req = \DB::SELECT("select created_at from att_training where employee_id='$name' and  type_id=$type and start_='$start' and end='$end' ");
						if($check_req != null){
								$message = "Request is same as before request at ".$check_req[0]->created_at;
								$status = 500;
								$data = [];
						}else{

						// $createRequest = \DB::select("CALL insert_training('$name',$type,'$start','$end')");
						// $select = \DB::SELECT("select id from att_schedule_request where employee_id='$name' order by id DESC");
						// $id_sel = $select[0]->id;
						// $modex  =  new notificationV3;
						// //$typex  = ($type == 1 ? 'train' : 'ob');
						// //($ids,$type,$employee_id,$local_it,$user,$from_type = null)
					 //   	$insert = \DB::SELECT("insert into command_center(request_id,type_id,master_type,comment,path,filename,employee_id) values
					 //   		                                             ($id_sel,$type,1,'$comment','null','null','$name')");
					 //   		                                             
		                                             
						return $check = $modex->notif($id_sel,$type,$name,'local','user','attendance');	
						

						return \Response::json(['header'=>['message'=>'Success to apply training request','status'=>200, 'access'=>$access[3]],'data'=>[]],200);
						}
					}
			}else{

				$file = \Input::file('file');
				$json = \Input::get('data');
				$data = json_decode($json,1);

				if(!isset($data['data']['type'])){
					return  $request->getMessage("failed , please fill required form before submit data",[], 500, $access[3]);
				}else{
					$type = $data['data']['type'];
				}

				if(!isset($data['data']['name'])){
					return  $request->getMessage("failed , please fill required form before submit data",[], 500, $access[3]);
				}else{
					$name = $data['data']['name'];
				}

				if(!isset($data['data']['end'])){
					$end = null;
				}else{
					$end = $data['data']['end'];
				}

				if(!isset($data['data']['start'])){
					$start = null;
				}else{
					$start = $data['data']['start'];
				}

				if(isset( $data['data']['comment'])){
					$comment = $data['data']['comment'];
				}else{
					//$comment = "null";
					return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);
				}

				$image = \Input::file('file');
				if(isset($image)){
					$com = \Input::get('data');
					$comment = json_decode($com,1);
					$comment = $comment['data']['comment'];
					if(!isset($comment)){return response()->json(["header" => ["message"=> "please insert comment before submit data", "status" => 500],"data"=> []],500);}
					$findType = \DB::select("CALL Search_type_by_name('$type')");
					if (!$findType){
						return \Response::json(['header'=>['message'=>'No type found','status'=>500],'data'=>null],500);
					}
					else{
						$type = $findType[0]->type_id;
					}
					// find employee name by id
					$userID = \DB::select("CALL search_employee_by_id('$name')");
					if (!$userID){
						return \Response::json(['header'=>['message'=>'No employee','status'=>200],'data'=>null],200);}
					else{
						$imageName = $image->getClientOriginalName();
						$ext = \Input::file('file')->getClientOriginalExtension();
						$path = "hrms_upload/training";
						$size = $image->getSize();

						$rename = 'Training_request'.str_random(6).".".$image->getClientOriginalExtension();

			  			//if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
							if($size > 104857600){
								return \Response::json(['header'=>['message'=>'Image size only support up to 10 MB','status'=>500],'data'=>null],500);
							}else{
								\Input::file('file')->move(storage_path($path),$rename);
							}
						// }else{
						// 		return \Response::json(['header'=>['message'=>'Unsupported format file','status'=>500],'data'=>null],500);
						// }

						// if user id is found, insert trainig request , ,'$comment','$imageName','$path'  insert into command_center(trail,id_trail,comment,path,filename) values(1,2,"shit","shit","shit");
						//check request already exist
	 				   $check_req = \DB::SELECT("select created_at from att_training where employee_id='$name' and  type_id=$type and start_='$start' and end='$end' ");
					   $get_detail  = \DB::SELECT("select concat(first_name,' ',middle_name,' ',last_name)as name, personal_email,work_email  from emp where employee_id =  '$name' ");
					
					   if($get_detail[0]->personal_email ==  null and $get_detail[0]->work_email == null){
					   		return response()->json(['header' => ['message' => 'please complete personal detail especially for work email and personal email', 'status' => 500],'data' => []],500);
					   }

						if($check_req != null){
								$message = "Request is same before ".$check_req[0]->created_at;
								$status = 500;
								$data = [];
						}else{



						$createRequest = \DB::select("CALL insert_training('$name',$type,'$start','$end')");
						$select = \DB::SELECT("select id from att_schedule_request where employee_id='$name' order by id DESC");
						//$select = \DB::('att_schedule_request')->insertGetId
						$id_sel = $select[0]->id;
						$typex  = ($type == 1 ? 'train' : 'ob');
						$insert = \DB::SELECT("insert into command_center(request_id,type_id,master_type,comment,path,filename,employee_id) values
																             ($id_sel,$type,1,'$comment','$path','$rename','$name')");
						$modex = new notificationV3;
						$check = $modex->notif($id_sel,$type,$name,'local','user','attendance');	
						return \Response::json(['header'=>['message'=>'Success to apply training request ','status'=>200, 'access'=>$access[3]],'data'=>[]],200);
						}
					}

				}



			}

	}else{
   			$message = $access[0]; $status = $access[1]; $data=$access[2];
  		}
  		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// search name using suggestion
	public function findName($string){

		$input = \Input::get("key");
		$explode = explode("-",base64_decode($input));
		$empID = $explode[1];
		$searchData  =  null;

		//$data  = $model->departmentList($empID);
		$hr = \DB::select("select * from view_nonactive_login where employee_id  = '$empID' and  role_name like '%human%' and role_name like '%human resource%' ");
		$user = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$empID' AND t2.role_name like '%user%' AND lower(t2.role_name) != 'superuser' ");
    	$sup  = \DB::SELECT("select * from emp_supervisor where supervisor =  '$empID'  ");
		$check_employee = \DB::SELECT("select role.role_name  from role,ldap where ldap.employee_id = '$empID' and ldap.role_id = role.role_id and role.role_name like '%employee%' ");
		

 		if($user != null && $sup == null){
    			$searchData = \DB::SELECT("CALL search_employee_by_id('$empID')");			
    		}
	/**
    	if($sup != null && $user != null){
    		$searchother  = \DB::SELECT("select emp.employee_id,concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as name from emp,emp_supervisor where emp.employee_id  =  emp_supervisor.employee_id and emp_supervisor.supervisor = '$empID' and (emp.first_name like '%$string%' or emp.middle_name like '%$string%' and  emp.last_name like '%$string%')");			
    		$selfsearch  = \DB::SELECT("select emp.employee_id,concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as name from emp where employee_id =  '$empID' and (emp.first_name like '%$string%' or emp.middle_name like '%$string%' and  emp.last_name like '%$string%') ");

    		$searchData =  array_merge($selfsearch,$searchother);


    	}**/
	
	$arr  = [];

	//return $arr  = [$hr,$sup,$user];
	if($searchData == null && strlen($string) >= 4){	
		
	  	if($hr != null  ){
			$searchData = \DB::select("CALL Search_emp_by_name('%$string%')");
			if($searchData != null){	
		  		foreach($searchData  as $key  => $value){
					$terminate_who  = $value->name;
					if(strpos($terminate_who,'(') !== false){
						unset($searchData[$key]);
					}
		  		}
				
				$arr  = $searchData;	
			}
					
	 	}
		

		if($hr == null && $sup != null){	
		
				$searchData = \DB::select("CALL Search_emp_by_name('%$string%')");
				
					$searchData = \DB::select("CALL Search_emp_by_name('%$string%')");
					
					foreach($sup as $key  => $value ){
						foreach($searchData  as $keyx  => $valuex){
							if($value->employee_id != $valuex->employee_id){
								if($valuex->employee_id  !=   $empID){		
									if(strpos($value->name,'(') !== false){	
										unset($searchData[$keyx]);
									}elseif(strtolower($value->name) == 'superuser' ){
										unset($searchData[$key]);	
									}else{
										unset($searchData[$keyx]);
									}
								}
							}
						}
					}	
						
						$arr  = $searchData;
					
		}

		if($empID  == '2014888'){
			$searchData = \DB::select("CALL Search_emp_by_name('%$string%')");
			if($searchData != null){	
		  		foreach($searchData  as $key  => $value){
					$terminate_who  = $value->name;
					if(strpos($terminate_who,'(') !== false){
						unset($searchData[$key]);
					}

					if(strtolower($value->name) == 'superuser' ){
						unset($searchData[$key]);	
					}
		  		}
				
				$arr  = $searchData;	
			}
		}
		return \Response::json(array_values(array_filter($arr)));
		
	}

	// elseif($arr  ==  null){
	// 	return \Response::json(['header'=>['message'=>'No employee name found','status'=>200],'data'=>null],200);		
	// }elseif($arr != null){
	// 	return \Response::json($arr);
	// }elseif(strlen($string) < 4){
	// 	return \Response::json(['header'=>['message'=>'Minimum character is 4','status'=>200],'data'=>null],200);

	// }else{
	// 	return \Response::json(['header'=>['message'=>'Error when search data','status'=>200],'data'=>null],200);
	// }
 	
	
	/**
        if (strlen($string) >= 4){
    	    if ($searchData){
    	    		if($check_employee != null){
	    	   			foreach ($searchData as $key => $value) {
		    	   			$sub =  str_split($value->name);
		    	   			foreach ($sub as $subkey => $subvalue) {
		    	   				if($subvalue == "("){
		    	   					unset($searchData[$key]);	
		    	   				}
		    	   			}
	    	   			}
	    	   		}
	    	   		
    	   			if(!isset($searchData[0])){
    	   				return \Response::json([$searchData]);
    	   			}else{
    	   				return \Response::json($searchData);
    	   			}

    	   		//	return $result = gettype($searchData) ;
    	   		//return \Response::json($result);
        	}else{
                return \Response::json(['header'=>['message'=>'No employee name found','status'=>200],'data'=>null],200);}
        }else{
            return \Response::json(['header'=>['message'=>'Minimum character is 4','status'=>200],'data'=>null],200);
        }**/
	}

	public function findName1($string)
	{

		$input = \Input::get("key");
		$explode = explode("-",base64_decode($input));
		$empID = $explode[1];


		//	$data  = $model->departmentList($empID);
		$hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$empID' AND t2.role_name like '%hr%' or t2.role_name like '%human%' or  t2.role_name like '%human resource%' ");
		$user = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$empID' AND t2.role_name like '%user%' ");
    	
		$check_employee = \DB::SELECT("select role.role_name  from role,ldap where ldap.employee_id = '$empID' and ldap.role_id = role.role_id and role.role_name like '%employee%' ");

    	if($hr != null){
    		return $searchData = \DB::select("CALL Search_emp_by_name('%$string%')");
    		
    	}else if($user != null){
    		$searchData = \DB::SELECT("CALL search_employee_by_id('$empID')");		
    		
    	}else{
    		$searchData = \DB::select("CALL search_emp_subordinate('$empID','%$string%')");
    		
    	}
    
        if (strlen($string) >= 4){
    	    if ($searchData){
    	    		if($check_employee != null){
	    	   			foreach ($searchData as $key => $value) {
		    	   			$sub =  str_split($value->name);
		    	   			foreach ($sub as $subkey => $subvalue) {
		    	   				if($subvalue == "("){
		    	   					unset($searchData[$key]);	
		    	   				}
		    	   			}
	    	   			}
	    	   		}
	    	   		
    	   			if(!isset($searchData[0])){
    	   				return \Response::json([$searchData]);
    	   			}else{
    	   				return \Response::json($searchData);
    	   			}

    	   		//	return $result = gettype($searchData) ;
    	   		return \Response::json($result);
        	}else{
                return \Response::json(['header'=>['message'=>'No employee name found','status'=>200],'data'=>null],200);}
        }
        else{
            return \Response::json(['header'=>['message'=>'Minimum character is 3','status'=>200],'data'=>null],200);}
	}



	// public function findNamev1($string)
	// {
	// 	$input = \Input::get("key");
	// 	$explode = explode("-",base64_decode($input));
	// 	$empID = $explode[1];
	// 	//	$data  = $model->departmentList($empID);
	// 	$hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$empID' AND t2.role_name like '%hr%' or t2.role_name like '%human%' or  t2.role_name like '%human resource%' ");
	// 	$user = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$empID' AND t2.role_name like '%user%' ");
 //    	if($hr != null){
 //    		$searchData = \DB::select("CALL Search_emp_by_name('%$string%')");
 //    	}else if($user != null){
 //    		$searchData = \DB::SELECT("CALL search_employee_by_id('$empID')");		
 //    	}else{
 //    		$searchData = \DB::select("CALL search_emp_subordinate('$empID','%$string%')");
 //    	}
   		
 //   		if($searchData == null){		
 //   			    return \Response::json(['header'=>['message'=>'Employee name  not found','status'=>200],'data'=>null],200);
 //   		}else{
 //            return \Response::json(['header'=>['message'=>'Data not exist ( character miniumum 4 character)','status'=>200],'data'=>null],200);
 //        }
	// }

	public function filter(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$string = \Input::get('name');
		  $input = \Input::get("key");
			$explode = explode("-",base64_decode($input));
			$eemp_super = $explode[1];
			$searchData = \DB::select("CALL Search_emp_by_name('%$string%')");
			foreach ($searchData as $key => $value) {
				$emp[] = $value->employee_id;
			}
			$implode = implode(',',$emp);

			$data = \DB::SELECT("select emp_supervisor.employee_id,concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name)as name from emp_supervisor,emp where emp_supervisor.supervisor='$eemp_super' and emp_supervisor.employee_id in ($implode) and emp.employee_id in ($implode) ");

			if($data != null){
					return \Response::json(['header'=>['message'=>'Success','status'=>200],'data'=>data],200);
			}else{
					return \Response::json(['header'=>['message'=>'Name not found','status'=>500],'data'=>null],500);
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];

				}
				return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
	}


	public function api_check(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$message = "Unauthorized"; $data = []; $status =  200;
			}else{

			$request = new leaverequest_Model;
				$key = \Input::get("key");
				$encode = base64_decode($key);
				$explode = explode("-",$encode);
				$explode_id = $explode[1];
				$supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$explode_id' ");
				//return $hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$explode_id' AND (lower(t2.role_name) like '%hr%' or '%human resource%' or '%resource human%')");
				$hr = \DB::SELECT("select * from view_nonactive_login where employee_id = '$explode_id' and (lower(role_name) like '%hr%' or  lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' )");
				if($supervisor != null){
					$supervisor = "supervisor";
				}elseif($hr != null){
					$supervisor = "hr";
				}else{
					$supervisor =  "user";
				}
				$emp_data = \DB::SELECT("CALL search_employee_by_id('$explode_id')");

				$checkSU = \DB::SELECT("select role_id from ldap where employee_id ='$explode_id' ");
				if($checkSU[0]->role_id ==  33 && $supervisor == "user"){
					$supervisor = "superSU";
				}
					$data_exp =
					 [
					 "employee_name" => $emp_data[0]->name,
					 "employee_id" => $explode_id,
					 "status" => $supervisor
					];

				$message = "success"; $data = $data_exp; $status =  200;
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];

		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
	}
}
