<?php namespace Larasite\Http\Controllers\Attendance\ScheduleRequestDetail;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Attendance\Request\RequestDetail_Model;
use Illuminate\Http\Request;
use Larasite\Library\FuncAccess;

class RequestDetailUpdate_Ctrl extends Controller {
	protected $form = 68;

	// VIEW SCHEDULE REQUEST DETAIL
	
	public function edit($id)
	{

		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
	
			// current logged in user id
			$decode = base64_decode(\Request::input('key'));
			$empID = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));

			// if($empID == null){
			// 	$empID = " ";
			// }
			$type_id = \Input::get('type_id');

			if($type_id ==  null){
			 	$type_id  =  \DB::SELECT("select type_id from att_schedule_request where id  =  $id ");
				$type_id  = $type_id[0]->type_id;
			}
			$ex = \Input::get('type');
			$sub = \Input::get('sub');
			$empreq = \Input::get('employee_id');
			if($empreq == null ){
				$empreq = " ";
			}
				$empswap = \Input::get('empswap');
				if($empswap = null){
					$empswap = " ";
				}
				$avail = \Input::get('availment_date');
				$avails = \Input::get('availment');
				if($avail == null ){
					$avail = " ";
				}
			$model = new RequestDetail_Model;
			
			$data = [];
			$avails_not_array = false;
			if(strpos(",",$avails)){
				$avails_not_array = true;
				$avails = explode(" , ",$avails);
			}
			
			if(count($sub) > 0){			
				for ($i=0; $i < count($sub); $i++) { 

					if($empswap == " " || !$empswap){
						if($type_id == 6){						
							$r = \DB::select("select json_data from pool_request where id_req=$sub[$i] and type_id = $type_id and master_type = 1");
							if(count($r)>0){
								$json_x2  = json_decode($r[0]->json_data,1);
								$json_r = $json_x2;
								$swap_raw = array_keys($json_r['swap'][0]);
								$empswap = $swap_raw[0];
							}
						}
					}
					//return [$sub[$i],$type_id,$ex,$empID,$empreq,$empswap,$avails[$i]];
					if($avails_not_array){
						$a = $model->editDetail($sub[$i],$type_id,$ex,$empID,$empreq,$empswap,$avails[$i]);
					}else{
						$a = $model->editDetail($sub[$i],$type_id,$ex,$empID,$empreq,$empswap,$avails);
					}
					
					if(count($a) > 0){
						//$data[$i] = $a[0];
						$data[$i] = $a[0];
					}
				}

			}else{
				$data = $model->editDetail($id,$type_id,$ex,$empID,$empreq,$empswap,$avail);
			}
			//$data[0]->status = \Input::get('status'); // sementara

			// $comments = \Input::get('comment');
			// foreach ($data as $key => $value) {
			// 	$data[$key]->comment = $comments;
			// }
			return $data;

			//return \Response::json(['header'=>['message'=>$data['message'],'status'=>$data['status'], 'access'=>$access[3]],'data'=>$data['data']],$data['status']);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		//return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	//DOWNLOAD FILE
	public function getfile($id){

		if(isset($id)){
			$data = \DB::SELECT("select path from command_center where filename='$id' ");
			 $path = $data[0]->path;
			$path = storage_path($path.'/'.$id);
			$file = \File::get($path);
			$type = \File::mimeType($path);
			return \Response::make($file,200,['Content-Type'=>$type]);
		}else{
			return \Response::json(['header'=>['message'=>"Failed to get image data",'status'=>401]],401);
		}
	}


	//APPROVE DATA
	function  approve($id){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200;
			}else{

				$i = \Input::all();
		
				$emp = explode('-',base64_decode(\Input::get('key')));
				$arr = $emp[1];

			if(isset($i['id']) && $i['id'] !=  null){
				$db  = \DB::SELECT("select * from pool_request where id_req  = $i[id] ");
				$request_id  =  $db[0]->id_req;
				if($db !=  null){
					$request = [];
					$js =  json_decode($db[0]->json_data);
						//foreach ($js as $max => $max_val) {
					$emp  =  $db[0]->employee_id;
					$lc = \DB::SELECT("select local_it from emp where employee_id = '$emp'");
					if($db[0]->type_request == 'time' ){
						$schema = ['hr','sup','emp']; 
						$end = 'hr'; 
					}elseif ($db[0]->type_request == 'swap') {
						$end  = 'sup';
						$schema = ['emp','sup','hr'];
					}elseif ($db[0]->type_request == 'time') {
						$schema = ['sup','hr','emp'];
						$end = 'sup';
					}elseif ($db[0]->type_request == 'over') {
						$schema = ['sup','hr','emp'];
						$end = 'sup';
					}elseif ($db[0]->type_request == 'under') {
						$schema = ['sup','hr','emp'];
						$end = 'sup';
					}elseif ($db[0]->type_request == 'late') {
						$schema = ['sup','hr','emp'];
						$end  =  'hr';
					}elseif ($db[0]->type_request == 'early') {
						$schema = ['sup','hr','emp'];
						$end  = 'hr';
					}elseif ($db[0]->type_request == 'train'){
						$end = 'sup';
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'ob'){
						$schema = ['sup','hr','emp'];
						$end  =  'sup';
					}elseif ($db[0]->type_request == 'change'){
						$emp = \DB::SELECT("select * from emp_supervisor where supervisor =  '$emp' ");
						if($emp != null){
							$end = 'emp';
							$schema = ['emp','hr','sup'];
						}else{
							$end  =  'sup';
							$schema = ['sup','emp','hr'];
						}
					}elseif ($db[0]->type_request == 'vacation'  &&  $lc[0]->local_it != 1){
						$schema = ['hr','sup','hr'];
					}elseif ($db[0]->type_request == 'ADO'  &&  $lc[0]->local_it == 1){
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'vacation'  &&  $lc[0]->local_it == 1){
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'enhance'  &&  $lc[0]->local_it == 1){
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'birth'  &&  $lc[0]->local_it != 1){
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'sick' ) {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}else{
								$schema = ['hr','sup','emp'];
							}
						}		
					}elseif ($db[0]->type_request == 'maternity' ) {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it != 1){
								$schema = ['hr','sup','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['hr','sup','emp'];
							}
						}
					}elseif ($db[0]->type_request == 'paternity'  &&  $lc[0]->local_it != 1) {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'paternity'  &&  $lc[0]->local_it == 1) {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'bereavement') {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it != 1){
								$schema = ['sup','hr','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}else{
								$schema = ['sup','hr','emp'];
							}
						}
					}elseif ($db[0]->type_request == 'marriage') {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'emmergency'  ) {
						$schema = ['sup','hr','emp'];
					}else{
						$schema = ['sup','hr','emp'];
					}

					$time =  date("h:i:s");
					$date =  date("Y-m-d");
					
					if($schema[0] == "hr"){
						if($js->hr != null){
					 		if($js->app_hr == null){
					 			$pinghr_count = count($js->hr_stat[0]);
					 			for($k= 0; $k < $pinghr_count; $k++){
					 				if(isset($js->hr_stat[0][$k]->$arr)){
					 					if($js->hr_stat[0][$k]->$arr == 'u'){
					 				    	//$js->hr_stat[0][$k]->$arr = 'r';
					 				    }		
					 				    $js->app_hr = [$arr,1];
					 				    $js->hr_date =  [$date];
					 				    $js->hr_time = [$time]; 
					 				    if($end == 'hr'){
					 				    	\DB::SELECT("update att_schedule_request set status_id = 2 where id  = $request_id ");
					 				    }
					 				}
					 			}
					 			
					 		}else{
								if($js->sup != null and $js->sup[0] ==  $arr){
					 				if($js->app_sup == null ){
							 			$js->app_sup = [$arr,1];
							 			$js->sup_date =  [$date];
					 				    $js->sup_time = [$time];
					 				    if($end == 'sup'){
					 				    	\DB::SELECT("update att_schedule_request set status_id = 2  where id  = $request_id ");
					 				    }
								 	}else{
							 			if($js->emp != null){
								 			if($js->app_emp  == null ){
									 			if(isset($js->emp_stat[0]->$arr) and $js->emp_stat[0]->$arr == 'u'){
									 				$js->app_emp = [$arr,1];
									 				$js->emp_date =  [$date];
					 				    			$js->emp_time = [$time];
									 			}

									 			if($end == 'emp'){
					 				    			\DB::SELECT("update att_schedule_request set status_id = 2  where id  = $request_id ");
					 				    		}
									 		}
									 	}
								 	}
					 			}else{
					 				if($js->emp != null and $js->emp[0] ==  $arr){
						 				if($js->app_emp == null ){
								 			$js->app_emp = [$arr,1];
								 			$js->emp_date =  [$date];
					 				   	    $js->emp_time = [$time];
					 				   	    if($end == 'emp'){
					 				    		\DB::SELECT("update att_schedule_request set status_id = 2  where id  = $request_id ");
					 				    	}
								 		}	
					 				}
					 			}
					 		}
			 			}
					}

					if($schema[0] == "sup"){
						return $end;
						if($js->sup != null and $js->sup[0] ==  $arr){
					 		if($js->app_sup == null ){
							 	$js->app_sup = [$arr,1];
							 	$js->sup_date =  [$date];
					 			$js->sup_time = [$time];
					 			if($end == 'sup'){

					 				\DB::SELECT("update att_schedule_request set status_id = 2  where id  = $request_id ");
					 			}
							}
						}else{
							if($js->hr != null){
						 		if($js->app_hr == null){
						 			$pinghr_count = count($js->hr_stat[0]);
						 			for($k= 0; $k < $pinghr_count; $k++){
						 				if(isset($js->hr_stat[0][$k]->$arr)){
						 					if($js->hr_stat[0][$k]->$arr == 'u'){
						 				    	//$js->hr_stat[0][$k]->$arr = 'r';
						 				    }

						 				    $js->app_hr = [$arr,1];
						 				    $js->hr_date =  [$date];
					 				    	$js->hr_time = [$time];
					 				    	if($end == 'hr'){
					 				    		\DB::SELECT("update att_schedule_request set status_id = 2  where id  = $request_id ");
					 				    	}
						 				}
						 			}
						 			
						 		}
							}else{
								if($js->emp != null and $js->emp[0] ==  $arr){
						 			if($js->app_emp == null ){
								 		$js->app_emp = [$arr,1];
								 		$js->emp_date =  [$date];
					 				    $js->emp_time = [$time];
					 				    if($end == 'emp'){
					 				    	\DB::SELECT("update att_schedule_request set status_id = 2  where id  = $request_id ");
					 				    }
								 	}	
					 			}
							}
						}
					}

					if($schema[0] == "emp"){
						if($js->emp != null and $js->emp[0] ==  $arr){
					 		if($js->app_emp == null ){
							 	$js->app_emp = [$arr,1];
							 	$js->emp_date =  [$date];
					 			$js->emp_time = [$time];
					 			if($end == 'emp'){
					 				\DB::SELECT("update att_schedule_request set status_id = 2  where id  = $request_id ");
					 			}
							}	
				 		}else{
				 			if($js->sup != null and $js->sup[0] ==  $arr){
					 			if($js->app_sup == null ){
							 		$js->app_sup = [$arr,1];
							 		$js->sup_date =  [$date];
					 				$js->sup_time = [$time];
					 				if($end == 'sup'){
					 				    \DB::SELECT("update att_schedule_request set status_id = 2  where id  = $request_id ");
					 				}
								}
							}else{
								if($js->app_hr == null){
						 			$pinghr_count = count($js->hr_stat[0]);
						 			for($k= 0; $k < $pinghr_count; $k++){
						 				if(isset($js->hr_stat[0][$k]->$arr)){
						 					if($js->hr_stat[0][$k]->$arr == 'u'){
						 				    	//$js->hr_stat[0][$k]->$arr = 'r';
						 				    }

						 				    $js->app_hr = [$arr,1];
						 				    $js->hr_date =  [$date];
					 				   		$js->hr_time = [$time];
					 				   		if($end == 'hr'){
					 				    		\DB::SELECT("update att_schedule_request set status_id = 2  where id  = $request_id ");
					 				    	}
						 				}
						 			}	
						 		}
							}
				 		}
					}

					$js =  json_encode($js);
					
					$update  = \DB::SELECT("update pool_request set json_data = '$js' where id_req  = $i[id] ");
					$data = [];
					$message = 'success';
					$status = 200;

					return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access[3] ],'data'=>$data],$status);
					}
				}	
				
			}
			
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access[3] ],'data'=>$data],$status);
		}
		
	}

	function  cancel($id){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
				$i = \Input::all();
		
				$emp = explode('-',base64_decode(\Input::get('key')));
				$arr = $emp[1];

			if(isset($i['id']) && $i['id'] !=  null){
				$db  = \DB::SELECT("select * from pool_request where id_req  = $i[id] ");
				$request_id  =  $db[0]->id_req;
				if($db !=  null){
					$request = [];
					$js =  json_decode($db[0]->json_data);
						//foreach ($js as $max => $max_val) {
					$emp  =  $db[0]->employee_id;
					$lc = \DB::SELECT("select local_it from emp where employee_id = '$emp'");
					if($db[0]->type_request == 'time'  ){
						$schema = ['hr','sup','emp']; 
						$end = 'hr'; 
					}elseif ($db[0]->type_request == 'swap'  ) {
						$end  = 'sup';
						$schema = ['emp','sup','hr'];
					}elseif ($db[0]->type_request == 'time' ) {
						$schema = ['sup','hr','emp'];
						$end = 'sup';
					}elseif ($db[0]->type_request == 'over'  ) {
						$schema = ['sup','hr','emp'];
						$end = 'sup';
					}elseif ($db[0]->type_request == 'under'  ) {
						$schema = ['sup','hr','emp'];
						$end = 'sup';
					}elseif ($db[0]->type_request == 'late'  ) {
						$schema = ['sup','hr','emp'];
						$end  =  'hr';
					}elseif ($db[0]->type_request == 'early'  ) {
						$schema = ['sup','hr','emp'];
						$end  = 'hr';
					}elseif ($db[0]->type_request == 'train'){
						$end = 'sup';
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'ob'){
						$schema = ['sup','hr','emp'];
						$end  =  'sup';
					}elseif ($db[0]->type_request == 'change'){
						$emp = \DB::SELECT("select * from emp_supervisor where supervisor =  '$emp' ");
						if($emp != null){
							$end = 'emp';
							$schema = ['emp','hr','sup'];
						}else{
							$end  =  'sup';
							$schema = ['sup','emp','hr'];
						}
					}elseif ($db[0]->type_request == 'vacation'  &&  $lc[0]->local_it != 1){
						$schema = ['hr','sup','hr'];
					}elseif ($db[0]->type_request == 'ADO'  &&  $lc[0]->local_it == 1){
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'vacation'  &&  $lc[0]->local_it == 1){
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'enhance'  &&  $lc[0]->local_it == 1){
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'birth'  &&  $lc[0]->local_it != 1){
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'sick' ) {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}else{
								$schema = ['hr','sup','emp'];
							}
						}		
					}elseif ($db[0]->type_request == 'maternity' ) {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it != 1){
								$schema = ['hr','sup','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['hr','sup','emp'];
							}
						}
					}elseif ($db[0]->type_request == 'paternity'  &&  $lc[0]->local_it != 1) {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'paternity'  &&  $lc[0]->local_it == 1) {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'bereavement') {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it != 1){
								$schema = ['sup','hr','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}else{
								$schema = ['sup','hr','emp'];
							}
						}
					}elseif ($db[0]->type_request == 'marriage') {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'emmergency'  ) {
						$schema = ['sup','hr','emp'];
					}else{
						$schema = ['sup','hr','emp'];
					}

					$time =  date("h:i:s");
					$date =  date("Y-m-d");
					
					if($schema[0] == "hr"){
						$js->app_hr = [$arr,3];
						$js->hr_date = [$date];
					 	$js->hr_time = [$time];
					 	if($end == 'hr'){
					 		\DB::SELECT("update att_schedule_request set status_id = 4  where id  = $request_id ");
					 	}
					}

					if($schema[0] == "sup"){
						$js->app_sup = [$arr,3];
						$js->sup_date = [$date];
					 	$js->sup_time = [$time];
					 	if($end == 'sup'){
					 		\DB::SELECT("update att_schedule_request set status_id = 1  where id  = $request_id ");
					 	}
					}

					if($schema[0] == "emp"){
						$js->app_emp = [$arr,3];
						$js->emp_date = [$date];
					 	$js->emp_time = [$time];
						if($end == 'emp'){
					 		\DB::SELECT("update att_schedule_request set status_id = 1  where id  = $request_id ");
					 	}
					}	



					$js =  json_encode($js);
					$update  = \DB::SELECT("update pool_request set json_data = '$js' where id_req  = $i[id] ");
					$data = [];
					$message = 'success';
					$status = 200;

					return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access[3] ],'data'=>$data],$status);
					}
				}	
				
			}
			
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access[3] ],'data'=>$data],$status);
		}
	}



	function  reject($id){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
				$i = \Input::all();
		
				$emp = explode('-',base64_decode(\Input::get('key')));
				$arr = $emp[1];

			if(isset($i['id']) && $i['id'] !=  null){
				$db  = \DB::SELECT("select * from pool_request where id_req  = $i[id] ");
				$request_id  =  $db[0]->id_req;
				if($db !=  null){
					$request = [];
					$js =  json_decode($db[0]->json_data);
						//foreach ($js as $max => $max_val) {
					$emp  =  $db[0]->employee_id;
					$lc = \DB::SELECT("select local_it from emp where employee_id = '$emp'");
					if($db[0]->type_request == 'time' ){
						$schema = ['hr','sup','emp']; 
						$end = 'hr'; 
					}elseif ($db[0]->type_request == 'swap' ) {
						$end  = 'sup';
						$schema = ['emp','sup','hr'];
					}elseif ($db[0]->type_request == 'time' ) {
						$schema = ['sup','hr','emp'];
						$end = 'sup';
					}elseif ($db[0]->type_request == 'over' ) {
						$schema = ['sup','hr','emp'];
						$end = 'sup';
					}elseif ($db[0]->type_request == 'under' ) {
						$schema = ['sup','hr','emp'];
						$end = 'sup';
					}elseif ($db[0]->type_request == 'late' ) {
						$schema = ['sup','hr','emp'];
						$end  =  'hr';
					}elseif ($db[0]->type_request == 'early' ) {
						$schema = ['sup','hr','emp'];
						$end  = 'hr';
					}elseif ($db[0]->type_request == 'train'){
						$end = 'sup';
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'ob'){
						$schema = ['sup','hr','emp'];
						$end  =  'sup';
					}elseif ($db[0]->type_request == 'change'){
						$emp = \DB::SELECT("select * from emp_supervisor where supervisor =  '$emp' ");
						if($emp != null){
							$end = 'emp';
							$schema = ['emp','hr','sup'];
						}else{
							$end  =  'sup';
							$schema = ['sup','emp','hr'];
						}
					}elseif ($db[0]->type_request == 'vacation'  &&  $lc[0]->local_it != 1){
						$schema = ['hr','sup','hr'];
					}elseif ($db[0]->type_request == 'ADO'  &&  $lc[0]->local_it == 1){
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'vacation'  &&  $lc[0]->local_it == 1){
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'enhance'  &&  $lc[0]->local_it == 1){
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'birth'  &&  $lc[0]->local_it != 1){
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'sick' ) {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}else{
								$schema = ['hr','sup','emp'];
							}
						}		
					}elseif ($db[0]->type_request == 'maternity' ) {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it != 1){
								$schema = ['hr','sup','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['hr','sup','emp'];
							}
						}
					}elseif ($db[0]->type_request == 'paternity'  &&  $lc[0]->local_it != 1) {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'paternity'  &&  $lc[0]->local_it == 1) {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'bereavement') {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it != 1){
								$schema = ['sup','hr','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}else{
								$schema = ['sup','hr','emp'];
							}
						}
					}elseif ($db[0]->type_request == 'marriage') {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'emmergency'  ) {
						$schema = ['sup','hr','emp'];
					}else{
						$schema = ['sup','hr','emp'];
					}

					$time =  date("h:i:s");
					$date =  date("Y-m-d");
					
					if($schema[0] == "hr"){
						if($js->hr != null){
					 		if($js->app_hr == null){
					 			$pinghr_count = count($js->hr_stat[0]);
					 			for($k= 0; $k < $pinghr_count; $k++){
					 				if(isset($js->hr_stat[0][$k]->$arr)){
					 					if($js->hr_stat[0][$k]->$arr == 'u'){
					 				    	//$js->hr_stat[0][$k]->$arr = 'r';
					 				    }		
					 				    $js->app_hr = [$arr,2];
					 				    $js->hr_date =  [$date];
					 				    $js->hr_time = [$time]; 

					 				    if($end == 'hr'){
					 				    	\DB::SELECT("update att_schedule_request set status_id = 3  where id  = $request_id ");
					 				    }

					 				}
					 			}
					 			
					 		}else{
								if($js->sup != null and $js->sup[0] ==  $arr){
					 				if($js->app_sup == null ){
							 			$js->app_sup = [$arr,2];
							 			$js->sup_date =  [$date];
					 				    $js->sup_time = [$time];
					 				    if($end == 'sup'){
					 				    	\DB::SELECT("update att_schedule_request set status_id = 3  where id  = $request_id ");
					 				    }
								 	}else{
							 			if($js->emp != null){
								 			if($js->app_emp  == null ){
									 			if(isset($js->emp_stat[0]->$arr) and $js->emp_stat[0]->$arr == 'u'){
									 				$js->app_emp = [$arr,2];
									 				$js->emp_date =  [$date];
					 				    			$js->emp_time = [$time];
					 				    			if($end == 'emp'){
								 				    	\DB::SELECT("update att_schedule_request set status_id = 3  where id  = $request_id ");
								 				    }
									 			}
									 		}
									 	}
								 	}
					 			}else{
					 				if($js->emp != null and $js->emp[0] ==  $arr){
						 				if($js->app_emp == null ){
								 			$js->app_emp = [$arr,2];
								 			$js->emp_date =  [$date];
					 				   	    $js->emp_time = [$time];
					 				   	    if($end == 'emp'){
					 				    		\DB::SELECT("update att_schedule_request set status_id = 3  where id  = $request_id ");
					 				    	}	
								 		}	
					 				}
					 			}
					 		}
			 			}
					}

					if($schema[0] == "sup"){
						if($js->sup != null and $js->sup[0] ==  $arr){
					 		if($js->app_sup == null ){
							 	$js->app_sup = [$arr,2];
							 	$js->sup_date =  [$date];
					 			$js->sup_time = [$time];
					 			if($end == 'sup'){
					 				\DB::SELECT("update att_schedule_request set status_id = 3  where id  = $request_id ");
					 			}
							}
						}else{
							if($js->hr != null){
						 		if($js->app_hr == null){
						 			$pinghr_count = count($js->hr_stat[0]);
						 			for($k= 0; $k < $pinghr_count; $k++){
						 				if(isset($js->hr_stat[0][$k]->$arr)){
						 					if($js->hr_stat[0][$k]->$arr == 'u'){
						 				    	//$js->hr_stat[0][$k]->$arr = 'r';
						 				    }

						 				    $js->app_hr = [$arr,2];
						 				    $js->hr_date =  [$date];
					 				    	$js->hr_time = [$time];
					 				    	if($end == 'hr'){
					 				    		\DB::SELECT("update att_schedule_request set status_id = 3  where id  = $request_id ");
					 				    	}
						 				}
						 			}
						 			
						 		}
							}else{
								if($js->emp != null and $js->emp[0] ==  $arr){
						 			if($js->app_emp == null ){
								 		$js->app_emp = [$arr,2];
								 		$js->emp_date =  [$date];
					 				    $js->emp_time = [$time];
					 				    if($end == 'emp'){
					 				    	\DB::SELECT("update att_schedule_request set status_id = 3  where id  = $request_id ");
					 				    }
								 	}	
					 			}
							}
						}
					}

					if($schema[0] == "emp"){
						if($js->emp != null and $js->emp[0] ==  $arr){
					 		if($js->app_emp == null ){
							 	$js->app_emp = [$arr,2];
							 	$js->emp_date =  [$date];
					 			$js->emp_time = [$time];
					 			if($end == 'emp'){
					 				\DB::SELECT("update att_schedule_request set status_id = 3  where id  = $request_id ");
					 			}
							}	
				 		}else{
				 			if($js->sup != null and $js->sup[0] ==  $arr){
					 			if($js->app_sup == null ){
							 		$js->app_sup = [$arr,2];
							 		$js->sup_date =  [$date];
					 				$js->sup_time = [$time];
					 				if($end == 'sup'){
								 		\DB::SELECT("update att_schedule_request set status_id = 3  where id  = $request_id ");
								 	}
								}
							}else{
								if($js->app_hr == null){
						 			$pinghr_count = count($js->hr_stat[0]);
						 			for($k= 0; $k < $pinghr_count; $k++){
						 				if(isset($js->hr_stat[0][$k]->$arr)){
						 					if($js->hr_stat[0][$k]->$arr == 'u'){
						 				    	//$js->hr_stat[0][$k]->$arr = 'r';
						 				    }

						 				    $js->app_hr = [$arr,2];
						 				    $js->hr_date =  [$date];
					 				   		$js->hr_time = [$time];
					 				   		if($end == 'hr'){
								 				\DB::SELECT("update att_schedule_request set status_id = 3  where id  = $request_id ");
								 			}
						 				}
						 			}	
						 		}
							}
				 		}
					}


					$js =  json_encode($js);
					
					$update  = \DB::SELECT("update pool_request set json_data = '$js' where id_req  = $i[id] ");
					$data = [];
					$message = 'success';
					$status = 200;

					return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access[3] ],'data'=>$data],$status);
					}
				}	
				
			}
			
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access[3] ],'data'=>$data],$status);
		}
	}
	public function insert_data(){
		$input = \Input::get('data');
		if(!isset($input)){
			$i = \Input::get('all');
			$filename = null;
			$id = $i['id'];
			$comment = $i['comment'];
			($comment == null ? $comment = null : $comment = $comment);

		}else{
			$input = json_decode($input,1);
			$comment = $input['data']['comment'];
			$comment = $input['data']['id'];
			($comment == null ? $comment = null : $comment = $comment);
			$file = \Input::file('file');
			$ext = $file->getClientOriginalExtension();
			$rename = "Overtime_req_".str_random(6).$ext;
			$path = "hrms_upload/overtime_request";
			$filename = $rename;
			$path = $path;
		}

		if($filename == null && $path != null ){
			$db = \DB::SELECT("update att_overtime set filename='$filename', comment='$comment', path='$path' where id  ");
		}else{

		}
	}



	// UPDATE (APPROVE/REJECT) SCHEDULE REQUEST
	public function update()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update');
		if($access[1] == 200){
			$employeeID = \Input::json('employee_id');
			$availmentDate = \Input::json('availment_date');
			$status = \Input::json('status_id');
			$data = $this->updateRequest($employeeID, $availmentDate, $status);
			return $data;
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}

	// THROW UPDATE SCHEDULE REQUEST BY REQUEST TO MODEL
	public function updateRequest($i)
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update');
		if($access[1] == 200){
			$model = new RequestDetail_Model;
			$type = strtolower($i['type']);

			if (strpos($type,'overtime') !== false){
				$data = $model->updateOvertime($i);
			}
			else if (strpos($type,'undertime') !== false){
				$data = $model->updateUndertime($i);
			}
			else if (strpos($type,'training') !== false){
				$data = $model->updateTraining($i);
			}
			else if (strpos($type,'change') !== false){
				$data = $model->updateChange($i);
			}
			else if (strpos($type,'swap') !== false){
				$data = $model->updateSwap($i);
			}
			else if (strpos($type,'time in') !== false){
				$data = $model->updateTimein($i);
			}
			else if (strpos($type,'late') !== false){
				$data = $model->updateLate($i);
			}
			else{
				return \Response::json(['header'=>['message'=>'Can not update request','status'=>500],'data'=>null],500);
			}
			return \Response::json(['header'=>['message'=>$data['message'],'status'=>$data['status']],'data'=>$data['data']],$data['status']);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	public function index2(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		$model = new RequestDetail_Model;
		if($access[1] == 200){
			$name = \Input::get('name');
			$rule = [
				'name' => 'required | Regex:/^[A-Za-z0-9 ]+$/',
			];
			$validator = \Validator::make(\Input::all(),$rule);

			if($validator->fails()){
				$val = $validator->errors()->all();
				return response()->json(['header' => ['message' => $val[0], 'status' => 500 ], 'data' => null], 500 );
			}else{
				// throw user input to model to get attendance record
				return $getRecordDetail = $model->getDetail2($name);
				return response()->json(['header' => ['message' => $getRecordDetail['message'], 'status' => $getRecordDetail['status'], 'access'=>$access[3] ], 'data' => $getRecordDetail['data']], $getRecordDetail['status']);
			}
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function save_request($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update');
		$model = new RequestDetail_Model;

		$key = \Input::get("key");
		$explode = explode("-",base64_decode($key));
		$empID = $explode[1];
		$json = \Input::get('data');

		$json = \Input::get('data');
		$json = json_decode($json,1);
		
		if($json['data']['employee_id'] == $empID){
			$access[1] = 200;	
		}
		
		if($access[1] == 200){
			
			if(isset($json)){
				//$json = json_decode($json,1);
				$type_id = $json['data']['type_id'];
				if(isset($json['data']['newcomment'])){
					$comment = $json['data']['newcomment'];
				}else{
					$comment = null;
				}
			}else{
				$comment = \Input::get("newcomment");
				if(!$comment){
					$comment = null;
				}
				$type_id = \Input::get("type_id");
			}

			if(!isset($comment) ){
				$comment = null;
			}
			if(!isset($type_id) || $type_id == null){
					return response()->json(["header" => ["message" => "Failed , type id not found", "status" => 500],"data" =>[] ],500);
			}

			if($type_id == "Training" ){ $type_id = 1;}
			if($type_id == "Official Business" ){ $type_id = 2;}
			if($type_id == "Overtime" ){ $type_id = 3;}
			if($type_id == "Undertime" ){ $type_id = 4;}
			if($type_id == "Time-In / Time-Out" ){ $type_id = 9;}
			if($type_id == "Change Shift" ){ $type_id = 5;}
			if($type_id == "Swap Shift / Day Off" ){ $type_id = 6;}
			if($type_id == "Late" ){ $type_id = 7;}
			if($type_id == "Early Out" ){ $type_id = 8;}
			//$input = \Input::json("data");
			$file = \Input::file("file");
			if(isset($file) && $file != "undefined"){
					$imageName = $file->getClientOriginalName();
					$ext = $file->getClientOriginalExtension();
					$size = $file->getSize();

					
					$path = "hrms_upload/schedule";
					$rename = "Reply_".str_random(6).".".$ext;

					$file->move(storage_path($path),$rename);
					$db = \DB::SELECT("insert into command_center(request_id,type_id,comment,employee_id,path,filename,master_type) value($id,$type_id,'$comment','$empID','$path','$rename',1) ");
					$feed = \DB::SELECT("select command_center.filename, command_center.comment,  concat(emp.first_name,' ',middle_name,' ',last_name)  as employeeName ,  command_center.created_at, command_center.id , command_center.path    from command_center,emp  where request_id=$id  and command_center.type_id = $type_id and command_center.employee_id  =  emp.employee_id order by id desc limit 1");

					
			}else{
				$path = "NULL";
				$rename = "NULL";

				$db = \DB::SELECT("insert into command_center(request_id,type_id,comment,employee_id,path,filename,master_type) value($id,$type_id,'$comment','$empID','$path','$rename',1) ");
				$feed = \DB::SELECT("select command_center.filename, command_center.comment,  concat(emp.first_name,' ',middle_name,' ',last_name)  as employeeName ,  command_center.created_at, command_center.id , command_center.path    from command_center,emp  where request_id=$id and command_center.type_id = $type_id and command_center.employee_id  =  emp.employee_id order by id desc limit 1");
				
			}

			if(isset($json)){
				array_push($json['data']['comment'], $feed[0]);
				for ($i=0; $i < count($json['data']['comment']); $i++) { 
					if(isset($json['data']['comment'][$i]->filename)){
						if(strtolower($json['data']['comment'][$i]->filename) == 'null'){
							$json['data']['comment'][$i]->path = null;	
							$json['data']['comment'][$i]->filename = null;
						}
					}
				}
				$callback = $json['data'];
			}else{
				$a = \Request::all();
				unset($a['key']);
				array_push($a['comment'], $feed[0]);
				for ($i=0; $i < count($a['comment']); $i++) { 
					if(isset($a['comment'][$i]->filename)){
						if(strtolower($a['comment'][$i]->filename) == 'null'){
							$a['comment'][$i]->path = null;	
							$a['comment'][$i]->filename = null;
						}
					}
				}
				$callback = $a;
			}

			// SEND EMAIL
			$employee_id  =  \Input::get('employee_id');
			$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");

			if(count($set_data) > 0){
				if(strlen($set_data[0]->work_email) < 2){
					$set_data[0]->work_email = null;
				}
				if(strlen($set_data[0]->personal_email) < 2){
					$set_data[0]->personal_email = null;
				}
				//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_email :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

				$namex  			=  \Input::get('employee');
				$request 			=  \Input::get('type');
				$personal_email1   	=  $set_data[0]->personal_email;
				$work_email1  	 	=  $set_data[0]->work_email;
				$comment 			= \Input::get('newcomment');
				$fullname_createdby = \Input::get('requestBy');
				$status_name 		= \Input::get('status');
				$availments 	= \Input::get('availment_date');
				$requestss 			= \Input::get('type');

				// if($email != null){
				// 	$data = array('name'=>$namex,"request" => $request, 'email' =>  $email);
				// 	\Mail::send('emails.approve', $data, function($message) use ($data){
				// 		$request 	=  \Input::get('type');
				// 		$subject 	= "Leekie request reminder : ($request)";
				// 		$message->to($data['email'])->subject($subject);
				// 	});
				// }


				$datas = array('name'=>$namex,"request" => $requestss, 'email1' => $work_email1, 'email2'=>$personal_email1,'created_by' => $fullname_createdby, 'status'=> 'status_name','availments'=>$availments);
				$template_email = 'emails.chat_email';
				if(isset($comment)){
					$datas['comment'] = $comment;
					$r_emp = \DB::SELECT(\DB::raw("select concat(first_name,' ',ifnull(middle_name,''),' ',ifnull(last_name,'')) as login_name from emp where employee_id='$empID' limit 1") );
					$login_name = $r_emp[0]->login_name;
					$datas['login_name'] = $login_name;
					//$datas['comment'] = "<table><tr><td>Name</td><td>Comment</td></tr><tr><td>$login_name</td><td>$comment</td></tr></table>";
					$template_email = 'emails.chat_email';
				}
				try {
					if($datas['email1']){
						\Mail::send($template_email, $datas, function($message) use ($datas){
							$req = $datas['request'];
							$message->to($datas['email1'])->subject("Leekie request reminder $req");
						});
					}
					if($datas['email2']){
						\Mail::send($template_email, $datas, function($message) use ($datas){
							$req = $datas['request'];
							$message->to($datas['email2'])->subject("Leekie request reminder $req");
						});
					}
					
				} catch (Exception $e) {
					return [$e->getMessage()];
				}
			}
			return response()->json(["header" => ["message" => "Success, send data", "status" => 200, "access" => $access[3]],"data" =>$callback ],200);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	// 04042019
	// public function save_request($id){
	// 	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
	// 	$model = new RequestDetail_Model;
	// 	if($access[1] == 200){
	// 		$key = \Input::get("key");
	// 		$explode = explode("-",base64_decode($key));
	// 		$empID = $explode[1];
	// 		$json = \Input::get('data');
	// 		if(isset($json)){
	// 			$json = json_decode($json,1);
	// 			$type_id = $json['data']['type_id'];
	// 			$comment = $json['data']['newcomment'];
	// 		}else{
	// 			$comment = \Input::get("newcomment");
	// 			$type_id = \Input::get("type_id");
	// 		}

	// 		if(!isset($comment) ){
	// 			$comment = null;
	// 		}
	// 		if(!isset($type_id) || $type_id == null){
	// 				return response()->json(["header" => ["message" => "Failed , type id not found", "status" => 500],"data" =>[] ],500);
	// 		}

	// 		if($type_id == "Training" ){ $type_id = 1;}
	// 		if($type_id == "Official Business" ){ $type_id = 2;}
	// 		if($type_id == "Overtime" ){ $type_id = 3;}
	// 		if($type_id == "Undertime" ){ $type_id = 4;}
	// 		if($type_id == "Time-In / Time-Out" ){ $type_id = 9;}
	// 		if($type_id == "Change Shift" ){ $type_id = 5;}
	// 		if($type_id == "Swap Shift / Day Off" ){ $type_id = 6;}
	// 		if($type_id == "Late" ){ $type_id = 7;}
	// 		if($type_id == "Early Out" ){ $type_id = 8;}
	// 		//$input = \Input::json("data");
	// 		$file = \Input::file("file");
	// 		if(isset($file) && $file != "undefined"){
	// 				$imageName = $file->getClientOriginalName();
	// 				$ext = $file->getClientOriginalExtension();
	// 				$size = $file->getSize();

					
	// 				$path = "hrms_upload/schedule";
	// 				$rename = "Reply_".str_random(6).$ext;

	// 				$file->move(storage_path($path),$rename);
	// 				$db = \DB::SELECT("insert into command_center(request_id,type_id,comment,employee_id,path,filename) value($id,$type_id,'$comment','$empID','$path','$rename') ");
	// 				$feed = \DB::SELECT("select command_center.filename, command_center.comment,  concat(emp.first_name,' ',middle_name,' ',last_name)  as employeeName ,  command_center.created_at, command_center.id , command_center.path    from command_center,emp  where request_id=$id and command_center.employee_id  =  emp.employee_id order by id desc limit 1");

					
	// 		}else{
	// 			$path = "NULL";
	// 			$rename = "NULL";

	// 			$db = \DB::SELECT("insert into command_center(request_id,type_id,comment,employee_id,path,filename) value($id,$type_id,'$comment','$empID','$path','$rename') ");
	// 			$feed = \DB::SELECT("select command_center.filename, command_center.comment,  concat(emp.first_name,' ',middle_name,' ',last_name)  as employeeName ,  command_center.created_at, command_center.id , command_center.path    from command_center,emp  where request_id=$id and command_center.employee_id  =  emp.employee_id order by id desc limit 1");
				
	// 		}

	// 		// SEND EMAIL
	// 		$employee_id  =  \Input::get('employee_id');
	// 		$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");

	// 		if(count($set_data) > 0){
	// 			if(strlen($set_data[0]->work_email) < 2){
	// 				$set_data[0]->work_email = null;
	// 			}
	// 			if(strlen($set_data[0]->personal_email) < 2){
	// 				$set_data[0]->personal_email = null;
	// 			}
	// 			//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_email :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

	// 			$namex  			=  \Input::get('employee');
	// 			$request 			=  \Input::get('type');
	// 			$personal_email1   	=  $set_data[0]->personal_email;
	// 			$work_email1  	 	=  $set_data[0]->work_email;
	// 			$comment 			= \Input::get('newcomment');
	// 			$fullname_createdby = \Input::get('requestBy');
	// 			$status_name 		= \Input::get('status');
	// 			$availments 	= \Input::get('availment_date');
	// 			$requestss 			= \Input::get('type');

	// 			// if($email != null){
	// 			// 	$data = array('name'=>$namex,"request" => $request, 'email' =>  $email);
	// 			// 	\Mail::send('emails.approve', $data, function($message) use ($data){
	// 			// 		$request 	=  \Input::get('type');
	// 			// 		$subject 	= "Leekie request reminder : ($request)";
	// 			// 		$message->to($data['email'])->subject($subject);
	// 			// 	});
	// 			// }

	// 			$datas = array('name'=>$namex,"request" => $requestss, 'email1' => $work_email1, 'email2'=>$personal_email1,'created_by' => $fullname_createdby, 'status'=> 'status_name','availments'=>$availments);
	// 			$template_email = 'emails.approve';
	// 			if(isset($comment)){
	// 				$datas['comment'] = $comment;
	// 				$template_email = 'emails.sendcomment';
	// 			}
	// 			try {
	// 				if($datas['email1']){
	// 					\Mail::send($template_email, $datas, function($message) use ($datas){
	// 						$req = $datas['request'];
	// 						$message->to($datas['email1'])->subject("Leekie request reminder $req");
	// 					});
	// 				}
	// 				if($datas['email2']){
	// 					\Mail::send($template_email, $datas, function($message) use ($datas){
	// 						$req = $datas['request'];
	// 						$message->to($datas['email2'])->subject("Leekie request reminder $req");
	// 					});
	// 				}
					
	// 			} catch (Exception $e) {
	// 				return [$e->getMessage()];
	// 			}
	// 		}
	// 		return response()->json(["header" => ["message" => "Success, send data", "status" => 200, "access" => $access[3]],"data" =>"OK" ],200);
	// 	}
	// 	else{
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 	}
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	// }


	// public function save_request($id){
	// 	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
	// 	$model = new RequestDetail_Model;
	// 	if($access[1] == 200){
	// 		$key = \Input::get("key");
	// 		$explode = explode("-",base64_decode($key));
	// 		$empID = $explode[1];
	// 		$json = \Input::get('data');
	// 		if(isset($json)){
	// 			$json = json_decode($json,1);
	// 			$type_id = $json['data']['type_id'];
	// 			$comment = $json['data']['newcomment'];
	// 		}else{
	// 			$comment = \Input::get("newcomment");
	// 			$type_id = \Input::get("type_id");
	// 		}

	// 		if(!isset($comment) ){
	// 			$comment = null;
	// 		}
	// 		if(!isset($type_id) || $type_id == null){
	// 				return response()->json(["header" => ["message" => "Failed , type id not found", "status" => 500],"data" =>[] ],500);
	// 		}

	// 		if($type_id == "Training" ){ $type_id = 1;}
	// 		if($type_id == "Official Business" ){ $type_id = 2;}
	// 		if($type_id == "Overtime" ){ $type_id = 3;}
	// 		if($type_id == "Undertime" ){ $type_id = 4;}
	// 		if($type_id == "Time-In / Time-Out" ){ $type_id = 9;}
	// 		if($type_id == "Change Shift" ){ $type_id = 5;}
	// 		if($type_id == "Swap Shift / Day Off" ){ $type_id = 6;}
	// 		if($type_id == "Late" ){ $type_id = 7;}
	// 		if($type_id == "Early Out" ){ $type_id = 8;}
	// 		//$input = \Input::json("data");
	// 		$file = \Input::file("file");
	// 		if(isset($file) && $file != "undefined"){
	// 				$imageName = $file->getClientOriginalName();
	// 				$ext = $file->getClientOriginalExtension();
	// 				$size = $file->getSize();

					
	// 				$path = "hrms_upload/schedule";
	// 				$rename = "Reply_".str_random(6).$ext;

	// 				$file->move(storage_path($path),$rename);
	// 				$db = \DB::SELECT("insert into command_center(request_id,type_id,comment,employee_id,path,filename) value($id,$type_id,'$comment','$empID','$path','$rename') ");
	// 				$feed = \DB::SELECT("select command_center.filename, command_center.comment,  concat(emp.first_name,' ',middle_name,' ',last_name)  as employeeName ,  command_center.created_at, command_center.id , command_center.path    from command_center,emp  where request_id=$id and command_center.employee_id  =  emp.employee_id order by id desc limit 1");

					
	// 		}else{
	// 			$path = "NULL";
	// 			$rename = "NULL";

	// 			$db = \DB::SELECT("insert into command_center(request_id,type_id,comment,employee_id,path,filename) value($id,$type_id,'$comment','$empID','$path','$rename') ");
	// 			$feed = \DB::SELECT("select command_center.filename, command_center.comment,  concat(emp.first_name,' ',middle_name,' ',last_name)  as employeeName ,  command_center.created_at, command_center.id , command_center.path    from command_center,emp  where request_id=$id and command_center.employee_id  =  emp.employee_id order by id desc limit 1");
				
	// 		}

	// 		// SEND EMAIL
	// 		$employee_id  =  \Input::get('employee_id');
	// 		$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");

	// 		$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

	// 		$namex  	=  \Input::get('employee_id');
	// 		$request 	=  \Input::get('type');
	// 		$email   	=  $set_data[0]->personal_email;
	// 		$comment 	= \Input::get('newcomment');

	// 		if($email != null){
	// 			$data = array('name'=>$namex,"request" => $request, 'email' =>  $email);
	// 			\Mail::send('emails.approve', $data, function($message) use ($data){
	// 				$request 	=  \Input::get('type');
	// 				$subject 	= "Leekie request reminder : ($request)";
	// 				$message->to($data['email'])->subject($subject);
	// 			});
	// 		}
	// 		return response()->json(["header" => ["message" => "Success, send data", "status" => 200, "access" => $access[3]],"data" =>"OK" ],200);
	// 	}
	// 	else{
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 	}
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	// }
}
