<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Model\Attendance\Request\ScheduleRequestList_model as requestList;
use Larasite\Library\FuncAccess;
use League\Csv\Writer;

class ScheduleRequestList_Ctrl extends Controller {
	protected $form = 67;

	// SHOW SCHEDULE REQUEST LIST BY 1 MONTH BEFORE CURRENT MONTH UNTIL 1 YEAR NEXT (DEFAULT)**********************
	public function index()
	{
		// for ($i = 1; $i < 76; $i++){
		// 	DB::select("insert into permissions (role_id, form_id, create_, read_, update_, delete_) values (33, $i, 1, 1, 1, 1)");
		// }
		// return 0;
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
				$model = new requestList;
				// get emplloyee id from from API key
				$decode = base64_decode(\Request::get('key'));
				$empID = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
				// get employee name
				$empName = $model->getName($empID);
				($empName != null ? $empName = $empName[0]->name : $empName = "tes");
				// check employee role
				$checkRole = $model->checkRole($empID);
				
				// if($checkRole == 'Supervisor' ){
				// 	$checkRole = 'human resource';
				// }
				// //$arr = ['superuser','human resource','resource human','hr','admin','admin it','it admin','Supervisor'];
				
				//$explode  =  explode(' ',$checkRole[0]->role_name);				
				
				// if($checkRole ==  ''){
				// 	$checkRole = 'HRD';
				// }else{
				// 	$checkRole = 'USER';
				// }	
				

				$department =  \DB::select("select * from department where id <> 1");//$model->departmentList($empID);				
				$request = $model->requestList();



				$empDepartment = $model->getEmpDepartment($empID);

				$from = date('Y-m-d', strtotime(date('Y-m-d')."-1 month"));
				$to = date('Y-m-d',strtotime($from."+1 year"));
				$temp = [
					'employee_name' => $empName,
					'employee_id' => $empID,
					'employee_role' => $checkRole,
					'emp_department'=>$empDepartment,
					'from' => $from,
					'to' => $to,
					'pending' => "1",
					'department' => $department,
					'requestType' => $request
				];
				return \Response::json(['header'=>['message'=>'Show record data','status'=>'200', 'access'=>$access[3]],'data'=>$temp],200);
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3] ],'data'=>$data],$status);
	}

	function  approve($id){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
				$emp = explode('-',base64_decode(\Input::get('key')));
				$empID = $emp[1];
				$data = \DB::SELECT(" update att_schedule_request set status_id=2 and approver ='$empID' where id=$id ");
				if($data){
					$message = "Success approve data";
					$data = [];
					$status = 200;
				}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3] ],'data'=>$data],$status);
	}

	function  reject($id){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
				$emp = explode('-',base64_decode(\Input::get('key')));
				$empID = $emp[1];
				$data = \DB::SELECT(" update att_schedule_request set status_id=3 and approver ='$empID' where id=$id ");
				if($data){
					$message = "Success approve data";
					$data = [];
					$status = 200;
				}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3] ],'data'=>$data],$status);
	}

	// SEARCH SCHEDULE REQUEST LIST BY CHOOSEN OPTION****************************************************************
	private function cari($value,$arrs,$idx,$stats,$results)
	{	
		$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
		if(isset($arrs['hrx_comp'])){ 
			$arr_idx = array_search($value,$arrs['hrx_comp']);
			if(gettype($arr_idx) == 'integer'){
				$subs = 'hr'; 
			}
		}if(isset($arrs['supx_comp'])){ 
			$arr_idx = array_search($value,$arrs['supx_comp']);
			if(gettype($arr_idx) == 'integer'){
				$subs = 'sup'; 
			}
		}if(isset($arrs['swapx_compx'])){ 
			$arr_idx = array_search($value,$arrs['swapx_compx']);
			if(gettype($arr_idx) == 'integer')
			$subs = 'swap'; 
		}

		
		if(isset($subs)){
			$i2=-1;		
			for ($i1=0; $i1 < count($arrs[$subs]); $i1++) {
				$keysx=array_keys($arrs[$subs][$i1]);
				
				if((string)$keysx[0]==$idx){
					//return $keysx;
					$i2=$i1;
				}else{
					$act = 'not';
				}
			}
			//return [$arrs[$subs][$i2]];			
			//for ($i=0; $i < count($arrs[$subs]); $i++) { 
				//if(isset($arrs[$subs][$i][$value])){
					// if($idx == 264){
					// 	return $arrs[$subs][$i];
					// }
				//return $arrs[$subs];
			//return $i2;
			if($i2==-1){
				//return 121;
				$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
			}else{
					
					try {
						if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){

							if(isset($arrs['req_flow']['employee_requestor'])){
								$requestor_id 	= $arrs['req_flow']['employee_requestor'][0];
								$requestor_jobs = $arrs['req_flow']['employee_requestor'][1];
								if($idx == $requestor_id){
									try {
										if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){
											if($arrs[$subs][$i2][$subs.'_stat'] == 1 && $stats == 2){
												
												$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
											}else{
												
												$act = ["cancel"=>true,"approve"=>false,"reject"=>false];
											}		
										}
									} catch (\Exception $e) {
										
										$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
									}

									
								}else{
									
									try {
										if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){
											if($arrs[$subs][$i2][$subs.'_stat'] == 1){
												$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
											}else{
												$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
											}		
										}
									} catch (\Exception $e) {
										$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
									}
								}
								if($stats != 1){
									$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
								}

							}else{

								try {
									if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){
										if($arrs[$subs][$i2][$subs.'_stat'] == 1){
											$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
										}else{
											$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
										}		
									}
								} catch (\Exception $e) {
									$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
								}
								//return $act;
							}
						}
					} catch (\Exception $e) {
						$act=null;
					}
			}
					// if($arrs[$subs][$i2][$subs.'_stat'] == 1){
					// 	$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
					// }else{
					// 	if($idx == $arr)				
					// }
				//}
			//}
			
		}else{

			if(isset($arrs['req_flow']['employee_requestor'])){
				try{

					if($arrs['requestor_stat'][$idx] == 0 || $arrs['requestor_stat'][$idx] == 1){
						

						if($arrs['req_flow']['employee_approve'] == "x"){
							$act = ["cancel"=>false,"approve"=>false,"reject"=>false];								
						}else{
							$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
						}
						if($stats > 1){
							$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
						}
				
					}else{
						//$act = $arrs['requestor_stat'][$idx];
						// if($stats == 1){
						// 	$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
						// }else{
						// 	$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
						// }
						//$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
					}
				}catch(\Exception $e){

					$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
				}
			}else{	

				try{
					if($arrs['requestor_stat'][$value] == 0 || $arrs['requestor_stat'][$value] == 1){
						$act = ["cancel"=>true,"approve"=>false,"reject"=>false];	
					}else{					
						if($stats == 1){
							$act = ["cancel"=>true,"approve"=>false,"reject"=>false];
						}else{
							$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
						}
					}
					// if($arrs['requestor_stat'][$idx] == 0){
					// }elseif($arrs['requestor_stat'][$idx] == 1){
					// 	$act = ["cancel"=>false,"approve"=>false,"reject"=>false];	
					// }
				}catch(\Exception $e){
					
					$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
				}
			}
		}
		return $act;
	}
	// public function search()
	// {
	// 	$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	// 	if($access[1] == 200){
	// 		$model = new requestList;
	// 		$i = \Input::all();
	// 		$include = \Input::get("include");
	// 		if(isset($include)){
	// 			$include = $include;
	// 		}else{
	// 			$include = 0;
	// 		}
	// 		$dep = \Input::json('department');


	// 		$reqType = \Input::json('requestType');
	// 		$decode = base64_decode(\Request::get('key'));
	// 		$empID = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
	// 		//$empID = \Input::get('employee_name');
	// 		$role = $model->checkRole($empID);

	// 		if (!(isset($i['employee_name']))){
	// 			$i['employee_id'] = "";
	// 		}
	// 		else{
	// 			$i['employee_id'] = $i['employee_name'];
	// 		}

	// 		$empDepartment = $model->getEmpDepartment($empID);
	// 		// show request list after checking role
	// 		$role = $model->checkRole($empID);

	// 		$manipulate = [];
	// 		$temp  = []; 
	// 		$arr  = ['all' =>  0,'canceled'  => 4,'past' => 5,'pending' => 1,'schedule' => 2, 'rejected' => 3];

	// 		foreach ($arr as $key => $value) {
	// 			if(isset($i[$key]) && $i[$key] != 0){
	// 				$temp[] = $value;
	// 			}
	// 		}	
			

	// 		/**
	// 		 * get type
	// 		 */	
			
	// 		$EMP =   $i['employee_id'];
	// 		if(isset($EMP) && $EMP != ""){
	// 			$issetEMP = "emp.employee_id =  '$EMP'";
	// 		}else{
	// 			$issetEMP = "emp.employee_id = att_schedule_request.employee_id";
	// 		}
	// 		$TYP = (isset($i['requestType']) && $i['requestType'] != null ? $i['requestType']['type_id'] : null);
	// 		if($TYP){
	// 			$TYP = "and att_schedule_request.type_id = $TYP";
	// 		}else{
	// 			$TYP = '';
	// 		}
	// 		$DEPART  =  ((isset($i['department'])  && $i['department'] != null) || (isset($i['department'])  && $i['department']['id'] == 0) ? $i['department']['id'] : null);

	// 		if($DEPART){
	// 			$Q_DEPART = "and emp.department = $DEPART";
	// 		}else{
	// 			$Q_DEPART = '';
	// 		}
            
 //          /*return "select emp.employee_id, 
	// 										   concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as employee,
	// 										   att_schedule_request.status_id,
	// 										   att_schedule_request.id,
	// 										   att_type.type,
	// 										   att_schedule_request .type_id,
	// 										   att_schedule_request.approval_date as approval_id,
	// 				                           att_schedule_request.approver_date as approver_id,
	// 										   (select concat(first_name,' ',middle_name,' ',last_name)  from emp where employee_id  =  (select approval_id))  as approval_name,
	// 				            			   (select concat(first_name,' ',middle_name,' ',last_name) from  emp  where employee_id  =  (select approver_id)) as  approver_name,
	// 										   att_schedule_request.id,
	// 										   att_schedule_request.approval,
	// 										   att_schedule_request.approver,
	// 										   att_schedule_request.availment_date,
	// 										   att_status.status,
	// 										   att_schedule_request.update_at,
	// 										   att_schedule_request.date_request
	// 									from 
	// 									att_schedule_request,att_type,emp,att_status where 
	// 									$issetEMP $TYP
	// 									and att_type.type_id = att_schedule_request.type_id 
	// 									and att_status.status_id =  att_schedule_request.status_id
	// 									and emp.employee_id =  att_schedule_request.employee_id ";*/
				
	// 		if(in_array(0,$temp)){
	// 			/*emp.employee_id =  '$EMP' */
	// 			$result  = \DB::SELECT("select emp.employee_id, 
	// 										   concat(emp.first_name,' ',ifnull(emp.middle_name,''),' ',ifnull(emp.last_name,'')) as employee,
	// 										   att_schedule_request.status_id,
	// 										   att_schedule_request.id,
	// 										   att_type.type,
	// 										   att_schedule_request .type_id,
	// 										   att_schedule_request.approval_date as approval_id,
	// 				                           att_schedule_request.approver_date as approver_id,
	// 										   (select concat(first_name,' ',ifnull(middle_name,''),' ',ifnull(last_name,''))  from emp where employee_id  =  (select approval_id))  as approval_name,
	// 				            			   (select concat(first_name,' ',ifnull(middle_name,''),' ',ifnull(last_name,'')) from  emp  where employee_id  =  (select approver_id)) as  approver_name,
	// 										   att_schedule_request.id,
	// 										   att_schedule_request.approval,
	// 										   att_schedule_request.approver,
	// 										   att_schedule_request.availment_date,
	// 										   att_status.status,
	// 										   att_schedule_request.update_at,
	// 										   att_schedule_request.date_request
	// 									from 
	// 									att_schedule_request,att_type,emp,att_status where 
	// 									$issetEMP 
	// 									$Q_DEPART 
	// 									$TYP
	// 									and att_type.type_id = att_schedule_request.type_id 
	// 									and att_status.status_id =  att_schedule_request.status_id
	// 									and emp.employee_id =  att_schedule_request.employee_id ");
         
	// 			if($result  !=  null){	
                   
	// 				foreach ($result as $key => $value) {
				
	// 					$loop_emp  =  $value->employee_id;

	// 					if($DEPART != null){
	// 						$check_department =  \DB::SELECT("select  department from emp  where  employee_id = '$loop_emp' ");
	// 						if($check_department != null){
	// 							if($check_department[0]->department != null){
	// 								if($DEPART != $check_department[0]->department){
                                       
	// 									unset($result[$key]);
	// 								}
	// 							}
	// 						}else{
	// 							return response()->json(['header' => ['message' => 'Search Option fail', 'status' => 500 ],'data' => []],500);
	// 						}
	// 					}
                        
 //                           //return $result[0]->id;
	// 						//if(isset($result[0]->id)){
	// 					//return response()->json(['header' => ['message' => 'Result  : 0', 'status' => 200 ],'data' => []],200);
	// 					//}
                        
                      
	// 					 $id  = $result[$key]->id;
	// 					//print_r("select * from pool_request where employee_id =  '$EMP' and id_req = $id and master_type = 1 ");
	// 					 $pool  =  \DB::SELECT("select * from pool_request where employee_id =  '$EMP' and id_req = $id and master_type = 1 ");			

	// 					 $josnx 	=  json_decode($pool[0]->json_data,1);
	// 					 $act = $this->cari($empID,$josnx,$empID,$result[$key]->status_id,$result[$key]->availment_date);

	// 					 if($empID == '2014888'){
	// 							$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
	// 						}
						
	// 					/*if($result[$key]->availment_date == '2018-09-28'){
	// 						return [$act,12];
	// 					}*/
	// 					if($act['approve']){ $result[$key]->can_approve = 'y'; }
	// 					else{ $result[$key]->can_approve = 'n'; }

	// 					if($act['cancel']){ $result[$key]->can_cancel = 'y'; }
	// 					else{ $result[$key]->can_cancel = 'n'; }

	// 					if($act['reject']){ $result[$key]->can_reject = 'y'; }
	// 					else{ $result[$key]->can_reject = 'n'; }
	// 					/*if($result[$key]->approval_name == null && $result[$key]->approver_name == null  ){
						
	// 						$json =  json_decode($pool[0]->json_data,1);
	// 						$flow = $json['req_flow'];
	// 						$nil =  [];
	// 						$nil_key  = '';

	// 						foreach ($flow as $keys => $values) {
	// 							if($values == 1){

	// 								foreach ($json[$keys] as $x => $z) {
	// 									$final =  $json[$keys][$x];
	// 									foreach ($final as $a=> $b) {
	// 										if(is_numeric($a)){
	// 											if($empID  == $a){
	// 												$result[$key]->can_approve  = 'y';
	// 												$result[$key]->can_reject   = 'y';
	// 												$result[$key]->can_cancel   = 'n';
	// 											}
	// 										}
	// 									}
	// 								}
	// 						    }		
	// 						}

	// 						if(!isset($result[$key]->can_approve)){
	// 							if($empID  == $EMP){
	// 								$result[$key]->can_approve  = 'n';
	// 								$result[$key]->can_reject   = 'n';
	// 								$result[$key]->can_cancel   = 'y';	
	// 							}else{
	// 								$result[$key]->can_approve  = 'n';
	// 								$result[$key]->can_reject   = 'n';
	// 								$result[$key]->can_cancel   = 'n';	
	// 							}
	// 						}
	// 					}else if($result[$key]->approval_name != null and  $result[$key]->approver_name ==  null ){
	// 						$json =  json_decode($pool[0]->json_data,1);
	// 						$flow = $json['req_flow'];
	// 						$nil =  [];
	// 						$nil_key  = '';


	// 						if($result[$key]->status_id  != 2 && $result[$key]->status_id  != 1){
	// 							$result[$key]->can_approve  = 'n';
	// 							$result[$key]->can_reject   = 'n';
	// 							$result[$key]->can_cancel   = 'n';
	// 						}else{
	// 							foreach ($flow as $keys => $values) {
	// 								if($values == 2){
	// 									foreach ($json[$keys] as $x => $z) {
	// 									$final =  $json[$keys][$x];
	// 										foreach ($final as $a=> $b) {
	// 											if(is_numeric($a)){
	// 												if($empID  == $a){
	// 													$result[$key]->can_approve  = 'y';
	// 													$result[$key]->can_reject   = 'y';
	// 													$result[$key]->can_cancel   = 'n';
	// 												}
	// 											}
	// 										}
	// 									}
	// 							    }		
	// 							}

	// 							if(!isset($result[$key]->can_approve)){
	// 								if($empID  == $EMP){
	// 									$result[$key]->can_approve  = 'n';
	// 									$result[$key]->can_reject   = 'n';
	// 									$result[$key]->can_cancel   = 'y';	
	// 								}else{
	// 									$result[$key]->can_approve  = 'n';
	// 									$result[$key]->can_reject   = 'n';
	// 									$result[$key]->can_cancel   = 'n';	
	// 								}
	// 							}
	// 						}
	// 					}else if($result[$key]->approval_name != null and  $result[$key]->approver_name !=  null){
	// 							$result[$key]->can_approve  = 'n';
	// 							$result[$key]->can_reject   = 'n';
	// 							$result[$key]->can_cancel   = 'n';	
	// 					}elseif($result[$key]->approval_name == null and  $result[$key]->approver_name !=  null){
	// 							$result[$key]->can_approve  = 'n';
	// 							$result[$key]->can_reject   = 'n';
	// 							$result[$key]->can_cancel   = 'n';	
	// 					}else{
	// 						//KUDOS
	// 					}*/

						
	// 					$type_id  = $result[$key]->type_id;
	// 					$com = \DB::SELECT("select command_center.id,command_center.filename,command_center.created_at,command_center.path,command_center.comment, concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name)as employeeName   from command_center,emp where command_center.request_id=$id and command_center.type_id=$type_id and emp.employee_id=command_center.employee_id order by command_center.id ASC");

                       
	// 					if(!isset($com) && $com == null){
	// 						$com = [];
	// 					}else{
	// 						//KUDOS
	// 					}

	// 					$result[$key]->comment = $com;

	// 					if($TYP != null){
	// 						if($value->type_id != $TYP){
	// 							unset($result[$key]);
	// 						}
	// 					}
                        
                        
	// 				}

	// 				return response()->json(['header' => ['message' =>  'Success search data', 'status' => 200],'data' => $result],200);
	// 			}else{
	// 				return response()->json(['header' => ['message' =>  'data not found', 'status' => 200],'data' => []],200);
	// 			}
	// 		}
 //            else{
	// 			$implode  =  implode(',',$temp);
 //               if(in_array(5,$temp)){
 //               		 $past  =  " and att_schedule_request.availment_date < now()";	
 //               	}

	// 				/*emp.employee_id =  '$EMP'  */
 //               	$query  = "select emp.employee_id, 
	// 										   concat(emp.first_name,' ',ifnull(emp.middle_name,''),' ',ifnull(emp.last_name,'')) as employee,
	// 										   att_schedule_request.status_id,
	// 										   att_type.type,
	// 										    att_schedule_request .type_id,
	// 										   att_schedule_request.approval_date as approval_id,
	// 				                           				att_schedule_request.approver_date as approver_id,
	// 										   (select concat(first_name,' ',ifnull(middle_name,''),' ',ifnull(last_name,''))  from emp where employee_id  =  (select approval_id))  as approval_name,
	// 				            			  		 (select concat(first_name,' ',ifnull(middle_name,''),' ',ifnull(last_name,'')) from  emp  where employee_id  =  (select approver_id)) as  approver_name,
	// 										   att_schedule_request.id,
	// 										   att_schedule_request.availment_date,
	// 										   att_schedule_request.approval,
	// 										   att_schedule_request.approver,
	// 										   att_status.status,
	// 										   att_schedule_request.date_request
	// 									from 
	// 									att_schedule_request,att_type,emp,att_status where 
	// 									$issetEMP 
	// 									$Q_DEPART 
	// 									$TYP
	// 									and att_type.type_id = att_schedule_request.type_id 
	// 									and att_status.status_id in ($implode)
	// 									and att_status.status_id =  att_schedule_request.status_id
	// 									and emp.employee_id =  att_schedule_request.employee_id ";


	// 			if(isset($past)){
	// 				$query .= $past;
	// 			}
		
 //                $result  = \DB::SELECT($query);
	// 			if($result  !=  null){	


	// 				foreach ($result as $key => $value) {
	// 					$loop_emp  =  $value->employee_id;

	// 					//$pool  = \DB::SELECT("select * from  ");	

	// 					if($DEPART != null){
	// 						$check_department =  \DB::SELECT("select department from emp  where  employee_id = '$loop_emp' ");
	// 						if($check_department != null){
	// 							if($check_department[0]->department != null){
	// 								if($DEPART != $check_department[0]->department){
	// 									unset($result[$key]);
	// 								}
	// 							}
	// 						}
	// 					}

	// 					if(!isset($result[0])){
	// 						return response()->json(['header' => ['message' => 'Result  : 0', 'status' => 200 ],'data' => []],200);
	// 					}

						
	// 					$id  = $result[$key]->id;
	// 					$pool  =  \DB::SELECT("select * from pool_request where employee_id =  '$EMP' and id_req = $id and master_type = 1 ");			
	// 					if(count($pool) > 0){


	// 							$josnx 	=  json_decode($pool[0]->json_data,1);
	// 							$act = $this->cari($empID,$josnx,$empID,$result[$key]->status_id,$result[$key]->availment_date);
	// 							if($empID == '2014888'){
	// 								$act = ["cancel"=>true,"approve"=>true,"reject"=>true];
	// 							}
	// 							// if($result[$key]->availment_date == "2017-03-30"){
	// 							// 	return [$act,$id,$result[$key]];
	// 							// }
	// 							/*if($result[$key]->availment_date == '2018-09-28'){
	// 								return [$act,$josnx,13];
	// 							}*/
								
	// 							if($act['approve']){ $result[$key]->can_approve = 'y'; }
	// 							else{ $result[$key]->can_approve = 'n'; }

	// 							if($act['cancel']){ $result[$key]->can_cancel = 'y'; }
	// 							else{ $result[$key]->can_cancel = 'n'; }

	// 							if($act['reject']){ $result[$key]->can_reject = 'y'; }
	// 							else{ $result[$key]->can_reject = 'n'; }
	// 					}else{

	// 						if($empID == '2014888'){
	// 								$act = ["cancel"=>true,"approve"=>true,"reject"=>true];
	// 							}
	// 							// if($result[$key]->availment_date == "2017-03-30"){
	// 							// 	return [$act,$id,$result[$key]];
	// 							// }
	// 							/*if($result[$key]->availment_date == '2018-09-28'){
	// 								return [$act,$josnx,13];
	// 							}*/
	// 							if(!$act){
	// 								$act['approve'] = null;
	// 								$act['cancel'] = null;
	// 								$act['reject'] = null;
	// 							}

	// 							if($act['approve']){ $result[$key]->can_approve = 'y'; }
	// 							else{ $result[$key]->can_approve = 'n'; }

	// 							if($act['cancel']){ $result[$key]->can_cancel = 'y'; }
	// 							else{ $result[$key]->can_cancel = 'n'; }

	// 							if($act['reject']){ $result[$key]->can_reject = 'y'; }
	// 							else{ $result[$key]->can_reject = 'n'; }
	// 					}
	// 					/*if($result[$key]->approval_name == null && $result[$key]->approver_name == null  ){
	// 						// if(count($pool) == 0){
	// 						// 		return "select * from pool_request where employee_id =  '$EMP' and id_req = $id and master_type = 1 ";									
	// 						// }
	// 						$json =  json_decode($pool[0]->json_data,1);
	// 						$flow = $json['req_flow'];
	// 						$nil =  [];
	// 						$nil_key  = '';

	// 						foreach ($flow as $keys => $values) {
	// 							if($values == 1){

	// 								foreach ($json[$keys] as $x => $z) {
	// 									$final =  $json[$keys][$x];
	// 									foreach ($final as $a=> $b) {
	// 										if(is_numeric($a)){
	// 											if($empID  == $a){
	// 												$result[$key]->can_approve  = 'y';
	// 												$result[$key]->can_reject   = 'y';
	// 												$result[$key]->can_cancel   = 'n';
	// 											}
	// 										}
	// 									}
	// 								}
	// 						    }		
	// 						}

							
	// 						if(!isset($result[$key]->can_approve)){

	// 							if($empID  == $EMP){
	// 								$result[$key]->can_approve  = 'n';
	// 								$result[$key]->can_reject   = 'n';
	// 								$result[$key]->can_cancel   = 'y';	
	// 							}else{
	// 								$result[$key]->can_approve  = 'n';
	// 								$result[$key]->can_reject   = 'n';
	// 								$result[$key]->can_cancel   = 'n';	
	// 							}
	// 						}
	// 					}else if($result[$key]->approval_name != null and  $result[$key]->approver_name ==  null ){
						
	// 						$json =  json_decode($pool[0]->json_data,1);
	// 						$flow = $json['req_flow'];
	// 						$nil =  [];
	// 						$nil_key  = '';


	// 						if($result[$key]->status_id  != 2 && $result[$key]->status_id  != 1){
	// 							$result[$key]->can_approve  = 'n';
	// 							$result[$key]->can_reject   = 'n';
	// 							$result[$key]->can_cancel   = 'n';
	// 						}else{
	// 							foreach ($flow as $keys => $values) {
	// 								if($values == 2){
	// 									foreach ($json[$keys] as $x => $z) {
	// 									$final =  $json[$keys][$x];
	// 										foreach ($final as $a=> $b) {
	// 											if(is_numeric($a)){
	// 												if($empID  == $a){
	// 													$result[$key]->can_approve  = 'y';
	// 													$result[$key]->can_reject   = 'y';
	// 													$result[$key]->can_cancel   = 'n';
	// 												}
	// 											}
	// 										}
	// 									}
	// 							    }		
	// 							}

	// 							if(!isset($result[$key]->can_approve)){
	// 								if($token  == $EMP){
	// 									$result[$key]->can_approve  = 'n';
	// 									$result[$key]->can_reject   = 'n';
	// 									$result[$key]->can_cancel   = 'y';	
	// 								}else{
	// 									$result[$key]->can_approve  = 'n';
	// 									$result[$key]->can_reject   = 'n';
	// 									$result[$key]->can_cancel   = 'n';	
	// 								}
	// 							}
	// 						}
	// 					}else if($result[$key]->approval_name != null and  $result[$key]->approver_name !=  null){
	// 							$result[$key]->can_approve  = 'n';
	// 							$result[$key]->can_reject   = 'n';
	// 							$result[$key]->can_cancel   = 'n';	
	// 					}elseif($result[$key]->approval_name == null and  $result[$key]->approver_name !=  null){
	// 							$result[$key]->can_approve  = 'n';
	// 							$result[$key]->can_reject   = 'n';
	// 							$result[$key]->can_cancel   = 'n';	
	// 					}else{
	// 						//KUDOS
	// 					}
	// 	*/$type_id  = $result[$key]->type_id;
	// 					$com = \DB::SELECT("select command_center.id,command_center.filename,command_center.created_at,command_center.path,command_center.comment, concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name)as employeeName   from command_center,emp where command_center.request_id=$id and command_center.type_id=$type_id and emp.employee_id=command_center.employee_id order by command_center.id ASC");

	// 					if(!isset($com) && $com == null){
	// 						$com = [];
	// 					}else{
	// 						//KUDOS
	// 					}

	// 					$result[$key]->comment = $com;


	// 					/*if($TYP != null){
	// 						if($value->type_id != $TYP){
	// 							unset($result[$key]);
	// 						}
	// 					}*/
	// 				}

	// 				return response()->json(['header' => ['message' =>  'Success search data', 'status' => 200],'data' => $result],200);
	// 			}else{
	// 				return response()->json(['header' => ['message' =>  'data not found', 'status' => 200],'data' => []],200);
	// 			}
	// 		}



	// 		/**
	// 		 * check  user   is   hrd , superuser, supervisor, employee [ if ] hrd can look at all 
	// 		 */
			
	// 		//if()


	// 		//return \Response::json(['header'=>['message'=>$data['message'],'status'=>$data['status'], 'access'=>$access[3]],'data'=>$data['data']],$data['status']);
	// 	}
	// 	else{
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 	}
 //        return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access[3]],'data'=>$data],$status);
	// }

	public function search()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$model = new requestList;
			$i = \Input::all();
			$include = \Input::get("include");
			if(isset($include)){
				$include = $include;
			}else{
				$include = 0;
			}
			$dep = \Input::json('department');


			$reqType = \Input::json('requestType');
			$decode = base64_decode(\Request::get('key'));
			$empID = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
			//$empID = \Input::get('employee_name');
			$role = $model->checkRole($empID);

			if (!(isset($i['employee_name']))){
				$i['employee_id'] = "";
			}
			else{
				$i['employee_id'] = $i['employee_name'];
			}

			$empDepartment = $model->getEmpDepartment($empID);
			// show request list after checking role
			$role = $model->checkRole($empID);

			$manipulate = [];
			$temp  = []; 
			$arr  = ['all' =>  0,'canceled'  => 4,'past' => 5,'pending' => 1,'schedule' => 2, 'rejected' => 3];

			foreach ($arr as $key => $value) {
				if(isset($i[$key]) && $i[$key] != 0){
					$temp[] = $value;
				}
			}	
			

			/**
			 * get type
			 */	
			
			$EMP =   $i['employee_id'];
			if(isset($EMP) && $EMP != ""){
				$issetEMP = "emp.employee_id =  '$EMP'";
			}else{
				$issetEMP = "emp.employee_id = att_schedule_request.employee_id";
			}
			$TYP = (isset($i['requestType']) && $i['requestType'] != null ? $i['requestType']['type_id'] : null);
			if($TYP){
				$TYP = "and att_schedule_request.type_id = $TYP";
			}else{
				$TYP = '';
			}
			$DEPART  =  ((isset($i['department'])  && $i['department'] != null) || (isset($i['department'])  && $i['department']['id'] == 0) ? $i['department']['id'] : null);

			if($DEPART){
				$Q_DEPART = "and emp.department = $DEPART";
			}else{
				$Q_DEPART = '';
			}
            
          /*return "select emp.employee_id, 
											   concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as employee,
											   att_schedule_request.status_id,
											   att_schedule_request.id,
											   att_type.type,
											   att_schedule_request .type_id,
											   att_schedule_request.approval_date as approval_id,
					                           att_schedule_request.approver_date as approver_id,
											   (select concat(first_name,' ',middle_name,' ',last_name)  from emp where employee_id  =  (select approval_id))  as approval_name,
					            			   (select concat(first_name,' ',middle_name,' ',last_name) from  emp  where employee_id  =  (select approver_id)) as  approver_name,
											   att_schedule_request.id,
											   att_schedule_request.approval,
											   att_schedule_request.approver,
											   att_schedule_request.availment_date,
											   att_status.status,
											   att_schedule_request.update_at,
											   att_schedule_request.date_request
										from 
										att_schedule_request,att_type,emp,att_status where 
										$issetEMP $TYP
										and att_type.type_id = att_schedule_request.type_id 
										and att_status.status_id =  att_schedule_request.status_id
										and emp.employee_id =  att_schedule_request.employee_id ";*/
				
			if(in_array(0,$temp)){
				/*emp.employee_id =  '$EMP' */
				$querys = 				"select emp.employee_id, 
											   concat(emp.first_name,' ',ifnull(emp.middle_name,''),' ',ifnull(emp.last_name,'')) as employee,
											   att_schedule_request.status_id,
											   att_schedule_request.id,
											   att_type.type,
											   att_schedule_request .type_id,
											   att_schedule_request.approval_date as approval_id,
					                           att_schedule_request.approver_date as approver_id,
											   (select concat(first_name,' ',ifnull(middle_name,''),' ',ifnull(last_name,''))  from emp where employee_id  =  (select approval_id))  as approval_name,
					            			   (select concat(first_name,' ',ifnull(middle_name,''),' ',ifnull(last_name,'')) from  emp  where employee_id  =  (select approver_id)) as  approver_name,
											   att_schedule_request.id,
											   att_schedule_request.approval,
											   att_schedule_request.approver,
											   att_schedule_request.availment_date,
											   att_status.status,
											   att_schedule_request.update_at,
											   att_schedule_request.date_request
										from 
										att_schedule_request,att_type,emp,att_status where 
										$issetEMP 
										$Q_DEPART 
										$TYP
										and att_type.type_id = att_schedule_request.type_id 
										and att_status.status_id =  att_schedule_request.status_id
										and emp.employee_id =  att_schedule_request.employee_id ";

				$result  = \DB::SELECT($querys);
         		//return [$result,$querys];
				if($result  !=  null){	
                   
					foreach ($result as $key => $value) {
				
						$loop_emp  =  $value->employee_id;

						if($DEPART != null){
							$check_department =  \DB::SELECT("select  department from emp  where  employee_id = '$loop_emp' ");
							if($check_department != null){
								if($check_department[0]->department != null){
									if($DEPART != $check_department[0]->department){
                                       
										unset($result[$key]);
									}
								}
							}else{
								return response()->json(['header' => ['message' => 'Search Option fail', 'status' => 500 ],'data' => []],500);
							}
						}
                        
                           //return $result[0]->id;
							//if(isset($result[0]->id)){
						//return response()->json(['header' => ['message' => 'Result  : 0', 'status' => 200 ],'data' => []],200);
						//}
                        
                      
						 $id  = $result[$key]->id;
						//print_r("select * from pool_request where employee_id =  '$EMP' and id_req = $id and master_type = 1 ");
						 $pool  =  \DB::SELECT("select * from pool_request where employee_id =  '$EMP' and id_req = $id and master_type = 1 ");
						 if(count($pool) == 0){

						 	return ["select * from pool_request where employee_id =  '$EMP' and id_req = $id and master_type = 1 "];
						 }
						 	$josnx 	=  json_decode($pool[0]->json_data,1);
						 $act = $this->cari($empID,$josnx,$empID,$result[$key]->status_id,$result[$key]->availment_date);

						 if($empID == '2014888'){
								$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
							}
						
						/*if($result[$key]->availment_date == '2018-09-28'){
							return [$act,12];
						}*/
						if($act['approve']){ $result[$key]->can_approve = 'y'; }
						else{ $result[$key]->can_approve = 'n'; }

						if($act['cancel']){ $result[$key]->can_cancel = 'y'; }
						else{ $result[$key]->can_cancel = 'n'; }

						if($act['reject']){ $result[$key]->can_reject = 'y'; }
						else{ $result[$key]->can_reject = 'n'; }
						/*if($result[$key]->approval_name == null && $result[$key]->approver_name == null  ){
						
							$json =  json_decode($pool[0]->json_data,1);
							$flow = $json['req_flow'];
							$nil =  [];
							$nil_key  = '';

							foreach ($flow as $keys => $values) {
								if($values == 1){

									foreach ($json[$keys] as $x => $z) {
										$final =  $json[$keys][$x];
										foreach ($final as $a=> $b) {
											if(is_numeric($a)){
												if($empID  == $a){
													$result[$key]->can_approve  = 'y';
													$result[$key]->can_reject   = 'y';
													$result[$key]->can_cancel   = 'n';
												}
											}
										}
									}
							    }		
							}

							if(!isset($result[$key]->can_approve)){
								if($empID  == $EMP){
									$result[$key]->can_approve  = 'n';
									$result[$key]->can_reject   = 'n';
									$result[$key]->can_cancel   = 'y';	
								}else{
									$result[$key]->can_approve  = 'n';
									$result[$key]->can_reject   = 'n';
									$result[$key]->can_cancel   = 'n';	
								}
							}
						}else if($result[$key]->approval_name != null and  $result[$key]->approver_name ==  null ){
							$json =  json_decode($pool[0]->json_data,1);
							$flow = $json['req_flow'];
							$nil =  [];
							$nil_key  = '';


							if($result[$key]->status_id  != 2 && $result[$key]->status_id  != 1){
								$result[$key]->can_approve  = 'n';
								$result[$key]->can_reject   = 'n';
								$result[$key]->can_cancel   = 'n';
							}else{
								foreach ($flow as $keys => $values) {
									if($values == 2){
										foreach ($json[$keys] as $x => $z) {
										$final =  $json[$keys][$x];
											foreach ($final as $a=> $b) {
												if(is_numeric($a)){
													if($empID  == $a){
														$result[$key]->can_approve  = 'y';
														$result[$key]->can_reject   = 'y';
														$result[$key]->can_cancel   = 'n';
													}
												}
											}
										}
								    }		
								}

								if(!isset($result[$key]->can_approve)){
									if($empID  == $EMP){
										$result[$key]->can_approve  = 'n';
										$result[$key]->can_reject   = 'n';
										$result[$key]->can_cancel   = 'y';	
									}else{
										$result[$key]->can_approve  = 'n';
										$result[$key]->can_reject   = 'n';
										$result[$key]->can_cancel   = 'n';	
									}
								}
							}
						}else if($result[$key]->approval_name != null and  $result[$key]->approver_name !=  null){
								$result[$key]->can_approve  = 'n';
								$result[$key]->can_reject   = 'n';
								$result[$key]->can_cancel   = 'n';	
						}elseif($result[$key]->approval_name == null and  $result[$key]->approver_name !=  null){
								$result[$key]->can_approve  = 'n';
								$result[$key]->can_reject   = 'n';
								$result[$key]->can_cancel   = 'n';	
						}else{
							//KUDOS
						}*/

						
						$type_id  = $result[$key]->type_id;
						$com = \DB::SELECT("select command_center.id,command_center.filename,command_center.created_at,command_center.path,command_center.comment, concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name)as employeeName   from command_center,emp where command_center.request_id=$id and command_center.type_id=$type_id and emp.employee_id=command_center.employee_id order by command_center.id ASC");

                       
						if(!isset($com) && $com == null){
							$com = [];
						}else{
							//KUDOS
						}

						$result[$key]->comment = $com;

						if($TYP != null){
							if($value->type_id != $TYP){
								unset($result[$key]);
							}
						}
                        
                        
					}

					return response()->json(['header' => ['message' =>  'Success search data', 'status' => 200],'data' => $result],200);
				}else{
					return response()->json(['header' => ['message' =>  'data not found', 'status' => 200],'data' => []],200);
				}
			}
            else{
				$implode  =  implode(',',$temp);
               if(in_array(5,$temp)){
               		 $past  =  " and att_schedule_request.availment_date < now()";	
               	}

					/*emp.employee_id =  '$EMP'  */
               	$query  = "select emp.employee_id, 
											   concat(emp.first_name,' ',ifnull(emp.middle_name,''),' ',ifnull(emp.last_name,'')) as employee,
											   att_schedule_request.status_id,
											   att_type.type,
											    att_schedule_request .type_id,
											   att_schedule_request.approval_date as approval_id,
					                           				att_schedule_request.approver_date as approver_id,
											   (select concat(first_name,' ',ifnull(middle_name,''),' ',ifnull(last_name,''))  from emp where employee_id  =  (select approval_id))  as approval_name,
					            			  		 (select concat(first_name,' ',ifnull(middle_name,''),' ',ifnull(last_name,'')) from  emp  where employee_id  =  (select approver_id)) as  approver_name,
											   att_schedule_request.id,
											   att_schedule_request.availment_date,
											   att_schedule_request.approval,
											   att_schedule_request.approver,
											   att_status.status,
											   att_schedule_request.date_request
										from 
										att_schedule_request,att_type,emp,att_status where 
										$issetEMP 
										$Q_DEPART 
										$TYP
										and att_type.type_id = att_schedule_request.type_id 
										and att_status.status_id in ($implode)
										and att_status.status_id =  att_schedule_request.status_id
										and emp.employee_id =  att_schedule_request.employee_id ";


				if(isset($past)){
					$query .= $past;
				}
		
                $result  = \DB::SELECT($query);
				if($result  !=  null){	


					foreach ($result as $key => $value) {
						$loop_emp  =  $value->employee_id;

						//$pool  = \DB::SELECT("select * from  ");	

						if($DEPART != null){
							$check_department =  \DB::SELECT("select department from emp  where  employee_id = '$loop_emp' ");
							if($check_department != null){
								if($check_department[0]->department != null){
									if($DEPART != $check_department[0]->department){
										unset($result[$key]);
									}
								}
							}
						}

						if(!isset($result[0])){
							return response()->json(['header' => ['message' => 'Result  : 0', 'status' => 200 ],'data' => []],200);
						}

						
						$id  = $result[$key]->id;
						$pool  =  \DB::SELECT("select * from pool_request where employee_id =  '$EMP' and id_req = $id and master_type = 1 ");			
						if(count($pool) > 0){


								$josnx 	=  json_decode($pool[0]->json_data,1);
								$act = $this->cari($empID,$josnx,$empID,$result[$key]->status_id,$result[$key]->availment_date);
								if($empID == '2014888'){
									$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
								}
								// if($result[$key]->availment_date == "2017-03-30"){
								// 	return [$act,$id,$result[$key]];
								// }
								/*if($result[$key]->availment_date == '2018-09-28'){
									return [$act,$josnx,13];
								}*/
								
								if($act['approve']){ $result[$key]->can_approve = 'y'; }
								else{ $result[$key]->can_approve = 'n'; }

								if($act['cancel']){ $result[$key]->can_cancel = 'y'; }
								else{ $result[$key]->can_cancel = 'n'; }

								if($act['reject']){ $result[$key]->can_reject = 'y'; }
								else{ $result[$key]->can_reject = 'n'; }
						}else{
							// $josnx 	=  json_decode($pool[0]->json_data,1);
							// $act = $this->cari($empID,$josnx,$empID,$result[$key]->status_id,$result[$key]->availment_date);
							$act = null;
							if($empID == '2014888'){
									$act = ["cancel"=>true,"approve"=>true,"reject"=>true];
								}
								// if($result[$key]->availment_date == "2017-03-30"){
								// 	return [$act,$id,$result[$key]];
								// }
								/*if($result[$key]->availment_date == '2018-09-28'){
									return [$act,$josnx,13];
								}*/
								
								if(!$act){
									$act['approve'] = null;
									$act['cancel'] = null;
									$act['reject'] = null;
								}
								if($act['approve']){ $result[$key]->can_approve = 'y'; }
								else{ $result[$key]->can_approve = 'n'; }

								if($act['cancel']){ $result[$key]->can_cancel = 'y'; }
								else{ $result[$key]->can_cancel = 'n'; }

								if($act['reject']){ $result[$key]->can_reject = 'y'; }
								else{ $result[$key]->can_reject = 'n'; }
						}
						/*if($result[$key]->approval_name == null && $result[$key]->approver_name == null  ){
							// if(count($pool) == 0){
							// 		return "select * from pool_request where employee_id =  '$EMP' and id_req = $id and master_type = 1 ";									
							// }
							$json =  json_decode($pool[0]->json_data,1);
							$flow = $json['req_flow'];
							$nil =  [];
							$nil_key  = '';

							foreach ($flow as $keys => $values) {
								if($values == 1){

									foreach ($json[$keys] as $x => $z) {
										$final =  $json[$keys][$x];
										foreach ($final as $a=> $b) {
											if(is_numeric($a)){
												if($empID  == $a){
													$result[$key]->can_approve  = 'y';
													$result[$key]->can_reject   = 'y';
													$result[$key]->can_cancel   = 'n';
												}
											}
										}
									}
							    }		
							}

							
							if(!isset($result[$key]->can_approve)){

								if($empID  == $EMP){
									$result[$key]->can_approve  = 'n';
									$result[$key]->can_reject   = 'n';
									$result[$key]->can_cancel   = 'y';	
								}else{
									$result[$key]->can_approve  = 'n';
									$result[$key]->can_reject   = 'n';
									$result[$key]->can_cancel   = 'n';	
								}
							}
						}else if($result[$key]->approval_name != null and  $result[$key]->approver_name ==  null ){
						
							$json =  json_decode($pool[0]->json_data,1);
							$flow = $json['req_flow'];
							$nil =  [];
							$nil_key  = '';


							if($result[$key]->status_id  != 2 && $result[$key]->status_id  != 1){
								$result[$key]->can_approve  = 'n';
								$result[$key]->can_reject   = 'n';
								$result[$key]->can_cancel   = 'n';
							}else{
								foreach ($flow as $keys => $values) {
									if($values == 2){
										foreach ($json[$keys] as $x => $z) {
										$final =  $json[$keys][$x];
											foreach ($final as $a=> $b) {
												if(is_numeric($a)){
													if($empID  == $a){
														$result[$key]->can_approve  = 'y';
														$result[$key]->can_reject   = 'y';
														$result[$key]->can_cancel   = 'n';
													}
												}
											}
										}
								    }		
								}

								if(!isset($result[$key]->can_approve)){
									if($token  == $EMP){
										$result[$key]->can_approve  = 'n';
										$result[$key]->can_reject   = 'n';
										$result[$key]->can_cancel   = 'y';	
									}else{
										$result[$key]->can_approve  = 'n';
										$result[$key]->can_reject   = 'n';
										$result[$key]->can_cancel   = 'n';	
									}
								}
							}
						}else if($result[$key]->approval_name != null and  $result[$key]->approver_name !=  null){
								$result[$key]->can_approve  = 'n';
								$result[$key]->can_reject   = 'n';
								$result[$key]->can_cancel   = 'n';	
						}elseif($result[$key]->approval_name == null and  $result[$key]->approver_name !=  null){
								$result[$key]->can_approve  = 'n';
								$result[$key]->can_reject   = 'n';
								$result[$key]->can_cancel   = 'n';	
						}else{
							//KUDOS
						}
		*/$type_id  = $result[$key]->type_id;
						$com = \DB::SELECT("select command_center.id,command_center.filename,command_center.created_at,command_center.path,command_center.comment, concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name)as employeeName   from command_center,emp where command_center.request_id=$id and command_center.type_id=$type_id and emp.employee_id=command_center.employee_id order by command_center.id ASC");

						if(!isset($com) && $com == null){
							$com = [];
						}else{
							//KUDOS
						}

						$result[$key]->comment = $com;


						/*if($TYP != null){
							if($value->type_id != $TYP){
								unset($result[$key]);
							}
						}*/
					}

					return response()->json(['header' => ['message' =>  'Success search data', 'status' => 200],'data' => $result],200);
				}else{
					return response()->json(['header' => ['message' =>  'data not found', 'status' => 200],'data' => []],200);
				}
			}



			/**
			 * check  user   is   hrd , superuser, supervisor, employee [ if ] hrd can look at all 
			 */
			
			//if()


			//return \Response::json(['header'=>['message'=>$data['message'],'status'=>$data['status'], 'access'=>$access[3]],'data'=>$data['data']],$data['status']);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
        return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access[3]],'data'=>$data],$status);
	}

	// GET LIST OF REQUEST*****************************************************************
	public function getList($i,$dep,$empDepartment,$reqType,$role,$include)
	{
		$model = new requestList;
		$data = [];
		if(isset($i['all'])){
			if ($i['all'] == "1"){
				$status = 0;
				for ($j=1; $j <= 5 ; $j++){
					return $temp = $model->viewList($i,$role,$j,$dep['id'],$reqType,$include);
					if ($temp == null OR $temp == []){
						// NOOP
					}
					else{
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
		}
		else{
			if (isset($i['pending'])){
				if ($i['pending'] == "1"){
					$status = 1;
					return $temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
			if (isset($i['rejected'])){
				if ($i['rejected'] == "1"){
					$status = 3;
					$temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
			if (isset($i['canceled'])){
				if ($i['canceled'] == "1"){
					$status = 4;
					$temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
			if (isset($i['schedule'])){
				if ($i['schedule'] == "1"){
					$status = 2;
					$temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
			if (isset($i['past'])){
				if ($i['past'] == "1"){
					$status = 5;
					$temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
			if (isset($i['all'])){
				if ($i['past'] == "1"){
					$status = 0;
					$temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
		}
		return $data;
	}
}
