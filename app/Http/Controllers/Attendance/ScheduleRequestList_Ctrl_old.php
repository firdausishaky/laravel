<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Model\Attendance\Request\ScheduleRequestList_model as requestList;
use Larasite\Library\FuncAccess;
use League\Csv\Writer;

class ScheduleRequestList_Ctrl extends Controller {
	protected $form = 67;

	// SHOW SCHEDULE REQUEST LIST BY 1 MONTH BEFORE CURRENT MONTH UNTIL 1 YEAR NEXT (DEFAULT)**********************
	public function index()
	{
		// for ($i = 1; $i < 76; $i++){
		// 	DB::select("insert into permissions (role_id, form_id, create_, read_, update_, delete_) values (33, $i, 1, 1, 1, 1)");
		// }
		// return 0;
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
				$model = new requestList;
				// get emplloyee id from from API key
				$decode = base64_decode(\Request::get('key'));
				$empID = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
				// get employee name
				$empName = $model->getName($empID);
				($empName != null ? $empName = $empName[0]->name : $empName = "tes");
				// check employee role
				$checkRole = $model->checkRole($empID);

				$department = $model->departmentList($empID);
				$request = $model->requestList();


				$empDepartment = $model->getEmpDepartment($empID);

				$from = date('Y-m-d', strtotime(date('Y-m-d')."-1 month"));
				$to = date('Y-m-d',strtotime($from."+1 year"));
				$temp = [
					'employee_name' => $empName,
					'employee_id' => $empID,
					'employee_role' => $checkRole,
					'emp_department'=>$empDepartment,
					'from' => $from,
					'to' => $to,
					'pending' => "1",
					'department' => $department,
					'requestType' => $request
				];
				return \Response::json(['header'=>['message'=>'Show record data','status'=>'200', 'access'=>$access[3]],'data'=>$temp],200);
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3] ],'data'=>$data],$status);
	}

	function  approve($id){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
				$emp = explode('-',base64_decode(\Input::get('key')));
				$emp_id = $emp[1];
				$data = \DB::SELECT(" update att_schedule_request set status_id=2 and approver ='$empID' where id=$id ");
				if($data){
					$message = "Success approve data";
					$data = [];
					$status = 200;
				}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3] ],'data'=>$data],$status);
	}

	function  reject($id){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
				$emp = explode('-',base64_decode(\Input::get('key')));
				$emp_id = $emp[1];
				$data = \DB::SELECT(" update att_schedule_request set status_id=3 and approver ='$empID' where id=$id ");
				if($data){
					$message = "Success approve data";
					$data = [];
					$status = 200;
				}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3] ],'data'=>$data],$status);
	}

	// SEARCH SCHEDULE REQUEST LIST BY CHOOSEN OPTION****************************************************************
	public function search()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$model = new requestList;
			$i = \Input::all();
			$include = \Input::get("include");
			if(isset($include)){
				$include = $include;
			}else{
				$include = 0;
			}
			$dep = \Input::json('department');
			$reqType = \Input::json('requestType');
			$decode = base64_decode(\Request::get('key'));
			$empID = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
			//$empID = \Input::get('employee_name');
			$role = $model->checkRole($empID);

			if (!(isset($i['employee_name']))){
				$i['employee_id'] = "";
			}
			else{
				$i['employee_id'] = $i['employee_name'];
			}

			$empDepartment = $model->getEmpDepartment($empID);
			// show request list after checking role

			if ($model->checkRole($empID) != "HRD"){
				if ($dep['name'] != $empDepartment['name']){
					$dataReq = ['data'=>null, 'status'=>500, 'message'=>'Can not view other department request list'];
				}
				else{
					$dataReq = $this->getList($i,$dep,$empDepartment,$reqType,$empID,$include);
				}
			}
			else{
				$dataReq = $this->getList($i,$dep,$empDepartment,$reqType,$empID,$include);
			}

			// $this->exportToCSV($data);
			// return request list
			if ($dataReq == null){
				$data = ['data'=>[], 'status'=>200, 'message'=>'No schedule request list found'];
			}
			else{
				$data = ['data'=>$dataReq, 'status'=>200, 'message'=>'Show schedule request list'];
			}
			return \Response::json(['header'=>['message'=>$data['message'],'status'=>$data['status'], 'access'=>$access[3]],'data'=>$data['data']],$data['status']);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access[3]],'data'=>$data],$status);
	}

	// GET LIST OF REQUEST*****************************************************************
	public function getList($i,$dep,$empDepartment,$reqType,$role,$include)
	{
		$model = new requestList;
		$data = [];
		if (isset($i['all'])){
			if ($i['all'] == "1"){
				$status = 0;
				for ($j=1; $j <= 5 ; $j++) {
					$temp = $model->viewList($i,$role,$j,$dep['id'],$reqType,$include);
					if ($temp == null OR $temp == []){
						// NOOP
					}
					else{
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
		}
		else{
			if (isset($i['pending'])){
				if ($i['pending'] == "1"){
					$status = 1;
					$temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
			if (isset($i['rejected'])){
				if ($i['rejected'] == "1"){
					$status = 3;
					$temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
			if (isset($i['canceled'])){
				if ($i['canceled'] == "1"){
					$status = 4;
					$temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
			if (isset($i['schedule'])){
				if ($i['schedule'] == "1"){
					$status = 2;
					$temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
			if (isset($i['past'])){
				if ($i['past'] == "1"){
					$status = 5;
					$temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
			if (isset($i['all'])){
				if ($i['past'] == "1"){
					$status = 0;
					$temp = $model->viewList($i,$role,$status,$dep['id'],$reqType,$include);
					if ($temp != null){
						$data = array_merge_recursive($data, $temp);
					}
				}
			}
		}
		return $data;
	}
}
