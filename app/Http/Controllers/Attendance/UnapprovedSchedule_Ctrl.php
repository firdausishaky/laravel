<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Attendance\Schedule\unapproved_schedule_list;
use Larasite\Model\Attendance\Request\ScheduleRequestList_model as requestList;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Library\calculation;
class UnapprovedSchedule_Ctrl extends Controller {

	protected $form = 56;

		public function index_schedulelist(){
		 	 /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
       		 	if($access[1] == 200){
							$model = new requestList;
	       		 	$unapproved = new unapproved_schedule_list;
	       		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					return $unapproved->getMessage('Unauthorized',500,[], $access[3]);
				}else{
					$unapproved = new unapproved_schedule_list;
					$input = \Input::get("key");
					$explode = explode("-",base64_decode($input));
					$empID = $explode[1];
					$hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$empID' AND t2.role_name = 'hr'");
					$adminex = \DB::SELECT("select role.role_name from ldap,role where ldap.employee_id=$empID and ldap.role_id=role.role_id ");	
					// if($hr != null){
					// 	$data = \DB::SELECT("select name,id from department where id<>1 ");
					// }elseif($adminex[0]->role_name == "adminEx"){
					$default = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");
					if(!isset($default[0])){
						$check_default = []; 
					}else{
						$check_default = $default[0];
					}
					// }elseif ($adminex[0]->role_name == "SuperUser") {
					$data = \DB::SELECT("select department.id,department.name from department where id > 1 ");
					// }elseif($adminex[0]->role_name == "admin"){
					// 	$data = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");	
					// }elseif($adminex[0]->role_name == "user"){
					// 	$data = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");	
					// }elseif($adminex[0]->role_name == "adminIt"){
					// 	$data = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");	
					// }else{
					// 	$data = null;
					// }
					//$data  = $model->departmentList($empID);
					$date = \DB::SELECT("select date from att_schedule where status=2");

					foreach($date as $value){
						$data_year = $value->date;
						$year = substr($data_year,0,4);
						   $datax[$value->date][]= $value->date;
					}

					if($date != null){
								foreach($datax as $key => $value){
									$datex[] = substr($key,0,4);
								}

								foreach($datex as $value){
									$date_datex[$value] =$value;
								}

								$unique = array_unique($date_datex);

								foreach ($unique as $key => $value) {
									$test[] = ["year" => $value];
								}
					}else{
								$test = null;
					}

					$record = ["default" => $check_default, "department" => $data, "year" => $test];
					return $unapproved->getMessage("success",200,$record, $access[3]);
				}
			}else{
				$message = $access[0]; $status = $access[1]; $data=$access[2];
				return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
			}
		}

		public function search_schedule_list(){
		  /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
			if($access[1] == 200){
	       		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$data = []; $message = 'Unauthorized'; $status = 200;
				}else{
					$unapproved = new unapproved_schedule_list;
					$input = \Input::all();
					$department = \Input::get('Department');
					$year = \Input::get('years');
					$month  = \Input::get('month')+1;

					$rule = [ 'Department' => 'required|integer', 'years' => 'required|integer', 'month' => 'required|integer' ];
					$validation = \Validator::make($input, $rule);
					//$search = \DB::SELECT("call View_ScheduleList($department,'$year','$month')");	
				
					$search = \DB::SELECT("SELECT distinct t1.employee_id, concat(t1.first_name,' ',IFNULL(t1.middle_name,''), IFNULL(t1.last_name,'')) as employee, t3.date, t2.shift_code,t5.name as department
							  FROM emp t1
							  LEFT JOIN department t5 ON t5.id = t1.department
							  LEFT JOIN att_schedule t3 ON t3.employee_id = t1.employee_id
							  LEFT JOIN attendance_work_shifts t2 ON t2.shift_id = t3.shift_id
							  WHERE t1.department = $department
							  AND YEAR( t3.DATE ) =  '$year'
						     AND month( t3.DATE ) = '$month'
							  AND t3.status = 2");
					
					if($search == null){
						return $unapproved->getMessage("Data not exist",200,[], $access[3]);
					}
					$data = [];
					$arr_x=[];
					foreach ($search as $key => $value){
						$data[$value->employee_id][] = ["date" =>$value->date,"shift_code" => $value->shift_code,"employee_id" =>$value->employee_id,"employee" => $value->employee, "department" => $value->department ];
						$arr_x[$value->employee_id][] = $value->date;
					}

					 $dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];

					$arr = [];

					//manipulation  shift code
					foreach ($arr_x as $key => $value) {
							$count = count($arr_x[$key])-1;
							for($i = 0; $i <= $count; $i++){
								$index = (integer)substr($arr_x[$key][$i], 8, 2);
								$j = $dictionary[$index-1];
								$monthx = (integer)substr($data[$key][$i]['date'], 5, 2);
								$yearx = (integer)substr($data[$key][$i]['date'], 0, 4);
								$date_sisx[$key][] = $index;
								$dx=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
								$data_tanggal[$key][$j]= $data[$key][$i]['shift_code'];
							}
					}

					//end manipulation

					$keyr = [];
					$d_date = [];

					// date manipulation
					foreach ($data as $key => $value) {
							$keyr[] = $key;
							$count = count($data[$key])-1;
							for($i = 0; $i <= $count; $i++){
							$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
							$month = substr($data[$key][$i]['date'], 5, 2);
							$year = (integer)substr($data[$key][$i]['date'], 0, 4);
							$jp = $dictionary[$indexp-1];
							$date_sisa[$key][] = $indexp;
							$d=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
							$data_tanggax[$key]["date".$jp] =$data[$key][$i]['date'];
							}
					}

					foreach ($date_sisa as $key => $value) {
						for($i = 0; $i <= $d; $i++){
									$df = $i+1;
									$sd = strlen($df);
									($sd == 1 ? $sdx = "0".$df : $sdx = $df);
									$data_tanggax[$key]["date".$dictionary[$i]] = date($year)."-".date($month)."-".date($sdx);
						}
					}
					//end date manipulation

					foreach ($data as $key => $value) {
							$count = count($data[$key])-1;

							for($i = 0; $i <= $count; $i++){
							$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
								$jp = $dictionary[$indexp-1];
								$data_tang[$key]= [$data[$key][$i]['employee'],$data[$key][$i]['employee_id'],$data[$key][$i]['department']];
							}
					}

				foreach ($data_tanggax as $key => $value) {
					$data_er[] = array_merge_recursive($value,$data_tanggal[$key],["employee" => $data_tang[$key][0]],["employee_id" => $data_tang[$key][1]],["department" => $data_tang[$key][2]]) ;
				}

				foreach ($data_er as $key => $value) {
							$data_r[] =array_merge_recursive($value,["name" => "Employee"],["dep" => "Department"]);
				}
				return $unapproved->index($data_r, $access[3]);

			}
			}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
			}

		}

		// public function search_schedule(){
		//    /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		// 	if($access[1] == 200){
  //      		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
		// 		$data = []; $message = 'Unauthorized'; $status = 200;
		// 		}else{
		// 			$unapproved = new unapproved_schedule_list;
		// 			$input = \Input::all();
		// 			$department = \Input::get('Department');
		// 			$year = \Input::get('years');
		// 			$month  = \Input::get('month')+1;

		// 			$rule = [ 'Department' => 'required|integer', 'years' => 'required|integer', 'month' => 'required|integer' ];
		// 			$validation = \Validator::make($input, $rule);
		// 			$search = \DB::SELECT("call View_ScheduleList($department,'$year',$month)");

		// 			if($search == null){
		// 				return $unapproved->getMessage("Data not exist",200,[], $access[3]);
		// 			}
		// 			$data = [];
		// 			$arr_x=[];
		// 			foreach ($search as $key => $value){
		// 				$data[$value->employee_id][] = ["date" =>$value->date,"shift_code" => $value->shift_code,"employee_id" =>$value->employee_id,"employee" => $value->employee, "department" => $value->department ];
		// 				$arr_x[$value->employee_id][] = $value->date;
		// 			}

		// 			 $dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];

		// 			$arr = [];

		// 			//manipulation  shift code
		// 			foreach ($arr_x as $key => $value) {
		// 					$count = count($arr_x[$key])-1;
		// 					for($i = 0; $i <= $count; $i++){
		// 						$index = (integer)substr($arr_x[$key][$i], 8, 2);
		// 						$j = $dictionary[$index-1];
		// 						$monthx = (integer)substr($data[$key][$i]['date'], 5, 2);
		// 						$yearx = (integer)substr($data[$key][$i]['date'], 0, 4);
		// 						$date_sisx[$key][] = $index;
		// 						$dx=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
		// 						$data_tanggal[$key][$j]= $data[$key][$i]['shift_code'];
		// 					}
		// 			}

		// 			//end manipulation

		// 			$keyr = [];
		// 			$d_date = [];

		// 			// date manipulation
		// 			foreach ($data as $key => $value) {
		// 					$keyr[] = $key;
		// 					$count = count($data[$key])-1;
		// 					for($i = 0; $i <= $count; $i++){
		// 						$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
		// 						$month = substr($data[$key][$i]['date'], 5, 2);
		// 						$year = (integer)substr($data[$key][$i]['date'], 0, 4);
		// 						$jp = $dictionary[$indexp-1];
		// 						$date_sisa[$key][] = $indexp;
		// 						$d=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
		// 						$data_tanggax[$key]["date".$jp] =$data[$key][$i]['date'];
		// 					}
		// 			}

		// 			foreach ($date_sisa as $key => $value) {
		// 				for($i = 0; $i <= $d; $i++){
		// 							$df = $i+1;
		// 							$sd = strlen($df);
		// 							($sd == 1 ? $sdx = "0".$df : $sdx = $df);
		// 							$data_tanggax[$key]["date".$dictionary[$i]] = date($year)."-".date($month)."-".date($sdx);
		// 				}
		// 			}
		// 			//end date manipulation

		// 			foreach ($data as $key => $value) {
		// 					$count = count($data[$key])-1;

		// 					for($i = 0; $i <= $count; $i++){
		// 						$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
		// 						$jp = $dictionary[$indexp-1];
		// 						$data_tang[$key]= [$data[$key][$i]['employee'],$data[$key][$i]['employee_id'],$data[$key][$i]['department']];
		// 					}
		// 			}

		// 			foreach ($data_tanggax as $key => $value) {
		// 				$data_er[] = array_merge_recursive($value,$data_tanggal[$key],["employee" => $data_tang[$key][0]],["employee_id" => $data_tang[$key][1]],["department" => $data_tang[$key][2]]) ;
		// 			}

		// 			foreach ($data_er as $key => $value) {
		// 						$data_r[] =array_merge_recursive($value,["name" => "Employee"],["dep" => "Department"]);
		// 			}
		// 			return $unapproved->index($data_r, $access[3]);

		// 		}
		// 	}else{
		// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
		// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		// 	}

		// }

		// public function search_schedule_backup(){
		//    /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); $LIB_ATT = new calculation;
		// 	if($access[1] == 200){
  //      		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
		// 		$data = []; $message = 'Unauthorized'; $status = 200;
		// 		}else{
		// 			$unapproved = new unapproved_schedule_list;
		// 			$input = \Input::all();
		// 			$department = \Input::get('Department');
		// 			$year = \Input::get('years');
		// 			$month  = \Input::get('month')+1;

		// 			$rule = [ 'Department' => 'required|integer', 'years' => 'required|integer', 'month' => 'required|integer' ];
		// 			$validation = \Validator::make($input, $rule);

		// 			// new inject
		// 			$query_emp   =  "MONTH(t1.date)  = $month and YEAR(t1.date) = $year";
					
		// 			$query_radio  = " and  t2.local_it in (1,2,3)";
		// 			$query_department  =   " and t2.department = '$department' ";
		// 			$query_job = null;

		// 			$query_order   =  "and t1.status = 2 order by t1.date ASC";

		// 			//$datas = $LIB_ATT->att('view-attendance',null,$query_emp,$query_job,$query_department,$query_radio);
		// 			$datas = $LIB_ATT->att('cut-off-record',$query_emp,$query_job,$query_department,$query_radio);
		// 			//return $datas; 
		// 			// end inject
					
		// 			//$search = \DB::SELECT("call View_ScheduleList($department,'$year',$month)");

		// 			if(count($datas['data']) == 0){
		// 				return $unapproved->getMessage("Data not exist",200,[], $access[3]);
		// 			}
		// 			$search = $datas['data'];

		// 			if($search == null){
		// 				return $unapproved->getMessage("Data not exist",200,[], $access[3]);
		// 			}
		// 			$data = [];
		// 			$arr_x=[];
		// 			foreach ($search as $key => $value){
		// 				if(strlen($value['schedule']) > 3){
		// 					$sch = $value['schedule_shift_code'];
		// 				}else{
		// 					$sch = $value['schedule'];
		// 				}
		// 				$data[$value['employee_id']][] = ["date" =>$value['date'],"shift_code" => $sch,"employee_id" =>$value['employee_id'],"employee" => $value['Name'], "department" => $value['department_name'] ];
		// 				$arr_x[$value['employee_id']][] = $value['date'];

		// 				// if($value['employee_id'] == '2017006' && $value['date'] == '2019-03-26'){
		// 				// 	return [$value];
		// 				// }
		// 			}

		// 			 $dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];

		// 			$arr = [];

		// 			//manipulation  shift code
		// 			foreach ($arr_x as $key => $value) {
		// 					$count = count($arr_x[$key])-1;
		// 					for($i = 0; $i <= $count; $i++){
		// 						$index = (integer)substr($arr_x[$key][$i], 8, 2);
		// 						$j = $dictionary[$index-1];
		// 						$monthx = (integer)substr($data[$key][$i]['date'], 5, 2);
		// 						$yearx = (integer)substr($data[$key][$i]['date'], 0, 4);
		// 						$date_sisx[$key][] = $index;
		// 						$dx=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
		// 						$data_tanggal[$key][$j]= $data[$key][$i]['shift_code'];
		// 					}
		// 			}

		// 			//end manipulation

		// 			$keyr = [];
		// 			$d_date = [];

		// 			// date manipulation
		// 			foreach ($data as $key => $value) {
		// 					$keyr[] = $key;
		// 					$count = count($data[$key])-1;
		// 					for($i = 0; $i <= $count; $i++){
		// 						$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
		// 						$month = substr($data[$key][$i]['date'], 5, 2);
		// 						$year = (integer)substr($data[$key][$i]['date'], 0, 4);
		// 						$jp = $dictionary[$indexp-1];
		// 						$date_sisa[$key][] = $indexp;
		// 						$d=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
		// 						$data_tanggax[$key]["date".$jp] =$data[$key][$i]['date'];
		// 					}
		// 			}

		// 			foreach ($date_sisa as $key => $value) {
		// 				for($i = 0; $i <= $d; $i++){
		// 							$df = $i+1;
		// 							$sd = strlen($df);
		// 							($sd == 1 ? $sdx = "0".$df : $sdx = $df);
		// 							$data_tanggax[$key]["date".$dictionary[$i]] = date($year)."-".date($month)."-".date($sdx);
		// 				}
		// 			}
		// 			//end date manipulation

		// 			foreach ($data as $key => $value) {
		// 					$count = count($data[$key])-1;

		// 					for($i = 0; $i <= $count; $i++){
		// 						$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
		// 						$jp = $dictionary[$indexp-1];
		// 						$data_tang[$key]= [$data[$key][$i]['employee'],$data[$key][$i]['employee_id'],$data[$key][$i]['department']];
		// 					}
		// 			}

		// 			foreach ($data_tanggax as $key => $value) {
		// 				$data_er[] = array_merge_recursive($value,$data_tanggal[$key],["employee" => $data_tang[$key][0]],["employee_id" => $data_tang[$key][1]],["department" => $data_tang[$key][2]]) ;
		// 			}

		// 			foreach ($data_er as $key => $value) {
		// 						$data_r[] =array_merge_recursive($value,["name" => "Employee"],["dep" => "Department"]);
		// 			}
		// 			return $unapproved->index($data_r, $access[3]);

		// 		}
		// 	}else{
		// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
		// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		// 	}

		// }

		public function search_schedule_backup_20191129(){

		   /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); $LIB_ATT = new calculation;
			if($access[1] == 200){
       		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				$data = []; $message = 'Unauthorized'; $status = 200;
				}else{
					$unapproved = new unapproved_schedule_list;
					$input = \Input::all();
					$department = \Input::get('Department');
					$year = \Input::get('years');
					$month  = \Input::get('month')+1;

					$rule = [ 'Department' => 'required|integer', 'years' => 'required|integer', 'month' => 'required|integer' ];
					$validation = \Validator::make($input, $rule);

					// new inject
					$query_emp   =  "MONTH(t1.date)  = $month and YEAR(t1.date) = $year";
					
					$query_radio  = " and  t2.local_it in (1,2,3)";
					$query_department  =   " and t2.department = '$department' ";
					$query_job = null;

					$query_order   =  "and t1.status = 2 order by t1.date ASC";

					// create  pagination mode
					// show per page (person)
					// page display

					// checking total employee per department
					//$total_emp = \DB::select("select a.employee_id from emp a where a.department=$department ");
					$total_emp = \DB::select("select distinct b.employee_id from emp a, att_schedule b where a.department=$department and b.employee_id = a.employee_id and month(b.date)=$month and year(b.date)=$year and b.status=2");
					
					$group = [];

						foreach ($total_emp as $key => $value) {
							$group[] = $value->employee_id;	
						}
					$limit = 20;
					$page = 1;
					
					$total_data = count($group);
					$total_page = ceil($total_data/$limit);
					$page = max($page, 1);
					$page = min($page, $total_page);
					$offset = ($page - 1) * $limit;
					if($offset < 0){ $offset = 0; }

					$group = array_slice($group, $offset, $limit);
					$group_str = "";
					
					for($i=0; $i < count($group) ; $i++){
						if(isset($group[$i+1])){
							$group_str .= "'$group[$i]',";
						}else{
							$group_str .= "'$group[$i]'";
						}
					}

					$query_radio .= " and t2.employee_id in($group_str)";
					
					//return [$query_radio];

					// if($total_data > 0){
					// 	foreach ($total_emp as $key => $value) {
					// 		$group[] = $value->employee_id	
					// 	}

					// 	$groups = array_slice($group, 20)
					// }
					// end paging


					//$datas = $LIB_ATT->att('view-attendance',null,$query_emp,$query_job,$query_department,$query_radio);
					//$datas = $LIB_ATT->att('cut-off-record',$query_emp,$query_job,$query_department,$query_radio);
					$datas = $LIB_ATT->att_schedule_list('cut-off-record',$query_emp,$query_job,$query_department,$query_radio);
					//return $datas; 
					// end inject
					
					//$search = \DB::SELECT("call View_ScheduleList($department,'$year',$month)");

					if(count($datas['data']) == 0){
						return $unapproved->getMessage("Data not exist",200,[], $access[3]);
					}
					$search = $datas['data'];

					if($search == null){
						return $unapproved->getMessage("Data not exist",200,[], $access[3]);
					}

					// GET COLOR SHIFT
					//$color_shift = \DB::SELECT("select shift_code, color from attendance_work_shifts order by shift_id desc");

					$data = [];
					$arr_x=[];
					//return [$search];
					foreach ($search as $key => $value){

						if(strlen($value['schedule']) > 3){
							$sch = $value['schedule_shift_code'];
						}else{
							$sch = $value['schedule'];
						}

						$col = "#000000";

						// if(count($color_shift) > 0){
						// 	foreach ($color_shift as $key_col => $value_col) {
						// 		if($value_col->shift_code == $sch){
						// 			$col = $value_col->color;
						// 		}
						// 	}
						// }

						$data[$value['employee_id']][] = ["date" =>$value['date'],"color_shift"=>$value['color_shift'],"shift_code" => $sch,"employee_id" =>$value['employee_id'],"employee" => $value['Name'], "department" => $value['department_name'] ];
						//$data[$value['employee_id']][] = ["date" =>$value['date'],"shift_code" => $sch,"employee_id" =>$value['employee_id'],"employee" => $value['Name'], "department" => $value['department_name'] ];
						$arr_x[$value['employee_id']][] = $value['date'];

						// if($value['employee_id'] == '2017006' && $value['date'] == '2019-03-26'){
						// 	return [$value];
						// }
					}

					 $dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];

					$arr = [];

					//manipulation  shift code
					foreach ($arr_x as $key => $value) {
							$count = count($arr_x[$key])-1;
							for($i = 0; $i <= $count; $i++){
								$index = (integer)substr($arr_x[$key][$i], 8, 2);
								$j = $dictionary[$index-1];
								$monthx = (integer)substr($data[$key][$i]['date'], 5, 2);
								$yearx = (integer)substr($data[$key][$i]['date'], 0, 4);
								$date_sisx[$key][] = $index;
								$dx=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
								$data_tanggal[$key][$j]= $data[$key][$i]['shift_code'];
							}
					}

					//end manipulation

					$keyr = [];
					$d_date = [];

					// date manipulation
					foreach ($data as $key => $value) {
							$keyr[] = $key;
							$count = count($data[$key])-1;
							for($i = 0; $i <= $count; $i++){
								$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
								$month = substr($data[$key][$i]['date'], 5, 2);
								$year = (integer)substr($data[$key][$i]['date'], 0, 4);
								$jp = $dictionary[$indexp-1];
								$date_sisa[$key][] = $indexp;
								$d=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
								$data_tanggax[$key]["date".$jp] =$data[$key][$i]['date'];
								$data_tanggax[$key]["color".$jp] =$data[$key][$i]['color_shift'];
							}
					}

					foreach ($date_sisa as $key => $value) {
						for($i = 0; $i <= $d; $i++){
									$df = $i+1;
									$sd = strlen($df);
									($sd == 1 ? $sdx = "0".$df : $sdx = $df);
									$data_tanggax[$key]["date".$dictionary[$i]] = date($year)."-".date($month)."-".date($sdx);
						}
					}
					//end date manipulation

					foreach ($data as $key => $value) {
							$count = count($data[$key])-1;

							for($i = 0; $i <= $count; $i++){
								$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
								$jp = $dictionary[$indexp-1];
								$data_tang[$key]= [$data[$key][$i]['employee'],$data[$key][$i]['employee_id'],$data[$key][$i]['department']];
							}
					}

					foreach ($data_tanggax as $key => $value) {
						$data_er[] = array_merge_recursive($value,$data_tanggal[$key],["employee" => $data_tang[$key][0]],["employee_id" => $data_tang[$key][1]],["department" => $data_tang[$key][2]]) ;
					}

					foreach ($data_er as $key => $value) {
								$data_r[] =array_merge_recursive($value,["name" => "Employee"],["dep" => "Department"]);
					}
					return $unapproved->index($data_r, $access[3]);

				}
			}else{
				$message = $access[0]; $status = $access[1]; $data=$access[2];
				return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
			}

		}

		public function search_schedule(){

		   /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); $LIB_ATT = new calculation;
			if($access[1] == 200){
       		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				$data = []; $message = 'Unauthorized'; $status = 200;
				}else{
					$unapproved = new unapproved_schedule_list;
					$input = \Input::all();
					$department = \Input::get('Department');
					$year = \Input::get('years');
					$month  = \Input::get('month')+1;

					$rule = [ 'Department' => 'required|integer', 'years' => 'required|integer', 'month' => 'required|integer', 'limit'=>'required|integer', 'page'=>'required|integer' ];
					$validation = \Validator::make($input, $rule);

					// new inject
					$query_emp   =  "MONTH(t1.date)  = $month and YEAR(t1.date) = $year";
					
					$query_radio  = " and  t2.local_it in (1,2,3)";
					$query_department  =   " and t2.department = '$department' ";
					$query_job = null;

					$query_order   =  "and t1.status = 2 order by t1.date ASC";

					// create  pagination mode
					// show per page (person)
					// page display

					// checking total employee per department
					//$total_emp = \DB::select("select a.employee_id from emp a where a.department=$department ");
					$total_emp = \DB::select("select distinct b.employee_id from emp a, att_schedule b where a.department=$department and b.employee_id = a.employee_id and month(b.date)=$month and year(b.date)=$year and b.status=2");
					
					if(count($total_emp) == 0){
						return response()->json(['header' => ['message' => "data not exist", 'status' => 500, "access" => $access[3]], 'data' => $data_r],500);
					}
					
					$group = [];

						foreach ($total_emp as $key => $value) {
							$group[] = $value->employee_id;	
						}
					$limit = /*20*/ $input['limit'];
					$page = /*1*/$input['page'];
					
					$total_data = count($group);
					$total_page = ceil($total_data/$limit);
					$page = max($page, 1);
					$page = min($page, $total_page);
					$offset = ($page - 1) * $limit;
					if($offset < 0){ $offset = 0; }

					$group = array_slice($group, $offset, $limit);
					$group_str = "";
					
					for($i=0; $i < count($group) ; $i++){
						if(isset($group[$i+1])){
							$group_str .= "'$group[$i]',";
						}else{
							$group_str .= "'$group[$i]'";
						}
					}
					
					$query_radio .= " and t2.employee_id in($group_str)";
					
					//return [$query_radio];

					// if($total_data > 0){
					// 	foreach ($total_emp as $key => $value) {
					// 		$group[] = $value->employee_id	
					// 	}

					// 	$groups = array_slice($group, 20)
					// }
					// end paging
					
					if(strlen($group_str) < 4){
						return response()->json(['header' => ['message' => "data not exist", 'status' => 500, "access" => $access[3]], 'data' =>[]],200);
					}

					//$datas = $LIB_ATT->att('view-attendance',null,$query_emp,$query_job,$query_department,$query_radio);
					//$datas = $LIB_ATT->att('cut-off-record',$query_emp,$query_job,$query_department,$query_radio);
					$datas = $LIB_ATT->att_schedule_list('cut-off-record',$query_emp,$query_job,$query_department,$query_radio);
					
					// end inject
					
					//$search = \DB::SELECT("call View_ScheduleList($department,'$year',$month)");

					if(count($datas['data']) == 0){
						return $unapproved->getMessage("Data not exist",200,[], $access[3]);
					}
					$search = $datas['data'];

					if($search == null){
						return $unapproved->getMessage("Data not exist",200,[], $access[3]);
					}

					// GET COLOR SHIFT
					//$color_shift = \DB::SELECT("select shift_code, color from attendance_work_shifts order by shift_id desc");

					$data = [];
					$arr_x=[];
					//return [$search];
					foreach ($search as $key => $value){

						if(strlen($value['schedule']) > 3){
							$sch = $value['schedule_shift_code'];
						}else{
							$sch = $value['schedule'];
						}

						$col = "#000000";

						// if(count($color_shift) > 0){
						// 	foreach ($color_shift as $key_col => $value_col) {
						// 		if($value_col->shift_code == $sch){
						// 			$col = $value_col->color;
						// 		}
						// 	}
						// }

						// $data[$value['employee_id']][] = ["date" =>$value['date'],"color_shift"=>$col,"shift_code" => $sch,"employee_id" =>$value['employee_id'],"employee" => $value['Name'], "department" => $value['department_name'] ];
						$data[$value['employee_id']][] = ["date" =>$value['date'],"color_shift"=>$value['color_shift'],"shift_code" => $sch,"employee_id" =>$value['employee_id'],"employee" => $value['Name'], "department" => $value['department_name'] ];

						//$data[$value['employee_id']][] = ["date" =>$value['date'],"shift_code" => $sch,"employee_id" =>$value['employee_id'],"employee" => $value['Name'], "department" => $value['department_name'] ];
						$arr_x[$value['employee_id']][] = $value['date'];

						// if($value['employee_id'] == '2017006' && $value['date'] == '2019-03-26'){
						// 	return [$value];
						// }
					}

					 $dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];

					$arr = [];

					//manipulation  shift code
					foreach ($arr_x as $key => $value) {
							$count = count($arr_x[$key])-1;
							for($i = 0; $i <= $count; $i++){
								$index = (integer)substr($arr_x[$key][$i], 8, 2);
								$j = $dictionary[$index-1];
								$monthx = (integer)substr($data[$key][$i]['date'], 5, 2);
								$yearx = (integer)substr($data[$key][$i]['date'], 0, 4);
								$date_sisx[$key][] = $index;
								$dx=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
								$data_tanggal[$key][$j]= $data[$key][$i]['shift_code'];
							}
					}

					//end manipulation

					$keyr = [];
					$d_date = [];

					// date manipulation
					foreach ($data as $key => $value) {
							$keyr[] = $key;
							$count = count($data[$key])-1;
							for($i = 0; $i <= $count; $i++){
								$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
								$month = substr($data[$key][$i]['date'], 5, 2);
								$year = (integer)substr($data[$key][$i]['date'], 0, 4);
								$jp = $dictionary[$indexp-1];
								$date_sisa[$key][] = $indexp;
								$d=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
								$data_tanggax[$key]["date".$jp] =$data[$key][$i]['date'];
								$data_tanggax[$key]["color".$jp] =$data[$key][$i]['color_shift'];
							}
					}

					foreach ($date_sisa as $key => $value) {
						for($i = 0; $i <= $d; $i++){
									$df = $i+1;
									$sd = strlen($df);
									($sd == 1 ? $sdx = "0".$df : $sdx = $df);
									$data_tanggax[$key]["date".$dictionary[$i]] = date($year)."-".date($month)."-".date($sdx);
						}
					}
					//end date manipulation

					foreach ($data as $key => $value) {
							$count = count($data[$key])-1;

							for($i = 0; $i <= $count; $i++){
								$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
								$jp = $dictionary[$indexp-1];
								$data_tang[$key]= [$data[$key][$i]['employee'],$data[$key][$i]['employee_id'],$data[$key][$i]['department']];
							}
					}

					foreach ($data_tanggax as $key => $value) {
						$data_er[] = array_merge_recursive($value,$data_tanggal[$key],["employee" => $data_tang[$key][0]],["employee_id" => $data_tang[$key][1]],["department" => $data_tang[$key][2]]) ;
					}

					foreach ($data_er as $key => $value) {
								$data_r[] =array_merge_recursive($value,["name" => "Employee"],["dep" => "Department"]);
					}
					
					if(count($data_r) == 0){
						return response()->json(['header' => ['message' => "data not exist", 'status' => 500, "access" => $access[3]], 'data' => $data_r],500);
					}else{
						return response()->json(['header' => ['message' => "View Data", 'status' => 200, "access" => $access[3]], 'data' => $data_r,"total_data"=>$total_data],200);
					}
					//return $unapproved->index($data_r, $access[3],$total_data);

				}
			}else{
				$message = $access[0]; $status = $access[1]; $data=$access[2];
				return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, 'total_data'=>$total_data],$status);
			}

		}
		
		// public function search_schedule(){

		//    /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); $LIB_ATT = new calculation;
		// 	if($access[1] == 200){
  //      		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
		// 		$data = []; $message = 'Unauthorized'; $status = 200;
		// 		}else{
		// 			$unapproved = new unapproved_schedule_list;
		// 			$input = \Input::all();
		// 			$department = \Input::get('Department');
		// 			$year = \Input::get('years');
		// 			$month  = \Input::get('month')+1;

		// 			$rule = [ 'Department' => 'required|integer', 'years' => 'required|integer', 'month' => 'required|integer' ];
		// 			$validation = \Validator::make($input, $rule);

		// 			// new inject
		// 			$query_emp   =  "MONTH(t1.date)  = $month and YEAR(t1.date) = $year";
					
		// 			$query_radio  = " and  t2.local_it in (1,2,3)";
		// 			$query_department  =   " and t2.department = '$department' ";
		// 			$query_job = null;

		// 			$query_order   =  "and t1.status = 2 order by t1.date ASC";

		// 			// create  pagination mode
		// 			// show per page (person)
		// 			// page display

		// 			// checking total employee per department
		// 			//$total_emp = \DB::select("select a.employee_id from emp a where a.department=$department ");
		// 			$total_emp = \DB::select("select distinct b.employee_id from emp a, att_schedule b where a.department=$department and b.employee_id = a.employee_id and month(b.date)=$month and year(b.date)=$year and b.status=2");
					
		// 			$group = [];

		// 				foreach ($total_emp as $key => $value) {
		// 					$group[] = $value->employee_id;	
		// 				}
		// 			$limit = 100;
		// 			$page = 1;
					
		// 			$total_data = count($group);
		// 			$total_page = ceil($total_data/$limit);
		// 			$page = max($page, 1);
		// 			$page = min($page, $total_page);
		// 			$offset = ($page - 1) * $limit;
		// 			if($offset < 0){ $offset = 0; }

		// 			$group = array_slice($group, $offset, $limit);
		// 			$group_str = "";
					
		// 			for($i=0; $i < count($group) ; $i++){
		// 				if(isset($group[$i+1])){
		// 					$group_str .= "'$group[$i]',";
		// 				}else{
		// 					$group_str .= "'$group[$i]'";
		// 				}
		// 			}

		// 			$query_radio .= " and t2.employee_id in($group_str)";
					
		// 			//return [$query_radio];

		// 			// if($total_data > 0){
		// 			// 	foreach ($total_emp as $key => $value) {
		// 			// 		$group[] = $value->employee_id	
		// 			// 	}

		// 			// 	$groups = array_slice($group, 20)
		// 			// }
		// 			// end paging


		// 			//$datas = $LIB_ATT->att('view-attendance',null,$query_emp,$query_job,$query_department,$query_radio);
		// 			//$datas = $LIB_ATT->att('cut-off-record',$query_emp,$query_job,$query_department,$query_radio);
		// 			$datas = $LIB_ATT->att_schedule_list('cut-off-record',$query_emp,$query_job,$query_department,$query_radio);
		// 			//return $datas; 
		// 			// end inject
					
		// 			//$search = \DB::SELECT("call View_ScheduleList($department,'$year',$month)");

		// 			if(count($datas['data']) == 0){
		// 				return $unapproved->getMessage("Data not exist",200,[], $access[3]);
		// 			}
		// 			$search = $datas['data'];

		// 			if($search == null){
		// 				return $unapproved->getMessage("Data not exist",200,[], $access[3]);
		// 			}

		// 			// GET COLOR SHIFT
		// 			//$color_shift = \DB::SELECT("select shift_code, color from attendance_work_shifts order by shift_id desc");

		// 			$data = [];
		// 			$arr_x=[];
		// 			//return [$search];
		// 			foreach ($search as $key => $value){

		// 				if(strlen($value['schedule']) > 3){
		// 					$sch = $value['schedule_shift_code'];
		// 				}else{
		// 					$sch = $value['schedule'];
		// 				}

		// 				$col = "#000000";

		// 				// if(count($color_shift) > 0){
		// 				// 	foreach ($color_shift as $key_col => $value_col) {
		// 				// 		if($value_col->shift_code == $sch){
		// 				// 			$col = $value_col->color;
		// 				// 		}
		// 				// 	}
		// 				// }

		// 				// $data[$value['employee_id']][] = ["date" =>$value['date'],"color_shift"=>$col,"shift_code" => $sch,"employee_id" =>$value['employee_id'],"employee" => $value['Name'], "department" => $value['department_name'] ];
		// 				$data[$value['employee_id']][] = ["date" =>$value['date'],"color_shift"=>$value['color_shift'],"shift_code" => $sch,"employee_id" =>$value['employee_id'],"employee" => $value['Name'], "department" => $value['department_name'] ];

		// 				//$data[$value['employee_id']][] = ["date" =>$value['date'],"shift_code" => $sch,"employee_id" =>$value['employee_id'],"employee" => $value['Name'], "department" => $value['department_name'] ];
		// 				$arr_x[$value['employee_id']][] = $value['date'];

		// 				// if($value['employee_id'] == '2017006' && $value['date'] == '2019-03-26'){
		// 				// 	return [$value];
		// 				// }
		// 			}

		// 			 $dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];

		// 			$arr = [];

		// 			//manipulation  shift code
		// 			foreach ($arr_x as $key => $value) {
		// 					$count = count($arr_x[$key])-1;
		// 					for($i = 0; $i <= $count; $i++){
		// 						$index = (integer)substr($arr_x[$key][$i], 8, 2);
		// 						$j = $dictionary[$index-1];
		// 						$monthx = (integer)substr($data[$key][$i]['date'], 5, 2);
		// 						$yearx = (integer)substr($data[$key][$i]['date'], 0, 4);
		// 						$date_sisx[$key][] = $index;
		// 						$dx=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
		// 						$data_tanggal[$key][$j]= $data[$key][$i]['shift_code'];
		// 					}
		// 			}

		// 			//end manipulation

		// 			$keyr = [];
		// 			$d_date = [];

		// 			// date manipulation
		// 			foreach ($data as $key => $value) {
		// 					$keyr[] = $key;
		// 					$count = count($data[$key])-1;
		// 					for($i = 0; $i <= $count; $i++){
		// 						$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
		// 						$month = substr($data[$key][$i]['date'], 5, 2);
		// 						$year = (integer)substr($data[$key][$i]['date'], 0, 4);
		// 						$jp = $dictionary[$indexp-1];
		// 						$date_sisa[$key][] = $indexp;
		// 						$d=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
		// 						$data_tanggax[$key]["date".$jp] =$data[$key][$i]['date'];
		// 						$data_tanggax[$key]["color".$jp] =$data[$key][$i]['color_shift'];
		// 					}
		// 			}

		// 			foreach ($date_sisa as $key => $value) {
		// 				for($i = 0; $i <= $d; $i++){
		// 							$df = $i+1;
		// 							$sd = strlen($df);
		// 							($sd == 1 ? $sdx = "0".$df : $sdx = $df);
		// 							$data_tanggax[$key]["date".$dictionary[$i]] = date($year)."-".date($month)."-".date($sdx);
		// 				}
		// 			}
		// 			//end date manipulation

		// 			foreach ($data as $key => $value) {
		// 					$count = count($data[$key])-1;

		// 					for($i = 0; $i <= $count; $i++){
		// 						$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
		// 						$jp = $dictionary[$indexp-1];
		// 						$data_tang[$key]= [$data[$key][$i]['employee'],$data[$key][$i]['employee_id'],$data[$key][$i]['department']];
		// 					}
		// 			}

		// 			foreach ($data_tanggax as $key => $value) {
		// 				$data_er[] = array_merge_recursive($value,$data_tanggal[$key],["employee" => $data_tang[$key][0]],["employee_id" => $data_tang[$key][1]],["department" => $data_tang[$key][2]]) ;
		// 			}

		// 			foreach ($data_er as $key => $value) {
		// 						$data_r[] =array_merge_recursive($value,["name" => "Employee"],["dep" => "Department"]);
		// 			}
		// 			return $unapproved->index($data_r, $access[3]);

		// 		}
		// 	}else{
		// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
		// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		// 	}

		// }



}
