<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Attendance\Schedule\unapproved_schedule_list;
use Larasite\Model\Attendance\Request\ScheduleRequestList_model as requestList;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
class UnapprovedSchedule_Ctrl extends Controller {

	protected $form = 56;

		public function index_schedulelist(){
		 	 /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
       		 	if($access[1] == 200){
							$model = new requestList;
	       		 	$unapproved = new unapproved_schedule_list;
	       		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					return $unapproved->getMessage('Unauthorized',500,[], $access[3]);
				}else{
					$unapproved = new unapproved_schedule_list;
					$input = \Input::get("key");
					$explode = explode("-",base64_decode($input));
					$empID = $explode[1];
					$hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$empID' AND t2.role_name = 'hr'");
					$adminex = \DB::SELECT("select role.role_name from ldap,role where ldap.employee_id=$empID and ldap.role_id=role.role_id ");	
					// if($hr != null){
					// 	$data = \DB::SELECT("select name,id from department where id<>1 ");
					// }elseif($adminex[0]->role_name == "adminEx"){
					$default = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");
					if(!isset($default[0])){
						$check_default = []; 
					}else{
						$check_default = $default[0];
					}
					// }elseif ($adminex[0]->role_name == "SuperUser") {
					$data = \DB::SELECT("select department.id,department.name from department where id <>1 ");
					// }elseif($adminex[0]->role_name == "admin"){
					// 	$data = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");	
					// }elseif($adminex[0]->role_name == "user"){
					// 	$data = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");	
					// }elseif($adminex[0]->role_name == "adminIt"){
					// 	$data = \DB::SELECT("select department.id,department.name from emp,department where emp.employee_id='$empID' and emp.department=department.id ");	
					// }else{
					// 	$data = null;
					// }
					//$data  = $model->departmentList($empID);
					$date = \DB::SELECT("select date from att_schedule where status=2");

					foreach($date as $value){
						$data_year = $value->date;
						$year = substr($data_year,0,4);
						   $datax[$value->date][]= $value->date;
					}

					if($date != null){
								foreach($datax as $key => $value){
									$datex[] = substr($key,0,4);
								}

								foreach($datex as $value){
									$date_datex[$value] =$value;
								}

								$unique = array_unique($date_datex);

								foreach ($unique as $key => $value) {
									$test[] = ["year" => $value];
								}
					}else{
								$test = null;
					}

					$record = ["default" => $check_default, "department" => $data, "year" => $test];
					return $unapproved->getMessage("success",200,$record, $access[3]);
				}
			}else{
				$message = $access[0]; $status = $access[1]; $data=$access[2];
				return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
			}
		}

		public function search_schedule_list(){
		  /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
			if($access[1] == 200){
	       		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$data = []; $message = 'Unauthorized'; $status = 200;
				}else{
					$unapproved = new unapproved_schedule_list;
					$input = \Input::all();
					$department = \Input::get('Department');
					$year = \Input::get('years');
					$month  = \Input::get('month')+1;

					$rule = [ 'Department' => 'required|integer', 'years' => 'required|integer', 'month' => 'required|integer' ];
					$validation = \Validator::make($input, $rule);
					//$search = \DB::SELECT("call View_ScheduleList($department,'$year','$month')");	
				
					$search = \DB::SELECT("SELECT distinct t1.employee_id, concat(t1.first_name,' ',IFNULL(t1.middle_name,''), IFNULL(t1.last_name,'')) as employee, t3.date, t2.shift_code,t5.name as department
							  FROM emp t1
							  LEFT JOIN department t5 ON t5.id = t1.department
							  LEFT JOIN att_schedule t3 ON t3.employee_id = t1.employee_id
							  LEFT JOIN attendance_work_shifts t2 ON t2.shift_id = t3.shift_id
							  WHERE t1.department = $department
							  AND YEAR( t3.DATE ) =  '$year'
						     AND month( t3.DATE ) = '$month'
							  AND t3.status = 2");
					
					if($search == null){
						return $unapproved->getMessage("Data not exist",200,[], $access[3]);
					}
					$data = [];
					$arr_x=[];
					foreach ($search as $key => $value){
						$data[$value->employee_id][] = ["date" =>$value->date,"shift_code" => $value->shift_code,"employee_id" =>$value->employee_id,"employee" => $value->employee, "department" => $value->department ];
						$arr_x[$value->employee_id][] = $value->date;
					}

					 $dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];

					$arr = [];

					//manipulation  shift code
					foreach ($arr_x as $key => $value) {
							$count = count($arr_x[$key])-1;
							for($i = 0; $i <= $count; $i++){
								$index = (integer)substr($arr_x[$key][$i], 8, 2);
								$j = $dictionary[$index-1];
								$monthx = (integer)substr($data[$key][$i]['date'], 5, 2);
								$yearx = (integer)substr($data[$key][$i]['date'], 0, 4);
								$date_sisx[$key][] = $index;
								$dx=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
								$data_tanggal[$key][$j]= $data[$key][$i]['shift_code'];
							}
					}

					//end manipulation

					$keyr = [];
					$d_date = [];

					// date manipulation
					foreach ($data as $key => $value) {
							$keyr[] = $key;
							$count = count($data[$key])-1;
							for($i = 0; $i <= $count; $i++){
							$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
							$month = substr($data[$key][$i]['date'], 5, 2);
							$year = (integer)substr($data[$key][$i]['date'], 0, 4);
							$jp = $dictionary[$indexp-1];
							$date_sisa[$key][] = $indexp;
							$d=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
							$data_tanggax[$key]["date".$jp] =$data[$key][$i]['date'];
							}
					}

					foreach ($date_sisa as $key => $value) {
						for($i = 0; $i <= $d; $i++){
									$df = $i+1;
									$sd = strlen($df);
									($sd == 1 ? $sdx = "0".$df : $sdx = $df);
									$data_tanggax[$key]["date".$dictionary[$i]] = date($year)."-".date($month)."-".date($sdx);
						}
					}
					//end date manipulation

					foreach ($data as $key => $value) {
							$count = count($data[$key])-1;

							for($i = 0; $i <= $count; $i++){
							$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
								$jp = $dictionary[$indexp-1];
								$data_tang[$key]= [$data[$key][$i]['employee'],$data[$key][$i]['employee_id'],$data[$key][$i]['department']];
							}
					}

				foreach ($data_tanggax as $key => $value) {
					$data_er[] = array_merge_recursive($value,$data_tanggal[$key],["employee" => $data_tang[$key][0]],["employee_id" => $data_tang[$key][1]],["department" => $data_tang[$key][2]]) ;
				}

				foreach ($data_er as $key => $value) {
							$data_r[] =array_merge_recursive($value,["name" => "Employee"],["dep" => "Department"]);
				}
				return $unapproved->index($data_r, $access[3]);

			}
			}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
			}

		}

		public function search_schedule(){
		  /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
			if($access[1] == 200){
	       		 	if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$data = []; $message = 'Unauthorized'; $status = 200;
				}else{
					$unapproved = new unapproved_schedule_list;
					$input = \Input::all();
					$department = \Input::get('Department');
					$year = \Input::get('years');
					$month  = \Input::get('month')+1;

					$rule = [ 'Department' => 'required|integer', 'years' => 'required|integer', 'month' => 'required|integer' ];
					$validation = \Validator::make($input, $rule);
					//$search = \DB::SELECT("call View_ScheduleList($department,'$year','$month')");	
				
					$search = \DB::SELECT("SELECT distinct t1.employee_id, concat(t1.first_name,' ',IFNULL(t1.middle_name,''), IFNULL(t1.last_name,'')) as employee, t3.date, t2.shift_code,t5.name as department
							  FROM emp t1
							  LEFT JOIN department t5 ON t5.id = t1.department
							  LEFT JOIN att_schedule t3 ON t3.employee_id = t1.employee_id
							  LEFT JOIN attendance_work_shifts t2 ON t2.shift_id = t3.shift_id
							  WHERE t1.department = $department
							  AND YEAR( t3.DATE ) =  '$year'
						     AND month( t3.DATE ) = '$month'
							  AND t3.status = 2");
					
					if($search == null){
						return $unapproved->getMessage("Data not exist",200,[], $access[3]);
					}
					$data = [];
					$arr_x=[];
					foreach ($search as $key => $value){
						$data[$value->employee_id][] = ["date" =>$value->date,"shift_code" => $value->shift_code,"employee_id" =>$value->employee_id,"employee" => $value->employee, "department" => $value->department ];
						$arr_x[$value->employee_id][] = $value->date;
					}

					 $dictionary = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","aa","ab","ac","ad","ae","af","ag","ah"];

					$arr = [];

					//manipulation  shift code
					foreach ($arr_x as $key => $value) {
							$count = count($arr_x[$key])-1;
							for($i = 0; $i <= $count; $i++){
								$index = (integer)substr($arr_x[$key][$i], 8, 2);
								$j = $dictionary[$index-1];
								$monthx = (integer)substr($data[$key][$i]['date'], 5, 2);
								$yearx = (integer)substr($data[$key][$i]['date'], 0, 4);
								$date_sisx[$key][] = $index;
								$dx=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
								$data_tanggal[$key][$j]= $data[$key][$i]['shift_code'];
							}
					}

					//end manipulation

					$keyr = [];
					$d_date = [];

					// date manipulation
					foreach ($data as $key => $value) {
							$keyr[] = $key;
							$count = count($data[$key])-1;
							for($i = 0; $i <= $count; $i++){
							$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
							$month = substr($data[$key][$i]['date'], 5, 2);
							$year = (integer)substr($data[$key][$i]['date'], 0, 4);
							$jp = $dictionary[$indexp-1];
							$date_sisa[$key][] = $indexp;
							$d=cal_days_in_month(CAL_GREGORIAN,date($month),date($year));
							$data_tanggax[$key]["date".$jp] =$data[$key][$i]['date'];
							}
					}

					foreach ($date_sisa as $key => $value) {
						for($i = 0; $i <= $d; $i++){
									$df = $i+1;
									$sd = strlen($df);
									($sd == 1 ? $sdx = "0".$df : $sdx = $df);
									$data_tanggax[$key]["date".$dictionary[$i]] = date($year)."-".date($month)."-".date($sdx);
						}
					}
					//end date manipulation

					foreach ($data as $key => $value) {
							$count = count($data[$key])-1;

							for($i = 0; $i <= $count; $i++){
							$indexp = (integer)substr($data[$key][$i]['date'], 8, 2);
								$jp = $dictionary[$indexp-1];
								$data_tang[$key]= [$data[$key][$i]['employee'],$data[$key][$i]['employee_id'],$data[$key][$i]['department']];
							}
					}

				foreach ($data_tanggax as $key => $value) {
					$data_er[] = array_merge_recursive($value,$data_tanggal[$key],["employee" => $data_tang[$key][0]],["employee_id" => $data_tang[$key][1]],["department" => $data_tang[$key][2]]) ;
				}

				foreach ($data_er as $key => $value) {
							$data_r[] =array_merge_recursive($value,["name" => "Employee"],["dep" => "Department"]);
				}
				return $unapproved->index($data_r, $access[3]);

			}
			}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
			}

		}



}
