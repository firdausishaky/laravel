<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Exeception;

class notification extends Controller {

	public function getUser($key = null){
		if($key == null){
			$key = \Input::get('key');
		}
		$keys= base64_decode($key);
		$test = explode('-',$keys);
		return $data = $test[1];
	}

	public function insert_notif($id,$type,$employee_id,$local,$typeUser,$typeRequest){
		$check = $this->notif($id,$type,$employee_id,$local,$typeUser,$typeRequest);	
		if($check == "success"){
			$data = [];
			$message = "success";
			$status = 200;
		}else{
			$data = [];
			$message = $check;
			$status = 200;
		}

		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, ],$status);
	}

	public function read_notif(){

		$i  =  \Input::all();
		$id_req   =  $i['data']['id_req'];
		
		if(!isset($i['data']['id_pool'])){
			$update  = \DB::SELECT("update att_schedule_request set read_stat = 'r'  where id  =  '$id_req' ");
			return response()->json([]);	
		}
	
		$id_pool =  $i['data']['id_pool'];

		$requestor  = $i['whoiam'];
		$get_data  = \DB::SELECT("select * from pool_request where id = $id_pool");
		$type_request  = $get_data[0]->type_request;	
		$emp =  $get_data[0]->employee_id;
		$json  =  $get_data[0]->json_data;


		$lc = \DB::SELECT("select local_it from emp where employee_id = '$emp'");
		if($type_request == 'time'  &&  $lc[0]->local_it != 1){
			$schema = ['hr','sup','emp'];  
		}elseif ($type_request == 'time'  &&  $lc[0]->local_it == 1) {
			$schema = ['sup','hr','emp'];  
		}elseif ($type_request == 'over'  &&  $lc[0]->local_it != 1) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'under'  &&  $lc[0]->local_it != 1) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'late'  &&  $lc[0]->local_it != 1) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'early'  &&  $lc[0]->local_it != 1) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'train'  ) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'ob') {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'change') {
			$emp = \DB::SELECT("select * from emp_supervisor where supervisor =  '$emp'");
			if($emp != null){
				$schema = ['emp','hr','sup'];
			}else{
				$schema = ['sup','emp','hr'];
			}
		}elseif ($type_request == 'vacation'  &&  $lc[0]->local_it != 1){
			$schema = ['hr','sup','hr'];
		}elseif ($type_request == 'ADO'  &&  $lc[0]->local_it == 1) {
			$schema = ['hr','sup','emp'];
		}elseif ($type_request == 'vacation'  &&  $lc[0]->local_it == 1) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'enhance'  &&  $lc[0]->local_it == 1) {
			$schema = ['hr','sup','emp'];
		}elseif ($type_request == 'birth'  &&  $lc[0]->local_it != 1) {
			$schema = ['hr','sup','emp'];
		}elseif ($type_request == 'swap'  &&  $lc[0]->local_it != 1) {
			$schema = ['emp','sup','hr'];
		}elseif ($type_request == 'sick' ) {
			$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
			if($hr_cek != null){
				if($lc[0]->local_it == 1){
					$schema = ['sup','hr','emp'];
				}
			}else{
				if($lc[0]->local_it == 1){
					$schema = ['sup','hr','emp'];
				}else{
					$schema = ['hr','sup','emp'];
				}
			}		
		}elseif ($type_request == 'maternity' ) {
			$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
			if($hr_cek != null){
				if($lc[0]->local_it != 1){
					$schema = ['hr','sup','emp'];
				}
			}else{
				if($lc[0]->local_it == 1){
					$schema = ['hr','sup','emp'];
				}
			}
		}elseif ($type_request == 'paternity'  &&  $lc[0]->local_it != 1) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'paternity'  &&  $lc[0]->local_it == 1) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'bereavement') {
			$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
			if($hr_cek != null){
				if($lc[0]->local_it != 1){
					$schema = ['sup','hr','emp'];
				}
				}else{
					$schema = ['sup','hr','emp'];
				}
			}
		}elseif ($type_request == 'marriage') {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'emmergency'  ) {
			$schema = ['sup','hr','emp'];
		}else{
			$schema = ['sup','hr','emp'];
		}
		$json = json_decode($json,1);


		foreach ($schema as $key => $value) {
			
			if($type_request == "late"){
				//$schema = ['sup','hr','emp'];
				if(isset($json['app_sup']) and $json['app_sup'] == null){
					$count =  count($json['sup_stat']);
					for($i = 0; $i < $count; $i++){
						if(isset($json['sup_stat'][$i][$requestor])){
							$json['sup_stat'][$i][$requestor] = 'r';
						}
					}
				}else{
					if(isset($json['app_hr']) and $json['app_hr'] != null ){
						if(isset($json['hr_stat'])){
							$count =  count($json['hr_stat']);
							for($i = 0; $i < $count; $i++){
								if(isset($json['hr_stat'][$i][$requestor])){
									$json['hr_stat'][$i][$requestor] = 'r';
									
								}
							}
						}
					}
				}

			}elseif ($type_request == "swap") {
				//$schema = ['emp','sup','hr'];
				if(isset($json['app_emp']) and $json['app_emp'] == null){
					$count =  count($json['swap_stat']);
					for($i = 0; $i < $count; $i++){
						if(isset($json['swap_stat'][$i][$requestor])){
							$json['swap_stat'][$i][$requestor] = 'r';

							//return $json;
						}
					}
				}else{
					if(isset($json['app_sup']) and $json['app_sup'] != null ){
						if(isset($json['sup_stat'])){
							$count =  count($json['sup_stat']);
							for($i = 0; $i < $count; $i++){
								if(isset($json['sup_stat'][$i][$requestor])){
									$json['sup_stat'][$i][$requestor] = 'r';
									
								}
							}
						}
					}
				}
			}else{
				
				if($value  == 'hr'){
					if(isset($json['hr_stat'])){
						$count =  count($json['hr_stat']);
						for($i = 0; $i < $count; $i++){
							if(isset($json['hr_stat'][$i][$requestor])){
								$json['hr_stat'][$i][$requestor] = 'r';
								
							}
						}
					}
				}
				if($value  == 'sup'){
					if(isset($json['sup_stat'])){
						$count =  count($json['sup_stat']);
						for($i = 0; $i < $count; $i++){
							if(isset($json['sup_stat'][$i][$requestor])){
								$json['sup_stat'][$i][$requestor] = 'r';
								
							}
						}
					}
				}
				if($value  == 'emp'){
					if(isset($json['emp_stat'])){
						$count =  count($json['emp_stat']);
						for($i = 0; $i < $count; $i++){
							if(isset($json['emp_stat'][$i][$requestor])){
								$json['emp_stat'][$i][$requestor] = 'r';
								
							}
						}
					}
				}	
			}

			

		} 	
		$json = json_encode($json);
		//return "update pool_request set json_data   = '$json' where id  =  $id_req "; 
		$update  = \DB::SELECT("update pool_request set json_data   = '$json' where id  =  $id_pool ");
		
		return response()->json([]);	
	}

	public function notif($ids,$type,$employee_id,$local_it,$user,$from_type){

		//user =  hr /supervisor ? regular employee no needed it

		$hr = \DB::SELECT("select employee_id from  ldap, role where ldap.role_id = role.role_id and (lower(role.role_name) like '%human%' or  '%human resource%' or '%hr%')");

		$hrx = [];
		foreach($hr as $key => $value){
			$hrx[] =  $value->employee_id;
		}
		$hrx[]  = '2014888';

		$sub = \DB::SELECT("select subordinate from emp_subordinate where employee_id = '$employee_id' ");
		
		$subx = [];
		foreach($sub as $key => $value){
			$subx[] =  $value->subordinate; 
		}

		$subx =  '2014888';
		$sup = \DB::SELECT("select supervisor from emp_supervisor where employee_id = '$employee_id' ");
		
		$supx = [];
		foreach($sup as $key => $value){
			$supx[] =  $value->supervisor; 
		}
		$supx[] = '2014888';
		$swap = \DB::SELECT("select swap_with from att_swap_shift where employee_id = '$employee_id' and swap_id = (select request_id from att_schedule_request where employee_id='$employee_id' and id = $ids and type_id = 6 order by id desc limit 1 )  ");
		
		$swapx = [];
		foreach($swap as $key => $value){
			$swapx[] =  $value->swap_with; 
		}

		$swapx[] = '2014888';

		if($type == 'time'){
			if($local_it == 'local'){
				$arr = [
							"hr" => $hrx,
							"sup" => [],
							"emp" => [],
							"app_hr" => [],
							"app_sup" => [],
							"app_emp" => [],
					   ];
			}else{
				$arr = [
							"sup" => $supx,
							"hr" => [],
							"emp" => [],
							"app_sup" => [],
							"app_hr" => [],
							"app_emp" => [],			
					   ];
			}
		}
		if($type == 'over'){
			if($local_it == 'local'){
				$arr = [
					"sup" => $supx,
					"hr" => [],
					"emp" => [],
					"app_sup" => [],
					"app_hr" => [],
					"app_emp" => [],			
					];
			} 
		}
		if($type == 'under'){
			if($local_it == 'local'){
				$arr = [
					"sup" => $supx,
					"hr" => [],
					"emp" => [],
					"app_sup" => [],
					"app_hr" => [],
					"app_emp" => [],			
					]; 
			}
		}
		if($type == 'late' || $type == 'early'){
			if($local_it == 'local'){	
				$arr = [
					"sup" => $supx,
					"hr" => $hrx,
					"emp" => [],
					"app_sup" => [],
					"app_hr" => [],
					"app_emp" => [],			
					]; 
			}
		}
		if($type == 'ob'){
			$arr = [
					"sup" => $supx,
					"hr" => [],
					"emp" => [],			
					"app_sup" => [],
					"app_hr" => [],
					"app_emp" => [],
					]; 
		}

		if($type == 'train'){
			$arr = [
					"sup" => $supx,
					"hr" => [],
					"emp" => [],			
					"app_sup" => [],
					"app_hr" => [],
					"app_emp" => [],
					]; 
		}
		if($type == 'change'){
			if($user == "sup"){
				$arr = [
						"emp" => $subx,			
						"sup" => [],
						"hr" => [],
						"app_emp" => [],
						"app_hr" => [],
						"app_sup" => [],
					   ]; 
			}else{
				$arr = [
					"sup" => $supx,
					"hr" => [],
					"emp" => [],
					"app_sup" => [],
					"app_hr" => [],
					"app_emp" => [],			
					];
			}
		}

		if($type == "swap"){
			if($local_it == 'local'){
				$arr = [
						"emp" => $swapx,			
						"sup" => $supx,
						"hr" => [],
						"app_emp" => [],
						"app_hr" => [],
						"app_sup" => [],
					   ]; 
	
			}
		}

		//leave request 

		if($type == "vacation"){
			if($local_it == 'local'){
				$arr = [
						"hr" => $hrx,
						"sup" => $supx,
						"emp" => [],			
						"app_emp" => [],
						"app_hr" => [],
						"app_sup" => [],
					   ]; 
			}else{
				$arr = [
						"sup" => $supx,
						"emp" => [],			
						"hr" => [],
						"app_emp" => [],
						"app_hr" => [],
						"app_sup" => [],
					   ];
			}
		}

		if($type == 'ADO'){
			$arr = [
				"hr" => $hrx,
				"sup" => $supx,
				"emp" => [],			
				"app_emp" => [],
				"app_hr" => [],
				"app_sup" => [],
			   ];
		}

		if($type == 'enhance'){
			if($local_it == 'local'){
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"emp" => [],			
					"app_emp" => [],
					"app_hr" => [],
					"app_sup" => [],
				   ];
			}	
		}

		if($type == 'birth'){
			if($local_it == 'local'){
				$arr = [
					"hr" => $hrx,
					"sup" => [],
					"emp" => [],			
					"app_emp" => [],
					"app_hr" => [],
					"app_sup" => [],
				   ];
			}
		}

		if($user == "hr" && $type == 'sick'){
			if($local_it == 'local'){
				$arr = [
					"hr" => $hrx,
					"sup" => [],
					"emp" => [],			
					"app_emp" => [],
					"app_hr" => [],
					"app_sup" => [],
				   ];
			}
		}else{
			if($type == 'sick' && $local_it == 'local'){
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"emp" => [],			
					"app_emp" => [],
					"app_hr" => [],
					"app_sup" => [],
				   ];	
			}
			if($type == 'sick' && $local_it != 'local'){
				$arr = [
					"sup" => $supx,
					"hr" => [],
					"emp" => [],			
					"app_emp" => [],
					"app_hr" => [],
					"app_sup" => [],
				   ];
			}
		}

		if($type ==  'maternity'){
			if($user == 'hr'){
				if($local_it == 'local'){
					$arr = [
					"hr" => $hrx,
					"sup" => [],
					"emp" => [],			
					"app_emp" => [],
					"app_hr" => [],
					"app_sup" => [],
				   ];		
				}	
			}else{
				if($local_it == 'expat'){
					$arr = [
					"sup" => $supx,
					"hr" => [],
					"emp" => [],			
					"app_emp" => [],
					"app_hr" => [],
					"app_sup" => [],
				   ];		
				}	
			}
		}

		if($type == 'paternity'){
			if($local_it == 'local'){
					$arr = [
					"sup" => $supx,
					"hr" => $hrx,
					"emp" => [],			
					"app_emp" => [],
					"app_hr" => [],
					"app_sup" => [],
				   ];		
			}else{
				$arr = [
					"sup" => $supx,
					"hr" => [],
					"emp" => [],			
					"app_emp" => [],
					"app_hr" => [],
					"app_sup" => [],
				   ];
			}
		}

		if($type == 'bereavement'){
			if($user == 'hr'){
				if($local_it == 'local'){
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],			
						"app_emp" => [],
						"app_hr" => [],
						"app_sup" => [],
				   ];		
				}
			}
			else{
				if($local_it == 'local'){
					$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"emp" => [],			
						"app_emp" => [],
						"app_hr" => [],
						"app_sup" => [],
				   ];		
				}else{
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],			
						"app_emp" => [],
						"app_hr" => [],
						"app_sup" => [],
				   ];
				}
			}
		}

		if($type == 'marriage'){
			if($local_it == 'local'){
				$arr = [
					"sup" => $supx,
					"hr" => $hrx,
					"emp" => [],			
					"app_emp" => [],
					"app_hr" => [],
					"app_sup" => [],
			   ];		
			}else{
				$arr = [
					"sup" => $supx,
					"hr" => [],
					"emp" => [],			
					"app_emp" => [],
					"app_hr" => [],
					"app_sup" => [],
			   ];
			}
		}

		if($type == 'emmergency'){
			if($user == 'hr'){
				if($local_it == 'local'){
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],			
						"app_emp" => [],
						"app_hr" => [],
						"app_sup" => [],
				   ];		
				}
			}
			else{
				if($local_it == 'local'){
					$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"emp" => [],			
						"app_emp" => [],
						"app_hr" => [],
						"app_sup" => [],
				   ];		
				}else{
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],			
						"app_emp" => [],
						"app_hr" => [],
						"app_sup" => [],
				   ];
				}
			}
		}

		if($type == 'suspension'){
			if($user == 'hr'){
				if($local_it == 'local'){
					$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"emp" => [],			
						"app_emp" => [],
						"app_hr" => [],
						"app_sup" => [],
				   ];		
				}
			}
		}
		
		if(isset($arr['hr']) && $arr['hr'] != null){
			foreach ($hr as $key => $value) {
				$hr_stat[] = [ $value->employee_id => 'u'];
			}
			$hr_stat[] = [ '2014888' => 'u'];
			$arr['hr_stat'] = $hr_stat;
		}

		if(isset($arr['sup']) && $arr['sup'] != null){
			foreach ($sup as $key => $value) {
				$sup_stat[] = [ $value->supervisor => 'u'];
			}
			$sup_stat[] = [ '2014888' => 'u'];
			$arr['sup_stat'] = $sup_stat;
		}

		if(isset($arr['sub']) && $arr['sub'] != null){
			foreach ($sub as $key => $value) {
				$sub_stat[] = [ $value->subordinate => 'u'];
			}

			$sub_stat[] = [ "2014888" => 'u'];
			$arr['sub_stat'] = $sub_stat;
		}

		if(isset($arr['emp']) && $arr['emp'] != null){
			foreach ($swap as $key => $value) {
				$swap_stat[] = [ $value->swap_with => 'u'];
			}
			$swap_stat[] = [ '2014888' => 'u'];
			$arr['swap_stat'] = $swap_stat;
		}
		//print_r($arr);
		$json = json_encode($arr);
		try{
			$insert = \DB::SELECT("insert into pool_request(employee_id,id_req,type_request,json_data,created_at) values('$employee_id',$ids,'$type','$json',now())");
			return "success";
		}catch(\Exeception $e){
			return $e;
		}
	}

	public function count_notif(){
		$key = \Input::get('key');
		$arr =  $this->getUser($key);
		$ping = 0;
     	$select = \DB::SELECT("select * from pool_request where DATE_SUB(now(),INTERVAL 7 DAY)");				
		$count_select = count($select);
		//for($j = 0; $j <= $count_select; $j++){

		//dictionary  

		$att  = ['time','over','under','late','early','ob','train','change','swap']; 
		$leav = ['vacation','ADO','enhnace','birth','sick','maternity','paternity','bereavement','marriage','emmergency','suspension'];

		$request = [];
		foreach ($select as $key => $value) {
			//$check_already_execute = $select[$key]->id_req;
			//$select_data =  \DB::SELECT("select * from att_schedule_request where  id = $check_already_execute");
			$emp  =  $value->employee_id;

			// if($select_data[0]->status_id != 1){
			// 	if($arr ==  $emp){

			// 	}	
			// }	
			$lc = \DB::SELECT("select local_it from emp where employee_id = '$emp'");
			if($value->type_request == 'time'  &&  $lc[0]->local_it != 1){
				$schema = ['hr','sup','emp'];  
			}elseif ($value->type_request == 'time'  &&  $lc[0]->local_it == 1) {
				$schema = ['sup','hr','emp'];  
			}elseif ($value->type_request == 'over'  &&  $lc[0]->local_it != 1) {
				$schema = ['sup','hr','emp'];
			}elseif ($value->type_request == 'under'  &&  $lc[0]->local_it != 1) {
				$schema = ['sup','hr','emp'];
			}elseif ($value->type_request == 'late'  &&  $lc[0]->local_it != 1) {
				$schema = ['sup','hr','emp'];
			}elseif ($value->type_request == 'early'  &&  $lc[0]->local_it != 1) {
				$schema = ['sup','hr','emp'];
			}elseif ($value->type_request == 'train'  ) {
				$schema = ['sup','hr','emp'];
			}elseif ($value->type_request == 'ob') {
				$schema = ['sup','hr','emp'];
			}elseif ($value->type_request == 'change') {
				$emp = \DB::SELECT("select * from emp_supervisor where supervisor =  '$emp'");
				if($emp != null){
					$schema = ['emp','hr','sup'];
				}else{
					$schema = ['sup','emp','hr'];
				}
			}elseif ($value->type_request == 'vacation'  &&  $lc[0]->local_it != 1){
				$schema = ['hr','sup','hr'];
			}elseif ($value->type_request == 'ADO'  &&  $lc[0]->local_it == 1) {
				$schema = ['hr','sup','emp'];
			}elseif ($value->type_request == 'vacation'  &&  $lc[0]->local_it == 1) {
				$schema = ['sup','hr','emp'];
			}elseif ($value->type_request == 'enhance'  &&  $lc[0]->local_it == 1) {
				$schema = ['hr','sup','emp'];
			}elseif ($value->type_request == 'birth'  &&  $lc[0]->local_it != 1) {
				$schema = ['hr','sup','emp'];
			}elseif ($value->type_request == 'swap'  &&  $lc[0]->local_it != 1) {
				$schema = ['emp','sup','hr'];
			}elseif ($value->type_request == 'sick' ) {
				$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
				if($hr_cek != null){
					if($lc[0]->local_it == 1){
						$schema = ['sup','hr','emp'];
					}
				}else{
					if($lc[0]->local_it == 1){
						$schema = ['sup','hr','emp'];
					}else{
						$schema = ['hr','sup','emp'];
					}
				}		
			}elseif ($value->type_request == 'maternity' ) {
				$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
				if($hr_cek != null){
					if($lc[0]->local_it != 1){
						$schema = ['hr','sup','emp'];
					}
				}else{
					if($lc[0]->local_it == 1){
						$schema = ['hr','sup','emp'];
					}
				}
			}elseif ($value->type_request == 'paternity'  &&  $lc[0]->local_it != 1) {
				$schema = ['sup','hr','emp'];
			}elseif ($value->type_request == 'paternity'  &&  $lc[0]->local_it == 1) {
				$schema = ['sup','hr','emp'];
			}elseif ($value->type_request == 'bereavement') {
				$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
				if($hr_cek != null){
					if($lc[0]->local_it != 1){
						$schema = ['sup','hr','emp'];
					}
				}else{
					if($lc[0]->local_it == 1){
						$schema = ['sup','hr','emp'];
					}else{
						$schema = ['sup','hr','emp'];
					}
				}
			}elseif ($value->type_request == 'marriage') {
				$schema = ['sup','hr','emp'];
			}elseif ($value->type_request == 'emmergency'  ) {
				$schema = ['sup','hr','emp'];
			}else{
				$schema = ['sup','hr','emp'];
			}

			$js =  json_decode($value->json_data);

			//if first step hr
			if($schema[0] == 'hr'){


			 	if($js->hr != null){
			 		if($js->app_hr == null){
			 		
			 			$pinghr_count = count($js->hr);
			 			for($k= 0; $k < $pinghr_count; $k++){
			 				if(isset($js->hr[$k])){
			 					if($js->hr[$k] == $arr){
			 				    	$ping += 1;	
			 				    	$status  = 'pending';
			 				    	$read =  true;
			 				    	$idx = $value->id_req;
			 						$request[] = $this->sql_query($idx,$arr);		 					
				 				}else{	
				 					$ping += 0;
				 					$read = false;
				 				}		
		 					}
			 			}
			 		}else{

			 			if($emp == $arr){
			 			
			 			$check_id_pool =  $value->id_req;
			 			$check_id_request = \DB::SELECT("select * from  att_schedule_request,emp,att_type   where id = $check_id_pool and emp.employee_id = att_schedule_request.employee_id and att_type.type_id  =  att_schedule_request.type_id");
			 			$appover_id  = $check_id_request[0]->approver;  
			 			$approver_name  = \DB::SELECT("select * from emp  where employee_id = '$appover_id' ");
				 			if(isset($check_id_request)){
				 				
				 				$checked =  $check_id_request[0]->status_id;
				 				if($checked ==  2){
				 					if($emp == $arr){
				 						$ping += 1;	
				 				     	$read =  true;
				 				   	 	$request[] = [
				 				   	 					'created_at'  => $check_id_request[0]->update_at,
				 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
				 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
				 				   	 				 	'read' => 'u',
				 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
				 				   	 				 	'id_req' => $check_id_request[0]->id,
				 				   	 				 	'status' =>  'Approved',
				 				   	 				 	'types' => $check_id_request[0]->type,
				 				   	 				 	'approve_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
				 				   	 				 ];
				 					}
				 				}else{
				 					if($emp == $arr){
				 						$ping += 1;	
				 				     	$read =  true;
				 				   	 	$request[] = [
				 				   	 					'created_at'  => $check_id_request[0]->update_at,
				 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
				 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
				 				   	 				 	'read' => 'u',
				 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
				 				   	 				 	'id_req' => $check_id_request[0]->id,
				 				   	 				 	'status' =>  'Approved',
				 				   	 				 	'types' => $check_id_request[0]->type,
				 				   	 				 	'rejected_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
				 				   	 				 ];
				 					}
				 				}		
				 			}
			 			}

						if($js->sup != null ){
			 				if($js->app_sup == null ){
			 					if($js->app_hr != null && $js->app_hr[1]  != 2){
			 						$ping += 1;
				 					$idx = $value->id_req;
			 						$request[] = $this->sql_query($idx,$arr);
			 					}else{
			 						$ping += 0;
			 					} 		
						 	}else{
						 		if($emp == $arr){
		 			
						 		$check_id_pool =  $value->id_req;
						 		$check_id_request = \DB::SELECT("select * from  att_schedule_request,emp,att_type   where id = $check_id_pool and emp.employee_id = att_schedule_request.employee_id and att_type.type_id  =  att_schedule_request.type_id");
						 		$appover_id  = $check_id_request[0]->approver;  
						 		$approver_name  = \DB::SELECT("select * from emp  where employee_id = '$appover_id' ");
						 			if(isset($check_id_request)){
						 				
						 				$checked =  $check_id_request[0]->status_id;
						 				if($checked ==  2){
						 					if($emp == $arr){
						 						$ping += 1;	
						 				     	$read =  true;
						 				   	 	$request[] = [
						 				   	 					'created_at'  => $check_id_request[0]->update_at,
						 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
						 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
						 				   	 				 	'read' => 'u',
						 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
						 				   	 				 	'id_req' => $check_id_request[0]->id,
						 				   	 				 	'status' =>  'Approved',
						 				   	 				 	'types' => $check_id_request[0]->type,
						 				   	 				 	'approve_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
						 				   	 				 ];
						 					}
						 				}else{
						 					if($emp == $arr){
						 						$ping += 1;	
						 				     	$read =  true;
						 				   	 	$request[] = [
						 				   	 					'created_at'  => $check_id_request[0]->update_at,
						 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
						 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
						 				   	 				 	'read' => 'u',
						 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
						 				   	 				 	'id_req' => $check_id_request[0]->id,
						 				   	 				 	'status' =>  'Approved',
						 				   	 				 	'types' => $check_id_request[0]->type,
						 				   	 				 	'rejected_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
						 				   	 				 ];
						 					}
						 				}		
						 			}
				 				}
					 			if($js->emp != null and $js->emp[0] == $arr){
						 			if($js->app_emp  == null ){
							 			if(isset($js->emp_stat[0]->$arr) and $js->emp_stat[0]->$arr == 'u'){
							 				if($js->app_sup != null && $js->app_sup[1] != 2){
								 				$ping += 0;
								 				$idx = $value->id_req;
			 									$request[] = $this->sql_query($idx,$arr);
						 					}else{
						 						$ping += 1;
						 					} 
							 			}
							 		}else{
							 			if($emp == $arr){
			 			
							 			$check_id_pool =  $value->id_req;
							 			$check_id_request = \DB::SELECT("select * from  att_schedule_request,emp,att_type   where id = $check_id_pool and emp.employee_id = att_schedule_request.employee_id and att_type.type_id  =  att_schedule_request.type_id");
							 			$appover_id  = $check_id_request[0]->approver;  
							 			$approver_name  = \DB::SELECT("select * from emp  where employee_id = '$appover_id' ");
								 			if(isset($check_id_request)){
								 				
								 				$checked =  $check_id_request[0]->status_id;
								 				if($checked ==  2){
								 					if($emp == $arr){
								 						$ping += 1;	
								 				     	$read =  true;
								 				   	 	$request[] = [
								 				   	 					'created_at'  => $check_id_request[0]->update_at,
								 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
								 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
								 				   	 				 	'read' => 'u',
								 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
								 				   	 				 	'id_req' => $check_id_request[0]->id,
								 				   	 				 	'status' =>  'Approved',
								 				   	 				 	'types' => $check_id_request[0]->type,
								 				   	 				 	'approve_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
								 				   	 				 ];
								 					}
								 				}else{
								 					if($emp == $arr){
								 						$ping += 1;	
								 				     	$read =  true;
								 				   	 	$request[] = [
								 				   	 					'created_at'  => $check_id_request[0]->update_at,
								 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
								 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
								 				   	 				 	'read' => 'u',
								 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
								 				   	 				 	'id_req' => $check_id_request[0]->id,
								 				   	 				 	'status' =>  'Approved',
								 				   	 				 	'types' => $check_id_request[0]->type,
								 				   	 				 	'rejected_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
								 				   	 				 ];
								 					}
								 				}		
								 			}
							 			}
							 		}
							 	}
					 		}	
			 			}else{
			 				if($js->emp != null and $js->sup[0] == $arr){
				 				if($js->app_emp == null ){
						 			if(isset($js->emp_stat[0]->$arr) and $js->emp_stat[0]->$arr == 'u'){
						 				if($js->app_sup != null && $js->app_sup[1]  != 2){
								 			$ping += 0;
						 				}else{
						 					$ping += 1;
						 					$idx = $value->id_req;
			 								$request[] = $this->sql_query($idx,$arr);
						 				} 
						 			}
						 		}else{
						 			if($emp == $arr){
			 			
						 			$check_id_pool =  $value->id_req;
						 			$check_id_request = \DB::SELECT("select * from  att_schedule_request,emp,att_type   where id = $check_id_pool and emp.employee_id = att_schedule_request.employee_id and att_type.type_id  =  att_schedule_request.type_id");
						 			$appover_id  = $check_id_request[0]->approver;  
						 			$approver_name  = \DB::SELECT("select * from emp  where employee_id = '$appover_id' ");
							 			if(isset($check_id_request)){
							 				
							 				$checked =  $check_id_request[0]->status_id;
							 				if($checked ==  2){
							 					if($emp == $arr){
							 						$ping += 1;	
							 				     	$read =  true;
							 				   	 	$request[] = [
							 				   	 					'created_at'  => $check_id_request[0]->update_at,
							 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
							 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
							 				   	 				 	'read' => 'u',
							 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
							 				   	 				 	'id_req' => $check_id_request[0]->id,
							 				   	 				 	'status' =>  'Approved',
							 				   	 				 	'types' => $check_id_request[0]->type,
							 				   	 				 	'approve_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
							 				   	 				 ];
							 					}
							 				}else{
							 					if($emp == $arr){
							 						$ping += 1;	
							 				     	$read =  true;
							 				   	 	$request[] = [
							 				   	 					'created_at'  => $check_id_request[0]->update_at,
							 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
							 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
							 				   	 				 	'read' => 'u',
							 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
							 				   	 				 	'id_req' => $check_id_request[0]->id,
							 				   	 				 	'status' =>  'Approved',
							 				   	 				 	'types' => $check_id_request[0]->type,
							 				   	 				 	'rejected_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
							 				   	 				 ];
							 					}
							 				}		
							 			}
						 			}
						 		}	
			 				}
			 			}
		 			}
				}
			}

			//if first step sup
			if($schema[0] == 'sup'){
				if($js->sup != null ){		
					if(in_array($arr, $js->sup)){

		 				if($js->app_sup == null ){
		 					$ping += 1;
		 					$status  = 'pending';
			 				$idx = $value->id_req;
		 					
		 					$request[] = $this->sql_query($idx,$arr);
					 	}else{
					 		if($emp == $arr){
				 			$check_id_pool =  $value->id_req;
				 			$check_id_request = \DB::SELECT("select * from  att_schedule_request,emp,att_type   where id = $check_id_pool and emp.employee_id = att_schedule_request.employee_id and att_type.type_id  =  att_schedule_request.type_id");
				 			$appover_id  = $check_id_request[0]->approver;  
				 			$approver_name  = \DB::SELECT("select * from emp  where employee_id = '$appover_id' ");
					 			if(isset($check_id_request)){
					 				
					 				$checked =  $check_id_request[0]->status_id;
					 				if($checked ==  2){
					 					if($emp == $arr){
					 						$ping += 1;	
					 				     	$read =  true;
					 				   	 	$request[] = [
					 				   	 					'created_at'  => $check_id_request[0]->update_at,
					 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
					 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
					 				   	 				 	'read' => 'u',
					 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
					 				   	 				 	'id_req' => $check_id_request[0]->id,
					 				   	 				 	'status' =>  'Approved',
					 				   	 				 	'types' => $check_id_request[0]->type,
					 				   	 				 	'approve_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
					 				   	 				 ];
					 					}
					 				}else{
					 					if($emp == $arr){
					 						$ping += 1;	
					 				     	$read =  true;
					 				   	 	$request[] = [
					 				   	 					'created_at'  => $check_id_request[0]->update_at,
					 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
					 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
					 				   	 				 	'read' => 'u',
					 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
					 				   	 				 	'id_req' => $check_id_request[0]->id,
					 				   	 				 	'status' =>  'Approved',
					 				   	 				 	'types' => $check_id_request[0]->type,
					 				   	 				 	'rejected_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
					 				   	 				 ];
					 					}
					 				}		
					 			}
				 			}
					 		if($js->hr != null){
						 		if($js->app_hr == null){
						 			$pinghr_count = count($js->hr);
						 			for($k= 0; $k < $pinghr_count; $k++){
						 				if(isset($js->hr[$k]) && ($js->hr[$k] == $arr)){
						 					
					 				    	$ping += 1;	
					 				    	$read =  true;
					 				    	$idx = $value->id_req;
											$request[] = $this->sql_query($idx,$arr);

						 				}else{
						 					$ping += 0;
						 					$read = false;
							 			}
						 			}
						 		}else{
						 			if($emp == $arr){
			 			
						 			$check_id_pool =  $value->id_req;
						 			$check_id_request = \DB::SELECT("select * from  att_schedule_request,emp,att_type   where id = $check_id_pool and emp.employee_id = att_schedule_request.employee_id and att_type.type_id  =  att_schedule_request.type_id");
						 			$appover_id  = $check_id_request[0]->approver;  
						 			$approver_name  = \DB::SELECT("select * from emp  where employee_id = '$appover_id' ");
							 			if(isset($check_id_request)){
							 				
							 				$checked =  $check_id_request[0]->status_id;
							 				if($checked ==  2){
							 					if($emp == $arr){
							 						$ping += 1;	
							 				     	$read =  true;
							 				   	 	$request[] = [
							 				   	 					'created_at'  => $check_id_request[0]->update_at,
							 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
							 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
							 				   	 				 	'read' => 'u',
							 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
							 				   	 				 	'id_req' => $check_id_request[0]->id,
							 				   	 				 	'status' =>  'Approved',
							 				   	 				 	'types' => $check_id_request[0]->type,
							 				   	 				 	'approve_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
							 				   	 				 ];
							 					}
							 				}else{
							 					if($emp == $arr){
							 						$ping += 1;	
							 				     	$read =  true;
							 				   	 	$request[] = [
							 				   	 					'created_at'  => $check_id_request[0]->update_at,
							 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
							 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
							 				   	 				 	'read' => 'u',
							 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
							 				   	 				 	'id_req' => $check_id_request[0]->id,
							 				   	 				 	'status' =>  'Approved',
							 				   	 				 	'types' => $check_id_request[0]->type,
							 				   	 				 	'rejected_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
							 				   	 				 ];
							 					}
							 				}		
							 			}
						 			}
						 		}
						 	}else{
						 		if($js->emp != null and $js->emp[0] == $arr){
						 			if($js->app_emp  == null ){
							 			if(isset($js->emp_stat[0]->$arr) and $js->emp_stat[0]->$arr == 'u'){
							 				if($js->app_sup != null && $js->app_sup[1]  == 2){
								 				$ping += 0;
						 					}else{
						 						$ping += 1;
						 						$idx = $value->id_req;
												$request[] = $this->sql_query($idx,$arr);
						 					} 
							 			}
							 		}else{
							 			if($emp == $arr){
							 			$check_id_pool =  $value->id_req;
							 			$check_id_request = \DB::SELECT("select * from  att_schedule_request,emp,att_type   where id = $check_id_pool and emp.employee_id = att_schedule_request.employee_id and att_type.type_id  =  att_schedule_request.type_id");
							 			$appover_id  = $check_id_request[0]->approver;  
							 			$approver_name  = \DB::SELECT("select * from emp  where employee_id = '$appover_id' ");
								 			if(isset($check_id_request)){
								 				
								 				$checked =  $check_id_request[0]->status_id;
								 				if($checked ==  2){
								 					if($emp == $arr){
								 						$ping += 1;	
								 				     	$read =  true;
								 				   	 	$request[] = [
								 				   	 					'created_at'  => $check_id_request[0]->update_at,
								 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
								 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
								 				   	 				 	'read' => 'u',
								 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
								 				   	 				 	'id_req' => $check_id_request[0]->id,
								 				   	 				 	'status' =>  'Approved',
								 				   	 				 	'types' => $check_id_request[0]->type,
								 				   	 				 	'approve_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
								 				   	 				 ];
								 					}
								 				}else{
								 					if($emp == $arr){
								 						$ping += 1;	
								 				     	$read =  true;
								 				   	 	$request[] = [
								 				   	 					'created_at'  => $check_id_request[0]->update_at,
								 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
								 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
								 				   	 				 	'read' => 'u',
								 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
								 				   	 				 	'id_req' => $check_id_request[0]->id,
								 				   	 				 	'status' =>  'Approved',
								 				   	 				 	'types' => $check_id_request[0]->type,
								 				   	 				 	'rejected_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
								 				   	 				 ];
								 					}
								 				}		
								 			}
							 			}
							 		}
							 	}
						 	}
					 	}
					}else{
						if($emp == $arr and $js->app_sup != null){
			 			$check_id_pool =  $value->id_req;
			 			$check_id_request = \DB::SELECT("select * from  att_schedule_request,emp,att_type   where id = $check_id_pool and emp.employee_id = att_schedule_request.employee_id and att_type.type_id  =  att_schedule_request.type_id");
			 			$appover_id  = $check_id_request[0]->approver;  
			 			$approver_name  = \DB::SELECT("select * from emp  where employee_id = '$appover_id' ");
				 			if(isset($check_id_request)){
				 				
				 				$checked =  $check_id_request[0]->status_id;
				 				if($checked ==  2){
				 					if($emp == $arr){
				 						$ping += 1;	
				 				     	$read =  true;
				 				   	 	$request[] = [
				 				   	 					'created_at'  => $check_id_request[0]->update_at,
				 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
				 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
				 				   	 				 	'read' => 'u',
				 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
				 				   	 				 	'id_req' => $check_id_request[0]->id,
				 				   	 				 	'status' =>  'Approved',
				 				   	 				 	'types' => $check_id_request[0]->type,
				 				   	 				 	'approve_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
				 				   	 				 ];
				 					}
				 				}else{
				 					if($emp == $arr){
				 						$ping += 1;	
				 				     	$read =  true;
				 				   	 	$request[] = [
				 				   	 					'created_at'  => $check_id_request[0]->update_at,
				 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
				 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
				 				   	 				 	'read' => 'u',
				 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
				 				   	 				 	'id_req' => $check_id_request[0]->id,
				 				   	 				 	'status' =>  'Approved',
				 				   	 				 	'types' => $check_id_request[0]->type,
				 				   	 				 	'rejected_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
				 				   	 				 ];
				 					}
				 				}		
					 		}
				 		}
					}
				}
			}

			if($schema[0] == 'emp' ){

				if($js->emp != null and $js->app_emp == null){
		 		 	$emp_count = count($js->emp);
		 		 	for($j = 0; $j < $emp_count; $j++){
			 			if($js->emp[$j] ==  $arr ){
				 			if(isset($js->app_emp[0]) and $js->app_emp[0] == $arr){
				 				$ping += 0;
			 					$idx = $value->id_req;
								$request[] = $this->sql_query($idx,$arr); 
				 			}else{
				 				$ping += 1;
			 					$idx = $value->id_req;
								$request[] = $this->sql_query($idx,$arr);
				 			}
				 		}
				 	}
			 	}else{
			 		if($emp == $arr){
			 			
		 			$check_id_pool =  $value->id_req;
		 			$check_id_request = \DB::SELECT("select * from  att_schedule_request,emp,att_type   where id = $check_id_pool and emp.employee_id = att_schedule_request.employee_id and att_type.type_id  =  att_schedule_request.type_id");
		 			$appover_id  = $check_id_request[0]->approver;  
		 			$approver_name  = \DB::SELECT("select * from emp  where employee_id = '$appover_id' ");
			 			if(isset($check_id_request)){
			 				
			 				$checked =  $check_id_request[0]->status_id;
			 				if($checked ==  2){
			 					if($emp == $arr){
			 						$ping += 1;	
			 				     	$read =  true;
			 				   	 	$request[] = [
			 				   	 					'created_at'  => $check_id_request[0]->update_at,
			 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
			 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
			 				   	 				 	'read' => 'u',
			 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
			 				   	 				 	'id_req' => $check_id_request[0]->id,
			 				   	 				 	'status' =>  'Approved',
			 				   	 				 	'types' => $check_id_request[0]->type,
			 				   	 				 	'approve_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
			 				   	 				 ];
			 					}
			 				}else{
			 					if($emp == $arr){
			 						$ping += 1;	
			 				     	$read =  true;
			 				   	 	$request[] = [
			 				   	 					'created_at'  => $check_id_request[0]->update_at,
			 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
			 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
			 				   	 				 	'read' => 'u',
			 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
			 				   	 				 	'id_req' => $check_id_request[0]->id,
			 				   	 				 	'status' =>  'Approved',
			 				   	 				 	'types' => $check_id_request[0]->type,
			 				   	 				 	'rejected_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
			 				   	 				 ];
			 					}
			 				}		
			 			}
		 			}
			 		if($js->sup != null){
		 				if($js->app_sup == null ){
		 					$count_sup = count($js->sup);
		 					for($i = 0 ; $i < $count_sup; $i++){
		 						if(isset($js->emp[$i]) and ($js->emp[$i] ==  $arr)  ){
				 					$ping += 1;
		 							$idx = $value->id_req;
									$request[] = $this->sql_query($idx,$arr);
		 						}else{
		 							$ping = 0;
		 						}	
		 					}
		 					 
					 	}else{
					 		if($emp == $arr){
			 			
				 			$check_id_pool =  $value->id_req;
				 			$check_id_request = \DB::SELECT("select * from  att_schedule_request,emp,att_type   where id = $check_id_pool and emp.employee_id = att_schedule_request.employee_id and att_type.type_id  =  att_schedule_request.type_id");
				 			$appover_id  = $check_id_request[0]->approver;  
				 			$approver_name  = \DB::SELECT("select * from emp  where employee_id = '$appover_id' ");
					 			if(isset($check_id_request)){
					 				
					 				$checked =  $check_id_request[0]->status_id;
					 				if($checked ==  2){
					 					if($emp == $arr){
					 						$ping += 1;	
					 				     	$read =  true;
					 				   	 	$request[] = [
					 				   	 					'created_at'  => $check_id_request[0]->update_at,
					 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
					 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
					 				   	 				 	'read' => 'u',
					 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
					 				   	 				 	'id_req' => $check_id_request[0]->id,
					 				   	 				 	'status' =>  'Approved',
					 				   	 				 	'types' => $check_id_request[0]->type,
					 				   	 				 	'approve_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
					 				   	 				 ];
					 					}
					 				}else{
					 					if($emp == $arr){
					 						$ping += 1;	
					 				     	$read =  true;
					 				   	 	$request[] = [
					 				   	 					'created_at'  => $check_id_request[0]->update_at,
					 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
					 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
					 				   	 				 	'read' => 'u',
					 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
					 				   	 				 	'id_req' => $check_id_request[0]->id,
					 				   	 				 	'status' =>  'Approved',
					 				   	 				 	'types' => $check_id_request[0]->type,
					 				   	 				 	'rejected_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
					 				   	 				 ];
					 					}
					 				}		
					 			}
				 			}
					 		if($js->hr != null){
					 			return 1;
						 		if($js->app_hr == null){
						 			$pinghr_count = count($js->hr_stat[0]);
						 			for($k= 0; $k < $pinghr_count; $k++){
						 				if(isset($js->hr_stat[0][$k]->$arr)){
						 					if($js->hr_stat[0][$k]->$arr == 'u'){
						 				    $ping += 1;	
						 				    $read =  true;		 					
						 				}else{
						 					$ping += 0;
						 					$read = false;
						 				}
						 				
						 					$idx = $value->id_req;
											$request[] = $this->sql_query($idx,$arr);
					 					}
						 			}
						 		}else{
						 			if($emp == $arr){
			 			
						 			$check_id_pool =  $value->id_req;
						 			$check_id_request = \DB::SELECT("select * from  att_schedule_request,emp,att_type   where id = $check_id_pool and emp.employee_id = att_schedule_request.employee_id and att_type.type_id  =  att_schedule_request.type_id");
						 			$appover_id  = $check_id_request[0]->approver;  
						 			$approver_name  = \DB::SELECT("select * from emp  where employee_id = '$appover_id' ");
							 			if(isset($check_id_request)){
							 				
							 				$checked =  $check_id_request[0]->status_id;
							 				if($checked ==  2){
							 					if($emp == $arr){
							 						$ping += 1;	
							 				     	$read =  true;
							 				   	 	$request[] = [
							 				   	 					'created_at'  => $check_id_request[0]->update_at,
							 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
							 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
							 				   	 				 	'read' => 'u',
							 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
							 				   	 				 	'id_req' => $check_id_request[0]->id,
							 				   	 				 	'status' =>  'Approved',
							 				   	 				 	'types' => $check_id_request[0]->type,
							 				   	 				 	'approve_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
							 				   	 				 ];
							 					}
							 				}else{
							 					if($emp == $arr){
							 						$ping += 1;	
							 				     	$read =  true;
							 				   	 	$request[] = [
							 				   	 					'created_at'  => $check_id_request[0]->update_at,
							 				   	 					'employee_name' => $check_id_request[0]->first_name.' '.$check_id_request[0]->middle_name.' '.$check_id_request[0]->last_name,  
							 				   	 				 	'employee_id' =>  $check_id_request[0]->employee_id,
							 				   	 				 	'read' => 'u',
							 				   	 				 	'date_request' =>  $check_id_request[0]->date_request,
							 				   	 				 	'id_req' => $check_id_request[0]->id,
							 				   	 				 	'status' =>  'Approved',
							 				   	 				 	'types' => $check_id_request[0]->type,
							 				   	 				 	'rejected_by' => $approver_name[0]->first_name.' '.$approver_name[0]->middle_name.' '.$approver_name[0]->last_name
							 				   	 				 ];
							 					}
							 				}		
							 			}
						 			}
						 		}
						 	}
					 	}
					}
			 	}
			}			
		}
		return \Response::json(['ping' => $ping,'request' => $request]);
	}

	public function approve(){
		$arr = $this->getUser();  

		// 1 train
		// 2 ob
		// 3 over
		// 4 under
		// 5 chnage
		// 6 swap
		// 7 late
		// 8 early  
		// 9 time  


		$i =  \Input::all();
		if(isset($i['id_pool']) && $i['id_pool'] !=  null){
			$db  = \DB::SELECT("select * from pool_request where id  = $i[id_pool] ");
			$request_id  =  $db[0]->id_req;
			if($db !=  null){
				$request = [];
				$js =  json_decode($db[0]->json_data);
					//foreach ($js as $max => $max_val) {
				$emp  =  $db[0]->employee_id;
				$lc = \DB::SELECT("select local_it from emp where employee_id = '$emp'");
				if($db[0]->type_request == 'time'  &&  $lc[0]->local_it != 1){
					$schema = ['hr','sup','emp']; 
					$end = 'hr'; 
				}elseif ($db[0]->type_request == 'swap'  &&  $lc[0]->local_it != 1) {
					$end  = 'sup';
					$schema = ['emp','sup','hr'];
				}elseif ($db[0]->type_request == 'time'  &&  $lc[0]->local_it == 1) {
					$schema = ['sup','hr','emp'];
					$end = 'sup';
				}elseif ($db[0]->type_request == 'over'  &&  $lc[0]->local_it != 1) {
					$schema = ['sup','hr','emp'];
					$end = 'sup';
				}elseif ($db[0]->type_request == 'under'  &&  $lc[0]->local_it != 1) {
					$schema = ['sup','hr','emp'];
					$end = 'sup';
				}elseif ($db[0]->type_request == 'late'  &&  $lc[0]->local_it != 1) {
					$schema = ['sup','hr','emp'];
					$end  =  'hr';
				}elseif ($db[0]->type_request == 'early'  &&  $lc[0]->local_it != 1) {
					$schema = ['sup','hr','emp'];
					$end  = 'hr';
				}elseif ($db[0]->type_request == 'train'){
					$end = 'sup';
					$schema = ['sup','hr','emp'];
				}elseif ($db[0]->type_request == 'ob'){
					$schema = ['sup','hr','emp'];
					$end  =  'sup';
				}elseif ($db[0]->type_request == 'change'){
					$emp = \DB::SELECT("select * from emp_supervisor where supervisor =  '$emp' ");
					if($emp != null){
						$end = 'emp';
						$schema = ['emp','hr','sup'];
					}else{
						$end  =  'sup';
						$schema = ['sup','emp','hr'];
					}
				}elseif ($db[0]->type_request == 'vacation'  &&  $lc[0]->local_it != 1){
					$schema = ['hr','sup','hr'];
				}elseif ($db[0]->type_request == 'ADO'  &&  $lc[0]->local_it == 1){
					$schema = ['hr','sup','emp'];
				}elseif ($db[0]->type_request == 'vacation'  &&  $lc[0]->local_it == 1){
					$schema = ['sup','hr','emp'];
				}elseif ($db[0]->type_request == 'enhance'  &&  $lc[0]->local_it == 1){
					$schema = ['hr','sup','emp'];
				}elseif ($db[0]->type_request == 'birth'  &&  $lc[0]->local_it != 1){
					$schema = ['hr','sup','emp'];
				}elseif ($db[0]->type_request == 'sick' ) {
					$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
					if($hr_cek != null){
						if($lc[0]->local_it == 1){
							$schema = ['sup','hr','emp'];
						}
					}else{
						if($lc[0]->local_it == 1){
							$schema = ['sup','hr','emp'];
						}else{
							$schema = ['hr','sup','emp'];
						}
					}		
				}elseif ($db[0]->type_request == 'maternity' ) {
					$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
					if($hr_cek != null){
						if($lc[0]->local_it != 1){
							$schema = ['hr','sup','emp'];
						}
					}else{
						if($lc[0]->local_it == 1){
							$schema = ['hr','sup','emp'];
						}
					}
				}elseif ($db[0]->type_request == 'paternity'  &&  $lc[0]->local_it != 1) {
					$schema = ['sup','hr','emp'];
				}elseif ($db[0]->type_request == 'paternity'  &&  $lc[0]->local_it == 1) {
					$schema = ['sup','hr','emp'];
				}elseif ($db[0]->type_request == 'bereavement') {
					$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
					if($hr_cek != null){
						if($lc[0]->local_it != 1){
							$schema = ['sup','hr','emp'];
						}
					}else{
						if($lc[0]->local_it == 1){
							$schema = ['sup','hr','emp'];
						}else{
							$schema = ['sup','hr','emp'];
						}
					}
				}elseif ($db[0]->type_request == 'marriage') {
					$schema = ['sup','hr','emp'];
				}elseif ($db[0]->type_request == 'emmergency'  ) {
					$schema = ['sup','hr','emp'];
				}else{
					$schema = ['sup','hr','emp'];
				}

				$time =  date("h:i:s");
				$date =  date("Y-m-d");
				
				if($schema[0] == "hr"){
					if($js->hr != null){
				 		if($js->app_hr == null){
				 			$pinghr_count = count($js->hr);
				 			for($k= 0; $k < $pinghr_count; $k++){
				 				if(isset($js->hr[$k]) && ($js->hr[$k] == $arr)){
				 				   
				 				    $js->app_hr = [$arr,1];
				 				    $js->hr_date =  [$date];
				 				    $js->hr_time = [$time];
				 				    if($end ==  'hr'){
				 				    	\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				    } 
				 				}
				 			}
				 		}else{
							if($js->sup != null and $js->sup[0] ==  $arr){
				 				if($js->app_sup == null ){
						 			$js->app_sup = [$arr,1];
						 			$js->sup_date =  [$date];
				 				    $js->sup_time = [$time];
				 				    if($end ==  'sup'){
				 				    	\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				   	}
							 	}else{
						 			if($js->emp != null){
							 			if($js->app_emp  == null ){
								 			if(isset($js->emp_stat[0]->$arr) and $js->emp_stat[0]->$arr == 'u'){
								 				$js->app_emp = [$arr,1];
								 				$js->emp_date =  [$date];
				 				    			$js->emp_time = [$time];
				 				    			if($end ==  'emp'){
				 				    				\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				   				}
								 			}
								 		}
								 	}
							 	}
				 			}else{
				 				if($js->emp != null and $js->emp[0] ==  $arr){
					 				if($js->app_emp == null ){
							 			$js->app_emp = [$arr,1];
							 			$js->emp_date =  [$date];
				 				   	    $js->emp_time = [$time];
				 				   	    if($end ==  'emp'){
				 				    		\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				    	}
							 		}	
				 				}
				 			}
				 		}
		 			}
				}

				if($schema[0] == "sup"){
					if($js->sup != null  and  $js->app_sup == null){
				 		if($js->app_sup == null){
					 		$count_sup =  count($js->sup);
					 		for($x = 0; $x < $count_sup;$x++){
						 		if(isset($js->sup[$x])  and ($js->sup[$x] == $arr)){

									$js->app_sup = [$arr,1];
							 		$js->sup_date =  [$date];
						 			$js->sup_time = [$time];
						 			if($end ==  'sup'){
						 				\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
						 			}
						 		}
							}
						}
					}else{
						if($js->hr != null){
					 		if($js->app_hr == null){
					 			$pinghr_count = count($js->hr);
					 			for($k= 0; $k < $pinghr_count; $k++){
					 				if(isset($js->hr[$k]) && ($js->hr[$k] == $arr)){
					 				    $js->app_hr = [$arr,1];
					 				    $js->hr_date =  [$date];
				 				    	$js->hr_time = [$time];
				 				    	if($end ==  'hr'){
				 				    		\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				    	}
					 				}
					 			}
					 			
					 		}
						}else{
							if($js->emp != null and $js->emp[0] ==  $arr){
					 			if($js->app_emp == null ){
							 		$js->app_emp = [$arr,1];
							 		$js->emp_date =  [$date];
				 				    $js->emp_time = [$time];
				 				    if($end ==  'emp'){
				 				    	\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				   	}
							 	}	
				 			}
						}
					}
				}

				if($schema[0] == "emp"){
					if($js->emp != null and $js->app_emp == null){
				 		$count_emp = count($js->emp);
					 	for($i = 0; $i < $count_emp; $i++){
					 		if(isset($js->emp[$i]) && ($js->emp[$i] == $arr)){
					 			$js->app_emp = [$arr,1];
							 	$js->emp_date =  [$date];
					 			$js->emp_time = [$time];
					 			// if($end ==  'emp'){
					 			// 	\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
					 			// }
							}	
						}
			 		}else{

			 			if($js->sup != null and $js->app_sup == null){
				 			$js_count =  count($js->sup);
				 			for($i  = 0; $i < $js_count; $i++){
				 				if($js->sup[$i] ==  $arr){

							 		$js->app_sup = [$arr,1];
							 		$js->sup_date =  [$date];
					 				$js->sup_time = [$time];
					 				if($end ==  'emp'){
					 				    \DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
					 				}
								}
							}
						}else{
							if($js->app_hr == null){
					 			$pinghr_count = count($js->hr);
					 			for($k= 0; $k < $pinghr_count; $k++){
					 				if(isset($js->hr_stat[$k]) && ($js->hr_stat[$k] == $arr) ){

					 				    $js->app_hr = [$arr,1];
					 				    $js->hr_date =  [$date];
				 				   		$js->hr_time = [$time];
				 				   		if($end =  'hr'){
				 				    		\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				   		}
					 				}
					 			}	
					 		}
						}
			 		}
				}


			$js  =  json_encode($js);
			$js  = (string)$js;
			$in  = \Input::all(); 
			$update  = \DB::SELECT("update pool_request set json_data = '$js' where id  = $in[id_pool] ");
			// return $this->count_notif();
			
			$data  = [];
			$status =  200;
			$message = 'Success approve data';
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, ],$status);
			}
		}	
	}

	public function reject(){
		$arr = $this->getUser();
		$time =  date("h:i:s");
		$date =  date("Y-m-d");		
		$i =  \Input::all();
			if(isset($i['id_pool']) && $i['id_pool'] !=  null){

				$db  = \DB::SELECT("select * from pool_request where id  = $i[id_pool] ");
				$request_id  =  $db[0]->id_req;

				if($db !=  null){
					$request = [];
					$js =  json_decode($db[0]->json_data);
						//foreach ($js as $max => $max_val) {
					$emp  =  $db[0]->employee_id;
					$lc = \DB::SELECT("select local_it from emp where employee_id = '$emp'");
					if($db[0]->type_request == 'time'  &&  $lc[0]->local_it != 1){
						$end = 'hr';
						$schema = ['hr','sup','emp'];  
					}elseif ($db[0]->type_request == 'time'  &&  $lc[0]->local_it == 1) {
						$end  =  'sup';
						$schema = ['sup','hr','emp'];  
					}elseif ($db[0]->type_request == 'over'  &&  $lc[0]->local_it != 1) {
						$end  =  'sup';
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'swap'  &&  $lc[0]->local_it != 1) {
						$end  =  'sup';
						$schema = ['emp','sup','hr'];
					}elseif ($db[0]->type_request == 'under'  &&  $lc[0]->local_it != 1) {
						$end  =  'sup';
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'late'  &&  $lc[0]->local_it != 1) {
						$end  =  'hr';
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'early'  &&  $lc[0]->local_it != 1) {
						$end  =  'hr';
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'train') {
						$end  = 'sup';
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'ob') {
						$end  = 'sup';
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'change') {
						$emp = \DB::SELECT("select * from emp_supervisor where supervisor =  '$emp'");
						if($emp != null){
							$end  = 'emp';
							$schema = ['emp','hr','sup'];
						}else{
							$end  = 'sup';
							$schema = ['sup','emp','hr'];
						}
					}elseif ($db[0]->type_request == 'vacation'  &&  $lc[0]->local_it != 1) {
						$schema = ['hr','sup','hr'];
					}elseif ($db[0]->type_request == 'ADO'  &&  $lc[0]->local_it == 1) {
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'vacation'  &&  $lc[0]->local_it == 1) {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'enhance'  &&  $lc[0]->local_it == 1) {
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'birth'  &&  $lc[0]->local_it != 1) {
						$schema = ['hr','sup','emp'];
					}elseif ($db[0]->type_request == 'sick' ) {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}else{
								$schema = ['hr','sup','emp'];
							}
						}		
					}elseif ($db[0]->type_request == 'maternity' ) {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it != 1){
								$schema = ['hr','sup','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['hr','sup','emp'];
							}
						}
					}elseif ($db[0]->type_request == 'paternity'  &&  $lc[0]->local_it != 1) {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'paternity'  &&  $lc[0]->local_it == 1) {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'bereavement') {
						$hr_cek = \DB::SELECT("select * from view_nonactive_login where employee_id = '$emp' and (lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%' lower(role_name) like '%hr%')");
						if($hr_cek != null){
							if($lc[0]->local_it != 1){
								$schema = ['sup','hr','emp'];
							}
						}else{
							if($lc[0]->local_it == 1){
								$schema = ['sup','hr','emp'];
							}else{
								$schema = ['sup','hr','emp'];
							}
						}
					}elseif ($db[0]->type_request == 'marriage') {
						$schema = ['sup','hr','emp'];
					}elseif ($db[0]->type_request == 'emmergency'  ) {
						$schema = ['sup','hr','emp'];
					}else{
						$schema = ['sup','hr','emp'];
					}

				 	if($schema[0] == "hr"){
						if($js->hr != null){
				 			if($js->app_hr == null){
				 				$pinghr_count = count($js->hr);
				 				for($k= 0; $k < $pinghr_count; $k++){
				 					if(isset($js->hr[$k]) && ($js->hr[$k] == $arr)){
				 				    $js->app_hr = [$arr,2];
				 				    $js->hr_date =  [$date];
				 				    $js->hr_time = [$time];
				 				    if($end ==  'hr'){
				 				    	\DB::SELECT("update att_schedule_request set status_id = 3 and approver = '$arr'  where id  = $request_id ");
				 				    } 
				 				}
				 			}
				 		}else{
							if($js->sup != null and $js->sup[0] ==  $arr){
				 				if($js->app_sup == null ){
						 			$js->app_sup = [$arr,2];
						 			$js->sup_date =  [$date];
				 				    $js->sup_time = [$time];
				 				    if($end ==  'sup'){
				 				    	\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				   	}
							 	}else{
						 			if($js->emp != null){
							 			if($js->app_emp  == null ){
								 			if(isset($js->emp_stat[0]->$arr) and $js->emp_stat[0]->$arr == 'u'){
								 				$js->app_emp = [$arr,2];
								 				$js->emp_date =  [$date];
				 				    			$js->emp_time = [$time];
				 				    			if($end ==  'emp'){
				 				    				\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				   				}
								 			}
								 		}
								 	}
							 	}
				 			}else{
				 				if($js->emp != null and $js->emp[0] ==  $arr){
					 				if($js->app_emp == null ){
							 			$js->app_emp = [$arr,2];
							 			$js->emp_date =  [$date];
				 				   	    $js->emp_time = [$time];
				 				   	    if($end ==  'emp'){
				 				    		\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				    	}
							 		}	
				 				}
				 			}
				 		}
		 			}
				}

				if($schema[0] == "sup"){
					if($js->sup != null  and  $js->app_sup == null){
				 		if($js->app_sup == null){
					 		$count_sup =  count($js->sup);
					 		for($x = 0; $x < $count_sup;$x++){
						 		if(isset($js->sup[$x])  and ($js->sup[$x] == $arr)){

									$js->app_sup = [$arr,2];
							 		$js->sup_date =  [$date];
						 			$js->sup_time = [$time];
						 			if($end ==  'sup'){
						 				\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
						 			}
						 		}
							}
						}
					}else{
						if($js->hr != null){
					 		if($js->app_hr == null){
					 			$pinghr_count = count($js->hr);
					 			for($k= 0; $k < $pinghr_count; $k++){
					 				if(isset($js->hr[$k]) && ($js->hr[$k] == $arr)){
					 				    $js->app_hr = [$arr,2];
					 				    $js->hr_date =  [$date];
				 				    	$js->hr_time = [$time];
				 				    	if($end ==  'hr'){
				 				    		\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				    	}
					 				}
					 			}
					 			
					 		}
						}else{
							if($js->emp != null and $js->emp[0] ==  $arr){
					 			if($js->app_emp == null ){
							 		$js->app_emp = [$arr,2];
							 		$js->emp_date =  [$date];
				 				    $js->emp_time = [$time];
				 				    if($end ==  'emp'){
				 				    	\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				   	}
							 	}	
				 			}
						}
					}
				}

				if($schema[0] == "emp"){
					if($js->emp != null and $js->app_emp == null){
				 		$count_emp = count($js->emp);
					 	for($i = 0; $i < $count_emp; $i++){
					 		if(isset($js->emp[$i]) && ($js->emp[$i] == $arr)){
					 			$js->app_emp = [$arr,2];
							 	$js->emp_date =  [$date];
					 			$js->emp_time = [$time];
					 			// if($end ==  'emp'){
					 			// 	\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
					 			// }
							}	
						}
			 		}else{

			 			if($js->sup != null and $js->app_sup == null){
				 			$js_count =  count($js->sup);
				 			for($i  = 0; $i < $js_count; $i++){
				 				if($js->sup[$i] ==  $arr){

							 		$js->app_sup = [$arr,2];
							 		$js->sup_date =  [$date];
					 				$js->sup_time = [$time];
					 				if($end ==  'emp'){
					 				    \DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
					 				}
								}
							}
						}else{
							if($js->app_hr == null){
					 			$pinghr_count = count($js->hr);
					 			for($k= 0; $k < $pinghr_count; $k++){
					 				if(isset($js->hr_stat[$k]) && ($js->hr_stat[$k] == $arr) ){

					 				    $js->app_hr = [$arr,2];
					 				    $js->hr_date =  [$date];
				 				   		$js->hr_time = [$time];
				 				   		if($end =  'hr'){
				 				    		\DB::SELECT("update att_schedule_request set status_id = 2, approver = '$arr'  where id  = $request_id  ");
				 				   		}
					 				}
					 			}	
					 		}
						}
			 		}
				}	


				$js =  json_encode($js);
				$update  = \DB::SELECT("update pool_request set json_data = '$js' where id  = $i[id_pool] ");
				///return $this->count_notif();
				$data  = [];
				$status =  200;
				$message = 'Suucess approve data';
				return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, ],$status);
				}
			}
		}	
	
	public function sql_query($idx,$requestor){
		$request = [];
		$db  =  \DB::SELECT(
				"select distinct t1.id,t1.date_request,t1.availment_date,t6.employee_id as empreq,t6.swap_with as empswap,
				 concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name)as employee,t1.employee_id,t3.type_id,
				 t3.type,t1.status_id,t4.status,t1.request_id,date_format(t1.update_at,'%Y-%m-%d') as date_, 
				 date_format(t1.update_at,'%H:%s') as time_,t1.approval,t1.approver,
				 /*-----------------for time in out request------------t7*/
				 t7.req_in,t7.req_out,t7.req_in_out_id, 		 			 
			 	 /*  ----------- for training and ob------------------ t8*/
			 	 t8.id as id_training,
			 	 t8.start_ as start_training,
			 	 t8.end as end_training,
			 	 t8.created_at as date_request_training,
			 	 /*--------------for overtime ----------------------- t11*/
			 	 t11.id as overtime_id,
			 	 t11.date_str as date_overtime,
			 	 (select shift_code from attendance_work_shifts where t11.id_shift = shift_id) as shift_code_overtime,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where t11.id_shift = shift_id) as time_shift_code_overtime,
			 	 t11.overtime_str as overtime_start,
			 	 t11.overtime_end as overtime_end,
			 	 t11.total_overtime as overtime_total,
			 	 t11.created_at as date_request_overtime,
			 	 /*--------------for undertime  --------------------- t15*/
			 	 t15.id as id_undertime,
			 	 t15.date as date_undertime,
			 	 (select shift_code from attendance_work_shifts where t15.shift_id = shift_id) as shift_code_undertime,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where t15.shift_id = shift_id) as time_shift_code_undertime,
			 	 t15.work_end_time as end_time_request_undertime,
			 	 t15.short as date_diff_end_time_and_shift_time,
			 	 t15.created_at as date_request_undertime,
			 	 /*-----------------for change shift----------------- t12 */
			 	 t12.id as id_change_shift,
			 	 date_format(t12.date,'%y-%m-%d') as date_request_change_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t12.old_shift ) as old_shift_change_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t12.new_shift ) as new_shift_change_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t12.old_shift ) as time_old_shift_change_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t12.new_shift ) as time_new_shift_change_shift,
			 	 t12.created_at as date_request_change_shift,
			 	 /*-----------------for swap shift--------------------- t13*/
			 	 t13.swap_id as  id_swap_shift,
			 	 t13.date  as date_swap_shift,
			 	 (select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) from emp where employee_id  = t13.swap_with) as employee_swap_for_swap_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t13.old_shift_id ) as old_shift_swap_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t13.new_shift_id ) as new_shift_swap_shift,
			 	  (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t13.old_shift_id ) as time_old_shift_swap_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t13.new_shift_id ) as time_new_shift_swap_shift,
			 	 t13.created_at as date_request_swap_shift,
			 	 /*-----------------for late / for early_out------------------------- t14*/
			 	 t14.date  as dateLate,
			 	 t14.late_id as lateId,
			 	 t14.late as lateTime,
			 	 t14.early_out as earlyOut,
			 	 t14.created_at as datecreated_att,
			 	 (select id from pool_request where id_req = t1.id) as id_pool
			 	 from att_schedule_request t1
				 left join emp t2 on t2.employee_id=t1.employee_id 
				 left join att_type t3 on t3.type_id=t1.type_id 
				 left join att_late t14 on t14.type_id =  t1.type_id and t14.employee_id =  t1.employee_id and t14.late_id =  t1.request_id
				 left join att_undertime t15 on t15.type_id =  t1.type_id and t15.employee_id =  t1.employee_id
				 left join att_overtime t11 on t11.type_id=t1.type_id and t11.employee_id =  t1.employee_id
				 left join att_change_shift t12 on t12.type_id=t1.type_id and t12.employee_id =  t1.employee_id
				 left join att_swap_shift t13 on t13.type_id=t1.type_id and t13.employee_id =  t1.employee_id and t13.swap_id = t1.request_id
				 left join att_training t8 on t8.type_id=t1.type_id and t8.employee_id =  t1.employee_id
				 left join att_time_in_out_req t7 on t7.type_id = t1.type_id and t7.employee_id =  t1.employee_id
				 left join att_status t4 on  t4.status_id=t1.status_id 
				 left join att_swap_shift t6 on t6.swap_id=t1.request_id and t6.employee_id =  t1.employee_id
				 left join command_center t17 on t17.schedule_request_ID = t1.id and t17.type_id  = t1.type_id
				 where t1.id = $idx order by t1.id asc");

	 				$count_db = count($db);
	 				for($l = 0; $l < $count_db; $l++){
	 					if($db[0]->type_id == 9){ //time
							$exploded = explode(' ',$db[0]->availment_date);
	 						$get_id  = \DB::SELECT("select * from pool_request,emp where pool_request.employee_id = emp.employee_id and id_req = $idx");
	 						$get_json = json_decode($get_id[0]->json_data,1);
	 						$get_sub_employee = $get_id[0]->local_it; 

	 						if($get_sub_employee == 1){
	 							$schema = ['sup','hr','emp'];  
					 		}else{
					 			$schema = ['hr','sup','emp'];  
	 						}

	 						foreach ($schema as $key => $value) {

	 							//check by schema

	 							if(isset($get_json['hr_stat'])){
									$count =  count($get_json['hr_stat']);
									for($i = 0; $i < $count; $i++){
										if(isset($get_json['hr_stat'][$i][$requestor])){

											$read  = $get_json['hr_stat'][$i][$requestor];
										}
									}
								}
								if($value  == 'sup'){
									if(isset($get_json['sup_stat'])){
										$count =  count($get_json['sup_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['sup_stat'][$i][$requestor])){
												$read =$get_json['sup_stat'][$i][$requestor];
												
											}
										}
									}
								}
								if($value  == 'emp'){
									if(isset($get_json['emp_stat'])){
										$count =  count($get_json['emp_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['emp_stat'][$i][$requestor])){
												$read = $get_json['emp_stat'][$i][$requestor];
												
											}
										}
									}
								}
	 						}


	 						$request =  [
			 							'status'	   => 'pending',
			 							'read'		   => $read,
			 							'employee_id'  => $db[0]->employee_id,
			 							'employee'     => $db[0]->employee,
			 							'request_date' => $db[0]->date_request,
			 							'req_in'       => $db[0]->req_in,
			 							'req_out'      => $db[0]->req_out,
			 							'for_request'  => $db[0]->availment_date,
			 							'id_req'       => $db[0]->req_in_out_id,
			 							'type'         => $db[0]->type,
			 							'id'           => $idx,
			 							'id_pool'      => $db[0]->id_pool,
							              ]; 
	 					}
	 					if($db[0]->type_id == 1 || $db[0]->type_id == 2 ){ //training & ob

	 						$get_id  = \DB::SELECT("select * from pool_request,emp where pool_request.employee_id = emp.employee_id and id_req = $idx");
	 						$get_json = json_decode($get_id[0]->json_data,1);
	 						$get_sub_employee = $get_id[0]->local_it; 

	 						$schema = ['sup','hr','emp'];

	 						foreach ($schema as $key => $value) {
	 							if(isset($get_json['hr_stat'])){
									$count =  count($get_json['hr_stat']);
									for($i = 0; $i < $count; $i++){
										if(isset($get_json['hr_stat'][$i][$requestor])){
											$read  = $get_json['hr_stat'][$i][$requestor];
										}
									}
								}
								if($value  == 'sup'){
									if(isset($get_json['sup_stat'])){
										$count =  count($get_json['sup_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['sup_stat'][$i][$requestor])){
												$read = $get_json['sup_stat'][$i][$requestor];
											}
										}
									}
								}
								if($value  == 'emp'){
									if(isset($get_json['emp_stat'])){
										$count =  count($get_json['emp_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['emp_stat'][$i][$requestor])){
												$read = $get_json['emp_stat'][$i][$requestor];
												
											}
										}
									}
								}
	 						}


	 						$request =  [
			 							'status'	     	=> 'pending',
			 							'read'		       	=> $read,
			 							'employee_id'  		=> $db[0]->employee_id,
			 							'employee'     		=> $db[0]->employee,
			 							'created_at' 		=> $db[0]->date_request_training,
			 							'start_train'  		=> $db[0]->start_training,
			 							'end_train'    		=> $db[0]->end_training,
			 							'id'           		=> $idx,
			 							'type'        		=> $db[0]->type,
			 							'id_req'       		=> $db[0]->id_training,
			 							'id_pool'      		=> $db[0]->id_pool,
							              ]; 	
	 					}
	 					if($db[0]->type_id == 3 ){
	 						$exploded = explode(' ',$db[0]->date_overtime);
	 						$get_id  = \DB::SELECT("select * from pool_request,emp where pool_request.employee_id = emp.employee_id and id_req = $idx");
	 						$get_json = json_decode($get_id[0]->json_data,1);
	 						$get_sub_employee = $get_id[0]->local_it; 

	 						$schema = ['sup','hr','emp'];

	 						foreach ($schema as $key => $value) {
	 							if(isset($get_json['hr_stat'])){
									$count =  count($get_json['hr_stat']);
									for($i = 0; $i < $count; $i++){
										if(isset($get_json['hr_stat'][$i][$requestor])){
											$read  = $get_json['hr_stat'][$i][$requestor];
										}
									}
								}
								if($value  == 'sup'){
									if(isset($get_json['sup_stat'])){
										$count =  count($get_json['sup_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['sup_stat'][$i][$requestor])){
												$read =$get_json['sup_stat'][$i][$requestor];
												
											}
										}
									}
								}
								if($value  == 'emp'){
									if(isset($get_json['emp_stat'])){
										$count =  count($get_json['emp_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['emp_stat'][$i][$requestor])){
												$read = $get_json['emp_stat'][$i][$requestor];
												
											}
										}
									}
								}
	 						}
	 						$request =  [
			 							'status'	            => 'pending',
			 							'read'		            => $read,
			 							'employee_id'           => $db[0]->employee_id,
			 							'employee'              => $db[0]->employee,
			 							'request_date'          => $exploded[0],
			 							'date_request_overtime' => $db[0]->date_request_overtime,
			 							'shift_code'            => $db[0]->shift_code_overtime,
			 							'time_shift_code'       => $db[0]->time_shift_code_overtime,
			 							'overtime_start'        => $db[0]->overtime_start,
			 							'overtime_end'          => $db[0]->overtime_end,
			 							'overtime_total'        => $db[0]->overtime_total,
			 							'created_at'            => $db[0]->date_request_overtime,
			 							'id_req'                => $db[0]->overtime_id,
			 							'type'                  => $db[0]->type,
			 							'id'                    => $idx,
			 							'id_pool'      => $db[0]->id_pool,
							              ]; 	
	 					}

	 					if($db[0]->type_id == 4 ){
	 						//$exploded = explode(' ',$db[0]->date_overtime);
	 						$get_id  = \DB::SELECT("select * from pool_request,emp where pool_request.employee_id = emp.employee_id and id_req = $idx");
	 						$get_json = json_decode($get_id[0]->json_data,1);
	 						$get_sub_employee = $get_id[0]->local_it; 

	 						$schema = ['sup','hr','emp'];

	 						foreach ($schema as $key => $value) {
	 							if(isset($get_json['hr_stat'])){
									$count =  count($get_json['hr_stat']);
									for($i = 0; $i < $count; $i++){
										if(isset($get_json['hr_stat'][$i][$requestor])){
											$read  = $get_json['hr_stat'][$i][$requestor];
										}
									}
								}
								if($value  == 'sup'){
									if(isset($get_json['sup_stat'])){
										$count =  count($get_json['sup_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['sup_stat'][$i][$requestor])){
												$read =$get_json['sup_stat'][$i][$requestor];
												
											}
										}
									}
								}
								if($value  == 'emp'){
									if(isset($get_json['emp_stat'])){
										$count =  count($get_json['emp_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['emp_stat'][$i][$requestor])){
												$read = $get_json['emp_stat'][$i][$requestor];
												
											}
										}
									}
								}
	 						}
	 						$request =  [
			 							'status'	      => 'pending',
			 							'read'		      => $read,
			 							'employee_id'     => $db[0]->employee_id,
			 							'employee'        => $db[0]->employee,
			 							'request_date'    => $db[0]->date_undertime,
			 							'shift_code'      => $db[0]->shift_code_undertime,
			 							'time_shift_code' => $db[0]->time_shift_code_undertime,
			 							'created_at'      => $db[0]->date_request_undertime,
			 							'work_end_time'   => $db[0]->end_time_request_undertime,
			 							'short'           => $db[0]->date_diff_end_time_and_shift_time,
			 							'id_req'          => $db[0]->id_undertime,
			 							'id'			  => $idx,
			 							'type'         	  => $db[0]->type,
			 							'id_pool'      => $db[0]->id_pool,
							              ]; 	
	 					}

	 					if($db[0]->type_id == 5 ){      //change shift
	 						$exploded = explode(' ',$db[0]->date_request_change_shift);
	 						$get_id  = \DB::SELECT("select * from pool_request,emp where pool_request.employee_id = emp.employee_id and id_req = $idx");
	 						$get_json = json_decode($get_id[0]->json_data,1);
	 						$get_sub_employee = $get_id[0]->local_it; 

	 						$schema = ['sup','hr','emp'];

	 						foreach ($schema as $key => $value) {
	 							if(isset($get_json['hr_stat'])){
									$count =  count($get_json['hr_stat']);
									for($i = 0; $i < $count; $i++){
										if(isset($get_json['hr_stat'][$i][$requestor])){
											$read  = $get_json['hr_stat'][$i][$requestor];
										}
									}
								}
								if($value  == 'sup'){
									if(isset($get_json['sup_stat'])){
										$count =  count($get_json['sup_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['sup_stat'][$i][$requestor])){
												$read =$get_json['sup_stat'][$i][$requestor];
												
											}
										}
									}
								}
								if($value  == 'emp'){
									if(isset($get_json['emp_stat'])){
										$count =  count($get_json['emp_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['emp_stat'][$i][$requestor])){
												$read = $get_json['emp_stat'][$i][$requestor];
												
											}
										}
									}
								}
	 						}
	 						$request =  [
			 							'status'	     => 'pending',
			 							'read'		     => $read,
			 							'employee_id'    => $db[0]->employee_id,
			 							'employee'       => $db[0]->employee,
			 							'request_date'   => $exploded[0],//$db[0]->date_request_change_shift,
			 							'old_shift'      => $db[0]->old_shift_change_shift,
			 							'new_shift'      => $db[0]->new_shift_change_shift,
			 							'time_old_shift' => $db[0]->time_old_shift_change_shift,
			 							'time_new_shift' => $db[0]->time_new_shift_change_shift,
			 							'created_at'     => $db[0]->date_request_change_shift,
			 							'id_req'         => $db[0]->id_change_shift,
			 							'type'           => $db[0]->type,
			 							'id'             => $idx,
			 							'id_pool'      => $db[0]->id_pool,
							             ]; 	
	 					}

	 					if($db[0]->type_id == 6){ //swap
	 						$exploded = explode(' ',$db[0]->date_swap_shift);
	 						$get_id  = \DB::SELECT("select * from pool_request,emp where pool_request.employee_id = emp.employee_id and id_req = $idx");
	 						$get_json = json_decode($get_id[0]->json_data,1);
	 						$get_sub_employee = $get_id[0]->local_it; 

	 						$schema = ['sup','hr','emp'];

	 						foreach ($schema as $key => $value) {
	 							if(isset($get_json['hr_stat'])){
									$count =  count($get_json['hr_stat']);
									for($i = 0; $i < $count; $i++){
										if(isset($get_json['hr_stat'][$i][$requestor])){
											$read  = $get_json['hr_stat'][$i][$requestor];
										}
									}
								}
								if($value  == 'sup'){
									if(isset($get_json['sup_stat'])){
										$count =  count($get_json['sup_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['sup_stat'][$i][$requestor])){
												$read =$get_json['sup_stat'][$i][$requestor];
												
											}
										}
									}
								}
								if($value  == 'emp'){
									if(isset($get_json['emp_stat'])){
										$count =  count($get_json['emp_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['emp_stat'][$i][$requestor])){
												$read = $get_json['emp_stat'][$i][$requestor];
												
											}
										}
									}
								}
	 						}
	 						$request =  [
			 							'status'	     => 'pending',
			 							'read'		     => $read,
			 							'employee_id'    => $db[0]->employee_id,
			 							'employee'       => $db[0]->employee,
			 							'request_date'   => $exploded[0],
			 							'created_at'     => $db[0]->date_request_swap_shift,
			 							'swap_with'      => $db[0]->employee_swap_for_swap_shift,
			 							'old_shift'      => $db[0]->old_shift_swap_shift,
			 							'new_shift'      => $db[0]->new_shift_swap_shift,
			 							'time_old_shift' => $db[0]->time_old_shift_swap_shift,
			 							'time_new_shift' => $db[0]->time_new_shift_swap_shift,
			 							'id_req'         => $db[0]->id_swap_shift,
			 							'type'           => $db[0]->type,
			 							'id'             => $idx,
			 							'id_pool'      => $db[0]->id_pool,
							              ]; 	
	 					}

	 					if($db[0]->type_id == 7 ){ //late 
	 						$get_id  = \DB::SELECT("select * from pool_request,emp where pool_request.employee_id = emp.employee_id and id_req = $idx");
	 						$get_json = json_decode($get_id[0]->json_data,1);
	 						$get_sub_employee = $get_id[0]->local_it; 

	 						$schema = ['sup','hr','emp'];

	 						foreach ($schema as $key => $value) {
	 							if(isset($get_json['hr_stat'])){
									$count =  count($get_json['hr_stat']);
									for($i = 0; $i < $count; $i++){
										if(isset($get_json['hr_stat'][$i][$requestor])){
											$read  = $get_json['hr_stat'][$i][$requestor];
										}
									}
								}
								if($value  == 'sup'){
									if(isset($get_json['sup_stat'])){
										$count =  count($get_json['sup_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['sup_stat'][$i][$requestor])){
												$read =$get_json['sup_stat'][$i][$requestor];
												
											}
										}
									}
								}
								if($value  == 'emp'){
									if(isset($get_json['emp_stat'])){
										$count =  count($get_json['emp_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['emp_stat'][$i][$requestor])){
												$read = $get_json['emp_stat'][$i][$requestor];
												
											}
										}
									}
								}
	 						}
	 						$request =  [
			 							'status'	   => 'pending',
			 							'read'		   => $read,
			 							'employee_id'  => $db[0]->employee_id,
			 							'employee'     => $db[0]->employee,
			 							'request_date' => $db[0]->dateLate,
			 							'created_at'   => $db[0]->datecreated_att,
			 							'late_time'    => $db[0]->lateTime,
			 							'id_req'       => $db[0]->lateId,
			 							'type'         => $db[0]->type,
			 							'id'           => $idx,
			 							'id_pool'      => $db[0]->id_pool,
							              ]; 	
	 					}

	 					if($db[0]->type_id == 8 ){
	 						$get_id  = \DB::SELECT("select * from pool_request,emp where pool_request.employee_id = emp.employee_id and id_req = $idx");
	 						$get_json = json_decode($get_id[0]->json_data,1);
	 						$get_sub_employee = $get_id[0]->local_it; 

	 						$schema = ['sup','hr','emp'];

	 						foreach ($schema as $key => $value) {
	 							if(isset($get_json['hr_stat'])){
									$count =  count($get_json['hr_stat']);
									for($i = 0; $i < $count; $i++){
										if(isset($get_json['hr_stat'][$i][$requestor])){
											$read  = $get_json['hr_stat'][$i][$requestor];
										}
									}
								}
								if($value  == 'sup'){
									if(isset($get_json['sup_stat'])){
										$count =  count($get_json['sup_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['sup_stat'][$i][$requestor])){
												$read =$get_json['sup_stat'][$i][$requestor];
												
											}
										}
									}
								}
								if($value  == 'emp'){
									if(isset($get_json['emp_stat'])){
										$count =  count($get_json['emp_stat']);
										for($i = 0; $i < $count; $i++){
											if(isset($get_json['emp_stat'][$i][$requestor])){
												$read = $get_json['emp_stat'][$i][$requestor];
												
											}
										}
									}
								}
	 						}
	 						$request =  [
			 							'status'	   => 'pending',
			 							'read'		   => $read,
			 							'employee_id'  => $db[0]->employee_id,
			 							'employee'     => $db[0]->employee,
			 							'request_date' => $db[0]->dateLate,
			 							'created_at'   => $db[0]->datecreated_att,
			 							'early_time'    => $db[0]->earlyOut,
			 							'id_req'       => $db[0]->lateId,
			 							'type'         => $db[0]->type,
			 							'id'           => $idx,
			 							'id_pool'      => $db[0]->id_pool,
							              ]; 
	 					}
	 				}

	 				return $request;
	}
}


