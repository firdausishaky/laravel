<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Exeception;

class notificationV2 extends Controller {


	public function getUser($key = null){
		if($key == null){
			$key = \Input::get('key');
		}
		$keys= base64_decode($key);
		$test = explode('-',$keys);
		return $data = $test[1];
	}

	public function insert_notif($id,$type,$employee_id,$local,$typeUser,$typeRequest){
		$check = $this->notif($id,$type,$employee_id,$local,$typeUser,$typeRequest);	
		if($check == "success"){
			$data = [];
			$message = "success";
			$status = 200;
		}else{
			$data = [];
			$message = $check;
			$status = 200;
		}

		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, ],$status);
	}

	public function notif($ids,$type,$employee_id,$local_it,$user,$from_type = null){

		//user =  hr /supervisor ? regular employee no needed it
		
		$hr = \DB::SELECT("select employee_id from  ldap, role where ldap.role_id = role.role_id and (lower(role.role_name) like '%human%' or  '%human resource%' or '%hr%')");

		$hrx = [];
		foreach($hr as $key => $value){
			$hrx[] = [ $value->employee_id => 0,
 					   "sup_stat" =>  0,
 					   "sup_date" => '0000-00-00',
 					   "sup_time" => "00:00",
 					   "read_stat" => 0,	
					 ]; 
		}
		$hrx[]  = [    "2014888" => 0,
 					   "sup_stat" =>  0,
 					   "sup_date" => '0000-00-00',
 					   "sup_time" => "00:00",
 					   "read_stat" => 0,	
				  ];
		

		$sub = \DB::SELECT("select subordinate from emp_subordinate where employee_id = '$employee_id' ");
		
		$subx = [];
		foreach($sub as $key => $value){
			$subx[] =  [ $value->subordinate => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
			$value->subordinate; 
		}

		$subx =  [ '2014888' => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
		$sup = \DB::SELECT("select supervisor from emp_supervisor where employee_id = '$employee_id' ");
		
		$supx = [];
		foreach($sup as $key => $value){
			$supx[] =  [ $value->supervisor => 0,
 					   	 "sup_stat" =>  0,
 					     "sup_date" => '0000-00-00',
 					     "sup_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
		}
		$supx[] = [ "2014888"  => 0,
 					"sup_stat" =>  0,
 					"sup_date" => '0000-00-00',
 					"sup_time" => "00:00",
 					"read_stat" => 0,	
				  ];
		$swap = \DB::SELECT("select swap_with from att_swap_shift where employee_id = '$employee_id' and swap_id = (select request_id from att_schedule_request where employee_id='$employee_id' and id = $ids and type_id = 6 order by id desc limit 1 )  ");
		
		$swapx = [];
		foreach($swap as $key => $value){
			$swapx[] =  [ $value->swap_with => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ]; 
		}

		$swapx[] = [ "2014888" => 0,
 					 "swap_stat" =>  0,
 					 "swap_date" => "0000-00-00",
 					 "swap_time" => "00:00",
 					 "read_stat" => 0,	
					   ];

		if($type == 'time'){

			if($local_it == 'local'){
				$master =  'schedule';
				$end = ['hr' => 1,'emp' => '0','sup' => '0'];
				$arr = [
							"hr" => $hrx,
							"sup" => [],
							"emp" => [],
						
					   ];
			}else{
				$master =  'schedule';
				$end = ['sup' => 1,'hr' => '0','emp' => '0'];
				$arr = [
							"sup" => $supx,
							"hr" => [],
							"emp" => [],
								
					   ];
			}
		}

		if($from_type == "attendance"){

			if($type == 'over'){

				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['sup' => 1 ,'emp' => 0,'hr' => 0];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],
								
						];
				} 
			}
			if($type == 'under'){
				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['sup' => 1,'emp' => 0,'hr' => 0];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],
								
						]; 
				}
			}
			if($type == 'late' || $type == 'early'){
				if($local_it == 'local'){
					$master =  'schedule';	
					$end = ['sup' => 1,'hr' => 0,'emp' => 0];
					$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"emp" => [],
								
						]; 
				}
			}
			if($type == 'ob'){
				$master =  'schedule';
				$end = ['sup' => 1,'hr' => 0,'emp' => 0];
				$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],			
						
						]; 
			}

			if($type == 'train'){
				$master =  'schedule';
				$end = ['sup' => 1,'hr' => 0,'emp' => 0];
				$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],			
						
						]; 
			}
			if($type == 'change'){

				if($user == "sup"){
					$master =  'schedule';
					$end = ['emp' => 1,'hr' => 0,'emp' => 0];
					$arr = [
							"emp" => $subx,			
							"sup" => [],
							"hr" => [],
							
						   ]; 
				}else{
					$master =  'schedule';
					$end = ['sup' => 1,'hr' => 0,'emp' => 0];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],
								
						];
				}
			}

			if($type == "swap"){
				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['emp' => 1,'sup' => 2,'emp' => 0];
					$arr = [
							"emp" => $swapx,			
							"sup" => $supx,
							"hr" => [],
							
						   ]; 
		
				}
			}
		}else{


		//leave request 

			if($type == "vacation"){
				if($local_it == 'local'){
					$master =  'leave';
					$end = ['hr' => 1,'sup' => 2,'emp' => 0];
					$arr = [
							"hr" => $hrx,
							"sup" => $supx,
							"emp" => [],			
							
						   ]; 
				}else{
					$master =  'leave';
					$end = ['sup' => 1,'hr' => 0,'emp' => 0];
					$arr = [
							"sup" => $supx,
							"emp" => [],			
							"hr" => [],
							
						   ];
				}
			}

			if($type == 'ADO'){
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'emp' => 0];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"emp" => [],			
					
				   ];
			}

			if($type == 'enhance'){
				$master =  'leave';
				if($local_it == 'local'){
					
					$end = ['hr' => 1,'sup' => 2,'emp' => 0];
					$arr = [
						"hr" => $hrx,
						"sup" => $supx,
						"emp" => [],			
					
					   ];
				}	
			}

			if($type == 'birth'){
				$master =  'leave';
				if($local_it == 'local'){
					$end = ['hr' => 1,'sup' => 0,'emp' => 0];
					$arr = [
						"hr" => $hrx,
						"sup" => [],
						"emp" => [],			
					
					   ];
				}
			}

			if($user == "hr" && $type == 'sick' && $local_it == 'local'){
					$master =  'leave';
					$end = ['sup' => 1,'emp' => 0,'hr' => 0];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],			
					
					   ];
				
			}
			
			if($type == 'sick' && $local_it == 'local'){
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'emp' => 0];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"emp" => [],			
					
				   ];	
			}
			if($type == 'sick' && $local_it != 'expat'){
				$master =  'leave';
				$end = ['sup' => 1,'emp' => 2, 'hr' => 0];
				$arr = [
					"sup" => $supx,
					"hr" => [],
					"emp" => [],			
					
				   ];
			}

			if($type ==  'maternity'){
				$master =  'leave';
				if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['hr' => 1,'emp' => 0,'sup' => 0];
						$arr = [
						"hr" => $hrx,
						"sup" => [],
						"emp" => [],			
					
					   ];		
					}	
				}else{
					$master =  'leave';
					if($local_it == 'expat'){
						$end = ['sup' => 1,'hr' => 0,'emp' => 0];
						$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],			
					
					   ];		
					}	
				}
			}

			if($type == 'paternity'){
				$master =  'leave';
				if($local_it == 'local'){
					$end = ['sup' => 1,'hr' => 2 ,'emp' => 0];
						$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"emp" => [],			
					
					   ];		
				}else{
					$end = ['sup' => 1,'hr' => 0,'emp' => 0];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],			
					
					   ];
				}
			}

			if($type == 'bereavement'){
				$master =  'leave';
				if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 0,'emp' => 0];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"emp" => [],			
							
					   ];		
					}
				}
				else{
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 2,'emp' => 0];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"emp" => [],			
							
					   ];		
					}else{
						$end = ['sup' => 1,'emp' => 0,'hr' => 0];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"emp" => [],			
							
					   ];
					}
				}
			}

			if($type == 'marriage'){
				$master =  'leave';
				if($local_it == 'local'){
					$end = ['sup' => 1,'hr' => 2,'emp' => 0];
					$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"emp" => [],			
					
				   ];		
				}else{
					$end = ['sup' => 1,'hr' => 0,'emp' => 0];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],			
					
				   ];
				}
			}

			if($type == 'emmergency'){
				$master =  'leave';
				if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 0,'emp' => 0];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"emp" => [],			
							
					   ];		
					}
				}
				else{
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 2,'emp' => 0];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"emp" => [],			
							
					   ];		
					}else{
						$end = ['sup' => 1,'hr' => 0,'emp' => 0];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"emp" => [],			
							
					   ];
					}
				}
			}

			if($type == 'suspension'){
				$master =  'leave';
				if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['sup' => 0,'hr' => 0,'emp' => 0];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"emp" => [],			
							
					   ];		
					}
				}
			}
		}
		
		$chatID = [];

		if(isset($arr) && $arr  !=  null){
			$chat  = \DB::SELECT("select id from command_center where  schedule_request_id =  $ids");
			if($chat != null){
				foreach ($chat as $key => $value) {
					$chatID[$value->id]  = 'u';
				}	
			}
			
			$arr['requestor_stat'] =  [$employee_id => 0];
			$arr['chat_id'] = $chatID;
			$arr['req_flow'] = $end;
			$arr['master'] =  $master ;
		}else{
			return  $arr  =[ 'data' => [], 'message' =>  'your not allowed to request '];
		}

		//print_r($arr);
		$json = json_encode($arr);
		try{
			$insert = \DB::SELECT("insert into pool_request(employee_id,id_req,type_request,json_data,created_at) values('$employee_id',$ids,'$type','$json',now())");
			return "success";
		}catch(\Exeception $e){
			return $e;
		}
	}

	public  function count_notif(){

		$KEY  = \Input::get('key');

		$DEC_KEY =  explode('-',(base64_decode($KEY)))[1];
		$notif   = 0;

		$select = \DB::SELECT("select * from pool_request where DATE_SUB(now(),INTERVAL 7 DAY)");				
		$count_select = count($select);

		for($i  = 0; $i < $count_select; $i++){
			 $enc  = json_decode($select[$i]->json_data,1);

			//request flow		
			foreach ($enc['chat_id'] as $keyx => $valuex){
				$get_user_chat  = \DB::SELECT("select employee_id from command_center where id  =  $keyx");
				foreach ($get_user_chat as $keyz => $valuez) {
					if(isset($valuez->employee_id) and ($valuez->employee_id != $DEC_KEY) and ($valuex ==  'u')){
						$notif += 1;
					}
				}
			}
			

			foreach ($enc['req_flow'] as $key => $value) {
				if($value ==  1){
					foreach ($enc[$key] as $key1 => $value1 ) {
						if(isset($value1[$DEC_KEY])){
							if($value1['read_stat'] == 0 and $value1[$key.'_stat'] == 0  ){
								$notif += 1;
								$get_flow  = $key ;
							}
						}
					}	
				}
							
				if($value == 2 ){
					foreach ($enc[$key] as $key2 => $value2) {
						return $value2;
						if(isset($value2[$DEC_KEY])){
							if($value2['read_stat'] == 0 and $value2[$get_flow.'_stat'] == 0 and  $get_flow[$get_flow.'_stat'] ==  1 )
							$notif += 1;
							$get_flow =  $key;
						}
					}
				}	
				
				if($value == 3){
					foreach ($enc[$key] as $key3 => $value3) {
						if(isset($value3[$DEC_KEY])){
							if($value3['read_stat'] == 0  and   $get_flow[$get_flow.'_stat'] ==  1  and $value3[$value3.'_stat'] != 1){
								$notif += 1;
							}
						}
					}
				}

			}	
		}

		return \Response::json(['ping' => $notif,'request' => [] ]);
	
	}


	public function read_notif(){
		$token =  explode('-',base64_decode((\Input::get('key'))))[1];
		$data  = \DB::SELECT("select * from pool_request");
		$arr  = [];

		//who i am 
		$whoiam = \DB::SELECT("select role_name from view_nonactive_login  where employee_id  =  '$token' and  (lower(role_name) not like '%regular employee  user%' or lower(role_name) not like '%employee%' ) ");


		foreach ($data as $key => $value) {
			$sup  = json_decode($value->json_data,1)['sup'];
			$emp  = json_decode($value->json_data,1)['emp'];
			$hr   = json_decode($value->json_data,1)['hr'];
			$req_flow = json_decode($value->json_data,1)['req_flow'];
			$chatID = json_decode($value->json_data,1)['chat_id'];
			$json  =  json_decode($value->json_data,1)['master'];
		    if($json ==  "leave"){
		    	$idP =  $value->id_req;
		    	 $db = \DB::SELECT("select  distinct
											concat(t1.first_name,' ',t1.middle_name,' ',t1.last_name)as employee, 
											t2.employee_id,  
											t2.from_ , 
											t2.to_ , 
											t4.status,
											t4.status_id,
											t2.taken_day, 
											t2.balance_day, 
											t2.taken_day_approval, 
											t2.taken_day_approver, 
											t2.created_at, 
											t3.leave_type,
											t2.id,
											'leave' as master
											from emp t1 
											left join leave_request t2  on  t2.employee_id  = t1.employee_id 
											left join leave_type t3 on t3.id  = t2.leave_type
											left join att_status t4 on  t4.status_id =  t2.status_id
											where t2.id  = $idP ");
/*
		   		if(isset($db[0]) and $db[0]->status_id  ==  1 and  $db[0]->employee_id != $token and  $whoiam != null){
				 	$arr[] =  $db[0];
				}*/
				//$db[0]->master =  'leave';
				if(isset($db[0])){
					$getEMP  =  $db[0]->employee_id;
					//crai sttatus aprove atau pending approval  
					foreach ($req_flow as $key => $value) {
						if($value ==  1){
							$track  =  $key;
							foreach ($$key as $kex => $valx) {

								if(isset($valx[$token]) and $valx[$token] ==  0 and $valx[$track.'_stat'] == 0 ){
									$arr[] =  $db[0];

									//get word isinya index[0] => name  (hr,emp,sup) tayang  index[1] isis nya index $$key ( $key ==  hr) => $hr  

									$get_wordly = [$track,$kex];
									
									// ${$get_wordly[0]}[$get_wordly[1]][$get_wordly[0].'_stat']
									// |	    |          |                |
									// $        hr 		  [ 0 ]			   hr_stat			
								}else{
									$chat_arr  = [];
									foreach ($chatID as $key => $value) {
										if($value ==  "u" || $value ==  "r"){	
											$get_type  =   \DB::SELECT("select t1.created_at as created_at, t2.leave_type as type from  leave_request  as  t1,leave_type  as t2  where  t1.id  =  $idP and  t1.leave_type =  t2.id ");
										
											$select_chat  = \DB::SELECT("select * from command_center where id = $key  and employee_id  !=  '$getEMP'");
											if(isset($select_chat[0])){
												if($token  ==  $getEMP){
													if($arr  !=  null){
														foreach ($arr as $key => $value) {

															if($value['typeX']  ==  'chat'  and   $value['id'] != $select_chat[0]->request_id ){
																if($value ==  "u"){
																	$stat =  'unread';
																}else{
																	$stat = 'read';
																} 
													    		$arr[]  = [
														    		   		'typeX'    => 'chat',
														    		   		'id'       => $select_chat[0]->request_id,
														    		   		'comment'  => $select_chat[0]->comment,
														    		   		'filename' => $select_chat[0]->filename,
														    		   		'path'     => $select_chat[0]->path,
														    		   		'stat'	   => $stat,
														    		   		"created_at" => $get_type[0]->created_at,
														    		   		"type" => $get_type[0]->type,
														    		    ];
															}	
														}
													}else{
														if($value ==  "u"){
																	$stat =  'unread';
																}else{
																	$stat = 'read';
																} 
														$arr[]  = [
											    			 		'typeX' => 'chat',
											    			 		'id'    => $select_chat[0]->request_id,
											    			 		'comment' =>  $select_chat[0]->comment,
											    			 		'filename' => $select_chat[0]->filename,
											    			 		'path'     =>  $select_chat[0]->path,
											    			 		'stat'	   => $stat,
											    			 		"created_at" => $get_type[0]->created_at,
														    		"type" => $get_type[0]->type,
												    		    ];
													}
												}
											}
										}
									}
								}								
							}
						}
						if($value ==  2){
							foreach ($$key as $kex => $valx) {
								if(isset($valx[$token]) and  $valx[$token] ==  0 and $valx[$key.'_stat'] == 0  and ${$get_wordly[0]}[$get_wordly[1]][$get_wordly[0].'_stat'] ==  1){
									$arr[] =  $db[0];
									$get_wordly = [$key,$kex];
								   // return $array_key_exist[$token,]
								}
							}						
						}
						if($value ==  3){
							foreach ($$key as $kex => $valx) {
								if(isset($valx[$token]) and  $valx[$token] ==  0 and $valx[$key.'_stat'] == 0  and ${$get_wordly[0]}[$get_wordly[1]][$get_wordly[0].'_stat'] ==  1){
									$arr[] =  $db[0];
									//$get_wordly = [$key,$kex];
								   // return $array_key_exist[$token,]
								}
							}						
						}
					}
				}	


		   	}else{
		    	 $idP =  $value->id_req;
		    	 $db  =  \DB::SELECT(
				"select distinct t1.id,t1.date_request,t1.availment_date,t6.employee_id as empreq,t6.swap_with as empswap,
				 concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name)as employee,t1.employee_id,t3.type_id,
				 t3.type,t1.status_id,t4.status,t1.request_id,date_format(t1.update_at,'%Y-%m-%d') as date_, 
				 date_format(t1.update_at,'%H:%s') as time_,t1.approval,t1.approver,
				 /*-----------------for time in out request------------t7*/
				 t7.req_in,t7.req_out,t7.req_in_out_id, 		 			 
			 	 /*  ----------- for training and ob------------------ t8*/
			 	 t8.id as id_training,
			 	 t8.start_ as start_training,
			 	 t8.end as end_training,
			 	 t8.created_at as date_request_training,
			 	 /*--------------for overtime ----------------------- t11*/
			 	 t11.id as overtime_id,
			 	 t11.date_str as date_overtime,
			 	 (select shift_code from attendance_work_shifts where t11.id_shift = shift_id) as shift_code_overtime,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where t11.id_shift = shift_id) as time_shift_code_overtime,
			 	 t11.overtime_str as overtime_start,
			 	 t11.overtime_end as overtime_end,
			 	 t11.total_overtime as overtime_total,
			 	 t11.created_at as date_request_overtime,
			 	 /*--------------for undertime  --------------------- t15*/
			 	 t15.id as id_undertime,
			 	 t15.date as date_undertime,
			 	 (select shift_code from attendance_work_shifts where t15.shift_id = shift_id) as shift_code_undertime,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where t15.shift_id = shift_id) as time_shift_code_undertime,
			 	 t15.work_end_time as end_time_request_undertime,
			 	 t15.short as date_diff_end_time_and_shift_time,
			 	 t15.created_at as date_request_undertime,
			 	 /*-----------------for change shift----------------- t12 */
			 	 t12.id as id_change_shift,
			 	 date_format(t12.date,'%y-%m-%d') as date_request_change_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t12.old_shift ) as old_shift_change_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t12.new_shift ) as new_shift_change_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t12.old_shift ) as time_old_shift_change_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t12.new_shift ) as time_new_shift_change_shift,
			 	 t12.created_at as date_request_change_shift,
			 	 /*-----------------for swap shift--------------------- t13*/
			 	 t13.swap_id as  id_swap_shift,
			 	 t13.date  as date_swap_shift,
			 	 (select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) from emp where employee_id  = t13.swap_with) as employee_swap_for_swap_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t13.old_shift_id ) as old_shift_swap_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t13.new_shift_id ) as new_shift_swap_shift,
			 	  (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t13.old_shift_id ) as time_old_shift_swap_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t13.new_shift_id ) as time_new_shift_swap_shift,
			 	 t13.created_at as date_request_swap_shift,
			 	 /*-----------------for late / for early_out------------------------- t14*/
			 	 t14.date  as dateLate,
			 	 t14.late_id as lateId,
			 	 t14.late as lateTime,
			 	 t14.early_out as earlyOut,
			 	 t14.created_at as datecreated_att,
			 	 (select id from pool_request where id_req = t1.id) as id_pool
			 	 from att_schedule_request t1
				 left join emp t2 on t2.employee_id=t1.employee_id 
				 left join att_type t3 on t3.type_id=t1.type_id 
				 left join att_late t14 on t14.type_id =  t1.type_id and t14.employee_id =  t1.employee_id and t14.late_id =  t1.request_id
				 left join att_undertime t15 on t15.type_id =  t1.type_id and t15.employee_id =  t1.employee_id
				 left join att_overtime t11 on t11.type_id=t1.type_id and t11.employee_id =  t1.employee_id
				 left join att_change_shift t12 on t12.type_id=t1.type_id and t12.employee_id =  t1.employee_id
				 left join att_swap_shift t13 on t13.type_id=t1.type_id and t13.employee_id =  t1.employee_id and t13.swap_id = t1.request_id
				 left join att_training t8 on t8.type_id=t1.type_id and t8.employee_id =  t1.employee_id
				 left join att_time_in_out_req t7 on t7.type_id = t1.type_id and t7.employee_id =  t1.employee_id
				 left join att_status t4 on  t4.status_id=t1.status_id 
				 left join att_swap_shift t6 on t6.swap_id=t1.request_id and t6.employee_id =  t1.employee_id
				 left join command_center t17 on t17.request_id = t1.id 
				 where t1.id = $idP order by t1.id asc");
				
				// if(isset($db[0]) and $db[0]->status_id  !=  1 and  $db[0]->employee_id != $token and  $whoiam != null){
				//  	$arr[] =  $db[0];
				// }
				// 
				
			
				if(isset($db[0])){
					//$db[0]->master =  'schedule'; 
					$getEMP  =  $db[0]->employee_id;
					$get_wordly =  null;
					//cari sttatus aprove atau pending approval  
					foreach ($req_flow as $key => $value) {
						$test  = $key;
						if($value ==  1){
							$key_index  =  $key;
							foreach ($$key as $kex => $valx) {
								if(isset($valx[$token]) and $valx[$token] ==  0 and $valx[$key_index.'_stat'] == 0 ){
									$db[0]->typeX =  'notif';
									$arr[] =  $db[0];

									//get word isinya index[0] => name  (hr,emp,sup) tayang  index[1] isi nya index $$key ( $key ==  hr) => $hr  
									$get_wordly = [$test,$kex];
									
									// ${$get_wordly[0]}[$get_wordly[1]][$get_wordly[0].'_stat']
									// |	    |          |                |
									// $        hr 		  [ 0 ]			   hr_stat			
								}else{	
									$chat_arr  = [];
									foreach ($chatID as $key => $value) {
										if($value ==  "u" || $value ==  "r"){
										
											$get_type  =   \DB::SELECT("select t1.availment_date as created_at, t2.type as type from  att_schedule_request as  t1,att_type  as t2  where  t1.id  =  $idP and  t1.type_id =  t2.type_id ");
											$select_chat  = \DB::SELECT("select * from command_center where id = $key  and employee_id  !=  '$getEMP'");
											if(isset($select_chat[0])){
												if($token  ==  $getEMP){
													if($arr  !=  null){
														foreach ($arr as $key => $value) {
															if($value['typeX']  ==  'chat'  and   $value['id'] != $select_chat[0]->request_id ){
																if($value ==  "u"){
																	$stat =  'unread';
																}else{
																	$stat = 'read';
																} 
													    		$arr[]  = [
														    		   		'typeX'    => 'chat',
														    		   		'id'       => $select_chat[0]->request_id,
														    		   		'comment'  => $select_chat[0]->comment,
														    		   		'filename' => $select_chat[0]->filename,
														    		   		'path'     => $select_chat[0]->path,
														    		   		'stat'	   => $stat,
														    		   		"created_at" => $get_type[0]->created_at,
														    		   		"type" => $get_type[0]->type,
														    		    ];
															}	
														}
													}else{
														if($value ==  "u"){
																	$stat =  'unread';
																}else{
																	$stat = 'read';
																} 
														$arr[]  = [
											    			 		'typeX' => 'chat',
											    			 		'id'    => $select_chat[0]->request_id,
											    			 		'comment' =>  $select_chat[0]->comment,
											    			 		'filename' => $select_chat[0]->filename,
											    			 		'path'     =>  $select_chat[0]->path,
											    			 		'stat'	   => $stat,
											    			 		"created_at" => $get_type[0]->created_at,
														    		"type" => $get_type[0]->type,
												    		    ];
													}
												}
											}
										}
									}
								}							
							}
						}

						$check_wordly = $get_wordly;
						if($value ==  2){

							foreach ($$key as $kex => $valx) {	
								if(isset($valx[$token]) and  $valx[$token] ==  0 and $valx[$key.'_stat'] == 0  and ${$get_wordly[0]}[$get_wordly[1]][$get_wordly[0].'_stat'] ==  1){
									$arr[] =  $db[0];
									$get_wordly = [$key,$kex];
								   // return $array_key_exist[$token,]
								}else{	
									$chat_arr  = [];
									foreach ($chatID as $key => $value) {

										if($value ==  "u" || $value ==  "r"){
											$get_type  =   \DB::SELECT("select t1.availment_date as created_at, t2.type as type from  att_schedule_request as  t1,att_type  as t2  where  t1.id  =  $idP and  t1.type_id =  t2.type_id ");

											$select_chat  = \DB::SELECT("select * from command_center where id = $key  and employee_id  !=  '$getEMP'");
											if(isset($select_chat[0])){
												if($token  ==  $getEMP){

													if($arr  !=  null){
														foreach ($arr as $key => $value) {
															if($value['typeX']  ==  'chat'  and   $value['id'] != $select_chat[0]->schedule_request_id ){
																if($value ==  "u"){
																	$stat =  'unread';
																}else{
																	$stat = 'read';
																} 
													    		$arr[]  = [
														    		   		'typeX'    => 'chat',
														    		   		'id'       => $select_chat[0]->schedule_request_id,
														    		   		'comment'  => $select_chat[0]->comment,
														    		   		'filename' => $select_chat[0]->filename,
														    		   		'path'     => $select_chat[0]->path,
														    		   		'stat'	   => $stat,
														    		   		"created_at" => $get_type[0]->created_at,
														    		   		"type" => $get_type[0]->type,
														    		    ];
															}	
														}
													}else{
														if($value ==  "u"){
																	$stat =  'unread';
																}else{
																	$stat = 'read';
																} 
														$arr[]  = [
											    			 		'typeX' => 'chat',
											    			 		'id'    => $select_chat[0]->schedule_request_id,
											    			 		'comment' =>  $select_chat[0]->comment,
											    			 		'filename' => $select_chat[0]->filename,
											    			 		'path'     =>  $select_chat[0]->path,
											    			 		'stat'	   => $stat,
											    			 		"created_at" => $get_type[0]->created_at,
														    		"type" => $get_type[0]->type,
												    		    ];														
													}
												}
											}
										}
									}
								}
							}						
						}
						if($value ==  3){
							foreach ($$key as $kex => $valx) {
								if(isset($valx[$token]) and  $valx[$token] ==  0 and $valx[$key.'_stat'] == 0  and ${$get_wordly[0]}[$get_wordly[1]][$get_wordly[0].'_stat'] ==  1){
									$arr[] =  $db[0];
									//$get_wordly = [$key,$kex];
								   // return $array_key_exist[$token,]
								}else{	

									$chat_arr  = [];
									foreach ($chatID as $key => $value) {

										if($value ==  "u" || $value ==  "r"){
											$get_type  =   \DB::SELECT("select t1.availment_date as created_at, t2.type as type from  att_schedule_request as  t1,att_type  as t2  where  t1.id  =  $idP and  t1.type_id =  t2.type_id ");

											$select_chat  = \DB::SELECT("select * from command_center where id = $key  and employee_id  !=  '$getEMP'");
											if(isset($select_chat[0])){
												if($token  ==  $getEMP){

													if($arr  !=  null){
														foreach ($arr as $key => $value) {
															if($value['typeX']  ==  'chat'  and   $value['id'] != $select_chat[0]->schedule_request_id ){
																if($value ==  "u"){
																	$stat =  'unread';
																}else{
																	$stat = 'read';
																} 
													    		$arr[]  = [
														    		   		'typeX'    => 'chat',
														    		   		'id'       => $select_chat[0]->schedule_request_id,
														    		   		'comment'  => $select_chat[0]->comment,
														    		   		'filename' => $select_chat[0]->filename,
														    		   		'path'     => $select_chat[0]->path,
														    		   		'stat'	   => $stat,
														    		   		"created_at" => $get_type[0]->created_at,
														    		   		"type" => $get_type[0]->type,
														    		    ];
															}	
														}
													}else{
														if($value ==  "u"){
																	$stat =  'unread';
																}else{
																	$stat = 'read';
																} 
														$arr[]  = [
											    			 		'typeX' => 'chat',
											    			 		'id'    => $select_chat[0]->schedule_request_id,
											    			 		'comment' =>  $select_chat[0]->comment,
											    			 		'filename' => $select_chat[0]->filename,
											    			 		'path'     =>  $select_chat[0]->path,
											    			 		'stat'	   => $stat,
											    			 		"created_at" => $get_type[0]->created_at,
														    		"type" => $get_type[0]->type,
												    		    ];
														
													}
												}
											}
										}
									}
								}
							}						
						}
					}
				}
		    } 
		}


		return $arr;

	}


	public function get_chat (){
		$key =  explode('-',base64_decode((\Input::get('key'))))[1];
		$id =  \Input::get('id');
		if($id == null ||  $id == "undefined"){		
			return response()->json(['header' =>['message' => 'id not  found', 'status' => 500],'data' =>[]],200);
		}else{
			$key  = explode('-',(base64_decode(\Input::get('key'))))[1];
			$master =  \Input::get('master_type');
			$type =  \Input::get('type_id');
			$chat  =   \DB::SELECT("select *  from command_center where request_id  = $id and master_type = $master and type_id = $type");
			foreach ($chat as $key => $value) {
				$EMP = $value->employee_id;
				$get_name_emp  =  \DB::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name from emp where employee_id = '$EMP' ");
				if($value->employee_id ==  '2014888'){
					$value->side = "force";
				
				}else{
					$value->side ="dark_side";
				}

					$value->comm_name = $get_name_emp[0]->name;
			}
			return response()->json(['header' =>['message' => 'success load data', 'status' => 200],'data' =>$chat],200);
		}

	}

	public function sent_message(){
		$key =  explode('-',base64_decode((\Input::get('key'))))[1];
		$id  =  \Input::get('id');
		$comment = \Input::get('comment');
		if(!isset($id) || $id == null){
			return  response()->json(['header' =>['message' => 'id not found', 'status' => 500],'data' => []],500);
		}else{
			try{
				$insert  = \DB::SELECT("insert into  command_center(request_id,comment,employee_id,created_at) values($id,'$comment','$key',now())");
				$get_last_id  = \DB::SELECT("select * from command_center order by id desc limit 1")[0]->id;

				$get_pool_requets  = \DB::SELECT("select json_data from  pool_request where id_req  =   $id");
				$data =   json_decode($get_pool_requets[0]->json_data,1);//
				$data['chat_id'][$get_last_id] = 'u';

				$data =  json_encode($data);

				$update  = \Db::SELECT("update pool_request set json_data = '$data' where  id_req =   $id");
			}catch(\Exeception $e){
				$insert  = \DB::SELECT("insert into  command_center(schedule_request_id,comment,employee_id,created_at) values($id,'$comment','$key',now())");				
			}
		}
	}

	public function read_stat(){
		return ;
		$i   = \Input::all();
		$id  =  $i['id'];
		$key =  explode('-',base64_decode(($i['key'])))[1];

		$pool = \DB::SELECT("select json_data from pool_request where id_req =  $id ");
		$pool = json_decode($pool[0]->json_data,1);

		//req_flow
		//$req_flow = json_decode($pool[0]->json_data,1);

		foreach ($pool['chat_id'] as $keyx => $valuex) {
			$get  =   \DB::SELECT("select employee_id from command_center where id  = $keyx");
			if($get[0]->employee_id !=  $key){
				$pool['chat_id'][$keyx] = 'r';
			}
		}

		$keygen  = '';
		foreach ($pool['req_flow'] as $keyx => $valuex) {
			if($valuex == 1){
				$count  = count($pool[$keyx]);
				for ($i=0; $i < $count ; $i++) { 
					if(isset($pool[$keyx][$i][$key])){
						if($pool[$keyx][$i][$keyx.'_stat'] == 1){
							$keygen  =  1;
						}
						$pool[$keyx][$i]['read_stat'] =  1;	
					}
				}
			}

			if($valuex == 2){
				$count  = count($pool[$keyx]);
				for ($i=0; $i < $count ; $i++) { 
					if(isset($pool[$keyx][$i][$key]) and $keygen == 1){
						if($pool[$keyx][$i][$keyx.'_stat'] == 1){
							$keygen  =  1;
						}
						$pool[$keyx][$i]['read_stat'] =  1;	
					}
				}
			}

			if($valuex == 3){
				$count  = count($pool[$keyx]);
				for ($i=0; $i < $count ; $i++) { 
					if(isset($pool[$keyx][$i][$key]) and $keygen == 1){
						if($pool[$keyx][$i][$keyx.'_stat'] == 1){
							$keygen  =  1;
						}
						$pool[$keyx][$i]['read_stat'] =  1;	
					}
				}
			}
		}
		
		$data =  json_encode($pool);
		$update  = \Db::SELECT("update pool_request set json_data = '$data' where  id_req =   $id");
		
		$input =   \Input::get('id');

		//select * from pool_request

		// BACKUP ISSUE EMPLOYEE_ID NOT DECLARE
		/*$pool_request_data = \DB::SELECT("select  
				(case 
				WHEN t1.master_type = 2
				THEN
					(select  leave_type from leave_type where t1.type_id = leave_type.id limit 1)
				WHEN t1.master_type = 1
				THEN
					(select type   from att_type  where  t1.type_id =   att_type.type_id limit 1)
				else
					1
				end)as type, t1.master_type,concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name) as name,
				t2.work_email,t2.personal_email
				from pool_request as t1, emp as t2 where t1.id = $input and t2.employee_id = t1.employee_id limit 1 ");

		if($pool_request_data[0]->master_type  == 1){
			$employee_id  =  $pool_request_data[0]->employee_id;
			$set_email   =  ($pool_request_data[0]->work_email  == null ?  $pool_request_data[0]->work_mail :  ($pool_request_data[0]->personal_email !=  null ? $pool_request_data[0]->personal_email :  null));

			$namex  =  $pool_request_data[0]->name;
			$request =  $pool_request_data[0]->type;
			$email   =  $set_email;
		}else{
			$employee_id  =  $pool_request_data[0]->employee_id;
			$set_email   =  ($pool_request_data[0]->work_email  == null ?  $pool_request_data[0]->work_mail :  ($pool_request_data[0]->personal_email !=  null ? $pool_request_data[0]->personal_email :  null));

			$namex  =  $pool_request_data[0]->name;
			$request =  $pool_request_data[0]->type;
			$email   =  $set_email;
		}

		if($email != null){
			$data = array('name'=>$namex,"request" => $request, 'email' =>  $email);
			\Mail::send('emails.employee', $data, function($message) use ($data){
				$message->to($data['email'])->subject('Leekie request reminder');
			});
		}*/

		return response()->json(['header' => ['message' => 'success' , 'status' => 200],'data' =>[]],200);
	}


	public function approve(){
		$master  =  \Input::get('master');
		$id   = \Input::get('id');
		
		if($master  ==   "schedule"){
		 	$schedule  = \DB::SELECT("select * from pool_request  where id_req = $id");

		}else{
			$schedule  = \DB::SELECT("select * from leave_request  where id_req = $id");
		}


	}

	// public function  reject(){

	// }

	// public function reject(){

	// }








	
}