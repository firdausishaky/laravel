<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Exeception;
use DateTime;
use DateTimeZone;

class notificationV3 extends Controller {

	/**
	 * @param  [type]
	 * @return [type]
	 */
	

	public function getUser($key = null){
		if($key == null){
			$key = \Input::get('key');
		}
		$keys= base64_decode($key);
		$test = explode('-',$keys);
		return $data = $test[1];
	}


	public function leave($id){
	     $db = \DB::SELECT("select  distinct
							concat(t1.first_name,' ',t1.middle_name,' ',t1.last_name)as employee, 
							t2.employee_id,  
							t2.from_ , 
							t2.to_ , 
							t4.status,
							t4.status_id,
							t2.taken_day, 
							t2.balance_day, 
							t2.taken_day_approval, 
							t2.taken_day_approver, 
							t2.created_at, 
							t2.approval,
							t2.approver,
							t3.leave_type,
							t2.id,
							if(t20.filename = null,'null',t20.filename),
							'leaved' as master
							from emp t1 
							left join emp_picture  t20 on t20.employee_id  =  t1.employee_id
							left join leave_request t2  on  t2.employee_id  = t1.employee_id 
							left join leave_type t3 on t3.id  = t2.leave_type
							left join att_status t4 on  t4.status_id =  t2.status_id
							where t2.id  = $id ");

		if($db ==  null){
			$arr  = ['data' => [], 'message' => 'empty data'];
		}else{
			$arr  = ['data' => $db, 'message' => 'success'];
		}

		return $arr;
	}

	public function  schedule($id){
		$db  =  \DB::SELECT(
				"select distinct t1.id,t1.date_request,t1.availment_date,t6.employee_id as empreq,t6.swap_with as empswap,
				 concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name)as employee,t1.employee_id,t3.type_id,
				 t3.type,t1.status_id,t4.status,t1.request_id,date_format(t1.update_at,'%Y-%m-%d') as date_, 
				 date_format(t1.update_at,'%H:%s') as time_,
				 t99.time_in as timeIn_device,
				 t99.time_out as timeOut_device,
				 concat('(',aws.shift_code,')',date_format(aws._from,'%H:%s'),' - ',date_format(aws._to,'%H:%s')) as current_schedule,
				 /*-----------------for time in out request------------t7*/
				 t7.req_in,t7.req_out,t7.req_in_out_id, 		 			 
			 	 /*  ----------- for training and ob------------------ t8*/
			 	 t8.id as id_training,
			 	 t8.start_ as start_training,
			 	 t8.end as end_training,
			 	 t8.created_at as date_request_training,
			 	 /*--------------for overtime ----------------------- t11*/
			 	 t11.id as overtime_id,
			 	 t11.date_str as date_overtime,
			 	 (select shift_code from attendance_work_shifts where t11.id_shift = shift_id) as shift_code_overtime,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where t11.id_shift = shift_id) as time_shift_code_overtime,
			 	 t11.overtime_str as overtime_start,
			 	 t11.overtime_end as overtime_end,
			 	 t11.total_overtime as overtime_total,
			 	 t11.created_at as date_request_overtime,
			 	 /*--------------for undertime  --------------------- t15*/
			 	 t15.id as id_undertime,
			 	 t15.date as date_undertime,
			 	 (select shift_code from attendance_work_shifts where t15.shift_id = shift_id) as shift_code_undertime,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where t15.shift_id = shift_id) as time_shift_code_undertime,
			 	 t15.work_end_time as end_time_request_undertime,
			 	 t15.short as date_diff_end_time_and_shift_time,
			 	 t15.created_at as date_request_undertime,
			 	 /*-----------------for change shift----------------- t12 */
			 	 t12.id as id_change_shift,
			 	 date_format(t12.date,'%y-%m-%d') as date_request_change_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t12.old_shift ) as old_shift_change_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t12.new_shift ) as new_shift_change_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t12.old_shift ) as time_old_shift_change_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t12.new_shift ) as time_new_shift_change_shift,
			 	 t12.created_at as date_request_change_shift,
			 	 /*-----------------for swap shift--------------------- t13*/
			 	 t13.swap_id as  id_swap_shift,
			 	 t13.date  as date_swap_shift,
			 	 (select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) from emp where employee_id  = t13.swap_with) as employee_swap_for_swap_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t13.old_shift_id ) as old_shift_swap_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t13.new_shift_id ) as new_shift_swap_shift,
			 	  (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t13.old_shift_id ) as time_old_shift_swap_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t13.new_shift_id ) as time_new_shift_swap_shift,
			 	 t13.created_at as date_request_swap_shift,
			 	 /*-----------------for late / for early_out------------------------- t14*/
			 	 t14.date  as dateLate,
			 	 t14.late_id as lateId,
			 	 t14.late as lateTime,
			 	 t14.early_out as earlyOut,
			 	 t14.created_at as datecreated_att,
			 	 (select id from pool_request where id_req = t1.id) as id_pool,
				 t1.approval,t1.approver,
				 if(t20.filename = null,'null',t20.filename)
			 	 from att_schedule_request t1
				 left join emp t2 on t2.employee_id=t1.employee_id 
				 left join biometrics_device t99 on t99.date = t1.availment_date and t99.employee_id = t1.employee_id
				 left join att_schedule t98 on t98.date = t1.availment_date and t98.employee_id = t1.employee_id
				 left join attendance_work_shifts aws on aws.shift_id = t98.shift_id
				 left join att_type t3 on t3.type_id=t1.type_id 
				 left join emp_picture t20 on t20.employee_id = t2.employee_id
				 left join att_late t14 on t14.type_id =  t1.type_id and t14.employee_id =  t1.employee_id and t14.late_id =  t1.request_id and t14.date = t1.availment_date
				 left join att_undertime t15 on t15.type_id =  t1.type_id and t15.employee_id =  t1.employee_id and t15.date = t1.availment_date
				 left join att_overtime t11 on t11.type_id=t1.type_id and t11.employee_id =  t1.employee_id and t11.date_str = t1.availment_date
				 left join att_change_shift t12 on t12.type_id=t1.type_id and t12.employee_id =  t1.employee_id and t12.date = t1.availment_date
				 left join att_swap_shift t13 on t13.type_id=t1.type_id and t13.employee_id =  t1.employee_id and t13.swap_id = t1.request_id and t13.date = t1.availment_date
				 left join att_training t8 on t8.type_id=t1.type_id and t8.employee_id =  t1.employee_id and t8.start_ = t1.availment_date
				 left join att_time_in_out_req t7 on t7.type_id = t1.type_id and t7.employee_id =  t1.employee_id and t7.date = t1.availment_date
				 left join att_status t4 on  t4.status_id=t1.status_id 
				 left join att_swap_shift t6 on t6.swap_id=t1.request_id and t6.employee_id =  t1.employee_id
				 left join command_center t17 on t17.request_id = t1.id 
				 where t1.id = $id order by t1.id asc");
	
		if($db ==  null){
			$arr  = ['data' => [], 'message' => 'empty data'];
		}else{
			$arr  = ['data' => $db, 'message' => 'success'];
		}
		return $arr;
	}


	/**
	 * function insert notification join from schedule and  leave
	 * @param  [ids] => [int]  parameter darri att_scheduleR_equest , atau leave_request
 	 * @param  [type] => [int] parameter type 
	 * @param  [employee_id] => employee_id
	 * @param  [local] => local_it  from employee_id 
	 * @param  [user] => find out she/her  
	 * @param  [from_type] => attendance / schedule '
	 *
	 *
	 *	local_it 
	 *	if : 1   lexpat  
	 *	if  :2   local /local_it
	 * 
	 * 
	 *  attendance
	 *  1 / training
	 *  2 / ob
	 *  3 / overtime
	 *  4 /undertime
	 *  5 / chnageshift
	 *  6 / swapshift
	 *  7 / late
	 *  8 /earlyout
	 *  9 / time in/out
	 *
	 *  leave
	 *  1 / birth leave (bl)
	 *  2 / vacation leave (vl)
	 *  3 / enhannce leave (E(VL))
	 *  4 / sick leave (sl)
	 *  5 / maternty leave (ML)
	 *  6 / paternity
	 *  7 / Bereavement leave (BE)
	 *  8 / Marriage Leave (LE)
	 *  9 / offday oncall (OC)
	 *  10 /  acumulation day offf  (ADO)
	 *  11 / ememrgeny leave  (EL)
	 *  12  / suspension leave  (SL) 
	 */
	


public function sent_message(){
		$key =  explode('-',base64_decode((\Input::get('key'))))[1];
		$id  =  \Input::get('id');
		$comment = \Input::get('comment');
		$employee_id  = \Input::get('employee_id');
		$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
		$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

		$namex  =  \Input::get('employee');
		$request =  \Input::get('type');
		$email   =  $set_email;
		if(strlen($set_data[0]->work_email) > 0){
			$email   =  $set_data[0]->work_email;
		}else{
			if(strlen($set_data[0]->personal_email) > 0){
				$email   =  $set_data[0]->personal_email;	
			}else{
				$email = null;
			}
		}
		if($email != null){
			$data = array('name'=>$namex,"request" => $request, 'email' => $email);
			\Mail::send('emails.approve', $data, function($message) use ($data){
				$message->to($data['email'])->subject('Leekie request reminder');
			});
		}
		//return "OK";
		if(!isset($id) || $id == null){
			return  response()->json(['header' =>['message' => 'id not found', 'status' => 500],'data' => []],500);
		}else{
			try{
				$insert  = \DB::SELECT("insert into  command_center(request_id,comment,employee_id,created_at) values($id,'$comment','$key',now())");
				$get_last_id  = \DB::SELECT("select * from command_center order by id desc limit 1")[0]->id;

				$get_pool_requets  = \DB::SELECT("select json_data,employee_id from  pool_request where id_req  =   $id");
				$data =   json_decode($get_pool_requets[0]->json_data,1);//
				$data['chat_id'][$get_last_id] = 'u';
				$data =  json_encode($data);

				$employee_id  = \Input::get('employee_id');
				$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
				$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

				$namex  =  \Input::get('employee');
				$request =  \Input::get('type');
				$email   =  $set_email;
				if(strlen($set_data[0]->work_email) > 0){
					$email   =  $set_data[0]->work_email;
				}else{
					if(strlen($set_data[0]->personal_email) > 0){
						$email   =  $set_data[0]->personal_email;	
					}else{
						$email = null;
					}
				}
				if($email != null){
					$data = array('name'=>$namex,"request" => $request, 'email' =>  $email);
					\Mail::send('emails.approve', $data, function($message) use ($data){
						$message->to($data['email'])->subject('Leekie request reminder');
					});
				}

				$update  = \Db::SELECT("update pool_request set json_data = '$data' where  id_req =   $id");
			}catch(\Exeception $e){
				$insert  = \DB::SELECT("insert into  command_center(schedule_request_id,comment,employee_id,created_at) values($id,'$comment','$key',now())");				
			}
		}
	}
	
	private function get_subordinate($id){
		$result = \DB::select("SELECT distinct employee_id from emp_supervisor where supervisor='$id'");
		if(count($result) > 0){		
			$tmp = "'".$id."'";
			foreach ($result as $key => $value) {
				if($value->employee_id){
					$tmp .= ",'".$value->employee_id."'";
					//array_push($tmp, );
				}
			}
			return $tmp;
		}else{ return false; }
	}

	public function read_notif(){
		/**
		 * check token exist
		 */
		$token  = \Input::get('key');
		if($token ==  'undefined'){
			return response(['header' => ['message' => 'token undefined', 'status' => 301],'data' => []],301);
		}

		$token 			=  explode('-',base64_decode((\Input::get('key'))))[1];
		$subordinates 	= $this->get_subordinate($token);

		//$data  = \DB::SELECT("select * from pool_request where json_data like '%$token%' and DATE_SUB(now(),INTERVAL 7 DAY) ");
		$hr = \DB::SELECT("select ldap.employee_id from ldap, role where ldap.employee_id='$token' and ldap.role_id = role.role_id and (lower(role.role_name) like '%human%' or  '%human resource%' or '%hr%')");
		if(count($hr) > 0){
			$select_leave = \DB::SELECT("select pool_request.* from  pool_request,leave_request,emp
							where 
							leave_request.employee_id = emp.employee_id and
							pool_request.id_req = leave_request.id and 
							master_type = 2 and
							leave_request.status_id in(1,2,3) and 
							leave_request.created_at >= now()-interval 4 month");
			$select_schedule = \DB::SELECT("select pool_request.* from  pool_request,att_schedule_request,emp
							where 
							att_schedule_request.employee_id = emp.employee_id and
							pool_request.id_req = att_schedule_request.id and 
							master_type = 1 and
							att_schedule_request.status_id in(1,2,3) and
							att_schedule_request.date_request >= now()-interval 4 month");
			//$data = array_merge($select_leave,$select_schedule);
		}else{
			//$data  = \DB::SELECT("select * from pool_request where json_data like '%$token%' and DATE_SUB(now(),INTERVAL 7 DAY) ");
			$select_leave = \DB::SELECT("select pool_request.* from  pool_request,leave_request,emp
							where 
							emp.employee_id in($subordinates) and
							leave_request.employee_id = emp.employee_id and
							pool_request.id_req = leave_request.id and 
							master_type = 2 and
							leave_request.status_id in(1,2,3) and 
							leave_request.created_at >= now()-interval 4 month");
			$select_schedule = \DB::SELECT("select pool_request.* from  pool_request,att_schedule_request,emp
							where 
							emp.employee_id in($subordinates) and
							att_schedule_request.employee_id = emp.employee_id and
							pool_request.id_req = att_schedule_request.id and 
							master_type = 1 and
							att_schedule_request.status_id in(1,2,3) and
							att_schedule_request.date_request >= now()-interval 4 month");
		}
		$data = array_merge($select_leave,$select_schedule);
		$arr  = [];

		if($token == '2014888'){
			$data  = \DB::SELECT("select * from pool_request where json_data like '%$token%' and created_at >= now()-interval 4 month");
		}

		foreach ($data as $key => $value) {
			$index =  $key;
			$result =   json_decode($value->json_data,1);
			
			$ID  =  $value->id_req;
			$test   = [];
			$test[] = $ID;
			
			/**
			 * [res_temp] [arr] ['data' => [], 'message' => 'success']
			 */

			if($result['master'] ==  'schedule'){
				// if($value->id_req == 121){
				// 	return $ID;
				// }
				$res_temp = $this->schedule($ID);
			}else{
				$res_temp = $this->leave($ID);
			}
			
			if( $res_temp['data'] !=  null){
				$res[$index] =  $res_temp['data'][0];
						
				/**
				* dapetin aturan  flow request
			 	*/
				$res[$index]->master_type =   $value->master_type;
				foreach ($result['req_flow'] as $key => $value) {
					
					/**
					 * e.g  get_flow = ['sup' => 1]
					 */
					$get_last_app  = '';
					$get_approver  = 0;
					$xvalue = $value;


					/**
					 * value 11111111111111111111111111111111111111111111
					 * @var [type]
					 */
					if($xvalue == 1){

						$get_last_app = $key;
						/**
						 * user siapa saja yang bisa liahat message
						 * @var [position] =  $key {sup,hr,emp}
						 */
						$position 	  = $key;
						$GETuser  = $result[$key];

						/**
						 * manipulate  string if already read or already approved
						 * @var [$token] ==  1 already approved
						 * @var [.._stat] ==  1 already aproved
						 * @var [read_stat]  ==   1 already rady // notif minus 1
	  					 * @var [CHK_A_COND] == 'o' <- default  and  'x' <- default
	   					 */
		
						$CHK_A_COND =  $result['req_flow'][$position.'_approve'];
						
						if( $CHK_A_COND == 'o' || $CHK_A_COND == 'x'){
							$chat_count =  0;
							if(in_array($token,$result[$position.'x_comp'])){		
								//return ['token' => $token, 'user' => $GETuser];
								foreach ($GETuser as $key => $value) {
									
									if(isset($value[$token])){
										if($res[$index]->approval ==  null && $res[$index]->approver ==  null){
											$res[$index]->status_request  = "unapproved";
										}else if(isset($res[$index]->approval)  and $res[$index]->approval !=  null){
											if($res[$index]->approval != null){		
												if($res[$index]->approval ==  0 and $res[$index]->approval != 2){
													$res[$index]->status_request = 'approved';
												}elseif($res[$index]->approval ==  2 and   $res[$index]->approval  == 2){
													$res[$index]->status_request  = "approved"; 
												}elseif($res[$index]->approval ==  3 and $res[$index]->status_id != 1){
													$res[$index]->status_request =  "approved";
												}elseif($res[$index]->approver ==  3 and $res[$index]->status_id  !=  1){
													$res[$index]->status_request =  "approved";
												}elseif($res[$index]->approval ==  4 and $res[$index]->status_id  !=  1){
													$res[$index]->status_request  = "approved";
												}elseif($res[$index]->approver ==  4 and $res[$index]->status_id  !=  1){
													$res[$index]->status_request  = "approved";
												}else{
														if($res[$index]->status_id ==  1){
															$res[$index]->status_request  = "unapproved";
														}else{
															$res[$index]->status_request = "approved";
														}
												}
											}
										}
									}else{
										
										if(isset($res[$index]->status_request)  && $res[$index]->status_request  == "unapproved"){
												if($res[$index]->status_id ==  1){
														$res[$index]->status_request  = "unapproved";
												}else{
														$res[$index]->status_request = "approved";
												}
										}else{
											$res[$index]->status_request  = "approved";
										}
									}
								}
								
								/**
								 * CHECK CHAT ID REQUEST 
								 */
														
								$chat  = $result['chat_id'	];
								foreach ($chat as $key => $value) {
									if($value == 'u'){
										$_chat = \DB::SELECT("select* from command_center where id = $key ");
										if(isset($_chat[0]->employee_id) && $_chat[0]->employee_id != $token){
											$chat_count += 1;
										}
									}
								}

								$res[$index]->new_message = $chat_count;
								$res[$index]->typeX = 'request';
								$res[$index]->whoiam  = "prime";

								/**
								 * the old prime  have  power, because biggest power need bigger responsibilty
								 */
								$res[$index]->whoiam = 'prime';	
							}else{
							
								$notif_c = 0;				
								if($res[$index]->employee_id == $token){
									
									$chat  = $result['chat_id'];
									
									foreach ($chat as $key => $value) {
										if($value == 'u'){
											$_chat = \DB::SELECT("select * from command_center where id = $key ");
											foreach ($_chat as $key => $value) {
												if($value->employee_id != $token){
													$notif_c += 1;
												}
											}	
										}
									}
									if($res[$index]->approval ==  null and $res[$index]->approver == null){
										$res[$index]->can_cancle = 'yes';
									}else{
										$res[$index]->can_cancle = 'no';
									}

							
									$res[$index]->new_message = $notif_c;  
									$res[$index]->typeX = 'request';
									$res[$index]->whoiam  =  'fallen';
								}				
							}
						}
					}

					//return $value;
					
					if($xvalue ==  2 ){
						$res[$index]->typeX = 'request';
						
						$get_last_app = $key;
						/**
						 * user siapa sayaja yang bisa liahat message
						 * @var [position] =  $key {sup,hr,emp}
						 */
						$position 	  = $key;
						$GETuser  = $result[$key];

						/**
						 * manipulate  string if already read or already approved
						 * @var [$token] ==  1 already approved
						 * @var [.._stat] ==  1 already aproved
						 * @var [read_stat]  ==   1 already rady // notif minus 1
	  					 * @var [CHK_A_COND] == 'o' <- default  and  'x' <- default
	   					 */
	  					
						$CHK_A_COND =  $result['req_flow'][$position.'_approve'];
						
						if( $CHK_A_COND == 'o' || $CHK_A_COND == 'x'){
							$chat_count =  0;
							if(in_array($token,$result[$position.'x_comp'])){		
								
								foreach ($GETuser as $key => $value) {
									
									if(isset($value[$token])){
										// if($res[$index]->leave_type == 'Sick Leave'){
										// 	return 4;
										// }
										if(isset($res[$index]->approval)  and $res[$index]->approval !=  null){
											if($res[$index]->approval != null){		
												if($res[$index]->approval ==  0 and $res[$index]->approval != 2){
													$res[$index]->status_request = 'approved';
												}elseif($res[$index]->approval ==  2 and   $res[$index]->approval  == 2){
													$res[$index]->status_request  = "approved"; 
												}elseif($res[$index]->approval ==  3 and $res[$index]->status_id != 1){
													$res[$index]->status_request =  "approved";
												}elseif($res[$index]->approver ==  3 and $res[$index]->status_id  !=  1){
													$res[$index]->status_request =  "approved";
												}elseif($res[$index]->approval ==  4 and $res[$index]->status_id  !=  1){
													$res[$index]->status_request  = "approved";
												}elseif($res[$index]->approver ==  4 and $res[$index]->status_id  !=  1){
													$res[$index]->status_request  = "approved";
												}else{
														if($res[$index]->status_id ==  1){
															$res[$index]->status_request  = "unapproved";
														}else{
															$res[$index]->status_request = "approved";
														}
												}
											} 
										}
									}else{
										if(isset($res[$index]->status_request)  && $res[$index]->status_request  == "unapproved"){
												if($res[$index]->status_id ==  1){
														$res[$index]->status_request  = "unapproved";
												}else{
														$res[$index]->status_request = "approved";
												}
										}else{
											$res[$index]->status_request  = "approved";
										}
									}
								}
								/**
								 * CHECK CHAT ID REQUEST 
								 */
														
								$chat  = $result['chat_id'	];
								foreach ($chat as $key => $value) {
									if($value == 'u'){
										$_chat = \DB::SELECT("select* from command_center where id = $key ");
										if(isset($_chat[0])){
											
											if($_chat[0]->employee_id != $token){
												$chat_count += 1;
											}
										}
									}
								}

								$res[$index]->new_message = $chat_count;
								$res[$index]->typeX = 'request';	
							}else{
								if($res[$index]->employee_id == $token){
									$chat  = $result['chat_id'	];
									$notif_c = 0;
									foreach ($chat as $key => $value) {
										if($value == 'u'){
											$_chat = \DB::SELECT("select * from command_center where id = $key ");
											foreach ($_chat as $key => $value) {
												if($value->employee_id != $token){
													$notif_c += 1;
												}
											}
										}
									}

									if($res[$index]->approval ==  null and $res[$index]->approver ==  null){
										$res[$index]->can_cancle = 'yes';
									}else{
										$res[$index]->can_cancle = 'no';
									}

									if($res[$index]->approval ==  null and $res[$index]->approver ==  null){
										$res[$index]->can_cancle = 'yes';
									}else{
										$res[$index]->can_cancle = 'no';
									}

									$res[$index]->new_message = $notif_c;  
									$res[$index]->typeX = 'request';
									$res[$index]->whoiam  =  'fallen';
								}		
							}
						}
					}
				}
			
			}else{
				//KUDOS
			}

			// end foreach
		}

		if(isset($res)){
			return $res;
		}else{
			return [];
		}
	}


	public function notif($ids,$type,$employee_id,$local_it,$user,$from_type = null){

		/**
		 * for notif hr
		 */
		$check_su = \DB::SELECT("select  * from  view_nonactive_login where employee_id  =  '$employee_id'  and lower(role_name) =   'superuser'  ");


		$supervisor=  \DB::SELECT("select * from emp_supervisor where supervisor =  '$employee_id' ");
		$emp_x  = \DB::SELECT("select * from view_nonactive_login where employee_id  =   '$employee_id' and (lower(role_name) = 'regular employee user' or  lower(role_name) = 'user') ");
		//$hr = \DB::SELECT("select employee_id from  ldap, role where ldap.role_id = role.role_id and (lower(role.role_name) like '%human%' or  '%human resource%' or '%hr%')");
		if($local_it == 'expat' && $from_type != "attendance"){
			$hr = \DB::SELECT("select distinct job.title, emp.employee_id from emp, job_history, job where emp.employee_id = job_history.employee_id and job.id =  job_history.job and (lower(job.title) like '%it director%')");
		}else{
			$hr = \DB::SELECT("select distinct job.title, emp.employee_id from emp, job_history, job where emp.employee_id = job_history.employee_id and job.id =  job_history.job and ((lower(job.title) like '%hr%') or (lower(job.title) like '%human resource%') or (lower(job.title) like '%human%'))");
		}


		$hrx = [];
		$hrx_comp  = [];
		foreach($hr as $key => $value){
			$hrx[] = [ $value->employee_id => 0,
 					   "hr_stat" =>  0,
 					   "hr_date" => '0000-00-00',
 					   "hr_time" => "00:00",
 					   "read_stat" => 0,	
					 ]; 

			$hrx_comp[] = $value->employee_id; 
		}
		$hrx_comp[] = '2014888';

			$hrx[]  = ["2014888" => 0,
 					   "hr_stat" =>  0,
 					   "hr_date" => '0000-00-00',
 					   "hr_time" => "00:00",
 					   "read_stat" => 0,	
				  ];
		

		/**
		 * find who is  employee supervisor
		 */
		$sub = \DB::SELECT("select subordinate from emp_subordinate where employee_id = '$employee_id' ");
		
		$subx = [];
		$subx_comp = [];
		foreach($sub as $key => $value){
			$subx[] =  [ $value->subordinate => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
			$subx_comp[] = $value->subordinate; 
		}

		$subx_comp[] =  '2014888';

		$subx =  [ '2014888' => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ];

		/**
		 * find subordinate for this employee  id
		 * @var [type]
		 */
		
		$sup = \DB::SELECT("select supervisor from emp_supervisor where employee_id = '$employee_id' ");
		
		$supx = [];
		$supx_comp  = [];
		foreach($sup as $key => $value){
			$supx[] =  [ $value->supervisor => 0,
 					   	 "sup_stat" =>  0,
 					     "sup_date" => '0000-00-00',
 					     "sup_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
			$supx_comp[]  = $value->supervisor;
		}

		$supx_comp[] = '2014888';

		$supx[] = [ "2014888"  => 0,
 					"sup_stat" =>  0,
 					"sup_date" => '0000-00-00',
 					"sup_time" => "00:00",
 					"read_stat" => 0,	
				  ];
		
		/**
		 * slelecempoyee how will swap with the other employee 
		 * @var [type]
		 */
		

		$swap = \DB::SELECT("select swap_with from att_swap_shift where employee_id = '$employee_id' and swap_id = (select request_id from att_schedule_request where employee_id='$employee_id' and id = $ids and type_id = 6 order by id desc limit 1 )  ");
		
		$swapx = [];
		$swapx_comp = [];
		foreach($swap as $key => $value){
			$swapx[] =  [ $value->swap_with => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
			$swapx_comp[] =   $value->swap_with;
		}
		$swapx_comp[] =  '2014888';

		$swapx[] = [ "2014888" => 0,
 					 "swap_stat" =>  0,
 					 "swap_date" => "0000-00-00",
 					 "swap_time" => "00:00",
 					 "read_stat" => 0,	
					   ];

		
		/**
		 * if employee == attendace
		 * @var [type]
		 */

		//create manipulation user
	
		if($from_type == "attendance"){

			if($type == 9){

				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['hr' => 1,'swap' => '0','sup' => '0','hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
								"hr" => $hrx,
								"sup" => [],
								"swap" => [],
								"hrx_comp" => $hrx_comp,
								"supx_comp" => [],
								"empx_comp" => [],
						   ];
				}else{
					$master =  'schedule';
					$end = ['sup' => 1,'hr' => '0','swap' => '0','hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
								"sup" => $supx,
								"hr" => [],
								"swap" => [],
								"supx_comp" => $supx_comp,
								"hrx_comp" => [],
								"swapx_comp" => [],
									
						   ];
				}
			}


			if($type == 3){

				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['sup' => 1 ,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],
								
						];
				} 
			}
			if($type == 4){
				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['sup' => 1,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],
								
						]; 
				}
			}
			if($type == 7 || $type == 8){
				if($local_it == 'local'){
					$master =  'schedule';	
					$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => []/*$supx*/,
						"hr" => $hrx,
						"swap" => [],
						"supx_comp" => []/*$supx_comp*/,
						"hrx_comp" => $hrx_comp,
						"swapx_comp" => [],
								
						]; 
				}
			}
			if($type == 2){
				$master =  'schedule';
				$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],			
						
						]; 
			}

			if($type == 1){
				$master =  'schedule';
				$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],			
						
						]; 
			}
			if($type == 5){

				if($user == "sup"){
					$master =  'schedule';
					$end = ['swap' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
							"swap" => $subx,			
							"sup" => [],
							"hr" => [],
							"swapx_comp" => $subx_comp,			
							"supx_comp" => [],
							"hrx_comp" => [],
							
						   ]; 
				}else{
					$master =  'schedule';
					$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"empx_comp" => [],
								
						];
				}
			}

			if($type == 6){
				if($local_it == 'local'){
					if($supervisor != null){
						$master =  'schedule';
						$end = ['swap' => 1,'sup' => 2,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
								"swap" => $swapx,			
								"sup" => $supx,
								"hr" => [],
								"swapx_comp" => $swapx_comp,			
								"supx_comp" => $supx_comp,
								"hrx_comp" => [],
							   ];
					}else{
						$master =  'schedule';
						$end = ['swap' => 1,'sup' => 2,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
								"swap" => $swapx,			
								"sup" => $supx,
								"hr" => [],
								"swapx_comp" => $swapx_comp,			
								"supx_comp" => $supx_comp,
								"hrx_comp" => [],
							   ]; 
					}

		
				}
			}
		}else{


		//leave request 

			if($type == 2){
				if($local_it == 'local'){
					$master =  'leave';
					$end = ['hr' => 1,'sup' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
							"hr" => $hrx,
							"sup" => $supx,
							"swap" => [],
							"hrx_comp" => $hrx_comp,
							"supx_comp" => $supx_comp,
							"swapx_comp" => [],			
							
						   ]; 
				}else{
					$master =  'leave';
					$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
							"sup" => $supx,
							"swap" => [],			
							"hr" => [],
							"supx_comp" => $supx_comp,
							"swapx_comp" => [],			
							"hrx_comp" => [],
						   ];
				}
			}

			if($type == 10){
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
			}

			if($type == 3){
				$master =  'leave';
				if($local_it == 'local'){
					
					$end = ['hr' => 1,'sup' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"hr" => $hrx,
						"sup" => $supx,
						"swap" => [],
						"hrx_comp" => $hrx_comp,
						"supx_comp" => $supx_comp,
						"swapx_comp" => [],			
					
					   ];
				}	
			}

			if($type == 1){
				$master =  'leave';
				if($local_it == 'local'){
					$end = ['hr' => 1,'sup' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"hr" => $hrx,
						"sup" => [],
						"swap" => [],
						"hrx_comp" => $hrx_comp,
						"supx_comp" => [],
						"swapx_comp" => [],			
					
					   ];
				}
			}

			if($user == "hr" && $type == 4 ){
				if($local_it == 'local'){
					$master =  'leave';
					$end = ['sup' => 1,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_compx" => $supx_comp,
						"hrx_compx" => [],
						"swapx_compx" => [],			
					
					   ];
				}
				
			}
			
			if($type == 4 && $local_it == 'local'){
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_compx" => [],			
					
				   ];	
			}
			if($type == 4 && $local_it == 'expat'){
				$master =  'leave';
				$end = ['sup' => 1,'swap' => 0, 'hr' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"sup" => $supx,
					"hr" => [],
					"swap" => [],
					"supx_comp" => $supx_comp,
					"hrx_comp" => [],
					"swapx_comp" => [],


					
				   ];
			}

			if($type ==  5){
				$master =  'leave';
				if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['hr' => 1,'swap' => 0,'sup' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
						"hr" => $hrx,
						"sup" => [],
						"swap" => [],
						"hrx_comp" => $hrx_comp,
						"supx_comp" => [],
						"swapx_comp" => [],			
					
					   ];		
					}	
				}else{
					$master =  'leave';
					if($local_it == 'expat'){
						$end = ['sup' => 1,'hr' => 0,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],			
					
					   ];		
					}	
				}
			}

			if($type == 6){
				$master =  'leave';
				if($local_it == 'local'){
					$end = ['sup' => 1,'hr' => 2 ,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => $hrx_comp,
						"swapx_comp" => [],			
					
					   ];		
				}else{
					$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],			
					
					   ];
				}
			}

			if($type == 7){
				$master =  'leave';
				if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],		
							"supx_comp" => $supx_comp,
							"hrx_comp" => [],
							"swap" => [],			
							
					   ];		
					}
					if($local_it == 'expat'){
						$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],		
							"supx_comp" => $supx_comp,
							"hrx_comp" => [],
							"swap" => [],			
							
					   ];		
					}
				}
				else{
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"swap" => [],
							"supx_comp" => $supx,
							"hrx_comp" => $hrx,
							"swap" => [],			
							
					   ];		
					}else{
						$end = ['sup' => 1,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],
							"supx_comp" => $supx_comp,
							"hrx_comp" => [],
							"swapx_comp" => [],			
							
					   ];
					}
				}
			}

			if($type == 8){
				$master =  'leave';
				if($local_it == 'local'){
					$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"swap" => [],			
						"supx_comp" => $supx_comp,
						"hrx_comp" => $hrx_comp,
						"swapx_comp" => [],					
				   ];		
				}else{
					$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],			
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],
				   ];
				}
			}

			if($type == 11){
				$master =  'leave';
				if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],			
							"supx_comp" => $supx,
							"hrx_comp" => [],
							"swapx_comp" => [],		
					   ];		
					}
				}
				else{
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"swap" => [],
							"supx_comp" => $supx,
							"hrx_comp" => $hrx,
							"swapx_comp" => [],			
							
					   ];		
					}else{
						$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],
							"supx_comp" => $supx,
							"hrx_comp" => [],
							"swapx_comp" => [],			
							
					   ];
					}
				}
			}


			if($type == 12){
				$master =  'leave';
				if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['hr' =>  1 ,'sup' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"hr" => $hrx,
							"sup" => [],
							"swap" => [],
							"hrx_comp" => $hrx_comp,
							"supx_comp" => [],
							"swapx_comp" => [],			
							
					   ];		
					}
				}
			}

			if($type == 9){

				$master =  'leave';
				if($swap_x != null){
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"swap" => [],
							"supx_comp" => $supx_comp,
							"hrx_comp" => $hrx_comp,
							"swapx_comp" => [],			
							
					   ];		
					}
				}
			}


		}
		
		
		$chatID = [];
		if(isset($arr) && $arr  !=  null){
			$chat  = \DB::SELECT("select id from command_center where  request_id=  $ids");
			if($chat != null){
				foreach ($chat as $key => $value) {
					$chatID[$value->id]  = 'u';
				}	
			}
			
			$arr['requestor_stat'] =  [$employee_id => 0];
			$arr['chat_id'	] = $chatID;
			$arr['req_flow'] = $end;
			$arr['master'] =  $master ;
			$arr['local_it'] = $local_it;
		}else{
			return  $arr  =[ 'data' => [], 'message' =>  'your not allowed to request '];
		}



		//print_r($arr);
		//
		//if $from_type  attendance $from_type  = 1 else 2  
		//
		// 
		//
				
		$from_type =  ($from_type == 'attendance' ? 1 : 2);
		$json = json_encode($arr);
		try{
			$insert = \DB::SELECT("insert into pool_request(employee_id,id_req,type_id,master_type,json_data,created_at) 
								   values('$employee_id',$ids,'$type','$from_type','$json',now())");
			return $arr;
		}catch(\Exeception $e){
			return $e;
		}
	}

	public function notif_leave_only($ids,$type,$employee_id,$local_it,$user,$from_type = null){

		/**
		 * for notif hr
		 */
		$check_su = \DB::SELECT("select  * from  view_nonactive_login where employee_id  =  '$employee_id'  and lower(role_name) =   'superuser'  ");


		$supervisor=  \DB::SELECT("select * from emp_supervisor where supervisor =  '$employee_id' ");
		$emp_x  = \DB::SELECT("select * from view_nonactive_login where employee_id  =   '$employee_id' and (lower(role_name) = 'regular employee user' or  lower(role_name) = 'user') ");
		//$hr = \DB::SELECT("select employee_id from  ldap, role where ldap.role_id = role.role_id and (lower(role.role_name) like '%human%' or  '%human resource%' or '%hr%')");
		//$hr = \DB::SELECT("select distinct job.title, emp.employee_id from emp, job_history, job where emp.employee_id = job_history.employee_id and job.id =  job_history.job and ((lower(job.title) like '%hr%') or (lower(job.title) like '%human resource%') or (lower(job.title) like '%human%'))");
		if($local_it == 'expat' && !$from_type){
			$hr = \DB::SELECT("select distinct job.title, emp.employee_id from emp, job_history, job where emp.employee_id = job_history.employee_id and job.id =  job_history.job and (lower(job.title) like '%it director%')");
		}else{
			$hr = \DB::SELECT("select distinct job.title, emp.employee_id from emp, job_history, job where emp.employee_id = job_history.employee_id and job.id =  job_history.job and ((lower(job.title) like '%hr%') or (lower(job.title) like '%human resource%') or (lower(job.title) like '%human%'))");
		}

		$hrx = [];
		$hrx_comp  = [];
		foreach($hr as $key => $value){
			$hrx[] = [ $value->employee_id => 0,
 					   "hr_stat" =>  0,
 					   "hr_date" => '0000-00-00',
 					   "hr_time" => "00:00",
 					   "read_stat" => 0,	
					 ]; 

			$hrx_comp[] = $value->employee_id; 
		}
		$hrx_comp[] = '2014888';

			$hrx[]  = ["2014888" => 0,
 					   "hr_stat" =>  0,
 					   "hr_date" => '0000-00-00',
 					   "hr_time" => "00:00",
 					   "read_stat" => 0,	
				  ];
		

		/**
		 * find who is  employee supervisor
		 */
		$sub = \DB::SELECT("select subordinate from emp_subordinate where employee_id = '$employee_id' ");
		
		$subx = [];
		$subx_comp = [];
		foreach($sub as $key => $value){
			$subx[] =  [ $value->subordinate => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
			$subx_comp[] = $value->subordinate; 
		}

		$subx_comp[] =  '2014888';

		$subx =  [ '2014888' => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ];

		/**
		 * find subordinate for this employee  id
		 * @var [type]
		 */
		
		$sup = \DB::SELECT("select supervisor from emp_supervisor where employee_id = '$employee_id' ");
		
		$supx = [];
		$supx_comp  = [];
		foreach($sup as $key => $value){
			$supx[] =  [ $value->supervisor => 0,
 					   	 "sup_stat" =>  0,
 					     "sup_date" => '0000-00-00',
 					     "sup_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
			$supx_comp[]  = $value->supervisor;
		}

		$supx_comp[] = '2014888';

		$supx[] = [ "2014888"  => 0,
 					"sup_stat" =>  0,
 					"sup_date" => '0000-00-00',
 					"sup_time" => "00:00",
 					"read_stat" => 0,	
				  ];
		
		/**
		 * slelecempoyee how will swap with the other employee 
		 * @var [type]
		 */
		

		$swap = \DB::SELECT("select swap_with from att_swap_shift where employee_id = '$employee_id' and swap_id = (select request_id from att_schedule_request where employee_id='$employee_id' and id = $ids and type_id = 6 order by id desc limit 1 )  ");
		
		$swapx = [];
		$swapx_comp = [];
		foreach($swap as $key => $value){
			$swapx[] =  [ $value->swap_with => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
			$swapx_comp[] =   $value->swap_with;
		}
		$swapx_comp[] =  '2014888';

		$swapx[] = [ "2014888" => 0,
 					 "swap_stat" =>  0,
 					 "swap_date" => "0000-00-00",
 					 "swap_time" => "00:00",
 					 "read_stat" => 0,	
					   ];

		
		/**
		 * if employee == attendace
		 * @var [type]
		 */

		//create manipulation user
	
		if($from_type == "attendance"){

			if($type == 9){

				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['hr' => 1,'swap' => '0','sup' => '0','hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
								"hr" => $hrx,
								"sup" => [],
								"swap" => [],
								"hrx_comp" => $hrx_comp,
								"supx_comp" => [],
								"empx_comp" => [],
						   ];
				}else{
					$master =  'schedule';
					$end = ['sup' => 1,'hr' => '0','swap' => '0','hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
								"sup" => $supx,
								"hr" => [],
								"swap" => [],
								"supx_comp" => $supx_comp,
								"hrx_comp" => [],
								"swapx_comp" => [],
									
						   ];
				}
			}


			if($type == 3){

				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['sup' => 1 ,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],
								
						];
				} 
			}
			if($type == 4){
				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['sup' => 1,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],
								
						]; 
				}
			}
			if($type == 7 || $type == 8){
				if($local_it == 'local'){
					$master =  'schedule';	
					$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => $hrx_comp,
						"swapx_comp" => [],
								
						]; 
				}
			}
			if($type == 2){
				$master =  'schedule';
				$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],			
						
						]; 
			}

			if($type == 1){
				$master =  'schedule';
				$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],			
						
						]; 
			}
			if($type == 5){

				if($user == "sup"){
					$master =  'schedule';
					$end = ['swap' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
							"swap" => $subx,			
							"sup" => [],
							"hr" => [],
							"swapx_comp" => $subx_comp,			
							"supx_comp" => [],
							"hrx_comp" => [],
							
						   ]; 
				}else{
					$master =  'schedule';
					$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"empx_comp" => [],
								
						];
				}
			}

			if($type == 6){
				if($local_it == 'local'){
					if($supervisor != null){
						$master =  'schedule';
						$end = ['swap' => 1,'sup' => 2,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
								"swap" => $swapx,			
								"sup" => $supx,
								"hr" => [],
								"swapx_comp" => $swapx_comp,			
								"supx_comp" => $supx_comp,
								"hrx_comp" => [],
							   ];
					}else{
						$master =  'schedule';
						$end = ['swap' => 1,'sup' => 2,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
								"swap" => $swapx,			
								"sup" => $supx,
								"hr" => [],
								"swapx_comp" => $swapx_comp,			
								"supx_comp" => $supx_comp,
								"hrx_comp" => [],
							   ]; 
					}

		
				}
			}
		}else{


		//leave request 

			if($type == 2){
				if($local_it == 'local'){
					$master =  'leave';
					$end = ['hr' => 1,'sup' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
							"hr" => $hrx,
							"sup" => $supx,
							"swap" => [],
							"hrx_comp" => $hrx_comp,
							"supx_comp" => $supx_comp,
							"swapx_comp" => [],			
							
						   ]; 
				}else{
					// $master =  'leave';
					// $end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					// $arr = [
					// 		"sup" => $supx,
					// 		"swap" => [],			
					// 		"hr" => [],
					// 		"supx_comp" => $supx_comp,
					// 		"swapx_comp" => [],			
					// 		"hrx_comp" => [],
					// 	   ];
					$master =  'leave';
					$end = ['hr' => 1,'sup' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
							"hr" => $hrx,
							"sup" => $supx,
							"swap" => [],
							"hrx_comp" => $hrx_comp,
							"supx_comp" => $supx_comp,
							"swapx_comp" => [],			
							
						   ]; 
				}
			}

			if($type == 10){
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
				/*if($local_it == 'expat'){
					$arr['hr'] = [];
					$arr['hrx_comp'] = [];
					$end['hr']=0;
				}*/
				
			}

			if($type == 3){
				$master =  'leave';
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
				/*if($local_it == 'expat'){
					$arr['hr'] = [];
					$arr['hrx_comp'] = [];
					$end['hr']=0;
				}	*/
				/*if($local_it == 'local'){
					
					$end = ['hr' => 1,'sup' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"hr" => $hrx,
						"sup" => $supx,
						"swap" => [],
						"hrx_comp" => $hrx_comp,
						"supx_comp" => $supx_comp,
						"swapx_comp" => [],			
					
					   ];
				}*/	
			}

			if($type == 1){
				$master =  'leave';
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
				/*if($local_it == 'expat'){
					$arr['hr'] = [];
					$arr['hrx_comp'] = [];
					$end['hr']=0;
				}*/
				// if($local_it == 'local'){
				// 	$end = ['hr' => 1,'sup' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				// 	$arr = [
				// 		"hr" => $hrx,
				// 		"sup" => [],
				// 		"swap" => [],
				// 		"hrx_comp" => $hrx_comp,
				// 		"supx_comp" => [],
				// 		"swapx_comp" => [],			
					
				// 	   ];
				// }
			}

			//if($user == "hr" && $type == 4 ){
			if($type == 4 ){
				// if($local_it == 'local'){
				// 	$master =  'leave';
				// 	$end = ['sup' => 1,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				// 	$arr = [
				// 		"sup" => $supx,
				// 		"hr" => [],
				// 		"swap" => [],
				// 		"supx_compx" => $supx_comp,
				// 		"hrx_compx" => [],
				// 		"swapx_compx" => [],			
					
				// 	   ];
				// }
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
				/*if($local_it == 'expat'){
					$arr['hr'] = [];
					$arr['hrx_comp'] = [];
					$end['hr']=0;
				}	*/
				
			}
			
			// if($type == 4 && $local_it == 'local'){
			// 	$master =  'leave';
			// 	$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
			// 	$arr = [
			// 		"hr" => $hrx,
			// 		"sup" => $supx,
			// 		"swap" => [],
			// 		"hrx_comp" => $hrx_comp,
			// 		"supx_comp" => $supx_comp,
			// 		"swapx_compx" => [],			
					
			// 	   ];	
			// }
			// if($type == 4 && $local_it == 'expat'){
			// 	// $master =  'leave';
			// 	// $end = ['sup' => 1,'swap' => 0, 'hr' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
			// 	// $arr = [
			// 	// 	"sup" => $supx,
			// 	// 	"hr" => [],
			// 	// 	"swap" => [],
			// 	// 	"supx_comp" => $supx_comp,
			// 	// 	"hrx_comp" => [],
			// 	// 	"swapx_comp" => []	
			// 	//    ];
			// 	$master =  'leave';
			// 	$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
			// 	$arr = [
			// 		"hr" => $hrx,
			// 		"sup" => $supx,
			// 		"swap" => [],
			// 		"hrx_comp" => $hrx_comp,
			// 		"supx_comp" => $supx_comp,
			// 		"swapx_compx" => [],			
					
			// 	   ];	
			// }

			if($type ==  5){
				$master =  'leave';
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
				/*if($local_it == 'expat'){
					$arr['hr'] = [];
					$arr['hrx_comp'] = [];
					$end['hr']=0;
				}*/	
				// if($user == 'hr'){
				// 	if($local_it == 'local'){
				// 		$end = ['hr' => 1,'swap' => 0,'sup' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				// 		$arr = [
				// 		"hr" => $hrx,
				// 		"sup" => [],
				// 		"swap" => [],
				// 		"hrx_comp" => $hrx_comp,
				// 		"supx_comp" => [],
				// 		"swapx_comp" => [],			
					
				// 	   ];		
				// 	}	
				// }else{
				// 	$master =  'leave';
				// 	if($local_it == 'expat'){
				// 		$end = ['sup' => 1,'hr' => 0,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				// 		$arr = [
				// 		"sup" => $supx,
				// 		"hr" => [],
				// 		"swap" => [],
				// 		"supx_comp" => $supx_comp,
				// 		"hrx_comp" => [],
				// 		"swapx_comp" => [],			
					
				// 	   ];		
				// 	}	
				// }
			}

			if($type == 6){
				$master =  'leave';
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
				/*if($local_it == 'expat'){
					$arr['hr'] = [];
					$arr['hrx_comp'] = [];
					$end['hr']=0;
				}*/	
				/*if($local_it == 'local'){
					$end = ['sup' => 1,'hr' => 2 ,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => $hrx_comp,
						"swapx_comp" => [],			
					
					   ];		
				}else{
					$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],			
					
					   ];
				}*/
			}

			if($type == 7){
				$master =  'leave';
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
				/*if($local_it == 'expat'){
					$arr['hr'] = [];
					$arr['hrx_comp'] = [];
					$end['hr']=0;
				}	*/
				/*if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],		
							"supx_comp" => $supx_comp,
							"hrx_comp" => [],
							"swap" => [],			
							
					   ];		
					}
					if($local_it == 'expat'){
						$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],		
							"supx_comp" => $supx_comp,
							"hrx_comp" => [],
							"swap" => [],			
							
					   ];		
					}
				}
				else{
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"swap" => [],
							"supx_comp" => $supx,
							"hrx_comp" => $hrx,
							"swap" => [],			
							
					   ];		
					}else{
						$end = ['sup' => 1,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],
							"supx_comp" => $supx_comp,
							"hrx_comp" => [],
							"swapx_comp" => [],			
							
					   ];
					}
				}*/
			}

			if($type == 8){
				$master =  'leave';
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
				/*if($local_it == 'expat'){
					$arr['hr'] = [];
					$arr['hrx_comp'] = [];
					$end['hr']=0;
				}*/	
				/*if($local_it == 'local'){
					$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"swap" => [],			
						"supx_comp" => $supx_comp,
						"hrx_comp" => $hrx_comp,
						"swapx_comp" => [],					
				   ];		
				}else{
					$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],			
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],
				   ];
				}*/
			}

			if($type == 11){
				$master =  'leave';
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
				/*if($local_it == 'expat'){
					$arr['hr'] = [];
					$arr['hrx_comp'] = [];
					$end['hr']=0;
				}	*/
				/*if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],			
							"supx_comp" => $supx,
							"hrx_comp" => [],
							"swapx_comp" => [],		
					   ];		
					}
				}
				else{
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"swap" => [],
							"supx_comp" => $supx,
							"hrx_comp" => $hrx,
							"swapx_comp" => [],			
							
					   ];		
					}else{
						$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],
							"supx_comp" => $supx,
							"hrx_comp" => [],
							"swapx_comp" => [],			
							
					   ];
					}
				}*/
			}


			if($type == 12){
				$master =  'leave';
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
				/*if($local_it == 'expat'){
					$arr['hr'] = [];
					$arr['hrx_comp'] = [];
					$end['hr']=0;
				}	*/
				/*if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['hr' =>  1 ,'sup' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"hr" => $hrx,
							"sup" => [],
							"swap" => [],
							"hrx_comp" => $hrx_comp,
							"supx_comp" => [],
							"swapx_comp" => [],			
							
					   ];		
					}
				}*/
			}

			if($type == 9){

				$master =  'leave';
				if($swap_x != null){
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"swap" => [],
							"supx_comp" => $supx_comp,
							"hrx_comp" => $hrx_comp,
							"swapx_comp" => [],			
							
					   ];		
					}
				}
			}


		}
		
		
		$chatID = [];
		if(isset($arr) && $arr  !=  null){
			$chat  = \DB::SELECT("select id from command_center where  request_id=  $ids");
			if($chat != null){
				foreach ($chat as $key => $value) {
					$chatID[$value->id]  = 'u';
				}	
			}
			
			$arr['requestor_stat'] =  [$employee_id => 0];
			$arr['chat_id'	] = $chatID;
			$arr['req_flow'] = $end;
			$arr['master'] =  $master ;
			$arr['local_it'] = $local_it;
		}else{
			return  $arr  =[ 'data' => [], 'message' =>  'your not allowed to request '];
		}



		//print_r($arr);
		//
		//if $from_type  attendance $from_type  = 1 else 2  
		//
		// 
		//
				
		$from_type =  ($from_type == 'attendance' ? 1 : 2);
		$json = json_encode($arr);
		try{
			$insert = \DB::SELECT("insert into pool_request(employee_id,id_req,type_id,master_type,json_data,created_at) 
								   values('$employee_id',$ids,'$type','$from_type','$json',now())");
			return $arr;
		}catch(\Exeception $e){
			return false;
		}
	}


	/**
	 * count notification from cahta an rquest
	 * @return [type]
	 */
	public  function count_notif(){

		$KEY  = \Input::get('key');
		if($KEY == 'undefined'){
			return response()->json(['header' =>['message' => 'token undefined', 'status' => 201],'data' =>  []],201);
		}else{
		$DEC_KEY =  explode('-',(base64_decode($KEY)))[1];
		}
		$check = array(); 
		$notif  =  0;
		
		$subordinates 	= $this->get_subordinate($DEC_KEY);

		//$data  = \DB::SELECT("select * from pool_request where json_data like '%$token%' and DATE_SUB(now(),INTERVAL 7 DAY) ");
		$select_leave = \DB::SELECT("select pool_request.* from  pool_request,leave_request,emp
						where 
						emp.employee_id in($subordinates) and
						leave_request.employee_id = emp.employee_id and
						pool_request.id_req = leave_request.id and 
						master_type = 2 and
						leave_request.status_id in(1,2,3) and 
						leave_request.created_at >= now()-interval 4 month");
		$select_schedule = \DB::SELECT("select pool_request.* from  pool_request,att_schedule_request,emp
						where 
						emp.employee_id in($subordinates) and
						att_schedule_request.employee_id = emp.employee_id and
						pool_request.id_req = att_schedule_request.id and 
						master_type = 1 and
						att_schedule_request.status_id in(1,2,3) and
						att_schedule_request.date_request >= now()-interval 4 month");
		$select = array_merge($select_leave,$select_schedule);
		//return [$select];
		//return ["select pool_request.* from  pool_request,leave_request,emp where emp.employee_id = '$DEC_KEY' and leave_request.employee_id = emp.employee_id and pool_request.id_req = leave_request.id and master_type in(1,2)"];
		$count_select = count($select);
		
		for($i  = 0; $i < $count_select; $i++){

			$enc  = json_decode($select[$i]->json_data,1);

			/**
			 * for calculate notification
			 */

				foreach ($enc['req_flow'] as $key => $value) {
					if($key == 'employee_times' || $key == 'employee_dates'){
						
					}else{
						if($value ==  1){
							foreach ($enc[$key] as $key1 => $value1 ){
							
								if(isset($value1[$DEC_KEY])){
									
									if($value1['read_stat'] == 0 and $value1[$key.'_stat'] == 0  ){
										$notif += 1;
									}
								}
							}	
							$get_flow  = $key ;
						}

						if($value == 2 ){

							foreach ($enc[$key] as $key2 => $value2){
								if(isset($value2[$DEC_KEY])){

									if($value2['read_stat'] == 0 and $value2[$key.'_stat'] == 0 and  $enc['req_flow'][$get_flow.'_approve'] == 'x' ){
										$notif += 1;
									}
								}
							}
							$get_flow =  $key;


						}	
						
						

						if($value == 3){
							foreach ($enc[$key] as $key3 => $value3) {
								if(isset($value3[$DEC_KEY])){
									if($value3['read_stat'] == 0  and   $value2[$get_flow.'_stat'] ==  1  and $value3[$value3.'_stat'] != 1){
										$notif += 1;
									}
								}	
							}
						}
					}
				}

				//request flow		
				

				
				foreach ($enc['chat_id'	] as $keyx => $valuex){
					$get_user_chat  = \DB::SELECT("select employee_id from command_center where id  =  $keyx");
					foreach ($get_user_chat as $keyz => $valuez) {
						if(isset($valuez->employee_id) and ($valuez->employee_id != $DEC_KEY) and ($valuex ==  'u')){
							  $flow_check   =  $enc['req_flow'];

							   /**
							    * logic  json    req_flow : { 'sup' : 1, 'hr' :  0, 'swap' :  0, "hr_approve":"o", "swap_approve":"o","sup_approve":"o"}
							    * if(already approve) if( 1 : sup  then  sup_approve : chnage  to  'x'  and continue)
							    * if( flow 1 and  already approve 'x '  thenn  find flow 2 dan  get properties  like 'sup'  fo $data['sup']) 
							    * if  loop $data['sup']  find token notif  +1;
							    * @var [type]
							    */
							   foreach ($flow_check as $key => $value) {
							   		if($value  ==  1 ) {
							   			if($flow_check[$key.'_approve'] ==  'x'){
							   				foreach ($flow_check as $k=> $v) {
							   					if($v == 2){
							   						$check_user  = $enc[$k];
							   						foreach ($check_user as $x => $y) {
							   							if(isset($y->$DEC_KEY)){
							   								 $notif += 1;
							   							}
							   						}
							   					}
							   				}
							   			}else{
							   				$check_user  = $enc[$key];
							   				foreach ($check_user as $x => $y) {
					   							if(isset($y->$DEC_KEY)){
					   								 $notif += 1;
					   							}
					   						}
							   			}
							   		}else{
							   			if($value ==  2){
							   				if($flow_check[$key.'_approve'] !=  'x'){
								   				foreach ($flow_check as $k=> $v) {
								   					if($v == 1){
								   						$check_user  = $enc[$k];
								   						foreach ($check_user as $x => $y) {
								   							if(isset($y->$DEC_KEY)){
								   								 $notif += 1;
								   							}
								   						}
								   					}
								   				}
								   			}else{
								   				$check_user  = $enc[$key];
								   				foreach ($check_user as $x => $y) {
						   							if(isset($y->$DEC_KEY)){
						   								 $notif += 1;
						   							}
						   						}
								   			}	
							   			}
							   		}
							   }

							 
						}
					}
				}	
		}
		//print_r($notif);
	
		return \Response::json(['ping' => $notif,'request' => [] ]);
	}

	function update_schedule_list($datas,$type){
		// if($datas['dt_update']['type_id'] == 1){ // training
		// 	$q = \DB::SELECT("CALL update_att_req_training(".$datas['dt_update']['employee_id'].",".$datas['dt_update']['id'].",".$datas['dt_update']['id'].",".$datas['dt_update']['status'].",".$datas['dt_update']['approver'].",".$datas['dt_update']['approval'].");");
		// }
		// elseif($datas['dt_update']['type_id'] == 5){ // change shift
		// 	\DB::SELECT("update att_change_shift set approver =  ".
		// 		$datas['dt_update']['approver'].
		// 		", approval = ".$datas['dt_update']['approval'].
		// 		", status_id = ".$datas['dt_update']['status'].
		// 		" where id = ".$datas['dt_update']['id'].
		// 		" and employee_id = '".$datas['dt_update']['employee_id']."'");
		// }
		//return ["CALL updated_all_request_attendance(".$datas['dt_update']['employee_id'].",".$datas['dt_update']['id'].",".$datas['dt_update']['request'].",".$datas['dt_update']['type_id'].",".$datas['dt_update']['status'].",".$datas['dt_update']['approver'].",".$datas['dt_update']['approval'].",".$datas['dt_update']['comment'].");"];
		\DB::SELECT("CALL updated_all_request_attendance(".$datas['dt_update']['employee_id'].",".$datas['dt_update']['id'].",".$datas['dt_update']['request'].",".$datas['dt_update']['type_id'].",".$datas['dt_update']['status'].",".$datas['dt_update']['approver'].",".$datas['dt_update']['approval'].",".$datas['dt_update']['comment'].");");
		
		if($datas['dt_update']['type_id']==5){ // CHNAGE SHIFT
			if($datas['dt_update']['status'] == 2){			
				$gets = \DB::SELECT("select distinct new_shift, date from att_change_shift where id = ".$datas['dt_update']['request']);
				if(count($gets) > 0){
					$new_shift = $gets[0]->new_shift;
					$date_shift = $gets[0]->date;
					//return ["update att_schedule set shift_id=$new_shift where employee_id='".$datas['dt_update']['employee_id']."' and date='$date_shift'"];
					\DB::SELECT("update att_schedule set shift_id=$new_shift where employee_id='".$datas['dt_update']['employee_id']."' and date='$date_shift'");
				}
			}
			
		}
		if($datas['dt_update']['type_id']==6){ // SWAP SHIFT
			if($datas['dt_update']['status'] == 2){			
				$gets = \DB::SELECT("select distinct * from att_swap_shift where swap_id = ".$datas['dt_update']['request']);
				if(count($gets) > 0){
					$emp_shift = $gets[0]->new_shift_id;
					$swap_shift = $gets[0]->old_shift_id;
					$date_shift = $gets[0]->date;
					$swap_with = $gets[0]->swap_with;

					//return ["update att_schedule set shift_id=$swap_shift where employee_id='".$datas['dt_update']['employee_id']."' and date='$date_shift'","update att_schedule set shift_id=$emp_shift where employee_id='".$swap_with."' and date='$date_shift'"];
					\DB::SELECT("update att_schedule set shift_id=$swap_shift where employee_id='".$datas['dt_update']['employee_id']."' and date='$date_shift'"); // untuk yang request
					\DB::SELECT("update att_schedule set shift_id=$emp_shift where employee_id='".$swap_with."' and date='$date_shift'"); // untuk yang terima swap
				}
			}
			
		}

		if($datas['dt_update']['type_id']==9){ // time IN/OUT
			$query = null;
			\DB::SELECT("update att_time_in_out_req set approver =  ".$datas['dt_update']['approver'].", approved = ".$datas['dt_update']['approval'].", status_id = ".$datas['dt_update']['status']." where req_in_out_id = ".$datas['dt_update']['id']." and employee_id = '".$datas['dt_update']['employee_id']."'");
			if($type == 'approve'){
				
				$bio_empid 	= $datas['biometrics_device']['employee_id'];
				$bio_date 	= $datas['biometrics_device']['date'];

				$dt = \DB::SELECT("select * from biometrics_device where date='$bio_date' and employee_id='$bio_empid' ORDER BY id DESC");
				if(count($dt) > 0){
					$t_out = $datas['biometrics_device']['time_out'];
					$t_in = $datas['biometrics_device']['time_in'];
					if($dt[0]->time_out == $datas['biometrics_device']['time_out']){

						$query = "UPDATE `biometrics_device` SET `time_in`='$t_in' WHERE `date`='$bio_date' and `employee_id`='$bio_empid' and `time_out`='$t_out'";

					}elseif($dt[0]->time_in == $datas['biometrics_device']['time_in']){
						$query = "UPDATE `biometrics_device` SET `time_out`='$t_out' WHERE `date`='$bio_date' and `employee_id`='$bio_empid' and `time_in`='$t_in'";
					}else{
						$query = "UPDATE `biometrics_device` SET `time_out`='$t_out',`time_in`='$t_in' WHERE `date`='$bio_date' and `employee_id`='$bio_empid' ";
					}
				}else{

					if($datas['biometrics_device']['inquire_date']){
						if($datas['biometrics_device']['time_out'] && $datas['biometrics_device']['time_out']){
							//return "A";
							$query = "insert into `biometrics_device` (`device_number`,`date`,`inquire_date`,`time_in`,`time_out`,`employee_id`,`authen_id`,`input_`) values('".$datas['biometrics_device']['device_number']."','".$datas['biometrics_device']['date']."','".$datas['biometrics_device']['inquire_date']."','".substr($datas['biometrics_device']['time_in'],0,5)."', '".substr($datas['biometrics_device']['time_out'], 0,5)."','".$datas['biometrics_device']['employee_id']."','".$datas['biometrics_device']['authen_id']."','".$datas['biometrics_device']['input_']."')";
						
						}elseif($datas['biometrics_device']['time_out']){
							//return "B";
							$query = "insert into `biometrics_device` (`device_number`,`date`,`inquire_date`,`time_out`,`employee_id`,`authen_id`,`input_`) values('".$datas['biometrics_device']['device_number']."','".$datas['biometrics_device']['date']."','".$datas['biometrics_device']['inquire_date']."','".substr($datas['biometrics_device']['time_out'],0,5)."','".$datas['biometrics_device']['employee_id']."','".$datas['biometrics_device']['authen_id']."','".$datas['biometrics_device']['input_']."')";
						}elseif($datas['biometrics_device']['time_in']){
							//return "C";
							$query = "insert into `biometrics_device` (`device_number`,`date`,`inquire_date`,`time_in`,`employee_id`,`authen_id`,`input_`) values('".$datas['biometrics_device']['device_number']."','".$datas['biometrics_device']['date']."','".$datas['biometrics_device']['inquire_date']."','".substr($datas['biometrics_device']['time_in'],0,5)."','".$datas['biometrics_device']['employee_id']."','".$datas['biometrics_device']['authen_id']."','".$datas['biometrics_device']['input_']."')";
						}
					}else{
						if($datas['biometrics_device']['time_out'] && $datas['biometrics_device']['time_out']){
							//return "D";
							$query = "insert into `biometrics_device` (`device_number`,`date`,`time_in`,`time_out`,`employee_id`,`authen_id`,`input_`) values('".$datas['biometrics_device']['device_number']."','".$datas['biometrics_device']['date']."','".substr($datas['biometrics_device']['time_in'],0,5)."', '".substr($datas['biometrics_device']['time_out'], 0,5)."','".$datas['biometrics_device']['employee_id']."','".$datas['biometrics_device']['authen_id']."','".$datas['biometrics_device']['input_']."')";
						
						}elseif($datas['biometrics_device']['time_out']){
							//return "E";
							$query = "insert into `biometrics_device` (`device_number`,`date`,`time_out`,`employee_id`,`authen_id`,`input_`) values('".$datas['biometrics_device']['device_number']."','".$datas['biometrics_device']['date']."','".substr($datas['biometrics_device']['time_out'],0,5)."','".$datas['biometrics_device']['employee_id']."','".$datas['biometrics_device']['authen_id']."','".$datas['biometrics_device']['input_']."')";
						}elseif($datas['biometrics_device']['time_in']){
							//return "F";
							$query = "insert into `biometrics_device` (`device_number`,`date`,`time_in`,`employee_id`,`authen_id`,`input_`) values('".$datas['biometrics_device']['device_number']."','".$datas['biometrics_device']['date']."','".substr($datas['biometrics_device']['time_in'],0,5)."','".$datas['biometrics_device']['employee_id']."','".$datas['biometrics_device']['authen_id']."','".$datas['biometrics_device']['input_']."')";
						}
					}
				}
				\DB::SELECT($query);
			}
		}
		// elseif($datas['dt_update']['type_id'] == 7){ // late
		// 	$q = \DB::SELECT("CALL update_late_request(".$datas['dt_update']['employee_id'].",".$datas['dt_update']['id'].",".$datas['dt_update']['id'].",".$datas['dt_update']['status'].",".$datas['dt_update']['approver'].",".$datas['dt_update']['approval'].");");
		// }
	}

	// public function approve(){
	// 	$token =  explode('-',base64_decode((\Input::get('key'))))[1];  
	// 	$set_email = null;

	// 	$check_role = \DB::SELECT("select distinct job.title, emp.employee_id from emp, job_history, job where emp.employee_id = job_history.employee_id and job.id =  job_history.job and emp.employee_id = '$token' and ((lower(job.title) like '%hr%') or (lower(job.title) like '%human resource%') or (lower(job.title) like '%human%'))");
	// 	$who 		= null;
	// 	if(count($check_role) > 0){
	// 		//if(/*$check_role[0]->role_id == 37 || */$check_role[0]->role_id == 38){
	// 			$who = "hr";
	// 		//}
	// 	}

	// 	$i  = \Input::all()["0"];
	// 	$flow_approval=[];

	// 	if(isset($i)){
	// 		if($i['master_type'] == 1 ){
				
	// 			if(isset($i['sub'])){ // MULTI CRUD
	// 				for ($j=0; $j < count($i['sub']); $j++) { 
	// 					// $id  =  $i['sub'][$j];
	// 					// $get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
	// 					// $json  =  json_decode($get_pool[0]->json_data,1);

	// 					// $flow =  $json['req_flow'];
						
	// 					// /**
	// 					// *	get time and date 
	// 					// **/

	// 					// $time  =  date('H:i');
	// 					// $date  = date('Y-m-d');

	// 					// /**
	// 					// * get user stat 2 or just 1 stat
	// 					// **/

	// 					// $get_step  =  0;
	// 					// $user = '';
	// 					// $temp = [];

	// 					// /**	
	// 					// *	get flow user
	// 					// **/
	// 					// foreach($flow as $key => $value){
	// 					// 	$temp[] = $value;		
	// 					// }
	// 					$id  =  $i['id'];
	// 					$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
	// 					$json  =  json_decode($get_pool[0]->json_data,1);

	// 					$flow =  $json['req_flow'];
						
	// 					$hrxcomp = $supxcomp = $swapcomp = false;
	// 					if(isset($json['hrx_comp'])){
	// 						$hrxcomp = array_search($token,$json['hrx_comp']);
	// 					}
	// 					if(isset($json['supx_comp'])){
	// 						$supxcomp = array_search($token,$json['supx_comp']);
	// 					}
	// 					if(isset($json['swapx_compx'])){
	// 						$swapcomp = array_search($token,$json['swapx_compx']);
	// 					}
						
	// 						if(gettype($hrxcomp) == 'integer'){  $numb = "hr"; }
	// 						else if(gettype($supxcomp) == 'integer'){ $numb = "sup"; }
	// 						else if(gettype($swapcomp) == 'integer'){ $numb = "swap"; }
						


	// 					if(isset($flow['employee'])){
	// 						if($flow['employee'] == $token){
	// 							$numb = "employee";
	// 						}
	// 					}
	// 					//return [gettype($hrxcomp) == 'integer',gettype($hrxcomp)];
	// 					/**
	// 					*	get time and date 
	// 					**/

	// 					$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
	// 					$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
	// 					$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
	// 					$date  = $datess->format('Y-m-d');

	// 					/**
	// 					* get user stat 2 or just 1 stat
	// 					**/

	// 					$get_step  =  0;
	// 					$user = '';
	// 					$temp = [];

	// 					/**	
	// 					*	get flow user
	// 					**/
						
	// 					foreach($flow as $key => $value){
	// 						$temp[] = $value;
	// 						if($key == "hr" || $key == "sup" || $key == "employee"){					
	// 							if($value > 0){

	// 								if($key == 'employee'){
	// 									$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];	
	// 								}else{
	// 									$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];
	// 								}
	// 								if($key == 'employee_dates' || $key == 'employee_times'){
	// 									$key = 'employee';
	// 								}
	// 								if($flow[$key."_approve"] != "x"){
	// 									//$tmp['approve'] = true;
	// 									array_push($flow_approval,$tmp);
	// 								}
	// 							}
	// 						}
	// 					}

	// 					/**	
	// 					*check using in array target data exist
	// 					**/

	// 					if(in_array(2,$temp)){
	// 						$get_step = 2;
	// 					}else{
	// 						$get_step = 1;
	// 					}

	// 					/**
	// 					*	if 2 step approval
	// 					*	check approval if already approved
	// 					*   if approval already approve
	// 					*	then approve approver
	// 					**/
						
	// 					$get_already_approval  = \DB::SELECT("select * from att_schedule_request where id =  $id ");
						
	// 					if($get_already_approval[0]->id){				
	// 						$objs['dt_update'] = [
	// 								"id" 			=> $id,
	// 								"request"		=> $get_already_approval[0]->request_id,
	// 								"employee_id"	=> $i['employee_id'],
	// 								"type_id"		=> $get_already_approval[0]->type_id,
	// 								"approval"		=> 0,
	// 								"approver"		=> $token,
	// 								"status"		=> 2,
	// 								"comment"=>"0"
	// 							];
							
	// 						if(isset($i['comment'])){
	// 							$objs['dt_update']["comment"]=$i['comment'];
	// 						}

	// 						// APPROVAL BY
	// 						if($get_already_approval[0]->type_id == 1 || $get_already_approval[0]->type_id == 2){

	// 							$sup = array_search($token,$json['supx_comp']);
	// 							if($i['employee_id'] == $token){
	// 								$stat = true;
	// 							}elseif(gettype($sup) == 'integer'){
	// 								$stat = true;
	// 							}
	// 						}elseif($get_already_approval[0]->type_id == 3 || $get_already_approval[0]->type_id == 4 || $get_already_approval[0]->type_id == 5){
	// 							$sup = array_search($token,$json['supx_comp']);
	// 							if(gettype($sup) == 'integer'){
	// 								$stat = true;
	// 							}
	// 						}elseif($get_already_approval[0]->type_id == 6){

	// 							$sup = array_search($token,$json['supx_comp']);
	// 							$swap = array_search($token,$json['swapx_compx']);
	// 							if($i['employee_id'] == $token){
	// 								$stat = true;
	// 							}elseif(gettype($sup) == 'integer'){
	// 								$stat = true;
	// 							}elseif(gettype($swap) == 'integer'){
	// 								$stat = true;
	// 							}
	// 						}elseif($get_already_approval[0]->type_id == 7 || $get_already_approval[0]->type_id == 8 || $get_already_approval[0]->type_id == 9){
	// 							$hr = array_search($token,$json['hrx_comp']);
	// 							if(gettype($hr) == 'integer'){
	// 								$stat = true;
	// 							}
	// 						}
	// 					}

	// 					if(!$stat){
	// 						return \Response::json(['header'=>['message'=>"You don't have permission to approve this request",'status'=>500],'data'=>null],500);
	// 					}

	// 					if($get_already_approval[0]->type_id == 9){
	// 						$req_id = $get_already_approval[0]->request_id;
	// 						$att_timeInOutReq = \DB::SELECT("select * from att_time_in_out_req where req_in_out_id = $req_id")[0];
	// 						if($att_timeInOutReq->req_in_out_id){	

	// 							if($att_timeInOutReq->req_out){						
	// 								$out = explode(":",$att_timeInOutReq->req_out);
	// 								$in = explode(":",$att_timeInOutReq->req_in);
	// 								if((Integer)$out[0] == 0 && (Integer)$out[1] == 0){
									
	// 									$inquire_dates = null;
	// 								}elseif((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
									
	// 									$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
	// 								}elseif((Integer)$in[0] <= 23 && (Integer)$out[0] <= 23){
	// 									$inquire_dates = null;
	// 								}elseif((Integer)$in[0] <= 23){
	// 									$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
	// 								}
	// 							}else{
	// 								if((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
	// 									$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
	// 								}elseif((Integer)$in[0] <= 23 ){
	// 									$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
	// 								}else{
	// 									$inquire_dates = null;
	// 								}
	// 							}
	// 							$objs['biometrics_device'] = [
	// 										'device_number'=> '001',
	// 										'time_in' => $att_timeInOutReq->req_in,
	// 										'time_out' => $att_timeInOutReq->req_out,
	// 										'date' => $att_timeInOutReq->date,
	// 										'inquire_date'=>$inquire_dates,
	// 										'date'=> $att_timeInOutReq->date,
	// 										'employee_id'=>$i['employee_id'],
	// 										'authen_id'=> 0,
	// 										'input_'=>1
	// 									];
	// 						}else{
	// 							return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
	// 						}
	// 					}
	// 					//return [$get_step,$get_already_approval];
	// 					if($get_step == 2){
							
	// 						if($get_already_approval[0]->approval != null ){
	// 							$arr_cek =   [1,3,4];
	// 							if(in_array($get_already_approval[0]->approval,$arr_cek)){
	// 								return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
	// 							}
					
	// 							//\DB::SELECT("update att_schedule_request set approver =  2 where id = $id");
								
	// 							// foreach($json['req_flow'] as $key  => $value){
	// 							// 	if($value == 2){
	// 							// 		$key_x = $key;
	// 							// 	}
	// 							// }

	// 							// $user_x = $json[$key_x];

	// 							// foreach($user_x as $key => $value){
	// 							// 	if(isset($value[$token])){
	// 							// 		$json[$key_x][$key][$token] =  1;
	// 							// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 							// 		$json[$key_x][$key][$key_x.'_date'] = $date;
	// 							// 		$json[$key_x][$key][$key_x.'_time'] = $time;
	// 							// 		$json[$key_x][$key]['read_stat'] = 1;
	// 							// 		$json['req_flow'][$key_x.'_approve'] = 'x';
	// 							// 	}else{
	// 							// 		$json[$key_x][$key]['read_stat'] = 1;
	// 							// 	}
	// 							// }

	// 							// /**  
	// 							//  * chnage  all chat to already read notif  has gone or end 
	// 							//  */

	// 							// foreach($json['chat_id']  as  $keyy  => $valuee){
	// 							// 	$json['chat_id'	][$keyy] = 'r';
	// 							// }
	// 							if($numb == "hr"){ $idx = 1; }
	// 							else if($numb == "sup"){ $idx = 2; }
	// 							else if($numb == "swap"){ $idx = 3; }
	// 							else if($numb == "employee"){ $idx = $token; }
	// 							foreach ($flow_approval as $keys => $values) {
	// 								if($values['flow'] == $numb){
	// 									unset($flow_approval[$keys]);
	// 								}
	// 							}
	// 							$key_x = $numb;
	// 							if($key_x == 'employee'){
	// 								$json['req_flow'][$key_x.'_approve']='x';
	// 								$json['req_flow'][$key_x.'_dates']=$date;
	// 								$json['req_flow'][$key_x.'_times']=$time;
	// 							}else{
	// 								$user_x = $json[$key_x];

	// 								foreach($user_x as $key => $value){
	// 									if(isset($value[$token])){
	// 										$json[$key_x][$key][$token] =  1;
	// 										$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 										$json[$key_x][$key][$key_x.'_date'] = $date;
	// 										$json[$key_x][$key][$key_x.'_time'] = $time;
	// 										$json['req_flow'][$key_x.'_approve'] = 'x';
	// 										$json[$key_x][$key]['read_stat'] = 1;
	// 									}else{
	// 										$json[$key_x][$key]['read_stat'] = 1;
	// 									}
	// 								}
	// 							}

	// 							foreach($json['chat_id']  as  $keyy  => $valuee){
	// 								$json['chat_id'][$keyy] = 'r';
	// 							}


	// 							/**
	// 							* update json_data;
	// 							**/
	// 							return [$flow_approval,$json,11];
	// 							//return ["update att_schedule_request set status_id = 2,approver =  2 where id  =  $id "];
	// 							$json_x  =  json_encode($json,1);
	// 							if(count($flow_approval) ==0 ){
	// 							//$schedule = \DB::SELECT("update att_schedule_request set status_id = 2,approver =  2 where id  =  $id ");
	// 							 $this->update_schedule_list($objs,'approve');
	// 							}
	// 							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
								

	// 						}else{
								
	// 							if($get_already_approval[0]->approval == null){
									
	// 								/**
	// 								* eg : $json[sup] or $json[sup]
	// 								**/
									
	// 								// foreach($json['req_flow'] as $key  => $value){
	// 								// 	if($value == 1){
	// 								// 		$key_x = $key;
	// 								// 	}
	// 								// }

	// 								// $user_x = $json[$key_x];

	// 								// foreach($user_x as $key => $value){
	// 								// 	if(isset($value[$token])){
	// 								// 		$json[$key_x][$key][$token] =  1;
	// 								// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 								// 		$json[$key_x][$key][$key_x.'_date'] = $date;
	// 								// 		$json[$key_x][$key][$key_x.'_time'] = $time;
	// 								// 		$json[$key_x][$key]['read_stat'] = 1;
	// 								// 		$json['req_flow'][$key_x.'_approve'] = 'x';
	// 								// 	}else{
	// 								// 	   $json[$key_x][$key]['read_stat'] = 1;
	// 								// 	}
	// 								// }

	// 								// foreach($json['chat_id']  as  $keyy  => $valuee){
	// 								// 		$json['chat_id'][$keyy] = 'r';
	// 								// }
	// 								if($numb == "hr"){ $idx = 1; }
	// 								else if($numb == "sup"){ $idx = 2; }
	// 								else if($numb == "swap"){ $idx = 3; }
	// 								else if($numb == "employee"){ $idx = $token; }
	// 								foreach ($flow_approval as $keys => $values) {
	// 									if($values['flow'] == $numb){
	// 										unset($flow_approval[$keys]);
	// 									}
	// 								}
	// 								$key_x = $numb;
	// 								if($key_x == 'employee'){
	// 									$json['req_flow'][$key_x.'_approve']='x';
	// 									$json['req_flow'][$key_x.'_dates']=$date;
	// 									$json['req_flow'][$key_x.'_times']=$time;
	// 								}else{
	// 									$user_x = $json[$key_x];

	// 									foreach($user_x as $key => $value){
	// 										if(isset($value[$token])){
	// 											$json[$key_x][$key][$token] =  1;
	// 											$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 											$json[$key_x][$key][$key_x.'_date'] = $date;
	// 											$json[$key_x][$key][$key_x.'_time'] = $time;
	// 											$json['req_flow'][$key_x.'_approve'] = 'x';
	// 											$json[$key_x][$key]['read_stat'] = 1;
	// 										}else{
	// 											$json[$key_x][$key]['read_stat'] = 1;
	// 										}
	// 									}
	// 								}

	// 								foreach($json['chat_id']  as  $keyy  => $valuee){
	// 									$json['chat_id'][$keyy] = 'r';
	// 								}

	// 								/**
	// 								* update json_data;
	// 								**/
									
	// 								$json_x  =  json_encode($json,1);
	// 								if(count($flow_approval) ==0){
	// 									//\DB::SELECT("update att_schedule_request set status_id =2,  approver =  0, approval = 2 where id = $id");							
	// 									$this->update_schedule_list($objs,'approve');
	// 								}
	// 								$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
	// 							}
	// 						}
	// 					}else{
							
	// 						if($get_already_approval[0]->approval == null){
	// 							//\DB::SELECT("update att_schedule_request set approver =  2, approval = 0 where id = $id");
	// 							/**
	// 							* eg : $json[sup] or $json[sup]
	// 							**/
								
	// 							/*foreach($json['req_flow'] as $key  => $value){
	// 								if($value == 1){
	// 									$key_x = $key;
	// 								}
	// 							}

	// 							$user_x = $json[$key_x];

	// 							foreach($user_x as $key => $value){
	// 								if(isset($value[$token])){
	// 									$json[$key_x][$key][$token] =  1;
	// 									$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 									$json[$key_x][$key][$key_x.'_date'] = $date;
	// 									$json[$key_x][$key][$key_x.'_time'] = $time;
	// 									$json[$key_x][$key]['read_stat'] = 1;
	// 									$json['req_flow'][$key_x.'_approve'] = 'x';
	// 								}else{
	// 									$json[$key_x][$key]['read_stat'] = 1;
	// 								}
	// 							}

	// 							foreach($json['chat_id']  as  $keyy  => $valuee){
	// 								$json['chat_id'][$keyy] = 'r';
	// 							}*/
	// 							if($numb == "hr"){ $idx = 1; }
	// 							else if($numb == "sup"){ $idx = 2; }
	// 							else if($numb == "swap"){ $idx = 3; }
	// 							else if($numb == "employee"){ $idx = $token; }
	// 							foreach ($flow_approval as $keys => $values) {
	// 								if($values['flow'] == $numb){
	// 									unset($flow_approval[$keys]);
	// 								}
	// 							}
	// 							$key_x = $numb;
	// 							if($key_x == 'employee'){
	// 								$json['req_flow'][$key_x.'_approve']='x';
	// 								$json['req_flow'][$key_x.'_dates']=$date;
	// 								$json['req_flow'][$key_x.'_times']=$time;
	// 							}else{
	// 								$user_x = $json[$key_x];

	// 								foreach($user_x as $key => $value){
	// 									if(isset($value[$token])){
	// 										$json[$key_x][$key][$token] =  1;
	// 										$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 										$json[$key_x][$key][$key_x.'_date'] = $date;
	// 										$json[$key_x][$key][$key_x.'_time'] = $time;
	// 										$json['req_flow'][$key_x.'_approve'] = 'x';
	// 										$json[$key_x][$key]['read_stat'] = 1;
	// 									}else{
	// 										$json[$key_x][$key]['read_stat'] = 1;
	// 									}
	// 								}
	// 							}

	// 							foreach($json['chat_id']  as  $keyy  => $valuee){
	// 								$json['chat_id'][$keyy] = 'r';
	// 							}

	// 							/**
	// 							* update json_data;
	// 							**/
								
	// 							$json_x  =  json_encode($json,1);
	// 							//$schedule = \DB::SELECT("update att_schedule_request set status_id = 2,approver =  2, approval = 0 where id  =  $id ");
	// 							if(count($flow_approval) == 0){
	// 								$this->update_schedule_list($objs,'approve');
	// 							}
	// 							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
	// 						}
	// 					}						
	// 				}// end for
	// 			}//endif
	// 			else{ // SINGLE CRUD
	// 				// $id  =  $i['id'];
	// 				// $get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
	// 				// $json  =  json_decode($get_pool[0]->json_data,1);

	// 				// $flow =  $json['req_flow'];
					
	// 				// /**
	// 				// *	get time and date 
	// 				// **/

	// 				// $time  =  date('H:i');
	// 				// $date  = date('Y-m-d');

	// 				// /**
	// 				// * get user stat 2 or just 1 stat
	// 				// **/

	// 				// $get_step  =  0;
	// 				// $user = '';
	// 				// $temp = [];

	// 				// /**	
	// 				// *	get flow user
	// 				// **/
	// 				// foreach($flow as $key => $value){
	// 				// 	$temp[] = $value;		
	// 				// }

	// 				$id  =  $i['id'];
	// 				$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
	// 				$json  =  json_decode($get_pool[0]->json_data,1);

	// 				$flow =  $json['req_flow'];
					
	// 				$hrxcomp = $supxcomp = $swapcomp = false;
	// 				if(isset($json['hrx_comp'])){
	// 					$hrxcomp = array_search($token,$json['hrx_comp']);
	// 				}
	// 				if(isset($json['supx_comp'])){
	// 					$supxcomp = array_search($token,$json['supx_comp']);
	// 				}
	// 				if(isset($json['swapx_compx'])){
	// 					$swapcomp = array_search($token,$json['swapx_compx']);
	// 				}
					
	// 					if(gettype($hrxcomp) == 'integer'){  $numb = "hr"; }
	// 					else if(gettype($supxcomp) == 'integer'){ $numb = "sup"; }
	// 					else if(gettype($swapcomp) == 'integer'){ $numb = "swap"; }
					


	// 				if(isset($flow['employee'])){
	// 					if($flow['employee'] == $token){
	// 						$numb = "employee";
	// 					}
	// 				}
	// 				//return [gettype($hrxcomp) == 'integer',gettype($hrxcomp)];
	// 				/**
	// 				*	get time and date 
	// 				**/

	// 				$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
	// 				$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
	// 				$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
	// 				$date  = $datess->format('Y-m-d');

	// 				/**
	// 				* get user stat 2 or just 1 stat
	// 				**/

	// 				$get_step  =  0;
	// 				$user = '';
	// 				$temp = [];

	// 				/**	
	// 				*	get flow user
	// 				**/
					
	// 				foreach($flow as $key => $value){
	// 					$temp[] = $value;
	// 					if($key == "hr" || $key == "sup" || $key == "employee"){					
	// 						if($value > 0){

	// 							if($key == 'employee'){
	// 								$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];	
	// 							}else{
	// 								$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];
	// 							}
	// 							if($key == 'employee_dates' || $key == 'employee_times'){
	// 								$key = 'employee';
	// 							}
	// 							if($flow[$key."_approve"] != "x"){
	// 								//$tmp['approve'] = true;
	// 								array_push($flow_approval,$tmp);
	// 							}
	// 						}
	// 					}
	// 				}

	// 				/**	
	// 				*check using in array target data exist
	// 				**/

	// 				if(in_array(2,$temp)){
	// 					$get_step = 2;
	// 				}else{
	// 					$get_step = 1;
	// 				}

	// 				/**
	// 				*	if 2 step approval
	// 				*	check approval if already approved
	// 				*   if approval already approve
	// 				*	then approve approver
	// 				**/
					
	// 				$get_already_approval  = \DB::SELECT("select * from att_schedule_request where id =  $id ");
	// 				$stat = false;
	// 				if($get_already_approval[0]->id){				
	// 					$objs['dt_update'] = [
	// 							"id" 			=> $id,
	// 							"request"		=> $get_already_approval[0]->request_id,
	// 							"employee_id"	=> $i['employee_id'],
	// 							"type_id"		=> $get_already_approval[0]->type_id,
	// 							"approval"		=> 0,
	// 							"approver"		=> $token,
	// 							"status"		=> 2,
	// 							"comment"=>"0"
	// 						];
						
	// 					if(isset($i['comment'])){
	// 						$objs['dt_update']["comment"]=$i['comment'];
	// 					}

	// 					if($get_already_approval[0]->type_id == 1 || $get_already_approval[0]->type_id == 2){

	// 						$sup = array_search($token,$json['supx_comp']);
	// 						if($i['employee_id'] == $token){
	// 							$stat = true;
	// 						}elseif(gettype($sup) == 'integer'){
	// 							$stat = true;
	// 						}
	// 					}elseif($get_already_approval[0]->type_id == 3 || $get_already_approval[0]->type_id == 4 || $get_already_approval[0]->type_id == 5){
	// 						$sup = array_search($token,$json['supx_comp']);
	// 						if(gettype($sup) == 'integer'){
	// 							$stat = true;
	// 						}
	// 					}elseif($get_already_approval[0]->type_id == 6){

	// 						$sup = array_search($token,$json['supx_comp']);
	// 						$swap = array_search($token,$json['swapx_compx']);
	// 						if($i['employee_id'] == $token){
	// 							$stat = true;
	// 						}elseif(gettype($sup) == 'integer'){
	// 							$stat = true;
	// 						}elseif(gettype($swap) == 'integer'){
	// 							$stat = true;
	// 						}
	// 					}elseif($get_already_approval[0]->type_id == 7 || $get_already_approval[0]->type_id == 8 || $get_already_approval[0]->type_id == 9){
	// 						$hr = array_search($token,$json['hrx_comp']);
	// 						if(gettype($hr) == 'integer'){
	// 							$stat = true;
	// 						}
	// 					}

	// 				}

	// 				if(!$stat){
	// 					return \Response::json(['header'=>['message'=>"You don't have permission to approve this request",'status'=>500],'data'=>null],500);
	// 				}

	// 				if($get_already_approval[0]->type_id == 9){
	// 					$req_id = $get_already_approval[0]->request_id;
	// 					$att_timeInOutReq = \DB::SELECT("select * from att_time_in_out_req where req_in_out_id = $req_id")[0];
	// 					if($att_timeInOutReq->req_in_out_id){	

	// 						if($att_timeInOutReq->req_out){						
	// 							$out = explode(":",$att_timeInOutReq->req_out);
	// 							$in = explode(":",$att_timeInOutReq->req_in);
	// 							if((Integer)$out[0] == 0 && (Integer)$out[1] == 0){
								
	// 								$inquire_dates = null;
	// 							}elseif((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
								
	// 								$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
	// 							}elseif((Integer)$in[0] <= 23 && (Integer)$out[0] <= 23){
	// 								$inquire_dates = null;
	// 							}elseif((Integer)$in[0] <= 23){
	// 								$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
	// 							}
	// 						}else{
	// 							if((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
	// 								$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
	// 							}elseif((Integer)$in[0] <= 23 ){
	// 								$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
	// 							}else{
	// 								$inquire_dates = null;
	// 							}
	// 						}
	// 						$objs['biometrics_device'] = [
	// 									'device_number'=> '001',
	// 									'time_in' => $att_timeInOutReq->req_in,
	// 									'time_out' => $att_timeInOutReq->req_out,
	// 									'date' => $att_timeInOutReq->date,
	// 									'inquire_date'=>$inquire_dates,
	// 									'date'=> $att_timeInOutReq->date,
	// 									'employee_id'=>$i['employee_id'],
	// 									'authen_id'=> 0,
	// 									'input_'=>1
	// 								];
	// 					}else{
	// 						return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
	// 					}
	// 				}
					
	// 				if($get_step == 2){
						
	// 					if($get_already_approval[0]->approval != null ){
	// 						$arr_cek =   [1,3,4];
	// 						if(in_array($get_already_approval[0]->approval,$arr_cek)){
	// 							return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
	// 						}
				
	// 						//\DB::SELECT("update att_schedule_request set approver =  2 where id = $id");
							
	// 						// foreach($json['req_flow'] as $key  => $value){
	// 						// 	if($value == 2){
	// 						// 		$key_x = $key;
	// 						// 	}
	// 						// }

	// 						// $user_x = $json[$key_x];

	// 						// foreach($user_x as $key => $value){
	// 						// 	if(isset($value[$token])){
	// 						// 		$json[$key_x][$key][$token] =  1;
	// 						// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 						// 		$json[$key_x][$key][$key_x.'_date'] = $date;
	// 						// 		$json[$key_x][$key][$key_x.'_time'] = $time;
	// 						// 		$json[$key_x][$key]['read_stat'] = 1;
	// 						// 		$json['req_flow'][$key_x.'_approve'] = 'x';
	// 						// 	}else{
	// 						// 		$json[$key_x][$key]['read_stat'] = 1;
	// 						// 	}
	// 						// }

	// 						// /**  
	// 						//  * chnage  all chat to already read notif  has gone or end 
	// 						//  */

	// 						// foreach($json['chat_id']  as  $keyy  => $valuee){
	// 						// 	$json['chat_id'	][$keyy] = 'r';
	// 						// }
	// 						if($numb == "hr"){ $idx = 1; }
	// 						else if($numb == "sup"){ $idx = 2; }
	// 						else if($numb == "swap"){ $idx = 3; }
	// 						else if($numb == "employee"){ $idx = $token; }
	// 						foreach ($flow_approval as $keys => $values) {
	// 							if($values['flow'] == $numb){
	// 								unset($flow_approval[$keys]);
	// 							}
	// 						}
	// 						$key_x = $numb;
	// 						if($key_x == 'employee'){
	// 							$json['req_flow'][$key_x.'_approve']='x';
	// 							$json['req_flow'][$key_x.'_dates']=$date;
	// 							$json['req_flow'][$key_x.'_times']=$time;
	// 						}else{
	// 							$user_x = $json[$key_x];

	// 							foreach($user_x as $key => $value){
	// 								if(isset($value[$token])){
	// 									$json[$key_x][$key][$token] =  1;
	// 									$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 									$json[$key_x][$key][$key_x.'_date'] = $date;
	// 									$json[$key_x][$key][$key_x.'_time'] = $time;
	// 									$json['req_flow'][$key_x.'_approve'] = 'x';
	// 									$json[$key_x][$key]['read_stat'] = 1;
	// 								}else{
	// 									$json[$key_x][$key]['read_stat'] = 1;
	// 								}
	// 							}
	// 						}

	// 						foreach($json['chat_id']  as  $keyy  => $valuee){
	// 							$json['chat_id'][$keyy] = 'r';
	// 						}


	// 						/**
	// 						* update json_data;
	// 						**/
							
	// 						//return ["update att_schedule_request set status_id = 2,approver =  2 where id  =  $id "];
	// 						$json_x  =  json_encode($json,1);
	// 						//$schedule = \DB::SELECT("update att_schedule_request set status_id = 2,approver =  2 where id  =  $id ");
	// 						if(count($flow_approval) == 0){
	// 							$this->update_schedule_list($objs,'approve');
	// 						}
	// 						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
							

	// 					}else{
							
	// 						if($get_already_approval[0]->approval == null){
								
	// 							/**
	// 							* eg : $json[sup] or $json[sup]
	// 							**/
								
	// 							// foreach($json['req_flow'] as $key  => $value){
	// 							// 	if($value == 1){
	// 							// 		$key_x = $key;
	// 							// 	}
	// 							// }

	// 							// $user_x = $json[$key_x];

	// 							// foreach($user_x as $key => $value){
	// 							// 	if(isset($value[$token])){
	// 							// 		$json[$key_x][$key][$token] =  1;
	// 							// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 							// 		$json[$key_x][$key][$key_x.'_date'] = $date;
	// 							// 		$json[$key_x][$key][$key_x.'_time'] = $time;
	// 							// 		$json[$key_x][$key]['read_stat'] = 1;
	// 							// 		$json['req_flow'][$key_x.'_approve'] = 'x';
	// 							// 	}else{
	// 							// 	   $json[$key_x][$key]['read_stat'] = 1;
	// 							// 	}
	// 							// }

	// 							// foreach($json['chat_id']  as  $keyy  => $valuee){
	// 							// 		$json['chat_id'][$keyy] = 'r';
	// 							// }
	// 							if($numb == "hr"){ $idx = 1; }
	// 							else if($numb == "sup"){ $idx = 2; }
	// 							else if($numb == "swap"){ $idx = 3; }
	// 							else if($numb == "employee"){ $idx = $token; }
	// 							foreach ($flow_approval as $keys => $values) {
	// 								if($values['flow'] == $numb){
	// 									unset($flow_approval[$keys]);
	// 								}
	// 							}
	// 							$key_x = $numb;
	// 							if($key_x == 'employee'){
	// 								$json['req_flow'][$key_x.'_approve']='x';
	// 								$json['req_flow'][$key_x.'_dates']=$date;
	// 								$json['req_flow'][$key_x.'_times']=$time;
	// 							}else{
	// 								$user_x = $json[$key_x];

	// 								foreach($user_x as $key => $value){
	// 									if(isset($value[$token])){
	// 										$json[$key_x][$key][$token] =  1;
	// 										$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 										$json[$key_x][$key][$key_x.'_date'] = $date;
	// 										$json[$key_x][$key][$key_x.'_time'] = $time;
	// 										$json['req_flow'][$key_x.'_approve'] = 'x';
	// 										$json[$key_x][$key]['read_stat'] = 1;
	// 									}else{
	// 										$json[$key_x][$key]['read_stat'] = 1;
	// 									}
	// 								}
	// 							}

	// 							foreach($json['chat_id']  as  $keyy  => $valuee){
	// 								$json['chat_id'][$keyy] = 'r';
	// 							}

	// 							/**
	// 							* update json_data;
	// 							**/
								
	// 							$json_x  =  json_encode($json,1);
	// 							//\DB::SELECT("update att_schedule_request set status_id =2,  approver =  0, approval = 2 where id = $id");	
	// 							if(count($flow_approval) == 0){
	// 								$this->update_schedule_list($objs,'approve');
	// 							}						
	// 							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
	// 						}
	// 					}
	// 				}else{
						
	// 					if($get_already_approval[0]->approval == null){
	// 						//\DB::SELECT("update att_schedule_request set approver =  2, approval = 0 where id = $id");
	// 						/**
	// 						* eg : $json[sup] or $json[sup]
	// 						**/
							
	// 						// foreach($json['req_flow'] as $key  => $value){
	// 						// 	if($value == 1){
	// 						// 		$key_x = $key;
	// 						// 	}
	// 						// }

	// 						// $user_x = $json[$key_x];

	// 						// foreach($user_x as $key => $value){
	// 						// 	if(isset($value[$token])){
	// 						// 		$json[$key_x][$key][$token] =  1;
	// 						// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 						// 		$json[$key_x][$key][$key_x.'_date'] = $date;
	// 						// 		$json[$key_x][$key][$key_x.'_time'] = $time;
	// 						// 		$json[$key_x][$key]['read_stat'] = 1;
	// 						// 		$json['req_flow'][$key_x.'_approve'] = 'x';
	// 						// 	}else{
	// 						// 		$json[$key_x][$key]['read_stat'] = 1;
	// 						// 	}
	// 						// }

	// 						// foreach($json['chat_id']  as  $keyy  => $valuee){
	// 						// 	$json['chat_id'][$keyy] = 'r';
	// 						// }
	// 						if($numb == "hr"){ $idx = 1; }
	// 						else if($numb == "sup"){ $idx = 2; }
	// 						else if($numb == "swap"){ $idx = 3; }
	// 						else if($numb == "employee"){ $idx = $token; }
	// 						foreach ($flow_approval as $keys => $values) {
	// 							if($values['flow'] == $numb){
	// 								unset($flow_approval[$keys]);
	// 							}
	// 						}
	// 						$key_x = $numb;
	// 						if($key_x == 'employee'){
	// 							$json['req_flow'][$key_x.'_approve']='x';
	// 							$json['req_flow'][$key_x.'_dates']=$date;
	// 							$json['req_flow'][$key_x.'_times']=$time;
	// 						}else{
	// 							$user_x = $json[$key_x];

	// 							foreach($user_x as $key => $value){
	// 								if(isset($value[$token])){
	// 									$json[$key_x][$key][$token] =  1;
	// 									$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 									$json[$key_x][$key][$key_x.'_date'] = $date;
	// 									$json[$key_x][$key][$key_x.'_time'] = $time;
	// 									$json['req_flow'][$key_x.'_approve'] = 'x';
	// 									$json[$key_x][$key]['read_stat'] = 1;
	// 								}else{
	// 									$json[$key_x][$key]['read_stat'] = 1;
	// 								}
	// 							}
	// 						}

	// 						foreach($json['chat_id']  as  $keyy  => $valuee){
	// 							$json['chat_id'][$keyy] = 'r';
	// 						}

	// 						/**
	// 						* update json_data;
	// 						**/
							
	// 						$json_x  =  json_encode($json,1);
	// 						//$schedule = \DB::SELECT("update att_schedule_request set status_id = 2,approver =  2, approval = 0 where id  =  $id ");
	// 						if(count($flow_approval) == 0){
	// 							$this->update_schedule_list($objs,'approve');
	// 						}
	// 						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
	// 					}
	// 				}						
	// 			}

	// 		}else{ // leave

	// 			$id  =  $i['id'];
	// 			$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 2");
	// 			$json  =  json_decode($get_pool[0]->json_data,1);

	// 			$flow =  $json['req_flow'];
				
	// 			$hrxcomp = $supxcomp = $swapcomp = false;
	// 			if(isset($json['hrx_comp'])){
	// 				$hrxcomp = array_search($token,$json['hrx_comp']);
	// 			}
	// 			if(isset($json['supx_comp'])){
	// 				$supxcomp = array_search($token,$json['supx_comp']);
	// 			}
	// 			if(isset($json['swapx_compx'])){
	// 				$swapcomp = array_search($token,$json['swapx_compx']);
	// 			}

	// 			if($i['leave_type'] != 'Vacation Leave' && $json['local_it'] == 'expat'){
	// 				$hrxcomp = false;
	// 			}
				
	// 				if(gettype($hrxcomp) == 'integer'){  $numb = "hr"; }
	// 				else if(gettype($supxcomp) == 'integer'){ $numb = "sup"; }
	// 				else if(gettype($swapcomp) == 'integer'){ $numb = "swap"; }
				


	// 			if(isset($flow['employee'])){
	// 				if($flow['employee'] == $token){
	// 					$numb = "employee";
	// 				}
	// 			}
	// 			//return [gettype($hrxcomp) == 'integer',gettype($hrxcomp)];
	// 			/**
	// 			*	get time and date 
	// 			**/
	// 			$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
	// 			$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
	// 			$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
	// 			$date  = $datess->format('Y-m-d');


	// 			/**
	// 			* get user stat 2 or just 1 stat
	// 			**/

	// 			$get_step  =  0;
	// 			$user = '';
	// 			$temp = [];

	// 			/**	
	// 			*	get flow user
	// 			**/
				
	// 			foreach($flow as $key => $value){
	// 				$temp[] = $value;
	// 				if($key == "hr" || $key == "sup" || $key == "employee"){					
	// 					if($value > 0){

	// 						if($key == 'employee'){
	// 							$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];	
	// 						}else{
	// 							$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];
	// 						}
	// 						if($key == 'employee_dates' || $key == 'employee_times'){
	// 							$key = 'employee';
	// 						}
	// 						if($flow[$key."_approve"] != "x"){
	// 							//$tmp['approve'] = true;
	// 							if($i['leave_type'] == 'Vacation Leave' && $json['local_it'] == 'expat' && $key == 'hr'){

	// 							}else{
	// 								array_push($flow_approval,$tmp);
	// 							}
	// 						}
	// 					}
	// 				}
	// 			}
	// 			/**	
	// 			*check using in array target data exist
	// 			**/

	// 			if(in_array(2,$temp)){
	// 				$get_step = 2;
	// 			}else{
	// 				$get_step = 1;
	// 			}

	// 			/**
	// 			*	if 2 step approval
	// 			*	check approval if already approved
	// 			*   if approval already approve
	// 			*	then approve approver
	// 			**/

	// 			$get_already_approval  = \DB::SELECT("select * from leave_request where id =  $id ");

	// 			//return [$get_step, $get_already_approval];
	// 			if($get_step == 2){
					
	// 				if($get_already_approval[0]->approval != null ){
	// 					$arr_cek =   [1,3,4];
					
	// 					if(in_array($get_already_approval[0]->approval,$arr_cek)){
	// 						return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
	// 					}
			
	// 					//\DB::SELECT("update leave_request set approver =  2 where id = $id");
						
	// 					// foreach($json['req_flow'] as $key  => $value){
								
	// 					// 	$numb = 0;
	// 					// 	if($value == 1){ $numb = 1; $hd = 'hr'; }
	// 					// 	else if($value == 2){ $numb = 2; $hd = 'sup'; }
	// 					// 	else if($value == 3){ $numb = 3; $hd = 'swap'; }								
	// 					// 	if($json['req_flow'][$hd."_approve"] == 'o'){
	// 					// 		$key_x = $hd;
	// 					// 		foreach ($flow_approval as $keys => $values) {
	// 					// 			if($values['number'] == $numb){
	// 					// 				unset($flow_approval[$keys]);
	// 					// 			}
	// 					// 		}
	// 					// 	}
	// 					// }
	// 					/*if($numb == "hr"){ $idx = 1; }
	// 					else if($numb == "sup"){ $idx = 2; }
	// 					else if($numb == "swap"){ $idx = 3; }
	// 					foreach ($flow_approval as $keys => $values) {
	// 						if($values['number'] == $idx){
	// 							unset($flow_approval[$keys]);
	// 						}
	// 					}
	// 					$key_x = $numb;

	// 					$user_x = $json[$key_x];

	// 					foreach($user_x as $key => $value){
	// 						if(isset($value[$token])){
	// 							$json[$key_x][$key][$token] =  1;
	// 							$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 							$json[$key_x][$key][$key_x.'_date'] = $date;
	// 							$json[$key_x][$key][$key_x.'_time'] = $time;
	// 							$json[$key_x][$key]['read_stat'] = 1;
	// 							$json['req_flow'][$key_x.'_approve'] = 'x';								
	// 						}else{
	// 							$json[$key_x][$key]['read_stat'] = 1;
	// 						}
	// 					}*/
	// 					if($numb == "hr"){ $idx = 1; }
	// 					else if($numb == "sup"){ $idx = 2; }
	// 					else if($numb == "swap"){ $idx = 3; }
	// 					else if($numb == "employee"){ $idx = $token; }
	// 					foreach ($flow_approval as $keys => $values) {
	// 						if($values['number'] == $idx){
	// 							unset($flow_approval[$keys]);
	// 						}
	// 					}
	// 					$key_x = $numb;
	// 					if($key_x == 'employee'){
	// 						$json['req_flow'][$key_x.'_approve']='x';
	// 						$json['req_flow'][$key_x.'_dates']=$date;
	// 						$json['req_flow'][$key_x.'_times']=$time;
	// 					}else{
	// 						$user_x = $json[$key_x];

	// 						foreach($user_x as $key => $value){
	// 							if(isset($value[$token])){
	// 								$json[$key_x][$key][$token] =  1;
	// 								$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 								$json[$key_x][$key][$key_x.'_date'] = $date;
	// 								$json[$key_x][$key][$key_x.'_time'] = $time;
	// 								$json['req_flow'][$key_x.'_approve'] = 'x';
	// 								$json[$key_x][$key]['read_stat'] = 1;
	// 							}else{
	// 								$json[$key_x][$key]['read_stat'] = 1;
	// 							}
	// 						}
	// 					}

	// 					foreach($json['chat_id']  as  $keyy  => $valuee){
	// 						$json['chat_id'][$keyy] = 'r';
	// 					}
	// 					//return [$flow_approval,$json,1];
	// 					$check_ado = \DB::SELECT("select distinct leave_request_id from leave_ado where leave_request_id = $id");
	// 						$ado_include = false;
	// 						if(isset($check_ado[0])){
	// 							$ado_include = true;
	// 						}
	// 						if($ado_include || $i['leave_type']=='Accumulation Day Off'){
	// 							$get_ado = \DB::SELECT("SELECT `date_ado`, `schedule_id`, `name_ado`, `change_date` ,`id` , `employee_id` from leave_ado where leave_request_id=$id");

	// 							foreach ($get_ado as $key => $value) {
	// 								$emps_ado 	= $value->employee_id;
	// 								$dt_ado 	= $value->date_ado;
	// 								$shift_ado 	= $value->schedule_id;
	// 								$ch_ado 	= $value->change_date;
	// 								if(count($flow_approval) == 0){									
	// 									// $up1 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$dt_ado',$shift_ado)"); 14082018
	// 									// $up2 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$ch_ado',42)"); 14082018
	// 								}
	// 							}
	// 						}

	// 					/**
	// 					* update json_data;
	// 					**/
	// 					$json_x  =  json_encode($json,1);

	// 					if(count($flow_approval) == 0){
	// 						//return ["json"=>$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval not null :: single :: leave :: flow sudah lengkap"];
	// 						$schedule = \DB::SELECT("update leave_request set status_id = 2,approver = '$token' where id  =  $id ");
	// 					}else{
	// 						//return ["json"=>$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval not null :: single :: leave :: flow belum lengkap"];
	// 					}
	// 					//return [$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval not null :: single :: leave"];
	// 					//$schedule = \DB::SELECT("update leave_request set status_id = 2,approver =  2 where id  =  $id ");
	// 					$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						

	// 				}else{
						
	// 					if($get_already_approval[0]->approval == null){
						
	// 						//\DB::SELECT("update leave_request set approver =  0, approval = 2 where id = $id");

							
							
							
	// 						/**
	// 						* eg : $json[sup] or $json[sup]
	// 						**/
							
	// 						// foreach($json['req_flow'] as $key  => $value){
								
	// 						// 	$numb = 0;
	// 						// 	if($value == 1){ $numb = 1; $hd = 'hr'; }
	// 						// 	else if($value == 2){ $numb = 2; $hd = 'sup'; }
	// 						// 	else if($value == 3){ $numb = 3; $hd = 'swap'; }								
	// 						// 	if($json['req_flow'][$hd."_approve"] == 'o'){
	// 						// 		$key_x = $hd;
	// 						// 		foreach ($flow_approval as $keys => $values) {
	// 						// 			if($values['number'] == $numb){
	// 						// 				unset($flow_approval[$keys]);
	// 						// 			}
	// 						// 		}
	// 						// 	}
	// 						// }
	// 						// if($numb == "hr"){ $idx = 1; }
	// 						// else if($numb == "sup"){ $idx = 2; }
	// 						// else if($numb == "swap"){ $idx = 3; }
	// 						// else if($numb == "employee"){ $idx = $token; }
	// 						// foreach ($flow_approval as $keys => $values) {
	// 						// 	if($values['number'] == $idx){
	// 						// 		unset($flow_approval[$keys]);
	// 						// 	}
	// 						// }
	// 						// $key_x = $numb;

	// 						// $user_x = $json[$key_x];
	// 						// foreach($user_x as $key => $value){
	// 						// 	if(isset($value[$token])){
	// 						// 		$json[$key_x][$key][$token] =  1;
	// 						// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 						// 		$json[$key_x][$key][$key_x.'_date'] = $date;
	// 						// 		$json[$key_x][$key][$key_x.'_time'] = $time;
	// 						// 		$json[$key_x][$key]['read_stat'] = 1;
	// 						// 		$json['req_flow'][$key_x.'_approve'] = 'x';
	// 						// 	}else{
	// 						// 		$json[$key_x][$key]['read_stat'] = 1;
	// 						// 	}
	// 						// }

	// 						if($numb == "hr"){ $idx = 1; }
	// 						else if($numb == "sup"){ $idx = 2; }
	// 						else if($numb == "swap"){ $idx = 3; }
	// 						else if($numb == "employee"){ $idx = $token; }
	// 						foreach ($flow_approval as $keys => $values) {
	// 							if($values['number'] == $idx){
	// 								unset($flow_approval[$keys]);
	// 							}
	// 						}
	// 						$key_x = $numb;
	// 						if($key_x == 'employee'){
	// 							$json['req_flow'][$key_x.'_approve']='x';
	// 							$json['req_flow'][$key_x.'_dates']=$date;
	// 							$json['req_flow'][$key_x.'_times']=$time;
	// 						}else{
	// 							$user_x = $json[$key_x];

	// 							foreach($user_x as $key => $value){
	// 								if(isset($value[$token])){
	// 									$json[$key_x][$key][$token] =  1;
	// 									$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 									$json[$key_x][$key][$key_x.'_date'] = $date;
	// 									$json[$key_x][$key][$key_x.'_time'] = $time;
	// 									$json['req_flow'][$key_x.'_approve'] = 'x';
	// 									$json[$key_x][$key]['read_stat'] = 1;
	// 								}else{
	// 									$json[$key_x][$key]['read_stat'] = 1;
	// 								}
	// 							}
	// 						}
	// 						//return [$flow_approval,$json,2];
	// 						//return [$flow_approval, $numb,$json[$key_x],$token];
	// 						foreach($json['chat_id']  as  $keyy  => $valuee){
	// 							$json['chat_id'][$keyy] = 'r';
	// 						}

	// 						$check_ado = \DB::SELECT("select distinct leave_request_id from leave_ado where leave_request_id = $id");
							
	// 						$ado_include = false;
							
	// 						if(isset($check_ado[0])){
	// 							$ado_include = true;
	// 						}
	// 						if($ado_include || $i['leave_type']=='Accumulation Day Off'){
	// 							$get_ado = \DB::SELECT("SELECT `date_ado`, `schedule_id`, `name_ado`, `change_date` ,`id` , `employee_id` from leave_ado where leave_request_id=$id");

	// 							foreach ($get_ado as $key => $value) {
	// 								$emps_ado 	= $value->employee_id;
	// 								$dt_ado 	= $value->date_ado;
	// 								$shift_ado 	= $value->schedule_id;
	// 								$ch_ado 	= $value->change_date;
	// 								if(count($flow_approval) == 0){									
	// 									// $up1 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$dt_ado',$shift_ado)"); 14082018
	// 									// $up2 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$ch_ado',42)"); 14082018
	// 								}
	// 							}
	// 						}	
							
							
	// 						/**
	// 						* update json_data;
	// 						**/

	// 						$json_x  =  json_encode($json,1);
	// 						if(count($flow_approval) == 0){
	// 							//return [$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval null :: single :: leave","FLOW LENGKAP"];	
	// 							\DB::SELECT("update leave_request set approver =  '$token', approval = 2, status_id=2 where id = $id"); // 14082018
	// 						}else{
	// 							//return [$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval null :: single :: leave","FLOW BELUM LENGKAP"];	
	// 						}
	// 						//return [$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval null :: single :: leave"];
	// 						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
	// 					}
	// 				}
	// 			}else{ // get_data = 1
					
	// 				if($get_already_approval[0]->approval == null){
	// 					//\DB::SELECT("update leave_request set approver =  2, approval = 0 where id = $id");
	// 					/**
	// 					* eg : $json[sup] or $json[sup]
	// 					**/
						
	// 					// foreach($json['req_flow'] as $key  => $value){
								
	// 					// 	// $numb = 0;
	// 					// 	// if($value == 1){ $numb = 1; $hd = 'hr'; }
	// 					// 	// else if($value == 2){ $numb = 2; $hd = 'sup'; }
	// 					// 	// else if($value == 3){ $numb = 3; $hd = 'swap'; }								
	// 					// 	// if($json['req_flow'][$hd."_approve"] == 'o'){
	// 					// 	// 	$key_x = $hd;
	// 					// 	// 	foreach ($flow_approval as $keys => $values) {
	// 					// 	// 		if($values['number'] == $numb){
	// 					// 	// 			unset($flow_approval[$keys]);
	// 					// 	// 		}
	// 					// 	// 	}
	// 					// 	// }
	// 					// 	if($numb == "hr"){ $idx = 1; }
	// 					// 	else if($numb == "sup"){ $idx = 2; }
	// 					// 	else if($numb == "swap"){ $idx = 3; }
	// 					// 	if($json['req_flow'][$hd."_approve"] == 'o'){
	// 					// 		$key_x = $numb;
	// 					// 		foreach ($flow_approval as $keys => $values) {
	// 					// 			if($values['number'] == $idx){
	// 					// 				unset($flow_approval[$keys]);
	// 					// 			}
	// 					// 		}
	// 					// 	}
	// 					// }
	// 					/*if($numb == "hr"){ $idx = 1; }
	// 					else if($numb == "sup"){ $idx = 2; }
	// 					else if($numb == "swap"){ $idx = 3; }
	// 					else if($numb == "employee"){ $idx = $token; }
	// 					foreach ($flow_approval as $keys => $values) {
	// 						if($values['number'] == $idx){
	// 							unset($flow_approval[$keys]);
	// 						}
	// 					}
	// 					$key_x = $numb;
	// 					if($key_x == 'employee'){
	// 						$json['req_flow'][$key_x.'_approve']='x';
	// 						$json['req_flow'][$key_x.'_dates']=$date;
	// 						$json['req_flow'][$key_x.'_times']=$time;
	// 					}else{
	// 						$user_x = $json[$key_x];

	// 						foreach($user_x as $key => $value){
	// 							if(isset($value[$token])){
	// 								$json[$key_x][$key][$token] =  1;
	// 								$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 								$json[$key_x][$key][$key_x.'_date'] = $date;
	// 								$json[$key_x][$key][$key_x.'_time'] = $time;
	// 								$json['req_flow'][$key_x.'_approve'] = 'x';
	// 								$json[$key_x][$key]['read_stat'] = 1;
	// 							}else{
	// 								$json[$key_x][$key]['read_stat'] = 1;
	// 							}
	// 						}
	// 					}*/
	// 					if($numb == "hr"){ $idx = 1; }
	// 					else if($numb == "sup"){ $idx = 2; }
	// 					else if($numb == "swap"){ $idx = 3; }
	// 					else if($numb == "employee"){ $idx = $token; }
	// 					foreach ($flow_approval as $keys => $values) {
	// 						if($values['number'] == $idx){
	// 							unset($flow_approval[$keys]);
	// 						}
	// 					}
	// 					$key_x = $numb;
	// 					if($key_x == 'employee'){
	// 						$json['req_flow'][$key_x.'_approve']='x';
	// 						$json['req_flow'][$key_x.'_dates']=$date;
	// 						$json['req_flow'][$key_x.'_times']=$time;
	// 					}else{
	// 						$user_x = $json[$key_x];

	// 						foreach($user_x as $key => $value){
	// 							if(isset($value[$token])){
	// 								$json[$key_x][$key][$token] =  1;
	// 								$json[$key_x][$key][$key_x.'_stat'] = 1;
	// 								$json[$key_x][$key][$key_x.'_date'] = $date;
	// 								$json[$key_x][$key][$key_x.'_time'] = $time;
	// 								$json['req_flow'][$key_x.'_approve'] = 'x';
	// 								$json[$key_x][$key]['read_stat'] = 1;
	// 							}else{
	// 								$json[$key_x][$key]['read_stat'] = 1;
	// 							}
	// 						}
	// 					}

	// 					foreach($json['chat_id']  as  $keyy  => $valuee){
	// 						$json['chat_id'][$keyy] = 'r';
	// 					}
	// 					//return [$flow_approval,$json,3];
	// 					//return [$flow_approval, $numb,$json,$token,12];
	// 					$check_ado = \DB::SELECT("select distinct leave_request_id from leave_ado where leave_request_id = $id");
	// 					$ado_include = false;
	// 					if(isset($check_ado[0])){
	// 						$ado_include = true;
	// 					}
	// 					if($ado_include || $i['leave_type']=='Accumulation Day Off'){
	// 						$get_ado = \DB::SELECT("SELECT `date_ado`, `schedule_id`, `name_ado`, `change_date` ,`id` , `employee_id` from leave_ado where leave_request_id=$id");

	// 						foreach ($get_ado as $key => $value) {
	// 							$emps_ado 	= $value->employee_id;
	// 							$dt_ado 	= $value->date_ado;
	// 							$shift_ado 	= $value->schedule_id;
	// 							$ch_ado 	= $value->change_date;
	// 							if(count($flow_approval) == 0){									
	// 								// $up1 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$dt_ado',$shift_ado)"); 14082018
	// 								// $up2 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$ch_ado',42)"); 14082018
	// 							}
	// 						}
	// 					}

	// 					/**
	// 					* update json_data;
	// 					**/
	// 					$json_x  =  json_encode($json,1);

	// 					if(count($flow_approval) == 0){
	// 						//return [$json,"flow_approval"=>$flow_approval,"get_step == 1 && aproval null :: single :: leave","FLOW LENGKAP"];	
	// 						$schedule = \DB::SELECT("update leave_request set status_id = 2,approver =  '$token', approval = 0 where id  =  $id ");
	// 					}else{
	// 						//return [$json,"flow_approval"=>$flow_approval,"get_step == 1 && aproval null :: single :: leave","FLOW BELUM LENGKAP"];	
	// 					}

	// 					//return [$json,"flow_approval"=>$flow_approval,"get_step == 1 && aproval null :: single :: leave"];
	// 					//$schedule = \DB::SELECT("update leave_request set status_id = 2,approver =  2, approval = 0 where id  =  $id ");
	// 					//$schedule = \DB::SELECT("update leave_request set status_id = 2,approver =  '$token', approval = 0 where id  =  $id ");
	// 					$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
	// 				}
	// 			}
	// 		}
	// 		//return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
	// 	}
	// 	//return "ok";
	// 	$input =   \Input::get('master');
	// 	if(isset($input)){

	// 		if($input['master']  == "leave"){
	// 			$employee_id  =  $input['employee_id'];

	// 			$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
	// 			if(count($set_data) > 0){		
	// 				if(isset($set_data[0]->work_email)){
	// 					$set_email = $set_data[0]->work_email;
	// 				}else if(isset($set_data[0]->personal_email)){
	// 					$set_email = $set_data[0]->personal_email;
	// 				}
	// 				//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

	// 				$namex  =  $input['employee'];
	// 				$request =  $input['leave_type'];
	// 				$email   =  $set_data;
	// 			}else{
	// 				return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
	// 			}
	// 		}else{
	// 			$employee_id  =  $input['employee_id'];

	// 			$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
	// 			if(count($set_data) > 0){
	// 				if(isset($set_data[0]->work_email)){
	// 					$set_email = $set_data[0]->work_email;
	// 				}else if(isset($set_data[0]->personal_email)){
	// 					$set_email = $set_data[0]->personal_email;
	// 				}
	// 				//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

	// 				$namex  =  $input['employee'];
	// 				$request =  $input['type'];
	// 				$email   =  $set_data;
	// 			}else{
	// 				return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
	// 			}
	// 		}

	// 		if($email != null){
	// 			$data = array('name'=>$namex,"request" => $request, 'email' =>  $email);
	// 			\Mail::send('approve.employee', $data, function($message) use ($data){
	// 				$req = $data['request'];
	// 				$message->to($data['email'])->subject("Leekie request reminder $req");
	// 			});
	// 		}

	// 		return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
	// 	}else{
	// 		$employee_id  =  $i['employee_id'];

	// 		$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");

	// 		if(count($set_data) > 0){

	// 			if(isset($set_data[0]->work_email)){
	// 				$set_email = $set_data[0]->work_email;
	// 			}else if(isset($set_data[0]->personal_email)){
	// 				$set_email = $set_data[0]->personal_email;
	// 			}
	// 			//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

	// 			//hide email
	// 			if(isset($i['employee'])){ $namex = $i['employee']; }
	// 			else{ $namex = $i['EmployeeName']; }

	// 			if(isset($i['type'])){ $request = $i['type']; }
	// 			else{ $request = $i['leave_type']; }

	// 			$email   =  $set_email;
	// 			if(strlen($set_data[0]->work_email) > 0){
	// 				$email   =  $set_data[0]->work_email;
	// 			}else{
	// 				if(strlen($set_data[0]->personal_email) > 0){
	// 					$email   =  $set_data[0]->personal_email;	
	// 				}else{
	// 					$email = null;
	// 				}
	// 			}
	// 			if($email != null){
	// 				$data = array('name'=>$namex,"request" => $request, 'email' => $email);
	// 				\Mail::send('emails.approve', $data, function($message) use($data){
	// 					$req = $data['request'];
	// 					$message->to($data['email'],$data['name'])->subject("Leekie request reminder $req");
	// 					return (array)\Mail::failures();
	// 				});
	// 				return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
	// 				// \Mail::send('emails.approve', $data, function($message) use ($data){
	// 				// 	return $data;
	// 				// 	$message->to($data['email'])->subject('Leekie request reminder');
	// 				// 	return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
	// 				// });
	// 			}
	// 		}else{
	// 			return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
	// 		}

	// 	}

	// } 
	public function approve(){

		$token =  explode('-',base64_decode((\Input::get('key'))))[1];  
		$set_email = null;

		$check_role = \DB::SELECT("select distinct job.title, emp.employee_id from emp, job_history, job where emp.employee_id = job_history.employee_id and job.id =  job_history.job and emp.employee_id = '$token' and ((lower(job.title) like '%hr%') or (lower(job.title) like '%human resource%') or (lower(job.title) like '%human%'))");
		$who 		= null;
		if(count($check_role) > 0){
			//if(/*$check_role[0]->role_id == 37 || */$check_role[0]->role_id == 38){
				$who = "hr";
			//}
		}

		$i  = \Input::all()["0"];

		$flow_approval=[];
		
		if(isset($i)){
			if($i['master_type'] == 1 ){
				
				if(isset($i['sub'])){ // MULTI CRUD
					for ($j=0; $j < count($i['sub']); $j++) { 
						// $id  =  $i['sub'][$j];
						// $get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
						// $json  =  json_decode($get_pool[0]->json_data,1);

						// $flow =  $json['req_flow'];
						
						// /**
						// *	get time and date 
						// **/

						// $time  =  date('H:i');
						// $date  = date('Y-m-d');

						// /**
						// * get user stat 2 or just 1 stat
						// **/

						// $get_step  =  0;
						// $user = '';
						// $temp = [];

						// /**	
						// *	get flow user
						// **/
						// foreach($flow as $key => $value){
						// 	$temp[] = $value;		
						// }
						$id  =  /*$i['id']*/  $i['sub'][$j];
						$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
						$json  =  json_decode($get_pool[0]->json_data,1);

						$flow =  $json['req_flow'];
						
						$hrxcomp = $supxcomp = $swapcomp = false;
						if(isset($json['hrx_comp'])){
							$hrxcomp = array_search($token,$json['hrx_comp']);
						}
						if(isset($json['supx_comp'])){
							$supxcomp = array_search($token,$json['supx_comp']);
						}
						if(isset($json['swapx_compx'])){
							$swapcomp = array_search($token,$json['swapx_compx']);
						}
						
							if(gettype($hrxcomp) == 'integer'){  $numb = "hr"; }
							else if(gettype($supxcomp) == 'integer'){ $numb = "sup"; }
							else if(gettype($swapcomp) == 'integer'){ $numb = "swap"; }
						


						if(isset($flow['employee'])){
							if($flow['employee'] == $token){
								$numb = "employee";
							}
						}

						//return [gettype($hrxcomp) == 'integer',gettype($hrxcomp)];
						/**
						*	get time and date 
						**/

						$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
						$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
						$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
						$date  = $datess->format('Y-m-d');

						/**
						* get user stat 2 or just 1 stat
						**/

						$get_step  =  0;
						$user = '';
						$temp = [];

						/**	
						*	get flow user
						**/
						
						foreach($flow as $key => $value){
							$temp[] = $value;
							if($key == "hr" || $key == "sup" || $key == "employee"){					
								if($value > 0){

									if($key == 'employee'){
										$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];	
									}else{
										$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];
									}
									if($key == 'employee_dates' || $key == 'employee_times'){
										$key = 'employee';
									}
									if($flow[$key."_approve"] != "x"){
										//$tmp['approve'] = true;
										array_push($flow_approval,$tmp);
									}
								}
							}
						}





						/**	
						*check using in array target data exist
						**/

						if(in_array(2,$temp)){
							$get_step = 2;
						}else{
							$get_step = 1;
						}

						/**
						*	if 2 step approval
						*	check approval if already approved
						*   if approval already approve
						*	then approve approver
						**/
						
						$get_already_approval  = \DB::SELECT("select * from att_schedule_request where id =  $id and status_id = 1");
						
						if($get_already_approval[0]->id){				
							$objs['dt_update'] = [
									"id" 			=> $id,
									"request"		=> $get_already_approval[0]->request_id,
									"employee_id"	=> $i['employee_id'],
									"type_id"		=> $get_already_approval[0]->type_id,
									"approval"		=> 0,
									"approver"		=> $token,
									"status"		=> 2,
									"comment"=>"0"
								];
							
							if(isset($i['comment'])){
								$objs['dt_update']["comment"]=$i['comment'];
							}

							// APPROVAL BY
							if($get_already_approval[0]->type_id == 1 || $get_already_approval[0]->type_id == 2){

								$sup = array_search($token,$json['supx_comp']);
								if($i['employee_id'] == $token){
									$stat = true;
								}elseif(gettype($sup) == 'integer'){
									$stat = true;
								}
							}elseif($get_already_approval[0]->type_id == 3 || $get_already_approval[0]->type_id == 4 || $get_already_approval[0]->type_id == 5){
								$sup = array_search($token,$json['supx_comp']);
								if(gettype($sup) == 'integer'){
									$stat = true;
								}
							}elseif($get_already_approval[0]->type_id == 6){

								$sup = array_search($token,$json['supx_comp']);
								$swap = array_search($token,$json['swapx_compx']);
								if($i['employee_id'] == $token){
									$stat = true;
								}elseif(gettype($sup) == 'integer'){
									$stat = true;
								}elseif(gettype($swap) == 'integer'){
									$stat = true;
								}
							}elseif($get_already_approval[0]->type_id == 7 || $get_already_approval[0]->type_id == 8){
								// backup 09/10/2018	
								$hr = array_search($token,$json['hrx_comp']);
								if(gettype($hr) == 'integer'){
									$stat = true;
								}else{
									if(isset($json['req_flow']['employee_requestor'])){
									
										if($json['req_flow']['employee_requestor'][0] == '2014888'){
									
											$sup = array_search($token,$json['supx_comp']);
											if(gettype($sup) == 'integer'){
												$stat = true;		
											}
										}
									}
									/*else{
										$sup = array_search($token,$json['supx_comp']);
										if(gettype($sup) == 'integer'){
											$stat = true;		
										}
									}*/
								}
							}elseif($get_already_approval[0]->type_id == 9) {
								$sup = array_search($token,$json['supx_comp']);
								if($i['employee_id'] == $token){
									$stat = true;
								}elseif(gettype($sup) == 'integer'){
									$stat = true;
								}
								if(!isset($stat)){
									$hr = array_search($token,$json['hrx_comp']);
									if(gettype($hr) == 'integer'){
										$stat = true;
									}
								}
							}else{
								$stat = false;
							}
						}

						if(isset($numb)){
							if($numb == 'employee'){
								$stat = true;
							}
						}
						if(!isset($stat)){
							return \Response::json(['header'=>['message'=>"You don't have permission to approve this request",'status'=>500],'data'=>null],500);
						}

						// if($get_already_approval[0]->type_id == 9){
						// 	$req_id = $get_already_approval[0]->request_id;
						// 	$emp_req = $get_already_approval[0]->employee_id;
						// 	$att_timeInOutReq = \DB::SELECT("select * from att_time_in_out_req where req_in_out_id = $req_id and employee_id = '$emp_req' and status_id = 1");
						// 	if(count($att_timeInOutReq) > 0 && $att_timeInOutReq[0]->req_in_out_id){	

						// 		$att_timeInOutReq = $att_timeInOutReq[0];
								
						// 		if($att_timeInOutReq->req_out){						
						// 			$out = explode(":",$att_timeInOutReq->req_out);
						// 			$in = explode(":",$att_timeInOutReq->req_in);
						// 			if((Integer)$out[0] == 0 && (Integer)$out[1] == 0){
									
						// 				$inquire_dates = null;
						// 			}elseif((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
									
						// 				$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
						// 			}elseif((Integer)$in[0] <= 23 && (Integer)$out[0] <= 23){
						// 				$inquire_dates = null;
						// 			}elseif((Integer)$in[0] <= 23){
						// 				$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
						// 			}
						// 		}else{
						// 			if((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
						// 				$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
						// 			}elseif((Integer)$in[0] <= 23 ){
						// 				$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
						// 			}else{
						// 				$inquire_dates = null;
						// 			}
						// 		}
						// 		$objs['biometrics_device'] = [
						// 					'device_number'=> '001',
						// 					'time_in' => $att_timeInOutReq->req_in,
						// 					'time_out' => $att_timeInOutReq->req_out,
						// 					'date' => $att_timeInOutReq->date,
						// 					'inquire_date'=>$inquire_dates,
						// 					'date'=> $att_timeInOutReq->date,
						// 					'employee_id'=>$i['employee_id'],
						// 					'authen_id'=> 0,
						// 					'input_'=>1
						// 				];
						// 	}else{
						// 		return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => [/*"select * from att_time_in_out_req where req_in_out_id = $req_id and employee_id = '$emp_req' and status_id = 1","select * from att_schedule_request where id =  $id"*/]],500);
						// 	}
						// }
						if($get_already_approval[0]->type_id == 9){
							$req_id = $get_already_approval[0]->request_id;
							$emp_req = $get_already_approval[0]->employee_id;
							$att_timeInOutReq = \DB::SELECT("select * from att_time_in_out_req where req_in_out_id = $req_id and employee_id = '$emp_req' and status_id = 1");
							//return [$att_timeInOutReq];
							if(count($att_timeInOutReq) > 0 && $att_timeInOutReq[0]->req_in_out_id){	

								$att_timeInOutReq = $att_timeInOutReq[0];
								
								if($att_timeInOutReq->req_out){						
									$out = explode(":",$att_timeInOutReq->req_out);
									$in = explode(":",$att_timeInOutReq->req_in);
									if((Integer)$out[0] == 0 && (Integer)$out[1] == 0){
									
										$inquire_dates = null;
									}elseif((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
									
										$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
									}
									elseif((Integer)$in[0] <= 23 && (Integer)$out[0] <= 23){
										$inquire_dates = null;
									}elseif((Integer)$in[0] <= 23){
										$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
									}

									$timeIn 	= strtotime($att_timeInOutReq->req_in);
									$timeOut 	= strtotime($att_timeInOutReq->req_out);

									$timein_new       = explode(":",date("g:i:a", strtotime($att_timeInOutReq->date." ".$att_timeInOutReq->req_in)));
									$timeout_new       = explode(":",date("g:i:a", strtotime($att_timeInOutReq->date." ".$att_timeInOutReq->req_out)));

									
									if($timein_new[2] == "am"  && $timeout_new[2] == "am"){
										if($timeOut < $timeIn){
											$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
										}else{
											$inquire_dates = null;		
										}
									}else if($timein_new[2] == "pm"  && $timeout_new[2] == "am"){
										$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
									}else{
										if(!$inquire_dates){
											$inquire_dates = null;
										}
									}
								}else{

									if((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
										$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
									}elseif((Integer)$in[0] <= 23 ){
										$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
									}else{
										if($att_timeInOutReq->req_out){

											$timeIn 	= strtotime($att_timeInOutReq->req_in);
											$timeOut 	= strtotime($att_timeInOutReq->req_out);

											$timein_new       = date("g:i:a", strtotime($att_timeInOutReq->date." ".$att_timeInOutReq->req_in));
											$timeout_new       = date("g:i:a", strtotime($att_timeInOutReq->date." ".$att_timeInOutReq->req_out));

											if($timein_new[2] == "am"  && $timeout_new[2] == "am"){
												if($timeOut < $timeIn){
													$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
												}else{
													$inquire_dates = null;		
												}
											}else if($timein_new[2] == "pm"  && $timeout_new[2] == "am"){
												$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
											}else{
												$inquire_dates = null;
											}
										}else{
											$inquire_dates = null;
										}

									}
								}

								if($att_timeInOutReq->nextday > 0){
									$inquire_dates =  date('Y-m-d', strtotime($att_timeInOutReq->date . ' +1 day'));
								}else{
									$inquire_dates = null;
								}

								$objs['biometrics_device'] = [
											'device_number'=> '001',
											'time_in' => $att_timeInOutReq->req_in,
											'time_out' => $att_timeInOutReq->req_out,
											'date' => $att_timeInOutReq->date,
											'inquire_date'=>$inquire_dates,
											'date'=> $att_timeInOutReq->date,
											'employee_id'=>$i['employee_id'],
											'authen_id'=> 0,
											'input_'=>1
										];
							}else{
								return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => [/*"select * from att_time_in_out_req where req_in_out_id = $req_id and employee_id = '$emp_req' and status_id = 1","select * from att_schedule_request where id =  $id"*/]],500);
							}
						}
						if(in_array($get_already_approval[0]->type_id, [7,8,9])){
							for ($z=0; $z < count($flow_approval) ; $z++) { 
								if($flow_approval[$z]['flow'] == 'sup'){
									unset($flow_approval[$z]);
								}
							}
						}
						
						if($get_step == 2){
							
							if($get_already_approval[0]->approval != null ){
								$arr_cek =   [1,3,4];
								if(in_array($get_already_approval[0]->approval,$arr_cek)){
									return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
								}
					
								//\DB::SELECT("update att_schedule_request set approver =  2 where id = $id");
								
								// foreach($json['req_flow'] as $key  => $value){
								// 	if($value == 2){
								// 		$key_x = $key;
								// 	}
								// }

								// $user_x = $json[$key_x];

								// foreach($user_x as $key => $value){
								// 	if(isset($value[$token])){
								// 		$json[$key_x][$key][$token] =  1;
								// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
								// 		$json[$key_x][$key][$key_x.'_date'] = $date;
								// 		$json[$key_x][$key][$key_x.'_time'] = $time;
								// 		$json[$key_x][$key]['read_stat'] = 1;
								// 		$json['req_flow'][$key_x.'_approve'] = 'x';
								// 	}else{
								// 		$json[$key_x][$key]['read_stat'] = 1;
								// 	}
								// }

								// /**  
								//  * chnage  all chat to already read notif  has gone or end 
								//  */

								// foreach($json['chat_id']  as  $keyy  => $valuee){
								// 	$json['chat_id'	][$keyy] = 'r';
								// }
								if($numb == "hr"){ $idx = 1; }
								else if($numb == "sup"){ $idx = 2; }
								else if($numb == "swap"){ $idx = 3; }
								else if($numb == "employee"){ $idx = $token; }
								foreach ($flow_approval as $keys => $values) {
									if($values['flow'] == $numb){
										unset($flow_approval[$keys]);
									}
									if($numb == 'hr' && $get_already_approval[0]->type_id == 9 && $i['master_type'] == 1){
										if($values['flow'] == 'employee'){
											unset($flow_approval[$keys]);
										}	
									}
								}
								$key_x = $numb;
								if($key_x == 'employee'){
									$json['req_flow'][$key_x.'_approve']='x';
									$json['req_flow'][$key_x.'_dates']=$date;
									$json['req_flow'][$key_x.'_times']=$time;
								}else{
									$user_x = $json[$key_x];

									foreach($user_x as $key => $value){
										if(isset($value[$token])){
											$json[$key_x][$key][$token] =  1;
											$json[$key_x][$key][$key_x.'_stat'] = 1;
											$json[$key_x][$key][$key_x.'_date'] = $date;
											$json[$key_x][$key][$key_x.'_time'] = $time;
											$json['req_flow'][$key_x.'_approve'] = 'x';
											$json[$key_x][$key]['read_stat'] = 1;
										}else{
											$json[$key_x][$key]['read_stat'] = 1;
										}
									}
								}

								foreach($json['chat_id']  as  $keyy  => $valuee){
									$json['chat_id'][$keyy] = 'r';
								}


								/**
								* update json_data;
								**/
								//return ["update att_schedule_request set status_id = 2,approver =  2 where id  =  $id "];
								$json_x  =  json_encode($json,1);
								if(count($flow_approval) ==0 ){
								//$schedule = \DB::SELECT("update att_schedule_request set status_id = 2,approver =  2 where id  =  $id ");
								 $this->update_schedule_list($objs,'approve');
								}
								$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
								

							}else{
								
								if($get_already_approval[0]->approval == null){
									
									/**
									* eg : $json[sup] or $json[sup]
									**/
									
									// foreach($json['req_flow'] as $key  => $value){
									// 	if($value == 1){
									// 		$key_x = $key;
									// 	}
									// }

									// $user_x = $json[$key_x];

									// foreach($user_x as $key => $value){
									// 	if(isset($value[$token])){
									// 		$json[$key_x][$key][$token] =  1;
									// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
									// 		$json[$key_x][$key][$key_x.'_date'] = $date;
									// 		$json[$key_x][$key][$key_x.'_time'] = $time;
									// 		$json[$key_x][$key]['read_stat'] = 1;
									// 		$json['req_flow'][$key_x.'_approve'] = 'x';
									// 	}else{
									// 	   $json[$key_x][$key]['read_stat'] = 1;
									// 	}
									// }

									// foreach($json['chat_id']  as  $keyy  => $valuee){
									// 		$json['chat_id'][$keyy] = 'r';
									// }
									if($numb == "hr"){ $idx = 1; }
									else if($numb == "sup"){ $idx = 2; }
									else if($numb == "swap"){ $idx = 3; }
									else if($numb == "employee"){ $idx = $token; }
									foreach ($flow_approval as $keys => $values) {
										if($values['flow'] == $numb){
											unset($flow_approval[$keys]);
										}
									}
									$key_x = $numb;
									if($key_x == 'employee'){
										$json['req_flow'][$key_x.'_approve']='x';
										$json['req_flow'][$key_x.'_dates']=$date;
										$json['req_flow'][$key_x.'_times']=$time;
									}else{
										$user_x = $json[$key_x];

										foreach($user_x as $key => $value){
											if(isset($value[$token])){
												$json[$key_x][$key][$token] =  1;
												$json[$key_x][$key][$key_x.'_stat'] = 1;
												$json[$key_x][$key][$key_x.'_date'] = $date;
												$json[$key_x][$key][$key_x.'_time'] = $time;
												$json['req_flow'][$key_x.'_approve'] = 'x';
												$json[$key_x][$key]['read_stat'] = 1;
											}else{
												$json[$key_x][$key]['read_stat'] = 1;
											}
										}
									}

									foreach($json['chat_id']  as  $keyy  => $valuee){
										$json['chat_id'][$keyy] = 'r';
									}

									/**
									* update json_data;
									**/
									
									$json_x  =  json_encode($json,1);
									//return [$get_step,$get_already_approval,$flow_approval];
									if(count($flow_approval) ==0){
										//\DB::SELECT("update att_schedule_request set status_id =2,  approver =  0, approval = 2 where id = $id");							
										$this->update_schedule_list($objs,'approve');
									}
									$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
								}
							}
						}else{
							
							if($get_already_approval[0]->approval == null){
								//\DB::SELECT("update att_schedule_request set approver =  2, approval = 0 where id = $id");
								/**
								* eg : $json[sup] or $json[sup]
								**/
								
								/*foreach($json['req_flow'] as $key  => $value){
									if($value == 1){
										$key_x = $key;
									}
								}

								$user_x = $json[$key_x];

								foreach($user_x as $key => $value){
									if(isset($value[$token])){
										$json[$key_x][$key][$token] =  1;
										$json[$key_x][$key][$key_x.'_stat'] = 1;
										$json[$key_x][$key][$key_x.'_date'] = $date;
										$json[$key_x][$key][$key_x.'_time'] = $time;
										$json[$key_x][$key]['read_stat'] = 1;
										$json['req_flow'][$key_x.'_approve'] = 'x';
									}else{
										$json[$key_x][$key]['read_stat'] = 1;
									}
								}

								foreach($json['chat_id']  as  $keyy  => $valuee){
									$json['chat_id'][$keyy] = 'r';
								}*/
								if($numb == "hr"){ $idx = 1; }
								else if($numb == "sup"){ $idx = 2; }
								else if($numb == "swap"){ $idx = 3; }
								else if($numb == "employee"){ $idx = $token; }
								foreach ($flow_approval as $keys => $values) {
									if($values['flow'] == $numb){
										unset($flow_approval[$keys]);
									}

									if($numb == 'hr' && $get_already_approval[0]->type_id == 9 && $i['master_type'] == 1){
										if($values['flow'] == 'employee'){
											unset($flow_approval[$keys]);
										}	
									}
								}
								$key_x = $numb;
								if($key_x == 'employee'){
									$json['req_flow'][$key_x.'_approve']='x';
									$json['req_flow'][$key_x.'_dates']=$date;
									$json['req_flow'][$key_x.'_times']=$time;
								}else{
									$user_x = $json[$key_x];

									foreach($user_x as $key => $value){
										if(isset($value[$token])){
											$json[$key_x][$key][$token] =  1;
											$json[$key_x][$key][$key_x.'_stat'] = 1;
											$json[$key_x][$key][$key_x.'_date'] = $date;
											$json[$key_x][$key][$key_x.'_time'] = $time;
											$json['req_flow'][$key_x.'_approve'] = 'x';
											$json[$key_x][$key]['read_stat'] = 1;
										}else{
											$json[$key_x][$key]['read_stat'] = 1;
										}
									}
								}

								foreach($json['chat_id']  as  $keyy  => $valuee){
									$json['chat_id'][$keyy] = 'r';
								}

								/**
								* update json_data;
								**/
								
								$json_x  =  json_encode($json,1);
								//return \Response::json(['data'=>$flow_approval],200);
								//$schedule = \DB::SELECT("update att_schedule_request set status_id = 2,approver =  2, approval = 0 where id  =  $id ");
								if(count($flow_approval) == 0){
									$this->update_schedule_list($objs,'approve');
								}
								
								$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
							}
						}						
					}// end for
				}//endif
				else{ // SINGLE CRUD
					// $id  =  $i['id'];
					// $get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
					// $json  =  json_decode($get_pool[0]->json_data,1);

					// $flow =  $json['req_flow'];
					
					// /**
					// *	get time and date 
					// **/

					// $time  =  date('H:i');
					// $date  = date('Y-m-d');

					// /**
					// * get user stat 2 or just 1 stat
					// **/

					// $get_step  =  0;
					// $user = '';
					// $temp = [];

					// /**	
					// *	get flow user
					// **/
					// foreach($flow as $key => $value){
					// 	$temp[] = $value;		
					// }

					$id  =  $i['id'];
					$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
					$json  =  json_decode($get_pool[0]->json_data,1);

					$flow =  $json['req_flow'];
					
					$hrxcomp = $supxcomp = $swapcomp = false;
					if(isset($json['hrx_comp'])){
						$hrxcomp = array_search($token,$json['hrx_comp']);
					}
					if(isset($json['supx_comp'])){
						$supxcomp = array_search($token,$json['supx_comp']);
					}
					if(isset($json['swapx_compx'])){
						$swapcomp = array_search($token,$json['swapx_compx']);
					}
					
						if(gettype($hrxcomp) == 'integer'){  $numb = "hr"; }
						else if(gettype($supxcomp) == 'integer'){ $numb = "sup"; }
						else if(gettype($swapcomp) == 'integer'){ $numb = "swap"; }
					


					if(isset($flow['employee'])){
						if($flow['employee'] == $token){
							$numb = "employee";
						}
					}
					//return [gettype($hrxcomp) == 'integer',gettype($hrxcomp)];
					/**
					*	get time and date 
					**/

					$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
					$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
					$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
					$date  = $datess->format('Y-m-d');

					/**
					* get user stat 2 or just 1 stat
					**/

					$get_step  =  0;
					$user = '';
					$temp = [];

					/**	
					*	get flow user
					**/
					
					foreach($flow as $key => $value){
						$temp[] = $value;
						if($key == "hr" || $key == "sup" || $key == "employee"){					
							if($value > 0){

								if($key == 'employee'){
									$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];	
								}else{
									$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];
								}
								if($key == 'employee_dates' || $key == 'employee_times'){
									$key = 'employee';
								}
								if($flow[$key."_approve"] != "x"){
									//$tmp['approve'] = true;
									array_push($flow_approval,$tmp);
								}
							}
						}
					}

					/**	
					*check using in array target data exist
					**/

					if(in_array(2,$temp)){
						$get_step = 2;
					}else{
						$get_step = 1;
					}

					/**
					*	if 2 step approval
					*	check approval if already approved
					*   if approval already approve
					*	then approve approver
					**/
					
					$get_already_approval  = \DB::SELECT("select * from att_schedule_request where id =  $id and status_id = 1");
					$stat = false;
					if($get_already_approval[0]->id){				
						$objs['dt_update'] = [
								"id" 			=> $id,
								"request"		=> $get_already_approval[0]->request_id,
								"employee_id"	=> $i['employee_id'],
								"type_id"		=> $get_already_approval[0]->type_id,
								"approval"		=> 0,
								"approver"		=> $token,
								"status"		=> 2,
								"comment"=>"0"
							];
						
						if(isset($i['comment'])){
							$objs['dt_update']["comment"]=$i['comment'];
						}

						if($get_already_approval[0]->type_id == 1 || $get_already_approval[0]->type_id == 2){

							$sup = array_search($token,$json['supx_comp']);
							if($i['employee_id'] == $token){
								$stat = true;
							}elseif(gettype($sup) == 'integer'){
								$stat = true;
							}
						}elseif($get_already_approval[0]->type_id == 3 || $get_already_approval[0]->type_id == 4 || $get_already_approval[0]->type_id == 5){
							$sup = array_search($token,$json['supx_comp']);
							if(gettype($sup) == 'integer'){
								$stat = true;
							}
						}elseif($get_already_approval[0]->type_id == 6){

							$sup = array_search($token,$json['supx_comp']);
							$swap = array_search($token,$json['swapx_compx']);
							if($i['employee_id'] == $token){
								$stat = true;
							}elseif(gettype($sup) == 'integer'){
								$stat = true;
							}elseif(gettype($swap) == 'integer'){
								$stat = true;
							}
						}elseif($get_already_approval[0]->type_id == 7 || $get_already_approval[0]->type_id == 8){
							$hr = array_search($token,$json['hrx_comp']);
							if(gettype($hr) == 'integer'){
								$stat = true;
							}
						}elseif($get_already_approval[0]->type_id == 9) {
							$sup = array_search($token,$json['supx_comp']);
							if($i['employee_id'] == $token){
								$stat = true;
							}elseif(gettype($sup) == 'integer'){
								$stat = true;
							}
							if(!isset($stat)){
								$hr = array_search($token,$json['hrx_comp']);
								if(gettype($hr) == 'integer'){
									$stat = true;
								}
							}
						}else{
							$stat = false;
						}

					}

					if(isset($numb)){
						if($numb == 'employee'){
							$stat = true;
						}
					}
					if(!$stat){
						return \Response::json(['header'=>['message'=>"You don't have permission to approve this request",'status'=>500],'data'=>null],500);
					}

					// if($get_already_approval[0]->type_id == 9){
					// 	$req_id = $get_already_approval[0]->request_id;
					// 	$att_timeInOutReq = \DB::SELECT("select * from att_time_in_out_req where req_in_out_id = $req_id")[0];
					// 	if($att_timeInOutReq->req_in_out_id){	

					// 		if($att_timeInOutReq->req_out){						
					// 			$out = explode(":",$att_timeInOutReq->req_out);
					// 			$in = explode(":",$att_timeInOutReq->req_in);
					// 			if((Integer)$out[0] == 0 && (Integer)$out[1] == 0){
								
					// 				$inquire_dates = null;
					// 			}elseif((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
								
					// 				$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
					// 			}elseif((Integer)$in[0] <= 23 && (Integer)$out[0] <= 23){
					// 				$inquire_dates = null;
					// 			}elseif((Integer)$in[0] <= 23){
					// 				$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
					// 			}
					// 		}else{
					// 			if((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
					// 				$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
					// 			}elseif((Integer)$in[0] <= 23 ){
					// 				$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
					// 			}else{
					// 				$inquire_dates = null;
					// 			}
					// 		}
					// 		$objs['biometrics_device'] = [
					// 					'device_number'=> '001',
					// 					'time_in' => $att_timeInOutReq->req_in,
					// 					'time_out' => $att_timeInOutReq->req_out,
					// 					'date' => $att_timeInOutReq->date,
					// 					'inquire_date'=>$inquire_dates,
					// 					'date'=> $att_timeInOutReq->date,
					// 					'employee_id'=>$i['employee_id'],
					// 					'authen_id'=> 0,
					// 					'input_'=>1
					// 				];
					// 	}else{
					// 		return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
					// 	}
					// }
					if($get_already_approval[0]->type_id == 9){
						$req_id = $get_already_approval[0]->request_id;
						$emp_req = $get_already_approval[0]->employee_id;
						$att_timeInOutReq = \DB::SELECT("select * from att_time_in_out_req where req_in_out_id = $req_id and employee_id = '$emp_req' and status_id = 1");
						if(count($att_timeInOutReq) > 0 && $att_timeInOutReq[0]->req_in_out_id){	

							$att_timeInOutReq = $att_timeInOutReq[0];
							
							if($att_timeInOutReq->req_out){						
								$out = explode(":",$att_timeInOutReq->req_out);
								$in = explode(":",$att_timeInOutReq->req_in);
								if((Integer)$out[0] == 0 && (Integer)$out[1] == 0){
								
									$inquire_dates = null;
								}elseif((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
								
									$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
								}
								elseif((Integer)$in[0] <= 23 && (Integer)$out[0] <= 23){
									$inquire_dates = null;
								}elseif((Integer)$in[0] <= 23){
									$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
								}

								$timeIn 	= strtotime($att_timeInOutReq->req_in);
								$timeOut 	= strtotime($att_timeInOutReq->req_out);

								$timein_new       = explode(":",date("g:i:a", strtotime($att_timeInOutReq->date." ".$att_timeInOutReq->req_in)));
								$timeout_new       = explode(":",date("g:i:a", strtotime($att_timeInOutReq->date." ".$att_timeInOutReq->req_out)));

								
								if($timein_new[2] == "am"  && $timeout_new[2] == "am"){
									if($timeOut < $timeIn){
										$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
									}else{
										$inquire_dates = null;		
									}
								}else if($timein_new[2] == "pm"  && $timeout_new[2] == "am"){
									$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
								}else{
									if(!$inquire_dates){
										$inquire_dates = null;
									}
								}
							}else{

								if((Integer)$in[0] > 1 && (Integer)$in[0] < 4){
									$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
								}elseif((Integer)$in[0] <= 23 ){
									$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
								}else{
									if($att_timeInOutReq->req_out){

										$timeIn 	= strtotime($att_timeInOutReq->req_in);
										$timeOut 	= strtotime($att_timeInOutReq->req_out);

										$timein_new       = date("g:i:a", strtotime($att_timeInOutReq->date." ".$att_timeInOutReq->req_in));
										$timeout_new       = date("g:i:a", strtotime($att_timeInOutReq->date." ".$att_timeInOutReq->req_out));

										if($timein_new[2] == "am"  && $timeout_new[2] == "am"){
											if($timeOut < $timeIn){
												$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
											}else{
												$inquire_dates = null;		
											}
										}else if($timein_new[2] == "pm"  && $timeout_new[2] == "am"){
											$inquire_dates = date('Y-m-d',(strtotime ( '+1 day' , strtotime ($att_timeInOutReq->date))));
										}else{
											$inquire_dates = null;
										}
									}else{
										$inquire_dates = null;
									}

								}
							}
							$objs['biometrics_device'] = [
										'device_number'=> '001',
										'time_in' => $att_timeInOutReq->req_in,
										'time_out' => $att_timeInOutReq->req_out,
										'date' => $att_timeInOutReq->date,
										'inquire_date'=>$inquire_dates,
										'date'=> $att_timeInOutReq->date,
										'employee_id'=>$i['employee_id'],
										'authen_id'=> 0,
										'input_'=>1
									];
						}else{
							return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => [/*"select * from att_time_in_out_req where req_in_out_id = $req_id and employee_id = '$emp_req' and status_id = 1","select * from att_schedule_request where id =  $id"*/]],500);
						}
					}
					
					if($get_step == 2){
						
						if($get_already_approval[0]->approval != null ){
							$arr_cek =   [1,3,4];
							if(in_array($get_already_approval[0]->approval,$arr_cek)){
								return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
							}
				
							//\DB::SELECT("update att_schedule_request set approver =  2 where id = $id");
							
							// foreach($json['req_flow'] as $key  => $value){
							// 	if($value == 2){
							// 		$key_x = $key;
							// 	}
							// }

							// $user_x = $json[$key_x];

							// foreach($user_x as $key => $value){
							// 	if(isset($value[$token])){
							// 		$json[$key_x][$key][$token] =  1;
							// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
							// 		$json[$key_x][$key][$key_x.'_date'] = $date;
							// 		$json[$key_x][$key][$key_x.'_time'] = $time;
							// 		$json[$key_x][$key]['read_stat'] = 1;
							// 		$json['req_flow'][$key_x.'_approve'] = 'x';
							// 	}else{
							// 		$json[$key_x][$key]['read_stat'] = 1;
							// 	}
							// }

							// /**  
							//  * chnage  all chat to already read notif  has gone or end 
							//  */

							// foreach($json['chat_id']  as  $keyy  => $valuee){
							// 	$json['chat_id'	][$keyy] = 'r';
							// }
							if($numb == "hr"){ $idx = 1; }
							else if($numb == "sup"){ $idx = 2; }
							else if($numb == "swap"){ $idx = 3; }
							else if($numb == "employee"){ $idx = $token; }
							foreach ($flow_approval as $keys => $values) {
								if($values['flow'] == $numb){
									unset($flow_approval[$keys]);
								}
							}
							$key_x = $numb;
							if($key_x == 'employee'){
								$json['req_flow'][$key_x.'_approve']='x';
								$json['req_flow'][$key_x.'_dates']=$date;
								$json['req_flow'][$key_x.'_times']=$time;
							}else{
								$user_x = $json[$key_x];

								foreach($user_x as $key => $value){
									if(isset($value[$token])){
										$json[$key_x][$key][$token] =  1;
										$json[$key_x][$key][$key_x.'_stat'] = 1;
										$json[$key_x][$key][$key_x.'_date'] = $date;
										$json[$key_x][$key][$key_x.'_time'] = $time;
										$json['req_flow'][$key_x.'_approve'] = 'x';
										$json[$key_x][$key]['read_stat'] = 1;
									}else{
										$json[$key_x][$key]['read_stat'] = 1;
									}
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}


							/**
							* update json_data;
							**/
							
							//return ["update att_schedule_request set status_id = 2,approver =  2 where id  =  $id "];
							$json_x  =  json_encode($json,1);
							//$schedule = \DB::SELECT("update att_schedule_request set status_id = 2,approver =  2 where id  =  $id ");
							if(count($flow_approval) == 0){
								$this->update_schedule_list($objs,'approve');
							}
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
							

						}else{
							
							if($get_already_approval[0]->approval == null){
								
								/**
								* eg : $json[sup] or $json[sup]
								**/
								
								// foreach($json['req_flow'] as $key  => $value){
								// 	if($value == 1){
								// 		$key_x = $key;
								// 	}
								// }

								// $user_x = $json[$key_x];

								// foreach($user_x as $key => $value){
								// 	if(isset($value[$token])){
								// 		$json[$key_x][$key][$token] =  1;
								// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
								// 		$json[$key_x][$key][$key_x.'_date'] = $date;
								// 		$json[$key_x][$key][$key_x.'_time'] = $time;
								// 		$json[$key_x][$key]['read_stat'] = 1;
								// 		$json['req_flow'][$key_x.'_approve'] = 'x';
								// 	}else{
								// 	   $json[$key_x][$key]['read_stat'] = 1;
								// 	}
								// }

								// foreach($json['chat_id']  as  $keyy  => $valuee){
								// 		$json['chat_id'][$keyy] = 'r';
								// }
								if($numb == "hr"){ $idx = 1; }
								else if($numb == "sup"){ $idx = 2; }
								else if($numb == "swap"){ $idx = 3; }
								else if($numb == "employee"){ $idx = $token; }
								foreach ($flow_approval as $keys => $values) {
									if($values['flow'] == $numb){
										unset($flow_approval[$keys]);
									}
								}
								$key_x = $numb;
								if($key_x == 'employee'){
									$json['req_flow'][$key_x.'_approve']='x';
									$json['req_flow'][$key_x.'_dates']=$date;
									$json['req_flow'][$key_x.'_times']=$time;
								}else{
									$user_x = $json[$key_x];

									foreach($user_x as $key => $value){
										if(isset($value[$token])){
											$json[$key_x][$key][$token] =  1;
											$json[$key_x][$key][$key_x.'_stat'] = 1;
											$json[$key_x][$key][$key_x.'_date'] = $date;
											$json[$key_x][$key][$key_x.'_time'] = $time;
											$json['req_flow'][$key_x.'_approve'] = 'x';
											$json[$key_x][$key]['read_stat'] = 1;
										}else{
											$json[$key_x][$key]['read_stat'] = 1;
										}
									}
								}

								foreach($json['chat_id']  as  $keyy  => $valuee){
									$json['chat_id'][$keyy] = 'r';
								}

								/**
								* update json_data;
								**/
								
								$json_x  =  json_encode($json,1);
								//\DB::SELECT("update att_schedule_request set status_id =2,  approver =  0, approval = 2 where id = $id");	
								if(count($flow_approval) == 0){
									$this->update_schedule_list($objs,'approve');
								}						
								$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
							}
						}
					}else{
						
						if($get_already_approval[0]->approval == null){
							//\DB::SELECT("update att_schedule_request set approver =  2, approval = 0 where id = $id");
							/**
							* eg : $json[sup] or $json[sup]
							**/
							
							// foreach($json['req_flow'] as $key  => $value){
							// 	if($value == 1){
							// 		$key_x = $key;
							// 	}
							// }

							// $user_x = $json[$key_x];

							// foreach($user_x as $key => $value){
							// 	if(isset($value[$token])){
							// 		$json[$key_x][$key][$token] =  1;
							// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
							// 		$json[$key_x][$key][$key_x.'_date'] = $date;
							// 		$json[$key_x][$key][$key_x.'_time'] = $time;
							// 		$json[$key_x][$key]['read_stat'] = 1;
							// 		$json['req_flow'][$key_x.'_approve'] = 'x';
							// 	}else{
							// 		$json[$key_x][$key]['read_stat'] = 1;
							// 	}
							// }

							// foreach($json['chat_id']  as  $keyy  => $valuee){
							// 	$json['chat_id'][$keyy] = 'r';
							// }
							if($numb == "hr"){ $idx = 1; }
							else if($numb == "sup"){ $idx = 2; }
							else if($numb == "swap"){ $idx = 3; }
							else if($numb == "employee"){ $idx = $token; }
							foreach ($flow_approval as $keys => $values) {
								if($values['flow'] == $numb){
									unset($flow_approval[$keys]);
								}
							}
							$key_x = $numb;
							if($key_x == 'employee'){
								$json['req_flow'][$key_x.'_approve']='x';
								$json['req_flow'][$key_x.'_dates']=$date;
								$json['req_flow'][$key_x.'_times']=$time;
							}else{
								$user_x = $json[$key_x];

								foreach($user_x as $key => $value){
									if(isset($value[$token])){
										$json[$key_x][$key][$token] =  1;
										$json[$key_x][$key][$key_x.'_stat'] = 1;
										$json[$key_x][$key][$key_x.'_date'] = $date;
										$json[$key_x][$key][$key_x.'_time'] = $time;
										$json['req_flow'][$key_x.'_approve'] = 'x';
										$json[$key_x][$key]['read_stat'] = 1;
									}else{
										$json[$key_x][$key]['read_stat'] = 1;
									}
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}

							/**
							* update json_data;
							**/
							
							$json_x  =  json_encode($json,1);
							//$schedule = \DB::SELECT("update att_schedule_request set status_id = 2,approver =  2, approval = 0 where id  =  $id ");
							if(count($flow_approval) == 0){
								$this->update_schedule_list($objs,'approve');
							}
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
						}
					}						
				}

			}else{ // leave

				$id  =  $i['id'];
				$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 2 ");
				$json  =  json_decode($get_pool[0]->json_data,1);

				$flow =  $json['req_flow'];
				
				$hrxcomp = $supxcomp = $swapcomp = false;
				if(isset($json['hrx_comp'])){
					$hrxcomp = array_search($token,$json['hrx_comp']);
				}
				if(isset($json['supx_comp'])){
					$supxcomp = array_search($token,$json['supx_comp']);
				}
				if(isset($json['swapx_compx'])){
					$swapcomp = array_search($token,$json['swapx_compx']);
				}

				if($i['leave_type'] != 'Vacation Leave' && $json['local_it'] == 'expat'){
					$hrxcomp = false;
				}
				
					if(gettype($hrxcomp) == 'integer'){  $numb = "hr"; }
					else if(gettype($supxcomp) == 'integer'){ $numb = "sup"; }
					else if(gettype($swapcomp) == 'integer'){ $numb = "swap"; }
				


				if(isset($flow['employee'])){
					if($flow['employee'] == $token){
						$numb = "employee";
					}
				}
				//return [gettype($hrxcomp) == 'integer',gettype($hrxcomp)];
				/**
				*	get time and date 
				**/
				$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
				$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
				$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
				$date  = $datess->format('Y-m-d');

				
				/**
				* get user stat 2 or just 1 stat
				**/

				$get_step  =  0;
				$user = '';
				$temp = [];

				/**	
				*	get flow user
				**/
				
				foreach($flow as $key => $value){
					$temp[] = $value;
					if($key == "hr" || $key == "sup" || $key == "employee"){					
						if($value > 0){

							if($key == 'employee'){
								$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];	
							}else{
								$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];
							}
							if($key == 'employee_dates' || $key == 'employee_times'){
								$key = 'employee';
							}
							if($flow[$key."_approve"] != "x"){
								//$tmp['approve'] = true;
								/*if($i['leave_type'] == 'Vacation Leave' && $json['local_it'] == 'expat' && $key == 'hr'){

								}else{
								}*/
									array_push($flow_approval,$tmp);
							}
						}
					}
				}

				$in_arr = ["Sick Leave","Maternity Leave","Paternity Leave","Bereavement Leave","Marriage Leave","offday_oncall","Accumulation Day Off","Emergency Leave","Suspension"];
				if(in_array($i['leave_type'], $in_arr) && $json['local_it'] == 'expat'){
					for ($z=0; $z < count($flow_approval) ; $z++) { 
						if($flow_approval[$z]['flow'] == 'hr'){
							unset($flow_approval[$z]);
						}
					}
				}

				/**	
				*check using in array target data exist
				**/

				if(in_array(2,$temp)){
					$get_step = 2;
				}else{
					$get_step = 1;
				}

				/**
				*	if 2 step approval
				*	check approval if already approved
				*   if approval already approve
				*	then approve approver
				**/

				$get_already_approval  = \DB::SELECT("select * from leave_request where id =  $id and status_id = 1 ");

				
				if($get_step == 2){
					
					if($get_already_approval[0]->approval != null ){
						$arr_cek =   [1,3,4];
					
						if(in_array($get_already_approval[0]->approval,$arr_cek)){
							return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
						}
			
						//\DB::SELECT("update leave_request set approver =  2 where id = $id");
						
						// foreach($json['req_flow'] as $key  => $value){
								
						// 	$numb = 0;
						// 	if($value == 1){ $numb = 1; $hd = 'hr'; }
						// 	else if($value == 2){ $numb = 2; $hd = 'sup'; }
						// 	else if($value == 3){ $numb = 3; $hd = 'swap'; }								
						// 	if($json['req_flow'][$hd."_approve"] == 'o'){
						// 		$key_x = $hd;
						// 		foreach ($flow_approval as $keys => $values) {
						// 			if($values['number'] == $numb){
						// 				unset($flow_approval[$keys]);
						// 			}
						// 		}
						// 	}
						// }
						/*if($numb == "hr"){ $idx = 1; }
						else if($numb == "sup"){ $idx = 2; }
						else if($numb == "swap"){ $idx = 3; }
						foreach ($flow_approval as $keys => $values) {
							if($values['number'] == $idx){
								unset($flow_approval[$keys]);
							}
						}
						$key_x = $numb;

						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json[$key_x][$key]['read_stat'] = 1;
								$json['req_flow'][$key_x.'_approve'] = 'x';								
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}*/
						if($numb == "hr"){ $idx = 1; }
						else if($numb == "sup"){ $idx = 2; }
						else if($numb == "swap"){ $idx = 3; }
						else if($numb == "employee"){ $idx = $token; }
						foreach ($flow_approval as $keys => $values) {
							if($values['number'] == $idx){
								unset($flow_approval[$keys]);
							}
						}
						$key_x = $numb;
						if($key_x == 'employee'){
							$json['req_flow'][$key_x.'_approve']='x';
							$json['req_flow'][$key_x.'_dates']=$date;
							$json['req_flow'][$key_x.'_times']=$time;
						}else{
							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'x';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}
						
						//return [$flow_approval,$json,1]; // UNIT TEST
						
						$check_ado = \DB::SELECT("select distinct leave_request_id from leave_ado where leave_request_id = $id");
							$ado_include = false;
							if(isset($check_ado[0])){
								$ado_include = true;
							}
							if($ado_include || $i['leave_type']=='Accumulation Day Off'){
								$get_ado = \DB::SELECT("SELECT `date_ado`, `schedule_id`, `name_ado`, `change_date` ,`id` , `employee_id` from leave_ado where leave_request_id=$id");

								foreach ($get_ado as $key => $value) {
									$emps_ado 	= $value->employee_id;
									$dt_ado 	= $value->date_ado;
									$shift_ado 	= $value->schedule_id;
									$ch_ado 	= $value->change_date;
									if(count($flow_approval) == 0){									
										$up1 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$dt_ado',$shift_ado)");
										$up2 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$ch_ado',42)");
									}
								}
							}

						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);

						if(count($flow_approval) == 0){
							//return ["json"=>$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval not null :: single :: leave :: flow sudah lengkap"];
							$schedule = \DB::SELECT("update leave_request set status_id = 2,approver = '$token' where id  =  $id ");
						}else{
							//return ["json"=>$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval not null :: single :: leave :: flow belum lengkap"];
						}
						//return [$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval not null :: single :: leave"];
						//$schedule = \DB::SELECT("update leave_request set status_id = 2,approver =  2 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						

					}else{
						
						if($get_already_approval[0]->approval == null){
						
							//\DB::SELECT("update leave_request set approver =  0, approval = 2 where id = $id");

							
							
							
							/**
							* eg : $json[sup] or $json[sup]
							**/
							
							// foreach($json['req_flow'] as $key  => $value){
								
							// 	$numb = 0;
							// 	if($value == 1){ $numb = 1; $hd = 'hr'; }
							// 	else if($value == 2){ $numb = 2; $hd = 'sup'; }
							// 	else if($value == 3){ $numb = 3; $hd = 'swap'; }								
							// 	if($json['req_flow'][$hd."_approve"] == 'o'){
							// 		$key_x = $hd;
							// 		foreach ($flow_approval as $keys => $values) {
							// 			if($values['number'] == $numb){
							// 				unset($flow_approval[$keys]);
							// 			}
							// 		}
							// 	}
							// }
							// if($numb == "hr"){ $idx = 1; }
							// else if($numb == "sup"){ $idx = 2; }
							// else if($numb == "swap"){ $idx = 3; }
							// else if($numb == "employee"){ $idx = $token; }
							// foreach ($flow_approval as $keys => $values) {
							// 	if($values['number'] == $idx){
							// 		unset($flow_approval[$keys]);
							// 	}
							// }
							// $key_x = $numb;

							// $user_x = $json[$key_x];
							// foreach($user_x as $key => $value){
							// 	if(isset($value[$token])){
							// 		$json[$key_x][$key][$token] =  1;
							// 		$json[$key_x][$key][$key_x.'_stat'] = 1;
							// 		$json[$key_x][$key][$key_x.'_date'] = $date;
							// 		$json[$key_x][$key][$key_x.'_time'] = $time;
							// 		$json[$key_x][$key]['read_stat'] = 1;
							// 		$json['req_flow'][$key_x.'_approve'] = 'x';
							// 	}else{
							// 		$json[$key_x][$key]['read_stat'] = 1;
							// 	}
							// }

							if($numb == "hr"){ $idx = 1; }
							else if($numb == "sup"){ $idx = 2; }
							else if($numb == "swap"){ $idx = 3; }
							else if($numb == "employee"){ $idx = $token; }
							foreach ($flow_approval as $keys => $values) {
								if($values['number'] == $idx){
									unset($flow_approval[$keys]);
								}
							}
							$key_x = $numb;
							if($key_x == 'employee'){
								$json['req_flow'][$key_x.'_approve']='x';
								$json['req_flow'][$key_x.'_dates']=$date;
								$json['req_flow'][$key_x.'_times']=$time;
							}else{
								$user_x = $json[$key_x];

								foreach($user_x as $key => $value){
									if(isset($value[$token])){
										$json[$key_x][$key][$token] =  1;
										$json[$key_x][$key][$key_x.'_stat'] = 1;
										$json[$key_x][$key][$key_x.'_date'] = $date;
										$json[$key_x][$key][$key_x.'_time'] = $time;
										$json['req_flow'][$key_x.'_approve'] = 'x';
										$json[$key_x][$key]['read_stat'] = 1;
									}else{
										$json[$key_x][$key]['read_stat'] = 1;
									}
								}
							}
							
							//return [$flow_approval,$json,2]; // UNIT TEST

							//return [$flow_approval, $numb,$json[$key_x],$token];
							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}

							$check_ado = \DB::SELECT("select distinct leave_request_id from leave_ado where leave_request_id = $id");
							
							$ado_include = false;
							
							if(isset($check_ado[0])){
								$ado_include = true;
							}
							if($ado_include || $i['leave_type']=='Accumulation Day Off'){
								$get_ado = \DB::SELECT("SELECT `date_ado`, `schedule_id`, `name_ado`, `change_date` ,`id` , `employee_id` from leave_ado where leave_request_id=$id");

								foreach ($get_ado as $key => $value) {
									$emps_ado 	= $value->employee_id;
									$dt_ado 	= $value->date_ado;
									$shift_ado 	= $value->schedule_id;
									$ch_ado 	= $value->change_date;
									if(count($flow_approval) == 0){									
										$up1 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$dt_ado',$shift_ado)");
										$up2 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$ch_ado',42)");
									}
								}
							}	
							
							
							/**
							* update json_data;
							**/

							$json_x  =  json_encode($json,1);
							if(count($flow_approval) == 0){
								//return [$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval null :: single :: leave","FLOW LENGKAP"];	
								\DB::SELECT("update leave_request set approver =  '$token', approval = 2, status_id=2 where id = $id"); // 14082018
							}else{
								//return [$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval null :: single :: leave","FLOW BELUM LENGKAP"];	
							}
							//return [$json,"flow_approval"=>$flow_approval,"get_step == 2 && aproval null :: single :: leave"];
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						}
					}
				}else{ // get_data = 1
					
					if($get_already_approval[0]->approval == null){
						//\DB::SELECT("update leave_request set approver =  2, approval = 0 where id = $id");
						/**
						* eg : $json[sup] or $json[sup]
						**/
						
						// foreach($json['req_flow'] as $key  => $value){
								
						// 	// $numb = 0;
						// 	// if($value == 1){ $numb = 1; $hd = 'hr'; }
						// 	// else if($value == 2){ $numb = 2; $hd = 'sup'; }
						// 	// else if($value == 3){ $numb = 3; $hd = 'swap'; }								
						// 	// if($json['req_flow'][$hd."_approve"] == 'o'){
						// 	// 	$key_x = $hd;
						// 	// 	foreach ($flow_approval as $keys => $values) {
						// 	// 		if($values['number'] == $numb){
						// 	// 			unset($flow_approval[$keys]);
						// 	// 		}
						// 	// 	}
						// 	// }
						// 	if($numb == "hr"){ $idx = 1; }
						// 	else if($numb == "sup"){ $idx = 2; }
						// 	else if($numb == "swap"){ $idx = 3; }
						// 	if($json['req_flow'][$hd."_approve"] == 'o'){
						// 		$key_x = $numb;
						// 		foreach ($flow_approval as $keys => $values) {
						// 			if($values['number'] == $idx){
						// 				unset($flow_approval[$keys]);
						// 			}
						// 		}
						// 	}
						// }
						/*if($numb == "hr"){ $idx = 1; }
						else if($numb == "sup"){ $idx = 2; }
						else if($numb == "swap"){ $idx = 3; }
						else if($numb == "employee"){ $idx = $token; }
						foreach ($flow_approval as $keys => $values) {
							if($values['number'] == $idx){
								unset($flow_approval[$keys]);
							}
						}
						$key_x = $numb;
						if($key_x == 'employee'){
							$json['req_flow'][$key_x.'_approve']='x';
							$json['req_flow'][$key_x.'_dates']=$date;
							$json['req_flow'][$key_x.'_times']=$time;
						}else{
							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'x';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}
						}*/
						if($numb == "hr"){ $idx = 1; }
						else if($numb == "sup"){ $idx = 2; }
						else if($numb == "swap"){ $idx = 3; }
						else if($numb == "employee"){ $idx = $token; }
						foreach ($flow_approval as $keys => $values) {
							if($values['number'] == $idx){
								unset($flow_approval[$keys]);
							}
						}
						$key_x = $numb;
						if($key_x == 'employee'){
							$json['req_flow'][$key_x.'_approve']='x';
							$json['req_flow'][$key_x.'_dates']=$date;
							$json['req_flow'][$key_x.'_times']=$time;
						}else{
							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'x';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}
						
						//return [$flow_approval,$json,3]; // UNIT TEST

						//return [$flow_approval, $numb,$json,$token,12];
						$check_ado = \DB::SELECT("select distinct leave_request_id from leave_ado where leave_request_id = $id");
						$ado_include = false;
						if(isset($check_ado[0])){
							$ado_include = true;
						}
						if($ado_include || $i['leave_type']=='Accumulation Day Off'){
							$get_ado = \DB::SELECT("SELECT `date_ado`, `schedule_id`, `name_ado`, `change_date` ,`id` , `employee_id` from leave_ado where leave_request_id=$id");

							foreach ($get_ado as $key => $value) {
								$emps_ado 	= $value->employee_id;
								$dt_ado 	= $value->date_ado;
								$shift_ado 	= $value->schedule_id;
								$ch_ado 	= $value->change_date;
								if(count($flow_approval) == 0){									
									$up1 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$dt_ado',$shift_ado)");
									$up2 = \DB::SELECT("CALL att_schedule_update('$emps_ado','$ch_ado',42)");
								}
							}
						}

						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);

						if(count($flow_approval) == 0){
							//return [$json,"flow_approval"=>$flow_approval,"get_step == 1 && aproval null :: single :: leave","FLOW LENGKAP"];	
							$schedule = \DB::SELECT("update leave_request set status_id = 2,approver =  '$token', approval = 0 where id  =  $id ");
						}else{
							//return [$json,"flow_approval"=>$flow_approval,"get_step == 1 && aproval null :: single :: leave","FLOW BELUM LENGKAP"];	
						}

						//return [$json,"flow_approval"=>$flow_approval,"get_step == 1 && aproval null :: single :: leave"];
						//$schedule = \DB::SELECT("update leave_request set status_id = 2,approver =  2, approval = 0 where id  =  $id ");
						//$schedule = \DB::SELECT("update leave_request set status_id = 2,approver =  '$token', approval = 0 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
					}
				}
			}
			//return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
		}

		$input =   \Input::get('master');
		
			if(isset($input)){
				if(count($flow_approval) == 0){

					if($input['master']  == "leave"){
						$employee_id  =  $input['employee_id'];

						$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
						if(count($set_data) > 0){		
							if(isset($set_data[0]->work_email)){
								$set_email = $set_data[0]->work_email;
							}else if(isset($set_data[0]->personal_email)){
								$set_email = $set_data[0]->personal_email;
							}
							//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

							$namex  =  $input['employee'];
							$request =  $input['leave_type'];
							$email   =  $set_data;
						}else{
							return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
						}
					}else{
						$employee_id  =  $input['employee_id'];

						$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
						if(count($set_data) > 0){
							if(isset($set_data[0]->work_email)){
								$set_email = $set_data[0]->work_email;
							}else if(isset($set_data[0]->personal_email)){
								$set_email = $set_data[0]->personal_email;
							}
							//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

							$namex  =  $input['employee'];
							$request =  $input['type'];
							$email   =  $set_data;
						}else{
							return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
						}
					}

					if($email != null){
						$data = array('name'=>$namex,"request" => $request, 'email' =>  $email);
						\Mail::send('approve.employee', $data, function($message) use ($data){
							$req = $data['request'];
							$message->to($data['email'])->subject("Leekie request reminder $req");
						});
					}
				}

				return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
			}else{
				if(count($flow_approval) == 0){				
					$employee_id  =  $i['employee_id'];

					$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");

					if(count($set_data) > 0){

						if(isset($set_data[0]->work_email)){
							if(strlen($set_data[0]->work_email) > 0){
								$set_email = $set_data[0]->work_email;
							}
						}else if(isset($set_data[0]->personal_email)){
							if(strlen($set_data[0]->personal_email) > 0){
								$set_email = $set_data[0]->personal_email;
							}
						}else{
							$set_email = null;
						}
						//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

						//hide email

						if(isset($i['employee'])){ $namex = $i['employee']; }
						else{ $namex = $i['EmployeeName']; }

						if(isset($i['type'])){ $request = $i['type']; }
						else{ $request = $i['leave_type']; }

						$email   =  $set_email;
						// if(strlen($set_data[0]->work_email) > 0){
						// 	$email   =  $set_data[0]->work_email;
						// }else{
						// 	if(strlen($set_data[0]->personal_email) > 0){
						// 		$email   =  $set_data[0]->personal_email;	
						// 	}else{
						// 		$email = null;
						// 	}
						// }

						if($email != null){
							$data = array('name'=>$namex,"request" => $request, 'email' => $email);
							try {
								\Mail::send('emails.approve', $data, function($message) use($data){
									$req = $data['request'];
									$message->to($data['email'],$data['name'])->subject("Leekie request reminder $req");
									return (array)\Mail::failures();
								});
								
							} catch (Exception $e) {
								return [$e->getMessage()];
							}
							return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
							// \Mail::send('emails.approve', $data, function($message) use ($data){
							// 	return $data;
							// 	$message->to($data['email'])->subject('Leekie request reminder');
							// 	return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
							// });
						}
						return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
					}else{
						return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
					}
				}else{
					return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
				}

			}


	} 

	public function  reject(){

		$token =  explode('-',base64_decode((\Input::get('key'))))[1];  
		$i  = \Input::all()["0"];
		$set_email = null;
		$flow_approval=[];
		// $r = \DB::SELECT("select date_request,id from att_schedule_request");
		// foreach ($r as $key => $value) {
		// 	$dt=$value->date_request;
		// 	$id = $value->id;
		// 	\DB::SELECT("update att_schedule_request set created_at='$dt' where id=$id");
		// }
		// return "OK";
		if(isset($i['id'])){
			if($i['master_type'] == 1 ){
				if(isset($i['sub'])){ // MULTI CRUD
					for ($j=0; $j < count($i['sub']); $j++) { 
						$id  =  $i['sub'][$j];
						$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
						$json  =  json_decode($get_pool[0]->json_data,1);

						$flow =  $json['req_flow'];
						
						/**
						*	get time and date 
						**/

						$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
						$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
						$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
						$date  = $datess->format('Y-m-d');

						/**
						* get user stat 2 or just 1 stat
						**/

						$get_step  =  0;
						$user = '';
						$temp = [];

						/**	
						*	get flow user
						**/
						foreach($flow as $key => $value){
							$temp[] = $value;		
						}

						/**	
						*check using in array target data exist
						**/

						if(in_array(2,$temp)){
							$get_step = 2;
						}else{
							$get_step = 1;
						}

						/**
						*	if 2 step approval
						*	check approval if already approved
						*   if approval already approve
						*	then approve approver
						**/
					
						$get_already_approval  = \DB::SELECT("select * from att_schedule_request where id =  $id ");

						if($get_already_approval[0]->id){				
							$objs['dt_update']=[
									"id" => $id,
									"request"=> $get_already_approval[0]->request_id,
									"employee_id"=>$i['employee_id'],
									"type_id"=> $get_already_approval[0]->type_id,
									"approval"=>0,
									"approver"=>$token,
									"status"=>3,
									"comment"=>"0"
								];
			
							if(isset($i['comment'])){
								$objs['dt_update']["comment"]=$i['comment'];
							}
						}
						
						//return [$get_step,$get_already_approval[0]->approval];
						if($get_step == 2){
							
							if($get_already_approval[0]->approval != null ){
								//$arr_cek =   [1,3,4];
								$arr_cek =   [1,3];
								if(in_array($get_already_approval[0]->approval,$arr_cek)){
									return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
								}
					
								\DB::SELECT("update att_schedule_request set approver =  3 where id = $id");
								
								foreach($json['req_flow'] as $key  => $value){
									if($value == 2){
										$key_x = $key;
									}
								}

								$user_x = $json[$key_x];

								foreach($user_x as $key => $value){
									if(isset($value[$token])){
										$json[$key_x][$key][$token] =  1;
										$json[$key_x][$key][$key_x.'_stat'] = 1;
										$json[$key_x][$key][$key_x.'_date'] = $date;
										$json[$key_x][$key][$key_x.'_time'] = $time;
										$json['req_flow'][$key_x.'_approve'] = 'v';
									}else{
										$json[$key_x][$key]['read_stat'] = 1;
									}
								}

								foreach($json['chat_id']  as  $keyy  => $valuee){
									$json['chat_id'][$keyy] = 'r';
								}

								/**
								* update json_data;
								**/
								$json_x  =  json_encode($json,1);
								//$schedule = \DB::SELECT("update att_schedule_request set approver =  3, approval = 0,status_id = 3 where id  =  $id ");
								
								 $this->update_schedule_list($objs,'reject');
								$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
								

							}else{
								
								if($get_already_approval[0]->approval == null){
									
									/**
									* eg : $json[sup] or $json[sup]
									**/
									
									foreach($json['req_flow'] as $key  => $value){
										if($value == 1){
											$key_x = $key;
										}
									}

									$user_x = $json[$key_x];

									foreach($user_x as $key => $value){
										if(isset($value[$token])){
											$json[$key_x][$key][$token] =  1;
											$json[$key_x][$key][$key_x.'_stat'] = 1;
											$json[$key_x][$key][$key_x.'_date'] = $date;
											$json[$key_x][$key][$key_x.'_time'] = $time;
											$json['req_flow'][$key_x.'_approve'] = 'v';
											$json[$key_x][$key]['read_stat'] = 1;
										}else{
											$json[$key_x][$key]['read_stat'] = 1;
										}
									}

									foreach($json['chat_id']  as  $keyy  => $valuee){
										$json['chat_id'][$keyy] = 'r';
									}
									/**
									* update json_data;
									**/
									
									$json_x  =  json_encode($json,1);
									//\DB::SELECT("update att_schedule_request set approver =  3, approval = 0,status_id = 3 where id = $id");
									
									 $this->update_schedule_list($objs,'reject');
									$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
								}
							}
						}else{
							
							if($get_already_approval[0]->approval == null){
								//\DB::SELECT("update att_schedule_request set approver =  3, approval = 0 where id = $id");
								/**
								* eg : $json[sup] or $json[sup]
								**/
								
								foreach($json['req_flow'] as $key  => $value){
									if($value == 1){
										$key_x = $key;
									}
								}

								$user_x = $json[$key_x];

								foreach($user_x as $key => $value){
									if(isset($value[$token])){
										$json[$key_x][$key][$token] =  1;
										$json[$key_x][$key][$key_x.'_stat'] = 1;
										$json[$key_x][$key][$key_x.'_date'] = $date;
										$json[$key_x][$key][$key_x.'_time'] = $time;
										$json['req_flow'][$key_x.'_approve'] = 'v';
										$json[$key_x][$key]['read_stat'] = 1;
									}else{
										$json[$key_x][$key]['read_stat'] = 1;
									}
								}

								foreach($json['chat_id']  as  $keyy  => $valuee){
									$json['chat_id'][$keyy] = 'r';
								}

								/**
								* update json_data;
								**/
								
								$json_x  =  json_encode($json,1);
								//$schedule = \DB::SELECT("update att_schedule_request set status_id = 3 where id  =  $id "); // old
								//$schedule = \DB::SELECT("update att_schedule_request set approver =  3, approval = 0,status_id = 3 where id = $id");
								
								 $this->update_schedule_list($objs,'reject');
								$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
							}
						}
					}// end for
				}// end if
				else{ // single crud
					$id  =  $i['id'];
					$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
					$json  =  json_decode($get_pool[0]->json_data,1);

					$flow =  $json['req_flow'];
					
					/**
					*	get time and date 
					**/

				$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
				$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
				$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
				$date  = $datess->format('Y-m-d');

					/**
					* get user stat 2 or just 1 stat
					**/

					$get_step  =  0;
					$user = '';
					$temp = [];

					/**	
					*	get flow user
					**/
					foreach($flow as $key => $value){
						$temp[] = $value;		
					}

					/**	
					*check using in array target data exist
					**/

					if(in_array(2,$temp)){
						$get_step = 2;
					}else{
						$get_step = 1;
					}

					/**
					*	if 2 step approval
					*	check approval if already approved
					*   if approval already approve
					*	then approve approver
					**/
				
					$get_already_approval  = \DB::SELECT("select * from att_schedule_request where id =  $id ");

					if($get_already_approval[0]->id){				
						$objs['dt_update']=[
								"id" => $id,
								"request"=> $get_already_approval[0]->request_id,
								"employee_id"=>$i['employee_id'],
								"type_id"=> $get_already_approval[0]->type_id,
								"approval"=>0,
								"approver"=>$token,
								"status"=>3,
								"comment"=>"0"
							];
		
						if(isset($i['comment'])){
							$objs['dt_update']["comment"]=$i['comment'];
						}
					}
					

					if($get_step == 2){
						
						if($get_already_approval[0]->approval != null ){
							//$arr_cek =   [1,3,4];
							$arr_cek =   [1,3];
							if(in_array($get_already_approval[0]->approval,$arr_cek)){
								return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
							}
				
							\DB::SELECT("update att_schedule_request set approver =  3 where id = $id");
							
							foreach($json['req_flow'] as $key  => $value){
								if($value == 2){
									$key_x = $key;
								}
							}

							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'v';
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}

							/**
							* update json_data;
							**/
							$json_x  =  json_encode($json,1);
							//$schedule = \DB::SELECT("update att_schedule_request set approver =  3, approval = 0,status_id = 3 where id  =  $id ");
							
							$this->update_schedule_list($objs,'reject');
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
							

						}else{
							
							if($get_already_approval[0]->approval == null){
								
								/**
								* eg : $json[sup] or $json[sup]
								**/
								
								foreach($json['req_flow'] as $key  => $value){
									if($value == 1){
										$key_x = $key;
									}
								}

								$user_x = $json[$key_x];

								foreach($user_x as $key => $value){
									if(isset($value[$token])){
										$json[$key_x][$key][$token] =  1;
										$json[$key_x][$key][$key_x.'_stat'] = 1;
										$json[$key_x][$key][$key_x.'_date'] = $date;
										$json[$key_x][$key][$key_x.'_time'] = $time;
										$json['req_flow'][$key_x.'_approve'] = 'v';
										$json[$key_x][$key]['read_stat'] = 1;
									}else{
										$json[$key_x][$key]['read_stat'] = 1;
									}
								}

								foreach($json['chat_id']  as  $keyy  => $valuee){
									$json['chat_id'][$keyy] = 'r';
								}
								/**
								* update json_data;
								**/
								
								$json_x  =  json_encode($json,1);
								//\DB::SELECT("update att_schedule_request set approver =  3, approval = 0,status_id = 3 where id = $id");
								
								$this->update_schedule_list($objs,'reject');
								$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
							}
						}
					}else{
						
						if($get_already_approval[0]->approval == null){
							//\DB::SELECT("update att_schedule_request set approver =  3, approval = 0 where id = $id");
							/**
							* eg : $json[sup] or $json[sup]
							**/
							
							foreach($json['req_flow'] as $key  => $value){
								if($value == 1){
									$key_x = $key;
								}
							}

							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'v';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}

							/**
							* update json_data;
							**/
							
							$json_x  =  json_encode($json,1);
							//$schedule = \DB::SELECT("update att_schedule_request set status_id = 3 where id  =  $id "); // old
							//$schedule = \DB::SELECT("update att_schedule_request set approver =  3, approval = 0,status_id = 3 where id = $id");
							
							$this->update_schedule_list($objs,'reject');
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
						}
					}
				}
			}else{
				$id  =  $i['id'];
				$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 2");
				$json  =  json_decode($get_pool[0]->json_data,1);

				$flow =  $json['req_flow'];
				
				$hrxcomp = $supxcomp = $swapcomp = false;
				if(isset($json['hrx_comp'])){
					$hrxcomp = array_search($token,$json['hrx_comp']);
				}
				if(isset($json['supx_comp'])){
					$supxcomp = array_search($token,$json['supx_comp']);
				}
				if(isset($json['swapx_compx'])){
					$swapcomp = array_search($token,$json['swapx_compx']);
				}
				
					if(gettype($hrxcomp) == 'integer'){  $numb = "hr"; }
					else if(gettype($supxcomp) == 'integer'){ $numb = "sup"; }
					else if(gettype($swapcomp) == 'integer'){ $numb = "swap"; }
				


				if(isset($flow['employee'])){
					if($flow['employee'] == $token){
						$numb = "employee";
					}
				}
				//return [gettype($hrxcomp) == 'integer',gettype($hrxcomp)];
				/**
				*	get time and date 
				**/

				$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
				$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
				$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
				$date  = $datess->format('Y-m-d');

				/**
				* get user stat 2 or just 1 stat
				**/

				$get_step  =  0;
				$user = '';
				$temp = [];

				/**	
				*	get flow user
				**/
				
				foreach($flow as $key => $value){
					$temp[] = $value;
					if($key == "hr" || $key == "sup" || $key == "employee"){					
						if($value > 0){

							if($key == 'employee'){
								$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];	
							}else{
								$tmp = ["flow"=>$key,"number"=>$value,"approve"=>false];
							}
							if($key == 'employee_dates' || $key == 'employee_times'){
								$key = 'employee';
							}
							if($flow[$key."_approve"] != "x"){
								//$tmp['approve'] = true;
								array_push($flow_approval,$tmp);
							}
						}
					}
				}
				/**	
				*check using in array target data exist
				**/

				if(in_array(2,$temp)){
					$get_step = 2;
				}else{
					$get_step = 1;
				}

				/**
				*	if 2 step approval
				*	check approval if already approved
				*   if approval already approve
				*	then approve approver
				**/
			
				$get_already_approval  = \DB::SELECT("select * from leave_request where id =  $id ");
				if($get_step == 2){
					if($get_already_approval[0]->approval != null ){
						$arr_cek =   [1,3,4];
					
						if(in_array($get_already_approval[0]->approval,$arr_cek)){
							return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
						}
					    

					    //\DB::SELECT("update leave_request set approver =  3 where id = $id");
					    //\DB::SELECT("update leave_request set approver =  '$token' where id = $id");
						
						if($numb == "hr"){ $idx = 1; }
						else if($numb == "sup"){ $idx = 2; }
						else if($numb == "swap"){ $idx = 3; }
						else if($numb == "employee"){ $idx = $token; }
						foreach ($flow_approval as $keys => $values) {
							if($values['number'] == $idx){
								unset($flow_approval[$keys]);
							}
						}
						$key_x = $numb;
						if($key_x == 'employee'){
							$json['req_flow'][$key_x.'_approve']='v';
							$json['req_flow'][$key_x.'_dates']=$date;
							$json['req_flow'][$key_x.'_times']=$time;
						}else{
							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'v';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}
						}
						//$user_x = $json[$key_x];

						/*if($key_x == 'employee'){
							$json['req_flow'][$key_x.'_approve']='x';
							$json['req_flow'][$key_x.'_dates']=$date;
							$json['req_flow'][$key_x.'_times']=$time;
						}else{
							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'x';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}
						}*/

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}
						/**
						* update json_data;
						**/
						//return [$json,"REJECT::1"];
						$json_x  =  json_encode($json,1);
						\DB::SELECT("update leave_request set approver =  '$token' and status_id = 3 where id = $id");
						//$schedule = \DB::SELECT("update leave_request set status_id = 3 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						

					}else{
					
						if($get_already_approval[0]->approval == null){
					
							//\DB::SELECT("update leave_request set approver =  0, approval = 3, status_id  =  3 where id = $id");
							//\DB::SELECT("update leave_request set approver =  '$token', approval = 3, status_id  =  3 where id = $id");
							/**
							* eg : $json[sup] or $json[sup]
							**/
							if($numb == "hr"){ $idx = 1; }
							else if($numb == "sup"){ $idx = 2; }
							else if($numb == "swap"){ $idx = 3; }
							else if($numb == "employee"){ $idx = $token; }
							foreach ($flow_approval as $keys => $values) {
								if($values['number'] == $idx){
									unset($flow_approval[$keys]);
								}
							}
							$key_x = $numb;
							if($key_x == 'employee'){
								$json['req_flow'][$key_x.'_approve']='v';
								$json['req_flow'][$key_x.'_dates']=$date;
								$json['req_flow'][$key_x.'_times']=$time;
							}else{
								$user_x = $json[$key_x];

								foreach($user_x as $key => $value){
									if(isset($value[$token])){
										$json[$key_x][$key][$token] =  1;
										$json[$key_x][$key][$key_x.'_stat'] = 1;
										$json[$key_x][$key][$key_x.'_date'] = $date;
										$json[$key_x][$key][$key_x.'_time'] = $time;
										$json['req_flow'][$key_x.'_approve'] = 'v';
										$json[$key_x][$key]['read_stat'] = 1;
									}else{
										$json[$key_x][$key]['read_stat'] = 1;
									}
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}
								
							
							/**
							* update json_data;
							**/
							
							//return [$json,"REJECT::2"];
							$json_x  =  json_encode($json,1);
							\DB::SELECT("update leave_request set approver =  '$token', approval = 3, status_id  =  3 where id = $id");
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						}
					}
				}else{
					
					if($get_already_approval[0]->approval == null){
						//\DB::SELECT("update leave_request set approver = 3, approval = 0 where id = $id");
						//\DB::SELECT("update leave_request set approver = '$token', approval = 0 where id = $id");
						/**
						* eg : $json[sup] or $json[sup]
						**/
						
						if($numb == "hr"){ $idx = 1; }
						else if($numb == "sup"){ $idx = 2; }
						else if($numb == "swap"){ $idx = 3; }
						else if($numb == "employee"){ $idx = $token; }
						foreach ($flow_approval as $keys => $values) {
							if($values['number'] == $idx){
								unset($flow_approval[$keys]);
							}
						}
						$key_x = $numb;
						if($key_x == 'employee'){
							$json['req_flow'][$key_x.'_approve']='v';
							$json['req_flow'][$key_x.'_dates']=$date;
							$json['req_flow'][$key_x.'_times']=$time;
						}else{
							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'v';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}
						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);
						//return [$json,"REJECT::3"];
						\DB::SELECT("update leave_request set approver = '$token', approval = 0 and status_id=3 where id = $id");
						//$schedule = \DB::SELECT("update leave_request set status_id = 3 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
					}
				}
			}
			//return response()->json(['header' => ['message' =>  'success reject data',  'status' => 200] ,'data' => []], 200);
		}
		//return (array)$i;
		$input = \Input::get('master');
		if(isset($input)){

			if($input  == "leave"){
				$employee_id  =  $input['employee_id'];

				$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
				if(count($set_data) > 0){				
					if(isset($set_data[0]->work_email)){
						$set_email = $set_data[0]->work_email;
					}else if(isset($set_data[0]->personal_email)){
						$set_email = $set_data[0]->personal_email;
					}
					//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

					$namex  =  $input['employee'];
					$request =  $input['leave_type'];
					$email   =  $set_data;
				}else{
					return response()->json(['header' => ['message' =>  'success reject data',  'status' => 200] ,'data' => []], 200);
				}
			}else{
				$employee_id  =  $i['employee_id'];

				$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
				if(count($set_data) > 0){
					if(isset($set_data[0]->work_email)){
						$set_email = $set_data[0]->work_email;
					}else if(isset($set_data[0]->personal_email)){
						$set_email = $set_data[0]->personal_email;
					}
					//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));
				
					$namex  =  $i['employee'];
					$request =  $i['type'];
					$email   =  $set_email;
					if(strlen($set_data[0]->work_mail) > 0){
						$email   =  $set_data[0]->work_mail;
					}else{
						if(strlen($set_data[0]->personal_mail) > 0){
							$email   =  $set_data[0]->personal_mail;	
						}else{
							$email = null;
						}
					}
				}else{
					return response()->json(['header' => ['message' =>  'success reject data',  'status' => 200] ,'data' => []], 200);
				}
			}

			if($email != null){
				$data = array('name'=>$namex,"request" => $request, 'email' =>  $email);
				\Mail::send('emails.reject', $data, function($message) use($data){
					$req = $data['request'];
					$message->to($data['email'],$data['name'])->subject("Leekie request reminder $req");
					return (array)\Mail::failures();
				});
				// \Mail::send('reject.employee', $data, function($message) use ($data){
				// 	$message->to($data['email'])->subject('Leekie request reminder');
				// });
			}
			return response()->json(['header' => ['message' =>  'success reject data',  'status' => 200] ,'data' => []], 200);

		}else{
				$employee_id  =  $i['employee_id'];

				$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
				if(count($set_data) > 0){
					if(isset($set_data[0]->work_email)){
						$set_email = $set_data[0]->work_email;
					}else if(isset($set_data[0]->personal_email)){
						$set_email = $set_data[0]->personal_email;
					}
					//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

					if(isset($i['employee'])){ $namex = $i['employee']; }
					else{ $namex = $i['EmployeeName']; }

					if(isset($i['type'])){ $request = $i['type']; }
					else{ $request = $i['leave_type']; }
					
					$email   =  $set_email;
					if(strlen($set_data[0]->work_email) > 0){
						$email   =  $set_data[0]->work_email;
					}else{
						if(strlen($set_data[0]->personal_email) > 0){
							$email   =  $set_data[0]->personal_email;	
						}else{
							$email = null;
						}
					}
				}else{
					return response()->json(['header' => ['message' =>  'success reject data',  'status' => 200] ,'data' => []], 200);
				}
		}

			if($email != null){
				$data = array('name'=>$namex,"request" => $request, 'email' =>  $email);
				// \Mail::send('emails.reject', $data, function($message) use ($data){
				// 	$message->to($data['email'])->subject('Leekie request reminder');
				// });
				\Mail::send('emails.reject', $data, function($message) use($data){
					$req = $data['request'];
					$message->to($data['email'],$data['name'])->subject("Leekie request reminder $req");
					return (array)\Mail::failures();
				});
			}
			return response()->json(['header' => ['message' =>  'success reject data2',  'status' => 200] ,'data' => []], 200);
	}	

	public function cancle(){

		$token =  explode('-',base64_decode((\Input::get('key'))))[1];  
		$set_email = null;
		try{
			if(isset(\Input::all()["0"])){
				$i  = \Input::all()["0"];
			}else{
				$i  = \Input::all();	
			}
		}catch(\Exeception $e){
			$i  = \Input::all();
		}
		if(isset($i['id'])){
			
			if($i['master_type'] == 1 ){
				if(isset($i['sub'])){ // MULTI CRUD
					for ($j=0; $j < count($i['sub']); $j++) { 
						$id  =  $i['sub'][$j];
						$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
						$json  =  json_decode($get_pool[0]->json_data,1);

						$flow =  $json['req_flow'];
						
						/**
						*	get time and date 
						**/

						$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
						$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
						$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
						$date  = $datess->format('Y-m-d');

						/**
						* get user stat 2 or just 1 stat
						**/

						$get_step  =  0;
						$user = '';
						$temp = [];

						/**	
						*	get flow user
						**/
						foreach($flow as $key => $value){
							$temp[] = $value;		
						}

						/**	
						*check using in array target data exist
						**/

						if(in_array(2,$temp)){
							$get_step = 2;
						}else{
							$get_step = 1;
						}

						/**
						*	if 2 step approval
						*	check approval if already approved
						*   if approval already approve
						*	then approve approver
						**/
					

						$get_already_approval  = \DB::SELECT("select * from att_schedule_request where id =  $id ");
						//return $get_already_approval;

						if($get_already_approval[0]->id){				
							$objs['dt_update']=[
									"id" => $id,
									"request"=> $get_already_approval[0]->request_id,
									"employee_id"=>$i['employee_id'],
									"type_id"=> $get_already_approval[0]->type_id,
									"approval"=>0,
									"approver"=>$token,
									"status"=>4,
									"comment"=>"0"
								];
			
							if(isset($i['comment'])){
								$objs['dt_update']["comment"]=$i['comment'];
							}
						}
						if($get_step == 2){
							
							if($get_already_approval[0]->approval != null ){
								$arr_cek =   [1,3];
								//$arr_cek =   [1,3,4];
								if(in_array($get_already_approval[0]->approval,$arr_cek)){
									return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
								}
					
								//\DB::SELECT("update att_schedule_request set approver =  '$token' where id = $id");
								
								foreach($json['req_flow'] as $key  => $value){
									if($value == 2){
										$key_x = $key;
									}
								}

								$user_x = $json[$key_x];

								foreach($user_x as $key => $value){
									if(isset($value[$token])){
										$json[$key_x][$key][$token] =  1;
										$json[$key_x][$key][$key_x.'_stat'] = 1;
										$json[$key_x][$key][$key_x.'_date'] = $date;
										$json[$key_x][$key][$key_x.'_time'] = $time;
										$json['req_flow'][$key_x.'_approve'] = 'c';
									}else{
										$json[$key_x][$key]['read_stat'] = 1;
									}
								}

								foreach($json['chat_id']  as  $keyy  => $valuee){
									$json['chat_id'][$keyy] = 'r';
								}

								/**
								* update json_data;
								**/
								$json_x  =  json_encode($json,1);
								//$schedule = \DB::SELECT("update att_schedule_request set status_id = 4 and approver = '$token' where id  =  $id ");
								$this->update_schedule_list($objs,'cancel');
								$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
								

							}else{
								
								if($get_already_approval[0]->approval == null){
									
									//\DB::SELECT("update att_schedule_request set approver =  '$token', approval = 4 where id = $id");
									/**
									* eg : $json[sup] or $json[sup]
									**/
									
									foreach($json['req_flow'] as $key  => $value){
										if($value == 1){
											$key_x = $key;
										}
									}

									$user_x = $json[$key_x];

									foreach($user_x as $key => $value){
										if(isset($value[$token])){
											$json[$key_x][$key][$token] =  1;
											$json[$key_x][$key][$key_x.'_stat'] = 1;
											$json[$key_x][$key][$key_x.'_date'] = $date;
											$json[$key_x][$key][$key_x.'_time'] = $time;
											$json['req_flow'][$key_x.'_approve'] = 'c';
											$json[$key_x][$key]['read_stat'] = 1;
										}else{
											$json[$key_x][$key]['read_stat'] = 1;
										}
									}

									foreach($json['chat_id']  as  $keyy  => $valuee){
										$json['chat_id'][$keyy] = 'r';
									}
									/**
									* update json_data;
									**/
									
									$json_x  =  json_encode($json,1);
									 $this->update_schedule_list($objs,'cancel');
									//return "DISINI";
									$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
								}
							}
						}else{
							
							if($get_already_approval[0]->approval == null){
								//\DB::SELECT("update att_schedule_request set approver =  '$token', approval = 0 where id = $id");
								/**
								* eg : $json[sup] or $json[sup]
								**/
								
								foreach($json['req_flow'] as $key  => $value){
									if($value == 1){
										$key_x = $key;
									}
								}

								$user_x = $json[$key_x];

								foreach($user_x as $key => $value){
									if(isset($value[$token])){
										$json[$key_x][$key][$token] =  1;
										$json[$key_x][$key][$key_x.'_stat'] = 1;
										$json[$key_x][$key][$key_x.'_date'] = $date;
										$json[$key_x][$key][$key_x.'_time'] = $time;
										$json['req_flow'][$key_x.'_approve'] = 'c';
										$json[$key_x][$key]['read_stat'] = 1;
									}else{
										$json[$key_x][$key]['read_stat'] = 1;
									}
								}

								foreach($json['chat_id']  as  $keyy  => $valuee){
									$json['chat_id'][$keyy] = 'r';
								}

								/**
								* update json_data;
								**/
								
								$json_x  =  json_encode($json,1);
								//$schedule = \DB::SELECT("update att_schedule_request set status_id = 4 where id  =  $id ");
								$this->update_schedule_list($objs,'cancel');
								$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
							}
						}// end for
					}// end if
				}else{
					$id  =  $i['id'];
					$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
					$json  =  json_decode($get_pool[0]->json_data,1);

					$flow =  $json['req_flow'];
					
					/**
					*	get time and date 
					**/

					$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
					$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
					$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
					$date  = $datess->format('Y-m-d');

					/**
					* get user stat 2 or just 1 stat
					**/

					$get_step  =  0;
					$user = '';
					$temp = [];

					/**	
					*	get flow user
					**/
					foreach($flow as $key => $value){
						$temp[] = $value;		
					}

					/**	
					*check using in array target data exist
					**/

					if(in_array(2,$temp)){
						$get_step = 2;
					}else{
						$get_step = 1;
					}

					/**
					*	if 2 step approval
					*	check approval if already approved
					*   if approval already approve
					*	then approve approver
					**/
				

					$get_already_approval  = \DB::SELECT("select * from att_schedule_request where id =  $id ");
					//return $get_already_approval;
					if($get_step == 2){
						
						if($get_already_approval[0]->approval != null ){
							$arr_cek =   [1,3];
							//$arr_cek =   [1,3,4];
							if(in_array($get_already_approval[0]->approval,$arr_cek)){
								return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
							}
				
							\DB::SELECT("update att_schedule_request set approver =  '$token' where id = $id");
							
							foreach($json['req_flow'] as $key  => $value){
								if($value == 2){
									$key_x = $key;
								}
							}

							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'c';
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}

							/**
							* update json_data;
							**/
							$json_x  =  json_encode($json,1);
							$schedule = \DB::SELECT("update att_schedule_request set status_id = 4 and approver = '$token' where id  =  $id ");
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
							

						}else{
							
							if($get_already_approval[0]->approval == null){
								
								\DB::SELECT("update att_schedule_request set approver =  '$token', approval = 4 where id = $id");
								/**
								* eg : $json[sup] or $json[sup]
								**/
								
								foreach($json['req_flow'] as $key  => $value){
									if($value == 1){
										$key_x = $key;
									}
								}

								$user_x = $json[$key_x];

								foreach($user_x as $key => $value){
									if(isset($value[$token])){
										$json[$key_x][$key][$token] =  1;
										$json[$key_x][$key][$key_x.'_stat'] = 1;
										$json[$key_x][$key][$key_x.'_date'] = $date;
										$json[$key_x][$key][$key_x.'_time'] = $time;
										$json['req_flow'][$key_x.'_approve'] = 'c';
										$json[$key_x][$key]['read_stat'] = 1;
									}else{
										$json[$key_x][$key]['read_stat'] = 1;
									}
								}

								foreach($json['chat_id']  as  $keyy  => $valuee){
									$json['chat_id'][$keyy] = 'r';
								}
								/**
								* update json_data;
								**/
								
								$json_x  =  json_encode($json,1);
								$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
							}
						}
					}else{
						
						if($get_already_approval[0]->approval == null){
							\DB::SELECT("update att_schedule_request set approver =  '$token', approval = 0 where id = $id");
							/**
							* eg : $json[sup] or $json[sup]
							**/
							
							foreach($json['req_flow'] as $key  => $value){
								if($value == 1){
									$key_x = $key;
								}
							}

							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'c';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}

							/**
							* update json_data;
							**/
							
							$json_x  =  json_encode($json,1);
							$schedule = \DB::SELECT("update att_schedule_request set status_id = 4 where id  =  $id ");
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
						}
					}
				}// end if single

			}else{ // LEAVE
				
				$id  =  $i['id'];
				$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 2");
				$json  =  json_decode($get_pool[0]->json_data,1);

				$flow =  $json['req_flow'];
				
				/**
				*	get time and date 
				**/
		
				$datess = new DateTime(date('Y-m-d h:i:s'), new DateTimeZone('UTC'));
				$datess->setTimeZone(new DateTimeZone('Asia/Manila'));
				$time  =  strtoupper($datess->format('Y-m-d H:i:s a'));
				$date  = $datess->format('Y-m-d');

				/**
				* get user stat 2 or just 1 stat
				**/

				$get_step  =  0;
				$user = '';
				$temp = [];

				/**	
				*	get flow user
				**/
				foreach($flow as $key => $value){
					$temp[] = $value;		
				}

				/**	
				*check using in array target data exist
				**/

				if(in_array(2,$temp)){
					$get_step = 2;
				}else{
					$get_step = 1;
				}

				/**
				*	if 2 step approval
				*	check approval if already approved
				*   if approval already approve
				*	then approve approver
				**/
			
				$get_already_approval  = \DB::SELECT("select * from leave_request where id =  $id ");
				if($get_step == 2){
					if($get_already_approval[0]->approval != null ){
						$arr_cek =   [1,3,4];
					
						if(in_array($get_already_approval[0]->approval,$arr_cek)){
							return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
						}
					    

					    \DB::SELECT("update leave_request set approver = '$token' where id = $id");
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 2){
								$key_x = $key;
							}
						}

						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json['req_flow'][$key_x.'_approve'] = 'c';
								$json[$key_x][$key]['read_stat'] = 1;
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}
						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update leave_request set status_id = 4 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						

					}else{
					
						if($get_already_approval[0]->approval == null){
					
							\DB::SELECT("update leave_request set approver =  '$token', approval = 4, status_id  =  4 where id = $id");
							/**
							* eg : $json[sup] or $json[sup]
							**/
							
							foreach($json['req_flow'] as $key  => $value){
								if($value == 1){
									$key_x = $key;
								}
							}

							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'c';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}
							
							/**
							* update json_data;
							**/
							
							
							$json_x  =  json_encode($json,1);
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						}
					}
				}else{
					
					if($get_already_approval[0]->approval == null){
						\DB::SELECT("update leave_request set approver = '$token', approval = 0 where id = $id");
						/**
						* eg : $json[sup] or $json[sup]
						**/
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 1){
								$key_x = $key;
							}
						}
						
						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json['req_flow'][$key_x.'_approve'] = 'c';
								$json[$key_x][$key]['read_stat'] = 1;
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update leave_request set status_id = 4 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
					}
				}
			}
			//return response()->json(['header' => ['message' =>  'success canceling data',  'status' => 200] ,'data' => []], 200);
		}

		$input = \Input::get('master');
		if(isset($input)){		
			if($input == "leave"){
				$employee_id  =  $input['employee_id'];

				$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
				if(count($set_data) > 0){				
					if(isset($set_data[0]->work_email)){
						$set_email = $set_data[0]->work_email;
					}else if(isset($set_data[0]->personal_email)){
						$set_email = $set_data[0]->personal_email;
					}
					//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

					$namex  =  $input['employee'];
					$request =  $input['leave_type'];
					$email   =  $set_data;
				}else{
					return response()->json(['header' => ['message' =>  'success canceling data',  'status' => 200] ,'data' => []], 200);
				}
			}else{
				$employee_id  =  $input['employee_id'];

				$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
				if(count($set_data) > 0){				
					if(isset($set_data[0]->work_email)){
						$set_email = $set_data[0]->work_email;
					}else if(isset($set_data[0]->personal_email)){
						$set_email = $set_data[0]->personal_email;
					}
					//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

					$namex  =  $input['employee'];
					$request =  $input['type'];
					$email   =  $set_data;
				}else{
					return response()->json(['header' => ['message' =>  'success canceling data',  'status' => 200] ,'data' => []], 200);
				}
			}

			if($email != null){
				$data = array('name'=>$namex,"request" => $request, 'email' =>  $email);
				// \Mail::send('cancle.employee', $data, function($message) use ($data){
				// 	$message->to($data['email'])->subject('Leekie request reminder');
				// });
				\Mail::send('emails.cancel', $data, function($message) use($data){
					$req = $data['request'];
					$message->to($data['email'],$data['name'])->subject("Leekie request reminder $req");
					return (array)\Mail::failures();
				});
			}

			return response()->json(['header' => ['message' =>  'success canceling data',  'status' => 200] ,'data' => []], 200);
		}else{
				$employee_id  =  $i['employee_id'];

				$set_data  =  \DB::SELECT("select work_email,personal_email from  emp where  employee_id = '$employee_id' ");
				if(count($set_data) > 0){
					if(isset($set_data[0]->work_email)){
						$set_email = $set_data[0]->work_email;
					}else if(isset($set_data[0]->personal_email)){
						$set_email = $set_data[0]->personal_email;
					}
					//$set_email   =  ($set_data[0]->work_email  == null ?  $set_data[0]->work_mail :  ($set_data[0]->personal_email !=  null ? $set_data[0]->personal_email :  null));

					if(isset($i['employee'])){ $namex = $i['employee']; }
					else{ $namex = $i['EmployeeName']; }

					if(isset($i['type'])){ $request = $i['type']; }
					else{ $request = $i['leave_type']; }

					$email   =  $set_email;
					if(strlen($set_data[0]->work_email) > 0){
						$email   =  $set_data[0]->work_email;
					}else{
						if(strlen($set_data[0]->personal_email) > 0){
							$email   =  $set_data[0]->personal_email;	
						}else{
							$email = null;
						}
					}
					if($email != null){
						$data = array('name'=>$namex,"request" => $request, 'email' =>  $email);
						\Mail::send('emails.cancel', $data, function($message) use($data){
							$req = $data['request'];
							$message->to($data['email'],$data['name'])->subject("Leekie request reminder $req");
							return (array)\Mail::failures();
						});
						// \Mail::send('emails.reject', $data, function($message) use ($data){
						// 	$message->to($data['email'])->subject('Leekie request reminder');
						// });
					}
				}else{
					return response()->json(['header' => ['message' =>  'success cancel data',  'status' => 200] ,'data' => []], 200);		
				}
			return response()->json(['header' => ['message' =>  'success cancel data',  'status' => 200] ,'data' => []], 200);
		}

	}

}