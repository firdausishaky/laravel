<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Privilege;
use Mail;
use Larasite\Library\FuncAccess;
use Exeception;

class notificationV3 extends Controller {

	/**
	 * @param  [type]
	 * @return [type]
	 */
	
	public function getUser($key = null){
		if($key == null){
			$key = \Input::get('key');
		}
		$keys= base64_decode($key);
		$test = explode('-',$keys);
		return $data = $test[1];
	}


	public function leave($id){
	     $db = \DB::SELECT("select  distinct
							concat(t1.first_name,' ',t1.middle_name,' ',t1.last_name)as employee, 
							t2.employee_id,  
							t2.from_ , 
							t2.to_ , 
							t4.status,
							t4.status_id,
							t2.taken_day, 
							t2.balance_day, 
							t2.taken_day_approval, 
							t2.taken_day_approver, 
							t2.created_at, 
							t2.approval,
							t2.approver,
							t3.leave_type,
							t2.id,
							if(t20.filename = null,null,t20.filename) as filename,
							'leave' as master,
							'leave' as master_type
							from emp t1 
							left join emp_picture t20 on t20.employee_id =  t1.employee_id
							left join leave_request t2  on  t2.employee_id  = t1.employee_id 
							left join leave_type t3 on t3.id  = t2.leave_type
							left join att_status t4 on  t4.status_id =  t2.status_id
							where t2.id  = $id ");

		if($db ==  null){
			$arr  = ['data' => [], 'message' => 'empty data'];
		}else{
			$arr  = ['data' => $db, 'message' => 'success'];
		}

		return $arr;
	}

	public function  schedule($id){
		
		$db  =  \DB::SELECT(
				"select distinct t1.id,t1.date_request,t1.availment_date,t6.employee_id as empreq,t6.swap_with as empswap,
				 concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name)as employee,t1.employee_id,t3.type_id,
				 t3.type,t1.status_id,t4.status,t1.request_id,date_format(t1.update_at,'%Y-%m-%d') as date_, 
				 date_format(t1.update_at,'%H:%s') as time_,
				 /*-----------------for time in out request------------t7*/
				 t7.req_in,t7.req_out,t7.req_in_out_id, 
				 concat(t28.shift_code,' : ',t28._from,' - ',t28._to) as  time_in_out_schedule,		 			 
			 	 /*  ----------- for training and ob------------------ t8*/
			 	 t8.id as id_training,
			 	 t8.start_ as start_training,
			 	 t8.end as end_training,
			 	 t8.created_at as date_request_training,
			 	 /*--------------for overtime ----------------------- t11*/
			 	 t11.id as overtime_id,
			 	 t11.date_str as date_overtime,
			 	 (select shift_code from attendance_work_shifts where t11.id_shift = shift_id) as shift_code_overtime,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where t11.id_shift = shift_id) as time_shift_code_overtime,
				 t11.date_str,	
			 	 t11.overtime_str as overtime_start,
			 	 t11.overtime_end as overtime_end,
			 	 t11.total_overtime as overtime_total,
			 	 t11.created_at as date_request_overtime,
			 	 /*--------------for undertime  --------------------- t15*/
			 	 t15.id as id_undertime,
			 	 t15.date as date_undertime,
			 	 (select shift_code from attendance_work_shifts where t15.shift_id = shift_id) as shift_code_undertime,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where t15.shift_id = shift_id) as time_shift_code_undertime,
			 	 t15.work_end_time as end_time_request_undertime,
			 	 t15.short as date_diff_end_time_and_shift_time,
			 	 t15.created_at as date_request_undertime,
			 	 /*-----------------for change shift----------------- t12 */
			 	 t12.id as id_change_shift,
			 	 date_format(t12.date,'%y-%m-%d') as date_request_change_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t12.old_shift ) as old_shift_change_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t12.new_shift ) as new_shift_change_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t12.old_shift ) as time_old_shift_change_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t12.new_shift ) as time_new_shift_change_shift,
			 	 t12.date as date_request_change_shift,
			 	 /*-----------------for swap shift--------------------- t13*/
			 	 t13.swap_id as  id_swap_shift,
			 	 t13.date  as date_swap_shift,
			 	 (select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) from emp where employee_id  = t13.swap_with) as employee_swap_for_swap_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t13.old_shift_id ) as old_shift_swap_shift,
			 	 (select shift_code from attendance_work_shifts where shift_id = t13.new_shift_id ) as new_shift_swap_shift,
			 	  (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t13.old_shift_id ) as time_old_shift_swap_shift,
			 	 (select concat(_from,' - ',_to) from attendance_work_shifts where shift_id = t13.new_shift_id ) as time_new_shift_swap_shift,
			 	 t13.date as date_request_swap_shift,
			 	 /*-----------------for late / for early_out------------------------- t14*/
			 	 t14.date  as dateLate,
			 	 t14.late_id as lateId,
			 	 t14.late as lateTime,
				 time_format(t21.time_in,'%H:%i') as time_earlyOut,
				 time_format(t21.time_out,'%H:%i') as time_out,
				 concat(t23.shift_code,' : ',time_format(t23._from, '%H:%i'),' - ',time_format(t23._to,'%H:%i')) as schedule_earlyOut,
			 	 t14.early_out as earlyOut,
			 	 t14.created_at as datecreated_att,
			 	 (select id from pool_request where id_req = t1.id) as id_pool,
				 t1.approval,t1.approver,
				 t20.filename,
				 t2.gender
			 	 from att_schedule_request t1
				 left join emp t2 on t2.employee_id=t1.employee_id
				 left join emp_picture t20 on t20.employee_id  =  t2.employee_id
				 left join att_late t24 on  t24.employee_id =   t2.employee_id and  t1.request_id =  t24.late_id   
				 left join biometrics_device t21 on t21.employee_id =  t2.employee_id and  t24.date = t21.date   
				 left join att_schedule   t22 on t22.date =  t24.date and  t22.employee_id  = t24.employee_id    
				 left join attendance_work_shifts  t23 on t23.shift_id =   t22.shift_id
				 left join att_type t3 on t3.type_id=t1.type_id 
				 left join att_late t14 on t14.type_id =  t1.type_id and t14.employee_id =  t1.employee_id and t14.late_id =  t1.request_id
				 left join att_undertime t15 on t15.type_id =  t1.type_id and t15.employee_id =  t1.employee_id
				 left join att_overtime t11 on t11.type_id=t1.type_id and t11.employee_id =  t1.employee_id
				 left join att_change_shift t12 on t12.type_id=t1.type_id and t12.employee_id =  t1.employee_id
				 left join att_swap_shift t13 on t13.type_id=t1.type_id and t13.employee_id =  t1.employee_id and t13.swap_id = t1.request_id
				 left join att_training t8 on t8.type_id=t1.type_id and t8.employee_id =  t1.employee_id
				 left join att_time_in_out_req t7 on t7.type_id = t1.type_id and t7.employee_id =  t1.employee_id and t1.request_id = t7.req_in_out_id
				 left join att_schedule_request t26  on t7.req_in_out_id = t26.request_id and  t26.type_id =  9
				 left join att_schedule t27 on t26.availment_date = t27.date
			         left join attendance_work_shifts t28 on  t27.shift_id  =  t28.shift_id 	
				 left join att_status t4 on  t4.status_id=t1.status_id
				 left join att_swap_shift t6 on t6.swap_id=t1.request_id and t6.employee_id =  t1.employee_id
				 left join command_center t17 on t17.request_id = t1.id 
				 where t1.id = $id order by t1.id asc limit 1");
		
		
		if($db[0]->type  == "Early Out"){
		
		    if($db[0]->time_earlyOut != '-' and  $db[0]->time_out !=  '-' || $db[0]->time_earlyOut != null and  $db[0]->time_out	 !=  null){
			
			$schedule =   explode(' ',$db[0]->schedule_earlyOut);
			$fromx = $schedule[2].':00';
			$tox  =  $schedule[4].':00';	
			$timeIn  = $db[0]->time_earlyOut.':00';		
			$timeOut  =  $db[0]->time_out.':00';	
			
			
			//return  $arr=[$timeIn,$fromx];
			if(strtotime($timeIn) < strtotime($fromx)){
				$workhours  =  date('H:i',strtotime($timeOut) - strtotime($fromx)); 
			}else{
				$workhours  =  date('H:i',strtotime($timeOut) - strtotime($timeIn));
			}
				$workhours .=  ':00';

			if(strtotime($workhours) > strtotime(date('H:i',(strtotime($tox) -  strtotime($fromx)))) )
			    {  $Eout =  0; }
			else
			    { 
				
				//$time1 =  substr(($query[$key]->schedule),6,5).':00';
				//return $arr = [$tox,$timeOut];
				if(strtotime($tox) < strtotime($timeOut)){
				    $Eout =  0;	
				}else{
				    $Eout = date('H:i', strtotime($tox) - strtotime($timeOut));
			       }

			}	

		      if($Eout!=  null){
			$explode_eout = explode(':',$Eout);
			$eout_h = intval($explode_eout[0])  * 60; 
		      	$eout_m = $explode_eout[1];

			$Eout = $eout_h + $eout_m;
		      }   
		      $db[0]->earlyOut_for  =  $Eout;
		   }
		}		
		if($db ==  null){
			$arr  = ['data' => [], 'message' => 'empty data'];
		}else{
			$arr  = ['data' => $db, 'message' => 'success'];
		}
		return $arr;
	}


	/**
	 * function insert notification join from schedule and  leave
	 * @param  [ids] => [int]  parameter darri att_scheduleR_equest , atau leave_request
 	 * @param  [type] => [int] parameter type 
	 * @param  [employee_id] => employee_id
	 * @param  [local] => local_it  from employee_id 
	 * @param  [user] => find out she/her  
	 * @param  [from_type] => attendance / schedule '
	 *
	 *
	 *	local_it 
	 *	if : 1   lexpat  
	 *	if  :2   local /local_it
	 * 
	 * 
	 *  attendance
	 *  1 / training
	 *  2 / ob
	 *  3 / overtime
	 *  4 /undertime
	 *  5 / chnageshift
	 *  6 / swapshift
	 *  7 / late
	 *  8 /earlyout
	 *  9 / time in/out
	 *
	 *  leave
	 *  1 / birth leave (bl)
	 *  2 / vacation leave (vl)
	 *  3 / enhannce leave (E(VL))
	 *  4 / sick leave (sl)
	 *  5 / maternty leave (ML)
	 *  6 / paternity
	 *  7 / Bereavement leave (BE)
	 *  8 / Marriage Leave (LE)
	 *  9 / offday oncall (OC)
	 *  10 /  acumulation day offf  (ADO)
	 *  11 / ememrgeny leave  (EL)
	 *  12  / suspension leave  (SL) 
	 */
	



	
	public function read_notif(){
		/**
		 * check token exist
		 */
		$res = [];
		$token  = \Input::get('key');
		if($token ==  'undefined'){
			return response(['header' => ['message' => 'token undefined', 'status' => 301],'data' => []],301);
		}

		$token =  explode('-',base64_decode((\Input::get('key'))))[1];

		$view_stat  =  \DB::SELECT("select * from view_nonactive_login where employee_id =  '$token' and role_name like '%user%' and  lower(role_name) != 'superuser'   ");
		if($view_stat != null){
			$data  = \DB::SELECT("select * from pool_request where employee_id = $token");
		}else{			
			$data  = \DB::SELECT("select * from pool_request ");
		}
		$arr  = [];
				
		//$value->action  = 'done' || 'need_action' || 'can_cancel'
		$check_arr = [];

		foreach ($data as $key => $valuex) {
			$index =  $key;
			$result =   json_decode($valuex->json_data,1);
				

			$ID  =  $valuex->id_req;
			$test   = [];
			$test[] = $ID;
			
			/**
			 * [res_temp] [arr] ['data' => [], 'message' => 'success']
			 */

			if($result['master'] ==  'schedule'){
				$res_temp = $this->schedule($ID);
			}else{
				$res_temp = $this->leave($ID);
			}
			

			$check_arr[] = $ID;
			// get primary data
			$js  = json_decode($valuex->json_data,1);
			
			$master = $js['master'];
			$flow =  $js['req_flow'];				
			
			//check flow   role	
			$flow_hr = $flow['hr'];
			$flow_sup = $flow['sup'];
			$flow_swap = $flow['swap'];			
			
			$arr_flow = [ $flow['hr'] => 'hr', $flow['sup'] => 'sup', $flow['swap'] => 'swap']; 
			
			$i  =  1;


			foreach($res_temp['data'] as $key => $value){
			    $ind  =  $key;
			    $val  = $value;
			    $value->master = $master;    
				        
			 	//if(array_key_exists(2,$arr_flow)){
			 //    	$i += 1;
			 //    	$get_stat =  $arr_flow[2];
				
				// 	if($flow[$get_stat.'_approve'] == 'x'){
				// 		$value->action = 'done';				
				// 	}else{
				// 		$get_stat =  $arr_flow[1];
				    	
				// 		if($flow[$get_stat.'_approve'] == 'x'){
				// 			$value->action = 'done';				
				// 		}else{
				// 			$get_list = $js[$get_stat.'x_comp'];
				// 			if(in_array($token,$get_list)){  	
								
				// 		  		$val->action = 'need_action';
				// 			}else{
				// 				$val->action = 'can_cancle';
				// 			}
				// 		}
				// 	}
	
			 	//}else if(array_key_exists(1,$arr_flow)){
			 //    	$i += 1;
			 //    	//$get_stat => sup / hr /swap
				// $get_stat =  $arr_flow[1];
			    	
				// if($flow[$get_stat.'_approve'] == 'x'){
				// 	$value->action = 'done';
				// }else{
				// 	$get_list = $js[$get_stat.'x_comp'];
				// 	if(in_array($token,$get_list)){
				// 	  	$value->action = 'need_action'; 
				// 	}else{
				// 		$value->action = 'can_cancle';
				// 	}
				// }

				if($value->approver != 0  )
				{
					$value->action = 'done';
				}else{
					if($value->approval  ==  null and  $value->approver ==  null){
						if(array_key_exists(1,$arr_flow)){
							$get_stat =  $arr_flow[1];
							if(isset($flow[$get_stat.'_approve'])){
								$get_list = $js[$get_stat.'x_comp'];	
								if(in_array($token,$get_list)){  	
				 		  				$value->action = 'need_action';
								}			
							}
						}
					}elseif($value->approval  != null and $value->approver  == 0){
						if(array_key_exists(2,$arr_flow)){
							$get_stat =  $arr_flow[2];
							if(isset($flow[$get_stat.'_approve'])){
								$get_list = $js[$get_stat.'x_comp'];	
								if(in_array($token,$get_list)){  	
				 		  				$value->action = 'need_action';
								}			
							}
						}
					}else{
						$value->action = 'done';
					}
				}


			    ///}else{
				// return  response()->json(['header' => ['message' => 'please  try refesh browser, failed to get data', 'status' => 500],'data' => []],500);

			    //}

			    $res[] = $res_temp['data'][$key];
			}
		}	




		if(isset($res)){			

			//sort data by approver and date 
			
			//sort data first_time
			$sort_data_approval  =  $this->sort_data_unapproval($res);
			$count  =  count($sort_data_approval);			
		 	return $resU =  $this->find_approval($res,$count,$sort_data_approval);
		
			//return $this->sort_by_date($resU);
		}else{
			return [];
		}

		return $res;

	}

	public function sorting($a,$b){
		return strtotime($a['date']) -  strtotime($b['date']);
	}
	
	public function sort_by_date($data){
		$arr  = [];
		foreach($data  as $key  => $value){
			if(isset($value->created_at)){
			   $explode = explode(' ',$value->created_at);
			   $arr[] = ['key'  => $key, 'date' => $explode[0]];
			}else{
			   $arr[]  = ['key' => $key,  'date' => $value->date_  ];			
			}
		}	

		return $arr;
	}

	public function sort_data_unapproval($data){
	     $arr  = [];
	     $i    = 0;
	     foreach($data as $key => $value){
		
		if($value->approval != null and $value->approver != null){
		   unset($data[$key]);
		}else{
		   $arr[$i] =  $data[$key];
		   $i++;
		}
	     }
		
	     return $arr;
	}

	public function find_approval($data,$count,$arr){
	     
	     $i    = $count;
	     foreach($data as $key => $value){
		
		if($value->approval == null and $value->approver == null){
		   unset($data[$key]);
		}else{
		   $arr[$i] =  $data[$key];
		   $i++;
		}
	     }
		
	     return $arr;
	}



	public function notif($ids,$type,$employee_id,$local_it,$user,$from_type = null){

		/**
		 * for notif hr
		 */
		$supervisor=  \DB::SELECT("select * from emp_supervisor where supervisor =  '$employee_id' ");
		$emp_x  = \DB::SELECT("select * from view_nonactive_login where employee_id  =   '$employee_id' and (lower(role_name) = 'regular employee user' or  lower(role_name) = 'user') ");
		$hr = \DB::SELECT("select employee_id from  ldap, role where ldap.role_id = role.role_id and (lower(role.role_name) like '%human%' or  '%human resource%' or '%hr%')");

		$hrx = [];
		$hrx_comp  = [];
		foreach($hr as $key => $value){
			$hrx[] = [ $value->employee_id => 0,
 					   "hr_stat" =>  0,
 					   "hr_date" => '0000-00-00',
 					   "hr_time" => "00:00",
 					   "read_stat" => 0,	
					 ]; 

			$hrx_comp[] = $value->employee_id; 
		}
		$hrx_comp[] = '2014888';

			$hrx[]  = ["2014888" => 0,
 					   "hr_stat" =>  0,
 					   "hr_date" => '0000-00-00',
 					   "hr_time" => "00:00",
 					   "read_stat" => 0,	
				  ];
		

		/**
		 * find who is  employee supervisor
		 */
		$sub = \DB::SELECT("select subordinate from emp_subordinate where employee_id = '$employee_id' ");
		
		$subx = [];
		$subx_comp = [];
		foreach($sub as $key => $value){
			$subx[] =  [ $value->subordinate => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
			$subx_comp[] = $value->subordinate; 
		}

		$subx_comp[] =  '2014888';

		$subx =  [ '2014888' => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ];

		/**
		 * find subordinate for this employee  id
		 * @var [type]
		 */
		
		$sup = \DB::SELECT("select supervisor from emp_supervisor where employee_id = '$employee_id' ");
		
		$supx = [];
		$supx_comp  = [];
		foreach($sup as $key => $value){
			$supx[] =  [ $value->supervisor => 0,
 					   	 "sup_stat" =>  0,
 					     "sup_date" => '0000-00-00',
 					     "sup_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
			$supx_comp[]  = $value->supervisor;
		}

		$supx_comp[] = '2014888';

		$supx[] = [ "2014888"  => 0,
 					"sup_stat" =>  0,
 					"sup_date" => '0000-00-00',
 					"sup_time" => "00:00",
 					"read_stat" => 0,	
				  ];
		
		/**
		 * slelecempoyee how will swap with the other employee 
		 * @var [type]
		 */
		

		$swap = \DB::SELECT("select swap_with from att_swap_shift where employee_id = '$employee_id' and swap_id = (select request_id from att_schedule_request where employee_id='$employee_id' and id = $ids and type_id = 6 order by id desc limit 1 )  ");
		
		$swapx = [];
		$swapx_comp = [];
		foreach($swap as $key => $value){
			$swapx[] =  [ $value->swap_with => 0,
 					   	 "swap_stat" =>  0,
 					     "swap_date" => '0000-00-00',
 					     "swap_time" => "00:00",
 					     "read_stat" => 0,	
					   ];
			$swapx_comp[] =   $value->swap_with;
		}
		$swapx_comp[] =  '2014888';

		$swapx[] = [ "2014888" => 0,
 					 "swap_stat" =>  0,
 					 "swap_date" => "0000-00-00",
 					 "swap_time" => "00:00",
 					 "read_stat" => 0,	
					   ];

		
		/**
		 * if employee == attendace
		 * @var [type]
		 */
		if($from_type == "attendance"){

			if($type == 9){

				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['hr' => 1,'swap' => '0','sup' => '0','hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
								"hr" => $hrx,
								"sup" => [],
								"swap" => [],
								"hrx_comp" => $hrx_comp,
								"supx_comp" => [],
								"empx_comp" => [],
						   ];
				}else{
					$master =  'schedule';
					$end = ['sup' => 1,'hr' => '0','swap' => '0','hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
								"sup" => $supx,
								"hr" => [],
								"swap" => [],
								"supx_comp" => $supx_comp,
								"hrx_comp" => [],
								"swapx_comp" => [],
									
						   ];
				}
			}


			if($type == 3){

				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['sup' => 1 ,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],
								
						];
				} 
			}
			if($type == 4){
				if($local_it == 'local'){
					$master =  'schedule';
					$end = ['sup' => 1,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],
								
						]; 
				}
			}
			if($type == 7 || $type == 8){
				if($local_it == 'local'){
					$master =  'schedule';	
					$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => $hrx_comp,
						"swapx_comp" => [],
								
						]; 
				}
			}
			if($type == 2){
				$master =  'schedule';
				$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],			
						
						]; 
			}

			if($type == 1){
				$master =  'schedule';
				$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],			
						
						]; 
			}
			if($type == 5){

				if($user == "sup"){
					$master =  'schedule';
					$end = ['swap' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
							"swap" => $subx,			
							"sup" => [],
							"hr" => [],
							"swapx_comp" => $subx_comp,			
							"supx_comp" => [],
							"hrx_comp" => [],
							
						   ]; 
				}else{
					$master =  'schedule';
					$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"emp" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"empx_comp" => [],
								
						];
				}
			}

			if($type == 6){

				if($local_it == 'local'){
					if(isset($supervisor) && $supervisor != null){
						$master =  'schedule';
						$end = ['swap' => 1,'sup' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
								"swap" => $swapx,			
								"sup" => [],
								"hr" => [],
								"swapx_comp" => $swapx_comp,			
								"supx_comp" => [],
								"hrx_comp" => [],
							   ]; 
					}else{
						$master =  'schedule';
						$end = ['swap' => 1,'sup' => 2,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
								"swap" => $swapx,			
								"sup" => $supx,
								"hr" => [],
								"swapx_comp" => $swapx_comp,			
								"supx_comp" => $supx_comp,
								"hrx_comp" => [],
							   ]; 
						}	
				}
			}
		}else{


		//leave request 

			if($type == 2){
				if($local_it == 'local'){
					$master =  'leave';
					$end = ['hr' => 1,'sup' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
							"hr" => $hrx,
							"sup" => $supx,
							"swap" => [],
							"hrx_comp" => $hrx_comp,
							"supx_comp" => $supx_comp,
							"swapx_comp" => [],			
							
						   ]; 
				}else{
					$master =  'leave';
					$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
							"sup" => $supx,
							"swap" => [],			
							"hr" => [],
							"supx_comp" => $supx_comp,
							"swapx_comp" => [],			
							"hrx_comp" => [],
						   ];
				}
			}

			if($type == 10){
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],			
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_comp" => []
				   ];
			}

			if($type == 3){
				$master =  'leave';
				if($local_it == 'local'){
					
					$end = ['hr' => 1,'sup' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"hr" => $hrx,
						"sup" => $supx,
						"swap" => [],
						"hrx_comp" => $hrx_comp,
						"supx_comp" => $supx_comp,
						"swapx_comp" => [],			
					
					   ];
				}	
			}

			if($type == 1){
				$master =  'leave';
				if($local_it == 'local'){
					$end = ['hr' => 1,'sup' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"hr" => $hrx,
						"sup" => [],
						"swap" => [],
						"hrx_comp" => $hrx_comp,
						"supx_comp" => [],
						"swapx_comp" => [],			
					
					   ];
				}
			}

			if($user == "hr" && $type == 4 ){
				if($local_it == 'local'){
					$master =  'leave';
					$end = ['sup' => 1,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_compx" => $supx_comp,
						"hrx_compx" => [],
						"swapx_compx" => [],			
					
					   ];
				}
				
			}
			
			if($type == 4 && $local_it == 'local'){
				$master =  'leave';
				$end = ['hr' => 1,'sup' => 2,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"hr" => $hrx,
					"sup" => $supx,
					"swap" => [],
					"hrx_comp" => $hrx_comp,
					"supx_comp" => $supx_comp,
					"swapx_compx" => [],			
					
				   ];	
			}
			if($type == 4 && $local_it == 'expat'){
				$master =  'leave';
				$end = ['sup' => 1,'swap' => 0, 'hr' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				$arr = [
					"sup" => $supx,
					"hr" => [],
					"swap" => [],
					"supx_comp" => $supx_comp,
					"hrx_comp" => [],
					"swapx_comp" => [],


					
				   ];
			}

			if($type ==  5){
				$master =  'leave';
				//if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['hr' => 1,'swap' => 0,'sup' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
						"hr" => $hrx,
						"sup" => [],
						"swap" => [],
						"hrx_comp" => $hrx_comp,
						"supx_comp" => [],
						"swapx_comp" => [],			
					
					   ];		
				//}else{
					//$master =  'leave';
					//if($local_it == 'expat'){
					}else{
						$end = ['sup' => 1,'hr' => 0,'swap' => 0, 'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],			
					
					   ];		
					}	
				//}
			}

			if($type == 6){
				$master =  'leave';
				if($local_it == 'local'){
					$end = ['sup' => 1,'hr' => 2 ,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => $hrx_comp,
						"swapx_comp" => [],			
					
					   ];		
				}else{
					$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],			
					
					   ];
				}
			}

			if($type == 7){
				$master =  'leave';
				if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],		
							"supx_comp" => $supx_comp,
							"hrx_comp" => [],
							"swap" => [],			
							
					   ];		
					}
				}
				else{
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"swap" => [],
							"supx_comp" => $supx,
							"hrx_comp" => $hrx,
							"swap" => [],			
							
					   ];		
					}else{
						$end = ['sup' => 1,'swap' => 0,'hr' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],
							"supx_comp" => $supx_comp,
							"hrx_comp" => [],
							"swapx_comp" => [],			
							
					   ];
					}
				}
			}

			if($type == 8){
				$master =  'leave';
				if($local_it == 'local'){
					$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => $hrx,
						"swap" => [],			
						"supx_comp" => $supx_comp,
						"hrx_comp" => $hrx_comp,
						"swapx_comp" => [],					
				   ];		
				}else{
					$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
					$arr = [
						"sup" => $supx,
						"hr" => [],
						"swap" => [],			
						"supx_comp" => $supx_comp,
						"hrx_comp" => [],
						"swapx_comp" => [],
				   ];
				}
			}

			if($type == 11){
				$master =  'leave';
				// if($user == 'hr'){
				// 	if($local_it == 'local'){
				// 		$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
				// 		$arr = [
				// 			"sup" => $supx,
				// 			"hr" => [],
				// 			"swap" => [],			
				// 			"supx_comp" => $supx,
				// 			"hrx_comp" => [],
				// 			"swapx_comp" => [],		
				// 	   ];		
				// 	}
				// }
				// else{
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"swap" => [],
							"supx_comp" => $supx,
							"hrx_comp" => $hrx,
							"swapx_comp" => [],			
							
					   ];		
					}else{
						$end = ['sup' => 1,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => [],
							"swap" => [],
							"supx_comp" => $supx,
							"hrx_comp" => [],
							"swapx_comp" => [],			
							
					   ];
					}
				//}
			}

			if($type == 12){
				$master =  'leave';
				if($user == 'hr'){
					if($local_it == 'local'){
						$end = ['sup' => 0,'hr' => 0,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"swap" => [],
							"supx_comp" => $supx_comp,
							"hrx_comp" => $hrx_comp,
							"swapx_comp" => [],			
							
					   ];		
					}
				}
			}

			if($type == 9){

				$master =  'leave';
				if($swap_x != null){
					if($local_it == 'local'){
						$end = ['sup' => 1,'hr' => 2,'swap' => 0,'hr_approve' => 'o','swap_approve' => 'o','sup_approve' => 'o'];
						$arr = [
							"sup" => $supx,
							"hr" => $hrx,
							"swap" => [],
							"supx_comp" => $supx_comp,
							"hrx_comp" => $hrx_comp,
							"swapx_comp" => [],			
							
					   ];		
					}
				}
			}


		}
		
		$chatID = [];
		if(isset($arr) && $arr  !=  null){
			$chat  = \DB::SELECT("select id from command_center where  request_id=  $ids");
			if($chat != null){
				foreach ($chat as $key => $value) {
					$chatID[$value->id]  = 'u';
				}	
			}
			
			$arr['requestor_stat'] =  [$employee_id => 0];
			$arr['chat_id'	] = $chatID;
			$arr['req_flow'] = $end;
			$arr['master'] =  $master ;
		}else{
			return  $arr  =[ 'data' => [], 'message' =>  'your not allowed to request '];
		}

		//print_r($arr);
		//
		//if $from_type  attendance $from_type  = 1 else 2  
		//
		// 
		//
				
		$from_type =  ($from_type == 'attendance' ?
		 1 : 2);
		$json = json_encode($arr);
		try{
			if($from_type == 1){
				$check_schedule_type =  \DB::SELECT("select * from att_type  wherebid  =  $type ")[0]->type;
				$type_mail = "Schedule Requets List [ ".$check_schedule_type." ]";
			}else{

				$check_type_request  =  \DB::SELECT("select * from leave_type where id = $type ");
				$leave_c_type =$check_type_request[0]->leave_type;
				$type_mail  = "Leave Request List [ ".$leave_c_type."]";
			}
			///print_r("select concat(first_name,' ',middle_name,' ',last_name)as name, personal_email,work_email  from emp where employee_id =  '$employee_id' ");
			$dt = json_decode($json,1);
			$dt_flow =   $dt['req_flow'];

			$get_detail  = \DB::SELECT("select concat(first_name,' ',middle_name,' ',last_name)as name, personal_email,work_email  from emp where employee_id =  '$employee_id' ");
			$namex =  $get_detail[0]->name;
			$work_mail  =  $get_detail[0]->work_email;
			$personal_mail  =  $get_detail[0]->personal_email;


			foreach ($dt_flow as $key => $value) {
				if($value == 1){
					$get_data_employee = $dt[$key];

					 foreach ($get_data_employee as $key => $value) {
					 	foreach ($value as $key => $value) {
					 		if(is_numeric($key)){
					 			$get_detail  = \DB::SELECT("select concat(first_name,' ',middle_name,' ',last_name)as name, personal_email,work_email  from emp where employee_id =  '$key' ");
								
					 			$work_mails  =  $get_detail[0]->work_email;
					 			$personal_mails  =  $get_detail[0]->personal_email;


					 	        	if($work_mails == null){
					 	        		$mails  = $personal_mails;
					 	        	}else{
					 	        		$mails  =  $work_mails;
					 	        	}

					 	        	if($mails  != null){
					 	        		$data = array('name'=>$namex,"request" => $type_mail, 'email' =>  $mails);
					 			    	\Mail::send('emails.welcome', $data, function($message) use ($data){
					 			    		//print_r($data);
								        $message->to($data['email'])->subject('Leekie request reminder');
					 			    });
					 	        	}
					 		}
					 	}
					 }
				}
			}


	         if($work_mail == null){
	         	$mail  = $personal_mail;
	         }else{
	         	$mail  =  $work_mail;
	         }		

		     $data = array('name'=>$namex,"request" => $type_mail, 'email' =>  $mail);
		     \Mail::send('emails.employee', $data, function($message) use ($data){
		         $message->to($data['email'])->subject('Leekie request reminder');
		     });

			$insert = \DB::SELECT("insert into pool_request(employee_id,id_req,type_id,master_type,json_data,created_at) 
								   values('$employee_id',$ids,'$type','$from_type','$json',now())");
			return "success";
		}catch(\Exeception $e){
			return $e;
		}
	}


	/**
	 * count notification from cahta an rquest
	 * @return [type]
	 */
	public  function count_notif(){

		$KEY  = \Input::get('key');
		if($KEY == 'undefined'){
			return response()->json(['header' =>['message' => 'token undefined', 'status' => 201],'data' =>  []],201);
		}else{
		$DEC_KEY =  explode('-',(base64_decode($KEY)))[1];
		}
		$check = array();  
		$notif  =  0;
		
		$select = \DB::SELECT("select * from pool_request where DATE_SUB(now(),INTERVAL 7 DAY)");				
		$count_select = count($select);

		for($i  = 0; $i < $count_select; $i++){

			$enc  = json_decode($select[$i]->json_data,1);

			/**
			 * for calculate notification
			 */

				foreach ($enc['req_flow'] as $key => $value) {
					if($value ==  1){
						foreach ($enc[$key] as $key1 => $value1 ){
						
							if(isset($value1[$DEC_KEY])){
								
								if($value1['read_stat'] == 0 and $value1[$key.'_stat'] == 0  ){
									$notif += 1;
								}
							}
						}	
						$get_flow  = $key ;
					}

					if($value == 2 ){

						foreach ($enc[$key] as $key2 => $value2){
							
							if(isset($value2[$DEC_KEY])){
								//return $value2['read_stat'];
								if($value2['read_stat'] == 0 and $value2[$key.'_stat'] == 0 and  $enc['req_flow'][$get_flow.'_approve'] != 'x' ){
								
									$notif += 1;
								}
							}
						}
						$get_flow =  $key;


					}	
					
					

					if($value == 3){
						foreach ($enc[$key] as $key3 => $value3) {
							if(isset($value3[$DEC_KEY])){
								if($value3['read_stat'] == 0  and   $value2[$get_flow.'_stat'] ==  1  and $value3[$value3.'_stat'] != 1){
									$notif += 1;
								}
							}	
						}
					}
				}

		
			

				
				foreach ($enc['chat_id'	] as $keyx => $valuex){
					$get_user_chat  = \DB::SELECT("select employee_id from command_center where id  =  $keyx");
					foreach ($get_user_chat as $keyz => $valuez) {
						if(isset($valuez->employee_id) and ($valuez->employee_id != $DEC_KEY) and ($valuex ==  'u')){
							$flow_check   =  $enc['req_flow'];

							   /**
							    * logic  json    req_flow : { 'sup' : 1, 'hr' :  0, 'swap' :  0, "hr_approve":"o", "swap_approve":"o","sup_approve":"o"}
							    * if(already approve) if( 1 : sup  then  sup_approve : chnage  to  'x'  and continue)
							    * if( flow 1 and  already approve 'x '  thenn  find flow 2 dan  get properties  like 'sup'  fo $data['sup']) 
							    * if  loop $data['sup']  find token notif  +1;
							    * @var [type]
							    */
							   foreach ($flow_check as $key => $value) {
							   		if($value  ==  1 ) {
							   			if($flow_check[$key.'_approve'] ==  'x'){
							   				foreach ($flow_check as $k=> $v) {
							   					if($v == 2){
							   						$check_user  = $enc[$k];
							   						foreach ($check_user as $x => $y) {
							   							if(isset($y->$DEC_KEY)){
							   								 $notif += 1;

							   							}
							   						}
							   					}
							   				}
							   			}else{
							   				$check_user  = $enc[$key];
							   				foreach ($check_user as $x => $y) {
					   							if(isset($y->$DEC_KEY)){
					   								$notif += 1;
					   							}
					   						}
							   			}
							   		}else{
							   			if($value ==  2){
							   				if($flow_check[$key.'_approve'] !=  'x'){
								   				foreach ($flow_check as $k=> $v) {
								   					if($v == 1){
								   						$check_user  = $enc[$k];
								   						foreach ($check_user as $x => $y) {
								   							if(isset($y->$DEC_KEY)){
								   								$notif += 1;
								   							}
								   						}
								   					}
								   				}
								   			}else{
								   				$check_user  = $enc[$key];
								   				foreach ($check_user as $x => $y) {
						   							if(isset($y->$DEC_KEY)){
						   								$notif += 1;
						   							}
						   						}
								   			}	
							   			}
							   		}
							   }// 
						}
					}
				}	
		}
		
	
		return \Response::json(['ping' => $notif,'request' => [] ]);
	}


	public function approve(){

		$token =  explode('-',base64_decode((\Input::get('key'))))[1];  
	
		$i  = \Input::all();
		if(isset($i[0])){
			if($i[0]['master_type'] == 1 ){

				
				$id  =  $i[0]['id'];
				$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
				$json  =  json_decode($get_pool[0]->json_data,1);

				$flow =  $json['req_flow'];
				
				/**
				*	get time and date 
				**/

				$time  =  date('H:i');
				$date  = date('Y-m-d');

				/**
				* get user stat 2 or just 1 stat
				**/

				$get_step  =  0;
				$user = '';
				$temp = [];

				/**	
				*	get flow user
				**/
				foreach($flow as $key => $value){
					$temp[] = $value;		
				}

				/**	
				*check using in array target data exist
				**/

				if(in_array(2,$temp)){
					$get_step = 2;
				}else{
					$get_step = 1;
				}

				/**
				*	if 2 step approval
				*	check approval if already approved
				*   if approval already approve
				*	then approve approver
				**/
				

				$get_already_approval  = \DB::SELECT("select * from att_schedule_request where id =  $id ");
				if($get_step == 2){
					
					if($get_already_approval[0]->approval != null ){
						$arr_cek =   [1,3,4];
						if(in_array($get_already_approval[0]->approval,$arr_cek)){
							return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
						}
			
						\DB::SELECT("update att_schedule_request set approver =  2, approver_date  ='$token' where id = $id");
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 2){
								$key_x = $key;
							}
						}

						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json[$key_x][$key]['read_stat'] = 1;
								$json['req_flow'][$key_x.'_approve'] = 'x';
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						/**  
						 * chnage  all chat to already read notif  has gone or end 
						 */

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'	][$keyy] = 'r';
						}

						$json['action_2']['employee_id'] = $token;
						$json['action_2']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_2']['act'] = 'A';

						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);

						$gettimein  = \DB::SELECT("select * from pool_request where id_req = $id and master_type  =  1 and type_id  = 9");
						if($gettimein != null){
								$id_new =  $gettimein[0]->id_req;
								$sq  = \DB::SELECT("select * from att_schedule_request where id = $id_new ")[0]->request_id;
								$gte_data  = \DB::SELECT("select * from att_time_in_out_req where req_in_out_id  = $sq  ");
								$date_t =  $gte_data[0]->date;
								$time_in_t =  $gte_data[0]->req_in;
								$time_out_t =  $gte_data[0]->req_out; 
								$emp_t  =  $gte_data[0]->employee_id;
								$insert_biometric  = \Db::SELECT("insert into  biometrics_device(date,time_in,time_out,employee_id) values('$date_t','$time_in_t','$time_out_t','$emp_t') ");
						}		

						$schedule = \DB::SELECT("update att_schedule_request set status_id = 2 where id  =  $id ");

						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
						

					}else{
						
						if($get_already_approval[0]->approval == null){
							\DB::SELECT("update att_schedule_request set approver =  0, approval = 2 where id = $id");
							/**
							* eg : $json[sup] or $json[sup]
							**/
							
							foreach($json['req_flow'] as $key  => $value){
								if($value == 1){
									$key_x = $key;
								}
							}

							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json[$key_x][$key]['read_stat'] = 1;
									$json['req_flow'][$key_x.'_approve'] = 'x';
								}else{
								   $json[$key_x][$key]['read_stat'] = 1;
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
									$json['chat_id'][$keyy] = 'r';
							}

						$json['action_1']['employee_id'] = $token;
						$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_1']['act'] = 'A';

							/**
							* update json_data;
							**/

							$gettimein  = \DB::SELECT("select * from pool_request where id_req = $id and master_type  =  1 and type_id  = 9");
							if($gettimein != null){
									$id_new =  $gettimein[0]->id_req;
									$sq  = \DB::SELECT("select * from att_schedule_request where id = $id_new ")[0]->request_id;
									$gte_data  = \DB::SELECT("select * from att_time_in_out_req where req_in_out_id  = $sq  ");
									$date_t =  $gte_data[0]->date;
									$time_in_t =  $gte_data[0]->req_in;
									$time_out_t =  $gte_data[0]->req_out; 
									$emp_t  =  $gte_data[0]->employee_id;
									$insert_biometric  = \Db::SELECT("insert into  biometrics_device(date,time_in,time_out,employee_id) values('$date_t','$time_in_t','$time_out_t','$emp_t') ");
							}
					
							$json_x  =  json_encode($json,1);
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
						}
					}


				}else{
					
					if($get_already_approval[0]->approval == null){
						\DB::SELECT("update att_schedule_request set approver =  2, approval = 0, approver_date =  '$token' where id = $id");
						/**
						* eg : $json[sup] or $json[sup]
						**/
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 1){
								$key_x = $key;
							}
						}

						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json[$key_x][$key]['read_stat'] = 1;
								$json['req_flow'][$key_x.'_approve'] = 'x';
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						$json['action_1']['employee_id'] = $token;
						$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_1']['act'] = 'A';
						
						$gettimein  = \DB::SELECT("select * from pool_request where id_req = $id and master_type  =  1 and type_id  = 9");
						if($gettimein != null){
								$id_new =  $gettimein[0]->id_req;
								$sq  = \DB::SELECT("select * from att_schedule_request where id = $id_new ")[0]->request_id;
								$gte_data  = \DB::SELECT("select * from att_time_in_out_req where req_in_out_id  = $sq  ");
								$date_t =  $gte_data[0]->date;
								$time_in_t =  $gte_data[0]->req_in;
								$time_out_t =  $gte_data[0]->req_out; 
								$emp_t  =  $gte_data[0]->employee_id;
								$insert_biometric  = \Db::SELECT("insert into  biometrics_device(date,time_in,time_out,employee_id) values('$date_t','$time_in_t','$time_out_t','$emp_t') ");
						}



						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update att_schedule_request set status_id = 2 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
					}
				}
			}else{

				$id  =  $i[0]['id'];
				$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 2");
				$json  =  json_decode($get_pool[0]->json_data,1);

				$flow =  $json['req_flow'];
				
				/**
				*	get time and date 
				**/

				$time  =  date('H:i');
				$date  = date('Y-m-d');

				/**
				* get user stat 2 or just 1 stat
				**/

				$get_step  =  0;
				$user = '';
				$temp = [];

				/**	
				*	get flow user
				**/
				foreach($flow as $key => $value){
					$temp[] = $value;		
				}

				/**	
				*check using in array target data exist
				**/

				if(in_array(2,$temp)){
					$get_step = 2;
				}else{
					$get_step = 1;
				}

				/**
				*	if 2 step approval
				*	check approval if already approved
				*   if approval already approve
				*	then approve approver
				**/
				

				$get_already_approval  = \DB::SELECT("select * from leave_request where id =  $id ");
				if($get_step == 2){
					if($get_already_approval[0]->approval != null ){
						$arr_cek =   [1,3,4];
					
						if(in_array($get_already_approval[0]->approval,$arr_cek)){
							return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
						}
			
						\DB::SELECT("update leave_request set approver =  2 where id = $id");
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 2){
								$key_x = $key;
							}
						}

						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json[$key_x][$key]['read_stat'] = 1;
								$json['req_flow'][$key_x.'_approve'] = 'x';
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						$json['action_2']['employee_id'] = $token;
						$json['action_2']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_2']['act'] = 'A';

						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update leave_request set status_id = 2 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						

					}else{
						
						if($get_already_approval[0]->approval == null){
						
							\DB::SELECT("update leave_request set approver =  0, approval = 2 where id = $id");
							/**
							* eg : $json[sup] or $json[sup]
							**/
							
							foreach($json['req_flow'] as $key  => $value){
								if($value == 1){
									$key_x = $key;
								}
							}

							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json[$key_x][$key]['read_stat'] = 1;
									$json['req_flow'][$key_x.'_approve'] = 'x';
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}	

							$json['action_1']['employee_id'] = $token;
							$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
							$json['action_1']['act'] = 'A';
							
							
							/**
							* update json_data;
							**/

							$json_x  =  json_encode($json,1);
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						}
					}
				}else{
					
					if($get_already_approval[0]->approval == null){
						\DB::SELECT("update leave_request set approver =  2, approval = 0 where id = $id");
						/**
						* eg : $json[sup] or $json[sup]
						**/
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 1){
								$key_x = $key;
							}
						}
						
						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json['req_flow'][$key_x.'_approve'] = 'x';
								$json[$key_x][$key]['read_stat'] = 1;
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						$json['action_1']['employee_id'] = $token;
						$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_1']['act'] = 'A';

						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update leave_request set status_id = 2 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
					}
				}


				//trigger action 
				//
				$set_id =  $i[0]['id'];
				$set_employ =  $i[0]['employee_id'];

				$trigger  = \DB::SELECT("select * from leave_request where id  = $set_id ");

				if($trigger !=  null){
					$tgr_approval =  (empty($trigger[0]->approval) ? 0 : $trigger[0]->approval);
					$tgr_approved =  (empty($trigger[0]->approver) ? 0 : $trigger[0]->approver);

					if($tgr_approved != 0 ){
						$tgr_approved =  true;
					}else{
						$tgr_approved =  false;
					}

					if($tgr_approval != 0 ){
						$tgr_approval =  true;
					}else{
						$tgr_approval =  false;
					}

					$data_response  = [
										'approval' => $tgr_approval,
										'approver' =>  $tgr_approved,
										'id' => $i[0]['id']
									  ];

					return response()->json(['header' => ['status' => 200,  'messsage' => 'success  to approve data'],'data' => $data_response],200);
				}
			}
		}

		//return response()->json(['header' => ['message' =>  'success approve data',  'status' => 200] ,'data' => []], 200);
	} 

	public function  reject(){

		$token =  explode('-',base64_decode((\Input::get('key'))))[1];  
		$i  = \Input::all();

		if(isset($i[0]['id'])){
		    $set_id = $i[0]['id'];  
		}else{
		    $set_id = $i['id'];
		}
		
		if(isset($i[0]['master_type'])){
		  $master_type  =  $i[0]['master_type'];
		}elseif(isset($i['master_type'])){
		  $master_type  =  $i['master_type'];
		}else{
		  $master_type  =  null;
		}	
		
		
		if(isset($set_id)){	
			if($master_type == 1 ){

				$id  =  $set_id;
				$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
				$json  =  json_decode($get_pool[0]->json_data,1);

				$flow =  $json['req_flow'];
				
				/**
				*	get time and date 
				**/

				$time  =  date('H:i');
				$date  = date('Y-m-d');

				/**
				* get user stat 2 or just 1 stat
				**/

				$get_step  =  0;
				$user = '';
				$temp = [];

				/**	
				*	get flow user
				**/
				foreach($flow as $key => $value){
					$temp[] = $value;		
				}

				/**	
				*check using in array target data exist
				**/

				if(in_array(2,$temp)){
					$get_step = 2;
				}else{
					$get_step = 1;
				}

				/**
				*	if 2 step approval
				*	check approval if already approved
				*   if approval already approve
				*	then approve approver
				**/
			
				
				$get_already_approval  = \DB::SELECT("select * from att_schedule_request where id =  $id ");
				if($get_step == 2){
					
					if($get_already_approval[0]->approval != null ){
						$arr_cek =   [1,3,4];
						if(in_array($get_already_approval[0]->approval,$arr_cek)){
							return response()->json(['header' => ['message' => "taken already applied, contact programmer  for more information", 'status' => 500],'data' => []],500);
						}
			
						\DB::SELECT("update att_schedule_request set approver =  3, status_id  = 3 and approver_date = '$token'  where id = $id");
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 2){
								$key_x = $key;
							}
						}

						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json['req_flow'][$key_x.'_approve'] = 'x';
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						$json['action_2']['employee_id'] = $token;
						$json['action_2']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_2']['act'] = 'R';

						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update att_schedule_request set status_id = 3, approver_date = '$token' where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
						

					}else{
						
						if($get_already_approval[0]->approval == null){
							
							\DB::SELECT("update att_schedule_request set approver =  3, approval = 0,  approver_date = '$token' , status_id  = 3 where id = $id");
							/**
							* eg : $json[sup] or $json[sup]
							**/
							
							foreach($json['req_flow'] as $key  => $value){
								if($value == 1){
									$key_x = $key;
								}
							}

							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'x';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}

							$json['action_1']['employee_id'] = $token;
							$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
							$json['action_1']['act'] = 'R';
							/**
							* update json_data;
							**/


							
							$json_x  =  json_encode($json,1);
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
						}
					}
				}else{
					
					if($get_already_approval[0]->approval == null){
						\DB::SELECT("update att_schedule_request set approver =  3, approval = 0, status_id  =  3,approver_date = '$token' where id = $id");
						/**
						* eg : $json[sup] or $json[sup]
						**/
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 1){
								$key_x = $key;
							}
						}

						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json['req_flow'][$key_x.'_approve'] = 'x';
								$json[$key_x][$key]['read_stat'] = 1;
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						$json['action_1']['employee_id'] = $token;
						$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_1']['act'] = 'R';

						/**
						* update json_data;
						**/
						
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update att_schedule_request set status_id = 3, approver_date = '$token' where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
					}
				}
			}else{
				$id  =  $i[0]['id'];
				$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 2");
				$json  =  json_decode($get_pool[0]->json_data,1);

				$flow =  $json['req_flow'];
				
				/**
				*	get time and date 
				**/
		
				$time  =  date('H:i');
				$date  = date('Y-m-d');

				/**
				* get user stat 2 or just 1 stat
				**/

				$get_step  =  0;
				$user = '';
				$temp = [];

				/**	
				*	get flow user
				**/
				foreach($flow as $key => $value){
					$temp[] = $value;		
				}

				/**	
				*check using in array target data exist
				**/

				if(in_array(2,$temp)){
					$get_step = 2;
				}else{
					$get_step = 1;
				}

				/**
				*	if 2 step approval
				*	check approval if already approved
				*   if approval already approve
				*	then approve approver
				**/
			
				$get_already_approval  = \DB::SELECT("select * from leave_request where id =  $id ");
				if($get_step == 2){
					if($get_already_approval[0]->approval != null ){
						$arr_cek =   [1,3,4];
					
						if(in_array($get_already_approval[0]->approval,$arr_cek)){
							return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
						}
					    

					    \DB::SELECT("update leave_request set approver =  3 where id = $id");
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 2){
								$key_x = $key;
							}
						}

						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json['req_flow'][$key_x.'_approve'] = 'x';
								$json[$key_x][$key]['read_stat'] = 1;
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						$json['action_2']['employee_id'] = $token;
						$json['action_2']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_2']['act'] = 'R';
						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update leave_request set status_id = 3 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x' where id_req = $id  and master_type = 2");
						

					}else{
					
						if($get_already_approval[0]->approval == null){
					
							\DB::SELECT("update leave_request set approver =  0, approval = 3, status_id  =  3 where id = $id");
							/**
							* eg : $json[sup] or $json[sup]
							**/
							
							foreach($json['req_flow'] as $key  => $value){
								if($value == 1){
									$key_x = $key;
								}
							}

							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'x';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}

							$json['action_1']['employee_id'] = $token;
							$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
							$json['action_1']['act'] = 'R';
							
							/**
							* update json_data;
							**/
							
							
							$json_x  =  json_encode($json,1);
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						}
					}
				}else{
					
					if($get_already_approval[0]->approval == null){
						\DB::SELECT("update leave_request set approver = 3, approval = 0 where id = $id");
						/**
						* eg : $json[sup] or $json[sup]
						**/
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 1){
								$key_x = $key;
							}
						}
						
						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json['req_flow'][$key_x.'_approve'] = 'x';
								$json[$key_x][$key]['read_stat'] = 1;
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						$json['action_1']['employee_id'] = $token;
						$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_1']['act'] = 'R';

						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update leave_request set status_id = 3 where id  =  $id");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
					}
				}
			}
		}
		
		if(!isset($i[0]['id'])){
		     $set_id =  $i['id'];
		}else{
		     $set_id =  $i[0]['id'];
		}
		//$set_employ =  $i[0]['employee_id'];
		
		if(!isset($i[0]['master_type'])){
			$master_type  = $i['master_type'];  
		}else{		
			$master_type  =  $i[0]['master_type'];
		}

		if($master_type == 1){
			$type_R  = "att";
		}else{
			$type_R = "leave";
		}
		
		if($type_R  ==  "att"){
			$trigger  = \DB::SELECT("select * from att_schedule_request where id  = $set_id ");
		}else{
			$trigger  = \DB::SELECT("select * from leave_request where id  = $set_id ");
		}	
		

		if($trigger !=  null){

			if($trigger[0]->status_id  == 3){
				$reject_opt  =   true;
			}else{
				$reject_opt =  false;
			}

		

			$data_response  = [
						'reject' => $reject_opt,
						'id' => $set_id
					  ];

			return response()->json(['header' => ['status' => 200,  'messsage' => 'success  to reject request'],'data' => $data_response],200);
		}
	}	

	public function cancle(){

		$token =  explode('-',base64_decode((\Input::get('key'))))[1];  
		$i  = \Input::all();
				
		if(isset($i[0])){
			$i = $i[0];		
		}

		if(isset($i['id'])){
			
			if($i['master_type'] == 1 ){

				$id  =  $i['id'];
				$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 1");
				$json  =  json_decode($get_pool[0]->json_data,1);

				$flow =  $json['req_flow'];
				
				/**
				*	get time and date 
				**/

				$time  =  date('H:i');
				$date  = date('Y-m-d');

				/**
				* get user stat 2 or just 1 stat
				**/

				$get_step  =  0;
				$user = '';
				$temp = [];

				/**	
				*	get flow user
				**/
				foreach($flow as $key => $value){
					$temp[] = $value;		
				}

				/**	
				*check using in array target data exist
				**/

				if(in_array(2,$temp)){
					$get_step = 2;
				}else{
					$get_step = 1;
				}

				/**
				*	if 2 step approval
				*	check approval if already approved
				*   if approval already approve
				*	then approve approver
				**/
			

				$get_already_approval  = \DB::SELECT("select * from att_schedule_request where id =  $id ");
				if($get_step == 2){
					if($get_already_approval[0]->approval != null ){
						$arr_cek =   [1,3,4];
						if(in_array($get_already_approval[0]->approval,$arr_cek)){
							return response()->json(['header' => ['message' => "can't cancle", 'status' => 500],'data' => []],500);
						}
			
						\DB::SELECT("update att_schedule_request set approver =  4, approval  =  0, status_id = 4 where id = $id");
					

						foreach($json['req_flow'] as $key  => $value){
							if($value == 2){
								$key_x = $key;
							}
						}

						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json['req_flow'][$key_x.'_approve'] = 'x';
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						$json['action_2']['employee_id'] = $token;
						$json['action_2']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_2']['act'] = 'C';

						/**
						* update json_data;
						**/
						
						$json_x  =  json_encode($json,1);
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
						

					}else{
						
						if($get_already_approval[0]->approval == null){
							
							\DB::SELECT("update att_schedule_request set approver =  0, approval = 4,status_id = 4  where id = $id");
							
							/**
							* eg : $json[sup] or $json[sup]
							**/
							
							foreach($json['req_flow'] as $key  => $value){
								if($value == 1){
									$key_x = $key;
								}
							}

							$user_x = $json[$key_x];
							foreach($user_x as $key => $value){
								
								if(isset($value[$token])){
									
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'x';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}
							
				
							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}

							$json['action_1']['employee_id'] = $token;
							$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
							$json['action_1']['act'] = 'C';
							/**
							* update json_data;
							**/
							
						
							$json_x  =  json_encode($json,1);
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
						}
					}
				}else{
					
					if($get_already_approval[0]->approval == null){
						\DB::SELECT("update att_schedule_request set approver =  4, approval = 0, status_id = 4 where id = $id");
						/**
						* eg : $json[sup] or $json[sup]
						**/
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 1){
								$key_x = $key;
							}
						}

						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json['req_flow'][$key_x.'_approve'] = 'x';
								$json[$key_x][$key]['read_stat'] = 1;
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						$json['action_1']['employee_id'] = $token;
						$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_1']['act'] = 'C';

						/**
						* update json_data;
						**/
						
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update att_schedule_request set status_id = 4 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 1");
					}
				}
			}else{
				
				$id  =  $i['id'];
				$get_pool =  \DB::SELECT("select * from pool_request where id_req = $id  and master_type = 2");
				$json  =  json_decode($get_pool[0]->json_data,1);

				$flow =  $json['req_flow'];
				
				/**
				*	get time and date 
				**/
		
				$time  =  date('H:i');
				$date  = date('Y-m-d');

				/**
				* get user stat 2 or just 1 stat
				**/

				$get_step  =  0;
				$user = '';
				$temp = [];

				/**	
				*	get flow user
				**/
				foreach($flow as $key => $value){
					$temp[] = $value;		
				}

				/**	
				*check using in array target data exist
				**/

				if(in_array(2,$temp)){
					$get_step = 2;
				}else{
					$get_step = 1;
				}

				/**
				*	if 2 step approval
				*	check approval if already approved
				*   if approval already approve
				*	then approve approver
				**/
			
				$get_already_approval  = \DB::SELECT("select * from leave_request where id =  $id ");
				if($get_step == 2){
					if($get_already_approval[0]->approval != null ){
						$arr_cek =   [1,3,4];
					
						if(in_array($get_already_approval[0]->approval,$arr_cek)){
							return response()->json(['header' => ['message' => "can't approve", 'status' => 500],'data' => []],500);
						}
					    

					    \DB::SELECT("update leave_request set approver =  4 where id = $id");
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 2){
								$key_x = $key;
							}
						}

						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json['req_flow'][$key_x.'_approve'] = 'x';
								$json[$key_x][$key]['read_stat'] = 1;
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						$json['action_2']['employee_id'] = $token;
						$json['action_2']['dateTime'] = date('Y-m-d H:i:s');
						$json['action_2']['act'] = 'C';
						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update leave_request set status_id = 4 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						

					}else{
					
						if($get_already_approval[0]->approval == null){
					
							\DB::SELECT("update leave_request set approver =  0, approval = 4, status_id  =  4 where id = $id");
							/**
							* eg : $json[sup] or $json[sup]
							**/
							
							foreach($json['req_flow'] as $key  => $value){
								if($value == 1){
									$key_x = $key;
								}
							}

							$user_x = $json[$key_x];

							foreach($user_x as $key => $value){
								if(isset($value[$token])){
									$json[$key_x][$key][$token] =  1;
									$json[$key_x][$key][$key_x.'_stat'] = 1;
									$json[$key_x][$key][$key_x.'_date'] = $date;
									$json[$key_x][$key][$key_x.'_time'] = $time;
									$json['req_flow'][$key_x.'_approve'] = 'x';
									$json[$key_x][$key]['read_stat'] = 1;
								}else{
									$json[$key_x][$key]['read_stat'] = 1;
								}
							}

							foreach($json['chat_id']  as  $keyy  => $valuee){
								$json['chat_id'][$keyy] = 'r';
							}
							
							$json['action_1']['employee_id'] = $token;
							$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
							$json['action_1']['act'] = 'C';

							/**
							* update json_data;
							**/
							
							
							$json_x  =  json_encode($json,1);
							$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
						}
					}
				}else{
					
					if($get_already_approval[0]->approval == null){
						\DB::SELECT("update leave_request set approver = 4, approval = 0 where id = $id");
						/**
						* eg : $json[sup] or $json[sup]
						**/
						
						foreach($json['req_flow'] as $key  => $value){
							if($value == 1){
								$key_x = $key;
							}
						}
						
						$user_x = $json[$key_x];

						foreach($user_x as $key => $value){
							if(isset($value[$token])){
								$json[$key_x][$key][$token] =  1;
								$json[$key_x][$key][$key_x.'_stat'] = 1;
								$json[$key_x][$key][$key_x.'_date'] = $date;
								$json[$key_x][$key][$key_x.'_time'] = $time;
								$json['req_flow'][$key_x.'_approve'] = 'x';
								$json[$key_x][$key]['read_stat'] = 1;
							}else{
								$json[$key_x][$key]['read_stat'] = 1;
							}
						}

						foreach($json['chat_id']  as  $keyy  => $valuee){
							$json['chat_id'][$keyy] = 'r';
						}

						$json['action_1']['employee_id'] = $token;
						$json['action_1']['dateTime'] =   date('Y-m-d H:i:s');
						$json['action_1']['act'] = 'C';

						/**
						* update json_data;
						**/
						$json_x  =  json_encode($json,1);
						$schedule = \DB::SELECT("update leave_request set status_id = 4 where id  =  $id ");
						$get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $id  and master_type = 2");
					}
				}
			}
		}

		$set_id =  $i['id'];
		//$set_employ =  $i['employee_id'];

		$trigger  = \DB::SELECT("select * from leave_request where id  = $set_id ");

		if($trigger !=  null){

			if($trigger[0]->status_id  == 4){
				$cancle_opt  =   true;
			}else{
				$cancle_opt =  false;
			}

		

			$data_response  = [
								'Cancel' => true,
								'id' => $i['id']
							  ];

			return response()->json(['header' => ['status' => 200,  'messsage' => 'success  to cancle request'],'data' => $data_response],200);
		}
	}

}