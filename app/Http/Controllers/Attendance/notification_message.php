<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Exeception;

class notification extends Controller {		

	public function getUser($key = null){
		if($key == null){
			$key = \Input::get('key');
		}
		$keys= base64_decode($key);
		$test = explode('-',$keys);
		return $data = $test[1];
	}

	public function terms_schedule($type_request,$local_it,$emp){
		$lc = \DB::SELECT("select local_it from emp where employee_id = '$emp'");
		if($type_request == 'time'  &&  $local_it != 1){
			$schema = ['hr','sup','emp'];  
		}elseif ($type_request == 'time'  &&  $local_it == 1) {
			$schema = ['sup','hr','emp'];  
		}elseif ($type_request == 'over'  &&  $local_it != 1) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'under'  &&  $local_it != 1) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'late'  &&  $local_it != 1) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'early'  &&  $local_it != 1) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'train'  ) {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'ob') {
			$schema = ['sup','hr','emp'];
		}elseif ($type_request == 'change') {
			$emp = \DB::SELECT("select * from emp_supervisor where supervisor =  '$emp'");
			if($emp != null){
				$schema = ['emp','hr','sup'];
			}else{
				$schema = ['sup','emp','hr'];
			}
		}

		return $schema;
	}

	public function terms_leave($type_request,$local_it,$emp){
		$lc = \DB::SELECT("select local_it from emp where employee_id = '$emp'");
		$chck_department  = \DB::SELECT("select role_name  from view_nonactive_login where employee_id =  '$emp'  and (role_name like '%human resource%' or  role_name like '%resource%' ");
		
		//vacation 
		if($type_request == 'vacation'  &&  $local_it != 1){
			$schema = ['hr','sup','emp'];
			$end  =  'sup';  
		}elseif ($type_request == 'vacation'  &&  $local_it == 1) {
			$schema = ['sup','hr','emp'];
			$end =  'sup';
		//ADO	
		}elseif ($type_request == 'ado'  &&  $local_it != 1) {
			$schema = ['hr','sup','emp'];
			$end =  'sup';

		//Enhance
		}elseif ($type_request == 'enhance'  &&  $local_it != 1) {
			$schema = ['hr','sup','emp'];
			$end =  'sup';

		//Sick
		}elseif ($type_request == 'sick'  &&  $local_it != 1 and $chck_department != null) {
			$schema = ['sup','hr','emp'];
			$end = 'sup';
		}elseif ($type_request == 'sick'  &&  $local_it != 1 and $chck_department == null) {
			$schema = ['hr','sup','emp'];
			$end  = 'sup';
		}elseif ($type_request == 'sick'  &&  $local_it == 1) {
			$schema = ['sup','hr','emp'];
			$end  = 'hr';
		}

		// maternity
		}elseif ($type_request == 'maternity'  &&  $local_it != 1  and $chck_department  != null) {
			$schema = ['hr','sup','emp'];
			$end  = 'hr';
		}elseif ($type_request == 'maternity'  &&  $local_it == 1) {
			$schema = ['sup','hr','emp'];
			$end  = 'sup';
		}		

		//paternity
		}elseif ($type_request == 'paternity'  &&  $local_it != 1) {
			$schema = ['sup','hr','emp'];
			$end  = 'hr';
		}elseif ($type_request == 'paternity'  &&  $local_it == 1) {
			$schema = ['sup','hr','emp'];
			$end  = 'sup';
		}


		//bereavement
		}elseif ($type_request == 'bereavement'  &&  $local_it != 1 and $chck_department != null) {
			$schema = ['sup','hr','emp'];
			$end  = 'sup';
		}elseif ($type_request == 'bereavement'  &&  $local_it != 1) {
			$schema = ['sup','hr','emp'];
			$end  = 'hr';
		}elseif ($type_request == 'bereavement'  &&  $local_it == 1) {
			$schema = ['sup','hr','emp'];
			$end  = 'sup';
		}

		//marriage
		}elseif ($type_request == 'marriage'  &&  $local_it != 1) {
			$schema = ['sup','hr','emp'];
			$end  = 'hr';
		}elseif ($type_request == 'marriage'  &&  $local_it == 1) {
			$schema = ['sup','hr','emp'];
			$end  = 'sup';
		}

		//emerge
		}elseif ($type_request == 'emergency'  &&  $local_it != 1 & $chck_department != null) {
			$schema = ['sup','hr','emp'];
			$end  = 'hr';		
		}elseif ($type_request == 'emergency'  &&  $local_it == 1) {
			$schema = ['sup','hr','emp'];
			$end  = 'sup';
		}elseif ($type_request == 'emergency'  &&  $local_it != 1) {
			$schema = ['sup','hr','emp'];
			$end  = ' hr';
		}

		//suspension
		}elseif ($type_request == 'suspension'  &&  $local_it != 1) {
			$schema = ['sup','hr','emp'];
			$end  = ' hr';
		}


	}

	//check requestor
	public function chk_requestor($requestor){
		$emp  =   \DB:SELECT("select * from emp,department  where  emp.department   =  department.id  and employee_id =  '$requestor' ");

		return $data  = ['sub_employee' => $emp[0]->local_it, 'department' => $emp[0]->name];
	}

	publc function whoiam($requestor){

		$supervisor =   \DB::SELECT("select * from emp_supervisor where supervisor = '$requestor' ");
		$view_login  =   \DB::SELECT("select * from view_nonactive_login where employee_id =  '$requestor' and  (role_name like '%Human Resource%' 							  or role_name like '%Human%')");
		$superuser   = \Db:SELECT("select *  from emp where employee_id =  '2014888'  and  ad_username =  'superuser' ");

		if(!isset($supervisor)  && !isset($view_login) && !isset($superuser)){
			return  1;
		}else{ return  0; }
	}

	public function calculcate_notif(){
		$leave         = ['Birthday Leave', 'Vacation Leave','Enhance  Vacation Leave','Sick Leave','Maternity Leave','Paternity Leave',
						   'Bereavement leave', 'Marriage Leave','offday_oncall','Accumulation Day Off','Emergency Leave','Supension'];

		$schedule      = ['Training','Official Business','Overtime','Undertime','Change Shift','Swap Shift / Day Off','Late','Early Out','Time-In / 			   Time-Out'];

 
		$type          = "vacation";
		$dob           = 1;
		$from          = "2017-06-11";
		$includeAdo    = 'No';
		$to            = "2017-06-12";
		$agggred       = 1;
		$comment       = 'test';
		$name          = '2017999';
		$remaining_day = 2;
		$requestor     = $this->getUser();

		//check master branch
		if(in_array($type,$leave)){
			$table = "att_schedule_request";
		}

		if(in_array($type,$schedule)){
			$table = "leave_request";
		}

		if(!isset($table)){
			$message =  "request not found";
			$data  = [];
			$status = 500;
		}else{		
			$chk_requestor = $this->chck_department($requestor);

		    if($table  ==   "leave_request"){
		   		$chk_whoiam  =  $this->whoiam($requestor);
		   		if($this->whoiam ==  1){
		   			$message = 'not allowed to request';
		   			$status   =  500;
		   			$data  = [];
		   		}else{
		    		return $portion  = $this->terms_leave($type,$chk_requestor[0]->sub_employee,$requestor);

		    	}
		    }
		}


		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function count_notif(){

	}

}