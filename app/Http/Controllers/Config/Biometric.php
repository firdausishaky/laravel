<?php namespace Larasite\Http\Controllers\Config;
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Model\Config\Biometric_model;
class Biometric extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	/*	method = get / index data
		hrms.dev/api/timekeeping/biometric-setup/setup?key=VXdRUlhXMjJoSkRQSndKeEdmZ2FDa3djZVlXMEd6eE41dW84eERrai0yMDE0ODg4
		method = delete /delete data
		hrms.dev/api/timekeeping/biometric-setup/setup/7?key=VXdRUlhXMjJoSkRQSndKeEdmZ2FDa3djZVlXMEd6eE41dW84eERrai0yMDE0ODg4
		method = post /update data
		hrms.dev/api/timekeeping/biometric-setup/update/8?key=VXdRUlhXMjJoSkRQSndKeEdmZ2FDa3djZVlXMEd6eE41dW84eERrai0yMDE0ODg4&names=rtg&name=Studio&number=83
		parameter[names = "test1", name = "studio", number = 83 ]
		method = post /insert data 
		hrms.dev/api/timekeeping/biometric-setup/store?key=VXdRUlhXMjJoSkRQSndKeEdmZ2FDa3djZVlXMEd6eE41dW84eERrai0yMDE0ODg4&names=test&name=Studio&number=9
		parameter[names = "test1", name = "studio", number = 83 ]	

	*/

	private	$rule = array(
					'device_location' => 'required| Regex:/^[a-zA-Z ]+$/',
					'device_number' => 'required| integer',
					'device_name' => 'required|Regex:/^[a-zA-Z0-9]+$/',
					);

	function getMessage($message = array(), $data = array(), $status_code=""){
		return response()->json(['header' =>['message' => $message, 'status' => $status_code], 'data' => $data],$status_code);
	}

	
	public function store()
	{
		//created 04 january 2015;
		$input = \Input::all();
		$device_number = \Input::get('device_number');
		$device_location = \Input::get('device_location');
		$device_name = \Input::get('device_name');
       		$validation = \Validator::make($input,$this->rule);
		if($validation->fails()){
			$message = $validation->errors()->all();
			$db = null;
			$status_code = 500;
			return $this->getMessage($message,$db,$status_code);
		}else{
				$check = \DB::SELECT(" select * from biometrics_location where device_number = $device_number  and  device_location='$device_location' and device_name='$device_name' ");
				if($check != null){
					return $this->getMessage('Device device_number already exist ',null,200);
				}else{
					$db = \DB::SELECT("CALL Insert_biometrics_Device_location('$device_name',$device_number,'$device_location')");
					if(isset($db)){
						$query = \DB::SELECT("select * from biometrics_location where device_number=$device_number");
				
						foreach($query as $key => $value){
					
						$data =[
							 'device_location' => $value->device_location,
							'device_name' => $value->device_name,
							'device_number' => $value->device_number
							];
						}
						return $this->getMessage('success',$data,200);
						break;
					}else{
						return $this->getMessage('failed',null,500);
						break;
					}
				}
			}
	}

	public function update_ex($id)
	{
		//created 04 january 2015
		$input = \Input::all();
		$device_number = \Input::get('device_number');
		$device_location = \Input::get('device_location');
		$device_name = \Input::get('device_name');
       		$validation = \Validator::make($input,$this->rule);
		if($validation->fails()){
			return  var_dump($validation->fails());
			$message = $validation->errors()->all();
			$db = null;
			$status_code = 500;
			return $this->getMessage($message,$db,$status_code);
		}else{
	
			$check = \DB::SELECT(" select * from biometrics_location where device_number = $device_number  and  device_location='$device_location' and device_name='$device_name' ");
				if($check != null){
					return  var_dump("step2");
					return $this->getMessage('Device device_number already exist ',null,200);
				}else{
					//return  var_dump("3");
				//$db = \DB::SELECT("CALL update_biometrics_location($id,'$device_location','$device_name')");
				$db = \DB::SELECT("update biometrics_location set `device_location`='$device_location', `device_name`='$device_name' where `device_number`=$id");
				if(isset($db)){
					//return  var_dump("4");
					$query = \DB::SELECT("select * from biometrics_location where device_number=$id");
					return $this->getMessage('success',$query[0],200);
				}
			}
		}
	}

	public function destroy_ex($id)
	{
		
		if($id != null){
			$explode_id = explode(",",$id);	
			$count = count($explode_id)-1;
			$i = 0;
			for($i; $i <= $count; $i++){
				$data = $explode_id[$i];
				$db = \DB::SELECT("CALL delete_biometrics_localtion($data)");
			}
				return $this->getMessage("success",[],200);
			
		 }
	}

	

}
