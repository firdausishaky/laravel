<?php namespace Larasite\Http\Controllers\Config;
set_time_limit(0);
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use  Log;
use PDO;
use Larasite\Library\FuncAccess;
//use Larasite\Model\Config\Biometric_model;
class Biometric_inb extends Controller {

    protected $form =  50;
     private $b_import = array();
          private $b_backup = array();
          private $b_time   = array();
          private $connectionId;
          private $loginOk = false;
          private $messageArray = array();

         function __construct(){
            if($this->b_time == null){
              $imports = 'Biometrics/import_Biometrics.json';
              $backups = 'Biometrics/backup_Biometrics.json';
              $times   = 'Biometrics/time_Biometrics.json';


              $backup = json_decode(file_get_contents(storage_path($backups)),1);
              $import = json_decode(file_get_contents(storage_path($imports)),1);
              $time   = json_decode(file_get_contents(storage_path($times)),1);

              if(file_exists(storage_path($imports))){
                  foreach ($import as $key ) {
                      $this->b_import = $key;
                  }
              }
              if(file_exists(storage_path($backups))){
                  foreach ($backup as $key ) {
                      $this->b_backup = $key;
                  }
              }
 
                      $this->b_time = $time;
                
            }
          }

          function index($get = null){
            /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
          if($access[1] == 200){
               $biometric_setup = \DB::SELECT("CALL View_device_location");
                $check_data =  \DB::SELECT("select * from authen_biometric");
                $authen_biometric = [ "time_in" => $check_data[0]->time_in , "time_out" => $check_data[0]->time_out, 'biometric_A' => $check_data[0]->authen_type ];
                if($this->b_import == null){$this->b_import = [];}
                if($this->b_backup == null){$this->b_backup = [];}
                if($this->b_time == null){$this->b_time = [];}
                $data = [
                          'import' => $this->b_import,
                          'backup' => $this->b_backup,
                          'sub_time' => $this->b_time,
                          'biometric_setup' => $biometric_setup,
                           "authen_biometric" => $authen_biometric,
                         ];
                  if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
                      return $this->getMessage("Unauthorized",[], 200, $access[3]);
                  }else{     
                    if(isset($data)){
                          if($get != null){
                              return $data;
                          }else{
                            return $this->getMessage('success',$data,200,$access[3]);
                          }
                    } else{
                      return $this->getMessage('failed',null,500,$access[3]);
                    }
                  }
              }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
               return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
            }
          }

          function test_authen(){
               /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
               if($access[1] == 200){
                  $in  =  \Input::get('time_in');
                  $out  = \Input::get('time_out');
                   $type   =   \Input::get('biometric_A');
                  if(isset($type) and $type  !=  null){
                    $type   =   \Input::get('biometric_A');
                  }else{
                    $type  = 'A';
                  } 
                  $tmp = strpos($in,"0");
                  if($tmp){                  
                    if($in == 0 && $out ==  0 || $in > 1 || $out > 1 || $in == 1 && $out ==  1){
                        $message  = "please set up one of them must be time in and out with option 0 or 1";
                        $data = [];
                        $status = 500;
                    }else{
                        $check =  \DB::SELECT("select time_in, time_out from authen_biometric");
                        $update  = \Db::SELECT("update authen_biometric set time_in =  $in , time_out = $out,   authen_type  = '$type' ");

                        $message  = "Success apply changed data";
                        $data = $this->index(1);
                        $status = 200;
                     }
                  }else{
                        $check =  \DB::SELECT("select time_in, time_out from authen_biometric");
                        $update  = \Db::SELECT("update authen_biometric set time_in =  '$in' , time_out = '$out',   authen_type  = '$type' ");

                        $message  = "Success apply changed data";
                        $data = $this->index(1);
                        $status = 200;
                  }

               }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];}
                return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
          }



          function  test_ftp(){

              //parameter for import server
              $ftp_server = 'IT-09.inovasisolusi.com';
              $ftp_username = 'root';
              $ftp_passowrd = 1234;
              $ftp_shared  = 'stephen';


              //parameter for backup server
              $ftp_serverUp = 'IT-09.inovasisolusi.com';
              $ftp_usernameUp = 'root';
              $ftp_passowrdUp = 1234;
              $ftp_sharedUp  = 'sanjaya';
              $header = array();
              $convert = 0;
              $confirm = false;

              //create connection  ftp and login
              $conn_id = ftp_connect($ftp_server);
              $login_result = ftp_login($conn_id,$ftp_username,$ftp_passowrd);

              //move directory ftp from parent directory to specified folder
               if(ftp_chdir($conn_id,$ftp_shared)){

                     // get name file from ftp
                    $connect = ftp_nlist($conn_id, ".");
                    $total = strlen($connect[0]);
                     $name_victim = substr($connect[0], 2, $total-1);

                    //get file from local file
                    $name_file   = fopen(storage_path("old.biometric/$name_victim"),'w');

                    // download file from ftp
                    if(ftp_fget($conn_id,$name_file,$name_victim, FTP_ASCII)){
                          //convert from txt format to json format
                         $header= $this->convert_to_txt();
                          //function for delete file from import server
                          if(ftp_delete($conn_id, $name_victim)){
                              //close connection  ftp
                              fclose($name_file);
                                  $convert = 1;
                         }else{ return " can't delete file from import server"; }
                   }else{ return "failed to get data from import server ";}
               }else{ return  "can't change directory path ftp import server";}

             if($convert == 1){
                            //create connection  for backup server
                           $conn_backUp = ftp_connect($ftp_serverUp);
                           $login_resultUp = ftp_login($conn_backUp,$ftp_usernameUp,$ftp_passowrdUp);

                           //get file name from local storage
                            $scandir = scandir(storage_path('old.biometric'));
                            $local_scandir =  $scandir[2];
                            $fp_local =  fopen(storage_path("old.biometric")."/".$scandir[2],'rb');

                            //move directory ftp from parent directory to specified folder
                            if(ftp_chdir($conn_backUp,$ftp_sharedUp)){
                                    //upload file local file to remote file
                                    if(@ftp_fput($conn_backUp,$local_scandir , $fp_local , FTP_ASCII)){
                                            //close connection
                                            ftp_close($conn_backUp);
                                            fclose($fp_local);
                                                $confirm = true;
                                   }else{ return " can't move data to server backup";}
                           }else{ return "can't change directory ftp";}

                           //delete local file
                           if($confirm == true){
                                if(unlink(storage_path("old.biometric")."/".$scandir[2])){
                                      return $header;
                                }else{ return "can't return data";}
                            }
                    }
          }


                                                 //}
                 //   }else{return "can't generate data, something missing";}
                    //$fpx = fopen("$c[0]",'w+');
                    //$fwrite = fwrite($fpx, $data_ftp,strlen($data_ftp));
                    //fclose($fpx);
                    //if($fwrite){
            // $a = storage_path("old.biometric")."/".$local_scandir;
            // $b = storage_path("biometric")."/".$local_scandir;
            // \Storage::disk('local')->move($a,$b);
            //\File::move(storage_path("old.biometric/$local_scandir"),storage_path("biometrics/$local_scandir"));
                                            // move_uploaded_file(path, new_path)
                                          // if(ftp_site($conn_backUp,"CHMOD 0777 /$ftp_sharedUp/")){
                                          //  $fpx = fopen($local_scandir,'w');
                                          //  fwrite($fp_create, $data_ftp,strlen($data_ftp));
                                          //   fclose($fp_create);
                                          //  return $header;

          function insert_setting(){
                /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
          if($access[1] == 200){
                $input = \Input::all();
                $import_json = \Input::json('import');
                $backup_json = \Input::json('backup');
                $time_json = \Input::json('sub_time');

                          if($import_json == []){
                              return $this->getMessage('please insert form fill before submit data',[],500);
                          }
                          if($backup_json == []){
                             return $this->getMessage('please insert form fill before submit data',[],500);
                          }
                          if($time_json == []){
                            return $this->getMessage('please insert form fill before submit data',[],500);
                          }


                         $validation = \Validator::make(
                                     ['server_imp'    =>  $import_json['server_imp']],
                                     ['username_imp'  => $import_json['username_imp']],
                                     ['password_imp'  => $import_json['password_imp']],
                                     ['share_imp'     => $import_json['share_imp']],
                                     ['domain_imp'    => $import_json['domain_imp']],
                                     ['server_up'     => $backup_json['server_up']],
                                     ['username_up'   => $backup_json['username_up']],
                                     ['password_up'   => $backup_json['password_up']],
                                     ['share_up'      => $backup_json['share_up']],
                                     ['domain_up'     => $backup_json['domain_up']],

                                     ['server_imp'    => 'Required|ip'],
                                     ['username_imp'  => 'Required|Regex :/^[A-Za-z0-9]/'],
                                     ['password_imp'  => 'Required|Regex :/[A-Za-z0-9 ]/'],
                                     ['share_imp'     => 'Required|Regex :/[A-Za-z0-9 ]/'],
                                     ['domain_imp'    => 'Required|Regex :/[A-Za-z0-9 ]/'],
                                     ['server_up'     => 'Required|ip'],
                                     ['username_up'   => 'Required|Regex :/[A-Za-z0-9 ]/'],
                                     ['password_up'   => 'Required|Regex :/[A-Za-z0-9 ]/'],
                                     ['share_up'      => 'Required|Regex :/[A-Za-z0-9 ]/'],
                                     ['domain_up'     => 'Required|Regex :/[A-Za-z0-9 ]/']
                                   );
       
                         if($validation->fails()){
                             $check = $validation->errors()->all();
                             return $this->getMessage('invalidate format input',$check,500);
                         }else{
                                 $importer = [
                                         "import" => [
                                           'domain_imp'    =>  $import_json['domain_imp'],
                                           'username_imp'  => $import_json['username_imp'],
                                           'password_imp'  => $import_json['password_imp'],
                                           'share_imp'     => $import_json['share_imp'],
                                           'server_imp'    => $import_json['server_imp']
                                               ],
                                         ];
                                 $backuper = [
                                         "backup" => [
                                                'domain_up'     => $backup_json['domain_up'],
                                                 'password_up'   => $backup_json['password_up'],
                                                 'server_up'   => $backup_json['server_up'],
                                                 'share_up'      => $backup_json['share_up'],
                                                 'username_up'     => $backup_json['username_up']
                                               ]
                                         ];

                                 $time =  $time_json;


                                  // return $importer;
                                  if(isset($importer)){
                                     $fileimport = 'Biometrics/import_Biometrics.json';
                                     $fpimport = fopen(storage_path($fileimport),'w');
                                     fwrite($fpimport, json_encode($importer));
                                     fclose($fpimport);
                                  }
                                  if(isset($backuper)){
                                    $filebackup = 'Biometrics/backup_Biometrics.json';
                                    $fpbackup = fopen(storage_path($filebackup),'w');
                                    fwrite($fpbackup, json_encode($backuper));
                                    fclose($fpbackup);
                                  }
                                  if(isset($time)){
                                    $filename = 'Biometrics/time_Biometrics.json';
                                    $fp = fopen(storage_path($filename),'w');
                                    fwrite($fp, json_encode($time));
                                    fclose($fp);
                                  }
                                  if(isset($importer) and isset($backuper) and isset($time)){
                                     $result = [$importer,$backuper,$time];
                                     return $this->getMessage("success",$result,200,$access[3]);
                                  }
                        }
              }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
               return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
            }
          }

      //
           public function convert_to_txt(){
                  $file_name= scandir(storage_path("old.biometric"));
                  $biometrics = $file_name[2];
                  $csv = array();
                  $handle = fopen(storage_path("old.biometric/$biometrics"),'rb');
                  $l = 0;
                  $handle1 = array();
                  $list = array();
                  if ($handle )
                      {
                          while($data = fgets($handle,1000))
                          {
                              // $i = 0;
                             $csv[]= preg_replace('/\s/',',',$data);
                             $handle1[$l] = explode(",",$csv[$l]);


                             // $device_numbers[] = $handle1[$l][3];
                             //  $dates[] = $handle1[$l][1];
                             //  $times[] =$handle1[$l][2];
                             $emps[] = $handle1[$l][0];
                             //  $authens[] = $handle1[$l][4];

                             //  $datas[] = [$handle1[$l][0],$handle1[$l][2],$handle1[$l][1],$handle1[$l][4]];

                              // if($date==$date && $authen ==  0 &&  ){
                              //       $date_array[] = $date;
                              //       return $date_array;
                              //       $test = \DB::SELECT("CALL Insert_Biometrics($device_number,'$time',null,'$emp',$authen,'$date')");
                              // }else{
                              //     $test = \DB::SELECT("CALL Insert_Biometrics($device_number,null,'$time','$emp',$authen,'$date')");
                              // }
                              $l += 1;
                          }

                         $temp_array = array();
                        $i = 0;
                        $key_array = array();
                        $in_array = array();

                        foreach($handle1 as $val) {
                            if (!in_array($val[0], $key_array)) {
                                $key_array[$i] = $val[0];
                                $temp_array[$i] = $val;
                            }
                            $i++;
                        }

                       //php return $temp_array[0][2];
                        $count =  count($handle1);
                        //return $count;
                        $c_data = count($emps);

                        $in_array = [];

                          for ($k = 0; $k < $c_data;$k++){
                                for($j = 0; $j < $count; $j++){
                                          if($handle1[$j][0]  == $temp_array[$k][0]){
                                                 $in_array[] = $handle1[$j];
                                          }
                                }
                          }

                          return $in_array;


                            for($i = 0; $i<$count-1; $i++){

                                    $device_number = $handle1[$i][3];
                                    $date = (string)$handle1[$i][1];
                                    $time =(string)$handle1[$i][2];
                                    $emp = (string)$handle1[$i][0];
                                    $authen = $handle1[$i][4];

                                    if($authen ==  0 ){
                                          $test = \DB::SELECT("CALL Insert_Biometrics($device_number,'$time',null,'$emp',$authen,'$date')");
                                    }else{
                                        $test = \DB::SELECT("CALL Insert_Biometrics($device_number,null,'$time','$emp',$authen,'$date')");
                                    }
                              }
                          }
                }

          function delete(){
                      $importer = [];
                      $backuper = [];
                      $time = [];

                       if(isset($importer)){
                                     $fileimport = 'Biometrics/import_Biometrics.json';
                                     $fpimport = fopen(storage_path($fileimport),'w');
                                     fwrite($fpimport, json_encode($importer));
                                     fclose($fpimport);
                                  }
                                  if(isset($backuper)){
                                    $filebackup = 'Biometrics/backup_Biometrics.json';
                                    $fpbackup = fopen(storage_path($filebackup),'w');
                                    fwrite($fpbackup, json_encode($backuper));
                                    fclose($fpbackup);
                                  }
                                  if(isset($time)){
                                    $filename = 'Biometrics/time_Biometrics.json';
                                    $fp = fopen(storage_path($filename),'w');
                                    fwrite($fp, json_encode($time));
                                    fclose($fp);
                                  }
                                  if(isset($importer) and isset($backuper) and isset($time)){
                                     $result = [$importer,$backuper,$time];
                                     return $this->getMessage("success",$result,200);
                                  }
          }

          function test(){

                  //start open and read json file setting ftp
                  $imports = 'Biometrics/import_Biometrics.json';
                  $backups = 'Biometrics/backup_Biometrics.json';
                  $times   = 'Biometrics/time_Biometrics.json';

                  $backup = json_decode(file_get_contents(storage_path($backups)),1);
                  $import = json_decode(file_get_contents(storage_path($imports)),1);
                  $time   = json_decode(file_get_contents(storage_path($times)),1);
                // return $import;

                  $import_server = "";
                  $import_username = "";
                  $import_password = "";
                  $import_share = "";
                  $import_domain = "";

                  $backup_server = "";
                  $backup_username = "";
                  $backup_password = "";
                  $backup_share = "";
                  $backup_domain = "";
                  //end open and rean ajson file ftp

                  // start check file already exist or not , and read than save value in a variable
                  if(file_exists(storage_path($imports)) == true and file_exists(storage_path($backups)) == true and file_exists(storage_path($times)) == true){
                     if($import != null){
                          foreach($import as $data){
                            $import_server = $data['server-import'];
                            $import_username = $data['server-username'];
                            $import_password = $data['password'];
                            $import_share = $data['share'];
                            $import_domain = $data['domain'];
                          }
                     }

                     if($backup != null){
                          foreach($backup as $data){
                            $backup_server = $data['server-backup'];
                            $backup_username = $data['server-username'];
                            $backup_password = $data['password'];
                            $backup_share = $data['share'];
                            $backup_domain = $data['domain'];
                          }
                     }
                  // end start check file already exist or not , and read than save value in a variable
                     if($import != null){
                          $dir = $import_share;
                          $ftp_file = scandir($dir,1);

                          //set up connection with file transfer protocol
                          $conn_id = ftp_connect($import_server);

                          //login to file transfer protocol
                          if(@ftp_login($conn_id,$import_username,$import_password)){
                              if(ftp_get($conn_id,storage_path($ftp_file),$import_share.$ftp_file,$import_server,FTP_ASCII)){
                                  if(file_exist(storage_path($ftp_file))){
                                      if(ftp_delete($conn_id,$import_share.$ftp_file)){
                                          return "success";
                                      }
                                  }
                              }
                          }else{
                              $this->getMessage("failed to make connection with ftp, please try again", null,500);
                          }



                     }
              }
          }


          function getMessage($message = array(), $data=array(), $status = "", $access = array()){
              return response()->json(['header' => ['message' => $message, 'status' => $status, "access" => $access], 'data' => $data],$status);
          }

      public  function db_query(){
                  $data = \DB::SELECT("CALL view_leave_vacation( 2, 1 )");

                // search child
                foreach ($data  as $key => $value) {
                  $subdata =  ["year" => $value->year, "days" => $value->days, "offday_oncall" => $value->offday_oncall];
                          $datax[$value->id][]= $subdata;
                }

                //search parent
                  foreach ($data  as $key => $value) {
                  $subdata =  ["id" => $value->id, "LeaveRules" => $value->LeaveRules];
                          $datay[$value->id][]= $subdata;
                }

                //merge parent and child for happy life
                foreach ($datax as $key => $value) {
                          $explode = explode(".",$datay[$key][0]['LeaveRules']);
                          $applied = $explode[0];
                          $applied_ex = substr($applied, -1);
                              if($applied_ex == "1"){
                                $applied = "Applied for :Expat";
                              }
                              if($applied_ex ==  "2"){
                                $applied = "Applied for :Local";
                              }
                          $department = $explode[1];
                          $ent_day = $explode[2];
                          $ent_start = $explode[3];
                          $dataz[]  = ["id" => $datay[$key][0]['id'], "Applied for :" => $applied, "Department" => $department, "Entitlementdays :" => $ent_day, "StartFrom :" => $ent_start,  "subdata" => $datax[$key] ];
                }

                return $dataz;

        }
}
