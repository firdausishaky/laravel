<?php namespace Larasite\Http\Controllers\Config;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use adLDAP\adLDAP;
/* Model */
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;

class Ldap extends Controller {

protected $form = 8;
protected $dir = 'config/ldap-setting.json';

private function check_id()
{
	$rule = ['undefined',NULL,''];
	if(in_array($rule,$id)){ $data = ['message'=>'ID Undefined.','status'=>500,'data'=>null];  }
	else{ $data = ['message'=>'ID Valid.','status'=>200,'data'=>null]; } return $data;
}
private function set_valid()
{
	$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text_only'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
	$rule = [
				'host' =>$reg['text_num'],
				'port' =>'numeric',
				'encryption' =>'alpha',
				'from' =>  ['address'=>'mail','name'=>'min:3'],
				'to'=>'mail',
				'sent'=>'mail',
				'username'=>'mail',
				'password'=>$reg['text_num'],
				'auth'=>'alpha'
			];
	$valid =  \Validator::make($this->set_input(true),$rule); return $valid;
}

 	private function ldapData($datax,$type){
 		if($type){
 			if($datax['ldap_protocol'] == 'ldaps'){
 				$inputPost['use_ssl']  = true;
 			}else{
 				$inputPost['use_ssl']  = false;
 			}
 			$inputPost['domain_controllers']  = [$datax['ldap_domain']];
 			$inputPost['ad_port']  = $datax['ldap_port'];
 			$inputPost['ldap_protocol']  = $datax['ldap_protocol'];
 			$inputPost['account_suffix']  = $datax['ldap_account_suffix'];
 			$inputPost['base_dn']  = $datax['ldap_basedn'];
 			$inputPost['admin_username']  = $datax['ldap_username'];
 			$inputPost['admin_password']  = $datax['ldap_password'];
 			$inputPost['use_tls']  = $datax['ldap_tls'];
 			$inputPost['sso']  = false;
 			$inputPost['recursive_groups']  = true;
 			$inputPost['real_primarygroup']  = true;
 			return $inputPost;
 		}else{
	 		if($datax['ldap_ssl']){ $datax['ldap_protocol'] = 'ldaps'; }
	 		else{ $datax['ldap_protocol'] = 'ldap';
	 		return [
				   	"ldap_domain" 			=> $datax['ldap_domain'],
				    "ldap_port" 			=> $datax['ldap_port'],
				    "ldap_protocol"			=> $datax['ldap_protocol'],
				    "ldap_basedn" 			=> $datax['ldap_basedn'],
				    "ldap_account_suffix" 	=> $datax['ldap_account_suffix'],
				    "ldap_username"			=> $datax['ldap_username'],
				    "ldap_password" 		=> $datax['ldap_password'],
				    "ldap_ssl" 				=> $datax['ldap_ssl'],
				    "ldap_tls"				=> $datax['ldap_tls'],
				    "ldap_sso"				=> $datax['ldap_sso'],
				    "ldap_recursive_group"	=> $datax['ldap_recursive_group'],
				    "ldap_primary_group"	=> $datax['ldap_primary_group']
				];
 		}
 	}
}
 	private function jsonEnc($value) {
 		if($value){ $data = json_decode(file_get_contents('php://input'),1); }
 		else{ $data = json_decode(file_get_contents(storage_path($this->dir)),1);  }
 		return $data;
 	}

 	private function set_input($type)
 	{
 		$get = $this->jsonEnc(true); $datax = $this->jsonEnc(false);

 		if($get && $type){
 			return $this->ldapData($get,true);
 			// $EncryptPasswd = $this->EncryptPasswd();
 			// return [$EncryptPasswd[0],$EncryptPasswd[1],$EncryptPasswd[2]];
 		}else{
 			$get2 = $this->ldapData($datax,false);
 			foreach ($get2 as $key => $value) {
 				if($key == 'admin_password' ){ $data[$key] = base64_encode($value);	}
 				else{$data[$key] = $value;	}
 			}
 			return $data;
 		}
 	}


	// INDEX
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$message = 'Ldap : Show Configuration.'; $status = 200; $data = $this->set_input(false);
			
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// STORE
	public function store()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
		if($access[1] == 200){
			$valid = $this->set_valid();
			if($valid->fails()){ $message='Required Input Failed'; $status=500; $data=NULL; }
			else{
				$input = $this->set_input(true);
				if($this->test($input[2])){
					$fopen = fopen(storage_path($this->dir),'w');
					fwrite($fopen,json_encode($input[2])); fclose($fopen);
					$message = 'Success, Ldap connection.'; $status=200; $data =  $this->set_input(false);
				}else{
					$message = 'Failed, Please check configuration ldap.'; $status=200; $data = NULL;
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}


		private function EncryptPasswd($array)
	{
		$input = $this->jsonEnc(false);
		$password = base64_decode($array['admin_password']);
		if(isset($input['admin_password']) && $input['ldap_password'] == $password ){
			$array['admin_password'] = $password; $data = ['OK',200,$array];
		}
		else{$data = [NULL,404,$array]; }
		return $data;
	}

	// UPDATE
	public function update($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*ldap*/
		if($access[1] == 200){
			$input = $this->set_input(true);
				$ldap = new adLDAP($input);
				if($ldap->getLastError()){
					$fopen = fopen(storage_path($this->dir),'w');
					fwrite($fopen,json_encode($input)); fclose($fopen);
					$message = 'Connection Success.'; $status=200; $data =  $this->set_input(false);

				}else{ $message = 'Connection failed, Please check LDAP configuration.'; $status=200; $data = NULL; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}


	// DEL ##########################################################################################
	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete');
		if($access[1] == 200){
			$message='Page Not Found.'; $status=404; $data=NULL;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	} // END DELETE
	// SHOW
	public function show($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$message='Page Not Found.'; $status=404; $data=NULL;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// EDIT
	public function edit($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$message='Page Not Found.'; $status=404; $data=NULL;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
}
