<?php namespace Larasite\Http\Controllers\Config;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;

class Mail extends Controller {

protected $form = 8;

private function check_id()
{
	$rule = ['undefined',NULL,''];
	if(in_array($rule,$id)){ $data = ['message'=>'ID Undefined.','status'=>500,'data'=>null];  }
	else{ $data = ['message'=>'ID Valid.','status'=>200,'data'=>null]; } return $data;
}
private function set_valid()
{
	$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text_only'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
	$rule = [
				'host' =>$reg['text_num'],
				'port' =>'numeric',
				'encryption' =>'alpha',
				'from' =>  ['address'=>'mail','name'=>'min:3'],
				'to'=>'mail',
				'sent'=>'mail',
				'username'=>'mail',
				'password'=>$reg['text_num'],
				'auth'=>'alpha'
			];
	$valid =  \Validator::make($this->set_input(),$rule); return $valid;
}
 	private function set_input()
 	{
 		$data = array();
 		$get = json_decode(file_get_contents('php://input'),1);
 		if($get){	
 			$data = $get;
 		}else{
 			$data = json_decode(file_get_contents(storage_path('/config/mail-setting.json')),1);
 		}
 		//$data = json_decode(file_get_contents("php://input"));
 		return [
 			'driver'=>$data['driver'],
			'host' => $data['host'],
			'port' => $data['port'],
			'encryption' => $data['encryption'],
			'from'=>[
				'address'=>$data['from']['address'],
				'name'=>$data['from']['name'],
			],
			'username'=>$data['username'],
			'password'=>$data['password'],
			'auth' => $data['auth'],
			'sent' => $data['sent'],
			'to'=>$data['to']
		];
 	}


	// INDEX
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ //$model = new Qualification_Model;
		if($access[1] == 200){
			$message = 'Mail : Show Records Data.'; $status = 200; $data = $this->set_input();
			//else{ $message = 'Mail : Empty Records Data.'; $status = 500; $data = NULL; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// STORE
	public function store()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); 
		if($access[1] == 200){
			$valid = $this->set_valid();
			if($valid->fails()){ $message='Required Input Failed'; $status=500; $data=NULL; }
			else{
				$set = \Config::set('mail',$this->set_input());
				$fopen = fopen(storage_path('/config/mail-setting.json'),'w');
				fwrite($fopen,json_encode($this->set_input())); fclose($fopen);

				$message = 'Store Successfully.'; $status=200; $data =  \Config::get('mail');
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// SHOW
	public function show($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$message='Page Not Found.'; $status=404; $data=NULL;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// EDIT
	public function edit($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); 
		if($access[1] == 200){
			$message='Page Not Found.'; $status=404; $data=NULL;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// UPDATE
	public function update($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update');
		if($access[1] == 200){
			$input = $this->set_input();
			\Config::set('mail',$input);
			$fopen = fopen(storage_path('/config/mail-setting.json'),'w');
			fwrite($fopen,json_encode($input)); fclose($fopen);
			if($this->test() != 0 ){
				$message = 'Success, Please Check mail inbox.'; $status=200; $data =  $this->set_input();	
			}else{
				$message = 'Failed, Please Check your mail.'; $status=200; $data =  $this->set_input();	
			}
			
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);	
	}

	// DEL
	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete'); 
		if($access[1] == 200){
			$message='Page Not Found.'; $status=404; $data=NULL;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	} // END DELETE

	private function user()
	{
		return[
			'to'=>'agusmemons90@gmail.com',
			'name'=>'agus',
			'subject'=>'welcome',
			'from_mail'=>'noreply@localhost',
			'from_msg'=>'Do Not Reply'
		];
	}
	public function test(){
		$user = $this->user();
		\Config::set('mail',$this->set_input());
		return \Mail::send('emails.test', ['msg'=>'test bro'], function($message) use ($user){
	    	$message->to($user['to'],$user['name'])->subject($user['subject']);
			$message->from($user['from_mail'], $user['from_msg']);   
		});
	}
}
