<?php namespace Larasite\Http\Controllers\DocumentTemplate;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Library\FuncAccess;
use Larasite\Privilege;
use Larasite\Model\DocumentTemplate_Model;

class DocumentTemplate_Ctrl extends Controller {

protected $form = 16;
	
	public function Get_Binding($param)
	{	
		$tables = ['emp','job_history','manage_salary','emp_termination_reason'];
				if(in_array($param,$tables)){
					$get_field = \DB::select("select concat('#{{',table_name,'.',column_name,'}}') as binding, column_comment as field_name from information_schema.columns where table_schema = 'human_resource_system' and table_name = '$param' and column_name not in('id','filename','path') order by table_name,ordinal_position");
					foreach ($get_field as $keys) {
						$data[] = ['binding'=>$keys->binding,'field_name'=>$keys->field_name];
					}
					
			}else{
				$data = "$input Not Found.";
			}
		return \Response::json($data);		
	}

	public function Get_Table()
	{
		$tables = ['-select-','emp','job_history','manage_salary','emp_termination_reason'];
	
		return $tables;
	}


// INDEX #############################################################################
// FIX (METHOD POST)
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); 
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
						$model = new DocumentTemplate_Model;
						$result = $model->Read_Doc(null);
						if($result['status'] == 200){
							$data = $result['data']; $message = 'Show records.'; $status = 200;	
						}else{ $data = null; $message = 'Empty record data.'; $status = 200; }
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}
// END INDEX #############################################################################



	public function store()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); 
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200; $access= $access[3];
			}else{
				$get_role = $this->check(); $id_doc = uniqid();
				$model = new DocumentTemplate_Model;
				$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->$form);
				if(isset($get_role['data']['role'])){
					// CHECK KEY DAN CHECK ROLE IF TRUE
					if(isset($data_role['create']) && $data_role['create'] != 0){
						$input = $this->check_input();
						if($input['status'] == 200){
							$input['data']['id'] = $id_doc;
							$store = \DB::table('document_template')->insert($input['data']);

							if($store){
								$data =['id'=>$input['data']['id']]; $message = 'Store Successfully.'; $status = 200;		
							}else{
								$data =['id'=>$input['data']['id']]; $message = 'Store Failed.'; $status = 500;		
							}
						}
						else{ $data = null; $message ='Input not complete.'; $status = 500; }
					}
					else{ $data = null; $message = 'Unauthorized' ; $status = 401;}
					return \Response::json( ['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status );
				}else{
					return \Response::json( ['header'=>['message'=>$get_role['message'],'status'=>403]],403 );
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access],'data'=>$data],$status);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); 
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200; $access= $access[3];
			}else{
				$get_role = $this->check(); 
				$model = new DocumentTemplate_Model;
				$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->$form);
				if(isset($get_role['data']['role'])){
					// CHECK KEY DAN CHECK ROLE IF TRUE
					if(isset($data_role['create']) && $data_role['create'] != 0){
						$input = $this->check_input();
						if($input['status'] == 200){
							if(\DB::table('document_template')->where('id','=',$id)->get()){
								$update = \DB::table('document_template')->where('id','=',$id)->update($input['data']);
								if($update){
									$message = 'Updated Successfully.'; $status = 200;		
								}else{
									$message = 'Updated Failed.'; $status = 500;		
								}	
							}else{
								 $message ='Id not found.'; $status = 500;		
							}
						}
						else{ $data = null; $message ='Input not complete.'; $status = 500; }
					}
					else{ $data = null; $message = 'Unauthorized' ; $status = 401;}
					return \Response::json( ['header'=>['message'=>$message,'status'=>$status]],$status );
				}else{
					return \Response::json( ['header'=>['message'=>$get_role['message'],'status'=>403]],403 );
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access],'data'=>$data],$status);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); 
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$data = []; $message = 'Unauthorized'; $status = 200; $access= $access[3];
			}else{
				$model = new DocumentTemplate_Model;
				$get_role = $this->check();
				$data = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->$form);
				if($get_role['data']['role']){
					if($data['delete'] != 0 && $data['delete'] != null){
						$toArray = explode(",", $id);
						$data = $model->Destroy_Doc($toArray);
						if(!empty($data['status']) && $data['status'] == 200){ 
							$message=$data['message'];	$status=$data['status'];	
						}else{	$message=$data['message'];	$status=$data['status']; }
					}
					else{	$message='Unauthorized';	$status=401; }
				}
				else{ $message=$get_role['message'];	$status=403; }
				return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access],'data'=>$data],$status);
	}

	/* CHECK INPUT ################################################################################################################
	* Param $id = USE FIND ID FOR UPDATE.
	* PARAM $TYPE = 1(STORE), 2(UPDATE).
	*/
	private function check_input(){
		$data = array(); // tampung input to array
		$data['content_text'] 		= \Input::get('content_text');
		$data['template_name']		= \Input::get('template_name');
		$data['link_to_section']	= '201-JOB';//\Input::get('link_to_section');
		$data['margin_left']		= \Input::get('margin_left');
		$data['margin_top']			= \Input::get('margin_top');
		$data['margin_right']		= \Input::get('margin_right');
		$data['margin_bottom']		= \Input::get('margin_bottom');
		$data['size']				= strtolower(\Input::get('size'));
		$data['description'] 	    = \Input::get('description');
		
		if($data['content_text'] && $data['template_name']){
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }	
		return ['status'=>$status,'data'=>$result];
	}
	// CHECK INPUT VALUE #################################################################################
	/* CHECK ROLE ###################################################################################
	* PARAM = ROLE AND FORM
	* CHECK USER ROLE YANG ACCESS SYSTEM
	* MENGHASILKAN DATA ACCESS DAN ID
	*/
	// CHECK ROLE ############################################################################
//PARAM $ROLE = PERMISSION USER
	public function check_role($role,$employee_id,$form)
	{
		$getModel = new Privilege;
		if(isset($role, $form)){
			return $getModel->check_role_personal2($role,$employee_id,$form);
		}
		else{
			$msg['warning'] = 'Unauthorized';
			$msg['status'] = 404;
			return $msg; 
		}
	}
//END CHECK ROLE #########################################################################
// CHECK ACCESS ###############################################################################
	public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(isset($q) != null){ //check isset key in session
				 
						 $r = array();
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } // end for store role to array.

						 if(isset($r['key'])){
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							//return $employee_id;
							 $q2 = $getModel->session_role($employee_id);
							 foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 } // end for store role to array.
						 }
						 else{
						 	return ['message'=>'ACCESS DENIED','data'=>null];
						 }
						

				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
						$msg = 'ACCESS GRANTED !';	
					}else{
						$msg = 'ACCESS DENIED!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED!';
					$data = null;	
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;	
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		return ['message'=>$msg,'data'=>$data];
	}
// END CHECK ACCESS ############################################################################

}


