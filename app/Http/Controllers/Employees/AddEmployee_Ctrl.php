<?php namespace Larasite\Http\Controllers\Employees;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\AddEmployee_Model;
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;
use Larasite\Model\Master\Job\JobTitle_Model;
use Larasite\Model\Qualification\Seminar_Model ;
use Larasite\Http\Controllers\UserManagement\SystemUser_Ctrl;


class AddEmployee_Ctrl extends Controller {

// FIX ALL ###

protected $form = 48;
protected $Extfile = ["title"=>"Photograph_","extend"=>['jpg','png','gif','jpeg']];
protected $dir = "/hrms_upload/personal_picture";

//#################################################################################################################
public function set_valid()
{
	$reg = ['text'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
				'num'=>'Regex:/^[0-9-\^ ]+$/',
				'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
	$rule = ['first_name'=>"required|string",'middle_name'=>'string','last_name'=>"required|string",'autonumb'=>'required|numeric','ad_username'=>'string','local_it'=>'required'];
	$valid = \Validator::make(\Input::all(),$rule);
	return $valid;
}
// MOVE
	public function Move($id,$file,$type){
		$model = new AddEmployee_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		$check = $model->check_file_id($file['id']);
		if($check || $file['id']){
			if($type == 'cancel'){ 
				$move = $FuncUpload->move_file($check,'emp_picture');
				$message='Cancel Operation.'; $status=200; $data = null;
			}
			else{
				$up = $this->Upload(NULL,$file,true);
				$message=$up[0]; $status=$up[1]; $data=$up[2];	
			}
		}else{ $message='Upload Error'; $status=500; $data=NULL; }
		return [$message,$status,$data];
	}
// END MOVE
// UPLOAD
	public function Upload($id,$files,$type){
		$model = new AddEmployee_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		$file = $files['file'];
		$upload =  $FuncUpload->upload($this->dir,$file,$this->Extfile);
		if($upload['filename'] && $type ){
			
				\DB::table('emp')->insert(['employee_id'=>$files['id'],'local_it'=>$this->set_local()]);
				\DB::table('emp_picture')->insert(['employee_id'=>$files['id'],'filename'=>$upload['filename'],'path'=>$upload['path']]);
				$file->move(storage_path($upload['path']),$upload['filename']);
				$message = $upload['message']; $data = ['id'=>$model->GetID(null,$upload['filename']),'filename'=>$upload['filename']]; $status = 200;

		}else{ $message='Update Failed.'; $status=500; $data = $update;}
		return [$message,$status,$data];
	}
// END UPLOAD



// --- CRUD METHOD ---
	/* INDEX SHOW ALL ### */
	public function index(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/$model = new AddEmployee_Model; 
		$crud = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$key = \Input::get('key');
			$keys= base64_decode($key);
			$test = explode('-',$keys);
			$data = $test[1];

			$data = \DB::SELECT("select role.role_name from ldap,role where ldap.employee_id =  '$data' and ldap.role_id = role.role_id ");
			if($data[0]->role_name != null){
				$lower = strtolower($data[0]->role_name);
				$explode = explode(" ",$lower);
				$count = count($explode);

				if($count > 1){
					foreach ($explode as $key => $value) {
						if($value == "admin" || $value  = "hr"){
							$statEmp = $value;
						}else{
							$statEmp = "user";
						}
					}
				}else{
					$statEmp = $explode[0] == "admin" ? "admin" : ( $explode[0] == "hr" ? "hr" : "user");
				}
			}

			$type_loc = $model->getLocalAction($this->getKey());
			if( $type_loc == 2){ $type_loc = 'expat'; }elseif($type_loc == 0){ $type_loc = 'expat'; }else{ $type_loc = 'local'; }
			
			$data=['local_it'=>$type_loc,'autonumb'=>$this->make(),"stat_user" => $statEmp]; $status=200; $message='Add Employee.';
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
/* END INDEX ### */
	/* SHOW ### */
	public function show($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/$model = new AddEmployee_Model;
		$crud = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$data=['local_it'=>$this->get_local(),'autonumb'=>$this->make()]; $status=200; $message='Get id.';
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
/* END SHOW ### */
	/* STORE ### */

	public function short_user($key,$id){
		$keys= base64_decode($key);
		$test = explode('-',$keys);
		$data = $test[1];

		$data = \DB::SELECT("select role.role_name from ldap,role where ldap.employee_id =  '$data' and ldap.role_id = role.role_id ");
		if($data[0]->role_name != null){
			$lower = strtolower($data[0]->role_name);
			$explode = explode(" ",$lower);
			$count = count($explode);

			if($count > 1){
				foreach ($explode as $key => $value) {
					if($value == "admin" || $value  = "hr"){
						$statEmp = $value;
					}else{
						$statEmp = "user";
					}
				}
			}else{
				$statEmp = $explode[0] == "admin" ? "admin" : ( $explode[0] == "hr" ? "hr" : "user");
			}
		}

	}

	public function store(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/$model = new AddEmployee_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		$crud = $FRA->Access(\Request::all(),$this->form,'create');
		$key = \Input::get('key');
		if($access[1] == 200){
					/*Input*/$file = \Input::file('file'); $input = $this->set_input();  $autonumb = $this->make();
					if(isset($file) && isset($input['status']) && $input['status'] != 200 ){
						
						$Move = $this->Move(null,['id'=>$autonumb,'file'=>$file],null);
						$message=$Move[0]; $status=$Move[1]; $data=$Move[2];
						// $upload = $FuncUpload->upload($this->dir,$file,$this->ext);
						// if($upload['status'] == 200){
						// 	$file->move(storage_path($upload['path']),$upload['filename']); // move file

						// 	\DB::table('emp')->insert(['employee_id'=>$autonumb,'local_it'=>$this->set_local()]);
						// 	\DB::table('emp_picture')->insert(['employee_id'=>$autonumb,'filename'=>$upload['filename'],'path'=>$upload['path']]);
						// 	$message = $upload['message']; $data = ['id'=>$model->GetID(null,$upload['filename']),'filename'=>$upload['filename']]; $status = 200;

						// }else{ return $upload;}
					}
					elseif($input['status'] == 200 && empty($file)){
						$valid = $this->set_valid();
						if($valid->fails()){
							$message='Input Failed.'; $status=500; $data=null;
						}else{
							
							$check = $model->Store_Emp($autonumb,$input['data']);
							$message = $check['message']; $status = $check['status'];
							if($status == 200){ $data = $model->GetID($autonumb,null);}
							else{ $data=null; }	
						}
					}
					else{ $message = 'Store failed.';$data = null; $status=406; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
/* END STORE ### */
	/* STORE LANJUTAN ### */

	public function store2()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/$model = new AddEmployee_Model; /*Validasi*///$valid = $this->set_valid(); /*Upload*/ $FuncUpload = new FuncUpload;
		//$crud = $FRA->Access(\Request::all(),$this->form,'create');
		if($access[1] == 200){
			
			$request = new leaverequest_Model;
			$json = \Input::get('data');
			
			if($json != null){

				$data =  json_decode($json,1);
				if(!isset($data['local_it'])){
					$data['local_it'] = null;
				}
				if(!isset($data['middle_name'])){
					$data['middle_name'] = null;
				}
				$input = [
					"username" => $data['ad_username'],
					"employee_role" => $data['employee_role'],
					"autonumb" => $data['autonumb'],
					"firstname" => $data['first_name'],
					"lastname" => $data['last_name'],
					"local" => $data['local_it'],
					"middlename" => $data['middle_name']
					];

				$valid = $this->validasi($input['firstname'],$input['lastname']);
			
				if($valid != "ok"){
					return  $request->getMessage($valid,[],500,$access[3]);
				}else{
					$end = $model->insert_dataemp($input);
					return $end;
					// if($end['msg'] == "width" ){
					// 	return  $request->getMessage("Can't upload photo width format not support : ".$end['size']. " pixel, please upload photo width pixel size under 200 pixel only ",[],500,$access[3]);
					// }elseif($end['msg'] == "height"){
					// 	return  $request->getMessage("Can't upload photo height format not support : ".$end['size']. " pixel, please upload photo with pixel size under 200 pixel only ",[],500,$access[3]);
					// }else

					if($end['msg'] == "size"){
						return  $request->getMessage("Can't upload photo with size : ".$end['size']. " MB , please upload photo under 1 MB ",[],500,$access[3]);
					}elseif($end['msg'] == "success"){
						return  $request->getMessage("Insert data success ",$end['data'],200,$access[3]);
					}elseif($end['msg'] == "format"){
						return  $request->getMessage("failed, ".$end['ext']." unsupported format image only support .png and .jpg format ",[],500,$access[3]);
					}else{
						return  $request->getMessage("Insert data failed  ",[],500,$access[3]);
					}
				}
			}else{
				$input = [
					"username" => \Input::get('ad_username'),
					"autonumb" => \Input::get('autonumb'),
					"firstname" => \Input::get('first_name'),
					"lastname" => \Input::get('last_name'),
					"local" => \Input::get('local_it'),
					"employee_role" => \Input::get('employee_role'),
					"middlename" => \Input::get('middle_name')
					];
				
				$valid = $this->validasi($input['firstname'],$input['lastname']);
			
				if($valid != "ok"){
					return  $request->getMessage($valid,[],500,$access[3]);
				}else{
					$end = $model->insert_dataemp($input);
					return $end;
					if($end['msg'] == "success"){
						return  $request->getMessage("success",$end['data'],200,$access[3]);
					}else{
						return  $request->getMessage("failed",$end['data'],200,$access[3]);
					}
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }		
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}


	public function validasi($fs,$ls){
		$reg = ['text_num'=>'Regex:/^[ÑA-Zña-z0-9\-! ,\'\"\/@\.:\(\)]+$/'];
		$validasi = \Validator::make(
			["firstname" => $fs,
			 "lastname" => $ls],

			 ["firstname" => 'required|'.$reg['text_num'],
			  "lastname" => 'required|'.$reg['text_num']
			 ]
			);
		if($validasi->fails()){
			$check = $validasi->errors()->all();
			return $check;
		}else{
			return "ok";
		}
	}
/* END STORE LANJUTAN */
	/* UPDATE */
	public function update($id) // PUT FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/$model = new AddEmployee_Model;
		$crud = $FRA->Access(\Request::all(),$this->form,'update');
		if($access[1] == 200){
			if($id == 'store'){return $this->Store2();}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
/* END UPDATE */
	/* DESTROY */
	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/$model = new AddEmployee_Model;
		$crud = $FRA->Access(\Request::all(),$this->form,'destroy');
		if($access[1] == 200){
			$status=405; $message= $status.": METHOD NOT ALLOWED."; $data=NULL;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// END DESTROY ###
// --- END CRUD METHOD ---//
 
/* --- METHOD SET INPUT --- */ 
	public function set_input(){
		$data = array(); $data['first_name'] = \Input::get('first_name'); $data['ad_username']	= strtolower(\Input::get('ad_username')); $local_IT = \Input::get('local_it');
		if(isset($data['first_name'],$data['ad_username'])){
			if($this->set_local() == 1){
				if($local_IT == 'local_it'){ $data['local_it'] = 2; }
				elseif($local_IT == 'local'){ $data['local_it'] = 3;	}
				else{ $data['local_it'] = 1; }
			}
			elseif($local_IT == 'local_it'){ $data['local_it'] = 2; }
			elseif($local_IT == 'local'){ $data['local_it'] = 3; }
			$data['middle_name'] = \Input::get('middle_name'); $data['last_name']	= \Input::get('last_name');
			$data['employee_role'] = \Input::get('employee_role');
			if($data['employee_role'] != null && !is_numeric($data['employee_role'])){
				$status=200; $result = $data;	
			}else{
				$status=500; $result=null;		
			}
			
		}
		else{ $status=500; $result=null; }	
		return ['status'=>$status,'data'=>$result];
	}/* --- END METHOD SET INPUT --- */
/* --- METHOD LOCAL_IT ---*/
	/* GET LOCAL */
	public function get_local(){
		$r = \Request::all(); 	$model = new AddEmployee_Model;
		if(isset($r['key'])){
			$decode = base64_decode($r['key']);
			$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
			$get = $model->Get_local($id);
			foreach ($get as $key) { $type = $key->local_it; }
		}else{ $type = null; }
		return $type;
	}
	public function getKey()
	{
		$decode = base64_decode(\Request::get('key'));
	 	return  substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
	}
	/* SET LOCAL */
	public function set_local(){
		$r = \Request::all(); 	$model = new AddEmployee_Model;
		if(isset($r['key'])){
			$decode = base64_decode($r['key']);
			$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
			$get = $model->Set_local($id);
			foreach ($get as $key) { $type = $key->local_it; }
		}else{ $type = null; }
		return $type;
	} 
/* --- END METHOD LOCAL_IT ---- */
/* --- METHOD CHECK LOCAL,ROLE AND ACCESS --- */
	
	/* METHOD CHECK LOCAL
	* Parameter role id.
	*/ 
	public function check_local($role,$employee_id){
		$model = new AddEmployee_Model;
		if(isset($role)){ $get = $model->Get_Type($role,$employee_id); foreach ($get as $key) { $type = $key->local_it; } }
		else{ $type = null; }
		return $type;
	}
/* END CHECK LOCAL */
// MAKE AUTO NUMB EMP
	public function make(){
		$th = date('Y'); $q = '';
		$result = \DB::select("SELECT employee_id FROM emp where year(curdate()) = substr(employee_id,1,4) and   employee_id  not in('2017999','2017600') order by employee_id desc limit 1");
		foreach ($result as $key) { $q = $key->employee_id; }
		if($q){
			$index = substr($q ,4, 3)+1; $length = strlen($index);
			
			if($length == 1){ $auto = $th.'00'.$index;}
			elseif($length == 2){ $auto = $th.'0'.$index; }
			else{ $auto = $th.''.$index; }

			if($index == 1000){ return  'ID 999 not valid';}
			
			return $auto;	
		}else{ $auto = $th.'001'; return $auto; }
	}
}
