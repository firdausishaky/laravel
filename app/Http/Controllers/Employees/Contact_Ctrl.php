<?php namespace Larasite\Http\Controllers\Employees;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Larasite\Privilege;
use Larasite\Model\Contact_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;

class Contact_Ctrl extends Controller {

protected $form = ['expat'=>20,'local'=>29,'local_it'=>38];
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "20";
		}elseif($db_data == 2){
				return $this->formx = "29";
		}else{
			return $this->formx = "38";
		}
}
	private function check_id($id){
		$rule = ['undefined',NULL,''];
		if(in_array($id, $rule)){ return 500; } else{ return 200; }
	}
	private function set_valid(){
		$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\á\é\í\ó\ú\ü\Á\É\Í\Ó\Ú\Ü\ñ\Ñ\-! ,\'\"\/@\.:\(\)]*$/',
			'text'=>'Regex:/^[ñA-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'num'=>'Regex:/^[0-9-\^ ]+$/',
			'hmo'=>'Regex:/^[ñA-Za-z0-9-\^ ]+$/',
			'twit'=>'regex:/^[ñA-Za-z0-9_]{1,15}$/'];
		/*$reg = ['text'=>'regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)#]+$/',
				'num'=>'regex:/^[0-9-\^ ]+$/'
		       ];*/
		$rule = ['permanent_address'=>$reg['text_num'],
				'permanent_city'=>$reg['text_num'],
				'permanent_state'=>$reg['text_num'],
				'permanent_zip'=>$reg['num'],
				'permanent_country'=>$reg['text_num'],
				'present_address'=>$reg['text_num'],
				'present_city'=>$reg['text_num'],
				'present_state'=>$reg['text_num'],
				'present_zip'=>$reg['num'],
				'present_country'=>$reg['text_num'],
				'home_telephone'=>$reg['num'],
				'mobile'=>$reg['num'],

			];

		$valid = \Validator::make(\Input::all(),$rule); return $valid;
	}
// NOT USE
	public function destroy($id)
	{	return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>405]],405);  }
	public function create($id)
	{	return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>405]],405);  }
	public function edit($id)
	{	return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>405]],405);  }
	public function index()
	{	return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>405]],405);  }
	public function store()
	{	return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>405]],405);  }

// SHOW DATA
	private function show_data_contact($id){
		$model = new Contact_Model;
		$result = $model->ReadContact($id);
		if($result['data'] && $result['status'] == 200){
			$data=$result['data']; $status=$result['status'];	$message='Contact : Show Records Data.';
		}else{ $data=null; $status=200;	$message='Contact : Empty Records Data.'; }
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
// SHOW
	public function show($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){

			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				if($access[2][0] == 5 && $id == $access[2][2] ){
					$datas = $this->show_data_contact($access[2][2]);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
				}
				elseif($access[2][0] != 5 && $id == $access[2][2] ){
					$datas = $this->show_data_contact($access[2][2]);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
				}
				else{
					$datas = $this->show_data_contact($id);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// UPDATE
	public function update($id)
	{
		// 20190918_001
		// BEFORE /*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,3); $model = new Contact_Model;
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,3); $model = new Contact_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'update');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$valid = $this->set_valid();
				if($valid->fails()){ $message= $valid->errors(); $status=500; $data=null; }
				else{
					$work_email = \Input::get('work_email');
					$personal_email	= \Input::get('personal_email');

					if($work_email ==  null and   $personal_email  ==   null){
						return response()->json(['header' => ['message' => 'please fill one of the email (personal or work email)', 'status' => 500 ],'data' => []],500);
					}

					$input = $this->check_input(); $table = \DB::table('emp');
					$update = $model->StoreContact($input,$id);
					$message=$update['message']; $status=$update['status']; $data=$update['data'];

					if(isset($update)){	$message =$update['message']; $status =$update['status']; $data=$table->where('employee_id','=',$id)->get(['permanent_address','present_address','home_telephone','mobile','work_email','personal_email']);}
					else{ $message = 'Update Failed.'; $status = 406; $data=$table->where('employee_id','=',$id)->get(['permanent_address','present_address','home_telephone','mobile','work_email','personal_email']); }
			}	}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// CHECK INPUT
	public function check_input(){
		return [
			'permanent_address'		=> \Input::get('permanent_address'),
			'permanent_city'		=> \Input::get('permanent_city'),
			'permanent_state'		=> \Input::get('permanent_state'),
			'permanent_zip'			=> \Input::get('permanent_zip'),
			'permanent_country'		=> \Input::get('permanent_country'),
			'present_address'		=> \Input::get('present_address'),
			'present_city'			=> \Input::get('present_city'),
			'present_state'			=> \Input::get('present_state'),
			'present_zip'			=> \Input::get('present_zip'),
			'present_country'		=> \Input::get('present_country'),
			'home_telephone'		=> \Input::get('home_telephone'),
			'mobile'			=> \Input::get('mobile'),
			'work_email'			=> \Input::get('work_email'),
			'personal_email'		=> \Input::get('personal_email')
		];
	}
}
