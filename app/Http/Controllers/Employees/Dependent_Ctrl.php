<?php namespace Larasite\Http\Controllers\Employees;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\Dependent_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
use Larasite\Library\FuncParse;
use Larasite\Library\FuncDB;

class Dependent_Ctrl extends Controller {

protected $form =['expat'=>22,'local'=>31,'local_it'=>40];
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "22";
		}elseif($db_data == 2){
				return $this->formx = "31";
		}else{
			return $this->formx = "40";
		}
}
private function GetEMP(){
	$remove = json_decode(file_get_contents('php://input'));
	if(gettype($remove) == 'object'){		
		foreach ($remove as $key) { $tmp = $key; }	
		return $tmp;
	}else{
		return \Input::get('emp');
	}
}

// CHECK ID
private function check_id($id)
{
	$rule = ['undefined',NULL,''];
	if(in_array($id, $rule)){return 500;} else{ return 200; }
}
// VALID
private function set_valid()
{
	$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'num'=>'Regex:/^[0-9-\^ ]+$/',
			'hmo'=>'Regex:/^[A-Za-z0-9-\^ ]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
	$rule = ['name'=>"required|string",'relationship'=>'required|alpha','date_of_birth'=>'date','age'=>'numeric','gender'=>'alpha','hmo_account'=>$reg['hmo'],'hmo_numb'=>$reg['hmo']];
	$valid = \Validator::make(\Input::all(),$rule); return $valid;
}

// INDEX
	public function index($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			$show = $model->Read_Dependent($id,null);
			if(isset($show) && $show['status'] == 200){
				$data=$show['data']; $status=$show['status']; $message='Show Records Data.';
			}else{ $data=null; $status=404; $message='Empty Records Data.'; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
		
	}
// SHOW 
	private function show_data_dependent($id){
		$model = new Dependent_Model; // SET MODEL
		$show = $model->Read_Dependent($id,null);
		if($show['data'] && $show['status'] == 200){
			$data=$show['data']; $status=$show['status']; $message='Show Records Data.';
		}else{ $data=null; $status=200; $message='Empty Records Data.'; }
		
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
// SHOW
	public function show($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1); $model = new Dependent_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					if($access[2][0] == 5 && $id == $access[2][2] ){
						$datas = $this->show_data_dependent($access[2][2]);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
					elseif($access[2][0] != 5 && $id == $access[2][2] ){
						$datas = $this->show_data_dependent($access[2][2]);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
					else{
						$datas = $this->show_data_dependent($id);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
				}			
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// STORE
	public function store($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2); $model = new Dependent_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$valid = $this->set_valid();
				if($valid->fails()){ $message='Required Input.'; $status=500; $data=null; }
				else{
					$input = $this->check_input($id);
					$store = $model->Store_Dependent($input['data']);
					if($store != null){
						return  $store;
						$message = "success"; $status = 200; $data = $store['data'][0];	
					}else{
					$message = "failed"; $status = 500; $data = [];
					}	
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	} 
// UPDATE
	public function update($id)
	{	
		$FRA = new FuncAccess; 
		//$access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,3); 
		$model = new Dependent_Model;
		$access = $FRA->Access(\Request::all(),$this->formx,'update');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; } // check id
			else{
				
				$valid = $this->set_valid();
				if($valid->fails()){ $message='Required Input'; $status=500; $data=null; } // validator
				else{
					$item_id = \Input::get('id');
					$input = $this->check_input($item_id); $input2 = $input['data'];
					if(Dependent_Model::find($item_id)){

						$update = $model->Update_Dependent($input2,$item_id);
						if(isset($update)){
						      $message = "success"; $status=200; $data = $input2;
						}
						
					}else{ $message='ID not found.'; $status=404; $data=null; }
			}	}			
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}
// DELETE
	public function destroy($id)
	{
		$tmp = $this->GetEMP();
		
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$tmp,'Request'=>\Request::all()],$this->form,4); $model = new Dependent_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'destroy');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$toArray = explode(",", $id);
				$data = $model->Destroy_Dependent($toArray);
				if(isset($data)){
					$message = $data['message']; $status = $data['status'];	$data=null;
				}else{ $message = 'ID not found'; $status = 404; $data=null;}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
//CHECK INPUT
	private function check_input($id){
		$data = array(); // tampung input to array
		$data['name'] = \Input::get('name');
		$data['relationship'] = \Input::get('relationship');
		$data['date_of_birth'] = \Input::get('date_of_birth');
		$data['age'] = \Input::get('age');
		$data['gender'] = \Input::get('gender');
		$data['hmo_account'] = \Input::get('hmo_account');
		$data['hmo_numb'] = \Input::get('hmo_numb');
		$data['id'] = $id;
		
		if($data['name'] && $data['relationship']){
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }	
		return ['status'=>$status,'data'=>$result];
	}



	/*NOT USES*/
// EDIT
	public function edit($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(\Request::all(),$this->form,1); $model = new Dependent_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				// $edit = $model->Read_Dependent($id);
				// if(isset($edit) != null && $edit['status'] == 200){
				// 	$message='Read Data.'; $status=200; $data=$edit['data'];
				// }else{ $message='ID not found.'; $status=500; $data=null; }
				$message = 'Page not found.'; $status=404; $data=NULL;
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}

}// END CLASS ###
