<?php namespace Larasite\Http\Controllers\Employees;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Larasite\Privilege;
use Larasite\Model\Emergency_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;

class Emergency_Ctrl extends Controller {
// var $form_expat = 21;
// var $form_local = 30;
// var $form_localit = null;
protected $form = ['expat'=>21,'local'=>30,'local_it'=>39];
protected $formx = "";


public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "21";
		}elseif($db_data == 2){
				return $this->formx = "30";
		}else{
			return $this->formx = "39";
		}
}
private function GetEMP(){
	$remove = json_decode(file_get_contents('php://input'));
	if(gettype($remove) == 'object'){		
		foreach ($remove as $key) { $tmp = $key; }	
		return $tmp;
	}else{
		return \Input::get('emp');
	}
}

private function check_id($id)
{
	$rule = ['undefined',NULL,''];
	if(in_array($id, $rule)){return 500;}else{return 200;}
}
private function set_valid()
{
	$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'num'=>'Regex:/^[0-9-\^ ]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
			//,'home_telephone'=>$reg['num'],'mobile_telephone'=>$reg['num'],'work_telephone'=>$reg['num'],'address'=>$reg['text'],'city'=>$reg['text'],'state'=>$reg['text'],'zip'=>$reg['text_num'],'country'=>$reg['twit']
	$rule = ['name'=>'required|'.$reg['text'].'|max:50','relationship'=>'required|'.$reg['text'].'|max:50'];
	$valid = \Validator::make(\Input::all(),$rule); return $valid;
}

// CHECK INPUT
	private function check_input($id){
		$home = \Input::get('home_telephone'); (!isset($home) ? $home = null : $home = \Input::get('home_telephone'));
		$mobile = \Input::get('mobile_telephone'); (!isset($mobile) ? $mobile = null : $mobile = \Input::get('mobile_telephone'));
		$work = \Input::get('work_telephone'); (!isset($work) ? $work = null : $work = \Input::get('work_telephone'));
		$address = \Input::get('address'); (!isset($address) ? $address =null : $address = \Input::get('address'));
		$city = \Input::get('city'); (!isset($city) ? $city = null : $city = \Input::get('city'));
		$state = \Input::get('state'); (!isset($state) ? $state = null : $state = \Input::get('state'));
		$zip = \Input::get('zip'); (!isset($zip) ? $zip = 0 : $zip = \Input::get('zip'));
		$country = \Input::get('country'); (!isset($country) ? $country = null : $country = \Input::get('country'));
	
		$data = array(); // tampung input to array
		$data['name'] = \Input::get('name');
		$data['relationship'] = \Input::get('relationship');
		$data['home_telephone'] = $home;
		$data['mobile_telephone'] = $mobile;
		$data['work_telephone'] = $work;
		$data['address'] = $address;
		$data['city'] = $city;
		$data['state'] = $state;
		$data['zip'] = $zip;
		$data['country'] = $country;
		$data['id'] = $id;
		if(isset($data)){
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }	
		return ['status'=>$status,'data'=>$result];
	}
// END CLASS ###


// INDEX
	// public function index()
	// {
	// 	$get_role = $this->check();
	// 	if($get_role['data']['role']){ // CHECK KEY
	// 			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1); // GET ACCESS
				
	// 			if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS
	// 				$type = $this->check_local($get_role['data']); // GET LOCAL
	// 				$model = new Emergency_Model; // SET MODEL
	// 				if($type == 1){
	// 					$result = $model->ReadEmergency_Expat_All(); // GET RESULT MODEL
	// 					if(isset($result)){ $data = $result; $message='View data'; $status=200; } 
	// 					else{ $data = null; $message='Data not found'; $status=200;	}
	// 				}
	// 				elseif($type == 2){
	// 					$result = $model->ReadEmergency_Local_All(); // GET RESULT MODEL
	// 					if(isset($result)){ $data = $result; $message='View data'; $status=200; } 
	// 					else{ $data = null; $message='Data not found'; $status=200;	}
	// 				}
	// 				elseif($type == 3) {
	// 					$result = $model->ReadEmergency_Localit_All(); // GET RESULT MODEL
	// 					if(isset($result)){ $data = $result; $message='View data'; $status=200; } 
	// 					else{ $data = null; $message='Data not found'; $status=200;	}
	// 				}
	// 				else{
	// 					$message='Local it not set.'; $status=500; $data=null;
	// 				}
	// 			}else{ $message=$access['message'];	$status=$access['status']; $data=null; }
	// 	}
	// 	else{ // for not access or error
	// 		$message = $get_role['message']; $status=401; $data=null;
	// 	}
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	// }


// SHOW
	private function show_data_emergency($id){
		$model = new Emergency_Model; // SET MODEL
		$show = $model->Read_Emergency($id);
		if($show['data'] && $show['status'] == 200){
			$data=$show['data']; $status=$show['status']; $message='Show Records Data.';
		}else{ $data=null; $status=200; $message='Empty Records Data.'; }
		return ['data'=>$data,'status'=>$status,'message'=>$message];
	}
// SHOW 
	public function show($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				if($access[2][0] == 5 && $id == $access[2][2] ){
					$datas = $this->show_data_emergency($access[2][2]);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
				}elseif($access[2][0] != 5 && $id == $access[2][2] ){
					$datas = $this->show_data_emergency($id);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
				}elseif($access[2][0] != 5 && $id != $access[2][2] ){
					$datas = $this->show_data_emergency($id);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// STORE
	public function store($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2); $model= new Emergency_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$valid = $this->set_valid();

				if($valid->fails()){$message='Required Input.'; $status=500; $data=null;}
				else{
					$input = $this->check_input($id);
					$get = $model->StoreEmergency($input['data']);
					if(isset($get) && $get != null){
						$message='Store Successfully.'; $status=200; $data=$get;
					}else{ $message='Store not success.'; $status=406; $data=null; }
			}	}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access" => $crud[3]],$status);
	} 
// // EDIT
// 	public function edit($id)
// 	{
// 		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(\Request::all(),$this->form,1);
// 		if($access[1] == 200){
// 			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
// 			else{
// 				if($access[2][0] == 5 && $access[2][1] == $access[2][2] ){						
// 					$datas = $this->show_data_emergency($access['message']['employee_id']);
// 					$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
// 				}else{
// 					$datas = $this->show_data_emergency($id);
// 					$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
// 			}	}
// 		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
// 	}
// UPDATE
	public function update($id)
	{	
		$key = \Input::get('key');
		$keys= base64_decode($key);
		$test = explode('-',$keys);
		
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$test[1],'Request'=>\Request::all()],$this->form,3); $model= new Emergency_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				
				$valid = $this->set_valid();
				if($valid->fails()){$message='Required Input.'; $status=500; $data=null;}
				else{
					$input = $this->check_input($id);  $input2 = $input['data'];
					$db = \DB::SELECT("UPDATE emp_emergency SET name = '$input2[name]', address = '$input2[address]', relationship = '$input2[relationship]', 
									   home_telephone = '$input2[home_telephone]', mobile_telephone = '$input2[mobile_telephone]', work_telephone ='$input2[work_telephone]',
									   city = '$input2[city]', state = '$input2[state]', zip = $input2[zip], country = '$input2[country]' where id = $id ");
					if(isset($db)){

					 	$message = "Success"; $status = 200; $data= $input2;	
					}	
				}
			}	
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $crud[3] ],'data'=>$data],$status);
	}
// DELETE
	public function destroy($id)
	{
		$tmp = $this->GetEMP();
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$tmp,'Request'=>\Request::all()],$this->form,4); $model= new Emergency_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'destroy');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				
				$toArray = explode(",", $id);
				$data = $model->Destroy_Emergency($toArray);
				if(isset($data)){
					$message = $data['message']; $status = $data['status'];	
				}else{ $message = 'ID not found'; $status = 404; }
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
}