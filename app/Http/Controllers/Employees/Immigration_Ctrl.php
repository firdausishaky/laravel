<?php namespace Larasite\Http\Controllers\Employees;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\EmpImmigration_Model;
use Larasite\Model\Qualification\Seminar_Model ;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
class Immigration_Ctrl extends Controller {

// var $form_expat = 23;
// var $form_local = 30; 
protected $form = ['expat'=>23,'local'=>32,'local_it'=>41];
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "23";
		}elseif($db_data == 2){
				return $this->formx = "32";
		}else{
			return $this->formx = "41";
		}
}
protected $dir = "/hrms_upload/personal_immigration";
protected $Extfile = ["title"=>"PersonalImmigration","extend"=>["jpg","jpeg","png","gif","pdf"]];


	private function GetEMP(){
		$remove = json_decode(file_get_contents('php://input'));
		if(gettype($remove) == 'object'){		
			foreach ($remove as $key) { $tmp = $key; }	
			return $tmp;
		}else{
			return \Input::get('emp');
		}
	}

	private function check_id($id)
	{
		$rule = ['undefined',NULL,''];
		if(in_array($id, $rule)){return 500;}else{ return 200; }
	}
	private function set_valid()
	{	
		$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'num'=>'Regex:/^[0-9-\^ ]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
		$rule = ['immigration_numb'=>"required|".$reg['text_num'],
		'type_doc'=>'required|alpha',
		'issued_date'=>'date',
		'expiry_date'=>'date',
		'issued_by'=>'numeric',
		'comment'=>$reg['text_num']];
		$input = \Input::all();
		if(isset($input['issued_date']) || isset($input['expiry_date'])){
			if($input['issued_date'] == "0000-00-00" || $input['expiry_date'] == "0000-00-00"){ $input['expiry_date'] = NULL; $input['issued_date'] = NULL; }
		}
		$valid = \Validator::make($input,$rule); return $valid;
	}
	public function index()
	{ return \Response::json(['header'=>['message'=>'Page Not Found.','status'=>404]],404);  }
	
	public function download($id,$filename){
			$check = \DB::select("SELECT filename from emp_immigration where employee_id = '$id' and filename = '$filename'");
			
			if(!$check){ return \Response::json(['header'=>['message'=>'File not Found']],404); }
			foreach ($check as $key) { $files = $key->filename;}
			
			$path = storage_path()."/hrms_upload/personal_immigration/".$files;
			$file = \File::get($path); $type = \File::mimeType($path);		
			return \Response::make($file,200,['Content-Type'=>$type]);
	}
// STORE
	public function store($id)
	{
		/*Access*/$FRA = new FuncAccess; 
		$access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2);
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		if($crud[1] == 200){
		
					$request = new leaverequest_Model;
					$model = new Seminar_Model;
					$json = \Input::get('data');

					if($json != null){
						$data =  json_decode($json,1);
					
						$type = $data['type_doc'];
						$immigration = $data['immigration_numb'];
						if($type == null || $immigration == null  || !isset($data['type_doc']) || !isset($data['immigration_numb']))
						{return $request->getMessage("Please fill required form",[],500,$crud[3]);}
					
						(!isset($data['expiry_date'] ) || $data['expiry_date'] == null ? $expire = "null" : $expire = $data['expiry_date']  );
						(!isset($data['issued_by']) || $data['issued_by'] == null ? $issued = "null" : $issued = $data['issued_by']  );
						(!isset($data['issued_date']) || $data['issued_date'] == null ? $issued_date = "null" : $issued_date = $data['issued_date']  );
						(!isset($data['comment']) || $data['comment'] == null ? $comment = "null" : $comment = $data['comment']  );
						
						$check = $this->validasi($type,$immigration,$expire,$issued_date,$issued);
						if($check != null){
							return $request->getMessage($check,[],500,$crud[3]);
						}
						$image = \Input::file('file');
						$imageName = $image->getClientOriginalName();
						$ext = \Input::file('file')->getClientOriginalExtension();
						$path = "hrms_upload/personal_immigration";
						$size = $image->getSize();

							$req_image = $request->check_imgPDF($ext,$size,$image,$path,$crud[3]);
							if($req_image != null && $req_image == "size"){
								return $request->getMessage("failed image format supported but size of image more than 1 MB",[],200,$access);
							}
							if($req_image != null && $req_image == "format"){
								return $request->getMessage("unsupported format",[],200,$crud[3]);
							}else{
								$imageName = $req_image;
							}
						//}
					}else{
						$type = \Input::get('type_doc');
						$immigration = \Input::get('immigration_numb');
						if(!isset($type) && !isset($immigration)){
							return $request->getMessage("Please fill required form",[],500,$crud[3]);
						}
						$expire = \Input::get('expiry_date');
						(!isset($expire) ? $expire = null : $expire = $expire);
						$issued = \Input::get('issued_by');
						(!isset($issued) ? $issued = "null" : $issued = $issued);
						$issued_date = \Input::get('issued_date');
						(!isset($issued_date) ? $issued_date = null : $issued_date=$issued_date);
						$comment = \Input::get('comment');
						(!isset($comment) ? $comment = "null" : $comment = $comment);
						$imageName =  "null";
						$path = "null";
						$check = $this->validasi($type,$immigration,$expire,$issued_date,$issued);
						if($check != null){
							return $request->getMessage($check,[],500,$crud[3]);
						}
					}
				
					$db = \DB::SELECT(" insert into emp_immigration(immigration_numb,employee_id,type_doc,issued_date,issued_by,expiry_date,comment,filename,path) 
						values('$immigration','$id','$type','$issued_date',$issued,'$expire','$comment','$imageName','$path')");
					
					if(isset($db)){
						if($issued != null){
						     $data = \DB::SELECT(" select * from emp_immigration where employee_id='$id' order by id desc  limit 1");
						     foreach ($data as $key) {
						     	if($key->issued_date == '0000-00-00'){ $key->issued_date = NULL; }
							if($key->expiry_date == '0000-00-00'){ $key->expiry_date = NULL; }
							if($key->comment == 'NULL'){ $key->expiry_date = NULL; }
							if($key->issued_by == 'NULL'){ $key->expiry_date = NULL; }
							if($key->type_doc == 'NULL'){ $key->expiry_date = NULL; }
						     }
						
						}else{
						     $data = \DB::SELECT(" select emp_immigration.immigration_numb,emp_immigration.id,emp_immigration.employee_id,emp_immigration.issued_date,emp_immigration.expiry_date,emp_immigration.comment,emp_immigration.filename,emp_immigration.type_doc,emp_immigration.path,immigration_issuer.title as issued_by  from emp_immigration,immigration_issuer where emp_immigration.employee_id=$id  and immigration_issuer.id=$issued order by  id DESC limit 1 ");
						}
						$message =  "success";
						$status = 200;
						return  $request->getMessage("Success",$data,200,$crud[3]);
					 }
				
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// STORE LANJUTAN
	public function store_title($id) // IF FILE UPLOAD EXISTS .STORE
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2); $model = new EmpImmigration_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$file = \Input::file('file'); $id_immigration = \Input::get('id');	$getstr = $this->Set_InputString(); $InputStr = $getstr['data'];
				if(isset($InputStr) != null && $getstr['status'] == 200 && empty($file)){
					$valid= $this->set_valid();
					
					if($valid->fails()){ $message='Required Input.'; $status=500; $data=null; }
					else{
						if($id_immigration){
							if($model->StoreImmigration($InputStr,$id_immigration) == 200){ $message = 'Store Successfully.'; $status = 200; $data =\DB::table('emp_immigration')->where('id','=',$id_immigration)->get()[0]; }
							else{ $message = 'Store Failed.'; $status =500; $data = NULL; }	
						}else{
							$InputStr['employee_id'] = $id;
							EmpImmigration_Model::insert($InputStr);
							$message = 'Store Successfully.'; $status = 200; $data =$id_immigration; $data = \DB::table('emp_immigration')->where('immigration_numb','=',$InputStr['immigration_numb'])->get()[0];
						}
					}
				}elseif( isset($file) && !isset($InputStr) ){
					$Move = $this->Move($id_immigration,['file'=>$file],null);
					$message=$Move[0]; $status=$Move[1]; $data=$Move[2];
				}else{ $message='Required Input.'; $status = 406; $data = null; }// problem input
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// SHOW
	private function show_data_immigration($id){
		$model = new EmpImmigration_Model;
		$result = $model->Read_Immigration($id);
		if(isset($picture['filename']) != null && isset($result['data']) != null && $result['status'] == 200){
			$data = $result['data']; $message='Show Records Data'; $status=200;
		}elseif(empty($picture['filename']) && isset($result['data']) != null && $result['status'] == 200){
			$data = $result['data']; $message='Show Records Data.'; $status=200;
		}else{ $data = null; $message='Empty Records Data.'; $status=200;}
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
	public function show($id)
	{
		// /*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1); $model = new EmpImmigration_Model;
		// $crud = $FRA->Access(\Request::all(),$this->formx,'read');
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				if($access[2][0] == 5 && $access[2][1] == $access[2][2] ){
					$datas = $this->show_data_immigration($access[0],['employee_id']);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
				}else{
					$datas = $this->show_data_immigration($id);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
			}	}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}
// UPDATE
	public function update($id)
	{

		/*PERMISSIONS*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>NULL,'Request'=>\Request::all()],$this->form,3); $model = new EmpImmigration_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'update');
		if($access[1] == 200){
			// CHECK ID
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				// VALIDATOR
				$valid = $this->set_valid();
				if($valid->fails()){ $message='Required Input.'; $status=500; $data=null; }
				else{
					// ACTION
					$input = $this->Set_InputString();
					if(isset($input['data']) != null && $input['status'] == 200){
							// RESPONSE
							$update = $model->UpdateImmigration($input['data'],$id);
							$message=$update['message']; $status=$update['status']; 
					}else{ $message = 'Input not null'; $status=404; }
			}	}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);		
	}
//VALIDATION
	public function validasi($type,$immigration,$expire,$issued_date,$issued){
			$validation= \Validator::make(
								["type_doc" => $type,
								"immigration_numb" => $immigration,
								],
								
								["type_doc" => 'required',
								"immigration_numb" => 'required|alpha_num',
								]
							);
			if($validation->fails()){
				$check = $validation->errors()->all();
				if($check != null){
					return $check;
				}else{
					$check = null;
					return $check;
				}
			}
	}
//
// UPDATE FILE
	public function update_file($id,$emp)
	{
		/*Access*/$FRA = new FuncAccess; 
		$access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2);
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		if($crud[1] == 200){
					
					$request = new leaverequest_Model;
					$model = new Seminar_Model;
					$json = \Input::get('data');

					if($json != null){
						$data =  json_decode($json,1);
					
						$type = $data['type_doc'];
						$immigration = $data['immigration_numb'];
						if($type == null || $immigration == null  || !isset($data['type_doc']) || !isset($data['immigration_numb']))
						{return $request->getMessage("Please fill required form",[],500,$crud[3]);}
					
						(isset($data['expiry_date']) && $data['expiry_date'] != null ? $expire = $data['expiry_date'] : $expire = null  );
						(isset($data['issued_by']) && $data['issued_by'] != null ? $issued = $data['issued_by'] : $issued = "null"  );
						(isset($data['issued_date']) && $data['issued_date'] != null ? $issued_date = $data['issued_date'] : $issued_date = null  );
						(isset($data['comment']) && $data['comment'] != null ? $comment = $data['comment'] : $comment = "null"  );
						
						$check = $this->validasi($type,$immigration,$expire,$issued_date,$issued);
						if($check != null){
							return $request->getMessage($check,[],500,$crud[3]);
						}
						$image = \Input::file('file');
						$imageName = $image->getClientOriginalName();
						$ext = \Input::file('file')->getClientOriginalExtension();
						$path = "hrms_upload/personal_immigration";
						$size = $image->getSize();

							$req_image = $request->check_imgPDF($ext,$size,$image,$path,$crud[3]);
							if($req_image != null && $req_image == "size"){
								return $request->getMessage("failed image format supported but size of image more than 1 MB",[],200,$access);
							}
							if($req_image != null && $req_image == "format"){
								return $request->getMessage("unsupported format",[],200,$crud[3]);
							}else{
								$imageName = $req_image;
							}
						//}
					}else{
						$type = \Input::get('type_doc');
						$immigration = \Input::get('immigration_numb');
						if(!isset($type) && !isset($immigration)){
							return $request->getMessage("Please fill required form",[],500,$crud[3]);
						}
						$expire = \Input::get('expiry_date');
						(!isset($expire) ? $expire = null : $expire = $expire);
						$issued = \Input::get('issued_by');
						(!isset($issued) ? $issued = "null" : $issued = $issued);
						$issued_date = \Input::get('issued_date');
						(!isset($issued_date) ? $issued_date = null : $issued_date=$issued_date);
						$comment = \Input::get('comment');
						(!isset($comment) ? $comment = "null" : $comment = $comment);
						$imageName =  "null";
						$path = "null";
						$check = $this->validasi($type,$immigration,$expire,$issued_date,$issued);
						if($check != null){
							return $request->getMessage($check,[],500,$crud[3]);
						}
					}
					
						$db = \DB::SELECT(" update emp_immigration set immigration_numb='$immigration',employee_id='$emp',type_doc='$type',issued_by=$issued,issued_date='$issued_date',expiry_date='$expire',comment='$comment',filename='$imageName',path='$path' where id=$id ");
										
					if(isset($db)){
						if($issued == null){
						     $data = \DB::SELECT(" select * from emp_immigration where employee_id='$emp' order by id desc  limit 1");
						     foreach ($data as $key) {
						     	if($key->issued_date == '0000-00-00'){ $key->issued_date = NULL; }
							if($key->expiry_date == '0000-00-00'){ $key->expiry_date = NULL; }
							if($key->comment == 'NULL'){ $key->expiry_date = NULL; }
							if($key->issued_by == 'NULL'){ $key->expiry_date = NULL; }
							if($key->type_doc == 'NULL'){ $key->expiry_date = NULL; }
						     }
						
						}else{
						     $data = \DB::SELECT(" select emp_immigration.immigration_numb,emp_immigration.id,emp_immigration.employee_id,emp_immigration.issued_date,emp_immigration.expiry_date,emp_immigration.comment,emp_immigration.filename,emp_immigration.type_doc,emp_immigration.path,immigration_issuer.title as issued_by  from emp_immigration,immigration_issuer where emp_immigration.employee_id='$emp'  and immigration_issuer.id=$issued  order by  id DESC limit 1 ");
						}
						$message =  "success";
						$status = 200;
						return  $request->getMessage("Success",$data,200,$crud[3]);
					 }
				
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// DESTROY
	public function destroy($id)
	{
		$tmp = $this->GetEMP();
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$tmp,'Request'=>\Request::all()],$this->form,4); $model = new EmpImmigration_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'destroy');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$toArray = explode(",", $id);
				$data = $model->Destroy_Immigration($toArray);
				if(isset($data)){ $message = $data['message']; $status = $data['status'];	 }
				else{ $message = 'ID not found'; $status = 404; }
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// SETUP INPUT
	public function Set_InputString(){
		$input = array();
		$input['immigration_numb']	= \Input::get('immigration_numb');
		$input['type_doc']			= \Input::get('type_doc');
		$input['issued_date']		= \Input::get('issued_date');
		$input['expiry_date']		= \Input::get('expiry_date');
		$input['issued_by']			= \Input::get('issued_by');
		$input['comment'] = \Input::get('comment');
		if($input['type_doc'] && $input['immigration_numb']){ $data = $input; $status=200; }
		else{ $data = null; $status =500; }
		return ['data'=>$data,'status'=>$status];
	}
// MOVE
	private function Move($id,$file,$type){
		$model = new EmpImmigration_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		$check = $model->check_file_id($id);
		
		if($check || $id){
			if($type == 'cancel'){ 
				$move = $FuncUpload->move_file($check,'emp_immigration');
				$message='Cancel Operation.'; $status=200; $data = null;
			}
			elseif($type == 'delfile'){
				$move = $FuncUpload->move_file($check,'emp_immigration');
				$message=$move['message']; $status=$move['message']; $data=NULL;
			}
			else{
				if($check){
					$move = $FuncUpload->move_file($check,'emp_immigration');
					$up = $this->Upload($id,$file,false);
					$message=$up[0]; $status=$up[1]; $data=$up[2];	
				}else{
					$up = $this->Upload($id,$file,false);
					$message=$up[0]; $status=$up[1]; $data=$up[2];
				}	
			}
		}else{ $up = $this->Upload($id,$file,true); $message=$up[0]; $status=$up[1]; $data=$up[2]; }
		return [$message,$status,$data];
	}
// UPLOAD
	private function Upload($id,$files,$type){
		$model = new EmpImmigration_Model; /*Upload*/ 
		$FuncUpload = new FuncUpload; $file = $files['file'];
		$upload =  $FuncUpload->upload($this->dir,$file,$this->Extfile);

		if($upload['filename'] == true){
			if($type == true){
				$input = ['employee_id'=>$files['employee_id'],'filename'=>$upload['filename'],'path'=>$upload['path']];
				$model->StoreFile(false,$input);
				$file->move(storage_path($upload['path']),$upload['filename']);
				$message = $upload['message']; $data = ['id'=>$model->GetID($upload['filename']),'filename'=>$upload['filename']]; $status = 200;
			}else{
				$input = ['filename'=>$upload['filename'],'path'=>$upload['path']];
				$model->StoreFile($id,$input);
				$file->move(storage_path($upload['path']),$upload['filename']);
				$message = 'Updated Successfully.';$status = 200; $data = EmpImmigration_Model::find($id);
			}
		}else{ $message='Update Failed.'; $status=500; $data = $update;}
		return [$message,$status,$data];
	}
}
