<?php namespace Larasite\Http\Controllers\Employees\JobHistory;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Model\JobHistory\EmpTerminate_Model;
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;

class EmpTerminate_Ctrl extends Controller {

protected $form = ['expat'=>26,'local'=>35,'local_it'=>44];
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "26";
		}elseif($db_data == 2){
				return $this->formx = "35";
		}else{
			return $this->formx = "44";
		}
}
private function check_id($id)
{
	$rule = ['undefined',NULL,''];
	if(in_array($id,$rule)){return 500;}else{return 200;}
}
private function set_valid()
{
	$reg = ['text'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
				'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
	$rule = ['termination'=>'required|numeric','termination_date'=>'date','note'=>$reg['text']];
	$valid = \Validator::make(\Input::all(),$rule); return $valid;
}

// INDEX
	public function index()
	{
		if(\Request::get('param')){
			$id = \Request::get('param');
		}else{ $id = null; }
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1); $model = new EmpTerminate_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					//$show = \DB::table('termination_reasons')->get(['id','title']);
					$show = \DB::select("call view_emp_termination_reason");
						if($show){ $data=$show; $status=200;}else{ $data=null; $status=200;}
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// SHOW
	public function show($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1); $model = new EmpTerminate_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				//$show = \DB::table('emp_termination_reason')->where('employee_id','=',$id)->get();
				$show = \DB::select("call view_emp_termination_reason");
					if($show){ $data=$show; $status=200;}else{ $data=[['id'=>null,'title'=>'Empty Record Data']]; $status=200; $message='Termination Reason : Empty Records Data.';}
				return \Response::json($data,$status);
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// SHOW TERMINATED EMP
	public function show_emp($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1); $model = new EmpTerminate_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$show = $model->ShowEmp($id);
				if($show){ $data=$show[0]; $status=200; $message='Terminate : Show Records Data.';}else{ $data=null; $message='Terminate : Empty Records Data.'; $status=200;}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// STORE
	public function store($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2); $model = new EmpTerminate_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{

				$valid = $this->set_valid();
				if($valid->fails()){ return $message='Required Input.'; $status=500; $data=null; }
				else{

					$input = $this->check_input($id);
					$check_emp = $model->check_emp($id);
					if(isset($check_emp) && $check_emp['status'] == 200){
						$get = $model->Store_EmpTerminate($id,$input['data']);
						if(isset($get) && $get != null && $get != '[]' && $get['status']==200){
							$message='Employee is Terminate.'; $status=$get['status']; $data=$get['data'];
						}else{ $message='Store Failed.'; $status=500; $data=null; }
					}else{ $message='Employee is Terminate.'; $status=500; $data=null; }
			}	}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// EDIT
	public function edit($id)
	{
		$get_role = $this->check();	$model = new EmpTerminate_Model; // SET MODEL
		if(isset($get_role['data']['role'])){ // CHECK KEY
				$access = $this->check_role($id,$get_role['data'],$this->form); // GET ACCESS
				if(isset($access['update']) && $access['update'] != 0){ // CHECK ACCESS
					$edit = $model->Read_EmpTerminate($id);
					if(isset($edit) != null && $edit['status'] == 200){
						$message='Read Data.'; $status=200; $data=$edit['data'];
					}else{ $message='ID not found.'; $status=500; $data=null; }
				}else{ $message=$access['message'];	$status=$access['status']; $data=null; }
		}
		else{ // for not access or error
			$message = $get_role['message']; $status=401; $data=null;
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// UPDATE
	public function update($id)
	{
		// if(\Request::get('param')){
		// 	$id = \Request::get('param');
		// }else{ $id = null; }
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>NULL,'Request'=>\Request::all()],$this->form,3); $model = new EmpTerminate_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$valid = $this->set_valid();
				if($valid->fails()){ return $message='Required Input.'; $status=500; $data=null; }
				else{
					$input = $this->check_input($id);
					$update = $model->Update_EmpTerminate($input['data'],$id);
					if(isset($update)){ $message='Update Successfully.'; $status=200; 	}
					else{ $message='Update Failed.'; $status=500;  }
			}	}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);

	}
// DELETE
	public function destroy($id)
	{
		// if(\Request::get('param')){
		// 	$id = \Request::get('param');
		// }else{ $id = null; }
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>NULL,'Request'=>\Request::all()],$this->form,4); $model = new EmpTerminate_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'destroy');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					$toArray = explode(",", $id);
					$data = $model->Destroy_EmpTerminate($toArray);
					if(isset($data)){
						$message = $data['message'];
						$status = $data['status'];
					}else{ $message = 'ID not found'; $status = 404; }
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);


	}
// SET INPUT
	private function check_input($id){
		$data = array(); // tampung input to array
		$data['reason'] 	= \Input::get('termination');
		$data['termination_date']		= \Input::get('termination_date');
		$data['note']		= \Input::get('note');

		if(isset($data)){
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }
		return ['status'=>$status,'data'=>$result];
	}
}
