<?php namespace Larasite\Http\Controllers\Employees\JobHistory;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Model\JobHistory\JobHistory_Model;
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
use Larasite\Library\FuncDocument;

class JobHistory_Ctrl extends Controller {

protected $form = ['expat'=>26,'local'=>35,'local_it'=>44];
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "26";
		}elseif($db_data == 2){
				return $this->formx = "35";
		}else{
			return $this->formx = "44";
		}
}
protected $dir = '/hrms_upload/job_history';
protected $ExtFile = ['title'=>'job_history','extend'=>['jpg','png','gif','pdf','jpeg','doc','doc','docx']];

private function check_id($id)
{
	$rule = ['undefined',NULL,''];
	if(in_array($id,$rule)){return 500;}else{ return 200; }
}
private function set_valid($input)
{
	$rule = ['training_agreement'=>'date','probationary_start'=>'date','contract_start_date'=>'date',
		'fixed_term_contract'=>'date','status'=>'numeric','job'=>'numeric','sub_unit'=>'numeric','emp_contract_start'=>'date','emp_contract_end'=>'date'];
	$input = null;
	$input = json_decode($input,1);
	$valid = \Validator::make(['training_agreement'=>$input['training_agreement'],
							'probationary_start'=>$input['probationary_start'],
							'contract_start_date'=>$input['contract_start_date'],
							'fixed_term_contract'=>$input['fixed_term_contract'],
							'status'=>$input['status'],
							'job'=>$input['job'],
							'sub_unit'=>$input['sub_unit'],
							'emp_contract_start'=>$input['emp_contract_start'],
							'emp_contract_end'=>$input['emp_contract_end']],$rule);
	return $valid;
}

public function Create_Doc($employee_id,$id){
	// FOR FUNGSI GENERATE DOC
		$result = \DB::table('document_template')->where('id','=',$id)->get(['content_text','size','margin_left','margin_top','margin_right','margin_bottom','template_name']);
		foreach ($result as $key) {
			$content = $key->content_text;
			$margin[] = $key->margin_left;
			$margin[] = $key->margin_top;
			$margin[] = $key->margin_right;
			$margin[] = $key->margin_bottom;
			$size = strtolower($key->size);
			$title = $key->template_name;
		}

		$match = preg_split('/<\/p>.*?<p>|<\/?p>/',$content,-1,PREG_SPLIT_NO_EMPTY);
		$jmlRow = count($match); $row = 10; $i=1; $data = null;

		// Split
		if($jmlRow > $row){
			foreach ($match as $key) {
				if($i == $row){ $data .= "##<p>".$key."</p>"; }
				else{ $data .= "<p>".$key."</p>"; }
				$i++;
			}
			$data = explode("##",$data);
		}else{
			foreach ($match as $key) {
				$data .= "<p>".$key."</p>";
				$i++;
			}
		}
		// Size
		if($size == "a4"){
			$size_data = ['y' => '595px', 'x' => '842px'];
		}elseif($size == "f4"){ $size_data =  ['y' => '792px', 'x' => '1247.04px'];}

		// Count Row
		if(count($data) > 1){
			foreach ($data as $key) { $generate[] = $this->parser_param($employee_id,$key);	}
		}else{ $generate = $this->parser_param($employee_id,$data);	 }

		$theme = $this->HeaderFooter($generate,$title,$margin,$size_data);
		$pdf = \App::make('dompdf.wrapper');
		$filename = $title.'-'.$employee_id.'.pdf';
		$pdf->loadHTML($theme)->setPaper($size)->setOrientation('portrait')->setWarnings(false)->save($filename);
		return $pdf->stream($filename,array('Attachment'=>0,'compress'=>1));
}
// DOWNLOAD FILE
	public function download($id,$filename){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{

				$check = \DB::select("SELECT filename from job_history where employee_id = '$id' and filename = '$filename'");
				 if(!$check){
				 	return \Response::json(['header'=>['message'=>'File not found']],404);
				 }

				foreach ($check as $key) { $files = $key->filename;}
				$path = storage_path()."/hrms_upload/job_history/".$files;
				$file = \File::get($path);
				$type = \File::mimeType($path);
				return \Response::make($file,200,['Content-Type'=>$type]);
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

// INDEX
	public function index()
	{
		if(\Request::get('param')){
			$id = \Request::get('param');
		}else{ $id = null; }
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			$result['job'] 		= \DB::table('job')->get(['id','title','filename']);
			$result['status'] 	= \DB::table('jobs_emp_status')->get(['id','title']);
			$result['department'] 	= \DB::select("select * from department where id > 1");
				 return $result ;
				if($result){
					$data=$result; $status=200; $message=null;
					return \Response::json($data,$status);
				}else{ $data=null; $status=404; $message=null;}
		}else{
		 $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud],'data'=>$data],$status);
	}
// STORE
	public function check_update($id,$inputan){
		$db = \DB::SELECT("CALL view_job_history('$id')");
		if($db == null){
			return "store";
		}
		$result = array_diff_assoc($inputan,$db);
		return $result;
	}

	public function SubRoot(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		if($access[1] == 200){
			$id = \Input::get('id_departmet');
			$check = \DB::SELECT("SELECT * FROM where parent = $id ");
			if($check == null){
				$message = "sub department not found";
				$status = 500;
				$data = [];
			}else{
				$count = $check;
				$message = "Succes found ".$count." department";
				$data = $check;
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function check_data($db,$input){
		if($db[0]->contract_start_date != $input['contract_start_date']){
			return "unmatch";
		}elseif ($db[0]->emp_contract_start != $input['emp_contract_start']){
			return "unmatch";
		}elseif ($db[0]->emp_contract_end != $input['emp_contract_end']){
			return "unmatch";
		}
		elseif ($db[0]->filename != \Input::file()){
			return "unmatch";
		}
		elseif ($db[0]->fixed_term_contract != $input['fixed_term_contract']){
			return "unmatch";
		}
		elseif ($db[0]->job != $input['job']){
			return "unmatch";
		}
		elseif ($db[0]->local_it != $input['local_it']){
			return "unmatch";
		}
		elseif ($db[0]->path != $input['path']){
			return "unmatch";
		}
		elseif ($db[0]->probationary_start != $input['probationary_start']){
			return "unmatch";
		}
		elseif ($db[0]->status != $input['status']){
			return "unmatch";
		}
		elseif ($db[0]->sub_unit != $input['sub_unit']){
			return "unmatch";
		}
		elseif ($db[0]->training_agreement != $input['training_agreement']){
			return "unmatch";
		}else{
			return "match";
		}
	}

	public function store($id){

		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2); $model= new JobHistory_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
				if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
				else{


					$db = \DB::SELECT( " select * from job_history where employee_id='$id' order by id limit 1 ");
				 	$input = $this->set_input();
				 	if(isset($input['sub_unit'])){
				 		$departments = $input['sub_unit'];
				 		if($input['subDepartment']){
							$input['sub_unit']	= $input['subDepartment'];
						}
				 	}

					if($db != null){
						if($this->check_data($db,$input) == "match"){
							return \Response::json(['header'=>['message'=>'The data has not change','status'=>200],'data'=>true],200);
						}else{
							$file =  \Input::file('file');
							$data = $this->SaveLog($id);
							$status = 1; $get = $model->view_logjobhistory($id);
							$save_log = ['status'=>$status,'data'=>$get];

							// menahan edit job his, melihat request dulu
							/*$chk_submitReq = \Db::SELECT("select * from leave_request where employee_id = '$id' ");
							if($chk_submitReq != null){
								return \Response::json(['header'=>['message'=>"Job History Locked , already request in leave request can't change data anymore",'status'=>500],'data'=>[]],500);
							}*/
							$ext = $file->getClientOriginalExtension();
							if($save_log['status'] == 1 && isset($file) && strlen($ext) > 2 ){

								$imageName = $file->getClientOriginalName();
								$ext = $file->getClientOriginalExtension();
								$size = $file->getSize();

								$rename = "Job_history_".str_random(6).".".$ext;
								$path = "hrms_upload/job_history";
								\Input::file('file')->move(storage_path($path),$rename);
								$STORE = \DB::SELECT("UPDATE  job_history SET emp_contract_start='$input[emp_contract_start]',emp_contract_end='$input[emp_contract_end]',training_agreement='$input[training_agreement]',probationary_start='$input[probationary_start]',job=$input[job],status=$input[status],sub_unit=$input[sub_unit],contract_start_date='$input[contract_start_date]',fixed_term_contract='$input[fixed_term_contract]',filename='$rename',path='$path' where employee_id='$id'  ");

								if(isset($input['sub_unit'])){

									if($input['sub_unit'] == null)
									{
										$input['sub_unit'] = 0;
									}

									$up = \DB::SELECT("update emp set department=$departments where employee_id='$id' ");
								}

								if(isset($input['subDepartment']) && $input['subDepartment'] != 0){
									$depId = $input['subDepartment'];

									if(isset($input['sub-department']) && $input['sub-department'] == 'subRoot' ){
										$depId = 0;
									}
									$upx = \DB::SELECT("select * from addOnDepartment where employee_id='$id' ");
									if($upx == null){
										$insert = \DB::SELECT(" insert into addOnDepartment(department_id,employee_id) values ($depId,'$id')");
									}else{
										$upd = \DB::SELECT("update addOnDepartment set department_id=$depId  where employee_id='$id' ");
									}
								}else{
									$upx = \DB::SELECT("select * from addOnDepartment where employee_id='$id' ");
									if($upx != null){
										$upd = \DB::SELECT("update addOnDepartment set department_id=0  where employee_id='$id' ");
									}
								}

								$local_it = $input['local_it'];
								if(isset($local_it)){
									$local_it = ($local_it == "both" ? 1 : ($local_it == "yes" ? 2  : 3));
									$up = \DB::SELECT("update emp set local_it=$local_it where employee_id='$id' ");
								}

								$datax = $this->show($id);
									if($datax){
											  return \Response::json(['header'=>['message'=>'Success to update data','status'=>200],'data'=>$datax],200);
									}else{
									      return \Response::json(['header'=>['message'=>'Failled to update','status'=>500],'data'=>[]],500);
									}
							}else{

								$rename = " ";
								$path = " ";
								if(isset($input['sub_unit'])){
									if($input['sub_unit'] == null)
									{
										$input['sub_unit'] = " ";
									}
									$up = \DB::SELECT("update emp set department=$input[sub_unit] where employee_id='$id' ");
								}
								if(isset($input['subDepartment']) && $input['subDepartment'] != 0){
									$depId = $input['subDepartment'];
									$upx = \DB::SELECT("select * from addOnDepartment where employee_id='$id' ");
									if($upx == null){
										$insert = \DB::SELECT(" insert into addOnDepartment(department_id,employee_id) values ($depId,'$id')");
									}else{
										$upd = \DB::SELECT("update addOnDepartment set department_id=$depId  where employee_id='$id' ");
									}
								}

								$STORE = \DB::SELECT("UPDATE  job_history SET emp_contract_start='$input[emp_contract_start]',emp_contract_end='$input[emp_contract_end]',training_agreement='$input[training_agreement]',probationary_start='$input[probationary_start]',job=$input[job],status=$input[status],sub_unit=$input[sub_unit],contract_start_date='$input[contract_start_date]',fixed_term_contract='$input[fixed_term_contract]',filename='$rename',path='$path' where employee_id='$id'  ");
								$datax = $this->show($id);
								if($datax){
										  return \Response::json(['header'=>['message'=>'Success to update data','status'=>200],'data'=>$datax],200);
								}else{
								      return \Response::json(['header'=>['message'=>'Failled to update','status'=>500],'data'=>[]],500);
								}
							}
						}
					}else{




							$file = \Input::file("file");
							$ext = $file->getClientOriginalExtension();
							if(isset($file) && $file !=  null && strlen($ext) > 2){

									$file =  \Input::file('file');
									$imageName = $file->getClientOriginalName();
									$ext = $file->getClientOriginalExtension();
									$size = $file->getSize();

									$rename = "Job_history_".str_random(6).".".$file->getClientOriginalExtension();
									$path = "hrms_upload/job_history";
									\Input::file('file')->move(storage_path($path),$rename);

							}else{
								$rename = " ";
								$path = " ";
							}

							$model= new JobHistory_Model;

							if(isset($input['subDepartment']) && $input['subDepartment'] != 0){

								$depId = $input['subDepartment'];
								$upx = \DB::SELECT("select * from addOnDepartment where employee_id='$id' ");
								if($upx == null){
									$insert = \DB::SELECT(" insert into addOnDepartment(department_id,employee_id) values ($depId,'$id')");
								}else{
									$upd = \DB::SELECT("update addOnDepartment set department_id=$depId  where employee_id='$id' ");
								}

							}
							if(isset($input['sub_unit'])){
								if($input['sub_unit'] == null)
								{
									$input['sub_unit'] = 0;
								}
								$up = \DB::SELECT("update emp set department=$input[sub_unit] where employee_id='$id' ");
							}
							$data = \DB::SELECT("insert into job_history(employee_id,emp_contract_start,emp_contract_end,training_agreement,probationary_start,job,status,sub_unit,contract_start_date,fixed_term_contract,filename,path,created_at)
									     values('$id','$input[emp_contract_start]','$input[emp_contract_end]','$input[training_agreement]','$input[probationary_start]',$input[job],$input[status],$input[sub_unit],'$input[contract_start_date]','$input[fixed_term_contract]','$rename','$path',now())");
							$datax = $this->show($id);

							return \Response::json(['header'=>['message'=>'Success insert data ','status'=>200],'data'=>$datax],200);

					}

				 }

		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access"  => $crud[3]],'data'=>$data],$status);
	}
// CREATE LOG
	private function SaveLog($id){
			$model = new JobHistory_Model;
			$select = \DB::select("SELECT id from job_history where employee_id = '$id'");
			if($select){
					//akan melakukan update dan membuat log data
				$select = \DB::select("SELECT id,employee_id,training_agreement,contract_start_date,probationary_start,fixed_term_contract,job,status,sub_unit,emp_contract_start,emp_contract_end,filename,`path`
									from job_history where employee_id = $id order by id DESC limit 1");

					foreach ($select as $key) {
						$id_jobhistory_old = $key->id;
						$data['employee_id'] = $key->employee_id;
						$data['training_agreement'] = $key->training_agreement;
						$data['contract'] = $key->contract_start_date;
						$data['probationary'] = $key->probationary_start;
						$data['fixed_term_contract'] = $key->fixed_term_contract;
						$data['job'] = $key->job;
						$data['status'] = $key->status;
						$data['sub_unit'] = $key->sub_unit;
						$data['contract_start_date'] = $key->emp_contract_start;
						$data['contract_end_date'] = $key->emp_contract_end;
						$data['filename'] = $key->filename;
						$data['path'] = $key->path;
					}
						return $data;
					// // create log
					// \DB::table('log_jobhistory')->insert($data);
					// $status = 1; $get = $model->view_logjobhistory($id);

			}
		// 	else{ $status = null; $get = null; }
		// return ['status'=>$status,'data'=>$get];
	}
// UPDATE
	private function UpdatePic($id,$file){
		$model = new JobHistory_Model; $upload =  $this->upload_seminar($file); $check = $model->check_file_id($id);

		if($check != null && $upload['filename']){
			$move = $this->move_file($check);
			if($move['status'] == 200){
				$file->move(storage_path($upload['path']),$upload['filename']);
				$status = 200;
			}else{$status=202;	}
		}elseif($upload['filename']){
			$file->move(storage_path($upload['path']),$upload['filename']);
			$status = 200;
		}else{ $status = 406; $upload['filename'] = null; $upload['path'] = null; }
		return ['status'=>$status,'filename'=>$upload['filename'],'path'=>$upload['path']];
	}
	private function update_history($id) // PUT FIX***
	{
		$model = new JobHistory_Model; $FuncUpload = new FuncUpload;
		$file = \Input::file('file'); $inputan = $this->set_input(); $radio =NUll;
		$rename = "";
		$localit = "";
		return 1;
		if($this->formx == "26"){ $localit = 1;}elseif($this->formx == "35"){$localit = 2;}else{$localit = 3;}
		if($file != null){
			return $file;
			$imageName = $file->getClientOriginalName();
			$ext = $file->getClientOriginalExtension();
			$size = $file->getSize();
		// 	$STORE = \DB::SELECT(" CALL update_job_history('$id','$inputan[fixed_term_contract]','$inputan[training_agreement]','$inputan[probationary_start]','$inputan[contract_start_date]',$inputan[job],$inputan[status],$inputan[sub_unit],$localit,1,'$inputan[emp_contract_start]',$inputan[emp_contract_end],'$inputan[filename]','$inputan[path]') ");

		}else{ $status = 500; $message = 'Input not complete.'; }
		return ['message'=>$message,'status'=>$status];
	}

	private function store_history($id) // PUT FIX***
	{
		$model 	= new JobHistory_Model; $FuncUpload = new FuncUpload;
		$file = \Input::file('file'); $inputfan = $this->set_input();
		if($file != null){

			$imageName = $file->getClientOriginalName();
			return $imageName;
			$ext = $file->getClientOriginalExtension();
			$size = $file->getSize();
			$path = "hrms_upload/job_history";
			$rename = "job_history_".str_random(6).'.'.$ext;
			if($ext != "pdf" || $ext != "png" || $ext != "jpg"){
				$message = "can't upload image with format ".$ext." ,support only .png, .jpg, .pdf";
				$status = 500;
				$data = [];
			}elseif($size > 148576){
				$size = $size / 100000;
				$message = "file size to large ".$size." ,please upload less than 1 MB";
				$status = 500;
				$data = [];
			}else{
				Input::file('file')->move(storage_path($path),$rename);
			}


			$inputan['path'] = $path; $inputan['filename'] = $rename ;
			 $STORE = \DB::SELECT("insert into job_history(employee_id,emp_contract_start,emp_contract_end,training_agreement,probationary_start,job,status,sub_unit,contract_start_date,fixed_term_contract,filename,path) values('$id','$inputan[emp_contract_start]','$inputan[emp_contract_end]','$inputan[training_agreement]','$inputan[probationary_start]',$inputan[job],$inputan[status],$inputan[sub_unit],'$inputan[contract_start_date]','$inputan[fixed_term_contract]','$inputan[filename]','$inputan[path]') ");
			 if(isset($input['sub_unit'])){
				 if($input['sub_unit'] == null)
				 {
					 $input['sub_unit'] = " ";
				 }
				 $up = \DB::SELECT("update emp set department=$input[sub_unit] where employee_id='$id' ");
			 }
			//$STORE = \DB::SELECT("CALL insert_job_history('$id','$inputan[contract_start_date]','$inputan[probationary_start]','$inputan[fixed_term_contract]',$inputan[job],$inputan[status],$inputan[sub_unit],'$inputan[emp_contract_start]','$inputan[emp_contract_end]','$inputan[filename]','$inputan[path]',1)");
			$message = "Success "; $status = 200; $data = $this->show($id);
		}elseif($file == null){
			$inputan['path'] = "null"; $inputan['filename'] = "null";
			 $STORE = \DB::SELECT("insert into job_history(employee_id,emp_contract_start,emp_contract_end,training_agreement,probationary_start,job,status,sub_unit,contract_start_date,fixed_term_contract,filename,path) values('$id','$inputan[emp_contract_start]','$inputan[emp_contract_end]','$inputan[training_agreement]','$inputan[probationary_start]',$inputan[job],$inputan[status],$inputan[sub_unit],'$inputan[contract_start_date]','$inputan[fixed_term_contract]','$inputan[filename]','$inputan[path]') ");
			 if(isset($input['sub_unit'])){
				 if($input['sub_unit'] == null)
				 {
					 $input['sub_unit'] = " ";
				 }
				 $up = \DB::SELECT("update emp set department=$input[sub_unit] where employee_id='$id' ");
			 }
			if(isset($STORE)){
					$message = "success"; $status = 200; $data =$this->show($id);
			}
		}else{ $status = 500; $message = 'Input not complete.'; $data = null; }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
// SHOW DATA
	private function show_data_jobhistory($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		$model = new JobHistory_Model;
		$show = $model->Read_JobHistory($id);
		$data['action_terminate'] = $model->getTerminate(['emp'=>$id,'key'=>$this->getKey()]);
		if( $data['action_terminate'] == true ){
			$data['terminate'] = $model->ambil_data_terminate($id);
		}

		$local = $model->getReq($id);
		$key = $this->getKey();
		$checkKey = \DB::SELECT("select * from ldap where employee_id='$key' ");
		if($checkKey[0]->role_id == 33){
			$data['key'] = 'su';
		}else{
			$data['key'] = 'us';
		}

		if(!isset($show['data']['current_data']['sub-department'])){
			$show['data']['current_data']['sub-department'] = null;
		}
		if($show['data']['current_data']['sub-department'] == 'subDepart' ){

			$data['sub_department'] = [$show['data']['current_data']['sub-department-id']];
		}else{
			$checkSub = \DB::SELECT("select department.id,department.name,department.parent from department,addOnDepartment where addOnDepartment.employee_id='$id' and  addOnDepartment.department_id = department.id ");
			if($checkSub == null){
				$checkSub = [];
			}
			$data['sub_department'] = $checkSub;
		}
		if($local['typeLocal'] == 1){ $data['local_it'] = 'both'; }
		elseif($local['typeLocal'] == 2){ $data['local_it'] = 'yes'; }
		elseif($local['typeLocal'] == 3){ $data['local_it'] = 'no'; }

		if(isset($id) && $id != 'undefined'){
			if(isset($show) && $show['status'] == 200){
				$data['job_history'] = $show['data']; $status=$show['status']; $message='Show record.';
			}else{ $status=200; $message='Empty records data.'; $data['job_history'] = $show['data'];}
		}else{ $status=404; $message='Empty records data.'; }

		return ['data'=>$data,'status'=>$status,'message'=>$message, "access" => $crud[3]];
	}
// SHOW
	public function show($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			//return 1;
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					if($access[2][0] == 5 && $access[2][1] == $access[2][2] ){
						//return $access;
						$datas = $this->show_data_jobhistory($access[2][1]);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}else{
						$datas = $this->show_data_jobhistory($id);

						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// DESTROY
	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>NULL,'Request'=>\Request::all()],$this->form,1);
		$model 	= new JobHistory_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'destroy');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					$toArray = explode(",", $id);
					foreach ($toArray as $key => $value) {
						\DB::SELECT("delete from log_jobhistory where id=$value");
					}
					$data = [];
					$status = 200;
					$message =  "Success";
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// SET INPUT
	private function set_input(){
		$data = array(); // tampung input to array
		$dataString = \Input::get('data');
		$data = json_decode($dataString,1);
		if(isset($data['training_agreement'])){	// training agreemrnt
			$result['training_agreement'] = $data['training_agreement'];
		}elseif(\Input::get('training_agreement') != null){
			$result['training_agreement'] = \Input::get("training_agreement");
		}else{
			$result['training_agreement'] = "0000-00-00";
		}

		if(isset($data['sub-department'])){	// training agreemrnt
			$result['sub-department'] = $data['sub-department'];
		}elseif(\Input::get('sub-department') != null){
			$result['sub-department'] = \Input::get("sub-department");
		}else{
			$result['sub-department'] = 0;
		}

		if(isset($data['contract_start_date'])){ 	// contract_start_date
			$result['contract_start_date'] = $data['contract_start_date'];
		}elseif(\Input::get('contract_start_date') != null){
			$result['contract_start_date'] = \Input::get("contract_start_date");
		}
		else{
			$result['contract_start_date']  = '0000-00-00';
		}

		if(isset($data['fixed_term_contract'])){ 	// contract_start_date
			$result['fixed_term_contract'] = $data['fixed_term_contract'];
		}elseif(\Input::get('fixed_term_contract') != null){
			$result['fixed_term_contract'] = \Input::get('fixed_term_contract');
		}
		else{
			$result['fixed_term_contract']  = "0000-00-00";
		}

		if(isset($data['subDepartment'])){ 	//subDepartment
			$result['subDepartment'] = $data['subDepartment'];
		}elseif(\Input::get('subDepartment') != null){
			$result['subDepartment'] = \Input::get('subDepartment');
		}
		else{
			$result['subDepartment']  = null;
		}

		if(isset($data['probationary_start'])){ 	// probationary
			$result['probationary_start'] = $data['probationary_start'];
		}elseif(\Input::get('probationary_start') != null){
			$result['probationary_start'] = \Input::get('probationary_start');
		}
		else{
			$result['probationary_start']  = "0000-00-00";
		}
		if(isset($data['job'])){ 	// job
			$result['job'] = $data['job'];
		}elseif(\Input::get('job') != null){
			$result['job'] = \Input::get('job');
		}
		else{
			$result['job']  = 0;
		}
		if(isset($data['status'])){ 	// status
			$result['status'] = $data['status'];
		}elseif(\Input::get('status') != null){
			$result['status'] = \Input::get('status');
		}
		else{
			$result['status']  = 0;
		}
		if(isset($data['sub_unit'])){ // sub_unit
			$result['sub_unit'] = $data['sub_unit'];
		}elseif(\Input::get('sub_unit') != null){
			$result['sub_unit'] = \Input::get('sub_unit');
		}
		else{
			$result['sub_unit']  = 0;
		}
		if(isset($data['emp_contract_start'])){ 	// emp_contract_start
			$result['emp_contract_start'] = $data['emp_contract_start'];
		}elseif ( \Input::get('emp_contract_start') != null){
			$result['emp_contract_start'] = \Input::get('emp_contract_start');
		}
		else{
			$result['emp_contract_start']  = '0000-00-00';
		}
		if(isset($data['emp_contract_end'])){ 	// emp_contract_end
			$result['emp_contract_end'] = $data['emp_contract_end'];
		}elseif( \Input::get('emp_contract_end') != null){
			$result['emp_contract_end'] = \Input::get('emp_contract_end');
		}
		else{
			$result['emp_contract_end']  = '0000-00-00';
		}
		if(isset($data['emp_contract_end'])){ 	// emp_contract_end
			$result['emp_contract_end'] = $data['emp_contract_end'];
		}elseif( \Input::get('emp_contract_end') != null){
			$result['emp_contract_end'] = \Input::get('emp_contract_end');
		}
		else{
			$result['emp_contract_end']  = "0000-00-00";
		}
		if(isset($data['local_it'])){ 	// emp_contract_end
			$result['local_it'] = $data['local_it'];
		}elseif( \Input::get('local_it') != null){
			$result['local_it'] = \Input::get('local_it');
		}
		else{
			$result['local_it']  = 0;
		}
		return $result;
	}

	// CHECK INPUT VALUE #################################################################################
// GET KEY
	private function getKey()
	{	

		$token = \Request::get('key');
		$token1 = \Input::get('key');

		$decode = base64_decode(\Request::get('key'));
		if(isset($token1)){
			if(strlen($token1) < strlen($token)){
				$decode = base64_decode(\Input::get('key'));	
			}
		}
		
	 	return  substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
	}
// MOVE
	private function Move($id,$file,$type){
		$model = new JobHistory_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		$check = $model->check_file_id($file['employee_id']);
		if($check){
			if($type == 'cancel'){
				$move = $FuncUpload->move_file($check,'job_history');
				$message='Cancel Operation.'; $status=200; $data = null;
			}
			elseif($type == 'delfile'){
				$move = $FuncUpload->move_file($check,'job_history');
				$message=$move['message']; $status=$move['message']; $data=NULL;
			}
			else{
					$up = $this->Upload($id,$file,false);
					$message=$up[0]; $status=$up[1]; $data=$up[2];
			}
		}else{ $up = $this->Upload($id,$file,true); $message=$up[0]; $status=$up[1]; $data=$up[2]; }
		return [$message,$status,$data];
	}
// UPLOAD
	private function Upload($id,$files,$type){
		$model = new JobHistory_Model; /*Upload*/ $FuncUpload = new FuncUpload; $dir = $this->dir;
		$file = $files['file']; $inputan = $this->set_input();
		$upload =  $FuncUpload->upload($this->dir,$file,$this->ExtFile);
		if($upload['filename']){
			if($type == true){

				$inputan['path'] = $upload['path']; $inputan['filename'] = $upload['filename'];
				$STORE = $model->Store_JobHistory($files['employee_id'],$inputan);
				if($STORE['status'] == 200){ $file->move(storage_path($upload['path']),$upload['filename']); }
				$message=$STORE['message']; $status=$STORE['status']; $data=$STORE['data'];

			}else{
				$inputan['path'] = $upload['path']; $inputan['filename'] = $upload['filename'];
				$PUT = $model->Update_JobHistory($id,$inputan);
				if($PUT['status'] == 200){ $file->move(storage_path($upload['path']),$upload['filename']); }
				$message=$PUT['message']; $status=$PUT['status']; $data=$PUT['data'];
			}
		}else{ $message='Update Failed.'; $status=500; $data = $update;}
		return [$message,$status,$data];
	}
// END UPLOAD
}
