<?php namespace Larasite\Http\Controllers\Employees\JobHistory;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Model\JobHistory\JobHistory_Model;
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
use Larasite\Library\FuncDocument;

class JobHistory_Ctrl extends Controller {

protected $form = ['expat'=>26,'local'=>35,'local_it'=>44];
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "26";
		}elseif($db_data == 2){
				return $this->formx = "35";
		}else{
			return $this->formx = "44";
		}
}
protected $dir = '/hrms_upload/job_history';
protected $ExtFile = ['title'=>'job_history','extend'=>['jpg','png','gif','pdf','jpeg','doc','doc','docx']];

private function check_id($id)
{
	$rule = ['undefined',NULL,''];
	if(in_array($id,$rule)){return 500;}else{ return 200; }
}
private function set_valid($input)
{
	$rule = ['training_agreement'=>'date','probationary_start'=>'date','contract_start_date'=>'date',
		'fixed_term_contract'=>'date','status'=>'numeric','job'=>'numeric','sub_unit'=>'numeric','emp_contract_start'=>'date','emp_contract_end'=>'date'];
	$input = null;
	$input = json_decode($input,1);
	$valid = \Validator::make(['training_agreement'=>$input['training_agreement'],
							'probationary_start'=>$input['probationary_start'],
							'contract_start_date'=>$input['contract_start_date'],
							'fixed_term_contract'=>$input['fixed_term_contract'],
							'status'=>$input['status'],
							'job'=>$input['job'],
							'sub_unit'=>$input['sub_unit'],
							'emp_contract_start'=>$input['emp_contract_start'],
							'emp_contract_end'=>$input['emp_contract_end']],$rule);
	return $valid;
}

public function Create_Doc($employee_id,$id){
	// FOR FUNGSI GENERATE DOC
		$result = \DB::table('document_template')->where('id','=',$id)->get(['content_text','size','margin_left','margin_top','margin_right','margin_bottom','template_name']);
		foreach ($result as $key) {
			$content = $key->content_text;
			$margin[] = $key->margin_left;
			$margin[] = $key->margin_top;
			$margin[] = $key->margin_right;
			$margin[] = $key->margin_bottom;
			$size = strtolower($key->size);
			$title = $key->template_name;
		}

		$match = preg_split('/<\/p>.*?<p>|<\/?p>/',$content,-1,PREG_SPLIT_NO_EMPTY);
		$jmlRow = count($match); $row = 10; $i=1; $data = null;

		// Split
		if($jmlRow > $row){
			foreach ($match as $key) {
				if($i == $row){ $data .= "##<p>".$key."</p>"; }
				else{ $data .= "<p>".$key."</p>"; }
				$i++;
			}
			$data = explode("##",$data);
		}else{
			foreach ($match as $key) {
				$data .= "<p>".$key."</p>";
				$i++;
			}
		}
		// Size
		if($size == "a4"){
			$size_data = ['y' => '595px', 'x' => '842px'];
		}elseif($size == "f4"){ $size_data =  ['y' => '792px', 'x' => '1247.04px'];}

		// Count Row
		if(count($data) > 1){
			foreach ($data as $key) { $generate[] = $this->parser_param($employee_id,$key);	}
		}else{ $generate = $this->parser_param($employee_id,$data);	 }

		$theme = $this->HeaderFooter($generate,$title,$margin,$size_data);
		$pdf = \App::make('dompdf.wrapper');
		$filename = $title.'-'.$employee_id.'.pdf';
		$pdf->loadHTML($theme)->setPaper($size)->setOrientation('portrait')->setWarnings(false)->save($filename);
		return $pdf->stream($filename,array('Attachment'=>0,'compress'=>1));
}
// DOWNLOAD FILE
	public function download($id,$filename){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{

				$check = \DB::select("SELECT filename from job_history where employee_id = '$id' and filename = '$filename'");
				 if(!$check){
				 	return \Response::json(['header'=>['message'=>'File not found']],404);
				 }

				foreach ($check as $key) { $files = $key->filename;}
				$path = storage_path()."/hrms_upload/job_history/".$files;
				$file = \File::get($path);
				$type = \File::mimeType($path);
				return \Response::make($file,200,['Content-Type'=>$type]);
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

// INDEX
	public function index()
	{
		if(\Request::get('param')){
			$id = \Request::get('param');
		}else{ $id = null; }
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			$result['job'] 		= \DB::table('job')->get(['id','title','filename']);
			$result['status'] 	= \DB::table('jobs_emp_status')->get(['id','title']);
			$result['department'] 	= \DB::select("CALL view_department1") ;
				return $result ;
				if($result){
					$data=$result; $status=200; $message=null;
					return \Response::json($data,$status);
				}else{ $data=null; $status=404; $message=null;}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud],'data'=>$data],$status);
	}
// STORE
	public function check_update($id,$inputan){
		$db = \DB::SELECT("CALL view_job_history('$id')");
		if($db == null){
			return "store";
		}
		$result = array_diff_assoc($inputan,$db);
		return $result;
	}
	public function store($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2); $model= new JobHistory_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
				if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
				else{
					
					$inputan = $this->set_input();
					$valid = $this->set_valid($inputan);
					
					if($valid->fails()){ $message='Failed, Required Input.'; $status=500; $data=null; }
					else{
						
						if($this->check_update($id,$inputan) == null  && $this->check_update($id,$inputan) != "store"){

							return \Response::json(['header'=>['message'=>'The data has not change','status'=>200],'data'=>true],200);
						}

						$save_log = $this->SaveLog($id);
						if($save_log['status']){
							
							$UPDATE = $this->update_history($id);
							$status = $UPDATE['status']; $message = $UPDATE['message']; $data = $save_log['data'];
						}else{
							
							$STORE = $this->store_history($id);
							$status = $STORE['status']; $message = $STORE['message']; $data = 1;
						}
				
				}	}

				
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access"  => $crud[3]],'data'=>$data],$status);
	}
// CREATE LOG
	private function SaveLog($id){
			$model = new JobHistory_Model;
			$select = \DB::select("SELECT id from job_history where employee_id = '$id'");
			if($select){
					//akan melakukan update dan membuat log data
				$select = \DB::select("SELECT id,employee_id,training_agreement,contract_start_date,probationary_start,fixed_term_contract,job,status,sub_unit,emp_contract_start,emp_contract_end,filename,`path`
									from job_history where employee_id = $id");
					foreach ($select as $key) {
						$id_jobhistory_old = $key->id;
						$data['employee_id'] = $key->employee_id;
						$data['training_agreement'] = $key->training_agreement;
						$data['contract'] = $key->contract_start_date;
						$data['probationary'] = $key->probationary_start;
						$data['fixed_term_contract'] = $key->fixed_term_contract;
						$data['job'] = $key->job;
						$data['status'] = $key->status;
						$data['sub_unit'] = $key->sub_unit;
						$data['contract_start_date'] = $key->emp_contract_start;
						$data['contract_end_date'] = $key->emp_contract_end;
						$data['filename'] = $key->filename;
						$data['path'] = $key->path;
					}
					// create log
					\DB::table('log_jobhistory')->insert($data);
					$status = 1; $get = $model->view_logjobhistory($id);
			}else{ $status = null; $get = null; }
		return ['status'=>$status,'data'=>$get];
	}
// UPDATE
	private function UpdatePic($id,$file){
		$model = new JobHistory_Model; $upload =  $this->upload_seminar($file); $check = $model->check_file_id($id);

		if($check != null && $upload['filename']){
			$move = $this->move_file($check);
			if($move['status'] == 200){
				$file->move(storage_path($upload['path']),$upload['filename']);
				$status = 200;
			}else{$status=202;	}
		}elseif($upload['filename']){
			$file->move(storage_path($upload['path']),$upload['filename']);
			$status = 200;
		}else{ $status = 406; $upload['filename'] = null; $upload['path'] = null; }
		return ['status'=>$status,'filename'=>$upload['filename'],'path'=>$upload['path']];
	}
	private function update_history($id) // PUT FIX***
	{
		$model = new JobHistory_Model; $FuncUpload = new FuncUpload;
		$file = \Input::file('file'); $inputan = $this->set_input(); $radio =NUll;
		$rename = "";
		$localit = "";
		if($this->formx == "26"){ $localit = 1;}elseif($this->formx == "35"){$localit = 2;}else{$localit = 3;}
		if($file != null){
			
			$imageName = $file->getClientOriginalName();
			$ext = $file->getClientOriginalExtension();
			$size = $file->getSize();
			$message = "success"; $status = 200; 	$data = $ext;
		// 	if($ext != "pdf" || $ext != "png" || $ext != "jpg"){
		// 		$message = "can't upload image with format ".$ext." ,support only .png, .jpg, .pdf";
		// 		$status = 500;
		// 		$data = [];				
		// 	}elseif($size > 148576){
		// 		$size = $size / 100000;
		// 		$message = "file size to large ".$size." ,please upload less than 1 MB";
		// 		$status = 500;
		// 		$data = [];
		// 	}else{
		// 		$rename = "job_history_".str_random(6).$ext;
		// 		$file->move(storage_path("hrms_upload/job_history"),$rename);
		// 	}
		// 	$inputan['path'] = "hrms_upload/job_history"; $inputan['filename'] = $rename ;


		// 	$STORE = \DB::SELECT(" CALL update_job_history('$id','$inputan[fixed_term_contract]','$inputan[training_agreement]','$inputan[probationary_start]','$inputan[contract_start_date]',$inputan[job],$inputan[status],$inputan[sub_unit],$localit,1,'$inputan[emp_contract_start]',$inputan[emp_contract_end],'$inputan[filename]','$inputan[path]') ");
		// 	if(isset($STORE) && isset($destroy)){
		// 			$message = "success"; $status = 200; $data = $res = $model->view_jobhistory($id);;
		// 	}
		// }elseif($file == null){
		// 	$inputan['path'] = "null"; $inputan['filename'] = "null";
		// 	$STORE = \DB::SELECT(" CALL update_job_history('$id','$inputan[fixed_term_contract]','$inputan[training_agreement]','$inputan[probationary_start]','$inputan[contract_start_date]',$inputan[job],$inputan[status],$inputan[sub_unit],$localit,1,'$inputan[emp_contract_start]',$inputan[emp_contract_end],'$inputan[filename]','$inputan[path]') ");

		// 	if(isset($STORE) && isset($destroy)){
		// 			$message = "success"; $status = 200; $data =  $model->view_jobhistory($id);
			//}

			//$message = $PUT['message']; $status = $PUT['status']; 	$data = $PUT['data'];
		}else{ $status = 500; $message = 'Input not complete.'; }
		return ['message'=>$message,'status'=>$status];
	}

	private function store_history($id) // PUT FIX***
	{
		$model 	= new JobHistory_Model; $FuncUpload = new FuncUpload;
		$file = \Input::file('file'); $inputan = $this->set_input();
		
		if($file != null){
			
			$imageName = $file->getClientOriginalName();
			$ext = $file->getClientOriginalExtension();
			$size = $file->getSize();
			$path = "hrms_upload/job_history";
			$rename = "job_history_".str_random(6).'.'.$ext;
			if($ext != "pdf" || $ext != "png" || $ext != "jpg"){
				$message = "can't upload image with format ".$ext." ,support only .png, .jpg, .pdf";
				$status = 500;
				$data = [];				
			}elseif($size > 148576){
				$size = $size / 100000;
				$message = "file size to large ".$size." ,please upload less than 1 MB";
				$status = 500;
				$data = [];
			}else{
				Input::file('file')->move(storage_path($path),$rename);
			}

		
			$inputan['path'] = $path; $inputan['filename'] = $rename ;
			 $STORE = \DB::SELECT("insert into job_history(employee_id,emp_contract_start,emp_contract_end,training_agreement,probationary_start,job,status,sub_unit,contract_start_date,fixed_term_contract,filename,path) values('$id','$inputan[emp_contract_start]','$inputan[emp_contract_end]','$inputan[training_agreement]','$inputan[probationary_start]',$inputan[job],$inputan[status],$inputan[sub_unit],'$inputan[contract_start_date]','$inputan[fixed_term_contract]','$inputan[filename]','$inputan[path]') ");
			//$STORE = \DB::SELECT("CALL insert_job_history('$id','$inputan[contract_start_date]','$inputan[probationary_start]','$inputan[fixed_term_contract]',$inputan[job],$inputan[status],$inputan[sub_unit],'$inputan[emp_contract_start]','$inputan[emp_contract_end]','$inputan[filename]','$inputan[path]',1)");
			$message = "Success "; $status = 200; $data = $model->view_jobhistory($id);
		}elseif($file == null){
			$inputan['path'] = "null"; $inputan['filename'] = "null";
			 $STORE = \DB::SELECT("insert into job_history(employee_id,emp_contract_start,emp_contract_end,training_agreement,probationary_start,job,status,sub_unit,contract_start_date,fixed_term_contract,filename,path) values('$id','$inputan[emp_contract_start]','$inputan[emp_contract_end]','$inputan[training_agreement]','$inputan[probationary_start]',$inputan[job],$inputan[status],$inputan[sub_unit],'$inputan[contract_start_date]','$inputan[fixed_term_contract]','$inputan[filename]','$inputan[path]') ");
			if(isset($STORE)){
					$message = "success"; $status = 200; $data =$model->view_jobhistory($id);
			}
		}else{ $status = 500; $message = 'Input not complete.'; $data = null; }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
// SHOW DATA
	private function show_data_jobhistory($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		$model = new JobHistory_Model;
		$show = $model->Read_JobHistory($id);
		$data['action_terminate'] = $model->getTerminate(['emp'=>$id,'key'=>$this->getKey()]);
		if( $data['action_terminate'] == true ){
			$data['terminate'] = $model->ambil_data_terminate($id);
		}
		$local = $model->getReq($id);
		if($local['typeLocal'] == 1){ $data['local_it'] = 'both'; }
		elseif($local['typeLocal'] == 2){ $data['local_it'] = 'yes'; }
		elseif($local['typeLocal'] == 3){ $data['local_it'] = 'no'; }

		if(isset($id) && $id != 'undefined'){
			if(isset($show) && $show['status'] == 200){
				$data['job_history'] = $show['data']; $status=$show['status']; $message='Show record.';
			}else{ $status=200; $message='Empty records data.'; $data['job_history'] = $show['data'];}
		}else{ $status=404; $message='Empty records data.'; }

		return ['data'=>$data,'status'=>$status,'message'=>$message, "access" => $crud[3]];
	}
// SHOW
	public function show($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					if($access[2][0] == 5 && $access[2][1] == $access[2][2] ){
						$datas = $this->show_data_jobhistory($access['message']['employee_id']);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}else{
						$datas = $this->show_data_jobhistory($id);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// DESTROY
	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>NULL,'Request'=>\Request::all()],$this->form,1);
		$model 	= new JobHistory_Model; 
		$crud = $FRA->Access(\Request::all(),$this->formx,'destroy');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					$toArray = explode(",", $id);
					foreach ($toArray as $key => $value) {
						\DB::SELECT("delete from log_jobhistory where id=$value");
					}
					$data = [];
					$status = 200;
					$message =  "Success";
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// SET INPUT
	private function set_input(){
		$data = array(); // tampung input to array
		$dataString = \Input::get('data');
		$data = json_decode($dataString,1);
		if(isset($data['training_agreement'])){	// training agreemrnt
			$result['training_agreement'] = $data['training_agreement'];
		}elseif(\Input::get('training_agreement') != null){
			$result['training_agreement'] = \Input::get("training_agreement");
		}else{
			$result['training_agreement'] = "null";
		}

		if(isset($data['contract_start_date'])){ 	// contract_start_date
			$result['contract_start_date'] = $data['contract_start_date'];
		}elseif(\Input::get('contract_start_date') != null){
			$result['contract_start_date'] = \Input::get("contract_start_date");
		}
		else{
			$result['contract_start_date']  = "null";
		}

		if(isset($data['fixed_term_contract'])){ 	// contract_start_date
			$result['fixed_term_contract'] = $data['fixed_term_contract'];
		}elseif(\Input::get('fixed_term_contract') != null){
			$result['fixed_term_contract'] = \Input::get('fixed_term_contract');
		}
		else{
			$result['fixed_term_contract']  = "null";
		}

		if(isset($data['probationary_start'])){ 	// probationary
			$result['probationary_start'] = $data['probationary_start'];
		}elseif(\Input::get('probationary_start') != null){
			$result['probationary_start'] = \Input::get('probationary_start');
		}
		else{
			$result['probationary_start']  = "null";
		}
		if(isset($data['job'])){ 	// job
			$result['job'] = $data['job'];
		}elseif(\Input::get('job') != null){
			$result['job'] = \Input::get('job');
		}
		else{
			$result['job']  = "null";
		}
		if(isset($data['status'])){ 	// status
			$result['status'] = $data['status'];
		}elseif(\Input::get('status') != null){
			$result['status'] = \Input::get('status');
		}
		else{
			$result['status']  = "null";
		}
		if(isset($data['sub_unit'])){ // sub_unit
			$result['sub_unit'] = $data['sub_unit'];
		}elseif(\Input::get('sub_unit') != null){
			$result['sub_unit'] = \Input::get('sub_unit');
		}
		else{
			$result['sub_unit']  = "null";
		}
		if(isset($data['emp_contract_start'])){ 	// emp_contract_start
			$result['emp_contract_start'] = $data['emp_contract_start'];
		}elseif ( \Input::get('emp_contract_start') != null){
			$result['emp_contract_start'] = \Input::get('emp_contract_start');
		}
		else{
			$result['emp_contract_start']  = "null";
		}
		if(isset($data['emp_contract_end'])){ 	// emp_contract_end
			$result['emp_contract_end'] = $data['emp_contract_end'];
		}elseif( \Input::get('emp_contract_end') != null){
			$result['emp_contract_end'] = \Input::get('emp_contract_end');
		}
		else{
			$result['emp_contract_end']  = "null";
		}
		if(isset($data['include'])){ 	// emp_contract_end
			$result['include'] = $data['include'];
		}elseif(\Input::get('include') != null ){
			$result['include'] = \Input::get('include');
		}
		else{
			$result['include']  = "null";
		}

		return $result;
	}

	// CHECK INPUT VALUE #################################################################################
// GET KEY
	private function getKey()
	{
		$decode = base64_decode(\Request::get('key'));
	 	return  substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
	}
// MOVE
	private function Move($id,$file,$type){
		$model = new JobHistory_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		$check = $model->check_file_id($file['employee_id']);
		if($check){
			if($type == 'cancel'){
				$move = $FuncUpload->move_file($check,'job_history');
				$message='Cancel Operation.'; $status=200; $data = null;
			}
			elseif($type == 'delfile'){
				$move = $FuncUpload->move_file($check,'job_history');
				$message=$move['message']; $status=$move['message']; $data=NULL;
			}
			else{
					$up = $this->Upload($id,$file,false);
					$message=$up[0]; $status=$up[1]; $data=$up[2];
			}
		}else{ $up = $this->Upload($id,$file,true); $message=$up[0]; $status=$up[1]; $data=$up[2]; }
		return [$message,$status,$data];
	}
// UPLOAD
	private function Upload($id,$files,$type){
		$model = new JobHistory_Model; /*Upload*/ $FuncUpload = new FuncUpload; $dir = $this->dir;
		$file = $files['file']; $inputan = $this->set_input();
		$upload =  $FuncUpload->upload($this->dir,$file,$this->ExtFile);
		if($upload['filename']){
			if($type == true){

				$inputan['path'] = $upload['path']; $inputan['filename'] = $upload['filename'];
				$STORE = $model->Store_JobHistory($files['employee_id'],$inputan);
				if($STORE['status'] == 200){ $file->move(storage_path($upload['path']),$upload['filename']); }
				$message=$STORE['message']; $status=$STORE['status']; $data=$STORE['data'];

			}else{
				$inputan['path'] = $upload['path']; $inputan['filename'] = $upload['filename'];
				$PUT = $model->Update_JobHistory($id,$inputan);
				if($PUT['status'] == 200){ $file->move(storage_path($upload['path']),$upload['filename']); }
				$message=$PUT['message']; $status=$PUT['status']; $data=$PUT['data'];
			}
		}else{ $message='Update Failed.'; $status=500; $data = $update;}
		return [$message,$status,$data];
	}
// END UPLOAD
}
