<?php namespace Larasite\Http\Controllers\Employees\JobHistory;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Model\JobHistory\JobHistory_Model;
use Larasite\Privilege;

class JobHistory_Ctrl extends Controller {

protected $form_expat = 26;
protected $form_local=35;
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "26";
		}elseif($db_data == 2){
				return $this->formx = "35";
		}else{
			return $this->formx = "44";
		}
}


public function download($id,$filename){
		 $get_role = $this->check();
		if(isset($get_role['data']['role'])){

			 $check = \DB::select("SELECT filename from job_history where employee_id = '$id' and filename = '$filename'");
			 if(!$check){
			 	return \Response::json(['header'=>['message'=>'File not found']],404);
			 }

			foreach ($check as $key) { $files = $key->filename;}
			$path = storage_path()."\hrms_upload\job_history\\".$files;
			$file = \File::get($path);
			$type = \File::mimeType($path);
			return \Response::make($file,200,['Content-Type'=>$type]);
		}else{
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>401]],401);
		}
}

	// INDEX #####################################################################################
	public function index()
	{
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($crud[1] == 200){
			$get_role = $this->check();	$model = new JobHistory_Model; // SET MODEL

			if(isset($get_role['data']['role'])){ // CHECK KEY
					$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1); // GET ACCESS

					if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS

						$result['job'] 		= \DB::table('job')->get(['id','title','filename']);
						$result['status'] 	= \DB::table('jobs_emp_status')->get(['id','title']);
						$result['sub_unit'] 	= \DB::select("select id,descript,sub_unit,parent_id FROM sub_unit where id != 1") ;
							if($result){
								$data=$result; $status=200;
							}else{ $data=null; $status=404;}
					}else{ $message=$access['message'];	$status=$access['status']; $data=null; }
			}
		else{ $message = $get_role['message']; $status=401; $data=null;}
			return \Response::json($data,$status);
		}else{ $message = $crud[0]; $crud = $crud[1]; $data=$crud[2]; }
			return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud],'data'=>$data],$status);
	}// END INDEX #########################################################################################


// STORE #############################################################################
// FIX (METHOD POST) FIX
	public function store($id){
			$access = $FRA->Access(\Request::all(),$this->formx,'read');
			if($access[1] == 200){
		$model 				= new JobHistory_Model;
		$file 				= \Input::file('file');
		//$file['job']	 		= \Input::file('file_job');
		$input 				= $this->set_input();
		$get_role 			= $this->check();
		$id_job = \Input::get('data');
		if(isset($get_role['data']['role'])){
			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1); // GET ACCESS
				if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS

				if(isset($file) && $input['status'] != 200 ){


					$save_log = $this->SaveLog($id);

					if($save_log['status'] == 200){

						$upload =  $this->upload_seminar($file);

						$check = $model->check_file_id($id_job);

						if($check != null && $upload['filename']){
							$move = $this->move_file($check);

							if($move['status'] == 200){

									\DB::table('job_history')->where('id','=',$id_job)->update(['filename'=>$upload['filename'],'path'=>$upload['path']]);
									$file->move(storage_path($upload['path']),$upload['filename']);
									$message = 'Updated Successfully.';
									$status = 200;
									$data = $model->ambil_data_terbaru($id);
							}else{$message='Update Failed'; $status=200; $data=null;}
						} else{
							\DB::table('job_history')->where('id','=',$id_job)->update(['filename'=>$upload['filename'],'path'=>$upload['path']]);
								$file->move(storage_path($upload['path']),$upload['filename']);
								$message = 'Updated Successfully.';
								$status = 200;
								$data = $save_log['data_log'];
						}
					}// jika data ada
					else{
						$upload = $this->upload_seminar($file);
						if($upload['status'] == 200){
							$file->move(storage_path($upload['path']),$upload['filename']);
							\DB::table('job_history')->insert(['employee_id'=>$id,'filename'=>$upload['filename'],'path'=>$upload['path']]);
							//Response #######
							$message 	= $upload['message'];
							$data 		= ['id'=>$model->GetID(null,$upload['filename']),'filename'=>$upload['filename']];
							$status 	= 200;

						}else{ $message 	= 'Upload Failed';
								$data 		= $upload;
								$status 	= 500;}
							}// jika data tidak ada sebelumnya

				} // END UPLOAD
				elseif($input['status'] == 200 || empty($file)){ // INPUT STRING

					$save_log = $this->SaveLog($id);
					if($save_log['status'] == 200){

						$updated = $model->Update_JobHistory($id_job,$input['data']);

						if(isset($updated['status']) == 200){
							$message='Store Successfully.'; $status=200;
							$data = $data = $model->ambil_data_terbaru($id);;
						}else{ $message='Store Failed'; $status=200; $data=null;}

					}else{
						$check = $model->Store_JobHistory($id_job,$input['data']);
						if($check['status'] == 200){
							$message = $check['message']; $status = $check['status']; $data=$check['data'];
						}
						else{ $message = $check['message']; $status = $check['status']; $data=$check['data']; }
					}
				}
				else{
					$message = 'Upload failed.';$data = null; $status=406;
				}
			}else{ $message='Unauthorized'; $status=403;$data=null; }
		}else{ $message=$get_role['message']; $status=401;$data=null; }}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access],'data'=>$data],$status);
	}
// END STORE #############################################################################


/* SAVE LOG */
	public function SaveLog($id){
		/* get */
		//$select = \DB::select("SELECT id,employee_id,training_agreement,contract,probationary,fixed_term_contract,job,status,sub_unit,contract_start,contract_end,filename,`path` from manage_salary where employee_id = $id");
		$select = \DB::select("SELECT id,employee_id,training_agreement,contract_start_date,probationary_start,fixed_term_contract,job,status,sub_unit,emp_contract_start,emp_contract_end,filename,`path` from job_history where employee_id = $id");
		if($select){
			foreach ($select as $key) {
				$id_jobhistory_old = $key->id;
				$data['employee_id'] = $key->employee_id;
				$data['training_agreement'] = $key->training_agreement;
				$data['contract'] = $key->contract_start_date;
				$data['probationary'] = $key->probationary_start;
				$data['fixed_term_contract'] = $key->fixed_term_contract;
				$data['job'] = $key->job;
				$data['status'] = $key->status;
				$data['sub_unit'] = $key->sub_unit;
				$data['contract_start_date'] = $key->emp_contract_start;
				$data['contract_end_date'] = $key->emp_contract_end;
				$data['filename'] = $key->filename;
				$data['path'] = $key->path;
			}
			\DB::table('log_jobhistory')->insert($data);
			$status = 200;
			$id = $id_jobhistory_old;
		}else{ $status=500; $id=null; $data=null;}
		return ['status'=>$status,'id'=>$id,'data_log'=>$data];
	}
	public function SaveLog_String($id){
		/* get */

			$select = \DB::select("SELECT id,employee_id,training_agreement,contract_start_date,probationary_start,fixed_term_contract,job,status,sub_unit,emp_contract_start,emp_contract_end,filename,`path` from job_history where id = $id");
			//$select = \DB::select("SELECT employee_id from job_history where id = $id");
			if($select){
				foreach ($select as $key) {
					$id_jobhistory_old = $key->employee_id;
				}
				$id = $id_jobhistory_old;
				$status = 200;
			}else{ $status=500; $id=null;}

		return ['status'=>$status,'id'=>$id];
	}
/* END SAVE LOG */

// STORE LANJUTAN #############################################################################
// FIX (METHOD POST) FIX
// jalan jika sudah menhasilkan id
	public function store_string($id)
	{
		$access = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
		$store = new JobHistory_Model;
		$get_role = $this->check();
		if(isset($get_role['data']['role'])){
			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1); // GET ACCESS
				if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS

				$id_history = \Input::get('id');
				$file = \Input::file('file');
				$input = $this->set_input();

				if(isset($id) && $id != null){
					if($input['status'] == 200){


							$save_log = $this->SaveLog_String($id_history);
							if($save_log['status'] == 500){

								$result = $store->Store_JobHistory($id_history,$input['data']);
								if ($result) {
									$message = 'Store Successfully.'; $status = 200; $data =$save_log['id'];
								}else{ $message = 'Store Failed.'; $status = 500; $data =$save_log['id'];	 }
							}else{
								//return 2;
								$model = new JobHistory_Model;
								$data = $model->Update_JobHistory($id_history,$input['data']);
								if(isset($data['status']) == 200){
									$message = 'Store Successfully.'; $status = 200; $data =$id_history;
								}else{ $message = 'Store Failed.'; $status = 200; $data =$id_history;	 }
							}
					}
					// elseif( isset($id,$id_history,$file) && JobHistory_Model::find($id_history) && $input['status'] == 500){
					// 	$upload = $this->upload_seminar($file); // contract_details;
					// 	$file->move(storage_path($upload['path']),$upload['filename']);
					// 	\DB::table('job_history')->where('id','=',$id_history)->update(['filename'=>$upload['filename'],'path'=>$upload['path']]);

					// 	$message=$upload['message'];	$data=['filename'=>$upload['filename'],'id'=>$id_history];		$status=200;
					// }
					else{ $message = 'Store Not Successfully.'; $status = 406; $data = null; }
				}else{ $message = 'Upload failed...'; $status = 406; $data = null; }
			}else{ $message = 'Unauthorized'; $status = 403; $data=null; }
		}else{ $message = $get_role['message']; $status = 401; $data = null;  }
	}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}
//END STORE LANJUTAN ########################################################################

// UPDATE ##################################################################################
// PARAM ID INDEX FFIIIXXX......
	public function update_history($id) // PUT FIX***
	{
		/*SETUP MODEL*/$model = new JobHistory_Model;	$get_role = $this->check();
		/*SETUP INPUT*/$input = $this->set_input();	$file=\Input::file('file');
						//$id_history =\Input::get('id');

		if(isset($get_role['data']['role'])){
			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1); // GET ACCESS
				if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS

				if( isset($id,$file) && $input['status'] == 500 ){ // jika hanya update file

						$upload =  $this->upload_seminar($file);
						$check = $model->check_file_id($id);

						if($check != null && $upload['filename']){
							$move = $this->move_file($check);

							if($move['status'] == 200){

									\DB::table('job_history')->where('id','=',$id)->update(['filename'=>$upload['filename'],'path'=>$upload['path']]);
									$file->move(storage_path($upload['path']),$upload['filename']);
									$message = 'Updated Successfully.';
									$status = 200;

							}else{$message='Update Failed'; $status=202;	}
						} else{
							\DB::table('job_history')->where('id','=',$id)->update(['filename'=>$upload['filename'],'path'=>$upload['path']]);
								$file->move(storage_path($upload['path']),$upload['filename']);
								$message = 'Updated Successfully.';
								$status = 200;
						}
						return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
				}
				elseif( isset($id,$input) && $input['status']==200 && empty($file) ){ //  jika update only title or descript

						$data = $model->Update_JobHistory($id,$input['data']);
						if(isset($data['status']) == 200){
							$message=$data['message']; $status=$data['status'];
						}else{ $message=$data['message']; $status=$data['status'];	 }
				}

				else{ $message = 'Input Empty.'; $status = 500; $message_upload = null;}
				return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
			}

			else{ $message = 'Unauthorized'; $status = 401; }
			return \Response::json(['header'=>['message'=>'error','status'=>$status]],$status);
		}
		else{ return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403); }
	}
//END UPDATE ##############################################################################

// SHOW ###########################################################################################
// SHOW DATA EMERGENCY BERDASARKAN ID
// PARAM $id
// DI PERBAHARUI JIKA USER / BUKAN
	private function show_data_jobhistory($id){
		$model = new JobHistory_Model;
		$show = $model->Read_JobHistory($id);
		if(isset($id) && $id != 'undefined'){
			if(isset($show) && $show['status'] == 200){
				$data=$show['data']; $status=$show['status']; $message='Read Data.';
				$data['terminate'] = $model->ambil_data_terminate($id);
			}else{ $data=null; $status=200; $message='Data not found.'; }
		}else{ $data=null; $status=404; $message='Data not found.'; }

		return ['data'=>$data,'status'=>$status,'message'=>$message];
	}


	public function show($id){
		$access = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
		$get_role = $this->check();	//$model = new JobHistory_Model; // SET MODEL

		if(isset($get_role['data']['role'])){ // CHECK KEY
				$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1); // GET ACCESS
				if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS

					if($get_role['data']['role'] == 5 && $access['message']['employee_id'] == $get_role['data']['employee_id'] ){

						$datas = $this->show_data_jobhistory($access['message']['employee_id']);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}else{

						$datas = $this->show_data_jobhistory($id);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}

				}else{ $message=$access['message'];	$status=$access['status']; $data=null; }// access
		}else{ $message = $get_role['message']; $status=401; $data=null;}}// role

		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access],'data'=>$data],$status);
	}
// END SHOW ############################################################################################


// UPLOAD FILE ########################################################################################
// PARAM FILE
	public function upload_seminar($file){
		$AllowedExt = ['jpg','png','gif','pdf','jpeg','doc','doc','docx'];
		$CurrentExt = $file->getClientOriginalExtension();
		$date = date_create();
		$data_uniqid = uniqid();
		if(in_array($CurrentExt,$AllowedExt) ){ // filter type yang support upload

			if($file->getClientSize() <= 10000000){
				$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME); //SLICE CHAR
				$str = str_slug($slice_FileName,$separator = "_");

				$dir = '/hrms_upload/job_history';
				$dir_move = storage_path($dir);

				$data = "HistoryJob_".date_timestamp_get($date)."_".$data_uniqid.".".$CurrentExt;
				$msg = 'Upload Successfully...';	$status=200;
				return ['filename'=>$data,'path'=>$dir,'type'=>$CurrentExt,'size'=>$this->formatSizeUnits($file->getClientSize()),'message'=>$msg,'status'=>$status];
			}
			else{
				$msg = 'Max Size <= 10MB ';
				$data = null;
				$status=200;
				return ['filename'=>$data,'message'=>$msg];
			}
		}
		else{
			$msg = "Type not support $CurrentExt file";
			$data = null;
			$status = 500;
			return ['status'=>$status,'type'=>$AllowedExt,'msg'=>$msg,'size'=>$this->formatSizeUnits($file->getClientSize()),'message'=>$msg];
		}
	}
//END UPLOAD FILE ####################################################################################


// DESTROY #############################################################################################
	public function destroy($id)
	{
		$access = $FRA->Access(\Request::all(),$this->formx,'destroy');
		if($access[1] == 200){
		$model = new JobHistory_Model;
		$get_role = $this->check();
		if(isset($get_role['data']['role'])){
			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],3);
			if(isset($access['status']) && $access['status'] == 200){
						$toArray = explode(",", $id);

						$data = $model->Destroy_JobHistory($toArray);
						if(isset($data) && $data['status'] == 200){
						$message = $data['message'];
						$status = $data['status'];
						}else{ $message = 'ID not found'; $status = 404; }
					//}
					//else{ $message='ID Not Found'; $status=404; $data=null; }
			}else{ $message = $access['message']; $status = $access['status'];}
		}else{ $message=$get_role['message']; $status=403;}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access]"data" => []],$status);
	}// END DESTROY #######################################################################################

// MOVE FILE IN TRASH ########################################################################
	public function move_file($filename){
		$get_data = \DB::select("SELECT `filename`, `path` from job_history where filename = '$filename' ");

		foreach ($get_data as $key) {
			$data['path'] = $key->path;
			$data['filename'] = $key->filename;
		}
		if(\Storage::disk('local')->exists($data['path']."/".$data['filename'])){
			\Storage::disk('local')->move($data['path']."/".$data['filename'], "/Trash/".$data['filename']);
			if( \Storage::disk('local')->exists("/Trash/".$data['filename']) ){
				$message = 'Update Successfully.';
				$status = 200;
			}else{
				//\Storage::disk('local')->move("/Trash/".$data['filename'],$data['path']."/".$data['filename']);
				$message = 'gagal move file to trash';
				$status = 500;
			}
		}
		else{
			//$message = 'File tidak ditemukan, atau file telah dihapus check folder trash';
			$message = 'Update Successfully.';
			$status = 200;
		}
		return ['message'=>$message,'status'=>$status];
	}
// END MOVE FILE #################################################################################


// CONVERT SIZE ##########################################################################
// PARAM byte
	public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824){	$bytes = number_format($bytes / 1073741824, 2) . ' GB';	}
        elseif ($bytes >= 1048576){	$bytes = number_format($bytes / 1048576, 2) . ' MB';	}
        elseif ($bytes >= 1024){	$bytes = number_format($bytes / 1024, 2) . ' KB';}
        elseif ($bytes > 1){	$bytes = $bytes . ' bytes';}
        elseif ($bytes == 1){	$bytes = $bytes . ' byte';	}
        else{	$bytes = '0 bytes'; }
        return $bytes;
	}
// END CONVERT ##########################################################################



// /* SAVE LOG */
// 	public function SaveLog($id){
// 		/* get */
// 		$select = \DB::select("SELECT * from job_history where employee_id = $id");
// 		if($select){
// 			foreach ($select as $key) {
// 				$employee_id = $key->employee_id;
// 				$salary = $key->salary;
// 				$atm_numb = $key->atm_numb;
// 				$atm_account = $key->atm_account;
// 				$net_gross = $key->net_gross;
// 				$hmo = $key->hmo;
// 				$hmdf_loan = $key->hdmf_loan;
// 				$sss_loan = $key->sss_loan;
// 				$id_salary = $key->id;
// 			}
// 			$store = \DB::table('log_salary')->insert(['employee_id'=>$employee_id,'salary'=>$salary,'atm_numb'=>$atm_numb,'atm_account'=>$atm_account,'net_gross'=>$net_gross,'hmo'=>$hmo,'hdmf_loan'=>$hmdf_loan,'sss_loan'=>$sss_loan]);
// 			if($store){
// 				$status = 200;
// 			}else{ $status = 200; }
// 		}else{ $status=500; $id_salary=null;}
// 		return ['status'=>$status,'id'=>$id_salary];
// 	}
// /* END SAVE LOG */


	/* CHECK INPUT ################################################################################################################
	* Param $id = USE FIND ID FOR UPDATE.
	* PARAM $TYPE = 1(STORE), 2(UPDATE).
	*/
	private function set_input(){
		$data = array(); // tampung input to array
		$data['date_training'] 		= \Input::get('training_agreement');
		$data['date_probationary']	= \Input::get('probationary_start');
		$data['date_contract']		= \Input::get('contract_start_date');
		$data['date_fixed_term'] 	= \Input::get('fixed_term_contract');
		$data['contract_start'] 	= \Input::get('emp_contract_start');
		$data['contract_end'] 		= \Input::get('emp_contract_end');
		$data['job']				= \Input::get('job');
		$data['status']				= \Input::get('status');
		$data['sub_unit']			= \Input::get('sub_unit');

		if($data['date_training'] && $data['date_probationary'] && $data['date_training'] && $data['date_contract']){
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }
		return ['status'=>$status,'data'=>$result];
	}
	// CHECK INPUT VALUE #################################################################################



	/* CHECK LOCAL IT ###################################################################################
	* PARAM = ROLE
	*/
	private function check_local($role,$employee_id){
		if(isset($role)){
			$model = new JobHistory_Model;
			$get = $model->Get_Type($role,$employee_id);
			foreach ($get as $key) {
				$type['local_it'] = $key->local_it;
				$type['employee_id'] = $key->employee_id;
			}
		}else{ $type = null; }
		return $type;
	} // END CHECK LOCAL IT ############################################################################



	/* CHECK TYPE AND ACCESS ########################################################################
	* CHECK TYPE ID EMPLOYEE EXPAT OR LOCAl OR LOCAL_IT
	* ACTION = 1(CREATE & READ), 2(EDIT & UPDATE), 3(DESTROY)
	*/
private function check_type2($employee_id,$role,$action) // CHECK ROLE FIX***
	{
		$model = new JobHistory_Model;
		$status_error = 403;
		$not_access = 'Unauthorized';
		$type['data'] = $this->check_local($role,$employee_id);
		$type['local_it'] = $type['data']['local_it'];
		if(isset($type) && $type['local_it']){

			/* ########
			* FOR EXPAT
			*/
			if($type['local_it'] == 1){
				$check = $this->check_role($role,$this->form_expat);

				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error;}	// ERROR ACCESS
			} // if type local, expat, locit

			/* ########
			* FOR LOCAL
			*/
			elseif($type['local_it'] == 2){

				$check = $this->check_role($role,$this->form_local);

				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
			}

			/* ###########
			* FOR LOCAL_IT
			*/
			elseif($type['local_it'] == 3){
				$check = $this->check_role($role,$this->form_local);

				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
			}else{
				$message = $not_access; $status = $status_error;
			}
		}
		else{
			$message = $not_access; $status = $status_error; // ROLE NOT FOUND
		}
		return ['message'=>$message,'status'=>$status];
	} // CHECK TYPE AND ACCESS ####################################################################


	/* CHECK ROLE ###################################################################################
	* PARAM = ROLE AND FORM
	* CHECK USER ROLE YANG ACCESS SYSTEM
	* MENGHASILKAN DATA ACCESS DAN ID
	*/
	private function check_role($role,$form) // CHECK ROLE FIX***
	{
		$getModel = new Privilege;
		//$user 		= base64_decode(\Cookie::get('id'));
		if(isset($role, $form)){
			return $getModel->check_role_personal($role,$form);
		}
		else{
			$msg['warning'] = 'Unauthorize';
			$msg['status'] = 404;
			return $msg;
		}
	}// END CHECK ROLE #####################################################################

// CHECK ACCESS ###############################################################################
	public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(isset($q) != null){ //check isset key in session

						 $r = array();
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } // end for store role to array.

						 if(isset($r['key'])){
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							//return $employee_id;
							 $q2 = $getModel->session_role($employee_id);
							 foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 } // end for store role to array.
						 }
						 else{
						 	return ['message'=>'ACCESS DENIED','data'=>null];
						 }


				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
						$msg = 'ACCESS GRANTED !';
					}else{
						$msg = 'ACCESS DENIED!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED!';
					$data = null;
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		return ['message'=>$msg,'data'=>$data];
	}
// END CHECK ACCESS ############################################################################
}
