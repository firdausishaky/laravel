<?php namespace Larasite\Http\Controllers\Employees;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;

use Larasite\Model\Personal_Model;
use Larasite\ORM\PersonalAttach_ORM;

use Larasite\Library\FuncAccess;
class PersonalAttachment_Ctrl extends Controller {

// protected $form_expat = 19;
// protected $form_local = 28;
// protected $form_localit = 37;

protected $form = ['expat'=>19,'local'=>28,'local_it'=>37];

public function date(){
	return date('Y-m-d');
}

private function checkID($id)
{
		$rule = ['undefined',NULL,''];
		if(in_array($id,$rule)){ $data = ['ID Undefined.',500,NULL]; }
		else{ $data = ['OK',200,NULL]; }
		return $data;
}

// GET FILE #########################################################################\
	private function get_file($id,$filename){

		if(isset($id,$filename)){
			$check = \DB::select("SELECT filename from emp_picture where id = '$id' and filename = '$filename' ");
			if(isset($check)){
					$path = storage_path().'/hrms_upload/personal_picture/'.$filename;
					$file = \File::get($path);
					$type = \File::mimeType($path);
					return \Response::make($file,200,['Content-Type'=>$type]);
			}else{

			}
		}
	}
// END GET FILE ####################################################################

public function download($id,$filename){
		 $get_role = $this->check();
		if(isset($get_role['data']['role'])){

			 $check = \DB::select("SELECT filename from emp_attachment where employee_id = '$id' and filename = '$filename'");
			 if(!$check){
			 	return \Response::json(['header'=>['message'=>'File not found']],404);
			 }

			foreach ($check as $key) { $files = $key->filename;}
			$path = storage_path()."/hrms_upload/personal_attacthment/".$files;
			$file = \File::get($path);
			$type = \File::mimeType($path);
			return \Response::make($file,200,['Content-Type'=>$type]);
		}else{
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>401]],401);
		}
}

// SHOW ###########################################################################################
// PARAM $id
	public function show($id){
		// $get_role = $this->check();	$model = new Personal_Model; // SET MODEL

		// if(isset($get_role['data']['role'])){ // CHECK KEY
		// 		$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1); // GET ACCESS

		// 		if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS

		/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; $access = $FuncAccess->AccessPersonal(['Personal'=>$id,'Request'=>$REQ],$this->form,1);
		/*Model*/ $model = new Personal_Model;
		if($access[1] == 200){

			$checkID = $this->checkID($id);
			if($checkID[1] == 500){ $message=$checkID[0]; $status=$checkID[1]; $data=$checkID[2]; }
			else{

					$show = $model->Read_Personal_Attach($id,$access[0]);


					if(isset($show) && $show['status'] == 200){

						$data=$show['data']; $status=$show['status']; $message=$show['message'];
					}else{ $data=null; $status=500; $message=$show['message']; }
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		// 		}else{ $message=$access['message'];	$status=$access['status']; $data=null; }
		// }
		// else{ $message = $get_role['message']; $status=401; $data=null;}
		// return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// END SHOW ##########################################################################
// STORE #############################################################################
// FIX (METHOD POST) FIX
	public function store1($id){
		//$model 		= new Personal_Model;
		$file 		= \Input::file('file');
		$input 		= $this->set_input();
		$get_role 	= $this->check();
		$added_by	= $this->Find_AddedBy();
		$data = json_decode(\Input::get('data'),1);

		// if(isset($get_role['data']['role'])){
		// 	$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1); // GET ACCESS
		// 		if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS

		/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; $access = $FuncAccess->AccessPersonal(['Personal'=>$id,'Request'=>$REQ],$this->form,2);
		/*Model*/ $model = new Personal_Model;
		if($access[1] = 200){
			$checkID = $this->checkID($id);
			if($checkID[1] == 500){ $message=$checkID[0]; $status=$checkID[1]; $data=$checkID[2]; }
			else{

				if(isset($file)){
					//return 1;
					$upload = $this->upload_attach($file);
					if($upload['status'] == 200){
						(!isset($data['descript']) ? $data['descript'] = null : $data['descript'] = $data['descript']);
						($data['descript'] == null ? $data['descript'] = null : $data['descript'] = $data['descript']);
						$file->move(storage_path($upload['path']),$upload['filename']);

						\DB::table('emp_attachment')->insert(['employee_id'=>$id,'filename'=>$upload['filename'],'descript' =>$data['descript'] ,'path'=>$upload['path'],'size'=>$upload['size'],'type'=>$upload['type'],'added_by'=>$added_by,'created_at'=>$this->date()]);
						// Response #######
						$message 	= $upload['message'];
						//$data 		= ['id'=>$model->GetID($upload['filename']),'filename'=>$upload['filename'],'size'=>$upload['size'],'type'=>$upload['type'],'added_by'=>$added_by,'created_at'=>$this->date()];
						$data_x = \DB::SELECT("select * from emp_attachment where employee_id=$id order by id desc limit 1");
						$data = $data_x[0];
						$status 	= 200;
					}else{ $message = 'file not supported or size not acceptable less than 1MB.';$data = null; $status=406;}
				}
				// elseif(isset($input['status']) == 200 || empty($file)){
				// 	$check = \DB::table('emp_attachment')->insert(['employee_id'=>$id,'descript'=>$input['data']['descript'],'added_by'=>$added_by,'created_at'=>$this->date()]);
				// 	if($check){
				// 		$message='Store Successfully.'; $status=200;
				// 	}else{  $message ='Store Failed';$status= 500;
				// 	}
				// 	$data_x = \DB::SELECT("select * from emp_attachment where employee_id=$id order by id desc limit 1");
				// 	if($status == 200){ $data = $data_x[0]/*['id'=>4data_x,'added_by'=>$added_by,'created_at'=>$this->date()] */;}
				// 	else{ $data=null; }
				// }
				else{
					$message = 'please upload file before submit.';$data = null; $status=406;
				}

			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		// 	}else{ $message='Unauthorized'; $status=403;$data=null; }
		// }else{ $message=$get_role['message']; $status=401;$data=null; }
		// return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// END STORE #############################################################################


// STORE LANJUTAN #############################################################################
// FIX (METHOD POST) FIX
// jalan jika sudah menhasilkan id
	public function store2($id)
	{
		$store = new Personal_Model;
		$get_role = $this->check();
		$added_by = $this->Find_AddedBy();

		// if(isset($get_role['data']['role'])){
		// 	$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1); // GET ACCESS
		// 		if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS

		/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; $access = $FuncAccess->AccessPersonal(['Personal'=>$id,'Request'=>$REQ],$this->form,2);
		if($access[1] = 200){
			$checkID = $this->checkID($id);
			if($checkID[1] == 500){ $message=$checkID[0]; $status=$checkID[1]; $data=$checkID[2]; }
			else{

				$id_attach = \Input::get('id');
				$file = \Input::file('file');
				$input = $this->set_input();

				if(isset($id)){
					if(isset($id,$id_attach)){

						\DB::table('emp_attachment')->where('id','=',$id_attach)->update(['descript'=>$input['data']['descript'],'added_by'=>$added_by,'created_at'=>$this->date()]);
						$message = 'Store Successfully.';
						$status = 200;
						$data = ['id'=>$id_attach,'added_by'=>$added_by,'created_at'=>$this->date()];
					}
					elseif( isset($id,$id_attach,$file) && $input['status'] == 500){

						$upload = $this->upload_attach($file);
						$file->move(storage_path($upload['path']),$upload['filename']);
						\DB::table('emp_attachment')->where('id','=',$id_attach)->update(['filename'=>$upload['filename'],'path'=>$upload['path'],'size'=>$upload['size'],'type'=>$upload['type'],'added_by'=>$added_by,'created_at'=>$this->date()]);

						$message = $upload['message'];
						$data = ['id'=>$id_attach,'filename'=>$upload['filename'],'size'=>$upload['size'],'type'=>$upload['type'],'added_by'=>$added_by,'created_at'=>$this->date()];
						$status = 200;
					}
					else{ $message = 'Store Not Successfully.'; $status = 406; $data = null; }
				}else{ $message = 'Upload failed...'; $status = 406; $data = null; }
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		// 	}else{ $message = 'Unauthorized'; $status = 403; $data=null; }
		// }else{ $message = $get_role['message']; $status = 401; $data = null;  }

		// return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
//END STORE LANJUTAN ########################################################################

// UPDATE ##################################################################################
// PARAM ID INDEX FFIIIXXX......
	public function update_attach($id) // PUT FIX***
	{
		///*SETUP MODEL*/$model = new Personal_Model;	$get_role = $this->check();
		/*SETUP INPUT*/$input = $this->set_input();	$file=\Input::file('file');
		$added_by = $this->Find_AddedBy(); $date = $this->date();

		// if(isset($get_role['data']['role'])){
		// 	$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1); // GET ACCESS
		// 		if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS

		/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; $access = $FuncAccess->AccessPersonal(['Personal'=>$id,'Request'=>$REQ],$this->form,3);
		$model = new Personal_Model;
		if($access[1] = 200){
			$checkID = $this->checkID($id);
			if($checkID[1] == 500){ $message=$checkID[0]; $status=$checkID[1]; $data=$checkID[2]; }
			else{

				if( isset($id,$file)){ // jika hanya update file

						$upload =  $this->upload_attach($file);
						$check = $model->check_file_id($id);
						$data_descript = json_decode(\Input::get('data'),1);
						$data_dex = $data_descript['descript'];
						($data_dex != null ? $data_e = $data_dex : $data_e = null);
						if(isset($check) != null && isset($upload['filename'])){
							$move = $this->move_file($check);

							if($move['status'] == 200){

									\DB::table('emp_attachment')->where('id','=',$id)->update(['filename'=>$upload['filename'],'descript' =>$data_e ,'path'=>$upload['path'],'size'=>$upload['size'],'type'=>$upload['type'],'added_by'=>$added_by,'created_at'=>$date]);
									$file->move(storage_path($upload['path']),$upload['filename']);
									$message = 'Updated Successfully.';
									$status = 200;
									$data_x = \DB::SELECT("select * from emp_attachment where id=$id");
									$data = $data_x[0];//['filename'=>$upload['filename'],'path'=>$upload['path'],'size'=>$upload['size'],'type'=>$upload['type'],'created_at'=>$date,'added_by'=>$added_by];
							}else{$message='Update Faileds'; $status=202; $data=null;}
						} else{$message=$upload['message']; $status=500; $data=null;}
						return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
				}
				elseif( isset($id) && empty($file)  && !empty(\Input::get('data') )){ //  jika update only title or descript


						if(!$id || $id == null){
							return \Response::json(['header'=>['message'=>'ID Undefined.','status'=>500]],500);
						}
						$data_descript = json_decode(\Input::get('data'),1);
						$data_des = $data_descript['descript'];
						$data = PersonalAttach_ORM::find($id);
						$data->descript = $data_des;
						$data->save();
						$message='Update Successfully.'; $status=200; $data = \DB::table('emp_attachment')->where('id','=',$id)->get()[0];

				}

				else{ $message = 'Input Empty.'; $status = 500; $message_upload = null; $data=null;}
				return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		// 	}

		// 	else{ $message = 'Unauthorized'; $status = 401; }
		// 	return \Response::json(['header'=>['message'=>'error','status'=>$status]],$status);
		// }
		// else{ return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403); }
	}
//END UPDATE ##############################################################################


// UPLOAD FILE ########################################################################################
// PARAM FILE
	public function upload_attach($file){
		$AllowedExt = ['jpg','png','gif','pdf','jpeg','JPG','JPEG','PDF','xls','xlsx','bitmap','raw','doc','docx','ppt','pptx'];
		$CurrentExt = $file->getClientOriginalExtension();
		$date = date_create();
		$data_uniqid = uniqid();
		if(in_array($CurrentExt,$AllowedExt) ){ // filter type yang support upload

			if($file->getClientSize() <= 250000000){
				$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME); //SLICE CHAR
				$str = str_slug($slice_FileName,$separator = "_");

				$dir = 'hrms_upload/personal_attacthment';
				$dir_move = storage_path($dir);

				$data = "PersonalAttachment_".$data_uniqid.".".$CurrentExt;
				$msg = 'Upload Successfully.';	$status=200;
				return ['filename'=>$data,'path'=>$dir,'type'=>$CurrentExt,'size'=>$this->formatSizeUnits($file->getClientSize()),'message'=>$msg,'status'=>$status];
			}
			else{
				$msg = 'Max Size <= 10MB ';
				$data = null;
				$status=200;
				return ['filename'=>$data,'message'=>$msg];
			}
		}
		else{
			$msg = "Type not support $CurrentExt file";
			$data = null;
			$status = 500;
			return ['status'=>$status,'type'=>$AllowedExt,'msg'=>$msg,'size'=>$this->formatSizeUnits($file->getClientSize()),'message'=>$msg];
		}
	}
//END UPLOAD FILE ####################################################################################


// DESTROY #############################################################################################
	public function destroy($id)
	{

		 $model = new Personal_Model;
		// $get_role = $this->check();
		// if(isset($get_role['data']['role'])){
		// 	$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],3);
		// 	if(isset($access['status']) && $access['status'] == 200){

		/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; $access = $FuncAccess->AccessPersonal(['Personal'=>$id,'Request'=>$REQ],$this->form,4);
		if($access[1] = 200){
			$checkID = $this->checkID($id);
			if($checkID[1] == 500){ $message=$checkID[0]; $status=$checkID[1]; $data=$checkID[2]; }
			else{
						$toArray = explode(",", $id);

						$data = $model->Destroy_Personal_Attach($toArray);
						if(isset($data) && $data['status'] == 200){
						$message = $data['message'];
						$status = $data['status'];
						$data = null;
						}else{ $message = 'ID not found'; $status = 404; $data = null; }
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
					//}
					//else{ $message='ID Not Found'; $status=404; $data=null; }
		// 	}else{ $message = $access['message']; $status = $access['status'];}
		// }else{ $message=$get_role['message']; $status=403;}
		// return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
	}// END DESTROY #######################################################################################


	/* SET LOCAL */
	private function Find_AddedBy(){
		$r = \Request::all(); $model=new Personal_Model;
		if(isset($r['key'])){
			$decode = base64_decode($r['key']);
			$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
			$get = $model->Find_AddedBy($id);
		}else{ $get = null; }
		return $get;
	}
/* --- END METHOD LOCAL_IT ---- */




// MOVE FILE IN TRASH ########################################################################
	public function move_file($filename){
		$get_data = \DB::select("SELECT `filename`, `path` from emp_attachment where filename = '$filename' ");

		foreach ($get_data as $key) {
			$data['path'] = $key->path;
			$data['filename'] = $key->filename;
		}
		if(\Storage::disk('local')->exists($data['path']."/".$data['filename'])){
			\Storage::disk('local')->move($data['path']."/".$data['filename'], "/Trash/".$data['filename']);
			if( \Storage::disk('local')->exists("/Trash/".$data['filename']) ){
				$message = 'Update Successfully.';
				$status = 200;
			}else{
				\Storage::disk('local')->move("/Trash/".$data['filename'],$data['path']."/".$data['filename']);
				$message = 'gagal move file to trash';
				$status = 500;
			}
		}
		else{
			$message = 'File tidak ditemukan, atau file telah dihapus check folder trash';
			$status = 404;
		}
		return ['message'=>$message,'status'=>$status];
	}
// END MOVE FILE #################################################################################


// CONVERT SIZE ##########################################################################
// PARAM byte
	public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824){	$bytes = number_format($bytes / 1073741824, 2) . ' GB';	}
        elseif ($bytes >= 1048576){	$bytes = number_format($bytes / 1048576, 2) . ' MB';	}
        elseif ($bytes >= 1024){	$bytes = number_format($bytes / 1024, 2) . ' KB';}
        elseif ($bytes > 1){	$bytes = $bytes . ' bytes';}
        elseif ($bytes == 1){	$bytes = $bytes . ' byte';	}
        else{	$bytes = '0 bytes'; }
        return $bytes;
	}
// END CONVERT ##########################################################################

	/* CHECK INPUT ################################################################################################################
	* Param $id = USE FIND ID FOR UPDATE.
	* PARAM $TYPE = 1(STORE), 2(UPDATE).
	*/
	private function set_input(){
		$data = array(); // tampung input to array
		$data['descript']	= \Input::get('descript');
		if(!$data['descript']){ $data['descript'] = NULL; }
		$data['file']		= \Input::get('file');
		if($data['descript'] != null || $data['file'] !=null){
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }
		return ['status'=>$status,'data'=>$result];
	}
	// CHECK INPUT VALUE #################################################################################



	/* CHECK LOCAL IT ###################################################################################
	* PARAM = ROLE
	*/
	private function check_local($role,$employee_id){
		if(isset($role)){
			$model = new Personal_Model;
			$get = $model->Get_Type($role,$employee_id);
			foreach ($get as $key) {
				$type['local_it'] = $key->local_it;
				$type['employee_id'] = $key->employee_id;
			}
		}else{ $type = null; }
		return $type;
	} // END CHECK LOCAL IT ############################################################################



	/* CHECK TYPE AND ACCESS ########################################################################
	* CHECK TYPE ID EMPLOYEE EXPAT OR LOCAl OR LOCAL_IT
	* ACTION = 1(CREATE & READ), 2(EDIT & UPDATE), 3(DESTROY)
	*/
	private function check_type2($employee_id,$role,$action) // CHECK ROLE FIX***
	{
		$model = new Personal_Model;
		$status_error = 403;
		$not_access = 'Unauthorized';
		$type['data'] = $this->check_local($role,$employee_id);
		$type['local_it'] = $type['data']['local_it'];
		if(isset($type) && $type['local_it']){

			/* ########
			* FOR EXPAT
			*/
			if($type['local_it'] == 1){
				$check = $this->check_role($role,$this->form_expat);

				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error;}	// ERROR ACCESS
			} // if type local, expat, locit

			/* ########
			* FOR LOCAL
			*/
			elseif($type['local_it'] == 2){

				$check = $this->check_role($role,$this->form_local);

				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
			}

			/* ###########
			* FOR LOCAL_IT
			*/
			elseif($type['local_it'] == 3){
				$check = $this->check_role($role,$this->form_localit);

				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
			}else{
				$message = $not_access; $status = $status_error;
			}
		}
		else{
			$message = $not_access; $status = $status_error; // ROLE NOT FOUND
		}
		return ['message'=>$message,'status'=>$status];
	} // CHECK TYPE AND ACCESS ####################################################################


	/* CHECK ROLE ###################################################################################
	* PARAM = ROLE AND FORM
	* CHECK USER ROLE YANG ACCESS SYSTEM
	* MENGHASILKAN DATA ACCESS DAN ID
	*/
	private function check_role($role,$form) // CHECK ROLE FIX***
	{
		$getModel = new Privilege;
		//$user 		= base64_decode(\Cookie::get('id'));
		if(isset($role, $form)){
			return $getModel->check_role_personal($role,$form);
		}
		else{
			$msg['warning'] = 'Unauthorize';
			$msg['status'] = 404;
			return $msg;
		}
	}// END CHECK ROLE #####################################################################

// CHECK ACCESS ###############################################################################
	public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(isset($q) != null){ //check isset key in session

						 $r = array();
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } // end for store role to array.

						 if(isset($r['key'])){
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							//return $employee_id;
							 $q2 = $getModel->session_role($employee_id);
							 foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 } // end for store role to array.
						 }
						 else{
						 	return ['message'=>'ACCESS DENIED','data'=>null];
						 }


				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
						$msg = 'ACCESS GRANTED !';
					}else{
						$msg = 'ACCESS DENIED!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED!';
					$data = null;
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		return ['message'=>$msg,'data'=>$data];
	}
// END CHECK ACCESS ############################################################################
}
