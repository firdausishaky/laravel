<?php namespace Larasite\Http\Controllers\Employees;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\PersonalPict_Model;
use Larasite\Library\FuncAccess;

class PersonalPict_Ctrl extends Controller {

protected $form_expat = 19;
protected $form_local = 28;
protected $form_localit = 37;

protected $form = ['expat'=>19,'local'=>28,'local_it'=>37];

protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "19";
		}elseif($db_data == 2){
				return $this->formx = "28";
		}else{
			return $this->formx = "37";
		}
}

private function pathFile($filename)
{
	return storage_path().'/hrms_upload/personal_picture/'.$filename;
}


// GET FILE #########################################################################\
	public function get_file($id,$filename){
		// try {
		// 	$getfile = \DB::table('emp_picture')->where('id','=',$id)->get(['filename']);
		// } catch (Exception $e) {
		// 	return null;
		// }
		// foreach ($getfile as $key) {
		// 	$file['filename'] = $key->filename;
		// }
		// $getmime = storage_path().'\hrms_upload\personal_picture\\'.$file['filename'];

		// $header = array('Content-Description : File Transfer','Content-Type : image/png','Content-Type : image/jpg','Content-Type : image/gif','Content-Type : image/jpeg','Content-Disposition : attachment;filename='.basename($getmime),'Content-Transfer-Encoding : binary','Expires : 0','Cache-Control : must-revalidate','Pragma : public');
		// //return \Response::json(['header'=>['message'=>'Get Picture','status'=>200],'data'=>$getmime],$header);
		// return \Response::make($getmime,304,['Content-Type' => 'image/jpeg']);
		if(isset($id,$filename)){
			$check = \DB::select("SELECT filename from emp_picture where id = '$id' and filename = '$filename' ");
			if(isset($check)){
					$path = $this->pathFile($filename);
					if(file_exists($path)){
						//KUDOS
					}else{
						$get_sex   = \DB::SELECT("select * from emp where employee_id = '$id' ")[0]->gender;

						if($get_sex == "male"){
							$path  =  $this->pathFile("man.png");	
						}else{
							$path  =  $this->pathFile("girl.png");
						}
 
					}
					$file = \File::get($path);
					$type = \File::mimeType($path);
					//return \Response::make($file,200,['Content-Type'=>$type]);
					//return ['file'=>$file,'mime'=>$type];
					$header = array('Content-Description : File Transfer',
							'Content-Type : image/png',
							'Content-Type : image/jpg','Content-Type : image/gif',
							'Content-Type : image/jpeg',
							'Content-Disposition : attachment;filename='.basename($type),
							'Content-Transfer-Encoding : binary','Expires : 0','Cache-Control : must-revalidate','Pragma : public');
					return \Response::make($file,200,$header);
					//return \Response::download($getmime,$file['filename'],$header);
			}else{
				
				$get_sex   = \DB::SELECT("select * from emp where employee_id = '$id' ")[0]->gender;

				if($get_sex == "male"){
					$path  =  $this->pathFile("man.png");	
				}else{
					$path  =  $this->pathFile("girl.png");
				}


				$file = \File::get($path);
				$type = \File::mimeType($path);
				//return \Response::make($file,200,['Content-Type'=>$type]);
				//return ['file'=>$file,'mime'=>$type];
				$header = array('Content-Description : File Transfer',
						'Content-Type : image/png',
						'Content-Type : image/jpg','Content-Type : image/gif',
						'Content-Type : image/jpeg',
						'Content-Disposition : attachment;filename='.basename($type),
						'Content-Transfer-Encoding : binary','Expires : 0','Cache-Control : must-revalidate','Pragma : public');
				return \Response::make($file,200,$header);
			}
		}
					//$response = \Response::make($file,304);
					//$response->header("Content-Type",$type);
	}
// END GET FILE ####################################################################

public function download($id,$filename){
		 $get_role = $this->check();
		if(isset($get_role['data']['role'])){
			
			 $check = \DB::select("SELECT filename from emp_picture where employee_id = '$id' and filename = '$filename'");
			 if(!$check){
			 	return \Response::json(['header'=>['message'=>'File not found']],404);
			 }

			foreach ($check as $key) { $files = $key->filename;}
			$path = $this->pathFile($files);
			$file = \File::get($path);
			$type = \File::mimeType($path);
			return \Response::make($file,200,['Content-Type'=>$type]);
		}else{
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>401]],401);
		}
}

// INDEX ########################################################################################
	public function index($id)
	{
		// NOT USES
		$get_role = $this->check();		$model = new PersonalPict_Model;
		if(isset($get_role['data']['role']) != null){
			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1);
			if(isset($access) != null && $access['status'] == 200){

				$show = $model->Read_Pict($id,null);
				if(isset($show) != null && $show['status'] == 200){
					$file = $show['data'];					
					$getmime = $this->pathFile($file['filename']);

					return \Response::json(['image'=> $this->get_file($id,$file['filename'])] );
					$message=$show['message']; $status=$show['status']; $data=$show['data'];
				
				}else{ $message=$show['message']; $status=200; $data=null; }
			}else{ $message='Unauthorized.'; $status=401; $data=null; $data=null;}
		}else{ $message=$get_role['message']; $status=403; $data=null; $data=null;}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
//END INDEX #######################################################################################

// SHOW ###############################################
	public function show($id){

		$get_role = $this->check();		$model = new PersonalPict_Model;
		
		if($get_role['data']['role'] != null){
			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1);
			if(isset($access) != null && $access['status'] == 200){

				$show = $model->Read_Pict(null,$id);
				if(isset($show) != null && $show['status'] == 200){
					$message=$show['message']; $status=$show['status']; $data=$show['data'];

				}else{ $message=$show['message']; $status=200; $data=null; }
			}else{ $message='Unauthorized.'; $status=401; $data=null; $data=null;}
		}else{ $message=$get_role['message']; $status=403; $data=null; $data=null;}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// END SHOW ###############################################

// STORE ###############################################################################################
// NOT USES
public function store(){
	return \Response::json(['header'=>['message'=>'Method not allowed','status'=>404],'data'=>null],404);
}
// END STORE #############################################################################################

// STORE #############################################################################
// FIX (METHOD POST)
	public function store_pict($id){
		$file = \Input::file('file');
		$model = new PersonalPict_Model;

		$get_role = $this->check();
		$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1);
		if(isset($get_role['data']['role'])){
			if(isset($access['status']) == 200){

				if(isset($file) != null){
					$check_emp = $model->Check_emp($id);
					if(isset($check_emp)){
						$upload = $this->upload_pict($file,$id);
						$file->move(storage_path($upload['path']),$upload['filename']);
						$upload['employee_id']=$id;
						$model->Store_Pict($upload);
						//\DB::table('job')->insert(['filename'=>$upload['filename'],'path'=>$upload['path']]);
						// Response
						$getid = $model->Get_ID($upload['filename']);
						$message = $upload['message'];
						$data = ['id'=>$getid['data'],'filename'=>$upload['filename']];
						$status = 200;
					}else{
						$getid = $model->Get_ID($upload['filename']);
						$message = $upload['message'];
						$data = ['id'=>$getid['data'],'filename'=>$upload['filename']];
						$status = 404;
					}
				}
				else{ $message = 'Upload failed.';$data = null; $status=406;}
			}else{ $message='Unauthorized'; $status=401;$data=null; }
		}else{ $message=$get_role['message']; $status=403;$data=null; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// END STORE #############################################################################


// DESTROY ###
/*public function destroy($id){

		$get_role = $this->check();
		$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1);
		if(isset($get_role['data']['role'])){
			
			if(isset($access['status']) == 200){

				$get = \DB::select("select filename from emp_picture where filename = '$id'");
				if($get){
					foreach ($get as $key) {
						$filename = $key->filename;
					}
					\DB::table('emp_picture')->where('filename','=',$filename)->delete();
					$message='Destroy Successfully.'; $status=200;
				}
				else{ $message='ID not found.'; $status=500; }
			}else{ $message='Unauthorized'; $status=401;$data=null; }
		}else{ $message=$get_role['message']; $status=403;$data=null; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
}*/

public function destroy($id){

		// $get_role = $this->check();
		// $access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1);
		// if(isset($get_role['data']['role'])){
			
		// 	if(isset($access['status']) == 200){

		// /*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2);
		// $crud = $FRA->Access(\Request::all(),$this->formx,'read');
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->formx,'delete');
		
		if($access[1] == 200){
			if($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['update'] == 0 && $access[3]['read'] == 0){
				 return \Response::json(['header'=>['message'=>'Unauthorized','status'=>401],'data'=>null],401);
			}else{

				$get = \DB::select("select filename from emp_picture where filename = '$id'");
				if($get){
					foreach ($get as $key) {
						$filename = $key->filename;
					}
					\DB::table('emp_picture')->where('filename','=',$filename)->delete();
					$message='Destroy Successfully.'; $status=200;
					//$message = $access[0]; $status = $access[1]; $data=$access[2];
					return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>null],$status);
				}
			}
			return \Response::json(['header'=>['message'=>'data not access','status'=>403],'data'=>null],403);

		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

		// }else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		// return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);


		// 		else{ $message='ID not found.'; $status=500; }
		// 	}else{ $message='Unauthorized'; $status=401;$data=null; }
		// }else{ $message=$get_role['message']; $status=403;$data=null; }
		// return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
}


// UPDATE ##################################################################################
// PARAM ID INDEX
	public function update_pict($id) // PUT FIX***
	{
		/*SETUP MODEL*/$model = new PersonalPict_Model;	$get_role = $this->check();		$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],2);
		/*SETUP INPUT*/$file=\Input::file('file');
		
		if(isset($get_role['data']['role'])){
			if(isset($access['status']) == 200){
				if( isset($file) && $file){ // jika hanya update file
						
						$check = $model->check_file($id);
						if(isset($check) && $check != null){
							$move = $this->move_file($check);
							if($move){
								$upload =  $this->upload_pict($file);
							
								if(isset($upload['filename'])){
									$model->Update_Picture($id,$upload['filename'], $upload['path']);
									$file->move(storage_path($upload['path']),$upload['filename']);
									$message = 'Updated Successfully.';
									$status = 200;
								}
								else{ $message='Update failed.'; $status=500; }
							}else{ $message=$move['message']; $status=500;}	
						}else{ $message=''; $status=500;}
				}
				else{ $message = 'Input Empty.'; $status = 406; $message_upload = null;}
				return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
			}
			else{ $message = 'Unauthorized'; $status = 401; }
			return \Response::json(['header'=>['message'=>'error','status'=>$status]],$status);
		}else{ return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403); }
	}
//END UPDATE ##############################################################################



// CANCEL ###########################################################################
// FUNGSI UNTUK DELETE RECORD JIKA TIDAK JADI SAVE
		public function cancel(){
		$id = \Request::all();
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data'],$this->form_id);
		if(isset($get_role['data'])){
			if(isset($data_role['create'])){
				$check = PersonalPict_Model::find($id['id']);
				if(isset($check)){
					$cancel = PersonalPict_Model::destroy($id['id']);
					if(isset($cancel)){
						$message = 'Cancel'; $status = 200;
					}else{ $message = 'cancel Failed'; $status = 404; }
				}else{$message = 'id not found'; $status = 404;}
			}else{$message = 'Unauthorized'; $status = 403;}
		}else{$message =$get_role['message']; $status = 401;}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
	}
// END CANCEL ############################################################################


// CONVERT SIZE ##########################################################################
// PARAM byte
	public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824){	$bytes = number_format($bytes / 1073741824, 2) . ' GB';	}
        elseif ($bytes >= 1048576){	$bytes = number_format($bytes / 1048576, 2) . ' MB';	}
        elseif ($bytes >= 1024){	$bytes = number_format($bytes / 1024, 2) . ' KB';}
        elseif ($bytes > 1){	$bytes = $bytes . ' bytes';}
        elseif ($bytes == 1){	$bytes = $bytes . ' byte';	}
        else{	$bytes = '0 bytes'; }
        return $bytes;
	}
// END CONVERT ##########################################################################



// UPLOAD FILE ########################################################################################
// PARAM FILE
	public function upload_pict($file){
		$AllowedExt = ['jpg','jpeg','png','gif','bmp'];
		$CurrentExt = $file->getClientOriginalExtension();
		$date = date_create();
		$data_uniqid = uniqid();
		if(in_array($CurrentExt,$AllowedExt) ){ // filter type yang support upload

			if($file->getClientSize() <= 10000000){
				$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME); //SLICE CHAR
				$str = str_slug($slice_FileName,$separator = "_");
				
				$dir = '/hrms_upload/personal_picture';
				$dir_move = storage_path($dir);
				//$filename = date_timestamp_get($date)."-".$str.".".$CurrentExt;
				//$file->move($dir_move,$filename);
				$data = "photograph_".$data_uniqid.".".$CurrentExt;
				$msg = 'Upload Successfully.';
				return ['filename'=>$data,'path'=>$dir,'type'=>$CurrentExt,'message'=>$msg];
			}
			else{
				$msg = 'Upload Failed, max file <= 10MB ';
				$data = null;
				return ['filename'=>$data,'message'=>$msg];
			}
		}
		else{
			$msg = 'Upload failed, type not support';
			$data = null;
			return ['filename'=>$data,'type'=>$allowed,'msg'=>$msg,'size'=>$this->formatSizeUnits($file->getClientSize()),'message'=>$msg];
		}
	}
//END UPLOAD FILE ####################################################################################


// MOVE FILE IN TRASH ########################################################################
	public function move_file($filename){
		$get_data = \DB::select("SELECT `filename`, `path` from emp_picture where filename = '$filename' ");

		foreach ($get_data as $key) {
			$data['path'] = $key->path;
			$data['filename'] = $key->filename;
		}
		if(\Storage::disk('local')->exists($data['path']."/".$data['filename'])){
			\Storage::disk('local')->move($data['path']."/".$data['filename'],"/Trash/".$data['filename']);
				$message = 'Update Successfully.';
				$status = 200;
		}
		else{
			$message = 'File tidak ditemukan, atau file telah dihapus check folder trash';
			$status = 404;
		}
		return ['message'=>$message,'status'=>$status];
	}
// END MOVE FILE #################################################################################


// CHECK ROLE OR PERMISSION EMPLOYEE  ############################################
	public function check_role($role,$form) // CHECK ROLE FIX***
	{
		$getModel = new Privilege;
		if(isset($role,$form)){
			return $getModel->check_role_personal($role,$form);
		}
		else{
			$msg['warning'] = 'Unauthorize';
			$msg['status'] = 404;
			return $msg; 
		}
	}
// END CHECK ROLE #######################################################################


//CHECK LOCAL IT ###################################################################################
// PARAM = ROLE
	private function check_local($role,$employee_id){
		if(isset($role)){
			$model = new PersonalPict_Model;
			$get = $model->Get_Type($role,$employee_id);
			foreach ($get as $key) {
				$type['local_it'] = $key->local_it;
				$type['employee_id'] = $key->employee_id;
			}
		}else{ $type = null; }
		return $type;
	} 
// END CHECK LOCAL IT ############################################################################
	
	/* ######################################################
	* CHECK KEY YANG DIKIRIM DI ROUTE API
	* KEY DI ENCRYPT DENGAN BASE64(TOKEN-ROLE)
	* IF TRUE ACCESS DENIED
	*/
		public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(isset($q) != null){ //check isset key in session
				 
						 $r = array();
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } // end for store role to array.

						 if(isset($r['key'])){
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							//return $employee_id;
							 $q2 = $getModel->session_role($employee_id);
							 foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 } // end for store role to array.
						 }
						 else{
						 	return ['message'=>'ACCESS DENIED','data'=>null];
						 }
						

				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
						$msg = 'ACCESS GRANTED !';	
					}else{
						$msg = 'ACCESS DENIED1!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED2!';
					$data = null;	
				}
			}
			else{
				$msg = 'ACCESS DENIED3 !';
				$data = null;	
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		
		return ['message'=>$msg,'data'=>$data];
	}

// END CHECK KEY #################################################################################

	/*CHECK TYPE AND ACCESS ########################################################################
	* CHECK TYPE ID EMPLOYEE EXPAT OR LOCAl OR LOCAL_IT
	* ACTION = 1(CREATE & READ), 2(EDIT & UPDATE), 3(DESTROY)
	*/
	private function check_type2($employee_id,$role,$action) // CHECK ROLE FIX***
	{
		$model = new PersonalPict_Model;
		$status_error = 403;
		$not_access = 'Unauthorized';
		$type['data'] = $this->check_local($role,$employee_id);

		$type['local_it'] = $type['data']['local_it'];
		if(isset($type) && $type['local_it']){

			/* ########
			* FOR EXPAT
			*/
			if($type['local_it'] == 1){
				$check = $this->check_role($role,$this->form_expat);
				
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error;}	// ERROR ACCESS
			} // if type local, expat, locit

			/* ########
			* FOR LOCAL
			*/
			elseif($type['local_it'] == 2){
				
				$check = $this->check_role($role,$this->form_local);
				
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
			}

			/* ###########
			* FOR LOCAL_IT
			*/
			elseif($type['local_it'] == 3){
				$check = $this->check_role($role,$this->form_localit);
				
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
			}else{
				$message = $not_access; $status = $status_error;
			}
		} 
		else{
			$message = $not_access; $status = $status_error; // ROLE NOT FOUND
		}
		return ['message'=>$message,'status'=>$status];
	} // CHECK TYPE AND ACCESS ####################################################################
}
