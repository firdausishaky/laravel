<?php namespace Larasite\Http\Controllers\Employees;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*Model*/
use Larasite\Privilege;
use Larasite\Model\Personal_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;
class Personal_Ctrl extends Controller {

protected $form = ['expat'=>19,'local'=>28,'local_it'=>37];
protected $formx = "";
public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	
	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

	if($db_data == 1){
		return $this->formx = "19";
	}elseif($db_data == 2){
		return $this->formx = "28";
	}else{
		return $this->formx = "37";
	}
}


	private function checkID($id)
	{
		$rule = ['undefined',NULL,''];
		if(in_array($id,$rule)){ $data = ['ID Undefined.',500,NULL]; }
		else{ $data = ['OK',200,NULL]; }
		return $data;
	}

	private function set_valid()
	{
		$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-\á\é\í\ó\ú\ü\Á\É\Í\Ó\Ú\Ü\ñ\Ñ ,\'\"\/@\.:\(\)]*$/',
			'text'=>'Regex:/^[ñA-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'num'=>'Regex:/^[0-9-\^ ]+$/',
			'hmo'=>'Regex:/^[ñA-Za-z0-9-\^ ]+$/',
			'twit'=>'regex:/^[ñA-Za-z0-9_]{1,15}$/'];
		
		$rule = [
				// 'first_name'=>'alpha',
				// 'middle_name'=>'alpha',
				// 'last_name'=>$reg['text'],
				// 'ad_username'=>$reg['text_num'],
				// 'sss'=>$reg['num'],
				// 'philhealth'=>$reg['num'],
				// 'hdmf'=>$reg['num'],
				// 'maiden_name'=>'alpha',
				// 'hmo_account'=>$reg['hmo'],
				// 'hmo_numb'=>$reg['hmo'],
				// 'rtn'=>$reg['num'],
				// 'mid'=>$reg['num'],
				// 'tin'=>$reg['num'],
				// 'gender'=>'alpha',
				// 'marital_status'=>'alpha',
				// 'nationality'=>'numeric',				
				// 'date_of_birth'=>'date',
				// 'place_of_birth'=>'alpha',
				// 'religion'=>'alpha',
				// 'biometric'=>'min:2'
				'first_name'=>$reg['text_num'],
				'middle_name'=>$reg['text_num'],
				'last_name'=>$reg['text_num'],
				'ad_username'=>$reg['text_num'],
				'sss'=>$reg['hmo'],
				'philhealth'=>$reg['num'],
				'hdmf'=>$reg['text_num'],
				'hmo_account'=>$reg['hmo'],
				'hmo_numb'=>$reg['hmo'],
				'rtn'=>$reg['text_num'],
				'mid'=>$reg['hmo'],
				'tin'=>$reg['hmo'],
				'gender'=>$reg['text_num'],
				'marital_status'=>$reg['text_num'],
				'nationality'=>$reg['text_num'],
				'date_of_birth'=>'date',
				'place_of_birth'=>$reg['text_num'],
				'religion'=>$reg['text_num']
		];
		if($rule['gender'] == 'female' && $rule['marital_status'] != 'single' ){ $rule['maiden_name'] = "required|".$reg['text_num']; }
		$valid = \Validator::make(\Input::all(),$rule); return $valid;
	}

	private function show_data_personal($id){
		$model = new Personal_Model; // SET MODEL
		$show = $model->Read_Personal($id);
		if($show['data'] && $show['status'] == 200){
			$data=$show['data']; $status=$show['status']; $message='Personal : Show Records Data.'; $pics = $show['picture'];
		}else{ $data=null; $status=200; $message='Personal : Empty Records Data.'; $pics = NULL;}
		return ['data'=>$data,'status'=>$status,'message'=>$message,'picture'=>$pics];
	}

	// SHOW 
	public function show($id)
	{
		/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; $access = $FuncAccess->AccessPersonal(['Personal'=>$id,'Request'=>$REQ],$this->form,1);
		/*Model*/ $model = new Personal_Model;
		/*Access*/$FRA = new FuncAccess;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		//return $access;
		if($access[1] == 200){

			$checkID = $this->checkID($id);
			if($checkID[1] == 500){ $message = $checkID[0]; $status = $checkID[1]; $data = $checkID[2]; }
			else{
				if($access[2][0] == 5 && $id == $access[2][2] ){
					
					$datas = $this->show_data_personal($access[2][2]);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message']; $picture = $datas['picture'];
				}elseif($access[2][0] != 5 && $id == $access[2][2]){
					
					$datas = $this->show_data_personal($access[2][2]);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message']; $picture = $datas['picture'];
				}elseif($access[2][0] != 5 && $id != $access[2][2]) {
					
					$datas = $this->show_data_personal($id);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message']; $picture = $datas['picture'];
				}else{ $pics = null; $data = null; $message='Unauthorized'; $status=500;}

		
			}	
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; $picture =NULL; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3] ],'data'=>$data,'pictures'=>$picture],$status);
	}

	// UPDATE
	public function update($id)
	{
		/*Access*/$FRA = new FuncAccess; 
		/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; $access = $FuncAccess->AccessPersonal(['Personal'=>$id,'Request'=>$REQ],$this->form,3);
		/*Model*/ $model = new Personal_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'update');
		if($access[1] = 200){
			$checkID = $this->checkID($id);
			if($checkID[1] == 500){ $message=$checkID[0]; $status=$checkID[1]; $data=$checkID[2]; }
			else{
				$valid = $this->set_valid();
				if($valid->fails()){ $message = 'Require Input Failed.'; $status = 500; $data = NULL; }
				else{
					$update = $model->Update_Personal($this->check_input(),$id,null);
					$message = $update['message']; $status = $update['status']; $data = $update['data']; 
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3] ],'data'=>$data],$status);	
	}

	// SETUP INPUT
	public function check_input(){
		$input = array();
		$tmp = array();
		$input['first_name']		= \Input::get('first_name');
		$input['middle_name']		= \Input::get('middle_name');
		$input['last_name']			= \Input::get('last_name');
		$input['ad_username']		= \Input::get('ad_username');
		$input['sss']				= \Input::get('sss');
		$input['philhealth']	= \Input::get('philhealth');
		$input['hdmf']				= \Input::get('hdmf');
		$input['maiden_name']		= \Input::get('maiden_name');
		$input['hmo_account']		= \Input::get('hmo_account');
		$input['hmo_numb']			= \Input::get('hmo_numb');
		$input['rtn']				= \Input::get('rtn');
		$input['mid']				= \Input::get('mid');
		$input['tin']				= \Input::get('tin');
		$input['gender']			= \Input::get('gender');
		$input['marital_status']	= \Input::get('marital_status');
		$input['biometric']			= \Input::get('biometric');
		
		if(\Input::get('nationality')){
			$input['nationality']		= \Input::get('nationality');	
		}else{ $input['nationality'] = NULL; }
		
		$input['date_of_birth']		= \Input::get('date_of_birth');
		$input['place_of_birth']	= \Input::get('place_of_birth');
		$input['religion']			= \Input::get('religion');
		foreach ($input as $key => $value) {
			
			if($value!=''){
				$tmp[$key] = $value;
				//array_push($tmp, [$key=>$value]);
			}
		}
		return $tmp;
	}



	// INDEX 
	public function index()
	{ return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>404]],404); }
	// STORE 
	public function store($id)
	{ return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>404]],404);  }
	public function edit($id)
	{ return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>404]],404);  }
	// DESTROY
	public function destroy($id)
	{ return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>404]],404);  }
}
