<?php namespace Larasite\Http\Controllers\Employees;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\Personal_Model;

class Personal_Ctrl extends Controller {

var $form_expat = 19;
var $form_local = 28; 


// INDEX ########################################################################################
	public function index()
	{
		// NOT USES
		return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>404]],404); 
	}
//END INDEX #######################################################################################


// STORE ########################################################################################
	public function store($id)
	{
		// NOT USES
		return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>404]],404); 
	}
//END STORE #######################################################################################


// SHOW ########################################################################################################
	public function show($id)
	{
		// UNTUK MENAMPILKAN DATA YG BERKAITAN DGN ID TERSEBUT

		$get_role = $this->check();		$model = new Personal_Model; $id_emp = $id;
		
		if(isset($get_role['data']['role']) != null){
			

			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1);
			//return $access;
			if(isset($access['status']) && $access['status'] == 200){
				
				if($id == 'undefined' || empty($id)){
					$message='ID Undefined.'; $status=200; $data=null;
				}
					// $data_pic = $model->check_picture($id_emp);
					// $picture = $data_pic['data'];
					
					if($get_role['data']['role'] == 5 && $access['message']['employee_id'] == $get_role['data']['employee_id']){
						
						$result = $model->Read_Personal($get_role['data']['employee_id']);
						if($result['picture'] != null && isset($result['data']) != null && $result['status'] == 200){
						
						$data = $result['data']; $message='View data'; $status=200;
						
						$pics	= $result['picture'];
						}elseif(empty($result['picture']) && isset($result['data']) != null && $result['status'] == 200){
							//return 'pic null, data not null';
							$data = $result['data']; $message='View data'; $status=200; $pics=null;
						}else{ $pics = null; $data = null; $message='Data not found'; $status=500;}
					
					}elseif($get_role['data']['role'] != 5 && $access['message']['employee_id'] != $get_role['data']['employee_id']){

						if($get_role['data']['role'] != null){
							

							$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1);
							
							if($access['status'] && $access['status'] == 200){
									$result = $model->Read_Personal($id_emp);
									if(isset($result['picture']) != null && isset($result['data']) != null && $result['status'] == 200){
									
									$data = $result['data']; $message='View data'; $status=200;
									$pics	= $result['picture'];
									}elseif(empty($result['picture']) && isset($result['data']) != null && $result['status'] == 200){
										//return 'pic null, data not null';
										$data = $result['data']; $message='View data'; $status=200; $pics=null;
									}else{ $pics = null; $data = null; $message='Data not found'; $status=500;}
							}else{ $message='Unauthorized.'; $status=401; $data=null; $pics=null;}
						}else{ $message=$get_role['message']; $status=403; $data=null; $pics=null;}
					
					}else{ $pics = null; $data = null; $message='Unauthorized'; $status=500;}
					

					// if(isset($result['picture']) != null && isset($result['data']) != null && $result['status'] == 200){
						
					// 	$data = $result['data']; $message='View data'; $status=200;
					// 	$pics	= $result['picture'];
					// }elseif(empty($result['picture']) && isset($result['data']) != null && $result['status'] == 200){
					// 	//return 'pic null, data not null';
					// 	$data = $result['data']; $message='View data'; $status=200; $pics=null;
					// }else{ $pics = null; $data = null; $message='Data not found'; $status=500;}
		
			}else{ $message='Unauthorized.'; $status=401; $data=null; $pics=null;}
		}else{ $message=$get_role['message']; $status=403; $data=null; $pics=null;}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data,'pictures'=>$pics],$status); 
	}
// END SHOW #######################################################################################################

// EDIT ########################################################################################################
	public function edit($id)
	{
		// UNTUK MENAMPILKAN DATA YG BERKAITAN DGN ID TERSEBUT
		$get_role = $this->check();		$model = new Personal_Model;
		if(isset($get_role['data']) != null){
			
			$access = $this->check_type2($id,$get_role['data'],1);
			if(isset($access['status']) && $access['status'] == 200){
					
					//$data_pic = $model->check_picture($id);
					$pict = $data_pic['data'];
					$result = $model->Read_Personal($id);
					if(isset($picture['filename']) != null && isset($result['data']) != null && $result['status'] == 200){
						//return 'pic not null, data not null';
						$data = $result['data']; $message='View data'; $status=200;
						$pics	= $picture['data'];
					}elseif(empty($picture['filename']) && isset($result['data']) != null && $result['status'] == 200){
						//return 'pic null, data not null';
						$data = $result['data']; $message='View data'; $status=200;
						$pics	= null;
					}else{
						$pics = null; $data = null; $message='Data not found'; $status=500;
					}
		
			}else{ $message='Unauthorized.'; $status=401; $data=null; $pics=null;}
		}else{ $message=$get_role['message']; $status=403; $data=null; $pics=null;}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data,'picture'=>$pics],$status); 
	}
// END EDIT #######################################################################################################

// UPDATE ######################################################################################
	public function update($id)
	{
		$get_role = $this->check();	$model = new Personal_Model;
		if($get_role['data']['role'] != null){
			
			$access = $this->check_type2($id,$get_role['data']['role'],2);
			if(isset($access['status']) && $access['status'] == 200){
				$input = $this->check_input();
				if($input['data'] != null && $input['status'] == 200){
						$update = $model->Update_Personal($input['data'],$id,null);
						if(isset($update) != null && $update['status'] == 200 ){
							$message=$update['message']; $status=$update['status']; 
						}else{ $message=$update['message']; $status=$update['status']; }
				}else{ $message = 'Input not null'; $status=404; }
			}else{ $message='Unauthorized.'; $status=401;  }
		}else{ $message=$get_role['message']; $status=403; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status); 
	}
// END UPDATE ######################################################################################

// DESTROY
	public function destroy($id)
	{
		// NOT USES
		return \Response::json(['header'=>['message'=>'Method not allowed.','status'=>404]],404); 
	}
// END DESTROY ###################################################################


//GET FILE #######################################################################
	public function get_file($id){
		$model = new Personal_Model;
		$check = $model->check_picture($id);
		if(!empty($checks)){

		}else{  }
		// \Response
	}
// END GET FILE ##################################################################


// SETUP INPUT ###################################################################
	public function check_input(){
		$input = array();
		$input['first_name']		= \Input::get('first_name');
		$input['middle_name']		= \Input::get('middle_name');
		$input['last_name']			= \Input::get('last_name');
		$input['ad_username']		= \Input::get('ad_username');
		//$input['local']				= \Input::get('local');
		$input['sss']				= \Input::get('sss');
		$input['philhealth']	= \Input::get('philhealth');
		$input['hdmf']				= \Input::get('hdmf');
		$input['maiden_name']		= \Input::get('maiden_name');
		$input['hmo_account']		= \Input::get('hmo_account');
		$input['hmo_numb']			= \Input::get('hmo_numb');
		$input['rtn']				= \Input::get('rtn');
		$input['mid']				= \Input::get('mid');
		$input['tin']				= \Input::get('tin');
		$input['gender']			= \Input::get('gender');
		$input['marital_status']	= \Input::get('marital_status');
		$input['nationality']		= \Input::get('nationality');
		$input['date_of_birth']		= \Input::get('date_of_birth');
		$input['place_of_birth']	= \Input::get('place_of_birth');
		$input['religion']			= \Input::get('religion');
		// $input['permanent_address']	= \Input::get('permanent_address');
		// $input['permanent_city']	= \Input::get('permanent_city');
		// $input['permanent_state']	= \Input::get('permanent_state');
		// $input['permanent_zip']		= \Input::get('permanent_zip');
		// $input['permanent_country']	= \Input::get('permanent_country');
		// $input['present_address']	= \Input::get('present_address');
		// $input['present_city']		= \Input::get('present_city');
		// $input['present_state']		= \Input::get('present_state');
		// $input['present_zip']		= \Input::get('present_zip');
		// $input['present_country']	= \Input::get('present_country');
		// $input['home_telephone']	= \Input::get('home_telephone');
		// $input['mobile']			= \Input::get('mobile');
		// $input['work_email']		= \Input::get('work_email');
		// $input['personal_email']	= \Input::get('personal_email');
		if(in_array(" ", $input)){
			$data = 'Input null'; $status=500;
		}else{
			$data = $input; $status =200;
		}
		return ['data'=>$data,'status'=>$status];
	}
// END SETUP INPUT ###############################################################


// CHECK ROLE OR PERMISSION EMPLOYEE  ############################################
	public function check_role($role,$form) // CHECK ROLE FIX***
	{
		$getModel = new Privilege;
		if(isset($role,$form)){
			return $getModel->check_role_personal($role,$form);
		}
		else{
			$msg['warning'] = 'Unauthorize';
			$msg['status'] = 404;
			return $msg; 
		}
	}
// END CHECK ROLE #######################################################################


//CHECK LOCAL IT ###################################################################################
// PARAM = ROLE
	private function check_local($role,$employee_id){
		if(isset($role)){
			$model = new Personal_Model;
			$get = $model->Get_Type($role,$employee_id);
			foreach ($get as $key) {
				$type['local_it'] = $key->local_it;
				$type['employee_id'] = $key->employee_id;
			}
		}else{ $type = null; }
		return $type;
	} 
// END CHECK LOCAL IT ############################################################################


	/* ######################################################
	* CHECK KEY YANG DIKIRIM DI ROUTE API
	* KEY DI ENCRYPT DENGAN BASE64(TOKEN-ROLE)
	* IF TRUE ACCESS DENIED
	*/
		public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(isset($q) != null){ //check isset key in session
				 
						 $r = array();
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } // end for store role to array.

						 if(isset($r['key'])){
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							//return $employee_id;
							 $q2 = $getModel->session_role($employee_id);
							 foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 } // end for store role to array.
						 }
						 else{
						 	return ['message'=>'ACCESS DENIED','data'=>null];
						 }
						

				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
						$msg = 'ACCESS GRANTED !';	
					}else{
						$msg = 'ACCESS DENIED!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED!';
					$data = null;	
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;	
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		
		return ['message'=>$msg,'data'=>$data];
	}
// END CHECK KEY #################################################################################

	/*CHECK TYPE AND ACCESS ########################################################################
	* CHECK TYPE ID EMPLOYEE EXPAT OR LOCAl OR LOCAL_IT
	* ACTION = 1(CREATE & READ), 2(EDIT & UPDATE), 3(DESTROY)
	*/
	private function check_type2($employee_id,$role,$action) // CHECK ROLE FIX***
	{
		$model = new Personal_Model;
		$status_error = 403;
		$not_access = 'Unauthorized';
		$type['data'] = $this->check_local($role,$employee_id);
		$type['local_it'] = $type['data']['local_it'];
		if(isset($type) && $type['local_it']){

			/* ########
			* FOR EXPAT
			*/
			if($type['local_it'] == 1){
				$check = $this->check_role($role,$this->form_expat);
				//return $check;
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error;}	// ERROR ACCESS
			} // if type local, expat, locit

			/* ########
			* FOR LOCAL
			*/
			elseif($type['local_it'] == 2){
				
				$check = $this->check_role($role,$this->form_local);
				//return $check;
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error;}	// ERROR ACCESS
			}

			/* ###########
			* FOR LOCAL_IT
			*/
			elseif($type['local_it'] == 3){
				$check = $this->check_role($role,$this->form_local);
				
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
			}else{
				$message = $not_access; $status = $status_error;
			}
		} 
		else{
			$message = $not_access; $status = $status_error; // ROLE NOT FOUND
		}
		return ['message'=>$message,'status'=>$status];
	} // CHECK TYPE AND ACCESS ####################################################################
}
