<?php namespace Larasite\Http\Controllers\Employees\Qualification;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\Qualification\EmpEducation_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;

class Education_Ctrl extends Controller {

protected $form = ['expat'=>25,'local'=>34,'local_it'=>43];
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}
	
	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "25";
		}elseif($db_data == 2){
				return $this->formx = "34";
		}else{
			return $this->formx = "43";
		}
}
//SETUP
//$rule = ['level'=>\Input::get('level'),'institution'=>\Input::get('institution'),'major'=>\Input::get('major'),'from_year'=>\Input::get('from_year'),'to_year'=>\Input::get('to_year')];
//CHECK ID
private function check_id($id){
	$rule = ['undefined',NULL,''];
	if(in_array($id,$rule)){
		return 500;
	}else{
		$get = \DB::table('education')->where('id','=',$id)->get();
		return $get;
	}

}// VALID
private function set_valid()
{
	$reg = ['textarea'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/'];
	$rule = ['level'=>'required|numeric','institution'=>$reg['textarea'],'major'=>$reg['textarea'],'from_year'=>'date','to_year'=>'date'];
	$valid = \Validator::make(\Input::all(),$rule);
	return $valid;
}
// INDEX
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; 
		$access = $FRA->AccessPersonal(['Personal'=>\Request::get('emp'),'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){

			$show = \DB::select("SELECT id , title as level from education");
			if($show){ return $show; }
			else{ $data=null; $status=404;}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// END INDEX
// GET DATA EDUCATION
public function Get_Edu(){
		$data = \DB::table('education')->get(['id','title']);
		return $data;
}
// END GET EDUCATION
// SHOW
	private function show_data_education($id){
		$model = new EmpEducation_Model;
		$show = $model->Read_Education($id,null);
		if($show['data'] && $show['status'] == 200){
			$data=$show['data']; $status=$show['status']; $message='Education : Show Records Data.';
		}else{ $data=null; $status=200; $message='Education : Empty Records Data.'; }
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
	public function show($id){
		/*Access*/$FRA = new FuncAccess; 
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		$access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					if($access[2][0] == 5 && $id == $access[2][2] ){
						$datas = $this->show_data_education($access[2][2]);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
					elseif($access[2][0] != 5 && $id == $access[2][2] ){
						$datas = $this->show_data_education($access[2][2]);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
					else{
						$datas = $this->show_data_education($id);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
			 }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// END SHOW
// STORE
	public function store($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2); $model= new EmpEducation_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		if($access[1] == 200){
				if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
				else{
					$valid = $this->set_valid();
					if($valid->fails()){ $message='Failed, Required Input.'; $status=500; $data=null; }
					else{
						$input = $this->check_input($id);
							$get = $model->Store_Education($input['data']);
							if(isset($get) && $get != null){
								$message='Store Successfully.'; $status=200; $data=$get['data'];
							}else{ $message='Store not success.'; $status=406; $data=null; }
					}
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access"=> $crud],'data'=>$data],$status);
	}
// END STORE
// EDIT
	public function edit($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>NULL,'Request'=>\Request::all()],$this->form,1); $model= new EmpEducation_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
				if($this->check_id($id) == 500){ $data=null; $status=404; $message='ID Undefined.'; }
				else{
					$edit = $model->Read_Education($id);
					if(isset($edit) != null && $edit['status'] == 200){
						$message='Read Data.'; $status=200; $data=$edit['data'];
					}else{ $message='ID not found.'; $status=500; $data=null; }
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);

	} // END EDIT
// UPDATE
	public function update($id)
	{
		/*Access*/$FRA = new FuncAccess;
		$crud = $FRA->Access(\Request::all(),$this->formx,'update');
		 $access = $FRA->AccessPersonal(['Personal'=>\Request::get('emp'),'Request'=>\Request::all()],$this->form,3); $model= new EmpEducation_Model;
		if($access[1] == 200){
				if($this->check_id($id) == 500){
					$data=null; $status=404; $message='ID Undefined.';
				}else{
					$valid = $this->set_valid();
					if($valid->fails()){
						$error = $valid->errors()->all();
						$message=$error; $status=500; $data=null;
					}else{
					$input = $this->check_input($id);
					$input2 = $input['data'];
					$input2['employee_id'] = $model->Get_emp($id);
					if(isset($input2) != null){
						$update = $model->Update_Education($input2,$id);
						if(isset($update)){ $message='Update Successfully.'; $status=200; $data=EmpEducation_Model::find($id);}
						else{ $message='Update Failed.'; $status=500;  $data=null;}
					}else{ $message='ID not found.'; $status=404; } // problem of id
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	} // END UPDATE ########################################################################################

	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>\Request::get('emp'),'Request'=>\Request::all()],$this->form,4); $model= new EmpEducation_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'delete');
		if($access[1] == 200){
				if($this->check_id($id) == 500){ $data=null; $status=404; $message='ID Undefined.'; }
				else{
						$toArray = explode(",", $id);
						$data = $model->Destroy_Education($toArray);
						if(isset($data)){
							$message = $data['message'];
							$status = $data['status'];
						}else{ $message = 'ID not found'; $status = 404; }
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
	// END DESTROY #######################################################################################
// SEARCH DOMAIN
	public function Search(){
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		$model = new EmpEducation_Model;
		$string = \Input::get('education');
		$search=$model->Search($string);
		if(isset($search) != null || $search['status'] == 200){ $message='Get domain.'; $status=$search['status']; $data=$search['data']; }
		else{ $status=$search['status']; $data=$search['data']; $message='Job not found.'; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// END SEARCH DOMAIN
	private function check_input($id){
		$data = array(); // tampung input to array
		$data['level'] 			= \Input::get('level');
		$data['institution']				= \Input::get('institution');
		$data['major']				= \Input::get('major');
		$data['from_year']				= \Input::get('from_year')."-01";
		$data['to_year']				= \Input::get('to_year')."-01";
		$data['id'] 				= $id;

		if(isset($data)){
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }
		return ['status'=>$status,'data'=>$result];
	}
}
