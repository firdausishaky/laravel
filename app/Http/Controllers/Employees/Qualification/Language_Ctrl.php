<?php namespace Larasite\Http\Controllers\Employees\Qualification;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*Model*/
use Larasite\Privilege;
use Larasite\Model\Qualification\EmpLanguage_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
class Language_Ctrl extends Controller {

protected $form = ['expat'=>25,'local'=>34,'local_it'=>43];
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "25";
		}elseif($db_data == 2){
				return $this->formx = "34";
		}else{
			return $this->formx = "43";
		}
}
// CHECK ID
private function check_id($id){
	$rule = ['undefined',NULL,''];
	if(in_array($id,$rule)){ $data = 500; }
	else{ $data = 200; }
	return $data;
}
// SET VALID
private function set_valid(){
	$rule = ['language'=>'required|numeric','fluent'=>'alpha'];
	$valid = \Validator::make(\Input::all(),$rule); return $valid;
}

// INDEX
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; 
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		$access = $FRA->AccessPersonal(['Personal'=>\Request::get('emp'),'Request'=>\Request::all()],$this->form,1);
		if($access[1] == 200){
			$show = \DB::table('language')->get(['id','title']);
			if($show){ return $show;}
			else{ $data=null; $status=404; $message='Empty Records Data.';}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// SHOW
	private function show_data_language($id){
		$model = new EmpLanguage_Model;
		$show = $model->Read_Language($id,null);
		if($show['data'] && $show['status'] == 200){
			$data=$show['data']; $status=$show['status']; $message='Language : Show Records Data.';
		}else{ $data=null; $status=200; $message='Language : Empty Records Data.'; }
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
// SHOW
	public function show($id){
		/*Access*/$FRA = new FuncAccess;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		 $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					if($access[2][0] == 5 && $id == $access[2][2] ){
						$datas = $this->show_data_language($access[2][2]);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
					elseif($access[2][0] != 5 && $id == $access[2][2] ){
						$datas = $this->show_data_language($access[2][2]);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
					else{
						$datas = $this->show_data_language($id);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// END SHOW
// STORE
	public function store($id)
	{
		/*Access*/$FRA = new FuncAccess;
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		 $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2); $model= new EmpLanguage_Model;
		if($access[1] == 200){
				if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
				else{
					$valid = $this->set_valid();
					if($valid->fails()){ $message='Failed, Required Input.'; $status=500; $data=null; }
					else{
							$input = $this->check_input($id);
							if(isset($input['status']) != null && $input['status'] == 200){
								$get = $model->Store_Language($input['data']);
								
								if(isset($get) && $get != null){
									$message='Store Successfully.'; $status=200; $data=$get['data'];
								}else{ $message='Store not success.'; $status=406; $data=null; }
							}else{ $message=$input; $status=500; $data=null; }
					} // Valid input
				} // Valid id
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// END STORE
// EDIT
	public function edit($id)
	{
		/*Access*/$FRA = new FuncAccess; 
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		$access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1); $model= new EmpLanguage_Model;
		if($access[1] == 200){
				if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
				else{ $message = 'Page Not Found'; $status = 404; $data=NULL;} // Valid id
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// END EDIT
// UPDATE
	public function update($id)
	{
		/*Access*/$FRA = new FuncAccess;
		$crud = $FRA->Access(\Request::all(),$this->formx,'update');
		 $access = $FRA->AccessPersonal(['Personal'=>\Request::get('emp'),'Request'=>\Request::all()],$this->form,3); $model= new EmpLanguage_Model;
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					$valid = $this->set_valid();
					if($valid->fails()){ $message='Failed, Required Input.'; $status=500; $data=null; }
					else{
						/**Input*/$input = $this->check_input($id);
						if(isset($input['status']) != null && $input['status'] == 200){
							$input2 = $input['data']; $input2['employee_id'] = $model->Get_emp($id);
							$update = $model->Update_Language($input2,$id);
							if(isset($update)){ $message='Update Successfully.'; $status=200; $data=EmpLanguage_Model::find($id); }
							else{ $message='Update Failed.'; $status=500; $data=null; }
						}else{ $message=$input; $status=500; $data=null; } // problem of input

					} // Valid input
				} // Valid id
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	} // END UPDATE
// DESTROY
	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; 
		$crud = $FRA->Access(\Request::all(),$this->formx,'delete');
		$access = $FRA->AccessPersonal(['Personal'=>\Request::get('emp'),'Request'=>\Request::all()],$this->form,4); $model= new EmpLanguage_Model;
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$toArray = explode(",", $id);
				$data = $model->Destroy_Language($toArray);
				if(isset($data)){
					$message = $data['message']; $status = $data['status'];	 $data=null;
				}else{ $message = 'ID not found'; $status = 404; $adat=null; }
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}// END DESTROY
// SEARCH DOMAIN
	public function Search(){
		/*Access*/$FRA = new FuncAccess; 
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
		$model = new EmpLanguage_Model;
		$string = \Input::get('job');
		$search=$model->Search($string);
		if(isset($search) != null || $search['status'] == 200){ $message='Get domain.'; $status=$search['status']; $data=$search['data']; }
		else{ $status=$search['status']; $data=$search['data']; $message='Job not found.'; }}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// END SEARCH DOMAIN
//CHECK INPUT
	private function check_input($id){
		$data = array(); // tampung input to array
		$data['language'] 			= \Input::get('language');
		$data['fluent']				= \Input::get('fluent');
		$data['id'] 				= $id;

		if(isset($data)){
			$data['comment']		= \Input::get('comment');
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }
		return ['status'=>$status,'data'=>$result];
	}
}
