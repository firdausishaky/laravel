<?php namespace Larasite\Http\Controllers\Employees\Qualification;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\Qualification\Seminar_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
class Seminar_Ctrl extends Controller {

// FIX ALL ###
protected $form = ['expat'=>25,'local'=>34,'local_it'=>43];
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "25";
		}elseif($db_data == 2){
				return $this->formx = "43";
		}else{
			return $this->formx = "43";
		}
}
protected $dir = "/hrms_upload/seminar";
protected $Extfile = ["title"=>"seminar","extend"=>["jpg","png","gif","pdf","jpeg"]];

private function check_id($id)
{
	$rule = ['undefined',NULL,''];
	if(in_array($id,$rule)){ return 500; }else{ return 200; }
}
private function set_valid(){
	$rule = ['name'=>'required|alpha','location'=>'alpha_num','date'=>'date'];
	$valid = \Validator::make(\Input::all(),$rule); return $valid;
}
// MOVE
	private function Move($id,$file,$type){
		$model = new Seminar_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		$check = $model->check_file_id($id);
		$checks = Seminar_Model::find($id);

		if($check || isset($checks->title) || $id){
			if($type == 'cancel'){
				$move = $FuncUpload->move_file($check,'emp_seminar');
				$message='Cancel Operation.'; $status=200; $data = null;
			}
			elseif($type == 'delfile'){
				$move = $FuncUpload->move_file($check,'emp_seminar');
				$message=$move['message']; $status=$move['message']; $data=NULL;
			}
			else{
				if($check){
					$move = $FuncUpload->move_file($check,'emp_seminar');
					$up = $this->Upload($id,$file,false);
					$message=$up[0]; $status=$up[1]; $data=$up[2];
				}else{
					$up = $this->Upload($id,$file,false);
					$message=$up[0]; $status=$up[1]; $data=$up[2];
				}
			}
		}else{ $up = $this->Upload($id,$file,true); $message=$up[0]; $status=$up[1]; $data=$up[2]; }
		return [$message,$status,$data];
	}
// END MOVE
// UPLOAD
	private function Upload($id,$files,$type){
		$model = new Seminar_Model; /*Upload*/ $FuncUpload = new FuncUpload; $dir = $this->dir;
		$file = $files['file'];
		$upload =  $FuncUpload->upload($this->dir,$file,$this->Extfile);
		if($upload['filename']){
			if($type == true){
				\DB::table('emp_seminar')->insert(['employee_id'=>$files['id'],'filename'=>$upload['filename'],'path'=>$upload['path']]);
				$file->move(storage_path($upload['path']),$upload['filename']);
				$message = $upload['message']; $data = ['id'=>$model->GetID($upload['filename']),'filename'=>$upload['filename']]; $status = 200;
			}else{
				Seminar_Model::where('id','=',$id)->update(['filename'=>$upload['filename'],'path'=>$upload['path']]);
				$file->move(storage_path($upload['path']),$upload['filename']);
				$message = 'Updated Successfully.';$status = 200; $data=Seminar_Model::find($id);
			}
		}else{ $message='Update Failed.'; $status=500; $data = $update;}
		return [$message,$status,$data];
	}
// END UPLOAD
public function download($id,$filename){
		 $get_role = $this->check_id($id);
		if($get_role == 200){
			 $check = \DB::select("SELECT filename from emp_seminar where employee_id = '$id' and filename = '$filename'");
			 if(!$check){ return \Response::json(['header'=>['message'=>'File not found']],404); }
			foreach ($check as $key) { $files = $key->filename;}
			$path = storage_path()."/hrms_upload/seminar/".$files;
			$file = \File::get($path); $type = \File::mimeType($path);
			return \Response::make($file,200,['Content-Type'=>$type]);
		}else{ return \Response::json(['header'=>['message'=>'File not found.','status'=>401]],401); }
}
// INDEX
public function index()
{
	/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>NULL,'Request'=>\Request::all()],$this->form,1);
	$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			$show = \DB::table('emp_seminar')->get(['id','name']);
			if($show != null ){ 
				foreach ($show as $key ) {
					if($key->date == '0000-00-00'){ $key->date = NULL; }
					$show[] = $key;
				}

				return $show;}
			else{ $data=null; $status=404; $message='Empty Records Data.';}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
}
// SHOW
	private function show_data_seminar($id){
		$model = new Seminar_Model; // SET MODEL
		$show = $model->Read_Seminar($id,null);
		if($show['data'] && $show['status'] == 200){
			if($show != null ){ 
				foreach ($show['data'] as $key ) {
					if($key->date == '0000-00-00'){ $key->date = NULL; }
					$showx[] = $key;
				}

			}
			$data=$showx; $status=$show['status']; $message='Seminar : Show Records Data.';
		}else{ $data=null; $status=200; $message='Seminar : Empty Records Data'; }
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
// SHOW
	public function show($id){
		/*Access*/$FRA = new FuncAccess; 
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		$access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1); /*Upload*/ $FuncUpload = new FuncUpload;

		if($access[1] == 200){

			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				if($access[2][0] == 5 && $id == $access[2][2] ){
						$datas = $this->show_data_seminar($access[2][2]);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
					elseif($access[2][0] != 5 && $id == $access[2][2] ){
						$datas = $this->show_data_seminar($access[2][2]);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
					else{
						$datas = $this->show_data_seminar($id);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}
// STORE
	public function store($id){
		$file = \Input::file('file'); $input = $this->set_input();
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2); $FuncUpload = new FuncUpload;
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		if($access[1] == 200){
				if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
				else{
					$request = new leaverequest_Model;
					$json = \Input::get('data');
					if($json != null){
						$data =  json_encode($json,1);
						$name = $data['data']['name'];
						$date = $data['data']['date'];
						$location = $data['data']['location'];
						$image = \Input::file('file');

						if (isset($image)){
							$imageName = $image->getClientOriginalName();
							$ext = \Input::file('file')->getClientOriginalExtension();
							$path = "/hrms_upload/seminar";
							$size = $image->getSize();

							$req_image = $request->check_image($ext,$size,$image,$path,$crud[3]);
							if($req_image != null && $req_image == "size"){
								return $request->getMessage("failed image format supported but size of image more than 1 MB",[],200,$access);
							}
							if($req_image != null && $req_image == "format"){
								return $request->getMessage("unsupported format",[],200,$access);
							}

						}else{
							$imageName =  "-";
							$path = "-";
						}

					}else{
						$date = \Input::get('date');
						$location = \Input::get("location");
						$name = \Input::get("name");
						$imageName =  "-";
						$path = "-";
					}
					$db = \DB::SELECT(" insert into emp_seminar(employee_id,date,location,filename,path) values($id,$date,$location,$imageName,$path)");
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// END STORE
	// public function store_string($id)
	// {
	// 	/*Access*/$FRA = new FuncAccess; 
	// 	$crud = $FRA->Access(\Request::all(),$this->formx,'create');
	// 	$access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,3); /*Model*/ $model = new Seminar_Model; /*Upload*/ $FuncUpload = new FuncUpload;
	// 	if($access[1] == 200){
	// 		if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
	// 		else{
	// 				$id_seminar = \Input::get('id'); $file = \Input::file('file'); $input = $this->set_input();
	// 					// STRING
	// 					if(isset($id) && $input['status'] == 200){
	// 						$valid = $this->set_valid();
	// 						if($valid->fails()){ $message='Input Failed.'; $status=500; $data=null; }
	// 						else{
	// 							$check = Seminar_Model::find($id_seminar);
	// 							if($check){
	// 								Seminar_Model::where('id','=',$id_seminar)->update($input['data']);
	// 								$message = 'Store Successfullys.'; $status = 200; $data = $check;
	// 							}else{
	// 								$input_ins = $input['data']; $input_ins['employee_id'] = $id;
	// 								if(\DB::table('emp_seminar')->insert($input_ins)){
	// 									$get = \DB::select("SELECT id, name,location,`date` from emp_seminar where employee_id = '$id' order by id desc");
	// 									$message = 'Store Successfully.'; $status = 200; $data = $get[0];
	// 								}else{ $message = 'Store Failed.';	 $status = 500; $id_seminar = null; }
	// 							}
	// 						}//Valid
	// 					}
	// 					// MOVE
	// 					elseif( isset($id,$id_seminar,$file) && Seminar_Model::find($id_seminar) && $input['status'] == 500){
	// 						$upload = $this->upload_seminar($file);
	// 						$file->move(storage_path($upload['path']),$upload['filename']);
	// 						\DB::table('emp_seminar')->where('id','=',$id_seminar)->update(['filename'=>$upload['filename'],'path'=>$upload['path']]);

	// 						$message = $upload['message'];
	// 						$data = ['filename'=>$upload['filename'],'id'=>$id_seminar];
	// 						$status = 200;
	// 					}
	// 					else{ $message = 'Store Not Successfully.'; $status = 406; $data = null; }
	// 		}// id
	// 	}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" =>$crud[3]],'data'=>$data],$status);
	//}
//END STORE LANJUTAN
// UPDATE
	public function update_seminar($id) // PUT FIX***
	{
		/*Access*/$FRA = new FuncAccess; 
		$crud = $FRA->Access(\Request::all(),$this->formx,'update');
		/*SETUP INPUT*/$input = $this->set_input();	$file=\Input::file('file');
		$access = $FRA->AccessPersonal(['Personal'=>\Request::get('emp'),'Request'=>\Request::all()],$this->form,3); /*Model*/ $model = new Seminar_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				 $radio = \Input::get('radio');
				if(isset($id,$file) && $input['status'] == 500 ){ // jika hanya update file
						$dataMove = ['id'=>$id,'file'=>$file];
						$Move = $this->Move($id,$dataMove,null);
						$message=$Move[0]; $status=$Move[1]; $data=$Move[2];
				}
				elseif( isset($id,$input) && $input['status']==200 && empty($file) ){ //  jika update only title or descript
					$valid = $this->set_valid();
					if($valid->fails()){ $message='Required Input.'; $data=null; $status=500;  }
					else{
						$input2 = $input['data'];

						if($radio == 2){ $this->Move($id,['id'=>$id,'file'=>NULL],'delfile'); $input2['filename'] = NULL; $input2['path'] = NULL;}

						$data = $model->Update_seminar($id,$input2);
						if(isset($data['status']) == 200){ $message='Update Successfully.'; $status=$data['status'];	$data = Seminar_Model::find($id); }
						else{ $message=$data['message']; $status=$data['status'];	$data=null; }
					}
				}
				else{ $message = 'Input Empty.'; $status = 500; $data = null;}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
//END UPDATE
// DESTROY
	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess;
		$crud = $FRA->Access(\Request::all(),$this->formx,'destroy');
		 $access = $FRA->AccessPersonal(['Personal'=>\Request::get('emp'),'Request'=>\Request::all()],$this->form,4); /*Model*/ $model = new Seminar_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					$toArray = explode(",", $id);
					$data = $model->Destroy_Seminar($toArray);
					if(isset($data) && $data['status'] == 200){
						$message = $data['message'];$status = $data['status']; $data =null;
					}else{ $message = 'ID not found'; $status = 404; $data=null; }
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);

	}// END DESTROY #######################################################################################
	private function set_input(){
		$data = array(); // tampung input to array
		if(\Input::get('name')){ $data['name'] = \Input::get('name'); }
		else{ $data['name'] = NULL; }

		if(\Input::get('location')){ $data['location'] = \Input::get('location'); }
		else{ $data['location'] = ''; }

		if(\Input::get('date')){ $data['date'] = \Input::get('date'); }
		else{ $data['date'] = NULL; }

		if(isset($data['name'])){
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }
		return ['status'=>$status,'data'=>$result];
	}
}
