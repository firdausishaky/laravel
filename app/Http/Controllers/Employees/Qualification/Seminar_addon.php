<?php namespace Larasite\Http\Controllers\Employees\Qualification;
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Model\Qualification\Seminar_Model ;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;
class Seminar_addon extends Controller {

protected $formx = "";
protected $form = ['expat'=>25,'local'=>34,'local_it'=>43];
public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "25";
			
		}elseif($db_data == 2){
				return $this->formx = "43";
			
		}else{
			return $this->formx = "43";
		}
}
	public function store($id){
		
		/*Access*/$FRA = new FuncAccess; 
		$access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2);
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		if($crud[1] == 200){
		
					$request = new leaverequest_Model;
					$json = \Input::get('data');

					if($json != null){
						$data =  json_decode($json,1);
						$name = $data['data']['name'];
						$image = \Input::file('file');

						if (!preg_match("/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/",$name)) {
 							 return $request->getMessage("invalidate input format",[],200,$crud[3]);
						}
						(isset($data['data']['date']) ?  $date = $data['data']['date'] : $date =null);
						(isset($data['data']['location']) ? $location = $data['data']['location'] : $location =null);
						
						$imageName = $image->getClientOriginalName();
						$ext = \Input::file('file')->getClientOriginalExtension();
						$path = "hrms_upload/seminar";
						$size = $image->getSize();
						
						$req_image = $request->check_imgPDF($ext,$size,$image,$path,$crud[3]);
						
						if($req_image != null && $req_image == "size"){
							return $request->getMessage("failed image format supported but size of image more than 1 MB",[],200,$crud[3]);
						}elseif($req_image != null && $req_image == "format"){
							return $request->getMessage("unsupported file format",[],200,$crud[3]);
						}else{
							$imageName = $req_image;
						}
						
					}else{
						$date = \Input::get('date');
						$location = \Input::get("location");
						$name = \Input::get("name");
						$imageName =  null;
						$path = null;
						(!isset($date) ? $date = null: $date =$date);
						(!isset($location) ? $location = null : $location =$location);
					}
					$db = \DB::SELECT(" insert into emp_seminar(employee_id,name,date,location,filename,path) values('$id','$name','$date','$location','$imageName','$path')");
					
					if(isset($db)){
						$data = \DB::SELECT(" select date,filename,id,location,name,path from emp_seminar order by created_at desc  limit 1");
					
						$data_tanggal = $data[0]->date;
						if($data_tanggal == "0000-00-00"){
							$datax[] = [
									"date" => "",
									"filename" =>$data[0]->filename,
									"id" =>$data[0]->id,
									"location" => $data[0]->location,
									"name" => $data[0]->name,
									"path" => $data[0]->path
								  ];
								  return  $request->getMessage("Success",$datax[0],200,$crud[3]);
						}
						$message =  "success";
						$status = 200;
						return  $request->getMessage("Success",$data[0],200,$crud[3]);
					 }
				
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}

	public function update($id){
		/*Access*/$FRA = new FuncAccess; 
		$access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2);
		$crud = $FRA->Access(\Request::all(),$this->formx,'update');
		$model = new Seminar_Model;
		if($crud[1] == 200){
					
					$request = new leaverequest_Model;
					$json = \Input::get('data');
					$key = \Input::get('key');
					$keys= base64_decode($key);
					$test = explode('-',$keys);
					$emp = $test[1];
					$activate = "";
					if($json != null){

						$data =  json_decode($json,1);
						$name = $data['data']['name'];
						if (!preg_match("/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/",$name)) {
 							 return $request->getMessage("invalidate input format",[],200,$crud[3]);
						}
						(isset($data['data']['date']) ?  $date = $data['data']['date'] : $date =null);
						(isset($data['data']['location']) ? $location = $data['data']['location'] : $location =null);
						(isset($data['data']['path']) ?  $date = $data['data']['path'] : $date =null);
						// ($data['data']['filename'] == null ? $location = $data['data']['filename'] : $location =null);
		
						$image = \Input::file('file');
						if($image != null){
							$imageName = $image->getClientOriginalName();

							$ext = \Input::file('file')->getClientOriginalExtension();

							$path = "hrms_upload/seminar";
							$size = $image->getSize();
							$activate = 1;
							
							$check = $model->chk_img_data($id);

							$req_image = $request->check_imgPDF($ext,$size,$image,$path,$crud[3]);
							if($req_image != null && $req_image == "size"){
								return $request->getMessage("failed image format supported but size of image more than 10 MB",[],200,$access);
							}
							if($req_image != null && $req_image == "format"){
								return $request->getMessage("unsupported format",[],200,$access);
							}else{
								$imageName = $req_image;
							}
						}else{
							$imageName = "null";
							$path = "null";
						}
						
					}else{
						$activate = 0;
						$date = \Input::get('date');
						$location = \Input::get("location");
						$name = \Input::get("name");
						$imageName =  "null";
						$path = "null";
						$check = $model->chk_img_data($id);
						(!isset($date) ? $date = 'null' : $date =$date);
						(!isset($location) ? $location = "null" : $location =$location);

					}

						$db = \DB::SELECT(" update emp_seminar set name='$name',date='$date',location='$location',filename='$imageName',path='$path' where id=$id");
					
					
					if(isset($db)){
						$data = \DB::SELECT(" select date,filename,id,location,name,path from emp_seminar where id=$id");
						$message =  "success";
						$status = 200;
						return  $request->getMessage("Success",$data[0],200,$crud[3]);
					 }
				
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}



}