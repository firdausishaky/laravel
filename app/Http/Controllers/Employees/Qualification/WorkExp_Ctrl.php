<?php namespace Larasite\Http\Controllers\Employees\Qualification;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\ORM\Job_Orm;
use Larasite\Model\Qualification\WorkExp_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
class WorkExp_Ctrl extends Controller {

protected $form = ['expat'=>25,'local'=>34,'local_it'=>43];
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "25";
		}elseif($db_data == 2){
				return $this->formx = "43";
		}else{
			return $this->formx = "43";
		}
}
// CHECK ID
private function check_id($id){
	$rule = ['undefined',NULL,''];
	if(in_array($id,$rule)){ $data = 500; }else{ $data = 200; }
	return $data;
}
// SET VALID
private function set_valid(){
	$reg = ['textarea'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/'];
	$rule = ['company'=>'required|'.$reg['textarea'].'|max:50','location'=>$reg['textarea'].'|max:30','job'=>'required|'.$reg['textarea'].'|max:50','from_date'=>'required|date',
			'to_date'=>'required|date','reason_of_leaving'=>'alpha_num'];
	$valid = \Validator::make(\Input::all(),$rule); return $valid;
}
// INDEX #####################################################################################
	public function index()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
	 		if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
	 			$message = "Unauthorized"; $status=200; $data=[];
	 		}
					$show = Job_Orm::get(['id','title']);
						if($show){
							return \Response::json($show,200);
						}else{ $data=null; $status=404;}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access" => $access[3]],$status);
	}
// SHOW
	private function show_data_workexp($id){
		$model = new WorkExp_Model; // SET MODEL
		$show = $model->Read_WorkEXP($id,null);
		if($show['data'] && $show['status'] == 200){
			$data=$show['data']; $status=$show['status']; $message='Work Experience : Show Records Data.';
		}else{ $data=null; $status=200; $message='Work Experience : Empty Records Data.'; }
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
// SHOW
	public function show($id){
		/*Access*/$FRA = new FuncAccess; 
		$access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				if($access[2][0] == 5 && $id == $access[2][2] ){
						$datas = $this->show_data_workexp($access[2][2]);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
					elseif($access[2][0] != 5 && $id == $access[2][2] ){
						$datas = $this->show_data_workexp($access[2][2]);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
					else{
						$datas = $this->show_data_workexp($id);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// STORE
	public function store($id)
	{
		/*Access*/$FRA = new FuncAccess; 
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		$access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2);$model = new WorkExp_Model;
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				/*Valid*/ $valid = $this->set_valid();
				if($valid->fails()){ $message='Failed, Required Input.'; $status=500; $data=null; }
				else{
					$input = $this->check_input($id);
					$get = $model->Store_WorkExp($input['data']);
					if(isset($get) && $get != null){
						$message='Store Successfully.'; $status=200; $data=$get['data'];
					}else{ $message='Store not success.'; $status=406; $data=null; }
			}	}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// EDIT
	public function edit($id)
	{
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{ $message='Page Not Found.'; $status=404; $data=null; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
//UPDATE
	public function update($id)
	{
		/*Access*/$FRA = new FuncAccess; 
		$crud = $FRA->Access(\Request::all(),$this->formx,'update');
		$access = $FRA->AccessPersonal(['Personal'=>\Request::get('emp'),'Request'=>\Request::all()],$this->form,3); $model = new WorkExp_Model;
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$valid = $this->set_valid();
				if($valid->fails()){ $message='Required Input.'; $status='500'; $data=null; }
				else{
					$input = $this->check_input($id); $input2 = $input['data'];
					$input2['employee_id'] = $model->Get_emp($id);
					$update = $model->Update_WorkExp($input2,$id);
					if(isset($update)){ $message='Update Successfully.'; $status=200; $data=$update;}
					else{ $message='Update Failed.'; $status=500; $data=$update;  }
			}	}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// DELETE
	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; 
		$crud = $FRA->Access(\Request::all(),$this->formx,'delete');
		$access = $FRA->AccessPersonal(['Personal'=>\Request::get('emp'),'Request'=>\Request::all()],$this->form,4); $model = new WorkExp_Model;
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				// $check = WorkExp_Model::find($id);
				// 	if($check){
						$toArray = explode(",", $id);
						$data = $model->Destroy_WorkExp($toArray);
						if(isset($data)){
							$message = $data['message'];
							$status = $data['status'];
						}else{ $message = 'ID not found'; $status = 404; }
					// }
					// else{ $message='ID Not Found'; $status=404; $data=null; }
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// SEARCH DOMAIN
	public function Search(){
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($crud[1] == 200){
		$model = new WorkExp_Model;
		$string = \Input::get('job');
		$search=$model->Search($string);
		if(isset($search) != null || $search['status'] == 200){ $message='Get domain.'; $status=$search['status']; $data=$search['data']; }
		else{ $status=$search['status']; $data=$search['data']; $message='Job not found.'; }}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access" => $crud[3]],$status);
	}
// END SEARCH DOMAIN
// CHECK INPUT
	private function check_input($id){
		$data = array(); // tampung input to array
		$data['company'] 				= \Input::get('company');
		$data['location'] 				= \Input::get('location');
		$data['job'] 					= \Input::get('job');
		$data['from_date']					= \Input::get('from_date');
		$data['to_date'] 					= \Input::get('to_date');
		$data['length_of_service'] 		= $this->Calculate_RangeDate($data['from_date'],$data['to_date']);
		$data['reason_of_leaving'] 	= \Input::get('reason_of_leaving');
		$data['id'] 					= $id;

		if(isset($data)){
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }
		return ['status'=>$status,'data'=>$result];
	}
// CALCULATE
	private function Calculate_RangeDate($date1,$date2){
		$diff = abs(strtotime($date2) - strtotime($date1));

		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		//$cal = printf("%d years, %d months, %d days\n", $years, $months, $days);
		$cal = "$years years $months months $days days";
		return $cal;
	}
}
