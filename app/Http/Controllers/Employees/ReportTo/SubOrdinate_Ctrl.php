<?php namespace Larasite\Http\Controllers\Employees\ReportTo;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\ReportTo\SubOrdinate_Model;
use Larasite\Model\ReportTo\Supervisor_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;

class SubOrdinate_Ctrl extends Controller {

protected $form = ['expat'=>24,'local'=>33,'local_it'=>42];
protected $formx = "";

public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "24";
		}elseif($db_data == 2){
				return $this->formx = "33";
		}else{
			return $this->formx = "42";
		}
}
private function GetEMP(){
	$remove = json_decode(file_get_contents('php://input'));
	if(gettype($remove) == 'object'){
		foreach ($remove as $key) { $tmp = $key; }
		return $tmp;
	}else{
		return \Input::get('emp');
	}
}

	private function check_id($id)
	{
		$rule = ['undefined',NULL,''];
		if(in_array($id,$rule)){ return 500; }else{ return 200; }
	}
	private function set_valid()
	{
		$reg = ['text'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
				'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
		$rule = ['subordinate'=>'numeric'];
		$valid = \Validator::make(\Input::all(),$rule); return $valid;
	}
	private function Searching($Personal,$r)
	{

		if($r['key'] && $r['search'] != null && $r['search'] != 'undefined'){
			$decode = base64_decode($r['key']);
			$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
			$name = $r['search'];
			$check = \DB::table('emp_subordinate')->get(['subordinate']);

			if($check){ foreach ($check as $key) { $data_check = $key->subordinate; }}
			else{ $data_check = NULL; }

		}else{ $message='Access Denied'; $status=500; }
		$check1 = \DB::select("SELECT subordinate from emp_subordinate where employee_id = '$Personal' group by subordinate");
		$check2 = \DB::select("SELECT supervisor from emp_supervisor where employee_id = '$Personal' group by supervisor");

		$datas=NULL;
		if($check2){
			foreach ($check2 as $key) {
				$datas .= $key->supervisor.',';
			}
		}else{ $datas = NULL; }
		
		if($check2 != null){
			if($check1 != null){
				foreach ($check1 as $key) {
					$datas .= $key->subordinate.',';
				}
			}	
		}else{
			if($check1 != null){
				foreach ($check1 as $key) {
					$datas .= $key->subordinate.',';
				}
			}else{ $datas = NULL; }	
		}
		
		if($datas != null){
			$rtrim = ltrim($datas,",");
			$rtrim .= "2014888".","."2014999".",".$Personal;
			$ltrim = rtrim($rtrim,",");
			$ltrim = preg_replace("/,+/", ",", $ltrim);
			
			$show = \DB::select("SELECT distinct a.employee_id , concat(a.first_name,' ',if(a.middle_name is null or a.middle_name = '',' ',a.middle_name),' ',a.last_name) as name from emp a, emp_supervisor b where (a.employee_id not in($ltrim)) and a.first_name LIKE '$name%'");
		}else{
			$rtrim = ltrim($datas,",");
			$rtrim .= "2014888".","."2014999".",".$Personal;
			$ltrim = rtrim($rtrim,",");
			$ltrim = preg_replace("/,+/", ",", $ltrim);
			
			$show = \DB::select("SELECT distinct a.employee_id , concat(a.first_name,' ',if(a.middle_name is null or a.middle_name = '',' ',a.middle_name),' ',a.last_name) as name from emp a where a.employee_id not in($ltrim) and a.first_name LIKE '$name%' ");
		}
		if($show){ $data=$show; $status=200; }
		else{ $data=[]; $status=500; $status = "Employee already use.";}
		return ['data'=>$data,'status'=>$status];
	}
// INDEX
	public function index()
	{
		
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1); $model = new SubOrdinate_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			$get = $this->Searching(\Request::all());
			return \Response::json("1");
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// SEARCH DOMAIN
	public function Search($id,Request $REQ){
				
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>$REQ->all()],$this->form,1); $model = new SubOrdinate_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			$get = $this->Searching($id,$REQ->all());
			foreach ($get['data'] as $key => $value) {
				if(isset($value->name)){	
					if(strpos($value->name,'(PAST)')){
						unset($get['data'][$key]);
					}
					if(strpos($value->name,'(DEL)')){
						
						unset($get['data'][$key]);
					}
					if(strpos($value->name,'(PAST).(DEL).A')){
						unset($get['data'][$key]);
					}
				}
			}
			if($get['data'] ==  null){
				$get['data'] = [["name" => "employee already in use or employee not found."]];
			}
			if($get['status'] ==  null){
				$get['status'] = 200;
			}

			$tmp = [];
			foreach ($get['data'] as $key) {
			 	array_push($tmp, $key);
			}
			$get['data'] = $tmp;
			return $get;
		//return \Response::json(['header'=>['message'=>"success",'status'=>$get['status'], "access" => $crud[3]],'data'=>$get['data']],$get['status']);
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		//return gettype($get);
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}

// SHOW
	private function show_data_subordinate($id){
		$model = new SubOrdinate_Model; // SET MODEL
		$show = $model->Read_Sub_Unit($id,null);
		if(isset($show) && $show['status'] == 200){
			if($show['data']){
				$status=$show['status']; $message='Subordinate : Show Records Data.';	$data = $show['data'];
			}else{ $status=$show['status']; $message='Subordinate : Empty Records Data.'; $data = null;}
		}else{ $data=null; $status=200; $message='Subordinate : Empty Records Data.'; }
		return ['data'=>$data,'status'=>$status,'message'=>$message];
	}
// SHOW
	public function show($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1);
		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				if($access[2][0] == 5 && $access[2][1] == $access[2][2] ){
					
					$datas = $this->show_data_subordinate($access[0],['employee_id']);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
				}else{
					$datas = $this->show_data_subordinate($id);
					$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// STORE
	public function store_db($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,1); $model = new SubOrdinate_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'create');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$valid= $this->set_valid();
				if($valid->fails()){$message='Required Input.'; $status=500; $data=null;}
				else{
					$input = $this->check_input($id);

					$get = $model->Store_Sub_Unit($id,$input['data']);
					if(isset($get) && $get != null){
						$message=$get['message']; $status=$get['status']; $data=$get['data'];
					}else{ $message='Store not success.'; $status=406; $data=null; }
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// func
	private function getID($id)
	{
		$sub = \DB::table('emp_subordinate')->where('id','=',$id)->get();
		if($sub){	 foreach ($sub as $key) { $sub = $key->subordinate; $emp = $key->employee_id; } }
		else{ $sub = NULL; }

		// $sup = \DB::table('emp_supervisor')->where('supervisor','=',$emp)->get();
		// if($sup){	 foreach ($sup as $key) { $sup = $key->supervisor; } }
		// else{ $sup = NULL; }
		return ['sub'=>$sub,'emp'=>$emp,'id'=>$id];
	}

// UPDATE
	public function update($id)
	{

		$getID = $this->getID($id);
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$getID ['emp'],'Request'=>\Request::all()],$this->form,1); $model = new SubOrdinate_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'update');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
				$valid= $this->set_valid();
				if($valid->fails()){$message='Required Input.'; $status=500; $data=null;}
				else{
					$input = \Input::get('subordinate');//$this->check_input($id); $input2 = $input['data'];

					$update = $model->Update_Sub_Unit($input,$getID,$id);
					
					$message = $update['message'];
					$data = $update['data'];
					$status = $update['status'];

					//return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
			}	}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// DELETE
	public function destroy($id)
	{
		$getID = $this->getID($id);
		$datax = array();
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$getID['emp'],'Request'=>\Request::all()],$this->form,4); $model = new SubOrdinate_Model;
		$crud = $FRA->Access(\Request::all(),$this->formx,'destroy');
		if($access[1] == 200){

			if($this->check_id($id)==500){ $data=null; $status=404; $message='ID Undefined.'; }
			else{
					//$toArray = explode(",", $id);
					$data =\DB::SELECT("select id,employee_id,subordinate from emp_subordinate where id in($id)");
					foreach ($data as $key => $value) {
						//return "delete from emp_supervisor where employee_id='$employee_id' and supervisor='$supervisor'";
						//$delete = \DB::SELECT("delete from emp_supervisor where employee_id='$employee_id' and supervisor='$supervisor' ");
						\DB::table('emp_subordinate')->where('id','=',$value->id)->delete();
						\DB::table('emp_supervisor')->where('employee_id','=',$value->subordinate)->where('supervisor','=',$value->employee_id)->delete();
					}
					$message = 'Delete Successfully.'; $status=200;
					// $toArray = explode(",", $id);
					// foreach ($toArray as $key) {
					// 	$checkSup =  \DB::table('emp_subordinate')->where('id','=',$key)->get();
					// 	foreach ($checkSup as $keys) {
					// 		$Sup = \DB::table('emp_supervisor')
					// 			->where('supervisor','=',$keys->employee_id)
					// 			->where('employee_id','=',$keys->subordinate)->get();
					// 		foreach ($Sup as $keyx) {
					// 			$datax[] = $keyx->id;
					// 		}
					// 	}
					// }
					// $data_Sup = $datax;
					// $data = $model->Destroy_Sub_Unit($toArray,$data_Sup);
					// if(isset($data)){
					// 	$message = $data['message']; $status = $data['status'];
					// }else{ $message = 'ID not found'; $status = 404;}
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
// CHECK INPUT
	private function check_input($id){
		$data = array(); // tampung input to array
		$data['subordinate'] = \Input::get('subordinate');

		if(isset($data) && $data){
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }
		return ['status'=>$status,'data'=>$result];
	}
}
