<?php namespace Larasite\Http\Controllers\Employees\Salary;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\Salary\Salary_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;

class Salary_Ctrl extends Controller {

protected $form = ['expat'=>27,'local'=>36,'local_it'=>45];
protected $formx = "";
private $id_login = null;
protected $access_local = ["read"=>1,"create"=>1,"update"=>1,"delete"=>1];
public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];
	$this->id_login = $data;

	$parm 		= \Request::route()->parameters();
	if(null !== $parm){
		$obj_parm 	= array_keys($parm);
		foreach ($obj_parm as $key) {

				$obj = $parm[$key];
				$obj_number = (integer)$obj;
				if(is_integer($obj_number) && strlen($obj) > 6 && $obj_number > 0){
					$data = $parm[$key];
				}
		}
	}

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "27";
		}elseif($db_data == 2){
				return $this->formx = "45";
		}else{
			return $this->formx = "36";
		}
}
protected $not_allowed = 'Method not allowed.';

public function create(){ return \Response::json(['message'=>$this->not_allowed],500); } // NOT USES
public function edit($id){ return \Response::json(['message'=>$this->not_allowed],500); } // NOT USES

private function check_id($id) // CHECK ID
{
	$rule=['undefined',NULL,''];
	if(in_array($id,$rule)){ return 500; }else{ return 200; }
}
private function set_valid()// SET VALIDASI
{
	$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'num'=>'Regex:/^[0-9-.,\^ ]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
	$rule = ['salary'=>"required|".$reg['num'],'atm_numb'=>$reg['num'],'atm_account'=>$reg['num'],'net_gross'=>'alpha','hmo'=>$reg['num'],'hdmf_loan'=>$reg['num'],'sss_loan'=>$reg['num']];
	$valid = \Validator::make(\Input::all(),$rule); return $valid;
}


// INDEX
	public function index($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=NULL; $status=404; $message='ID Undefined.'; }
			else{
					return 1;
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);

	}
// simpan
	private function objSave($id,$input_data){
		$store = $model->Store_Salary($id,$input_data); // STORE TO DB ####
		if(isset($store['status']) && $store['status'] == 200){
			$message=$store['message']; $status=$store['status']; $data=$store['data'];
		}else{ $message=$store['message']; $status=$store['status']; $data=$store['data']; }
		return $store;
	}
//CHECK DATA
	// public function check_update($input,$id){
	// 	$db = \DB::SELECT("select * from manage_salary,deduction  where deduction.history_salary_id = $id and ,manage_salary.id=$id "){
	// 		foreach ($db as $key => $value) {
	// 			$data_deduc[$value->history_salary_id] = [

	// 			];
	// 		}
	// 	}
	// }
// STORE
	//check_valid
	public function test_valid($id){

		$all =\Input::json("all_deduction");

		 $db = $this->show_data($id);

		$salary_payment = \Input::json("salary_payment");

		if(isset($all)){

			if(isset($all['HDMFLoan']) && $all['HDMFLoan'] != null  ){
					$hde_name = "HDMF Loan";

					if(isset($all['HDMFLoan']['initial_balance']) && substr($all['HDMFLoan']['initial_balance'],-3,1) == "."){
						$hi_balance = substr($all['HDMFLoan']['initial_balance'],0,-3);
					}

					if(isset($hi_balance)){$hi_balance = str_replace('.','',str_replace(',','',$hi_balance));}
					elseif(isset($all['HDMFLoan']['initial_balance']) && $all['HDMFLoan']['initial_balance'] != null){ $hi_balance = $all['HDMFLoan']['initial_balance']; }
					else{$hi_balance = 0; };


					if(isset($all['HDMFLoan']['deduction']) && substr($all['HDMFLoan']['deduction'],-3,1) == "."){
						$hr_balance = substr($all['HDMFLoan']['deduction'],0,-3);
					}

			 		if(isset($hr_balance)){ $hr_balance = str_replace('.','',str_replace(',','',$hr_balance));}
			 		elseif(isset($all['HDMFLoan']['deduction']) && $all['HDMFLoan']['deduction'] != null ){ $hr_balance = $all['HDMFLoan']['deduction'];}
			 		else{$hr_balance = 0; }

					if(isset($db['data_sub']['HDMFLoan']->deductionName)){
							$daa = $db['data_sub']['HDMFLoan']->deductionName ;
					}else{
							$daa= 0;
					}
					if(isset($db['data_sub']['HDMFLoan']->initial_balance)){
							$da = $db['data_sub']['HDMFLoan']->initial_balance;
					}else{
							$da = 0;
					}
					if(isset($db['data_sub']['HDMFLoan']->deduction)){
							$d = $db['data_sub']['HDMFLoan']->deduction;
					}else{
							$d = 0;
					}

					if($hde_name != $daa ){ return "hdmf";}
					if($hi_balance != $da ){ return "hdmf";}
					if($hr_balance != $d ){ return "hdmf";}

			}

			if(isset($all['HMO']) && $all['HMO'] != null ){
					$dde_name = "HMO";
					if(isset($all['HMO']['initial_balance']) && substr($all['HMO']['initial_balance'],-3,1) == "."){
						$di_balance = substr($all['HMO']['initial_balance'],0,-3);
					}
					if(isset($di_balance)){ $di_balance = str_replace('.','',str_replace(',','',$di_balance)); }
					elseif(isset($all['HMO']['initial_balance']) && $all['HMO']['initial_balance'] != null){ $di_balance = $all['HMO']['initial_balance']; }
					else{ $di_balance = 0; }

					if(isset($all['HMO']['deduction']) && substr($all['HMO']['deduction'],-3,1) == "."){
						$dr_balance = substr($all['HMO']['deduction'],0,-3);
					}

					if(isset($dr_balance)){ $dr_balance = str_replace('.','',str_replace(',','',$dr_balance)); }
					elseif(isset($all['HMO']['deduction']) && $all['HMO']['deduction'] != null){ $dr_balance = $all['HMO']['deduction']; }
					else{ $dr_balance = 0; }

					if(isset($db["data_sub"]['HMO']->deductionName)){
							$daa = $db["data_sub"]['HMO']->deductionName ;
					}else{
							$daa = 0;
					}
					if(isset($db["data_sub"]['HMO']->initial_balance)){
							$da = $db["data_sub"]['HMO']->initial_balance ;
					}else{
							$da = 0;
					}

					if(isset($db["data_sub"]['HMO']->deduction)){
						$d = $db["data_sub"]['HMO']->deduction;
					}else{
						$d = 0;
					}

				if($dde_name != $daa ){ return "hmo";}
				if($di_balance != $da){ return "hmo";}
				if($dr_balance != $d){ return "hmo";}
			}

			if(isset($all['SSSLoan']) && $all['SSSLoan'] != null){
				$se_name = "SSS Loan";

				if(isset($all['SSSLoan']['initial_balance']) && substr($all['SSSLoan']['initial_balance'],-3,1) == "."){
					$si_balance = substr($all['SSSLoan']['initial_balance'],0,-3);
				}

				if(isset($si_balance)){ $si_balance = str_replace('.','',str_replace(',','',$si_balance)); }
				elseif(isset($all['SSSLoan']['initial_balance']) && $all['SSSLoan']['initial_balance'] != null){ $si_balance = $all['SSSLoan']['initial_balance']; }
				else{ $si_balance = 0; }

				if(isset($all['SSSLoan']['deduction']) && substr($all['SSSLoan']['deduction'],-3,1) == "."){
					$sr_balance = substr($all['SSSLoan']['deduction'],0,-3);
				}

				if(isset($sr_balance)){ $sr_balance = str_replace('.','',str_replace(',','',$sr_balance)); }
				elseif(isset($all['SSSLoan']['deduction']) && $all['SSSLoan']['deduction'] != null){ $sr_balance = $all['SSSLoan']['deduction']; }
				else{ $sr_balance = 0; }

				if(!isset($db["data_sub"]['SSSLoan']->deductionName)){
					$daa = 0;
				}else{
					$daa = $db["data_sub"]['SSSLoan']->deductionName;
				}
				if(!isset($db["data_sub"]['SSSLoan']->initial_balance)){
					$da = 0;
				}else{
					$da = $db["data_sub"]['SSSLoan']->initial_balance;
				}
				if(!isset($db["data_sub"]['SSSLoan']->deduction)){
					$d = 0;
				}else{
					$d = $db["data_sub"]['SSSLoan']->deduction;
				}

				if($se_name != $daa){ return "sss";}
				if($si_balance != $da){ return "sss";}
				if($sr_balance != $d){ return "sss";}
			}

		}

		if(isset($salary_payment)){

				if(isset($salary_payment["MonthSalary"]) && $salary_payment["MonthSalary"] != null ){
					if(substr($salary_payment["MonthSalary"],-3,1) == "."){
					 	$MonthSalary = substr($salary_payment["MonthSalary"],0,-3);
					}
				}

				if(isset($MonthSalary)){ $MonthSalary = str_replace('.','',str_replace(',','',$MonthSalary)); }
				elseif(isset($salary_payment["MonthSalary"]) && $salary_payment["MonthSalary"] != null){ $MonthSalary = $salary_payment["MonthSalary"]; }
				else{ $MonthSalary = 0; }

		 		 $netGross = $salary_payment["netGross"];
				 $effectivedate = $salary_payment["effectiveDate"];
	  			 $ATMNcard = $salary_payment['ATMNcard'];
				 $ATMaccount = $salary_payment['ATMacount'];

				 if(!isset($db['data_master']->MonthSalary)){
					 $daaaa = 0;
				 }else{ $daaaa = $db['data_master']->MonthSalary;}
				 if(!isset($db['data_master']->netGross)){
					 $daaa = null;
				 }else{ $daaa = $db['data_master']->netGross; }
				 if(!isset($db['data_master']->effectiveDate)){
					 $daa = null;
				 }else{
					 $daa = $db['data_master']->effectiveDate;
				 }
				 if(!isset($db['data_master']->ATMNcard)){
					 $da = 0;
				 }else{
					 $da = $db['data_master']->ATMNcard;
				 }
				 if(!isset($db['data_master']->ATMacount)){
					 $d = null;
				 }else{$d = $db['data_master']->ATMacount;}


				 if($MonthSalary != $daaaa  ){ return "execute";}
				if($netGross != $daaa ){ return "execute";}
				if($effectivedate != $daa ){ return "execute";}
				if($ATMNcard != $da ){ return "execute";}
				if($ATMaccount != $d ){ return "execute";}


			}


		return "match";
	}


	public function getMessage($access){
		return response()->json(["header" => ["message" => "invalidate input format", "status" => 500 , "access" => $access],"data" => [] ],500);
	}

	// public function match($string){
	// 	// $preg = preg_match(/^\$?(?=\(.*\)|[^()]*$)\(?(\d{1,6}|\d{1,3},\d{3})(\.\d\d?)?\)?$/,$string,$match);
	// 	// if($match == false){
	// 	// 	return "failed";
	// 	// }
	// }

	public function store($id)
	{
		$model = new Salary_Model;
		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$id,'Request'=>\Request::all()],$this->form,2); $model = new Salary_Model;
		if($access[1] == 200){
			if($this->check_id($id)==500){ $data=NULL; $status=404; $message='ID Undefined.'; }
			else{
					//return $data = $this->show_data($id);

					$all =\Input::json("all_deduction");
					$salary_payment = \Input::json("salary_payment");

					if(isset($all)){
						if(isset($all['HDMFLoan'])){
							(isset($all['HDMFLoan']['deductionId']) ? $hdmf_id = $all['HDMFLoan']['deductionId'] : $hdmf_id = NULL );
							 $hde_name = "HDMF Loan";
							 if(isset($all['HDMFLoan']['initial_balance']) && substr($all['HDMFLoan']['initial_balance'],-3,1) == "."){
 								$hi_balance = substr($all['HDMFLoan']['initial_balance'],0,-3);
 							}

 							if(isset($hi_balance)){$hi_balance = str_replace('.','',str_replace(',','',$hi_balance));}
 							elseif(isset($all['HDMFLoan']['initial_balance']) && $all['HDMFLoan']['initial_balance'] != null){ $hi_balance = $all['HDMFLoan']['initial_balance']; }
 							else{$hi_balance = 0; };


 							if(isset($all['HDMFLoan']['deduction']) && substr($all['HDMFLoan']['deduction'],-3,1) == "."){
 								$hr_balance = substr($all['HDMFLoan']['deduction'],0,-3);
 							}

 					 		if(isset($hr_balance)){ $hr_balance = str_replace('.','',str_replace(',','',$hr_balance));}
 					 		elseif(isset($all['HDMFLoan']['deduction']) && $all['HDMFLoan']['deduction'] != null ){ $hr_balance = $all['HDMFLoan']['deduction'];}
 					 		else{$hr_balance = 0; }
						}

						if(isset($all['HMO'])){
							(isset($all['HMO']['deductionId']) ? $hmo_id = $all['HMO']['deductionId'] : $hmo_id = NULL );
							$dde_name = "HMO";
							if(isset($all['HMO']['initial_balance']) && substr($all['HMO']['initial_balance'],-3,1) == "."){
								$di_balance = substr($all['HMO']['initial_balance'],0,-3);
							}
							if(isset($di_balance)){ $di_balance = str_replace('.','',str_replace(',','',$di_balance)); }
							elseif(isset($all['HMO']['initial_balance']) && $all['HMO']['initial_balance'] != null){ $di_balance = $all['HMO']['initial_balance']; }
							else{ $di_balance = 0; }

							if(isset($all['HMO']['deduction']) && substr($all['HMO']['deduction'],-3,1) == "."){
								$dr_balance = substr($all['HMO']['deduction'],0,-3);
							}

							if(isset($dr_balance)){ $dr_balance = str_replace('.','',str_replace(',','',$dr_balance)); }
							elseif(isset($all['HMO']['deduction']) && $all['HMO']['deduction'] != null){ $dr_balance = $all['HMO']['deduction']; }
							else{ $dr_balance = 0; }
						}

						if(isset($all['SSSLoan'])){

							(isset($all['SSSLoan']['deductionId']) ? $sss_id = $all['SSSLoan']['deductionId'] : $sss_id = NULL );
							$se_name = "SSS Loan";
							if(isset($all['SSSLoan']['initial_balance']) && substr($all['SSSLoan']['initial_balance'],-3,1) == "."){
								$si_balance = substr($all['SSSLoan']['initial_balance'],0,-3);
							}

							if(isset($si_balance)){ $si_balance = str_replace('.','',str_replace(',','',$si_balance)); }
							elseif(isset($all['SSSLoan']['initial_balance']) && $all['SSSLoan']['initial_balance'] != null){ $si_balance = $all['SSSLoan']['initial_balance']; }
							else{ $si_balance = 0; }

							if(isset($all['SSSLoan']['deduction']) && substr($all['SSSLoan']['deduction'],-3,1) == "."){
								$sr_balance = substr($all['SSSLoan']['deduction'],0,-3);
							}

							if(isset($sr_balance)){ $sr_balance = str_replace('.','',str_replace(',','',$sr_balance)); }
							elseif(isset($all['SSSLoan']['deduction']) && $all['SSSLoan']['deduction'] != null){ $sr_balance = $all['SSSLoan']['deduction']; }
							else{ $sr_balance = 0; }
						}
					}

					if(isset($salary_payment)){

						if(isset($salary_payment)){
							if(isset($salary_payment["MonthSalary"]) && $salary_payment["MonthSalary"] != null ){
								if(substr($salary_payment["MonthSalary"],-3,1) == "."){
								 	$MonthSalary = substr($salary_payment["MonthSalary"],0,-3);
								}
							}

							if(isset($MonthSalary)){ $MonthSalary = str_replace('.','',str_replace(',','',$MonthSalary)); }
							elseif(isset($salary_payment["MonthSalary"]) && $salary_payment["MonthSalary"] != null){ $MonthSalary = $salary_payment["MonthSalary"]; }
							else{ $MonthSalary = 0; }

					 		 (isset($salary_payment["netGross"]) ? $netGross = $salary_payment["netGross"] : $netGross = NULL );
							 (isset($salary_payment["effectiveDate"]) ? $effectivedate = $salary_payment["effectiveDate"] : $effectivedate = NULL);
				  			 (isset($salary_payment['ATMNcard']) ? $ATMNcard = $salary_payment['ATMNcard'] : $ATMNcard = NULL);
							 (isset($salary_payment['ATMacount']) ? $ATMaccount = $salary_payment['ATMacount'] : $ATMaccount =NULL );
						}
					}

					 $db = \DB::SELECT("select * from manage_salary where employee_id = '$id' ");
					 if($db == NULL){
					 	if(isset($salary_payment)){
							// return $data = [$MonthSalary,$netGross,$ATMNcard,$ATMaccount];
					 	$db = \DB::SELECT(" insert into manage_salary(dates,employee_id,salary,net_gross,effective_date,atm_account,atm_numb) values(now(),'$id','$MonthSalary','$netGross',now(),'$ATMNcard','$ATMaccount');");
					 	$dbx = \DB::SELECT(" select id from manage_salary order by id DESC limit 1;");
					 	$id_deduc = $dbx[0]->id;
					 		if($id_deduc != NULL){
					 			if(isset($all['HDMFLoan'])){
					 				$total_paid = $hi_balance;
					 				$r_balance = $total_paid-$hr_balance;
					 				$deutch = \DB::SELECT(" insert into deduction(history_salary_id,employee_id,deduction_name,initial_balance,deduction,total_paid,remaining_balance) values($id_deduc,'$id','$hde_name',$hi_balance,$hr_balance,$total_paid,$r_balance)");
					 			}

					 			if(isset($all['HMO'])){
					 				$total_paid = $di_balance;
					 				$r_balance = $total_paid-$dr_balance;
					 				$deutch = \DB::SELECT(" insert into deduction(history_salary_id,employee_id,deduction_name,initial_balance,deduction,total_paid,remaining_balance) values($id_deduc,'$id','$dde_name',$di_balance,$dr_balance,$total_paid,$r_balance)");
					 			}
					 			if(isset($all['SSSLoan'])){
					 				$total_paid = $si_balance;
					 				$r_balance = $total_paid-$sr_balance;
					 				$deutch = \DB::SELECT(" insert into deduction(history_salary_id,employee_id,deduction_name,initial_balance,deduction,total_paid,remaining_balance) values($id_deduc,'$id','$se_name',$si_balance,$sr_balance,$total_paid,$r_balance)");
					 			}
					 		}
					 	}

					 	$message = "Success to insert salary payment detail and all deduction";
					 	$status = 200;
					 	$data = $this->show_data($id);
					 }else{
				 	 $data_c = $this->test_valid($id);

					 	if($data_c == "match"){
					 	$message = "failed to update data, There is no data have been change in payment detail and all deduction";
					 	$status = 500;
					 	$data = $this->show_data($id);
					 	}else{

							\DB::beginTransaction();
								try{

								$delete = \DB::SELECT("delete from manage_salary where employee_id=$id	");
								$db = \DB::SELECT(" insert into manage_salary(dates,employee_id,salary,net_gross,effective_date,atm_account,atm_numb) values(now(),'$id','$MonthSalary','$netGross',now(),'$ATMaccount','$ATMNcard')");
									$dbx = \DB::SELECT(" select id from manage_salary order by id DESC limit 1");

									$id_deduc = $dbx[0]->id;

										if($id_deduc != NULL){

											if(isset($all['HDMFLoan'])){

												$total_paid = $hi_balance;
												$r_balance = $total_paid-$hr_balance;

												//return $data=[$id_deduc,$id,$hde_name,$hi_balance,$hr_balance,$total_paid,$r_balance];
												$deutch = \DB::SELECT(" insert into deduction(history_salary_id,employee_id,deduction_name,initial_balance,deduction,total_paid,remaining_balance) values($id_deduc,'$id','$hde_name',$hi_balance,$hr_balance,$total_paid,$r_balance)");

											}

											if(isset($all['HMO'])){
												$total_paid = $di_balance;
												$r_balance = $total_paid-$dr_balance;

												$deutch = \DB::SELECT(" insert into deduction(history_salary_id,employee_id,deduction_name,initial_balance,deduction,total_paid,remaining_balance) values($id_deduc,'$id','$dde_name',$di_balance,$dr_balance,$total_paid,$r_balance)");
											}


											if(isset($all['SSSLoan'])){
												$total_paid = $si_balance;
												$r_balance = $total_paid-$sr_balance;

												$deutch = \DB::SELECT(" insert into deduction(history_salary_id,employee_id,deduction_name,initial_balance,deduction,total_paid,remaining_balance) values($id_deduc,'$id','$se_name',$si_balance,$sr_balance,$total_paid,$r_balance)");
											}

											$deutchs = \DB::SELECT("CALL insert_log_salary('$id',$id_deduc)");

									}


						 \DB::commit();
										$message = "Success update data payment";
										$status = 200;
										$data = $this->show_data($id);
									}catch(\Exception $e){
								\DB::rollBack();
								$message = "Failed update data payment";
								$status = 500;
								$data = $this->show_data($id);
							 	}
					 	}

					}
				}





		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// Show Data
	private function show_data_salary($id){
		return 1;
	}
// SHOW
	public function show($id) //FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->formx,'read');
		try{
			if($this->id_login == $id){
				$access[3] = $this->access_local;
			}
			if($access[3]['read'] == 1){

	            if($this->check_id($id)==500){ $data=NULL; $status=404; $message='ID Undefined.'; }
				else{

		    	    $data = $this->show_data($id);
				    if($data == "undefined"){
		        			$message = "data not exist";
	          				$data =  [ "salary_payment" => NULL, "all_deduction" => NULL, "salary_history" => []];
	     						$status = 200;
		      		}else{
	          			$message = "success";
	          			$data =  [ "salary_payment" => $data['data_master'], "all_deduction" => $data['data_sub'], "salary_history" => $data['salary_history']];
	     					$status = 200;
	   					}
				}

	           // }
			}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }

		} catch (\Exception $e) {
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data, ],$status);
	}
// UPDATE
	public function update($id)
	{
		$toArray = explode(",", $id); // EXPLOD COMMA
		if(!is_numeric($id)){
			$message = 500; $status = "failed"; $data="data not exiest";			
		}else{
			$localit = null;
			$dt = null;
			foreach ($toArray as $key => $value) {
				if(!isset($dt)){
					$dt = \DB::select("select distinct emp.employee_id , emp.local_it from log_salary log , emp where log.id = $value and emp.employee_id = log.employee_id");
				}
			}
			if(isset($dt)){
				foreach ($dt as $key => $value) {
					$emp_req = $value->employee_id;
				}
			}
		}

		if(!isset($emp_req)){
			$message = 500; $status = "failed"; $data="data not exiest";
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);			
		}

		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$emp_req,'Request'=>\Request::all()],$this->form,3); $model = new Salary_Model;
		///*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>NULL,'Request'=>\Request::all()],$this->form,3); $model = new Salary_Model;
		try{
			if($this->id_login == $id){
				$access[3] = $this->access_local;
			}
			if($access[3]['update'] == 1){
				if($this->check_id($id)==500){ $data=NULL; $status=404; $message='ID Undefined.'; }
				else{
						if(!is_numeric($id)){
							$message = 500; $status = "failed"; $data="data not exiest";			
							return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
						}
						$input = $this->set_input();
						if(isset($input['status']) && $input['status'] == 200){
							$update = $model->Update_Salary($id,$input['data']); // STORE TO DB ####
							if(isset($update['status']) && $update['status'] == 200){
								$message=$update['message']; $status=$update['status'];
							}else{ $message=$update['message']; $status=$update['status']; }
						}else{ $message='Input not NULL'; $data=NULL; $status=500; }
					}
			}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		} catch (\Exception $e) {
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}	
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// DESTROY
	public function destroy($id)
	{

		$toArray = explode(",", $id); // EXPLOD COMMA
		if(!is_numeric($id)){
			$message = 500; $status = "failed"; $data="data not exiest";			
		}else{
			$localit = null;
			$dt = null;
			foreach ($toArray as $key => $value) {
				if(!isset($dt)){
					$dt = \DB::select("select distinct emp.employee_id , emp.local_it from log_salary log , emp where log.id = $value and emp.employee_id = log.employee_id");
				}
			}
			if(isset($dt)){
				foreach ($dt as $key => $value) {
					$emp_req = $value->employee_id;
				}
			}
		}

		if(!isset($emp_req)){
			$message = 500; $status = "failed"; $data="data not exiest";
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);			
		}

		/*Access*/$FRA = new FuncAccess; $access = $FRA->AccessPersonal(['Personal'=>$emp_req,'Request'=>\Request::all()],$this->form,4); $model = new Salary_Model;
		
		try {		
			if($this->id_login == $id){
				$access[3] = $this->access_local;
			}
			if($access[3]['delete'] == 1){
				if($this->check_id($id)==500){ $data=NULL; $status=404; $message='ID Undefined.'; }
				else{
					$toArray = explode(",", $id); // EXPLOD COMMA
					if(!is_numeric($id)){
						$message = 500; $status = "failed"; $data="data not exiest";			
					}
					foreach ($toArray as $key => $value) {
							\DB::SELECT("CALL delete_history_salary($value)");
					}
					$message = "success to delete salary history ";
					$status = 200;
					$data =[];
				}
			}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		} catch (\Exception $e) {
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
/* SAVE LOG */
	public function SaveLog($id){
		/* get */
		$select = \DB::select("SELECT id,employee_id,salary,atm_numb,atm_account,net_gross,hmo,hdmf_loan,sss_loan from manage_salary where employee_id = $id");
		if($select){
			foreach ($select as $key) {
				$employee_id = $key->employee_id;
				$salary = $key->salary;
				$atm_numb = $key->atm_numb;
				$atm_account = $key->atm_account;
				$net_gross = $key->net_gross;
				$hmo = $key->hmo;
				$hmdf_loan = $key->hdmf_loan;
				$sss_loan = $key->sss_loan;
				$id_salary = $key->id;
			}
			$store = \DB::table('log_salary')->insert(['employee_id'=>$employee_id,'salary'=>$salary,'atm_numb'=>$atm_numb,'atm_account'=>$atm_account,'net_gross'=>$net_gross,'hmo'=>$hmo,'hdmf_loan'=>$hmdf_loan,'sss_loan'=>$sss_loan]);
			if($store){
				$status = 200; $data_log = ['employee_id'=>$id,'salary'=>$salary,'atm_numb'=>$atm_numb,'atm_account'=>$atm_account,'net_gross'=>$net_gross,'hmo'=>$hmo,'hdmf_loan'=>$hmdf_loan,'sss_loan'=>$sss_loan];
			}else{ $status = 200; }
		}else{ $status=500; $id_salary=NULL; $data_log = NULL;}
		return ['status'=>$status,'id'=>$id_salary];
	}
	private function Rm($array)
	{
		$tmp = NULL;
		if(strpos($array,',')){
			$rm2 = explode(',',substr($array,0,strpos($array,'.')));
			for($i = 0; $i < count($rm2); $i++){ $tmp .= $rm2[$i]; }
			return $tmp;
		}else{
			return $array;
		}


	}
// SET INPUT
	private function set_input(){
		$data = array(); // tampung input to array
		$data['salary']  			= $this->Rm(\Input::get('salary'));
		$data['atm_numb']			= \Input::get('atm_numb');
		$data['atm_account']		= \Input::get('atm_account');
		$data['net_gross']			= \Input::get('net_gross');
		$data['hmo']				= $this->Rm(\Input::get('hmo'));
		$data['hdmf_loan']			= $this->Rm(\Input::get('hdmf_loan'));
		$data['sss_loan']			= $this->Rm(\Input::get('sss_loan'));
		return $data;
	}

	public function show_data($id){
		    $master = \DB::SELECT("select id as salaryId,effective_date as effectiveDate,salary as MonthSalary, net_gross as netGross, atm_account as ATMacount, atm_numb as ATMNcard from manage_salary where employee_id='$id' ");
		    $HMO = \DB::SELECT("select total_paid,initial_balance,deduction,id as deductionId, deduction_name as deductionName, remaining_balance as remain_balance from deduction where employee_id='$id' and deduction_name = 'HMO' order by id DESC limit 1 ");
		    $HDMF = \DB::SELECT("select total_paid,initial_balance,deduction,id as deductionId, deduction_name as deductionName, remaining_balance as remain_balance from deduction where employee_id='$id' and deduction_name = 'HDMF Loan' order by id DESC limit 1 ");
		    $SSS = \DB::SELECT("select total_paid,initial_balance,deduction,id as deductionId, deduction_name as deductionName, remaining_balance as remain_balance from deduction where employee_id='$id' and deduction_name = 'SSS Loan' order by id DESC limit 1 ");
		    $data_salary = \Db::SELECT(" select salary as MonthSalary,id as deductionId, changedate, effective_date as effectiveDate, net_gross as netGross, hdmf_loan as MonthSalary_HDMFLoan,hmo as MonthSalary_HMO, sss_loan as MonthSalary_SSSLoan from log_salary where employee_id='$id'  ");

		   ($master == NULL ? $data_master = NULL : $data_master = $master[0]);
		   ($HMO == NULL ? $hmo = NULL: $hmo = $HMO[0]);
		   ($HDMF == NULL ? $hdmf = NULL: $hdmf = $HDMF[0]);
		   ($SSS == NULL ? $sss = NULL: $sss = $SSS[0]);
		   ($data_salary == NULL ? $data_salary = NULL : $data_salary = $data_salary);

		   $data_sub = ["HMO" => $hmo, "HDMFLoan" => $hdmf, "SSSLoan" => $sss];
 	  return $data_fin = ["data_master" => $data_master, "data_sub" => $data_sub, "salary_history" => $data_salary];
 	}


}
