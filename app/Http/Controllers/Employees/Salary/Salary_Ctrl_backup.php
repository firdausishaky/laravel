<?php namespace Larasite\Http\Controllers\Employees\Salary;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\Salary\Salary_Model;

class Salary_Ctrl extends Controller {

protected $form_local =36 ;
protected $form_expat =27 ;


protected $not_allowed = 'Method not allowed.';
// INDEX ####
	public function index($id)
	{
		$model = new Salary_Model;
		$get_role = $this->check();
		if(isset($get_role['data']['role']) && $get_role != null){
			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1);
			if(isset($access['status']) && $access['status'] == 200){
				if(isset($id) && $id != 'undefined'){ // for ID UNDEFINED

					$show = $model->Read_Salary($id,2); // STORE TO DB ####
					if(isset($show['status']) && $show['status'] == 200){
						$message=$show['message']; $status=$show['status']; $data=$show['data'];
					}else{ $message=$show['message']; $status=200; $data=$show['data']; }

				}else{ $data=null; $status=404; $message='ID Undefined.'; }
			}else{ $message = $access['message']; $status = $access['status']; $data=null;}
		}else{ $message=$get_role['message']; $data=null; $status=403; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// END INDEX #######

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return \Response::json(['message'=>$this->not_allowed],500);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	private function objSave($id,$input_data){
		$store = $model->Store_Salary($id,$input_data); // STORE TO DB ####
		if(isset($store['status']) && $store['status'] == 200){
			$message=$store['message']; $status=$store['status']; $data=$store['data'];
		}else{ $message=$store['message']; $status=$store['status']; $data=$store['data']; }
		return $store;
	}
	public function store($id)
	{
		$model = new Salary_Model;
		$get_role = $this->check();
		if(isset($get_role['data']['role']) && $get_role != null){
			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1);
			if(isset($access['status']) && $access['status'] == 200){
				if(isset($id) && $id != 'undefined'){ // for ID UNDEFINED

					$input = $this->set_input();
					if(isset($input['status']) && $input['status'] ==200){
						
						//return $this->SaveLog($id);
						$save_log = $this->SaveLog($id);
						if($save_log['status'] == 200){
								$store = $model->Store2_Salary($save_log['id'],$input['data']); // STORE TO DB ####
								if(isset($store['status']) && $store['status'] == 200){
									$message=$store['message']; $status=$store['status']; $data= $model->ambil_data_terbaru($id);
								}else{ $message=$store['message']; $status=$store['status']; $data=$store['data']; }
						}else{  
							$store = $model->Store_Salary($id,$input['data']); // STORE TO DB ####
								if(isset($store['status']) && $store['status'] == 200){
									$message=$store['message']; $status=$store['status']; $data=$store['data'];
								}else{ $message=$store['message']; $status=$store['status']; $data=$store['data']; }
						}

					}else{ $message='Input not null'; $data=null; $status=500; }

				}else{ $data=null; $status=404; $message='ID Undefined.'; }
			}else{ $message = $access['message']; $status = $access['status']; $data=null;}
		}else{ $message=$get_role['message']; $data=null; $status=403; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

// ########################################################################################################
	/**
	 * Display the specified resource.
	 * SHOW METHOD ###
	 * @param  int  $id
	 * @return Response
	 */
// KONSEP DI PERBAHARUI UNTUK IDENTIFIKASI JIKA USER / BUKAN	
	private function show_data_salary($id){
		$model = new Salary_Model;
		$show = $model->Read_Salary($id,1); // STORE TO DB ####
		if(isset($show['status']) && $show['status'] == 200){
			$message=$show['message']; $status=$show['status']; $data=$show['data'];
		}else{ $message=$show['message']; $status=$show['status']; $data=$show['data']; }

		return ['data'=>$data,'status'=>$status,'message'=>$message];
	}

	public function show($id) //FIX**
	{
		$get_role = $this->check();
		if(isset($get_role['data']['role']) && $get_role != null){
			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1);
			if(isset($access['status']) && $access['status'] == 200){
				if(isset($id) && $id != 'undefined'){ // for ID UNDEFINED

					if($get_role['data']['role'] == 5 && $access['message']['employee_id'] == $get_role['data']['employee_id'] ){
						
						$datas = $this->show_data_salary($access['message']['employee_id']);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}else{

						$datas = $this->show_data_salary($id);
						$data = $datas['data']; $status = $datas['status']; $message = $datas['message'];
					}

				}else{ $data=null; $status=404; $message='ID Undefined.'; }
			}else{ $message = $access['message']; $status = $access['status']; $data=null;}
		}else{ $message=$get_role['message']; $data=null; $status=403; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// ##########################################################################################################

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return \Response::json(['message'=>$this->not_allowed],500);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$model = new Salary_Model;
		$get_role = $this->check();
		if(isset($get_role['data']['role']) && $get_role != null){
			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1);
			if(isset($access['status']) && $access['status'] == 200){
				if(isset($id) && $id != 'undefined'){ // for ID UNDEFINED

					$input = $this->set_input();
					if(isset($input['status']) && $input['status'] == 200){
						$update = $model->Update_Salary($id,$input['data']); // STORE TO DB ####
						if(isset($update['status']) && $update['status'] == 200){
							$message=$update['message']; $status=$update['status']; 
						}else{ $message=$update['message']; $status=$update['status']; }		
					}else{ $message='Input not null'; $data=null; $status=500; }
				
				}else{ $message='ID Undefined.'; $data=null; $status=500; }
			}else{ $message='Unauthorized'; $data=null; $status=401; }
		}else{ $message=$get_role['message']; $data=null; $status=403; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
	}



// DESTROY #############################################################################################
// FIXXX ***
	public function destroy($id)
	{
		$model = new Salary_Model;
		$get_role = $this->check();
		if(isset($get_role['data']['role'])){
			$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],3);
			if(isset($access['status']) && $access['status'] == 200){
				if(isset($id) && $id != 'undefined'){ // for ID UNDEFINED
						
						$toArray = explode(",", $id); // EXPLOD COMMA
						$data = $model->Destroy_Salary($toArray);
						if(isset($data) && $data['status'] == 200){
							$message = $data['message'];$status = $data['status'];	
						}else{ $message = 'ID not found'; $status = 404; }
					
				}else{ $message='ID Undefined.'; $data=null; $status=500; }
			}else{ $message = $access['message']; $status = $access['status'];}
		}else{ $message=$get_role['message']; $status=403;}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
	}
// END DESTROY #######################################################################################


/* SAVE LOG */
	public function SaveLog($id){
		/* get */
		$select = \DB::select("SELECT id,employee_id,salary,atm_numb,atm_account,net_gross,hmo,hdmf_loan,sss_loan from manage_salary where employee_id = $id");
		if($select){
			foreach ($select as $key) {
				$employee_id = $key->employee_id;
				$salary = $key->salary;
				$atm_numb = $key->atm_numb;
				$atm_account = $key->atm_account;
				$net_gross = $key->net_gross;
				$hmo = $key->hmo;
				$hmdf_loan = $key->hdmf_loan;
				$sss_loan = $key->sss_loan;
				$id_salary = $key->id;
			}
			$store = \DB::table('log_salary')->insert(['employee_id'=>$employee_id,'salary'=>$salary,'atm_numb'=>$atm_numb,'atm_account'=>$atm_account,'net_gross'=>$net_gross,'hmo'=>$hmo,'hdmf_loan'=>$hmdf_loan,'sss_loan'=>$sss_loan]);
			if($store){
				$status = 200; $data_log = ['employee_id'=>$id,'salary'=>$salary,'atm_numb'=>$atm_numb,'atm_account'=>$atm_account,'net_gross'=>$net_gross,'hmo'=>$hmo,'hdmf_loan'=>$hmdf_loan,'sss_loan'=>$sss_loan];
			}else{ $status = 200; }
		}else{ $status=500; $id_salary=null; $data_log = null;}
		return ['status'=>$status,'id'=>$id_salary];
	}
/* END SAVE LOG */


// SET INPUT ########################################################################################
	private function set_input(){
		$data = array(); // tampung input to array
		$data['salary'] 			= \Input::get('salary');
		$data['atm_numb']			= \Input::get('atm_numb');
		$data['atm_account']		= \Input::get('atm_account');
		//$data['dates']		= \Input::get('dates');
		$data['net_gross']			= \Input::get('net_gross');
		$data['hmo']				= \Input::get('hmo');
		$data['hdmf_loan']			= \Input::get('hdmf_loan');
		$data['sss_loan']			= \Input::get('sss_loan');
		
		if(isset($data['salary'],$data['atm_numb'],$data['atm_account']) && $data['salary'] != '[]'){
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }	
		return ['status'=>$status,'data'=>$result];
	}
// CHECK INPUT VALUE #################################################################################




	/* CHECK LOCAL IT ###################################################################################
	* PARAM = ROLE
	*/
	private function check_local($role,$employee_id){
		if(isset($role)){
			$model = new Salary_Model;
			$get = $model->Get_Type($role,$employee_id);
			foreach ($get as $key) {
				$type['local_it'] = $key->local_it;
				$type['employee_id'] = $key->employee_id;
			}
		}else{ $type = null; }
		return $type;
	} // END CHECK LOCAL IT ############################################################################

	/* CHECK TYPE AND ACCESS ########################################################################
	* CHECK TYPE ID EMPLOYEE EXPAT OR LOCAl OR LOCAL_IT
	* ACTION = 1(CREATE & READ), 2(EDIT & UPDATE), 3(DESTROY)
	*/
	private function check_type2($employee_id,$role,$action) // CHECK ROLE FIX***
	{
		$model = new Salary_Model;
		$status_error = 403;
		$not_access = 'Unauthorized';
		$type['data'] = $this->check_local($role,$employee_id);
		$type['local_it'] = $type['data']['local_it'];
		if(isset($type) && $type['local_it']){

			/* ########
			* FOR EXPAT
			*/
			if($type['local_it'] == 1){
				$check = $this->check_role($role,$this->form_expat);
				
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error;}	// ERROR ACCESS
			} // if type local, expat, locit

			/* ########
			* FOR LOCAL
			*/
			elseif($type['local_it'] == 2){
				
				$check = $this->check_role($role,$this->form_local);
				
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
			}

			/* ###########
			* FOR LOCAL_IT
			*/
			elseif($type['local_it'] == 3){
				$check = $this->check_role($role,$this->form_local);
				
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
			}else{
				$message = $not_access; $status = $status_error;
			}
		} 
		else{
			$message = $not_access; $status = $status_error; // ROLE NOT FOUND
		}
		return ['message'=>$message,'status'=>$status];
	} // CHECK TYPE AND ACCESS ####################################################################


	/* CHECK ROLE ###################################################################################
	* PARAM = ROLE AND FORM
	* CHECK USER ROLE YANG ACCESS SYSTEM
	* MENGHASILKAN DATA ACCESS DAN ID
	*/
	private function check_role($role,$form) // CHECK ROLE FIX***
	{
		$getModel = new Privilege;
		if(isset($role, $form)){
			return $getModel->check_role_personal($role,$form);
		}
		else{
			$msg['warning'] = 'Unauthorize';
			$msg['status'] = 404;
			return $msg; 
		}
	}// END CHECK ROLE #####################################################################

// CHECK ACCESS ###############################################################################
	public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(isset($q) != null){ //check isset key in session
				 
						 $r = array();
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } // end for store role to array.

						 if(isset($r['key'])){
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							//return $employee_id;
							 $q2 = $getModel->session_role($employee_id);
							 foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 } // end for store role to array.
						 }
						 else{
						 	return ['message'=>'ACCESS DENIED','data'=>null];
						 }
						

				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
						$msg = 'ACCESS GRANTED !';	
					}else{
						$msg = 'ACCESS DENIED!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED!';
					$data = null;	
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;	
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		return ['message'=>$msg,'data'=>$data];
	}
// END CHECK ACCESS ############################################################################
}
