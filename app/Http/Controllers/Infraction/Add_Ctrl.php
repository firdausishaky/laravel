<?php namespace Larasite\Http\Controllers\Infraction;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Leave\leave_table\leaveTable_Model;
use Illuminate\Http\Request;
use Larasite\Library\FuncAccess;

class Add_Ctrl extends Controller {
	 protected $form = "76";

	//  public function __construct(){
	// 	 $inf = \Input::get('infraction');
	// 	 if($inf == "add"){
	// 		 	return $this->$form = "76";
	// 	 }else{
	// 		 return  $this->$form = "77";
	// 	 }
	//  }

	public function index()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		// return $access[1];
		if($access[1] == 200){
			if($access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0 && $access[3]['create'] == 0){
				return \Response::json(['header'=>['message'=>"Unauthorized",'status'=>500, "access" => $access[3]],'data'=>[]],500);
			}
		//created  18 January 2015

		$key = \Input::get("key");
		$encode = base64_decode($key);
		$explode = explode("-",$encode);
		$explode_id = $explode[1];

		//check departemnt by status
		// $hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$explode_id' AND t2.role_name = 'hr'");
		// $adminex = \DB::SELECT("select role.role_name from ldap,role where ldap.employee_id=$explode_id and ldap.role_id=role.role_id ");
		// $adminex = $adminex[0]->role_name;
		// $supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$explode_id' ");
		// if($hr != null){
		 	$department = \DB::SELECT("select name,id as department_id from department where id > 1 ");
		// }elseif($adminex == "SuperUser"){
		// 	$department = \DB::SELECT("select name,id as department_id from department where id<>1 ");
		// }elseif($supervisor != null){
		// 	$department = \DB::SELECT("select name,id as department_id from department where id<>1 ");
		// }elseif($adminex[0]->role_name == "admin"){
		// 	$department = \DB::SELECT("select department.id as department_id ,department.name from emp,department where emp.employee_id='$explode_id' and emp.department=department.id ");
		// }elseif($adminex[0]->role_name == "user"){
		// 	$department = \DB::SELECT("select department.id as department_id ,department.name from emp,department where emp.employee_id='$explode_id' and emp.department=department.id ");
		// }elseif($adminex[0]->role_name == "adminIt"){
		// 	$department = \DB::SELECT("select department.id as department_id ,department.name from emp,department where emp.employee_id='$explode_id' and emp.department=department.id ");
		// }else{
		// 	$department = null;
		// }
		$from = date('Y-m-d', strtotime(date('Y-m-d')."-1 month"));
		$to = date('Y-m-d',strtotime($from."+1 year"));
		//viewjob title
		$job_title = \DB::select("select id as job_id, title from job");
		if($job_title == null){
				$job_tile = NULL;
		}
		//infraction List

		$infraction = \DB::SELECT("select id as sanction_id, sancation, shortness_sanction from sanction_list");

		//check status
		$supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$explode_id' ");
		$hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$explode_id' AND t2.role_name like '%hr%'");
		if($supervisor != null){
			$supervisor = "supervisor";
		}elseif($hr != null){
			$supervisor = "hr";
		}else{
			$supervisor =  "user";
		}
		if($department != null){
			$department = array_merge($department,[["name" => "All", "department_id" => 0 ]]);
		}else{
			$department = null;
		}

		if($job_title != null){
			$job_title = array_merge($job_title,[["title" => "All", "job_id" => 0]]);
		}else{
			$job_title = null;
		}

		$emp_data = \DB::SELECT("CALL search_employee_by_id('$explode_id')");

			$data_exp =
			 [
			 "employee_name" => $emp_data[0]->name,
			 "employee_id" => $explode_id,
			"status" => $supervisor,
			"department" =>$department,
			"jobtitle" =>$job_title,
			"infraction" => $infraction,
			"date" => ["from" => $from, "to" => $to]
			];

			$message = "Success";
			$data = $data_exp;
			$status = 200;

		}else{
		   			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		  	return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
		}

	public function valid_insert($i){
		//if(!isset)
		if(!isset($i['days'])){
			$i['days'] = 0;
		}
		if(!isset($i['employee_id'])){
			$i['employee_id'] = " ";
		}
		if(!isset($i['infraction_name'])){
			$i['infraction_name'] = " ";
		}
		if(!isset($i['sanction_id'])){
			$i['sanction_id'] = 0;
		}
		if(!isset($i['date'])){
			$i['date'] = " ";
		}
		$valid = \Validator::make(
			["employee_id" => $i['employee_id'],
			 "infraction_name" => $i['infraction_name'],
			 "sanction_id" => $i['sanction_id'],
			 "days" => $i['days']//,
		   //"date" => $i['date']
			],
			["employee_id" => "required",
			 "infraction_name" => "required",
			 "sanction_id" => "required|integer",
			 "days" => "required|integer"//,
		   //"date" => "required|date"
			]
		);

		if($valid->fails()){
			return $message = $valid->errors()->all();
		}else{
			return $message = null;
		}
	}
	


	public function create()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		//return $access[1];
		if($access[1] == 200){
		$json = \Input::get('data');
		if(isset($json)){
			$i = json_decode($json,1);

		}else{
			$i = \Input::all();
		}
		if(!isset($i['employee_id'])){
			return response()->json(['header' => ['message' => "please fill before submit data", "status" => 500],"data" => []],500);
		}

		$valid = $this->valid_insert($i);

		if($valid != null){
			$message =  $valid;
			$status = 500;
			$data = [];
		}else{
				$file = \Input::file('file');
				$name_inf = $i['infraction_name'];		
				if(isset($file)  && $file != "undefined"){
					$path = "hrms_upload/infraction";
					$ext = $file->getClientOriginalExtension();
					$size = $file->getSize();
					$rename = "infraction_".str_random(6).".".$ext;

					if($size > 25048576 ){
							return response()->json(['header' => ['message' => "size too large, please upload less than 25 MB", "status" => 500],"data" => []],500);
					}elseif ($ext == "exe" || $ext == "js" || $ext == "php" || $ext == "sql") {
						return response()->json(['header' => ['message' => "unsupported image extension, please upload jpg or png", "status" => 500],"data" => []],500);
					}else{
						$file->move(storage_path($path),$rename);
					}
				}else{
					$path = NULL;
					$rename = NULL;
				}

				if(isset($i['comment'])){
						$comment = $i['comment'];
				}else{
						return response()->json(["header" => ["message" => "please insert comment before submit data", "status" => 500, "access" => $access[3]],"data" => []],500);
				}

				if(!isset($i['days'])){
					$i['days'] = 0;
				}
				$check_req = \DB::SELECT("select created_at from infraction where employee_id='$i[employee_id]' and infraction_name='$i[infraction_name]' and sanction=$i[sanction_id] and days=$i[days] and sanction_date='$i[date]' ");
				if($check_req != null){
					$message =  "Already apply infraction at ".$check_req[0]->created_at;
					$status = 200;
					$data = [];
				}else{
					$query = \DB::SELECT("CALL insert_infraction('$i[employee_id]','$i[infraction_name]',$i[sanction_id],$i[days],'$i[date]')");
					if($comment != null){
						$id_sel = \DB::SELECT("select id from infraction order by id DESC");
						$id_sel = $id_sel[0]->id;
						$insert = \DB::SELECT("insert into command_center(request_id,type_id,comment,path,filename,employee_id) values($id_sel,11,'$comment','$path','$rename','$i[employee_id]')");
					}
					$message =  "Successfull apply infraction";
					$status = 200;
					$data = [];
				}
		}
	}else{
					$message = $access[0]; $status = $access[1]; $data=$access[2];
	}
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}



}
