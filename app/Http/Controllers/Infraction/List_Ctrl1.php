<?php namespace Larasite\Http\Controllers\Infraction;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Library\FuncAccess;
use Illuminate\Http\Request;

class List_Ctrl extends Controller {
	protected $form = "77";

	//  public function __construct(){
	// 	 $inf = \Input::get('infraction');
	// 	 if($inf == "add"){
	// 		 	return $this->$form = "76";
	// 	 }else{
	// 		 return  $this->$form = "77";
	// 	 }
	//  }

	public function index()
	{
	 $FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	 // return $access[1];
	 if($access[1] == 200){
	 //created  18 January 2015

	 $key = \Input::get("key");
	 $encode = base64_decode($key);
	 $explode = explode("-",$encode);
	 $explode_id = $explode[1];

	 //check departemnt by status
	 $hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$explode_id' AND t2.role_name = 'hr'");
	 $adminex = \DB::SELECT("select role.role_name from ldap,role where ldap.employee_id=$explode_id and ldap.role_id=role.role_id ");
	 $supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$explode_id' ");
	 if($hr != null || $adminex == "SuperUser" || $supervisor != null){
		 $department = \DB::SELECT("select name,id as department_id from department where id<>1 ");
	 }elseif($adminex[0]->role_name == "admin"){
		 $department = \DB::SELECT("select department.id as department_id ,department.name from emp,department where emp.employee_id='$explode_id' and emp.department=department.id ");
	 }elseif($adminex[0]->role_name == "user"){
		 $department = \DB::SELECT("select department.id as department_id ,department.name from emp,department where emp.employee_id='$explode_id' and emp.department=department.id ");
	 }elseif($adminex[0]->role_name == "adminIt"){
		 $department = \DB::SELECT("select department.id as department_id ,department.name from emp,department where emp.employee_id='$explode_id' and emp.department=department.id ");
	 }else{
		 $department = null;
	 }
	 $from = date('Y-m-d', strtotime(date('Y-m-d')."-1 month"));
	 $to = date('Y-m-d',strtotime($from."+1 year"));
	 //viewjob title
	 $job_title = \DB::select("select id as job_id, title from job");
	 if($job_title == null){
			 $job_tile = NULL;
	 }
	 //infraction List

	 $infraction = \DB::SELECT("select id as sanction_id, sancation, shortness_sanction from sanction_list");

	 //check status
	 $supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$explode_id' ");
	 $hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$explode_id' AND t2.role_name = 'hr'");
	 if($supervisor != null){
		 $supervisor = "supervisor";
	 }elseif($hr != null){
		 $supervisor = "hr";
	 }else{
		 $supervisor =  "user";
	 }
	 $emp_data = \DB::SELECT("CALL search_employee_by_id('$explode_id')");


		 $data_exp =
			[
			"employee_name" => $emp_data[0]->name,
			"employee_id" => $explode_id,
		 "status" => $supervisor,
		 "department" => $department,
		 "jobtitle" => $job_title,
		 "infraction" => $infraction,
		 "date" => ["from" => $from, "to" => $to]
		 ];

		 $message = "Success";
		 $data = $data_exp;
		 $status = 200;

	 }else{
					 $message = $access[0]; $status = $access[1]; $data=$access[2];
	 }
			 return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	 }



	public function update_infraction(){

		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
				$id = \Input::get('id');  
				$inf_name  = \Input::get('infraction');
				$date = \Input::get('date');
				$sancation = \Input::get('idSanction');
				$day = \Input::get('day');
				$department = \Input::get('idDepartment');

				$employee_id = \Input::get('employee_id');

				$check_data  = \DB::SELECT("select * from leave_request where comment =  $id and leave_type  = 12");
				$check_name = \DB::SELECT("select * from infraction where infraction_name = '$inf_name' and id <> $id ");
				if($check_data != null){
					$message = "can't update data already in use "; $status = 500; $data=[];
				}else if($check_name != null){
					$message = "name infraction already taken"; $status = 500; $data=[];
				}else{
					$update = \DB::SELECT("update infraction set infraction_name = '$inf_name' , sanction = $sancation, sanction_date = '$date', days = $day where id  = $id ");
					
					$message = "success update infraction ";
					$data = \Input::all();
					$status =  200;
					
				}
		}else{
	 			$message = $access[0]; $status = $access[1]; $data=$access[2];
	 	}
	 		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);	
	}
	

	public function delete_infraction($id){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
				$explode =  explode(',',$id);
				$i = 0;
				foreach ($explode as $key => $value) {
					$check_data  = \DB::SELECT("select * from leave_request where comment =  $value and leave_type  = 12");
					if($check_data != null){
						$i = 'test1';
						break;
					}else{
						$update = \DB::SELECT("delete from infraction where id  = $value ");
						$i = 'test';	
					}
				}

				if($i == 'test1'){
					$message = "can't delete some of infraction already in use  "; $status = 500; $data=[];
				}else{
					$message = "Success delete data"; $status = 200; $data=[];		
				}
				
		}else{
	 			$message = $access[0]; $status = $access[1]; $data=$access[2];
	 	}
	 		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);	
	}

	public function detail($id){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
 	 // return $access[1];
 	 if($access[1] == 200){

 	 $i = \Input::all();
 // 	 $data = $this->valid_search($i);
 // 	 if($data != null){
 // 		 $message = $data; $status = 500; $data=[];
 // 	 }
 	 if($i['past'] == 1){
 		 $past = 1;
 	 }else{
 		 $past = 0;
 	 }

	 if(!isset($i['employee_id'])){
		$i['employee_id'] = " ";
	}
	if(!isset($i['from'])){
		$i['from'] = " ";
	}
	if(!isset($i['to'])){
		$i['to'] = " ";
	}
	if($i['department_id']){
		$i['department_id'] = " ";
	}
	if($i['job_id']){
		$i['job_id'] = " ";
	}

 	 $query = \DB::SELECT("CALL view_infraction_test('$i[from]','$i[to]','$i[employee_id]',$i[department_id],$past,$i[job_id])");
	 $comment = \DB::SELECT("select id,comment,path,filename from command_center where id=$id");
	 if($comment !=  null){
		 $query = ["data" => $query, "comment" => $comment];
	 }
 		 if($query != null){
 			 $message = "show record data"; $status = 200; $data=$query;
 		 }else{
 			 $message = "data no"; $status = 500; $data = [];
 		 }
 	 }else{
 			$message = $access[0]; $status = $access[1]; $data=$access[2];
 	 }
 			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
 	 }


 	//$employee_id =  null, $department_id  = null, $job_id =  null, $to =  null, $form =  null, $past = null
	public function search(){
	 $FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	 // return $access[1];
	 if($access[1] == 200){

	 	$past = \Input::get('past');		
	 	if(isset($past)){
			$past = 1;
		}
		$employee_id = \Input::get('employee_id');
		$from = \Input::get('from');
		$to = \Input::get('to');
	 	$department_id = \Input::get('department_id');
	 	$job_id = \input::get('job_id');


	 	$text_employee = " and t1.employee_id='$employee_id'";
	 	$text_pass = " and t1.employee_id not in(select employee_id from emp_termination_reason )";
		$text_depart = " and t2.department=$department_id";
		$text_job = " and t4.job=$job_id";
		$text_db = "select distinct t1.id,t1.employee_id,concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name)as employeeName,
		t2.department as idDepartment,t3.name as department,t4.job as idJob,t5.title as JobTitle,t1.sanction_date as date,
		t1.infraction_name as infraction,t6.id as idSanction,t6.sancation as sanction,t7.id as idCommentCenter,t7.comment,t7.path,t7.filename,t1.days as day,concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name) as name,
		t7.created_at as created
		from infraction t1
		left join emp t2 on t2.employee_id=t1.employee_id
		left join department t3 on t3.id=t1.department
		left join job_history t4 on t4.employee_id=t1.employee_id 
		left join job t5 on t5.id=t4.job
		left join sanction_list t6 on t6.id=t1.sanction
		left join command_center t7 on t7.type_id=t1.type_id and t7.request_id=t1.id
		where (t1.sanction_date between '$from' and '$to')";

		//left join emp t2 on t2.employee_id=t7.employee_id
		
		if(isset($past) && isset($employee_id)){
			$text_db .= $text_pass;
		}elseif(isset($past)){
			$text_db .= $text_pass;
		}elseif(isset($employee_id)){
			$text_db .= $text_employee;
		}else{
			$text_db = $text_db;
		}

		if(isset($department_id) && $department_id != "0"){
			$text_db .=$text_depart;
		}

		if(isset($job_id) && $job_id != "0"){
			$text_db .=$text_job;
		}

	   	$query = \DB::SELECT($text_db);
			if($query != null){
				 $message = "show record data"; $status = 200; $data=$query;
			}else{
				 $message = "data no found"; $status = 200; $data = [];
			}
 	}else{
		$message = $access[0]; $status = $access[1]; $data=$access[2];
 	}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}


	public function create()
	{
	 $FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	 //return $access[1];
	 if($access[1] == 200){
	 $infraction = new EmpEntitliments_Model;
	 $i = \Input::all();
	 $valid = $this->valid_insert($i);

	 if($valid != null){
		 $message =  $valid;
		 $status = 500;
		 $data = [];
	 }else{
			 $check_req = \DB::SELECT("select created_at from infraction where employee_id='$i[employee_id]' and infraction_name='$i[infraction_name]' and sanction=$i[sanction_id] and days=$i[days] and sanction_date=$i[sanction_date] ");
			 $check_name = \DB::SELECT("select * from infraction where infaction_name = '$i[infraction_name]' ");
			 if($check_req != null || $check_name != null){
				 $message =  "Already apply infraction at ".$check_req[0]->created_at;
				 $status = 200;
				 $data = [];
			 }else{
				 $query = \DB::SELECT("CALL insert_infraction('$i[employee_id]','$i[infraction_name]',$i[sanction_id],$i[days],'$i[date]')");
				 $message =  "Successfull apply infraction";
				 $status = 200;
				 $data = [];
			 }
	 }
	}else{
				 $message = $access[0]; $status = $access[1]; $data=$access[2];
	}
		 return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

}
