<?php namespace Larasite\Http\Controllers\Jobs;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Larasite\Model\Master\Job\Status_Model;
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
class EmploymentStatus_Ctrl extends Controller {

protected $form = 4;
private function check_id($id)
{
	$rule = ['undefined',NULL,''];
	if(in_array($id, $rule)){ return 500; }else{ return 200; }
}
private function set_valid()
{ 
	$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text_only'=>'Regex:/^[A-Za\-! ,\'\"\/@\.:\(\)]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
	$rule =  ['title'=>'required|'.$reg['text_num']]; 
	$valid =  \Validator::make(\Input::all(),$rule); return $valid; 
}
private function set_input(){ return ['title'=>\Input::get('title')];	 }

// INDEX [GET]
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Status_Model;
		return $access;
		if($access[1] == 200){
				 if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				 	$message = 'Unauthorized'; $status = 200; $data = [];
				 }else{
				 $datas = $model->Read_EmpStatus(1,[]);
				if($datas){ $message = 'Status : Show Records Data.'; $status = 200; $data = $datas;}
				else{$message = 'Status : Empty Records Data.'; $status = 200; $data = [];}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);	
	}
// STORE [POST]
	public function store() // POST
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/ $model = new Status_Model;
		if($access[1] == 200){
			$input = $this->set_input();
			$valid = $this->set_valid();
			if($valid->fails()){
				$message = 'Required Input Failed.'; $status=406; $data=null; 
			}else{
				$Store = $model->Store_EmpStatus($this->set_input());
				$data = $Store['data']; $message = $Store['message']; $status = $Store['status'];
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);	
	}
// SHOW [GET]
	public function show($id) // GET ID 
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = \DB::table('jobs_emp_status');
		if($access[1] == 200){
				$get = $data_table->find($id);
				if($get){ $message = 'Show Reacords Data.'; $status = 200; $data = $get; }
				else{ $message = 'Empty Records Data.';	$status = 404;	$data = null; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);	
	}
// EDIT [GET]
	public function edit($id) //GET / HEAD
	{	
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = \DB::table('jobs_emp_status');
		if($access[1] == 200){
				$get = $data_table->find($id);
				if($get){ $message = 'Show Reacords Data.'; $status = 200; $data = $get; }
				else{ $message = 'Empty Records Data.';	$status = 404;	$data = null; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);	
	}

//UPDATE [POST]
	public function update($id)
	{  
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new Status_Model;
		if($access[1] == 200){
			$input = $this->set_input();
			$valid = $this->set_valid();
			if($valid->fails()){
				$message = 'Required Input Failed.'; $status=406; $data=null; 
			}else{
				$Update = $model->Update_EmpStatus($id,$this->set_input());
				
				$data = $Update['data']; $message = $Update['message']; $status = $Update['status'];
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);	
	}
// DELETE [POST]
	public function destroy($id) // DELETE
	{
		// CHECK KEY API IN ROUTING
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Status_Model;
		if($access[1] == 200){
				$toArray = explode(",", $id);
				$data = $model->Destroy_EmpStatus($toArray);
				if(!empty($data['status']) && $data['status'] == 200){ 
					$message=$data['message'];	$status=$data['status'];	
				}else{	$message=$data['message'];	$status=$data['status']; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>[]],$status);
	}
}