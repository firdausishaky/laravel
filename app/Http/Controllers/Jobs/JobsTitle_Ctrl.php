<?php namespace Larasite\Http\Controllers\Jobs;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
/*Model*/
use Larasite\Privilege;
use Larasite\Model\Master\Job\JobTitle_Model;
use Larasite\Model\Qualification\Seminar_Model ;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;

class JobsTitle_Ctrl extends Controller {


protected $form = 3;
protected $dir = "/hrms_upload/job_attach";
protected $Extfile = ["title"=>"Job","extend"=>["doc","docx","pdf"]];
private function set_valid()
{ 
	$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text_only'=>'Regex:/^[A-Za\-! ,\'\"\/@\.:\(\)]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
	$rule =  ['title'=>'required|alpha|min:2','descript'=>$reg['text_num']];
	$valid =  \Validator::make(\Input::all(),$rule); return $valid; 
}
private function set_input(){ return ['title'=>\Input::get('title'),'descript'=>\Input::get('descript')];	 }
private function unauth($access){
	return ['header' => ['message' => 'Unauthorized' ,'status' => 200, 'access' => $access[3]], 'data' => []];
}

// INDEX
	public function index() // INDEX
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new JobTitle_Model;
		if($access[1] == 200){
				$result = \DB::table("job")->get(['id','title','descript','filename']);
				if($result)
				{
					if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
						return response()->json($this->unauth($access),200);
					}else{
					 $data = $result; $message = 'Job Title : Show Records Data.'; $status = 200;}
					}
				else{ 
					if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
					return response()->json($this->unauth($access),200);
					}else{$data = null; $message = 'Job Title : Empty Records Data.'; $status = 200; }
				       }			
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);	
	}
// END INDEX
// STORE
	public function store(){ // STORE 
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/ $model = new JobTitle_Model; 
		if($access[1] == 200){
					
					$request = new leaverequest_Model;
					$model = new Seminar_Model;

					$json = \Input::get('data');
					if($json != null){
						$data =  json_decode($json,1);
					
						$title = $data['title'];
						if($title == null || !isset($data['title']))
						{return $request->getMessage("Please fill required form",[],500,$access[3]);}
					
						(!isset($data['descript'] ) || $data['descript'] == null ?  $descript = null :  $descript = $data['descript']  );

						$check = $this->validasi($title);
						if($check != null){
							return $request->getMessage($check,[],500,$access[3]);
						}
						$image = \Input::file('file');
						$imageName = $image->getClientOriginalName();
						$ext = \Input::file('file')->getClientOriginalExtension();
						$path = "hrms_upload/job_attach";
						$size = $image->getSize();
						if($ext != "pdf" ){
							return $request->getMessage("Can't upload file with extension .".$ext.", support only pdf format file",[],500,$access[3]);	
						}elseif($size > 1048576){
							$size = $size / 1000000;
							return $request->getMessage("Size of file is ".substr($size,0,4)." MB, only accept up to 1 MB",[],500,$access[3]);
						}else{

							$req_image = $request->check_imgPDF($ext,$size,$image,$path,$access[3]);
							if($req_image != null && $req_image == "size"){
								return $request->getMessage("failed image format supported but size of image more than 1 MB",[],200,$access);
							}elseif($req_image != null && $req_image == "format"){
								return $request->getMessage("unsupported format",[],200,$access[3]);
							}else{
								 $imageName = $req_image;
							}
						}
					}else{
						$title = \Input::get('title');
						$descript = \Input::get('descript');
						(!isset($descript) ? $descript = null : $descript = $descript);
						$imageName = null;
						$path = null;
					}
					$check_data = \DB::SELECT("select *from job where title = '$title' ");
						if(!isset($check_data[0]->title) || $check_data[0]->title == null ){
							$db = \DB::SELECT(" insert into job(title,descript,filename,path)
							values('$title','$descript','$imageName','$path')");
						
							if(isset($db)){
								$data = \DB::SELECT(" select * from job order by id desc  limit 1");
								$message =  "success";
								$status = 200;
								return  $request->getMessage("Success",$data[0],200,$access[3]);
							}	
						}else{
							return  $request->getMessage("Error add job title, already exists",[],500,$access[3]);
						}
					
		
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,'access'=>$access[3]],'data'=>$data],$status);
	}
// END STORE 
//VALIDATION
	public function validasi($title){
			$reg = '/^[A-Za-z0-9_]{1,15}$/';
			$validation= \Validator::make(
							["title" => $title,
							],
							
							["title" => 'required',
							]
							);
			if($validation->fails()){
				$check = $validation->errors()->all();
				if($check != null){
					return $check;
				}else{
					$check = null;
					return $check;
				}
			}
	}

//END STORE LANJUTAN
// UPDATE 
	public function update($id) // PUT FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new JobTitle_Model; 
		if($access[1] == 200 && $id){
					$request = new leaverequest_Model;
					$model = new Seminar_Model;
					$model1 = new JobTitle_Model;

					$json = \Input::get('data');
					if($json != null){
						$data =  json_decode($json,1);
					
						$title = $data['title'];
						if($title == null || !isset($data['title']))
						{return $request->getMessage("Please fill required form",[],500,$access[3]);}
					
						(!isset($data['descript'] )  || $data['descript'] == null ?  $descript = null :  $descript = $data['descript']  );

						$check = $this->validasi($title);
						if($check != null){
							return $request->getMessage($check,[],500,$access[3]);
						}
						$type = $data['radio'];
						$image = \Input::file('file');
						$imageName = $image->getClientOriginalName();
						$ext = \Input::file('file')->getClientOriginalExtension();
						$size = $image->getSize();
						if($ext != "pdf" ){
							return $request->getMessage("Can't upload file with extension .".$ext.", support only pdf format file",[],500,$access[3]);	
						}elseif($size > 1048576){
							$size = $size / 1000000;
							return $request->getMessage("Size of file is ".substr($size,0,4)." MB, only accept up to 1 MB",[],500,$access[3]);
						}

						$test =  (string)$image;
						$input = [ "imageName" => $test,
							     "ext" => $ext,
							     "image" => $imageName,
							     "size" => $size,
							     "title" => $title,
							     "radio" => $data['radio'],
							     "path" => "hrms_upload/job_attach",
							     "descript" => $descript,
							     "type" => $type,
							   ];


						$data = $model1->check_data($id,$input);
						if(isset($data)){
							$dump= \DB::SELECT("select * from job where id=$id");
							if($data == "ok"){
								return $request->getMessage("Update succesfull",$dump,200,$access[3]);
							}elseif($data == "size" ){
								return $request->getMessage("Size file too large",[],500,$access[3]);
							}elseif($data == "format"){
								return $request->getMessage("Format file not support",[],500,$access[3]);
							}elseif($data == "undefined"){
								return $request->getMessage("file not found",[],500,$access[3]);
							}else{
								return $request->getMessage("Update unsuccesfull",[],500,$access[3]);
							}	
						}
						
					}
					else{
						$title = \Input::get('title');
						$descript = \Input::get('descript');
						(!isset($descript) ? $descript = null : $descript = $descript);
						$imageName = null;
						$path = null;
						$update = \DB::SELECT("update job set title='$title', descript='$descript', filename=null, path=null where id=$id ");
						if(isset($update)){
							$dump= \DB::SELECT("select * from job where id=$id");
							return $request->getMessage("Update succesfull",$dump,200,$access[3]);
						}else{
							return $request->getMessage("Update unsuccesfull",[],500,$access[3]);
						}
					}
				
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,'access'=>$access[3]],'data'=>$data],$status);
	}
//END UPDATE 
// DESTROY 
	public function destroy($id) // DELETE
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete'); /*Model*/ $model = new JobTitle_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		if($access[1] == 200){
				$toArray = explode(",", $id);
				$del = $model->DestroyJob($toArray);
				if(isset($del) && $del['fix'] == 200){ $message = $del['message']; $status = 200; $data=NULL;
				}else{ $message = $del['message']; $status = 500; $data = NULL; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
//END DESTROY 
// CANCEL
		public function cancel(){
		$id = \Request::all(); //$rule =   ['title'=>'required|alpha|min:2','descript'=>'alpha_num'];
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/ $model = new JobTitle_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		if($access[1] == 200){
				$check = JobTitle_Model::find($id['id']);
				if(isset($check,$id['id'])){
					$Move = $this->Move($id['id'],$check->filename,'cancel');
					if($Move[1] == 200){
						JobTitle_Model::destroy($id['id']);
						$message = $Move[0]; $status = $Move[1]; $data=$Move[2];	
					}else{ $message = $Move[0]; $status = $Move[1]; $data=$Move[2]; }
				}else{$message = 'Cancel Operation'; $status = 200; $data=null;}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// END CANCEL
// SHOW 
	public function show($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new JobTitle_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		if($access[1] == 200){ $message = 'Page Not Found.'; $status = 404; $data = null; } else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
//END SHOW
// EDIT 
	public function edit($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new JobTitle_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		if($access[1] == 200){ $message = 'Page Not Found.'; $status = 404; $data = null; } else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// END EDIT
// MOVE
	private function Move($id,$file,$type){
		$model = new JobTitle_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		//$check = $model->check_file_id($id);
		//$checks = JobTitle_Model::find($id);
		if($id){
		//if($check || isset($checks->title)){
			$check = $model->check_file_id($id);
			$checks = JobTitle_Model::find($id);
			if($type == 'cancel'){ 
				$move = $this->move_file($check,'job');
				$message='Cancel Operation.'; $status=200; $data = null;
			}
			elseif($type == 'delfile'){
				$move = $this->move_file($check,'job');
				$message=$move['message']; $status=$move['message']; $data=NULL;
			}
			else{
				if($check){
					$move = $this->move_file($check,'job');
					$up = $this->Upload($id,$file,false);
					$message=$up[0]; $status=$up[1]; $data=$up[2];	
				}else{
					$up = $this->Upload($id,$file,false);
					$message=$up[0]; $status=$up[1]; $data=$up[2];
				}
			}
		}else{ $up = $this->Upload(false,$file,true); $message=$up['message']; $status=$up['status']; $data=$up; }
		return [$message,$status,$data];
	}
// END MOVE
// UPLOAD
	private function Upload($id,$file,$type){
		$model = new JobTitle_Model;  $dir = $this->dir;
		$upload =  $this->upload_func($this->dir,$file,$this->Extfile);
		return $upload;
		// if($upload['status'] == 200){
		// 	if($type == true){
		// 		\DB::table('job')->insert(['filename'=>$upload['filename'],'path'=>$upload['path']]);
		// 		$file->move(storage_path($upload['path']),$upload['filename']);
		// 		$message = $upload['message']; $data = ['id'=>$model->GetID($upload['filename']),'filename'=>$upload['filename']]; $status = 200;
		// 	}else{
		// 		$model->JobUpdate($id,null,null,$upload['filename'], $upload['path']);
		// 		$file->move(storage_path($upload['path']),$upload['filename']);
		// 		$message = 'Updated Successfully.';$status = 200; $data=JobTitle_Model::find($id);
		// 	}
		// }else{ $message=$upload['message']; $status=$upload['status']; $data = null;}
		// return [$message,$status,$data];
	}
// END UPLOAD
// DOWNLOAD
	public function download($filename){
		$check = \DB::select("SELECT filename FROM job where filename = '$filename'");
		if($check){
			foreach ($check as $key) { $files = $key->filename;}
			$path = storage_path()."/hrms_upload/job_attach/".$files;
			$file = \File::get($path);$type = \File::mimeType($path);
			return \Response::make($file,200,['Content-Type'=>$type]); 	
		}else{ return \Response::json(['header'=>['message'=>'File not found']],404); }
	}
// END DOWNLOAD


	private function upload_func($dir,$file,$ext){
			$CurrentExt = $file->getClientOriginalExtension();
			$date = date_create(); $data_uniqid = uniqid();
			if(in_array($CurrentExt,$ext['extend']) ){ // filter type yang support upload

				if($file->getClientSize() <= 10000000){

					$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME); //SLICE CHAR
					$str = str_slug($slice_FileName,$separator = "_");
					$dir_move = storage_path($dir);
					$data = $ext['title']."_".date_timestamp_get($date).".".$CurrentExt; $msg = 'Upload Successfully.';	$status=200;

					return ['filename'=>$data,'path'=>$dir,'type'=>$CurrentExt,'size'=>$this->formatSizeUnits($file->getClientSize()),'message'=>$msg,'status'=>$status];
				}else{ $msg = 'Max Size <= 2MB '; $data = null; $status=200; return ['filename'=>$data,'message'=>$msg]; }
			}else{ $msg = "Type not support $CurrentExt file"; $data = null; $status = 500; return ['status'=>$status,'type'=>$CurrentExt,'message'=>$msg,'size'=>$this->formatSizeUnits($file->getClientSize())]; }
		}
		/* MOVE FILE TO TRASH */
		public function move_file($file,$table){
			$get_data = \DB::select("SELECT `filename`, `path` from $table where filename = '$file' ");
			foreach ($get_data as $key) { $data['path'] = $key->path; $data['filename'] = $key->filename; }
			
			if(\Storage::disk('local')->exists($data['path']."/".$data['filename'])){
				\Storage::disk('local')->move($data['path']."/".$data['filename'], "/Trash/".$data['filename']);
				if( \Storage::disk('local')->exists("/Trash/".$data['filename']) ){ $message = 'Update Successfully.'; $status = 200; }
				else{ \Storage::disk('local')->move("/Trash/".$data['filename'],$data['path']."/".$data['filename']); $message = 'gagal move file to trash'; $status = 500; }
			}
			else{ $message = 'File tidak ditemukan, atau file telah dihapus check folder trash'; $status = 404; }
			
			return ['message'=>$message,'status'=>$status];
		}
		/* CONVERT SIZE FILE */
		private function formatSizeUnits($bytes)
	    {
	        if ($bytes >= 1073741824){	$bytes = number_format($bytes / 1073741824, 2) . ' GB';	}
	        elseif ($bytes >= 1048576){	$bytes = number_format($bytes / 1048576, 2) . ' MB';	}
	        elseif ($bytes >= 1024){	$bytes = number_format($bytes / 1024, 2) . ' KB';}
	        elseif ($bytes > 1){	$bytes = $bytes . ' bytes';}
	        elseif ($bytes == 1){	$bytes = $bytes . ' byte';}
	        else{	$bytes = '0 bytes'; }
	        return $bytes;
		}/* --- END METHOD FILE ---*/
		public function Sizefile($bytes)
	    {
	        if ($bytes >= 1073741824){	$bytes = number_format($bytes / 1073741824, 2);	}
	        elseif ($bytes >= 1048576){	$bytes = number_format($bytes / 1048576, 2);	}
	        elseif ($bytes >= 1024){	$bytes = number_format($bytes / 1024, 2);}
	        elseif ($bytes > 1){	$bytes = $bytes . ' bytes';}
	        elseif ($bytes == 1){	$bytes = $bytes . ' byte';}
	        else{	$bytes = '0 bytes'; }
	        return $bytes;
		}/* --- END METHOD FILE ---*/
	
}
