<?php


class Upload{

var $path_img = '/hrms/upload/works/picture';
var $path_doc = '/hrms/upload/works/document';
	
/*
* deklarasi extension file
* return array ext jika true
*/	
	public function ext($page){
		
		$pages = ['contract_attach','seminar_attach','immigration_attach','comment_attach'];
		
		if($page == 'job_attach'){
			$ext = ['doc','docx','pdf'];	
		}
		elseif(in_array($page, $pages)) {
			$ext = ['pdf','png','jpg','jpeg','gif'];	
		}
		elseif($page == 'csv_import_attach'){
			$ext = ['csv'];
		}
		elseif($page == 'logo_sys'){
			$ext = ['png','jpg','jpeg','gif'];
		}
		return $ext;
	}

/*
* fungsi check ext untuk move to folder doc / img
*/
	public function check_ext($type){
		$ext_img = ['png','jpg','jpeg','gif'];
		$ext_doc = ['doc','docx','pdf'];
		
		if(in_array($type,$ext_doc)){

			$path = $this->path_doc;
		}
		else{
			$path = $this->path_img;
		}
		return $path;
	}



/*	########################################################################################
* Fungsi upload dokumen job
* return array = file_name,type jika true
*/
	public function upload_job($file){
		
		$allowed = $file->getClientOriginalExtension();
		
		if(in_array($allowed,$this->ext('job_attach')) ){

			$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);			
			$str = str_slug($slice_FileName,$separator = '_');
			
			$dir = "job_attach/";
			$file->move($this->check_ext($allowed)."/".$dir,$str.".".$allowed);
			$data = $str.".".$allowed;
		}
		else{
		
			$data = 0;
		}
		return ['file_name'=>$data,'type'=>$allowed];
	}

/*	########################################################################################
* Fungsi upload dokumen csv
* return array = file_name,type jika true
*/
	public function upload_csv($file){
		
		$dir = "csv_attach/";
		$allowed = $file->getClientOriginalExtension();
		
		if(in_array($allowed,$this->ext('csv_attach')) ){

			$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);			
			$str = str_slug($slice_FileName,$separator = '_');
			$file->move($this->check_ext($allowed)."/".$dir,$str.".".$allowed);
			$data = $str.".".$allowed;
		}
		else{
		
			$data = 0;
		}
		return ['file_name'=>$data,'type'=>$allowed];
	}



/* ########################################################################################
* Fungsi upload dokumen contract detail
* return array = file_name,type jika true
*/
	public function upload_contract($file){
		
		$dir = "contract_attach/";
		$allowed = $file->getClientOriginalExtension();
		
		if( in_array($allowed,$ext)){

			$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);			
			$str = str_slug($slice_FileName,$separator = '_');
			
			if($allowed)
			$file->move($this->check_ext($allowed)."/".$dir,$str.".".$allowed);
			$data = $str.".".$allowed;
		}
		else{
		
			$data = 0;
		}
		return ['file_name'=>$data,'type'=>$allowed];
	}


/* ########################################################################################
* Fungsi upload dokumen training detail
* return array = file_name,type jika true
*/
	public function upload_training($file){
		
		$dir = "training_attach/";
		$allowed = $file->getClientOriginalExtension();
		
		if( in_array($allowed,$ext)){

			$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);			
			$str = str_slug($slice_FileName,$separator = '_');
			$file->move($this->$this->check_ext($allowed)."/".$dir,$str.".".$allowed);
			$data = $str.".".$allowed;
		}
		else{
		
			$data = 0;
		}
		return ['file_name'=>$data,'type'=>$allowed];
	}	


/* ########################################################################################
* Fungsi upload dokumen comment detail
* return array = file_name,type jika true
*/
	public function upload_comment($file){
		
		$dir = "comment_attach/";
		$allowed = $file->getClientOriginalExtension();
		
		if( in_array($allowed,$ext)){

			$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);			
			$str = str_slug($slice_FileName,$separator = '_');
			$file->move($this->check_ext($allowed)."/".$dir,$str.".".$allowed);
			$data = $str.".".$allowed;
		}
		else{
		
			$data = 0;
		}
		return ['file_name'=>$data,'type'=>$allowed];
	}


/* ########################################################################################
* Fungsi upload dokumen immigration detail
* return array = file_name,type jika true
*/
	public function upload_immigration($file){
		
		$dir = "immigration_attach/";
		$allowed = $file->getClientOriginalExtension();
		
		if( in_array($allowed,$ext)){

			$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);			
			$str = str_slug($slice_FileName,$separator = '_');
			$file->move($this->check_ext($allowed)."/".$dir,$str.".".$allowed);
			$data = $str.".".$allowed;
		}
		else{
		
			$data = 0;
		}
		return ['file_name'=>$data,'type'=>$allowed];
	}

}