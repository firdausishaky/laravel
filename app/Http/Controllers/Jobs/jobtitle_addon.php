<?php namespace Larasite\Http\Controllers\Jobs;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
/*Model*/
use Larasite\Privilege;
use Larasite\Model\Master\Job\JobTitle_Model;
use Larasite\Model\Qualification\Seminar_Model ;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;

class jobtitle_addon extends Controller {

	public function insert()
	{
	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/ $model = new JobTitle_Model; 
		if($access[1] == 200){
				
					$request = new leaverequest_Model;
					$model = new Seminar_Model;
					$json = \Input::get('data');
					if($json != null){
						$data =  json_decode($json,1);
					
						$title = $data['title'];
						if($title != null || !isset($data['title']))
						{return $request->getMessage("Please fill required form",[],500,$crud[3]);}
					
						(isset($data['descript'] ) || $data['descript'] != null ? $descript = $data['descript'] : $descript = null  );

						$check = $this->validasi($title);
						if($check != null){
							return $request->getMessage($check,[],500,$crud[3]);
						}
						$image = \Input::file('file');
						$imageName = $image->getClientOriginalName();
						$ext = \Input::file('file')->getClientOriginalExtension();
						$path = "/hrms_upload/job_attach";
						$size = $image->getSize();
						if($ext != "pdf" ){
							return $request->getMessage("Can't upload file with extension .".$ext.", support only pdf format file",[],500,$crud[3]);	
						}elseif($size > 1048576){
							$size = $size / 1000000;
							return $request->getMessage("Can't upload file more than".$size."MB",[],500,$crud[3]);
						}else{

							$req_image = $request->check_imgPDF($ext,$size,$image,$path,$crud[3]);
							if($req_image != null && $req_image == "size"){
								return $request->getMessage("failed image format supported but size of image more than 1 MB",[],200,$access);
							}
							if($req_image != null && $req_image == "format"){
								return $request->getMessage("unsupported format",[],200,$crud[3]);
							}
						}
					}else{
						$title = \Input::get('title');
						$descript = \Input::get('descript');
						(!isset($descript) ? $descript = null : $descript = $descript);
						$imageName = null;
						$path = null;
					}
				
					$db = \DB::SELECT(" insert into job(title,descript,filename,path)
						values('$title','$descript','$filename','$path')");
					
					if(isset($db)){
						$data = \DB::SELECT(" select * from job order by id desc  limit 1");
						$message =  "success";
						$status = 200;
						return  $request->getMessage("Success",$data[0],200,$crud[3]);
					 }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
}