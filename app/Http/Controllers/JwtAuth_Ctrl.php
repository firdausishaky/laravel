<?php namespace Larasite\Http\Controllers;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Larasite\UserTest as User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class JwtAuth_Ctrl extends Controller {

	public function authenticate(\Request $request)
	{
		$credentials = $request->only('username','password');
		try {
			if (!$token = JWTAuth::attempt($credentials)) {
				return \Response::json(['error'=>'Invalid Creadentials'],401);
			}
		} catch (JWTException $e) {
			return \Response::json(compact('token'));		
		}
	}

	public function getAuthenticatedUser()
	{
		try {
			if (!$user = JWTAuth::parseToken()->authenticate() ) {
				return \Response::json(['User not found'],$e->getStatusCode());
			}
		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
			return \Response::json(['Token Expired'],$e->getStatusCode());
		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
			return \Response::json(['Token Invalid'],$e->getStatusCode());
		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
			return \Response::json(['Token Absent'],$e->getStatusCode());
		}

		return \Response::json(compact('user'));
	}

	public function Registered(Request $request)
	{
		$newuser = $request->all();
		$password = \Hash::make($request->input('password'));

		$newuser['password'] = $password;
		return User::create($newuser);
	}
}
