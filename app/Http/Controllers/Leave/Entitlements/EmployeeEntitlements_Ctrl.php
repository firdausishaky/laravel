<?php namespace Larasite\Http\Controllers\Leave\Entitlements;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Model\Leave\Entitlements\EmpEntitliments_Model;
class EmployeeEntitlements_Ctrl extends Controller {

/**
* Display a listing of the resource.
*
* @return Response
*/
	/*
	 employee autocomplete :
	 url : hrms.dev/api/leave/Emp?key=VXdRUlhXMjJoSkRQSndKeEdmZ2FDa3djZVlXMEd6eE41dW84eERrai0yMDE0ODg4
	parameter : name = character

	leave type
	url : hrms.dev/api/leave/viewEmp?name=jajang&key=?key=VXdRUlhXMjJoSkRQSndKeEdmZ2FDa3djZVlXMEd6eE41dW84eERrai0yMDE0ODg4
	*/
	protected $form = "69";

	public function adjust(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			
			$input = \Input::all();
			unset($input['key']);
			

			foreach ($input as $key => $value) {

				if($value['LeaveType'] == "Vacation Leave"){
					$arg =  $input[$key];
				}else{
					$arg =  null;
				}
			}



			$employee_idx =  $arg['employee_id'];

			$select = \DB::SELECT("select * from all_vacation where empx = '$employee_idx' ");
			if($select != null){
				$count =  count($select);
			  	$start_date =  $select[0]->entitlement_start_vl_expat;
				$date_final =  date_diff(date_create(date('Y-m-d')),date_create($start_date));
				$tenure = floor($date_final->days / 365);
				if($tenure == 0){
					$tenure  += 1;
				}
				$data_new = \DB::SELECT("select leave_vacation_detail.days,leave_vacation_detail.offday_oncall from emp,leave_vacation,leave_vacation_detail  where emp.employee_id = '$employee_idx'   
							and emp.department = leave_vacation.department and leave_vacation.id = leave_vacation_detail.vacation_id and leave_vacation_detail.year");
				if(!isset($select[$count-1]->dayoff)){
					return \Response::json(['header'=>['message'=>'please set up day_off in leave table','status'=>500],'data'=>[]],500);
				}else{
					$off_day =  $select[$count-1]->dayoff - $select[$count-1]->dayoff_used;
				}
				$date_final = $data_new[0]->days;
				$between =  date_diff(date_create($select[$count-1]->tos),date_create($select[$count-1]->froms));
				
				$between_dayoff =  $select[$count-1]->dayoff - $select[$count-1]->dayoff_used;
				$AvailedDays =  $select[$count-1]->days_vl_expat - $between->days;
				
			}else{
				$data =  \DB::SELECT("select entilited_start  from leave_vacation,emp where emp.employee_id =  '$employee_idx' and  emp.department =  leave_vacation.department");
				
				if($data != null){
				$get_start =  $data[0]->entilited_start;
				$job_history = \DB::SELECT("select $get_start  from job_history where employee_id = '$employee_idx' ");
				$tenure = date_diff(date_create(date('Y-m-d')),date_create($job_history[0]->$get_start));
				$tenure =  floor($tenure->days/365);
				if($tenure == 0){
					$tenure = $tenure + 1;
				}
				$data_new= \DB::SELECT("select leave_vacation_detail.days,leave_vacation_detail.offday_oncall from emp,leave_vacation,leave_vacation_detail where employee_id where employee_id = '$employee_idx'   
							and emp.department = leave_vacation.department and leave_vacation.id = leave_vacation_detail.vacation and leave_vacation_detail.year  = $tenure");
				$date_final = $date_new[0]->days;
				$off_day = $date_new[0]->offday_oncall;
				}else{
					return \Response::json(['header'=>['message'=>'please set up leave table','status'=>500],'data'=>[]],500);
				}
				$AvailedDays =  $date_final;
				$between_dayoff =  $offDay;
			}
			
			if($date_final == null){
				$message = "Data not exist";
				$status = 200;
				$data = [];
			}	else{
			//$db = \DB::SELECT("select sum(days) as days,sum(offday_oncall) as offday_oncall from leave_vacation_detail ");
			
			$updated  = \DB::SELECT(" select * from leave_table_personal where employee_id = '$employee_idx' ");
			if($updated != null){
				$date_final = $updated[0]->vacation;
				$off_day =  $updated[0]->day_off;
			}
			$var = [end($select)];

			$message = "success";
			$status = "200";
			$min = "24";
			$max = $date_final;
			$data =  ["vacLeave" => $date_final, "offDay" => $off_day, 
					  "AvailedVacation" => $AvailedDays,
					  "AvailedOffDay" => $between_dayoff, "total" => $max+$between_dayoff, "Min" => 24, "Max" => $max ];
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function confirm(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$message = "Success confirm data";
			$status = "200";
			//$data =  ["vacLeave" => "34", "offDay" => "32", "AvailedVacation" => "12","AvailedOffDay" => "24", "total" => "66"  ];
			$data =[];
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function viewEmp()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$empEntitlements = new EmpEntitliments_Model;
			$input = \Input::all();
			$name = \Input::get('name');
			$rule = [
					'name' => 'required|Regex:/^[A-Za-z] $/'
					];
			$validation = \Validator::make($input,$rule);
			if($validation->fails()){
				$check = $validation->errors()->all();
				return $this->getMessage('input format invalidate',$check,500, $access[3]);
			}else{
				$query = \DB::SELECT("CALL Search_emp_by_name('$name')");
				return $empEntitlements->index($query,$access[3]);
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
	}

	public  function index(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$leave = new EmpEntitliments_Model;
			$leaveType = \DB::SELECT("select id,leave_type from leave_type where id not in(1,3,9)");
			$department = \DB::SELECT("select id,name from department where id not in(0,1)");		
			$query = ["leave_type" => $leaveType, "department" => $department];
			//$query = array_merge($query,[["id" => "0", "leave_type" =>"All"]]);
			return  $leave->index($query,$access[3]);

		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
	}

	public function search(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$empEntitlements = new EmpEntitliments_Model;
			$input = \Input::all();
			$employee = \Input::get('employee');
			$typex = \Input::get('leave_type');
			$department = \Input::get('department');
			$year = \Input::get('year');



			// declare validasi
			$reg = [
			'num'=>'Regex:/^[0-9-\! -\^]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];

			$rule = [
				"employee"=>"required|numeric",
				"department"=>"numeric",
				"leave_type"=>"numeric",
				"year"=>"numeric"
			];
			$valid = \Validator::make(\Input::all(),$rule);

			if($valid->fails()){
				$message='Required Input.'; $status=500; $data=null;
				return response()->json(['header'=>['message' => 'data not found', 'status' => 500],'data' => []],500);
			}


			if(!isset($year)){
				$year  =   date('Y');
			}
			
			if(isset($typex)){
				$typex = "and t0.leave_type = $typex ";
			}else{
				$typex = '';
			}

			if($department){
				$department = " and t2.department =  $department ";
			}else{
				$department = '';
			}

			/*$q = "select concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name) as employee_name,
													t1.leave_type as LeaveType,
													(select ((balance_day-1) +  taken_day) as  DaysEntitled  from leave_request as lr where lr.leave_type = t0.leave_type  and year =  t0.year and  lr.employee_id  =  t0.employee_id order  by id asc limit 1) as DaysEntitled,
												    t0.balance_day as  DaysRemaining,
												    (select sum(taken_day) from leave_request as lr where lr.leave_type = t0.leave_type  and year =  t0.year and  lr.employee_id  =  t0.employee_id) as AvailedDays,
												    t0.created_at as lastDate,
												    (select sum(taken_day) from leave_request as lr where lr.leave_type = t0.leave_type  and year =  t0.year and  lr.employee_id  =  t0.employee_id  and status_id not in (1,3,4)) as NumberOfAvailment,  
													concat(YEAR(CURDATE()),'-12-31') as ValidUntil, t0.balance_day as leave_bl, t0.taken_day as leave_taken, t0.leave_type as leave_type from leave_request t0 
										left join leave_type t1  on t1.id = t0.leave_type
										left join emp t2  on t2.employee_id = t0.employee_id
										where  
										t0.employee_id  =   '$employee'
										and  t0.year =   '$year'  
										and t0.status_id = 2
										and t0.created_at = (select created_at from leave_request where leave_request.leave_type =  t0.leave_type order by id desc limit 1)";*/
			// backup 11/10/2018 
			// add total days entitled suspension
			/*			    //(DATEDIFF(t0.to_,t0.from_)+1) as DaysRemaining2,
			$q = "select concat(t2.first_name,' ',ifnull(t2.middle_name,''),' ',ifnull(t2.last_name,'')) as employee_name,
							t1.leave_type as LeaveType,
							(select ((balance_day-1) +  taken_day) as  DaysEntitled  from leave_request as lr where lr.leave_type = t0.leave_type  and year =  t0.year and  lr.employee_id  =  t0.employee_id order  by id asc limit 1) as DaysEntitled,
							(select sum((balance_day-1) +  taken_day) as  DaysEntitled  from leave_request as lr where lr.leave_type = t0.leave_type  and year =  t0.year and  lr.employee_id  =  t0.employee_id order  by id asc limit 1) as DaysEntitled_sum,
						    t0.balance_day as  DaysRemaining,
						    IF(
									((DATEDIFF(t0.to_,t0.from_)+1)-t0.day_off) < 1,
									(DATEDIFF(t0.to_,t0.from_)+1),
									(((DATEDIFF(t0.to_,t0.from_)+1)-t0.day_off))
								) as DaysRemaining2,
						    (select sum(taken_day) from leave_request as lr where lr.leave_type = t0.leave_type  and year =  t0.year and  lr.employee_id  =  t0.employee_id) as AvailedDays,
						    t0.created_at as lastDate,
						    (select sum(taken_day) from leave_request as lr where lr.leave_type = t0.leave_type  and year =  t0.year and  lr.employee_id  =  t0.employee_id  and status_id not in (1,3,4)) as NumberOfAvailment,  
							concat(YEAR(CURDATE()),'-12-31') as ValidUntil, t0.balance_day as leave_bl, t0.taken_day as leave_taken, t0.leave_type as leave_type 
				from leave_request t0 
				left join leave_type t1  on t1.id = t0.leave_type
				left join emp t2  on t2.employee_id = t0.employee_id
				where  
				t0.employee_id  =   '$employee'
				and  t0.year =   '$year'  
				and t0.status_id = 2";*/

							    //(DATEDIFF(t0.to_,t0.from_)+1) as DaysRemaining2,
			// $q = "select t0.id, concat(t2.first_name,' ',ifnull(t2.middle_name,''),' ',ifnull(t2.last_name,'')) as employee_name,
			// 				t1.leave_type as LeaveType,
			// 				(select ((balance_day-1) +  taken_day) as  DaysEntitled  from leave_request as lr where lr.leave_type = t0.leave_type  and year =  t0.year and  lr.employee_id  =  t0.employee_id order  by id asc limit 1) as DaysEntitled,
			// 				(select sum((balance_day-1) +  taken_day) as  DaysEntitled  from leave_request as lr where lr.leave_type = t0.leave_type  and year =  t0.year and  lr.employee_id  =  t0.employee_id order  by id asc limit 1) as DaysEntitled_sum,
			// 			    t0.balance_day as  DaysRemaining,
			// 			    IF(
			// 						((DATEDIFF(t0.to_,t0.from_)+1)-ifnull(t0.day_off,0)) < 1,
			// 						(DATEDIFF(t0.to_,t0.from_)+1),
			// 						(((DATEDIFF(t0.to_,t0.from_)+1)-ifnull(t0.day_off,0)))
			// 						) as AvailedDays,
						    
			// 			    t0.created_at as lastDate,
			// 			    1 as NumberOfAvailment,  
			// 				concat(YEAR(CURDATE()),'-12-31') as ValidUntil, t0.balance_day as leave_bl, t0.taken_day as leave_taken, t0.leave_type as leave_type 
			// 	from leave_request t0 
			// 	left join leave_type t1  on t1.id = t0.leave_type and YEAR(t0.updated_at) = '$year' and t0.status_id = 2 $typex
			// 	left join emp t2  on t2.employee_id = t0.employee_id
			// 	where  
			// 	t0.employee_id  =   '$employee'
			// 	and  t0.year =   '$year'  
			// 	";
				//and t0.created_at = (select created_at from leave_request where leave_request.leave_type =  t0.leave_type order by id desc limit 1)

			$q = "select t0.id, concat(t2.first_name,' ',ifnull(t2.middle_name,''),' ',ifnull(t2.last_name,'')) as employee_name,
							t1.leave_type as LeaveType,
							(select ((balance_day-1) +  taken_day) as  DaysEntitled  from leave_request as lr where lr.leave_type = t0.leave_type  and year =  t0.year and  lr.employee_id  =  t0.employee_id order  by id asc limit 1) as DaysEntitled,
							(select sum((balance_day-1) +  taken_day) as  DaysEntitled  from leave_request as lr where lr.leave_type = t0.leave_type  and year =  t0.year and  lr.employee_id  =  t0.employee_id order  by id asc limit 1) as DaysEntitled_sum,
						    t0.balance_day as  DaysRemaining,
						    IF(
									((DATEDIFF(t0.to_,t0.from_)+1)-ifnull(t0.day_off,0)) < 1,
									(DATEDIFF(t0.to_,t0.from_)+1),
									(((DATEDIFF(t0.to_,t0.from_)+1)-ifnull(t0.day_off,0)))
									) as AvailedDays,
						    
						    t0.created_at as lastDate,
						    1 as NumberOfAvailment,  
							concat(YEAR(CURDATE()),'-12-31') as ValidUntil, t0.balance_day as leave_bl, t0.taken_day as leave_taken, t0.leave_type as leave_type 
				from leave_request t0 
				left join leave_type t1  on t1.id = t0.leave_type
				left join emp t2  on t2.employee_id = t0.employee_id
				where  
				t0.employee_id  =   '$employee'
				and YEAR(t0.from_) = '$year' 
				and t0.status_id = 2 
				$typex  
				$department";

			


			$q .= " order by t0.created_at ASC";
			
			$get_last  = \DB::SELECT($q);
			$message = ( !isset($get_last) || $get_last  == null ? 'data not found' : 'success load searching data' );
				
				$tmp = [];
				
				for ($i=0; $i < count($get_last); $i++) { 

						$typeLeave = explode(' ', $get_last[$i]->LeaveType);
						
						
						if( isset($tmp[$typeLeave[0]]) ){
							$tmp[$typeLeave[0]]++;
							$get_last[$i]->NumberOfAvailment = $tmp[$typeLeave[0]];
						}else{
							$tmp[$typeLeave[0]] = 1;
							$get_last[$i]->NumberOfAvailment = $tmp[$typeLeave[0]];
						}
						// if($i == 2){
						// 	return [$tmp,$get_last[$i]->NumberOfAvailment, $tmp[$typeLeave[0]]];
						// }

						//$get_last[$i]->NumberOfAvailment = $i+1;

						if($get_last[$i]->leave_type == 12){
							// backup 11/10/2018
							// get days sanction
							$get_q = \DB::SELECT("SELECT distinct inf.* from infraction `inf` left join leave_request `lr` on lr.employee_id = inf.employee_id and inf.created_at >= concat('$year','-01-01') where lr.employee_id='$employee' and lr.year=$year and leave_type = 12");
							//return [$get_q];
							if(count($get_q) > 0){
								foreach ($get_q as $key1 => $value1) {
									$get_last[$i]->DaysEntitled += $value1->days;	
								}
							}
							//$get_last[$i]->DaysEntitled = $get_last[$i]->leave_bl;
							// BACKUP 04022019 :: Availment Days
							$get_last[$i]->AvailedDays = (string)$get_last[$i]->AvailedDays/*$get_last[$i]->leave_taken*/;
							
							$get_last[$i]->DaysRemaining = $get_last[$i]->DaysEntitled - $get_last[$i]->AvailedDays;
							
							/*//return [$get_last[$i]];
							$get_last[$i]->DaysEntitled = $get_last[$i]->leave_bl;
							$get_last[$i]->AvailedDays = $get_last[$i]->leave_taken;
							$get_last[$i]->DaysRemaining = $get_last[$i]->DaysEntitled - $get_last[$i]->AvailedDays;*/
						}
						if($get_last[$i]->leave_type == 10){
							$idss = $get_last[$i]->id;
							//return [$idss];
							$ado_count = \DB::SELECT("select distinct t1.name_ado,t1.date_ado,concat(t2.shift_code,' : ', time_format(t2._from,'%H:%i'),' - ',time_format(t2._to,'%H:%i'))as Schedule, t1.change_date, t1.next_day_off
											from leave_ado t1
											left join attendance_work_shifts t2 on t2.shift_id=t1.schedule_id
											where t1.leave_request_id= $idss");
							$ado_count = count($ado_count);
							$get_last[$i]->AvailedDays = $ado_count;
						}
						if($get_last[$i]->DaysEntitled < 0){
							$get_last[$i]->DaysEntitled += 1;
						}else{
							$get_last[$i]->AvailedDays = (string)$get_last[$i]->AvailedDays/*$get_last[$i]->leave_taken*/;
						}
					}
					//return $tmp;
					return \Response::json(['header'=>['message'=>$message,'status'=>200],'data'=>$get_last],200);
			// $base_db .= $group_db;
			$result = [];
			$arg = [];
			$arg2 = [];
			$arg3 = [];
			$final  = \DB::SELECT("call view_all_leave_balance_entitled()");

			foreach ($final as $key => $value) {
				if($value->status != 2){
					unset($final[$key]);
				}
			}

			foreach ($final as $key => $value) {
				$arg[$value->name]  =  $value->name;
				$arg2[$value->leave_type][$value->name] = $final[$key];
				$arg3[$value->name] =  $value->name;
			}	


			$arr = ['Birthday Leave', 'Vacation Leave','Enhance vacation Leave','Sick Leave','Maternity Leave','Paternity Leave','Bereavement Leave',
					'Marriage Leave','offday_oncall','Accumulation Day Off','Emergency Leave','Suspension'];

			foreach($arr as $key => $value){
				foreach ($arg3 as $keys => $values) {
					if(isset($arg2[$value])){
						if(isset($arg2[$value][$keys])){
							$result[] =  $arg2[$value][$keys];
						}
					}
				}
			}




			// for suspension 

			$arr_suspension = [];
			$finalv1  = \DB::SELECT("call view_all_leave_balance_entitled()");
			foreach ($finalv1 as $key => $value) {
				if($value->leave_type ==  "Suspension"){
					$arr_suspension[] = $finalv1[$key];
				}
			}

			// finsished suspension module 
			
			
			if($result != null){
				foreach ($result as $key => $value) {
					if($value->leave_type == "Birthday Leave"){
						$type = 1;
					}else if($value->leave_type == "Vacation Leave"){
						$type = 2;
					}else if($value->leave_type == "Enhance Vacation Leave"){
						$type = 3;
					}else if($value->leave_type ==  "Sick Leave"){
						$type = 4;
					}else if($value->leave_type ==  "Maternity Leave"){
						$type =  5;
					}else if($value->leave_type ==  "Paternity Leave"){
						$type =  6;
					}else if($value->leave_type ==  "Bereavement Leave"){
						$type = 7;
					}else if($value->leave_type ==  "Marriage Leave"){
						$type = 8;
					}else if($value->leave_type ==  "offday_oncall"){
						$type = 9;
					}else if($value->leave_type ==  "Accumulation Day Off"){
						$type = 10;
					}else if($value->leave_type ==  "Suspension"){
						$type = 12;
					}else{
						$type = 11;
					}	

				
					if(isset($employee) && $employee != null){
						if($value->empx != $employee){
							unset($result[$key]);
						}


					}

		
					if(isset($typex) && $typex != null){
						if($type != $typex){
							unset($result[$key]);
						}
					}

					if(isset($department) && $department != null){
						$depart = \DB::SELECT("select employee_id from emp where department=$department"); 
						if(in_array($value->empx,$depart) ==  true){
							unset($result[$key]);
						}
					}
				}


				foreach ($result as $key => $value) {
					$explode =  explode(" ",$value->date);
					if($value->leave_type == "Birthday Leave"){
						$type = 1;
					}else if($value->leave_type == "Vacation leave"){
						$type = 2;
					}else if($value->leave_type == "Enhance Vacation leave"){
						$type = 3;
					}else if($value->leave_type ==  "Sick Leave"){
						$type = 4;
					}else if($value->leave_type ==  "Maternity Leave"){
						$type =  5;
					}else if($value->leave_type ==  "Paternity Leave"){
						$type =  6;
					}else if($value->leave_type ==  "Bereavement Leave"){
						$type = 7;
					}else if($value->leave_type ==  "Marriage Leave"){
						$type = 8;
					}else if($value->leave_type ==  "offday_oncall"){
						$type = 9;
					}else if($value->leave_type ==  "Accumulation day Off"){
						$type = 10;
					}else if($value->leave_type ==  "Suspension"){
						$type = 12;
					}else{
						$type = 11;
					}	
					$year = date('Y') . '-12-31';
					$empx = $value->empx;
					if(isset($value->entilited_days) and $value->entilited_days == 0 and $value->LeaveType = "Vacation Leave"){
						$entil = \DB::SELECT("select days_vl_expat from all_vacation where empx = '$empx' order by ids ASC limit 1");
						$value->entilited_days  =  $entil[0]->days_vl_expat;
					}



					if($value->leave_type == "Suspension" and $value->title != null){
						$final_data[] = [	
										'employee_id' => $value->empx,	
										'employee_name' => $value->name,
										'LeaveType' => $value->leave_type.' ('.$value->title.')',
										'DaysEntitled' => $value->days,
										'DaysRemaining' => abs($value->days - $value->balance_leaves),
										'AvailedDays' => $value->balance_leaves,
										'lastDate' => $explode[0],
										'NumberOfAvailment' => $value->taken,
										'ValidUntil' => $year,
										'days_val' => $value->days,
										'balance_leaves' => $value->balance_leaves
										
									];
					}else{
						if($value->leave_type == "Suspension" and $value->title == null){
							unset($result[$key]);
						}else{
						$final_data[] = [	
										'employee_id' => $value->empx,	
										'employee_name' => $value->name,
										'LeaveType' => $value->leave_type,
										'DaysEntitled' => $value->entilited_days,
										'DaysRemaining' => abs($value->days - $value->balance_leaves),
										'AvailedDays' => $value->entilited_days - ($value->days - $value->balance_leaves),
										'lastDate' => $explode[0],
										'NumberOfAvailment' => $value->taken,
										'ValidUntil' => $year,
										'days_val' => $value->days,
										'balance_leaves' => $value->balance_leaves
									];
						}
					}
				}

				if(isset($final_data) != null){
					$message = ( !isset($final_data) || $final_data  == null ? 'data not found' : 'success load searching data' );
					return \Response::json(['header'=>['message'=>$message,'status'=>200],'data'=>$final_data],200);
				}else{
					$final_data = [];
					$message = ( !isset($final_data) || $final_data  == null ? 'data not found' : 'success load searching data' );
					return \Response::json(['header'=>['message'=>$message,'status'=>200],'data'=>[]],200);	
				}
			}
			
			//$result = \DB::SELECT($base_db);
			
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
	}
}