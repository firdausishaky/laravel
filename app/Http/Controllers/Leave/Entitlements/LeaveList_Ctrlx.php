<?php namespace Larasite\Http\Controllers\Leave\Entitlements;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\Attendance\Request\ScheduleRequestList_model as requestList;
use Larasite\Model\Leave\Entitlements\EmpEntitliments_Model;
use Larasite\Model\Leave\Entitlements\LeaveList_Model;
use Larasite\Library\FuncAccess;

class LeaveList_Ctrl extends Controller {

	protected  $form = "71";




	public function viewEmp()
	{
			/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
			if($access[1] == 200){
				$empEntitlements = new EmpEntitliments_Model;
				$input = \Input::all();
				$name = \Input::get('name');
				$rule = [
						'name' => 'required|Regex:/^[A-Za-z]+$/'
						];
				$validation = \Validator::make($input,$rule);
				if($validation->fails()){
					$check = $validation->errors()->all();
					return $empEntitlements->getMessage('input format invalidate',$check,500, $access[3]);
				}else{
					$query = \DB::SELECT("CALL Search_emp_by_name('$name')");
					return $empEntitlements->index($query,$access[3]);
				}
			}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
				return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
			}
	}

	public function index()
	{

		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			//return "test";
			$model = new requestList;
			// get emplloyee id from from API key
			$decode = base64_decode(\Request::get('key'));
			$empID = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
			// get employee name
			$empName = $model->getName($empID);
			($empName != null ? $empName = $empName[0]->name : $empName = "tes");
			// check employee role
			$checkRole = $model->checkRole($empID);

			$department =  \DB::select("select * from department where id <> 1"); //$model->departmentList($empID);
			$request = \DB::SELECT("select id,leave_type from leave_type");

			$empDepartment =  \DB::select("select * from department where id <> 1"); //$model->getEmpDepartment($empID);

			$from = date('Y-m-d', strtotime(date('Y-m-d')."-1 month"));
			$to = date('Y-m-d',strtotime($from."+1 year"));
			$temp = [
				'employee_name' => $empName,
				'employee_id' => $empID,
				'employee_role' => $checkRole,
				'emp_department'=>$empDepartment,
				'from' => $from,
				'to' => $to,
				'pending' => "1",
				'data_department' => $department,
				'LeaveType' => array_merge($request,[["id" => 0, "leave_type" => "All"]])
			];
			if($access[3]['create'] == 0 && $access[3]['read']  == 0 && $access[3]['update'] == 0  && $access[3]['delete'] == 0){
				return \Response::json(['header'=>['message'=>'Unauthorized','status'=>'200', 'access'=>$access[3]],'data'=>[]],200);
			}else{
				return \Response::json(['header'=>['message'=>'Show record data','status'=>'200', 'access'=>$access[3]],'data'=>$temp],200);
			}
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}


	public function search($employee = null, $from = null, $to = null, $pending = null,$rejected = null, $canceled = null, $key = null){
	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	if($access[1] == 200){

		$leave_ex = new LeaveList_Model;
		$i = \Input::all();
		$from = \Input::get('from');
		$to = \Input::get('to');
		if($canceled == null){
			$canceled = \Input::get('canceled');	
		}
		$all = \Input::get('all');
		if($pending == null){
			$pending = \Input::get('pending');
		}
		if($rejected == null){
			$rejected = \Input::get('rejected');
		}
		$schedule = \Input::get('schedule');
		$include = \Input::get('include');



		if($employee == null){
			$empl = \Input::get('employees');
			if(isset($empl)){
				$employee = \Input::get('employees');
			}else{
				$employee = \Input::get('employee_name');
			}
		}
	
		$key = \Input::get('key');
		$encode = base64_decode($key);
		$explode = explode("-", $encode);
		$explode_ex = $explode[1];
		$data = $leave_ex->checkRole($explode_ex);
		$data_hr = $leave_ex->hr($explode_ex);
		if($data == "SuperVisor"){
			$employee_id =$explode_ex;
		}else{
			$employee_id = $explode_ex;
		}

		$leave 	= \Input::get('type');
		$department = \Input::get('department');
		if(gettype($department) == "object"){
				$department = $department['id'];
		}
		$type = \Input::get('type');
		if(gettype($type) == "object"){
			$type = $type['id'];
		}

		$request = "";
		//create wrapper leave status
		$arr = ["canceled" => 4 ,"rejected" => 3,"schedule" => 2,"pending" => 1, 'all' => 0];
		$option = [];
		foreach ($arr as $key => $value) {
			if(isset($i[$key]) and $i[$key] == 1){
				if($key  ==  'all'){
					$option = [1,2,3,4];	
				}else{
					$option[] = $value;
				}
			}
		}

		$rule =  [
					'from' => 'date|required',
					'to'   => 'date|required',
				 ];

		$validation  = \Validator::make($i,$rule);

		if($validation->fails()){
			return  response()->json(['header' => ['message' => $validation->errors(), 'status' => 500],'data' => []],500);
		}else{
			$master  = "select * from  pool_request,leave_request where  pool_request.employee_id  = '$employee' and master_type = 2 and leave_request.id = pool_request.id_req and from_ > '$i[from]' and to_ < '$i[to]'  ";
			
			if(isset($i['type'])  and $i['type']){
				$master  .= " and  type_id  = $i[type]";
			}
		    
		    $option = implode(',',$option);

			if($option != null){
				$master  .= " and  leave_request.status_id in($option)";
			}
			
			$master_d  = \DB::SELECT($master);
			$get_final = [];
			// if(isset($i['department']) && $i['department']  !=  null){
			// 	$department  = \DB::SELECT("select * from department,emp  where   employee_id =  '$employee'  and  emp.department = department.id and department = '$i[department]' ");

				// if($department != null){
				// 	foreach ($master_d as $key => $value) {
				// 		$uid  =   $value->id_req; 
				// 		$get_final[]  = \DB::SELECT("select 
				// 			leave_request.add_on,
				// 			leave_request.approval,
				// 			leave_request.approver,
				// 			leave_request.balance_day,
				// 			leave_request.created_at,
				// 			leave_request.day_off,
				// 			leave_request.dayoff_between,
				// 			leave_request.employee_id,
				// 			leave_request.from_,
				// 			leave_request.id,
				// 			leave_type.leave_type,							
				// 			leave_request.status_id,
				// 			leave_request.taken_day,
				// 			leave_request.from_,
				// 			leave_request.to_,
				// 			2 as master_type,
				// 			concat(leave_request.from_,' - ',leave_request.to_) as schedule,
				// 			leave_request.year,
				// 			att_status.status,
				// 			concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as EmployeeName
				// 			from leave_request,att_status,leave_type,emp where leave_request.leave_type =  leave_type.id and emp.employee_id  = leave_request.employee_id and leave_request.id  =  $uid and  att_status.status_id = leave_request.status_id")[0];  
				// 		$get_final[] = ['master_type' => 2];

				// 	}

				// 	return response()->json(['header' =>  ['message' => 'Success searching data', 'status' => 200],'data' => $get_final],200);
				// }else{		 
				// 	return response()->json(['header' =>  ['message' => 'Department or Leave type for that employee not found', 'status' => 500],'data' => []],500);
				// }
			//}else{
				foreach ($master_d as $key => $value) {
					$uid  =   $value->id_req; 
					$get_final[]  = \DB::SELECT("select 
											   leave_request.add_on,
											   emp.department,
											   leave_request.approval,
											   leave_request.approver,
											   if(lower(leave_type.leave_type) = 'maternity leave',0,if(leave_request.leave_type in (2,3,4,6,7,8,11,12),(leave_request.balance_day + day_off),leave_request.balance_day)) as balance_day,
											   leave_request.created_at,
											   leave_request.day_off,
											   leave_request.dayoff_between,
											   leave_request.employee_id,
											   leave_request.from_,
											   leave_request.id,
											   leave_type.leave_type,							
											   leave_request.status_id,
											   leave_request.taken_day,
											   leave_request.from_,
											   leave_request.to_,
											   leave_request.year,
											   att_status.status,
											   2 as master_type,
											   concat(leave_request.from_,' - ',leave_request.to_) as schedule,
											   concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as EmployeeName
											   from leave_request,att_status,leave_type,emp where leave_request.leave_type =  leave_type.id and emp.employee_id  = leave_request.employee_id and leave_request.id  =  $uid and  att_status.status_id = leave_request.status_id ")[0];  
	
				}

				if(isset($i['department']) && $i['department']  !=  null){
					foreach ($get_final as $key => $value) {
						if($value->department  != $i['department']){
							unset($get_final[$key]);
						}

						if($value->leave_type  == "Emergency Leave"){
							$value->balance_day =  '0';
						}
					}
				}else{
					foreach ($get_final as $key => $value) {
						if($value->leave_type  == "Emergency Leave"){
							$value->balance_day =  '0';
						}
					}
				}

				foreach ($get_final as $key => $value) {
				 if($value->approver  ==   2){
				 	$value->status = 'Approval';
				 }
				}

					


			//}
				return response()->json(['header' =>  ['message' => 'Success searching data', 'status' => 200],'data' => $get_final],200);				
		}




		
	

		

		
			//return response()->json(['header'=>['message'=>$message,'status'=>200],'data'=>$data_fin],200);
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
	}



	public function searchx($employee = null, $from = null, $to = null, $pending = null,$rejected = null, $canceled = null, $key = null){
	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	if($access[1] == 200){

		$leave_ex = new LeaveList_Model;

		$input = \Input::all();
		$from = \Input::get('from');
		$to = \Input::get('to');
		if($canceled == null){
			$canceled = \Input::get('canceled');	
		}
		$all = \Input::get('all');
		if($pending == null){
			$pending = \Input::get('pending');
		}
		if($rejected == null){
			$rejected = \Input::get('rejected');
		}
		$schedule = \Input::get('schedule');
		$include = \Input::get('include');
		if($employee == null){
			$employee = \Input::get('employee_name');
		}
		
		$key = \Input::get('key');
		$encode = base64_decode($key);
		$explode = explode("-", $encode);
		$explode_ex = $explode[1];
		$data = $leave_ex->checkRole($explode_ex);
		$data_hr = $leave_ex->hr($explode_ex);
		if($data == "SuperVisor"){
			$employee_id =$explode_ex;
		}else{
			$employee_id = $explode_ex;
		}

		$leave 	= \Input::get('type');
		$department = \Input::get('department');
		if(gettype($department) == "object"){
				$department = $department['id'];
		}
		$type = \Input::get('type');
		if(gettype($type) == "object"){
			$type = $type['id'];
		}

		$request = "";

		//create wrapper leave status
		$arr = ["canceled","rejected","schedule","pending","taken"];
		if(isset($all) && $all ==  "1"){
				$request =  "1,2,3,4";
		}else{
			foreach ($arr as $key => $value) {
				if(isset($$arr[$key]) && $$arr[$key] !=  "0"){
					if($request == '')
					{
						$request = ($key == 0 ? "4" : ( $key == 1 ? "3" : ( $key == 2 ? "2" : ( $key == 3 ? "1" : ""))));
					}else{
						$request = ($key == 0 ? $request .=",4" : ( $key == 1 ? $request .=",3" : ( $key == 2 ? $request .=",2" : ( $key == 3 ? $request .=",1" : $request .=",1"))));
					}
				}
			}
		}


		//create possibilty database in one way;
		// $text_status = " t1.status_id IN ($request)";
		// $text_employeeid = " and t1.employee_id='$employee'";
		// $text_department = " and t3.department=$department";
		// $leave_type      = " and t1.leave_type=$type";
		// $text_db = "select concat(t1.from_,' to ',t1.to_)as Date,
		// 			concat(t3.first_name,' ',t3.middle_name,' ',t3.last_name)as EmployeeName,
		// 			t1.id, 
		// 			t3.employee_id,t4.leave_type,
		// 			(t1.balance_day-sum(t1.taken_day))as LeaveBalance,
		// 			t1.taken_day as NumberOfDay,
		// 			t5.status
		// 			from leave_request t1 left join emp t3 on t3.employee_id=t1.employee_id left join leave_type t4 on t4.id=t1.leave_type left join att_status t5 on t5.status_id=t1.status_id
		// 			where";
		// $group = " group by t1.leave_type;";

		$filter = [];
		$text_db = \DB::SELECT("call view_all_leave_balance()");
		if(isset($text_db)){
			$explode = explode(",",$request);
			foreach ($explode as $key => $value) {
				foreach ($text_db as $keys => $values) {
					$a = (int)$values->status;
					$b = (int)$value;
					if($a == $b){
						$filter[] = $text_db[$keys];
					}

					// if($values->balance_leaves == 0 || $values->balance_leaves == null){
					// 	unset($text_db[$keys]); 
					// }
				}
			}

			foreach ($filter as $key => $value) {

				$explode = explode(" ",$value->date);
				if($explode[0] >= $from   &&  $explode[0] <= $to ){
					//nope
				}else{
					unset($filter[$key]);
				}
			}

		
			if(isset($employee) and $employee != null){
			foreach ($filter as $key => $value) {
				 	if($value->froms == null || $value->tos == null){
						unset($filter[$key]);
					}
				 	if($value->empx !=  $employee ){
				 		unset($filter[$key]); 
				 	}
				}
			}


			
			if(isset($type) && $type != null){
				$retVal =   ($type == 1 ? 'Birthday Leave' : 
	                        ($type == 2 ? 'Vacation Leave' : (
	                            $type == 3 ? 'Enhance Vacation Leave' : (
	                                $type == 4 ? 'Sick Leave' : (
	                                    $type == 5 ? "Maternity Leave": (
	                                    $type ==  6 ? 'Paternity Leave' : (
	                                $type ==  7? 'Bereavement Leave' : (
	                            $type == 8 ? 'Marriage Leave' : (
	                        $type == 9 ? 'offday_oncall' : (
	                        $type == 12 ? 'Suspension' : (	
	                    $type == 10 ? 'Accumulation Day offday_oncall' : 'Emergency Leave')))))))))));
				

				foreach ($filter as $key => $value) {
					if($type != "0"){
						if($value->leave_type != $retVal){
							unset($filter[$key]);
						}
					}
				}
			}
			
			
			if(isset($department) && $department != ""){
				foreach ($filter as $key => $value) {
					$emo = $value->empx;
					$emp = \DB::select("SELECT * FROM emp where employee_id = '$emo' and department =  $department ");
					if($emp == null || $emp == []){
						unset($filter[$key]);
					}
				}
			}
			
			$data = $leave_ex->checkRole($employee);
			if($data == "SuperVisor" ){
				$data_ex="SuperVisor";
			}else{
				$data_ex="Employee";
			}

			$data_final =  array_values(array_filter($filter));
			if($data_final == []){
				$message = "Data empty";
				$data_end = [];

			}else{
				$message = "Success load data";
				$status = ($value->status == 4 ? "Canceled" : ($value->status == 3 ? "Rejected" : ($value->status == 2 ? "Approval" : "Pending Approval")));
				foreach ($data_final as $key => $value) {
				$date =  $value->froms.' - '.$value->tos;
				$status = ($value->status == 4 ? "Canceled" : ($value->status == 3 ? "Rejected" : ($value->status == 2 ? "Approval" : "Pending Approval")));
				$data_end[] = 
					  [ "Date" => $date,
						"EmployeeName" => $value->name,
						"Employee_id" => $value->empx,
						"LeaveBalance" => $value->days,
						"NumberOfDay" => $value->balance_leaves,
						"leave_type" => $value->leave_type,
						"status" => $status,
						"id" => $value->ids,
						"permission" => $data_ex
					 ];
				}
			}

		
			
			return response()->json(['header'=>['message'=>$message,'status'=>200],'data'=>$data_end],200);
		}elseif($message != 'ok'){
			return \Response::json(['header'=>['message'=>$message,'status'=>201],'data'=>[]],201);
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

		//possibilty
		// $pos = ['request','employee','depart',"type"];
		// $posin = ['text_status','text_employeeid','text_department',"leave_type"];
		// foreach ($pos as $key => $value) {
		// 	if(isset($$value) && $$value != null){
		// 		$text_db .= $$posin[$key];
		// 	}
		// }
		// $text_db .= $group;	
		// $db = \DB::SELECT($text_db);
		// // $decode = base64_decode(\Request::get('key'));
		// // $empID = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
		// $data = $leave_ex->checkRole($employee);
		// if($data == "SuperVisor" ){
		// 	$data_ex="SuperVisor";
		// }else{
		// 	$data_ex="Employee";
		// }
		// return $db;
		// foreach ($db as $key => $value) {
		// 	$data_fin[] = [ "Date" => $value->Date,
		// 			"EmployeeName" => $value->EmployeeName,
		// 			"Employee_id" => $value->employee_id,
		// 			"LeaveBalance" => $value->LeaveBalance,
		// 			"NumberOfDay" => $value->NumberOfDay,
		// 			"leave_type" => $value->leave_type,
		// 			"status" => $value->status,
		// 			"id" => $value->id,
		// 			"permission" => $data_ex
		// 			];
		// }

		// $message = "success";
		// $data_fin = (!isset($data_fin) || $data_fin == null ? [] : $data_fin);
		// $message = ( $data_fin == [] ? "data not found" : "success");
		// $data = $data_fin;
		// $status = 200;
			return response()->json(['header'=>['message'=>$message,'status'=>200],'data'=>$data_fin],200);
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
	}

	public function approve($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$check = \DB::SELECT("select * from leave_request where id = $id");
			if(isset($check[0]->leave_type) && $check[0]->leave_type == 10){
				$shift = $check[0]->comment;
				$date_start =  $check[0]->from_;
				$date_new = $check[0]->to_;
				$get_shift = \DB::SELECT("select * from att_schedule  where date =  '$date_start' ");
				$old_shift =  $get_shift[0]->schedule_id;
				$schedule  = \DB::SELECT("update att_schedule set shift_id =  $shift where date = '$date_start' ");
				$new_schedule = \Db::SELECT("update att_schedule set shift_id = 13   where date =  '$date_new' ");
				$data =  \DB::SELECT("CALL Update_Leave_request($id,2,'$approver','$employee_name')");
				if(isset($type) && $type == 2){
					return  $this-> leave_request_detail($id,$from,$to,null);
				}else{
					return  $this->search($employees,$from,$to,1,null,null,$key);		
				}
			}
			if(isset($check[0]->leave_type) && $check[0]->leave_type == 9){
				$schedule  = \DB::SELECT("update att_schedule set shift_id =  $shift where date = '$date_start' ");
				$data =  \DB::SELECT("CALL Update_Leave_request($id,2,'$approver','$employee_name')");
				if(isset($type) && $type == 2){
					return  $this-> leave_request_detail($id,$from,$to,null);
				}else{
					return  $this->search($employees,$from,$to,1,null,null,$key);		
				}	
			}
			$empEntitlements = new EmpEntitliments_Model;
			$leave_ex = new LeaveList_Model;


			$key = \Input::get('key');
			$employee_name = \Input::get('employee_name');
			$employees = \Input::get('employees');
			$encode = explode('-',base64_decode($key));
			$approver =  $encode[1];

			if($id != null){

				$data =  \DB::SELECT("CALL Update_Leave_request($id,2,'$approver','$employee_name')");
				$from = \Input::get('from');
				$to = \Input::get('to');
				$key =  \Input::get('key');
				$type =  \Input::get('type');
				if(isset($type) && $type == 2){
					return  $this-> leave_request_detail($id,$from,$to,null);
				}else{
					return  $this->search($employees,$from,$to,1,null,null,$key);		
				}
			
				
			}else{
				return $empEntitlements->getMessage('input format invalidate',$check,500, $access[3]);
			}


			//return $leave->index($data_ex,$access[3]);
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

	}

	public function reject($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$empEntitlements = new EmpEntitliments_Model;
			$leave_ex = new LeaveList_Model;


			$key = \Input::get('key');
			$employee_name = \Input::get('Employee_id');
			$encode = explode('-',base64_decode($key));
			$approver =  $encode[1];

			if($id != null){

				$data =  \DB::SELECT("CALL Update_Leave_request($id,3,'$employee_name','$approver')");
				$from = \Input::get('from');
				$to = \Input::get('to');
				$employees = \Input::get('employees');
				$key =  \Input::get('key');
				$type =  \Input::get('type');
				if(isset($type) && $type == 3){
					return  $this->leave_request_detail($id,$from,$to,null);
				}else{
					return  $this->search($employees,$from,$to,1,null,null,$key);		
				}
				// $datax = $this->leave_request_detail($approver,$from,$all,$id);
				// if(isset($datax) && $datax != null){
				// 	return $empEntitlements->getMessage('success',$data,200, $access[3]);
				// }else{
				// 	return $empEntitlements->getMessage('success',[],200, $access[3]);
				// }
			}else{
				return $empEntitlements->getMessage('input format invalidate',$check,500, $access[3]);
			}


			//return $leave->index($data_ex,$access[3]);
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

	}

	public function canceled($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$empEntitlements = new EmpEntitliments_Model;
			$leave_ex = new LeaveList_Model;


			$key = \Input::get('key');
			$employee_name = \Input::get('Employee_id');
			$encode = explode('-',base64_decode($key));
			$approver =  $encode[1];

			if($id != null){

				$data =  \DB::SELECT("CALL Update_Leave_request($id,4,'$employee_name','$approver')");
				$from = \Input::get('from');
				$to = \Input::get('to');
				$employees = \Input::get('employees');
				$key =  \Input::get('key');
				$type =  \Input::get('type');
				if(isset($type) && $type == 4){
					return  $this->leave_request_detail($id,$from,$to,null);
				}else{
					return  $this->search($employees,$from,$to,1,null,null,$key);		
				}
				// $data = $this-> leave_request_detail($id ,$from,$to ,$name = \Input::get('Employee_id'));
				// if(isset($data)){
				// 	return $empEntitlements->getMessage('success',[],200, $access[3]);
				// }

			}else{
				return $empEntitlements->getMessage('input format invalidate',$check,500, $access[3]);
			}


			//return $leave->index($data_ex,$access[3]);
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

	}

	function leave_request_detail($id,$date1 = null,$date2 = null ,$name = null){
		// id => leave_request, employee_id => leave_request, date => leave_request
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$empEntitlements = new EmpEntitliments_Model;
			$leave_ex = new LeaveList_Model;
			$input = \Input::all();
			if($id != null){
				$date = \Input::get('Date');
				$explode = explode("-",$date);
				if($date != null){$date1 = str_replace( "/", "-",$explode[0]);}
				if($date != null){$date2 = str_replace( "/", "-",$explode[1]);}
				if($name == null){$name = \Input::get('name');}


				
				/**
				 * get data rpom table leave_request
				 * @var [type]
				 */
				$db  = \DB::SELECT("select leave_request.add_on,
							   leave_request.approval,
							   leave_request.approver,
							   (leave_request.balance_day  +  leave_request.day_off) as balance_day,
							   leave_request.created_at,
							   leave_request.day_off,
							   leave_request.dayoff_between,
							   leave_request.employee_id,
							   leave_request.from_,
							   leave_request.id,
							   leave_type.leave_type,							
							   leave_request.status_id,
							   leave_request.taken_day,
							   leave_request.from_,
							   leave_request.to_,
							   leave_request.year,
							   att_status.status,
							   concat(leave_request.from_,' - ',leave_request.to_) as schedule,
							   concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as EmployeeName
							   from leave_request,att_status,leave_type,emp where leave_request.leave_type =  leave_type.id and emp.employee_id  = leave_request.employee_id and leave_request.id  =  $id and  att_status.status_id = leave_request.status_id ");
				
				/**
				 * get dtaa comment by id leave requet
				 * @var [type]
				 */
				$db_com = \DB::SELECT("select command_center.created_at ,command_center.comment,command_center.filename,command_center.path,concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name)as employee_id from command_center,emp  where command_center.request_id = $id and command_center.employee_id=emp.employee_id");
				if($db_com == null){$db_com = null;}
				$count = count($db);
				
				/**
				 * count 1 berarti gak ada ado
				 * @var [type]
				 */
				if($count == 1 ){		

					// extract get data from db	
					foreach($db as $key => $value){
						$loop  =  $value;
						
						// check data dari  db udah di approval atau belum  kalo dah lanjut
						if($value->approval != null and $value->approval != 0){

							//dapetin data json  dari id request dari table pool_request;
							$json  = \Db::SELECT("select * from pool_request  where id_req = $id and master_type =2")[0]->json_data;
							
							$json = json_decode($json,1);

							//dapetin alur approval dari jsondat dari properti  ['req_flow']
							$flow = $json['req_flow'];
							
							foreach ($flow as $key => $value) {

								//urutan approval  1,2,3  lanjut...
								if($value ==  1){
									
									// $value urutan usernya dapet dari  $key bentuk asli  ['hr' => 1, 'sup' => 2, 'emp' =>3, ...]
									$stat =  $key;


									//check dari bentuk json dari  $stat udah di approve blum kalo bloman 'o'  kalo udah 'x'
									if($json['req_flow'][$key.'_approve'] == 'x'){
										foreach ($json[$key] as $key => $value) {
												
											//check json_data misal approval  flow  1 = hr =>   [cari hr_stat  ==  1] ambil  siapa
											//
											
											if($value[$stat.'_stat'] == 1){
												foreach ($value as $keyx => $valuex) {
													if(is_numeric($keyx)){
														$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
													}
												}
											}
										}
									}
								}
							}

							if(isset($stat) and isset($select_emp)){
								$approval_stat = $stat;
								$approval_name =  $select_emp;
							}
						}else{
							$approval_stat  =  null;
							$approval_name =  null;
						}


						
						if($loop->approver != null and $loop->approver != 0){

							//dapetin data json  dari id request dari table pool_request;
							$json  = \Db::SELECT("select * from pool_request  where id_req = $id and master_type =2")[0]->json_data;
							
							$json = json_decode($json,1);

							//dapetin alur approval dari jsondat dari properti  ['req_flow']
							$flow = $json['req_flow'];
							
							foreach ($flow as $key => $value) {

								//urutan approval  1,2,3  lanjut...
								if($value ==  2){
									
									// $value urutan usernya dapet dari  $key bentuk asli  ['hr' => 1, 'sup' => 2, 'emp' =>3, ...]
									$stat =  $key;


									//check dari bentuk json dari  $stat udah di approve blum kalo bloman 'o'  kalo udah 'x'
									if($json['req_flow'][$key.'_approve'] == 'x'){
										foreach ($json[$key] as $key => $value) {
												
											//check json_data misal approval  flow  1 = hr =>   [cari hr_stat  ==  1] ambil  siapa
											//
											
											if($value[$stat.'_stat'] == 1){
												foreach ($value as $keyx => $valuex) {
													if(is_numeric($keyx)){
														$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
													}
												}
											}
										}
									}
								}
							}

							if(isset($stat) and isset($select_emp)){
								$approver_stat  = $stat;
								$approver_name =  $select_emp;
							}else{

							}
						}else{
							$approver_stat  = NULL;
							$approver_name =  NULL;
						}


						$select  =  \DB::SELECT("select * from  pool_request where id_req =  $id and master_type =  2 ")[0]->json_data;



						$select  = json_decode($select,1);
						
						foreach ($select['req_flow'] as $key => $value) {
							//return $select;
							if($value == 2 and $select['req_flow'][$key.'_approve'] == 'x'){	
							  $check_id = $select['action_2']['employee_id'];
						      $approver_name =  \DB::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as  name from  emp where employee_id = '$check_id' ")[0]->name;
						      $approver_stat  = \DB::SELECT("select * from view_nonactive_login where employee_id  = '$check_id' ")[0]->role_name; 
						      //$act = $select['action_2']['']

								//$approver_name =   
							}else{
								if($value == 1 and $select['req_flow'][$key.'_approve'] == 'x'){
									$check_id = $select['action_1']['employee_id'];
									$approval_name =  \DB::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as  name  from  emp where employee_id = '$check_id' ")[0]->name;
						      		$approval_stat  = \DB::SELECT("select * from view_nonactive_login where employee_id  = '$check_id' ")[0]->role_name; 
								}
							}
						}

						if(!isset($approver_name) and !isset($approver_stat)){
							$approver_name = null;
							$approver_stat  = null;
						}

						if(!isset($approval_name) and !isset($approval_stat)){
							$approval_name  = null;
							$approval_stat =  null;
						}

						if($loop->leave_type == "Emergency Leave"){
							$loop->balance_day = '-';

							
						}
						//if($select['req_flow'])

						// [$approval_name,$approval_stat,$approver_name,$approver_stat];

						$date_create   =  explode(' ',$loop->created_at);

						$db_core = [
							"Date_table" => $loop->from_.'-'.$loop->to_,
							"LeaveBalance" => $loop->balance_day,
							"taken_day" => $loop->taken_day,
							"RequestBy"=>$loop->EmployeeName,
							"Time"=> $loop->created_at,
							"master_type" => 2,
							"create_date" => $date_create[0],
							"approval" => $approval_name.' ('.strtoupper($approval_stat).')',
							"approver" => $approver_name.' ('.strtoupper($approver_stat).')',
							"dayoff_between"=> $loop->day_off,
							"request_time"=> $loop->created_at,
							"employee_id"=> $loop->employee_id,
							"id" => $loop->id,
							"leave_type" => $loop->leave_type,
							"status"=>  $loop->status,
							"number"=> $loop->taken_day
							];
					}

					
					$daa = array_merge($db_core,["comment" => $db_com]);
					return $empEntitlements->index($daa,$access[3]);

				}

				// else{
				// 	$emp = "";
				// 	foreach ($db as $key => $value) {
				// 		$emp = $key;
				// 		$string = trim(preg_replace('/\s+/','_',$value->name_ado));
				// 		$data_sub[$value->employee_id][]= [
				// 				 "name_ado_$string"=> $value->name_ado,
   	// 							 "date_ado_$string"=> $value->date_ado,
   	// 							 "NewSchedule_$string"=> $value->NewSchedule,
				// 				];


				// 		$data_master[$value->employee_id] = [
				// 				 "id"=> $value->id,
    //                      		 "employee_id"=> $value->employee_id,
				// 			     "leave_type"=> $value->leave_type,
    //                      		 "taken_day"=> $value->taken_day,
    //                      	     "request_time"=> $value->request_time,
    //                     		 "dayoff_between"=> $value->dayoff_between,
    //                      	     "LeaveBalance"=> $value->LeaveBalance,
    //                      	     "status"=> $value->status,
    //                              "RequestBy"=> $value->RequestBy,
    //                      		 "approval"=> $value->approval,
	   //  						 "Date"=> $value->Date,
	   // 						     "Time"=> $value->Time,
	   //  					     "approver"=> $value->approver
				// 				];

				// 	}


					// if(isset($data_sub)){
					// 	foreach ($data_sub as $key => $value) {
					// 		$data_sub_sub[] = $value;
					// 	}
					// 	$test = $data_sub_sub[0];
					// 	$arr = [];
					// 	foreach ($test as $key => $value) {
					// 		$arr = array_merge($arr, $test[$key]);
					// 	}
					// }

					// foreach ($data_master as $key => $value) {
					// 	foreach ($value as $key => $value_data) {
					// 		$data_end = $value;
					// 		break;
					// 	}
					// }

					// $data_core = array_merge($data_end,$arr);
					// return $empEntitlements->index($data_core,$access[3]);
				}

				// $dbx = DB::SELECT("select leave_request.approval,att_status.status,leave_request.created_at, leave_request.approver from leave_request,att_status
				// where leave_request.employee_id='$name' and att_status.status_id=leave_request.status_id  ");
				// $status_emp = $leave_ex->checkRole($id);
				// $data_ex = [
				// 						"view" => [$data],
				// 						"data" => $dbx,
				// 						"status" => $status_emp
				// 					 ];
			// }else{
			// 	return $empEntitlements->getMessage('data id missing',[],500, $access[3]);
			// 	return $empEntitlements->index($query,$access[3]);
			// }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}

	}


//$strore = \DB::SELECT("CALL insert_emp_emergency('$input[id]','$input[name]','$input[address]','$input[city]','$input[state]',$input[zip],'$input[country]','$input[relationship]','$input[home_telephone]','$input[mobile_telephone]','$input[work_telephone]' )");
public function getfile($id){
	if(isset($id)){
		$data = \DB::SELECT("select path from leave_request_comment where filename='$id' ");
		 $path = $data[0]->path;
		$path = storage_path($path.'/'.$id);
		$file = \File::get($path);
		$type = \File::mimeType($path);
		return \Response::make($file,200,['Content-Type'=>$type]);
	}else{
		return \Response::json(['header'=>['message'=>"Failed to get image data",'status'=>401]],401);
	}
}


public function save_request($id){
	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	if($access[1] == 200){
	//$model = new RequestDetail_Model;

		$key = \Input::get("key");
		$explode = explode("-",base64_decode($key));
		$empID = $explode[1];
		$json = \Input::get('data');
		if(isset($json)){
			$json = json_decode($json,1);
			if(!isset($json['newcomment']) ){
				return response()->json(["header" => ["message" => "Failed , please insert comment before submit data", "status" => 500],"data" =>[] ],500);
			}else{
				$comment = $json['newcomment'];
			}
		}else{
			$comment = \Input::get("newcomment");
			if(!isset($comment) ){
				return response()->json(["header" => ["message" => "Failed , please insert comment before submit data", "status" => 500],"data" =>[] ],500);
			}else{
				$comment = \Input::get("newcomment");
			}
		}

		if(!isset($comment) ){
			return response()->json(["header" => ["message" => "Failed , please insert comment before submit data", "status" => 500],"data" =>[] ],500);
		}

		//$input = \Input::json("data");
		$file = \Input::file("file");
		if(isset($file) && $file != "undefined"){
				$imageName = $file->getClientOriginalName();
				$ext = $file->getClientOriginalExtension();
				$size = $file->getSize();

				//if($ext == "jpg" || $ext == "png" || $ext == "pdf" ){
					$type_id  = $json['leave_type'];
					$type_id   =  \db::select("select * from leave_type  where  leave_type = '$json[leave_type]' ")[0]->id;

					$path = "hrms_upload/schedule";
					$rename = "Reply_".str_random(6).$ext;

					$file->move(storage_path($path),$rename);
					$db = \DB::SELECT("insert into command_center(request_id,comment,employee_id,path,filename,created_at,type_id,master_type) value($id,'$comment','$empID','$path','$rename',now(),$type_id,2)");
					$feed = \DB::SELECT("select command_center.created_at ,command_center.comment,command_center.filename,command_center.path,concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name)as employee_id from command_center,emp  where command_center.request_id=$id and command_center.employee_id=emp.employee_id ");
					return response()->json(["header" => ["message" => "Success, send data", "status" => 200, "access" => $access[3]],"data" =>$feed ],200);

				// }else{
				// 	return response()->json(["header" => ["message" => "Failed , format image not supported", "status" => 500],"data" =>null ],500);
				// }
		}else{
			$type_id  = $json['leave_type'];
			$type_id   =  \db::select("select * from leave_type  where  leave_type = '$json[leave_type]' ")[0]->id;
			$path = " ";
			$rename = " ";
			$db = \DB::SELECT("insert into command_center(request_id,comment,employee_id,path,filename,created_at,type_id,master_type) value($id,'$comment','$empID','$path','$rename',now(),$type_id,2) ");
			$feed = \DB::SELECT("select leave_request_comment.created_at ,leave_request_comment.comment,leave_request_comment.filename,leave_request_comment.path,concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name)as employee_id from leave_request_comment,emp  where leave_request_comment.leave_request_id=23 and leave_request_comment.employee_id=emp.employee_id");
			return response()->json(["header" => ["message" => "Success, send data", "status" => 200, "access" => $access[3]],"data" =>$feed ],200);
		}
	}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
}
