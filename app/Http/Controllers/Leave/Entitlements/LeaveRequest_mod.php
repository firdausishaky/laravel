<?php namespace Larasite\Http\Controllers\Leave\Entitlements;
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;
use Larasite\Model\Leave\Entitlements\Leaverequest_Ctrl_term ;
use Exeception;
use DateTime;

class Leaverequest_Ctrl extends Controller {

	protected $form = "72";

    private  $rule_ado = [
                'date1'   => 'required|date',
                'date2'   => 'required|date',
                'date3'   => 'required|date',
                'date4'   => 'required|date',
                'ado1'    => 'required|integer',
                'ado2'    => 'required|integer',
                'ado3'    => 'required|integer',
                'ado4'    => 'required|integer',
                'nextday' => 'required|date'
              ];

    public function insert_ado($data,$type_id,$name){
      $employee_id = $data['employee_id'];
      $ado1 =  (isset($data['ado1']) && $data['ado1'] != null ? $data['ado1'] : 0 );
      $ado2 =  (isset($data['ado2']) && $data['ado2'] != null ? $data['ado2'] : 0 );
      $ado3 =  (isset($data['ado3']) && $data['ado3'] != null ? $data['ado3'] : 0 );
      $ado4 =  (isset($data['ado4']) && $data['ado4'] != null ? $data['ado4'] : 0 ); 

      $date1 =  (isset($data['date1']) && $data['date1'] != null ? $data['date1'] : 0 );
      $date2 =  (isset($data['date2']) && $data['date2'] != null ? $data['date2'] : 0 );
      $date3 =  (isset($data['date3']) && $data['date3'] != null ? $data['date3'] : 0 );
      $date4 =  (isset($data['date4']) && $data['date4'] != null ? $data['date4'] : 0 );       

      $datev1 =  (isset($data['datev1']) && $data['datev1'] != null ? $data['datev1'] : 0 );
      $datev2 =  (isset($data['datev2']) && $data['datev2'] != null ? $data['datev2'] : 0 );
      $datev3 =  (isset($data['datev3']) && $data['datev3'] != null ? $data['datev3'] : 0 );
      $datev4 =  (isset($data['datev4']) && $data['datev4'] != null ? $data['datev4'] : 0 ); 
    
       try{
          if($date1 != 0 && $datev1 != 0 && $ado1 != 0){
            \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,comment) values('$employee_id',10,'$date1','$datev1','$ado1')");
          }
          if($date2 != 0 && $datev2 != 0 && $ado2 != 0){
            \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,comment) values('$employee_id',10,'$date2','$datev2','$ado2')");
          }
          if($date3 != 0 && $datev3 != 0 && $ado3 != 0){
            \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,comment) values('$employee_id',10,'$date3','$datev3','$ado3')");
          }
          if($date4 != 0 && $datev4 != 0 && $ado4 != 0){
            \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,comment) values('$employee_id',10,'$date4','$datev4','$ado4')");
          }
        }catch (Exception $e) {
            return $e;
        }
    }

    public function create_rule(){
    	 	$year = date('Y');
            $request = new leaverequest_Model;
            $json = \Input::get("data");
            $file = \Input::file('file');
            $data = json_decode($json,1);

            if(isset($json)){
                if(isset($data['data']['LeaveType'])){
                    $type =  $data['data']['LeaveType'];
                    $leave_type = \DB::select("select leave_type,id from leave_type where leave_type='$type' ");
                    $type_id = $leave_type[0]->id;
                }else{
                    return $request->getMessage("please select leave type first",[],500,$access[3]);
                }

                if(isset($data['data']['name'])){
                    $name =  $data['data']['name'];
                }else{
                    $name = null;
                }

                if(isset($data['data']['remaining_day'])){
                    $remaining_day =  $data['data']['remaining_day'];
                }else{
                    $remaining_day = null;
                }
                
                if($type != "Accumulation Day Off"){
                    if(isset($data['data']['name']) && isset($data['data']['From']) && isset($data['data']['To'])){
                        $from =  $data['data']['From'];
                        $to = $data['data']['To'];
                    }else{
                        return $request->getMessage("Please fill form data before submit data",[],500,$access[3]);
                    }
                }

                if($type != "Accumulation Day Off"){
                    if(isset($data['data']['DayOffBetween'])){
                        $dob = $data['data']['DayOffBetween'];
                    }else{
                        return $request->getMessage("Day Off Between not found",[],500,$access[3]);
                    }
                }

                if(!isset($data['data']['aggred'])){
                    return $request->getMessage("please check form aggrement before submit data",[],500,$access[3]);
                }else{
                    $aggree = $data['data']['aggred'];
                }

                if(isset($data['data']['comment'])){
                    $comment = $data['data']['comment'];
                }else{
                        $comment = NULL;
                }

                if(isset($data['data']['LeaveBrea'])){
                    $breave = $data['data']['LeaveBrea'];
                }else{
                    $breave = NULL;
                }
                (isset($data['data']['IncludeAdo']) ? $ado_x = $data['data']['IncludeAdo'] : $ado_x = "No" );
            }else{
                $data = \Input::all();
                if(isset($data['LeaveType'])){
                    $type =  $data['LeaveType'];
                    $leave_type = \DB::select("select leave_type,id from leave_type where leave_type='$type' ");
                    $type_id = $leave_type[0]->id;
                }else{
                    return $request->getMessage("please choose of the item in LeaveType",[],500,$access[3]);
                }

                if(isset($data['name'])){
                  $remaining_day =  $data['remaining_day'];
                }else{
                     $remaining_day = null;
                }

                if(isset($data['name'])){
                  $name =  $data['name'];
                }else{
                  return $request->getMessage("please insert name before submit data",[],500,$access[3]);
                }

                if($type !=  "Accumulation Day Off"){
                    if(isset($data['From']) && isset($data['To']) ){
                        $from =  $data['From'];
                        $to = $data['To'];
                    }else{
                        return $request->getMessage("please fill out all data before submit data ",[],500,$access[3]);
                    }
                }


                if(isset($data['DayOffBetween'])){
                    $dob = $data['DayOffBetween'];
                }else{
                     $dob = NULL;
                }

                if(!isset($data['aggred'])){
                    return $request->getMessage("please check form aggrement before submit data",[],500,$access[3]);
                }else{
                    $aggree = $data['aggred'];
                }

                if(isset($data['comment'])){
                    $comment = $data['comment'];
                }else{
                    $comment = NULL;
                }

                if(isset($data['LeaveBrea'])){
                    $breave = $data['LeaveBrea'];
                }else{
                    $breave = NULL;
                }
                
                (isset($data['IncludeAdo']) ? $ado_x = $data['IncludeAdo'] : $ado_x = "No" );
            }

            if(isset($json)){
                    $data = json_decode($json,1);
                    $data = $data['data'];
            }
             
             if(isset($dob)){
                $dob = $dob;
                $select_emp = \DB::SELECT("select * from emp where employee_id = '$name' ");
                $lc = $select_emp[0]->local_it;
            }
             //return $type;
            if($type == "Accumulation Day Off"){
                return $this->insert_ado($data,$type_id,$name);
            }

    	if()
    }
	
	
	//for check how long  have beenyou work in
	public function chk_start_from ($emp,$type){
		 $emp =   \DB::SELECT("select * from employee_id  = '$emp' ");
		 $date  =   date(Y-m-d);

		 //Training Period , Probationary Period, contract_start, Fixed Term Contract;

		 if(!isset($emp)){
		    $data  = [] ; $status  = 500;
		 }else{
		 	$department  =   $emp[0]->department;
		 	$local_it =  $emp[0]->local_it;

		 	if($local_it != 1){
		 			$local_it  = 2;
		 	}

		 	if($type == "vacation"){
		 	  $lv =  \DB::SELECT("
		 	  		case 
		 	  		when (select entitlement_start from leave_vacation, leave_vacation_detail where  department =  $department and  applied_for = $local_it) == 'Training Periode'
		 	  		then (select training_agreement as start_date from job_history  where employee_id  = '$emp');
		 	  		when (select entitlement_start from leave_vacation, leave_vacation_detail where  department =  $department and  applied_for = $local_it) == 'Probatinary Periode'
		 	  		then (select probationary_start as start_date from job_history  where employee_id  = '$emp');
		 	  		when (select entitlement_start from leave_vacation, leave_vacation_detail where  department =  $department and  applied_for = $local_it) == 'contract_start'
		 	  		then (select contract_start_date as date_start, emp_contract_start, emp_contract_end from job_history  where employee_id  = '$emp');
		 	  		when (select entitlement_start from leave_vacation, leave_vacation_detail where  department =  $department and  applied_for = $local_it) == 'Fixed Term Contract'
		 	  		then (select fixed_term_contract from job_history  where employee_id  = '$emp');
		 	  		else
		 	  			0;
		 	  		end
		 	  		");

		 	  if($lv  ==  0){
		 	 	$data  = []; $message  =  'please fill job history before request leave';
		 	  }else{
		 	  	if(isset($lv[0]->emp_contract_start) && isset($lv[0]->emp_contract_end)){
		 	  		if($lv[0]->emp_contract_start ==  null || $lv[0]->emp_contract_end  = null){
		 	  			$data  = []; $message  = "please fill employee contract (start date & end date) in job history before submit request";
		 	  		}else{
		 	  			if(isset($lv[0]->emp_contract_end)){
		 	  				if($lv[0]->emp_contract_end < $date ){
		 	  					$data =   []; $message = 'your contract start end is expire';
		 	  				}else{
		 	  					$date1 =  date_create($date);
		 	  					$date2  = date_create($lv[0]->emp_contract_end);
		 	  					$total  = date_diff($date1,$date2);
		 	  					$total  = $total->interval(%y);	
		 	  				}

		 	  				if(isset($total)){
		 	  					$get_total  =  \DB::SELECT("select days,offday_oncall from   leave_vacation,leave_vacation_detail  where
		 	  						                        leave_vacation.id = leave_vacation.vacation_id and department  =  $department and
		 	  						                        applied_for = '$local_it' and year  = $year ");

		 	  					if(!isset($get_total) || $get_total == null){
		 	  						$data = []; $message = "no leave for your department";
		 	  					}else{

		 	  					$year =  date('Y');
		 	  					$get_used  =  \DB::SELECT("select count(taken_day) from leave_request where leave_type =  2 and employee_id = '$emp  and YEAR(created_at) = '$year' ");

		 	  					$days_left =  \DB::SELECT("");

		 	  					}





		 	  				}
		 	  			}else{

		 	  			}
		 	  		}
		 	  	}
		 	  }		

		 	}

		 	if($type ==  "sick"){

		 	}

		 	if($type == "enhance" ){

		 	}

		 	if($type == "bereavevment" ){

		 	}

		 }
	}


	public function request_remaining(){
		$request = new leaverequest_Model;
		$input   = \Input::all();
		$name    = \Input::get('name');
		$type    = \Input::get('LeaveType');
	}

}