<?php namespace Larasite\Http\Controllers\Leave\Entitlements;
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;
use Larasite\Model\Leave\Entitlements\Leaverequest_Ctrl_term ;
use Larasite\Http\Controllers\Attendance\notificationV3;
use Exeception;
use DateTime;

class LeavereMasterCtrl extends Controller {

    protected $form = "72";
    public function index(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $key = \Input::get('key');
            $encode = base64_decode($key);
            $explode = explode("-",$encode);
            $att = $explode[1];
            $data_emp = \DB::SELECT("select supervisor from emp_supervisor where employee_id='$att' ");
            $data_hr = \DB::SELECT("select role_id from ldap where employee_id= '$att' ");
            if($data_emp != null){
                $data_emp = [];
            }elseif($data_hr != null){
                $data_hr_ex = $data_hr[0]->role_id;
                $role = \DB::SELECT("select  role_name from role where role_id = $data_hr_ex");
                if($role != null){
                    $data_emp = [];
                }
            }

            $key = \Input::get("key");
            $encode = base64_decode($key);
            $explode = explode("-",$encode);
            $explode_id = $explode[1];
            $userEmp  = \DB::SELECT("select role.role_name from role,ldap where ldap.employee_id =  '$explode_id' and ldap.role_id = role.role_id ");
                    if($userEmp[0]->role_name != "Regular employee user"){
                        $supervisor = "hr";
                        $db = \DB::SELECT("select id,leave_type from leave_type where id not in (1)");
                    }else{
                        $supervisor =  "user";
                        $db = \DB::SELECT("select id,leave_type from leave_type where id not in (1,12)");
                    }

                    $emp_data = \DB::SELECT("CALL search_employee_by_id('$explode_id')");

                        $data_exp =
                        [
                         "employee_name" => $emp_data[0]->name,
                         "employee_id" => $explode_id,
                         "status" => $supervisor
                        ];

                    //$message = "success"; $data = $data_exp; $status =  200;
            $data = ["data" => $db, "status" => $data_exp];
            return $request->index($data, $access[3]);
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }

    public function get_date_range(){

        // /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        // if($access[1] == 200){
        $date = \Input::get('from_date');
        $remaining = \Input::get('remaining_day');
        $name =  \Input::get('name');


        if(isset($name) and $name != null and isset($remaining) and $remaining != null and isset($remaining) and $remaining != null){
            $base_date_requets =  date_create($date);


            $next_last_request  =  date_create($date);

            $next_last_request  = $next_last_request->modify('+'.$remaining.' days');
            $next_last_request =  date_format($next_last_request,'Y-m-d');
            $sel =  \DB::SELECT("select * from att_schedule where  status  =  2 and employee_id  =   '$name' and   date > '$date'
                                 and date <  '$next_last_request' ");
            $plus_day_two =  0;
            $plus_day  =   0;
            if($sel != null){
                foreach ($sel as $key => $value) {
                    if($value->shift_id  == 36){
                            $plus_day  += 1;
                    }   
                }

                //modif last date +1
                $last_request_plus_one_day  =  date_create($next_last_request);
                $last_request_plus_one_day  = $last_request_plus_one_day->modify('+3 days');
                $last_request_plus_one_day =  date_format($last_request_plus_one_day,'Y-m-d');

                $new_sel =  \DB::SELECT("select * from att_schedule where  status  =  2 and employee_id  =   '$name' and   date > '$next_last_request'
                                 and date <=  '$next_last_request' ");                
                if($new_sel != null){
                    foreach ($new_sel as $key => $value) {
                        if($value->shift_id !=  36 ){
                            break;
                        }else{
                            $plus_day_two  += 1;
                        }
                    }
                }

                $total_day_between  = $plus_day_two+$plus_day; 
                $grand_total  =   $plus_day_two+$plus_day+$remaining;

              
                if($grand_total != 0){
                     $grand_total -=  1;     
                     $final_day  = $base_date_requets->modify('+'.$grand_total.' days');
                     $final_day =  date_format($final_day,'Y-m-d');
                }else{

                     $final_day  = $base_date_requets->modify('+'.$remaining.' days');
                     $final_day =  date_format($final_day,'Y-m-d');
                    
                }

                return response()-> json(['header' => ['message' =>'success  get data', 'status'=>200],'data' => ['date'  => $final_day, 'day_between' => $total_day_between]],200);
            }else{
                return response()->json(['header' =>['message' => 'get day success', 'status' => 500],'data' => []],500);
            }

        }
        //}
    }


    public function remain(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $name = \Input::get('name');
            $type = \Input::get('LeaveType');
            $input  = \Input::all();
            $rule = [
                'name' => 'required',
                'LeaveType' => 'required',
                ];




            $validation = \Validator::make($input,$rule);

            if($validation->fails()){
                $check = $validation->errors()->all();
                return $request->getMessage("failed",$check,500,$access[3]);
            }else{
                if(!isset($type)){
                 return $arr = ['data' => [], 'message' => 'Please choose one off the item in leave type'];
            }

                $local_it =  \DB::SELECT("select local_it from  emp where  employee_id = '$name'");
                $local_it =  ($local_it[0]->local_it != 1 ? 2 : 1);


                $object =  $this->OBJ_L($type);
                
                //let's find   out
                // $in_arr  =   [2,4]; 
                // if(in_array($object, $in_arr)){

                //     if($object == 2){
                //         $max_date =   $this->check_max_date($name,$object,$local_it);
                //     }
                //     if($object == 4){
                //          $max_date = $this->check_max_date($name,$object,$local_it);   
                           
                //     }

                // }


                    
                // end  of   let;'s find out

                if(isset($object)){
                    $object =  $this->OBJ_L($type);


                    if($object ==  2 and $local_it != 1){
                        return $this->remain_vacation_local($name);

                    }
                    if($object ==  2 and $local_it == 1){
                        return $this->remain_vacation_expat($name);
                    }
                    if($object ==  3 ){
                        return $this->remain_enhance($name);
                    }
                    if($object == 4 and $local_it ==  1 ){
               
                        return $this->sick_expat($name);
                    }
                    if($object == 4 and $local_it != 1 ){

                        return $this->sick_local($name);
                    }
                    if($object == 5 and $local_it ==  1 ){

                        return $this->expat_maternity($name);
                    }
                    if($object == 5 and $local_it !=  1 ){
                       
                        return $this->local_maternity($name);
                    }

                    if($object == 6 and $local_it ==  1 ){
                        return $this->expat_paternity($name);
                    }
                    if($object == 6 and $local_it !=  1 ){
                       return $this->local_paternity($name);
                    }

                    // 7 skip for bereavement

                    if($object == 8 and $local_it ==  1 ){
                        return $this->expat_marriage($name);
                    }
                    if($object == 8 and $local_it != 1 ){
                       return $this->local_marriage($name);
                    }
                    if($object == 11  ){
                       return $this->emmergency($name);
                    }

                    //offday on call
                    //       
                    if($object  ==   9){
                        return $this->offday_oncall($name);
                    }



                }
            }

        }else{
            $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }

    public function check_max_date($name,$object,$applied){
        //parameter name  object   
        $emp  =   \DB::SELECT("select * from emp where employee_id  =  '$name' ");
        if(!isset($emp[0]->department)){
            return $arr = ['data' =>  [], 'message' => 'department  not exist,please check it first'];
        }else{
            $department  = $emp[0]->department;
            if($object == 2){
                $require_date  =   \DB::SELECT("select entitlement_start from leave_vacation, leave_vacation_detail where 
                                    leave_vacation.id  = leave_vacation_detail.vacation_id 
                                    and department  =  $department and applied_for  =  $applied");  
            }

            if($object == 4){
                 $require_date  =   \DB::SELECT("select entitlement_start from leave_sick, leave_sick_detail where 
                                leave_sick.id  = leave_sick_detail.leave_sick_id 
                                and department  =  $department and applied_for  = $applied ");  
            }
        }

        if(isset($require_date[0]->entitlement_start)){
            $contract = $require_date[0]->entitlement_start;
        }else{
            $message   = ($object == 2 ?  'VACATION LEAVE is not yet entitled' : 'SICK LEAVE is not yet entitled' );
            return $arr =  ['data' => [], 'message' => $message];
        }

        if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$name' "); }
        if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$name' "); }
        if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$name' "); }
        if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$name' "); }
      
        $now  =  date('Y-m-d');
        $contract_start =  $contract_start[0]->date;
        $valRet = date_diff(date_create($contract_start),date_create($now));
        $valRet_year = $valRet->format("%y") ;
        $valRet_day = ( strlen($valRet->format("%d"))  == 1 ? '0'.$valRet->format("%d") : $valRet->format("%d"));
        $valRet_month = ( strlen($valRet->format("%m")) ==  1 ? '0'.$valRet->format("%m"): $valRet->format("%m"));
        $year_current =  (int)date('Y') +  1;

        $new_date   =  $year_current.'-'.$valRet_month.'-'.$valRet_day;

        return $arr = ['data' => $new_date,  'message' => 'success' ];

    }


    public  function remain_vacation_expat($id){
        $emp    =  \DB::SELECT("select * from  emp where  employee_id = '$id' ");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');
        

        $max_date =   $this->check_max_date($id,2,1);
        
        // if($max_date[0]->data == null){
            
        // }
        //return  $department;
        if(!isset($emp[0]->department)){
            return $arr = ['data' =>  [], 'message' => 'department  not exist,please check it first'];
        }else{
            $vacation  =   \DB::SELECT("select entitlement_start from leave_vacation, leave_vacation_detail where 
                                    leave_vacation.id  = leave_vacation_detail.vacation_id 
                                    and department  =  $department and applied_for  =  1");  
        }

        if(isset($vacation[0]->entitlement_start)){
            $contract = $vacation[0]->entitlement_start;
        }else{
            return $arr =  ['data' => null, 'message' => 'VACATION LEAVE is not yet entitled'];
        }

        if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$id' "); }
      
        $contract_start =  $contract_start[0]->date;
        $valRet = date_diff(date_create($contract_start),date_create($now));
        $valRet_year = $valRet->format("%y");
        $valRet_day = $valRet->format("%d");
        $valRet_month = $valRet->format("%m");

        //return $arr  = [$valRet_day, $valRet_year, $valRet_month];
        if($valRet_year <=  1 and $valRet_day < 1 and $valRet_month  < 1 ){
            return $arr  = ['data' => [], 'message' => 'VACATION LEAVE is not yet entitled']; 
        }
                  
        $personal_days =\DB::SELECT("select vacation as days from leave_table_personal  
                                      where employee_id = '$id' and year = $year_now ");
        $get_days = \DB::SELECT("select days from leave_vacation,leave_vacation_detail 
                                 where leave_vacation.id  =  leave_vacation_detail.vacation_id 
                                 and department  =  $department
                                 and applied_for  =  1
                                 and year  =  '$valRet_year' ");
        $taken_day = \DB::SELECT("select (sum(taken_day) -   sum(day_off))as taken from leave_request 
                                  where leave_type =  1 and employee_id = '$id' 
                                  and year(from_)=  '$year_now' and  year(to_) = '$year_now'  and status_id  in (1,2)");

        $taken_day =   $taken_day[0]->taken;
        $taken_day  = (!isset($taken_day) ? 0 : ($taken_day == null ? 0 : $taken_day));

        if(!isset($get_days) and $get_days == null and !isset($personal_days) and $personal_days == null){
            $arr = ['data' => [], 'message' =>  'VACATION LEAVE is not yet entitled']; 
            return response()->json($arr,500);
        }else{
           

            if(isset($personal_days) and $personal_days != null){            
                $total_days_left  =   $personal_days[0]->days -  $taken_day;
            }else{
               
                if(isset($get_days) and  $get_days != null){
                    $total_days_left  =  (int)$get_days[0]->days -  (int)$taken_day;
                } 
            }
         
            $last_date_request  =  \DB::SELECT("select created_at  from  leave_request where  employee_id = '$id'  and  year(from_)   =  '$year_now' and year(to_)   =  '$year_now' order by id DESC limit 1");
            if($last_date_request != null){
                $last_date_request =  $last_date_request[0]->created_at;
                $last_date_request = explode(' ',$last_date_request);
                $last_date_request =  $last_date_request[0];
                //next requets from past request +30 day
                $next_last_request  =  date_create($last_date_request);
                $next_last_request  = $next_last_request->modify('+30 days');
                $next_last_request =  date_format($next_last_request,'Y-m-d');

                //maximum request from next last request + 120days
                $max_last_request  =  date_create($next_last_request);
                $max_last_request  = $max_last_request->modify('+2 months');
                $max_last_request =  date_format($max_last_request,'Y-m-d');
            }else{
                $last_date_request = '-';
                $next_last_request = '-';
                $max_last_request  = '-';
            }             

                $arr   = ['data' => $total_days_left,'last' => $last_date_request, 
                                      'next' => $next_last_request, 'max' => $max_last_request, 
                                      'message' => 'success'];
                return response()->json($arr,200);
        }
    }

    public function  offday_oncall($id){
        //check masa jabatan 
        $emp    =  \DB::SELECT("select * from  emp where  employee_id = '$id' ");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');

        if(!isset($emp[0]->department)){
            return $arr = ['data' =>  [], 'message' => 'department  not exist,please check it first'];
        }else{
            $vacation  =   \DB::SELECT("select entitlement_start from leave_vacation, leave_vacation_detail where 
                                        leave_vacation.id  = leave_vacation_detail.vacation_id 
                                        and department  =  $department and applied_for  =  2");  
        }

        if(isset($vacation[0]->entitlement_start)){
            $contract = $vacation[0]->entitlement_start;
        }else{
            return $arr =  ['data' => null, 'message' => 'not provide  to take a leave'];
        }

        if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$id' "); }
      
        $contract_start =  $contract_start[0]->date;
        $valRet = date_diff(date_create($contract_start),date_create($now));
        $valRet_year = $valRet->format("%y");
        $valRet_month = $valRet->format("%m");



        if(isset($valRet_year) && $valRet_year != 0){
            $valRet_year  =  $valRet_year  * 12;
        }else{
            $valRet = 0;
        }

        $valRet  = round(($valRet_year  + $valRet_month) / 12);
       
       $get_days = \DB::SELECT("select * from leave_vacation,leave_vacation_detail 
                                 where leave_vacation.id  =  leave_vacation_detail.vacation_id 
                                 and department  =  $department
                                 and applied_for  =  2
                                 and year  =  $valRet");



        if(!isset($get_days[0])){
            return $arr =  ['data' => null, 'message' => 'Off Day / OC / OFF DAY on Call not set up yet  buy admin  for this year'];
        }else{

            $getDO          =  $get_days[0]->offday_oncall;
            $year           =   date('Y');
            $getOff_day     = \Db::SELECT("select count(t2.shift_id) as count  from  att_schedule as t1,  attendance_work_shifts as t2 where employee_id = '$id' and t1.shift_id = t2.shift_id  and lower(t2.shift_code) = 'oc'  and  YEAR(t1.date)  = '$year' ") ;  
            $getOff_day     =   $getOff_day[0]->count; 

            if($getDO <=  $getOff_day){
                return $arr = ['data' => [],  'message'  => 'less than  set up in leave table'];
            }else{
                $total =   $getDO - $getOff_day;

                if($total < 0){
                    return $arr = ['data' => [], 'messsage' => 'Exceed  more  than supposed, you can request again for this year' ];
                }else{
                    return $arr  = ['data' =>  $total,  'message' => "success"];
                }
            }
        }
    }

    public function remain_vacation_local($id){
        //check masa jabatan 
        $emp    =  \DB::SELECT("select * from  emp where  employee_id = '$id' ");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');
        
       // $max_date =   $this->check_max_date($id,2,2);

        if(!isset($emp[0]->department)){
            return $arr = ['data' =>  [], 'message' => 'department  not exist,please check it first'];
        }else{
            $vacation  =   \DB::SELECT("select entitlement_start from leave_vacation, leave_vacation_detail where 
                                    leave_vacation.id  = leave_vacation_detail.vacation_id 
                                    and department  =  $department and applied_for  =  2");  
        }

        if(isset($vacation[0]->entitlement_start)){
            $contract = $vacation[0]->entitlement_start;
        }else{
            return $arr =  ['data' => null, 'message' => 'not provide  to take a leave', 'date_max_from_request' => []];
        }

        if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$id' "); }
      
        $contract_start =  $contract_start[0]->date;
        $valRet = date_diff(date_create($contract_start),date_create($now));
        $valRet_year = $valRet->format("%y") ;
        $valRet_day = $valRet->format("%d");
        $valRet_month = $valRet->format("%m");

        //return $arr  = [$valRet_day, $valRet_year, $valRet_month];
        if($valRet_year <=  1 and $valRet_day < 1 and $valRet_month  < 1 ){
            return $arr  = ['data' => [], 'message' => 'VACATION LEAVE is not yet entitled' , 'date_max_from_request' => []]; 
        }



        $personal_days =\DB::SELECT("select vacation as days from leave_table_personal  
                                      where employee_id = '$id' and year = $year_now ");
        $get_days = \DB::SELECT("select days from leave_vacation,leave_vacation_detail 
                                 where leave_vacation.id  =  leave_vacation_detail.vacation_id 
                                 and department  =  $department
                                 and applied_for  =  2
                                 and year  =  '$valRet_year' ");
        $taken_day = \DB::SELECT("select (sum(taken_day) - sum(day_off)) as taken from leave_request 
                                  where leave_type =  2 and employee_id = '$id' 
                                  and year(from_)=  '$year_now' and  year(to_) = '$year_now'  and status_id  in (1,2)");

        $taken_day =   $taken_day[0]->taken;
        $taken_day  = (!isset($taken_day) ? 0 : ($taken_day == null ? 0 : $taken_day));

        if(!isset($get_days) and $get_days == null and !isset($personal_days) and $personal_days == null){
            $arr = ['data' => [], 'message' =>  'VACATION LEAVE is not yet entitled']; 
            return response()->json($arr,500);
        }else{
           

            if(isset($personal_days) and $personal_days != null){            
                $total_days_left  =   $personal_days[0]->days -  $taken_day;
            }else{
               
                if(isset($get_days) and  $get_days != null){
                    $total_days_left  =  (int)$get_days[0]->days -  (int)$taken_day;
                } 
            }
         
            $last_date_request  =  \DB::SELECT("select created_at  from  leave_request where  employee_id = '$id'  and  year(from_)   =  '$year_now' and year(to_)   =  '$year_now' order by id DESC limit 1");
            if($last_date_request != null){
                $last_date_request =  $last_date_request[0]->created_at;
                $last_date_request = explode(' ',$last_date_request);
                $last_date_request =  $last_date_request[0];
                //next requets from past request +30 day
                $next_last_request  =  date_create($last_date_request);
                $next_last_request  = $next_last_request->modify('+30 days');
                $next_last_request =  date_format($next_last_request,'Y-m-d');

                //maximum request from next last request + 120days
                $max_last_request  =  date_create($next_last_request);
                $max_last_request  = $max_last_request->modify('+2 months');
                $max_last_request =  date_format($max_last_request,'Y-m-d');
            }else{
                $last_date_request = '-';
                $next_last_request = '-';
                $max_last_request  = '-';
            }             
                    $arr   = ['data' => $total_days_left,'last' => $last_date_request, 
                                      'next' => $next_last_request, 'max' => $max_last_request, 
                                      'message' => 'success'];
                    return response()->json($arr,200);
        }
      
        

    }

    public function  remain_enhance($id){
        $job  = \DB::SELECT("select job from job_history where employee_id =  '$id' ");
        $year = date('Y');

        if(isset($job[0]->job) and  $job[0]->job != null ){     
            $job  =  $job[0]->job;
            $enhance  =   \DB::SELECT("select * from leave_enhance where  job_tittle  =  $job ");
            $chk_request  = \DB::SELECT("select (sum(taken_day) - sum(day_off)) as taken from leave_request where leave_type = 3 and employee_id =  '$id' 
                                         ");
            if(!isset($chk_request[0]->taken) ||  $chk_request[0]->taken == null){
                $chk_request[0]->taken =   0;
            }

            if(isset($enhance[0]) and $enhance[0]->days){
                $chk_request =   $enhance[0]->days - $chk_request[0]->taken;  
            }else{      
                return  $arr =  ['data' => [],  'message' =>  'ENHANCE LEAVE is not yet entitled'];
            }

            return $arr  = ['data' => $chk_request, 'message' => 'success'];
        }else{      
            return $arr  = ['data' =>[], 'message' =>  'please fill you job first'];
        }
    }


    public function sick_local($id){
        //
        $emp    =  \DB::SELECT("select * from  emp where  employee_id = '$id' ");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');

        $vacation  =   \DB::SELECT("select entitlement_start from leave_sick, leave_sick_detail where 
                                    leave_sick.id  = leave_sick_detail.leave_sick_id 
                                    and department  =  $department and applied_for  =  2");  
        if(!isset($emp[0]->department)){
           return $arr = ['data' =>  [], 'message' => 'Department  not exist,please check it first', 'date_max_from_request' => []];
        }
        if(isset($vacation[0]->entitlement_start)){
            $contract = $vacation[0]->entitlement_start;
        }else{
            return $arr =  ['data' => null, 'message' => 'SICK LEAVE is not yet entitled', 'date_max_from_request' => []];
        }

        if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$id' "); }
            
        $contract_start =  $contract_start[0]->date;
        $valRet = date_diff(date_create($contract_start),date_create($now));
        $valRet_year = $valRet->format("%y") ;
        $valRet_day = $valRet->format("%d");
        $valRet_month = $valRet->format("%m");

       
        if($valRet_year <=  1 and $valRet_day < 1 and $valRet_month  < 1 ){
            return $arr  = ['data' => [], 'message' => 'SICK LEAVE is not yet entitled', 'date_max_from_request' => []]; 
        }


        if($valRet_year > 0){
             $valRet = date_diff(date_create($now),date_create($contract_start));
             $valRetplusone =  $valRet_year  + 1;
             $base_date_modify = $contract_start.' + '.$valRet_year.' years';   
             $date_modify =  $contract_start.' + '.$valRetplusone.' years';

             $date_manipulation = date("Y-m-d", strtotime($base_date_modify));
             $date_manipulation_minus_one  =  date("Y-m-d",strtotime(' -1 day',strtotime(date("Y-m-d", strtotime($date_modify)))));
             //$personal_days =\DB::SELECT("select vacation as days from leave_table_personal  where employee_id = '$id' and year =  $year_now ");
             $get_days = \DB::SELECT("select days from leave_sick,leave_sick_detail 
                                     where leave_sick.id = leave_sick_detail.leave_sick_id 
                                     and department  =  $department
                                     and applied_for  =  2
                                     and year  =  '$valRet_year' ");
              $taken_day = \DB::SELECT("select (sum(taken_day)-sum(day_off)) as taken from leave_request
                                       where leave_type =  4 and employee_id = '$id' 
                                       and from_ > '$date_manipulation'  and  to_ < '$date_manipulation_minus_one' ");

             $taken_day  = (!isset($taken_day) ? 0 : ($taken_day == null ? 0 : $taken_day));

             if(!isset($get_days) and $get_days == null and !isset($personal_days) and $personal_days == null){
                return $arr = ['data' => [], 'message' =>  'SICK LEAVE is not yet entitled']; 
             }else{
                if(isset($get_days) and  $get_days != null){
                     $total_days_left  =   $get_days[0]->days -  $taken_day[0]->taken;
                      $set_day_max_from  = '-'.($total_days_left+1).'day';
                     $date_manipulation_max  =  date("Y-m-d",strtotime($set_day_max_from,strtotime($date_manipulation_minus_one)));
                     return $arr  = ['data' => $total_days_left, 'message' => 'success', 'date_max_from_request' =>  $date_manipulation_max];                
                 }
             }
            
        
        }else{
            return $arr  =  ['data' => [], 'message' => 'SICK LEAVE is not yet entitled', 'date_max_from_request' => []];
        }
    }

    public function sick_expat($id){
        //check masa jabatan 
         //check masa jabatan 
        $emp    =  \DB::SELECT("select * from  emp where  employee_id = '$id' ");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');
        
        $vacation  =   \DB::SELECT("select entitlement_start from leave_sick, leave_sick_detail where 
                                    leave_sick.id  = leave_sick_detail.leave_sick_id 
                                    and department  =  $department and applied_for  =  1 limit 1");  

        if(!isset($emp[0]->department)){
           return $arr = ['data' =>  [], 'message' => 'department  not exist,please check it first'];
        }
        if(isset($vacation[0]->entitlement_start)){
            $contract = $vacation[0]->entitlement_start;
        }else{
            return $arr =  ['data' => null, 'message' => 'SICK LEAVE is not yet entitled'];
        }

        if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$id' "); }
        if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$id' "); }

        $contract_start =  $contract_start[0]->date;


        $valRet = date_diff(date_create($contract_start),date_create($now));
        $valRet_year = $valRet->format("%y") ;
        $valRet_day = $valRet->format("%d");
        $valRet_month = $valRet->format("%m");

        //return $arr  = [$valRet_day, $valRet_year, $valRet_month];
        if($valRet_year <=  1 and $valRet_day < 1 and $valRet_month  < 1 ){
            return $arr  = ['data' => [], 'message' => 'SICK LEAVE is not yet entitled']; 
        }

        if(isset($contract_start)){
            
             $valRet = date_diff(date_create($now),date_create($contract_start));
             $valRetplusone =  $valRet_year  + 1;
             $base_date_modify = $contract_start.' + '.$valRet_year.' years';   
             $date_modify =  $contract_start.' + '.$valRetplusone.' years';

             $date_manipulation = date("Y-m-d", strtotime($base_date_modify));
             $date_manipulation_minus_one  =  date("Y-m-d",strtotime(' -1 day',strtotime(date("Y-m-d", strtotime($date_modify)))));

             $get_days = \DB::SELECT("select days from leave_sick,leave_sick_detail 
                                     where leave_sick.id = leave_sick_detail.leave_sick_id 
                                     and department  =  $department
                                     and applied_for  =   1
                                     and year  =  '$valRet_year' ");
             $taken_day = \DB::SELECT("select (sum(taken_day)-sum(day_off)) as taken from leave_request
                                       where leave_type =  4 and employee_id = '$id' 
                                       and from_ > '$date_manipulation'  and  to_ < '$date_manipulation_minus_one' ");

             $taken_day  = (!isset($taken_day) ? 0 : ($taken_day == null ? 0 : $taken_day));

             if(!isset($get_days) and $get_days == null and !isset($personal_days) and $personal_days == null){
                return $arr = ['data' => [], 'message' =>  'SICK LEAVE is not yet entitled']; 
             }else{
                if(isset($get_days) and  $get_days != null){
                     $total_days_left  =   $get_days[0]->days -  $taken_day[0]->taken;
                      $set_day_max_from  = '-'.($total_days_left+1).'day';
                     $date_manipulation_max  =  date("Y-m-d",strtotime($set_day_max_from,strtotime($date_manipulation_minus_one)));
                     return $arr  = ['data' => $total_days_left, 'message' => 'success', 'date_max_from_request' =>  $date_manipulation_max];                
                 }
             }
        }else{
            return $arr  =  ['data' => [], 'message' => 'SICK LEAVE is not yet entitled'];
        }
    }

    public function expat_maternity($id){
        //check masa jabatan 
        $emp    =  \DB::SELECT("select * from  emp where  employee_id = '$id' ");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');


        if(!isset($emp[0]->department)){
            $arr = ['data' =>  [], 'message' => 'department  not exist,please check it first'];
            return response()->json($arr,500);
        }
        // if(!isset($emp[0]->gender) and strtolower($e[mp0]->gender) != 'female' or !isset($emp[0]->marital_status) and strtolower($emp[0]->marital_status) !=  'married'){
        //     return $arr  =  ['data' => [], 'message' => 'not allowed to  only for women  for prepare childbirth'];
        // }else{
        //     $child_chk = \DB::SELECT("select * from  leave_request where  employee_id =  '$id' and leave_type = 5  and year(from_)  =  $year_now ");
        //     if($child_chk != null){
        //      return $arr  =  ['data' => [], 'message' => 'you have take place  for  maternity for this year'];
        //     }else{
                return $arr  = ['data' => 'No Limit', 'message' => 'success']; 
        //     }
        // }
    }

    public function local_maternity($id){
         //check masa jabatan 

        $key   = \input::get('key');
        $encode =  base64_decode($key);
        $explode  =  explode('-',$encode);
        $get_the_guy  =  $explode[1];


        $emp    =  \DB::SELECT("select * from  emp where  employee_id = '$id' ");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');


        // if(!isset($emp[0]->department)){
        //     return $arr = ['data' =>  [], 'message' => 'department  not exist,please check it first'];
        // }
        // if(!isset($emp[0]->gender) and strtolower($emp[0]->gender) != 'female' or !isset($emp[0]->marital_status) and strtolower($emp[0]->marital_status) !=  'married'){
        //     return $arr  =  ['data' => [], 'message' => 'not allowed to  only for women  for prepare childbirth'];
        // }else{
        //     $child_chk = \DB::SELECT("select * from  leave_request where  employee_id =  '$id' and leave_type = 5  and year(from_)  =  $year_now ");
        //     if($child_chk != null){
        //       return $arr  =  ['data' => [], 'message' => 'you have take place  for  maternity for this year'];
        //     }else{
        //         //get the guy
        //         $hr  = \DB::SELECT("select * from  view_nonactive_login where employee_id =  '$get_the_guy'  and (lower(role_name)  like '%human%' or   lower(role_name)  like '%human resource%')");
        //         if($hr != null)-{

        //print_r("select * from job_history where  employee_id = '$id' and (probationary_start  != '0000-00-00' or fixed_term_contract !=  '0000-00-00')");
        $data_exist  = \DB::select("select * from job_history where  employee_id = '$id' and (probationary_start  != '0000-00-00' or fixed_term_contract !=  '0000-00-00')");

        if($data_exist == null){
             $arr  = ['data' => [],  'message' => 'MATERNITY LEAVE is not yet entitled'];
             return response()->json($arr,500);
        }else{
            $arr  =  ['data' => 'No Limit'  , 'message' => 'success'];
            return response()->json($arr,200);
        }

                    
        //         }else{
        //             return $arr  = ['data' => [],  'message' => 'consideration for take place  applied by hr only'];
        //         } 
        //     }
        // }
    }

    public function expat_paternity($id){
        $emp    =  \DB::SELECT("select * from  emp where  employee_id = '$id' ");
        $department =  $emp[0]->department;
        //$local  =  $emp[0]->local_it;
        //$now    = date('Y-m-d');
        //$year_now   =  date('Y');

         if(!isset($emp[0]->department)){
             return $arr = ['data' =>  [], 'message' => 'department  not exist,please check it first'];
          }
        // if(!isset($emp[0]->gender) and strtolower($emp[0]->gender) != 'male' or !isset($emp[0]->marital_status) and strtolower($emp[0]->marital_status) !=  'married'){
        //      return $arr  =  ['data' => [], 'message' => 'not allowed to  only for a man who attend to his wife�s need during
        //                        childbirth or miscarriage'];
        // }else{
        //     $chk_this_year  = \DB::SELECT("select * from leave_request where employee_id =  '$id' and leave_type=6 and year(from_) = '$year_now' and  year(to_) = '$year_now' ");
        //     if($chk_this_year !=  null){
        //             return  $arr   =  ['data' => [], 'message' =>  'already request for this  year'];
        //     }else{
        //         return $arr  = ['data' => 7, 'message' => 'success']; 
        //     }
        // }
        $arr  =  ['data' => 7, 'message' => 'success'];
        return response()->json($arr,200);

    }   

    public function local_paternity($id){
        $emp    =  \DB::SELECT("select * from  emp where  employee_id = '$id'");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');

        if(!isset($emp[0]->department)){
            $arr = ['data' =>  [], 'message' => 'department  not exists,please check it first'];
            return response()->json($arr,500);
        }

        // $data_exist  = \DB::SELECT("select * from  leave_request where employee_id  =  $id and leave_type  =  7 and year(from_) = $year_now  and year(to_) = $year_now");

        // if($data_exist != null){
        //     return $arr  = ['data' => [],  'message' => 'already request for this year']; 
        // }

        // if(!isset($emp[0]->gender) and strtolower($emp[0]->gender) != 'male' or !isset($emp[0]->marital_status) and strtolower($emp[0]->marital_status) !=  'married'){
        //      return $arr  =  ['data' => [], 'message' => 'not allowed only for a man who attend to his wife�s need during childbirth or miscarriage'];
        // }else{
        //      $data  =  $this->remain_vacation_expat($id);

        //      if(isset($data['data']) && $data['data'] != null){
        //         $data  = $data['data'] ;
        //         if($data >= 5){
        //             $final_day = 12;
        //         }elseif ($data > 0  and $data  < 5) {
        //             $final_day =  7 + $data; 
        //         }else{
        //             $final_day = 7;
        //         }
        //      }else{
        //         $final_day   =  7;
        //      }
        $data_exist  = \DB::select("select * from job_history where  employee_id = '$id' and 
                                    ((contract_start_date is not null or probationary_start  != '0000-00-00') or 
                                    (fixed_term_contract is not null or fixed_term_contract !=  '0000-00-00'))");

        if($data_exist != null){
            $arr  =  ['data' => 7, 'message' => 'success'];
            return response()->json($arr,200);
        }else{
            $arr  =  ['data' => [], 'message' => 'PATERNITY LEAVE is not yet  entitled'];    
            return response()->json($arr,500);
        }

        

        //}        
    }

    public function  expat_marriage($id){
        $emp    =  \DB::SELECT("select * from emp where  employee_id = $id");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');

        if(!isset($emp[0]->department)){
            return $arr = ['data' =>  [], 'message' => 'department  not exist,please check it first'];
        }

        // $data_exist  = \DB::SELECT("select * from  leave_request where employee_id  =  $id and leave_type  =  8 and year(from_) = $year_now  and year(to_) = $year_now");

        // if($data_exist != null){
        //     return $arr  = ['data' => [],  'message' => 'already request for this year']; 
        // }

        // if(!isset($emp[0]->marital_status) and strtolower($emp[0]->marital_status) ==  'married'){
        //      return $arr  =  ['data' => [], 'message' => 'not allowed to take leave, why do you want to marriage more than one times  ?'];
        // }else{
        //      $data  =  $this->remain_vacation_expat($id);
        //      if(isset($data['data']) && $data['data'] != null){
        //         $data  = $data['data'];
        //         if($data >= 12){
        //             $final_day =  19;
        //         }elseif ($data > 0 and $data < 12) {
        //             $final_day =  7 + $data; 
        //         }else{
        //             $final_day = 7;
        //         }
        //      }else{
                $final_day   =  7;
        //     }

             return $arr  = [ 'data' => $final_day , 'message' => 'Marriage Leave successfully requested'];
        //}           
    }

    public function  local_marriage($id){
        $emp    =  \DB::SELECT("select * from  emp where  employee_id = $id");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');

        if(!isset($emp[0]->department)){
             $arr = ['data' =>  [], 'message' => 'department  not exist,please check it first'];
            return response()->json($arr,500);
        }

        //$data_exist  = \DB::SELECT("select * from  leave_request where employee_id  =  $id and leave_type  =  8 and year(from_) = $year_now  and year(to_) = $year_now");

       $data_exist  = \DB::select("select * from job_history where  employee_id = '$id' and 
                                    (contract_start_date != '0000-00-00' or fixed_term_contract != '0000-00-00')");

        if($data_exist == null){
            $arr  = ['data' => [],  'message' => 'MARRIAGE LEAVE is not entitled']; 
            return  response()->json($arr,500);
        }else{
             return $arr  = [ 'data' =>  10, 'message' => 'success'];
            return  response()->json($arr,200);
        }
    }

    public function get_bereavement(){
     
        $id  = \Input::get('name');
        $emp = \DB::SELECT("select * from emp where employee_id =   '$id' ");
        
        if(!isset($emp[0]->department)){  $arr  = ['data' => [], 'message' => 'department not found'];}
        if(!isset($emp[0]->local_it)){  $arr  = ['data' => [], 'message' => 'sub employee not found'];}
                

        $department = $emp[0]->department;
        $local      = $emp[0]->local_it;

        $local      = ($local != 1 ? 2 : 1);
    
       // $db  = \DB::SELECT("select add_on from leave_request where employee_id =  '$id' and leave_type = 7");
       // print_r("select t1.id, t2.relation_to_deceased,
        //                       t2.days from leave_bereavement t1
        //                       left join leave_bereavement_detail  t2 on t1.id  =  t2.bereavement_id  
        //                       left join leave_request t3 on t3.add_on  != t1.id and  t3.leave_type = $local and t3.employee_id = '$id'                     
        //                       where t1.applied_for = $local and t1.department =  $department ");
        // return;
        $row  =  \DB::SELECT("select t1.id, t2.relation_to_deceased,
                              t2.days from leave_bereavement t1
                              left join leave_bereavement_detail  t2 on t1.id  =  t2.bereavement_id  
                              left join leave_request t3 on t3.add_on  != t1.id and  t3.leave_type = $local and t3.employee_id = '$id'                     
                              where t1.applied_for = $local and t1.department =  $department ");

        $has_deleted  =  false;
        // if($db != null){
        //     foreach ($db as $key => $value) {
        //         foreach ($row as $k => $v) {
        //             if($value->add_on == $v->relation_to_deceased){
        //                 unset($row[$k]);
        //                 $has_deleted =  true;
        //             }
        //         }
        //     }
        // }

        
        // if($row ==  null and $has_deleted  ==  true){
        //     $res =   ['data' => [], 'message' => 'you have to request this type choose other type with same as your purpose'];
        //     return  response()->json($res,500);
        if($row ==  null ){
            $res =   ['data' => [], 'message' => "BEREAVEMENT LEAVE  is not yet entitled"];
            return  response()->json($res,500);
        }else{
            $res  =  ['data' => $row, 'message' => 'success'];
            return  response()->json($res,200);
        }

        if($res['data'] == null){
            return response()->json(['header' => ['message' => $res['message'],  'status' => 500],'data' => []], 500);
        }else{
            return response()->json(['header' => ['message' => $res['message'], 'status' => 200],'data' => []], 200);
        }

        // return response()->json(['header' => ['message' => 'success load data' ],'data' => []],200)
    }

    public function  emmergency($id){
        $emp    =  \DB::SELECT("select * from  emp where  employee_id = $id");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');

        if(!isset($emp[0]->department)){
            return $arr = ['data' =>  [], 'message' => 'department  not exist,please check it first'];
        }else{
             return $arr  = [ 'data' =>  "No Limit", 'message' => 'success'];
        }
    }

    public function remain_offday_oncall($id){
        $emp    =  \DB::SELECT("select * from  emp where  employee_id = $id");
        $department =  $emp[0]->department;
        $local  =  $emp[0]->local_it;
        $now    = date('Y-m-d');
        $year_now   =  date('Y');


        $local  = ($local  !=  1 ? 2 : 1);
        //check_personal_leave_table
        $db  =   \Db::SELECT("select * from  leave_table_personal  where employee_id  = '$id' and year =  '$year_now' ");
        if($db != null){
            if($local  == 1){
                $start_date   =  $year_now.'-01-01';
                $end_date     =  $year_now.'-12-31';
            }else{
                // $it  =   \DB::SELECT("select id from department where parent in (select id from department where lower(name) = 'it') ");
                // $non =   \DB::SELECT("select id from  department where parent in  (select id from department where lower(name)  like '%non%') ");

                // $department_it = [];
                // $non_it = [];
                
                // if(!isset($it)){
                //     return $arr  = ['data' => [],  'message' => 'department with parent and child it not found'];
                // }else{    
                //     foreach ($it as $key => $value) {
                //         $department_it[] = $value->id;
                //     } 
                // }  

                 
                // if(!isset($non)){
                //     return $arr  = ['data' => [],  'message' => 'department with parent and child it not found'];
                // }else{    
                //     foreach ($non as $key => $value) {
                //         $non_it[] = $value->id;
                //     } 
                // }

                $user  =  null;
                if(in_array($department, $department_it)){ $user  = 'it'; }
                elseif(in_array($department, $non_it)){ $user  = 'nonit'; }else{
                    $user =  'absurb';
                }

                if($user == null || $user == "absurb"){
                    return $arr = ['data' => [], 'message' => 'user not in it or non it'];
                }else{
                    
                }
            }
        }
    }

    public function chk_validate($i){

        if(!isset($i['LeaveType'])){
                 return $arr = ['data' => [], 'message' => 'Please choose one off item in leave type'];
        }

        if(isset($i["IncludeAdo"])){
            $ado   =   $i["IncludeAdo"];
        }else{
            $i["IncludeAdo"] =  'No';
            $ado   =   $i["IncludeAdo"];
        }
        

        $get_id_type  = $this->OBJ_L($i['LeaveType']);
      
        if(!isset($get_id_type)){
            return $arr = ['data' => [], 'message' => 'error to saving data , please refresh your browser'];
        }

        $RMN_DAY  =  (isset($i['remaining_day']) ?$i['remaining_day'] : null);

        // if(gettype($RMN_DAY) ==  "string" and $i['LeaveType'] != 'Emergency Leave'){
        //      return $arr = ['data' => [], 'message' => 'your not allowed to request'];
        // }


        if($get_id_type ==  10 ){
            $rule  = [
                        [
                            'employee' => (isset($i['name']) ? $i['name'] : null),
                        ],
                        [
                            'employee' => 'required',
                        ],
                        [
                            'employee' => 'name is required', 
                        ]
                     ];
        }

        if($get_id_type ==  12 ){
            $rule  = [
                        [
                            'employee' => (isset($i['name']) ? $i['name'] : null),
                            'suspension' =>  (isset($i['suspension']) ? $i['suspension'] : null),
                            'remaining_day'  =>  (isset($i['remaining_day']) ?  $i['remaining_day'] : null),
                            'from' => (isset($i['From']) ? $i['From'] : null),
                            'to' => (isset($i['To']) ? $i['To'] : null),
                            'dob' => (isset($i['DayOffBetween']) ? $i['To'] : null),
                            'agree' => (isset($i['aggred']) ? $i['aggred'] : null),
                        ],
                        [
                            'employee' => 'required',
                            'suspension' => 'required',
                            'remaining_day' => 'required',
                            'from' =>  'required',
                            'to' =>  'required',
                            'dob' =>  'required',
                            'agree' => 'required',
                        ],
                        [
                            'employee' => 'name is required', 
                            'suspension' => 'please choose the suspensiomn before submit data',
                            'remaining_day' => 'please insert start and end of date request',
                            'from' => 'please insert start date',
                            'to' => 'please insert end date',
                            'dob' =>  'dob not found',
                            'agree' =>  'you must checklist agreement license',
                        ]
                     ];
        }

        if($ado == "Yes" ){

            $rule = [
                        [
                         
                            'between'            => (isset($i['DayOffBetween']) ? $i['DayOffBetween'] : null ),
        
                            'from'               => (isset($i['From']) ? $i['From'] : null),
                            'to'                 => (isset($i['To']) ? $i['To'] : null),
                            'agree'              => (isset($i['aggred']) ? $i['aggred'] : null),
                            'comment'            => (isset($i['comment']) ? $i['comment'] : null),
                            'employee'           => (isset($i['employee_id']) ? $i['employee_id'] : null),
                            'remaining_day'      => (isset($i['remaining_day']) ?$i['remaining_day'] : null),
                         
                        ],
                        [           
                            'between'            => 'integer|min:4',
                         
                            'from'               => 'required|date',
                            'to'                 => 'required|date',
                            'agree'              => 'required|digits:1',
                            'comment'            => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
                            'employee'           => 'required',
                            'remaining_day'      => 'integer | required',
                           

                         ],
                         [
                            'between.min'        => 'minimal 5 days for take place',
                            'comment.alpha_dash' => 'not allowed, only support alphanumber and underscore',
                            
                            'from.required'      => 'start date request need it',
                            'from.date'          => 'start date format, invalidate',
                            'to.required'        => 'end date request need it',
                            'to.date'            => 'end date format, invalidate',
                            'agree.digits'       => 'please tick, i accepted and aggree option',
                            'comment.required'   => 'please insert comment',
                            'employee'           => 'name is required',
                            'remaining_day.required' =>  'remaining day is required',
                        ]
                    ]; 

            return $rule;           
        }

        if($get_id_type ==  2 ){
            $rule = [
                        [
                         
                            'between'            => (isset($i['DayOffBetween']) ? $i['DayOffBetween'] : null ),
                            'ado'                => (isset($i['IncludeAdo']) ? $i['IncludeAdo'] : null ),
                            'from'               => (isset($i['From']) ? $i['From'] : null),
                            'to'                 => (isset($i['To']) ? $i['To'] : null),
                            'agree'              => (isset($i['aggred']) ? $i['aggred'] : null),
                            'comment'            => (isset($i['comment']) ? $i['comment'] : null),
                            'employee'           => (isset($i['employee_id']) ? $i['employee_id'] : null),
                            'remaining_day'      => (isset($i['remaining_day']) ?$i['remaining_day'] : null),
                        ],
                        [           
                            'between'            => 'integer',
                            'ado'                => 'required|in : Yes,No',
                            'from'               => 'required|date',
                            'to'                 => 'required|date',
                            'agree'              => 'required|digits:1',
                            'comment'            => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
                            'employee'           => 'required',
                            'remaining_day'      => 'integer | required',

                         ],
                         [

                            'between.min'        => 'minimal 5 days for take place',
                            'comment.alpha_dash' => 'not allowed, only support alphanumber and underscore',
                            'ado.required'       => 'please choose with ado or not with ado',
                            'from.required'      => 'start date request need it',
                            'from.date'          => 'start date format, invalidate',
                            'to.required'        => 'end date request need it',
                            'to.date'            => 'end date format, invalidate',
                            'agree.digits'       => 'please tick, i accepted and aggree option',
                            'comment.required'   => 'please insert comment',
                            'employee'           => 'name is required',
                        ]
                    ];
        }

        
        if($get_id_type ==  9 ){
            $rule = [
                        [
                         
                            'between'            => (isset($i['DayOffBetween']) ? $i['DayOffBetween'] : null ),
                            'ado'                => (isset($i['IncludeAdo']) ? $i['IncludeAdo'] : null ),
                            'from'               => (isset($i['From']) ? $i['From'] : null),
                            'to'                 => (isset($i['To']) ? $i['To'] : null),
                            'agree'              => (isset($i['aggred']) ? $i['aggred'] : null),
                            'comment'            => (isset($i['comment']) ? $i['comment'] : null),
                            'employee'           => (isset($i['employee_id']) ? $i['employee_id'] : null),
                            'remaining_day'      => (isset($i['remaining_day']) ?$i['remaining_day'] : null),
                        ],
                        [           
                            'between'            => 'integer',
                          
                            'from'               => 'required|date',
                            'to'                 => 'required|date',
                            'agree'              => 'required|digits:1',
                            'comment'            => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
                            'employee'           => 'required',
                            'remaining_day'      => 'integer | required',

                         ],
                         [
                            'from.required'      => 'start date request need it',
                            'from.date'          => 'start date format, invalidate',
                            'to.required'        => 'end date request need it',
                            'to.date'            => 'end date format, invalidate',
                            'agree.digits'       => 'please tick, i accepted and aggree option',
                            'comment.required'   => 'please insert comment',
                            'employee'           => 'name is required',
                        ]
                    ];
        }



         if( $get_id_type == 3 || $get_id_type == 6 ){
            $rule = [
                        [
                         
                            'between'            => (isset($i['DayOffBetween']) ? $i['DayOffBetween'] : null ),
                            'from'               => (isset($i['From']) ? $i['From'] : null),
                            'to'                 => (isset($i['To']) ? $i['To'] : null),
                            'agree'              => (isset($i['aggred']) ? $i['aggred'] : null),
                            'comment'            => (isset($i['comment']) ? $i['comment'] : null),
                            'employee'           => (isset($i['employee_id']) ? $i['employee_id'] : null),
                            'remaining_day'      => (isset($i['remaining_day']) ?$i['remaining_day'] : null),
                        ],
                        [
                                        
                            'between'            => 'integer',
                            'from'               => 'required|date',
                            'to'                 => 'required|date',
                            'agree'              => 'required|digits:1',
                            'comment'            => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
                            'employee'           => 'required',
                            'remaining_day'      => 'integer | required',
                         ],
                         [

                            
                            'comment.alpha_dash' => 'not allowed, only support alphanumber and underscore',
                            'from.required'      => 'start date request need it',
                            'from.date'          => 'start date format, invalidate',
                            'to.required'        => 'end date request need it',
                            'to.date'            => 'end date format, invalidate',
                            'agree.digits'       => 'please tick, i accepted and aggree option',
                            'comment.required'   => 'please insert comment',
                            'employee'           => 'name is required',
                        ]
                    ];
        }

        if( $get_id_type == 5){
            $rule = [
                        [
                         
                            'between'            => (isset($i['DayOffBetween']) ? $i['DayOffBetween'] : null ),
                            'from'               => (isset($i['From']) ? $i['From'] : null),
                            'to'                 => (isset($i['To']) ? $i['To'] : null),
                            'agree'              => (isset($i['aggred']) ? $i['aggred'] : null),
                            'comment'            => (isset($i['comment']) ? $i['comment'] : null),
                            'employee'           => (isset($i['employee_id']) ? $i['employee_id'] : null),
                            'remaining_day'      => (isset($i['remaining_day']) ?$i['remaining_day'] : null),
                        ],
                        [
                                        
                            'between'            => 'integer',
                            'from'               => 'required|date',
                            'to'                 => 'required|date',
                            'agree'              => 'required|digits:1',
                            'comment'            => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
                            'employee'           => 'required',
                            'remaining_day'      => 'required',
                         ],
                         [

                            
                            'comment.alpha_dash' => 'not allowed, only support alphanumber and underscore',
                            'from.required'      => 'start date request need it',
                            'from.date'          => 'start date format, invalidate',
                            'to.required'        => 'end date request need it',
                            'to.date'            => 'end date format, invalidate',
                            'agree.digits'       => 'please tick, i accepted and aggree option',
                            'comment.required'   => 'please insert comment',
                            'employee'           => 'name is required',
                        ]
                    ];
        }

        if(  $get_id_type  == 8 ||  $get_id_type == 4 || $get_id_type  == 11){
            $rule = [
                        [
                            'between'            => (isset($i['DayOffBetween']) ? $i['DayOffBetween'] : null ),
                            'from'               => (isset($i['From']) ? $i['From'] : null),
                            'to'                 => (isset($i['To']) ? $i['To'] : null),
                            'agree'              => (isset($i['aggred']) ? $i['aggred'] : null),
                            'comment'            => (isset($i['comment']) ? $i['comment'] : null),
                            'employee'           => (isset($i['employee_id']) ? $i['employee_id'] : null),
                            'remaining_day'      => (isset($i['remaining_day']) ?$i['remaining_day'] : null),
                        ],
                        [
                                        
                       
                            'from'               => 'required|date',
                            'to'                 => 'required|date',
                            'agree'              => 'required|digits:1',
                            'comment'            => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
                            'employee'           => 'required',
                            'remaining_day'      =>  'required',
                         ],
                         [

                            'comment.alpha_dash' => 'not allowed, only support alphanumber and underscore',
                            'from.required'      => 'start date request need it',
                            'from.date'          => 'start date format, invalidate',
                            'to.required'        => 'end date request need it',
                            'to.date'            => 'end date format, invalidate',
                            'agree.digits'       => 'please tick, i accepted and aggree option',
                            'comment.required'   => 'please insert comment',
                            'employee'           => 'name is required',
                        ]
                    ];
        }
        if($get_id_type ==  7){
            $rule = [
                        [
                            'between'            => (isset($i['DayOffBetween']) ? $i['DayOffBetween'] : null ),
                            'from'               => (isset($i['From']) ? $i['From'] : null),
                            'to'                 => (isset($i['To']) ? $i['To'] : null),
                            'bereavement'        => (isset($i['LeaveBrea']) ? $i['LeaveBrea'] : null),
                            'agree'              => (isset($i['aggred']) ? $i['aggred'] : null),
                            'comment'            => (isset($i['comment']) ? $i['comment'] : null),
                            'employee'           => (isset($i['employee_id']) ? $i['employee_id'] : null),
                            'remaining_day'      => (isset($i['remaining_day']) ?$i['remaining_day'] : null),
                        ],
                        [
                            'between'              => 'integer',
                            'from'                 => 'required|date',
                            'to'                   => 'required|date',
                            'agree'                => 'required|digits:1',
                            'comment'              => 'required',
                            'bereavement'          => 'required',
                            'employee'             => 'required',
                            'remaining_day'        => 'integer | required',
                            'comment'              => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',

                        ],
                        [
                          
                            'bereavement.required' => 'please choose at lease one of the options  in  leave bereavement',
               
                            'from.required'        => 'start date request need it',
                            'from.date'            => 'start date format, invalidate',
                            'to.required'          => 'start date request need it',
                            'to.date'              => 'start date format, invalidate',
                            'agree.digits'         => 'please tick, i accepted and aggree option',
                            'comment.required'     => 'please insert comment',
                            'employee'             => 'name is required',
                        ]
                    ];
        }
        return $rule;

    }

    public function OBJ_L($req){
          $object  =  [
                            "Vacation Leave"              => 2,
                            "Enhance Vacation Leave"      => 3,
                            "Sick Leave"                  => 4,
                            "Maternity Leave"             => 5,
                            "Paternity Leave"             => 6,
                            "Bereavement Leave"           => 7,
                            "Marriage Leave"              => 8,
                            "offday_oncall"               => 9,
                            "Accumulation Day Off"        => 10,
                            "Emergency Leave"             => 11,
                            "Suspension"                  => 12
                        ];

            $type  =  $req;

            return $object[$type];
    }



    public function check_request_date($i,$last_date =  null){
            $now  =  date('Y-m-d');


            if(!isset($i['LeaveType'])){
                 return $arr = ['data' => [], 'message' => 'Please choose one off the item in leave type'];
            }

            $get_id_type  = $this->OBJ_L($i['LeaveType']);
            
            if(!isset($get_id_type)){
                return $arr = ['data' => [], 'message' => 'error to saving data , please refresh your browser'];
            }else{
               $from =  (isset($i['From']) ? $i['From'] :  null);
               $to =  (isset($i['To']) ?  $i['To']  : null);


               $year  = date('Y');
              
               if( $get_id_type != 10){
                   $EXP_FROM  =  explode('-',$from);

                   if($year  != $EXP_FROM[0]){
                        return  $arr = ['data' => [],'message' => 'can request only for current year'];
                   }
                } 


               if($get_id_type ==  2){
                    $nows  =  date_create($now);
                    $froms  =  date_create($from);

                    $interval  = date_diff($froms,$nows);
                    $interval  = $interval->format('%m');

                    // if($interval  < 2 || $interval > 4){
                    //      return $arr = ['data' => [], 'message' => 'can lower than 2 month and  more than 4 month  from date take a place'];
                    // }

                    // if($last_date != null){
                    //       $last_date  = date_create($last_date);
                    //       $MOD_LAST_DATE =  $last_date->modify('+1 months');
                    //       $MOD_LAST_DATE =  $MOD_LAST_DATE->format('Y-m-d');

                    //       if($MOD_LAST_DATE > $now){
                    //           return $arr  = ['data' => [], 'message' => 'you can request vacation request  again after '.$MOD_LAST_DATE];
                    //       } 
                    //}

                    // $explode  =  explode('-',$from);
                    // if($explode[2] > 20){
                    //     return $arr = ['data' => [], 'message' => 'request must be submitted before  20th for every months'];
                    // } 

                    return  $arr = ['data' => 1,'message' => 'approval'];
                    
               }
               // if($get_id_type ==  5){
               //      $now =   date_create ($now);
               //      $from = date_create($from);
               //      $to  = date_create($to);

               //      $interval =  date_diff($to,$from);
               //      $interval  = $interval->format('%m');

               //      $interval_now  =  date_diff($from,$now);
               //      $interval_now  = $interval_now->format("%m");


               //      if($interval  > 150){
               //          return   $arr = ['data' =>  [], 'message' => 'maternity only can request under 150 days']; 
               //      }
               //      if($interval_now < 2){
               //          return   $arr = ['data' =>  [], 'message' => 'maternity must requested before 2 months take a place']; 
               //      }   

               //      return  $arr = ['data' => 1,'message' => 'approval'];
               // }

               if($get_id_type ==  10){
                    return  $arr = ['data' => 1,'message' => 'approval'];
               }

               if($get_id_type ==  6){
                    
                    $now         = date_create($now);
                    $from        = date_create($from);

                    $interval    = date_diff($from,$now);
                    $interval    = $interval->format('%m');

                    // if($interval < 6){
                    // return $arr  = ['data'  => [], 'message' => 'paternity must requested 6 month before due date'];
                    // }

                    return  $arr = ['data' => 1,'message'   => 'approval'];

               }
               if($get_id_type ==  8){
                   $now         = date_create($now);
                   $from        = date_create($from);

                   $interval    = date_diff($from,$now);
                   $interval    = $interval->format('%m');

                   // if($interval < 2){
                   //      return $arr  = ['data' => [], 'message' => 'maternity must requested 2 month before due date'];
                   // } 

                   // if($interval  < 2 and $interval > 4){
                   //      return $arr  = ['data' => [], 'message' => 'can lower than 2 month and  more than 4 month  from date take a place'];
                   // }

                   return  $arr = ['data' => 1,'message'   => 'approval'];

               }
                   return  $arr = ['data' => 1,'message'   => 'approval'];
               
            }

    }


    // public function exception($RES){

    //     if(is_string($RES['remaining_day']) ==  true){
    //         if(strtolower($RES['LeaveType']) == "Maternity  leave"){}

    //     }
    // }

    public function request_leave(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
         $model =  new notificationV3;
        if($access[1] == 200){
            $file  = \Input::file('file');

            if(isset($file)){
                $i = \Input::get('data');
                $i = json_decode($i,1);
            }else{
                return   response()->json(['header' => ['message' =>  'file must be fill', 'status' => 500], 'data' => []], 500);
            }

          
           $rule =  $this->chk_validate($i['data'],$file); 
           
            //IF RULE PASSED
            if(isset($rule['message'])){
                return response()->json(['header' =>  ['message' => $rule['message'], 'status' => 500 ],'data' => []],500);
            }else{
                $validator =   \Validator::make($rule[0],$rule[1],$rule[2]);
                if($validator->fails()){
                  
                    $check = $validator->errors()->first();

                    return response()->json(['header' => ['message' => $check, 'status' => 500 ],'data' => []],500);     
                }else{
                   
                    $RES =  $i['data'];
                    if(!isset($RES['IncludeAdo'])){
                        $RES['IncludeAdo']  =  "No";   
                    }else{
                        //KUDOS
                        //$RES['IncludeAdo']  =  "No";   
                    }
                    $TYP_ID =  $this->OBJ_L($RES["LeaveType"]);

                    $check_isset  = ['date1','date2','date3','date4'];

                        foreach ($RES as $key => $value) {
                            if(in_array($key,$check_isset)){
                                $obiOne =  substr($key, -1); 
                                $luke = ($obiOne == 1 ? 'st' : ($obiOne ==  2 ? 'nd' : ($obiOne ==  3 ? 'rd' : 'th')));


                                if((isset($RES['date'.$obiOne])) and ($RES['date'.$obiOne] != '-') and ($RES['date'.$obiOne] != null)){
                                    if((isset($RES['datev'.$obiOne])) and ($RES['datev'.$obiOne] != '-') and ($RES['datev'.$obiOne] != null)){
                                        if((isset($RES['ado'.$obiOne])) and ($RES['ado'.$obiOne] != '-') and ($RES['ado'.$obiOne] != null)) {
                                            //KUDOS
                                        }else{
                                            return response()->json(['header' => ['message' =>  'date ado '.$obiOne.' '.$luke.' not complete yet' , 'status' => 500],'data' => []],500);
                                        } 
                                    }else{return response()->json(['header' => ['message' =>  'date ado  '.$obiOne.' '.$luke.' not complete yet' , 'status' => 500],'data' => []],500);}
                                }else{
                                    return response()->json(['header' => ['message' =>  'date ado '.$obiOne.' '.$luke.' not complete yet' , 'status' => 500],'data' => []],500);}
                            }
                        }
// start offday / oncall
                    $select = '';
                    if($RES['IncludeAdo'] == 'Yes' || $TYP_ID  != 10){
                       // print_r(1);
                        /**
                         * pertama  cari applied for employee_id 
                         * @var [type]
                         */
                        $applied  = \DB::SELECT("select local_it from emp where employee_id  = '$RES[name]' ")[0]->local_it;
                        $applied = ($applied ==  1 ? 1 : 2);
                        /**
                         * [$sky description]
                         * @var [type]
                         * cari berdasarkan department emp dan leave vacation 
                         */
                        // print_r("select leave_vacation.id,leave_vacation.applied_for,leave_vacation.entitlement_start,  if(emp.local_it > 1, 2, 1) as localit from  emp,leave_vacation where emp.employee_id = '$RES[name]' and emp.department =  leave_vacation.department and   leave_vacation.applied_for = (select localit) ");
                        $sky  = \DB::SELECT("select leave_vacation.id,leave_vacation.applied_for,leave_vacation.entitlement_start from  emp,leave_vacation where emp.employee_id = '$RES[name]' and emp.department =  leave_vacation.department and   leave_vacation.applied_for =  if(emp.local_it > 1, 2, 1) ");
                   
                        /**
                         *  dari $applied  bandigin dengan data dari sky -> aplied kalo cocok lanjut;
                         */

                       // print_r($applied);
                        if(isset($sky[0]->applied_for) and  $applied ==  $sky[0]->applied_for){    
                         // print_r(2);
                            /**
                             * cari jonb history dari employee_id   
                             * @var [type]
                             */
                            $get_job  = \DB::SELECT("select * from job_history where employee_id = '$RES[name]' ");
                            
                            // dari sky yang a diatas cari entitlemt daynya dapet  banding dengan datadari job_history
                            if(isset($sky[0]->entitlement_start) or  $sky[0]->entitlement_start){       
                                //print_r(3);
                                /**
                                 * kamus data  pembanding dengan dari  job history
                                 * @var [type]
                                 */
                                $arr_entitlment  =  ['Training Period' => 'training_agreement' ,'Probationary Period' => 'probationary_start','contract_start' => 'contract_start_date','Fixed Term Contract' => 'fixed_term_contract'];

                                $get_days = '';
                                foreach ($arr_entitlment as $key => $value) {
                                    if($key ==  $sky[0]->entitlement_start){
                                         $get_days  =   $get_job[0]->$value;
                                        // print_r(4);

                                         /**
                                          * cari berapa tanahun kerja   kalo udah dapet cek di leave_vacation_detail 
                                          * [$now description]
                                          * @var [type]
                                          */
                                         $d_detail =  $sky[0]->id;
                                         $now  = date('Y-m-d');
                                         $valRet = date_diff(date_create($now),date_create($get_days));
                                         $valRet = $valRet->format("%y");


                                         $select  =  \DB::SELECT("select offday_oncall from leave_vacation_detail where vacation_id  =  $d_detail and  year  = $valRet" )[0]->offday_oncall;
                                         if($select != null){
                                          // print_r(5);       
                                                $leave  = \DB::SELECT("select  count(id) as total_days from leave_request where leave_type = 10 and year(year) = year(now()) and employee_id  = '$RES[name]' ")[0]->total_days;
                                                if($select < $leave){
                                                    return  response()->json(['header' => ['message' => 'you offday oncall run out', 'status' => 500],'data' => []],500);
                                                }else{
                                                   // print_r(6);
                                                    /**
                                                     * loop for get  user
                                                     * @var integer
                                                     */
                                                    $xyz  =  1;
                                                    $default  =  'date';
                                                    $fin_variable  = $default.$xyz;
                                                    foreach ($RES as $key => $value) {
                                                        if($key  == $fin_variable){
                                                            $xyz += 1;
                                                        }
                                                    }
                                                    
                                                  //  print_r();
                                                    if($select <  ($leave+$xyz)){
                                                        return response()->json(['header' => ['messsage' => 'more than allowed offday in this year, it just left '.$select, 'status' => 500 ],'data' => []],500);
                                                    }else{
                                                        //KUDOS
                                                    }
                                                    
                                                }
                                         }else{ 
                                            return  response()->json(['header' => ['message' =>  'offday / oncall not found', 'status' => 500],'data' => []],500);
                                         }
                                    }
                                }
                            }else{      
                                return response()->json(['header' => ['message' => 'entitlement day for you not found, please contact our admin' , 'status'  => 500],'data' => 500], 500);
                            }
                        }

                        // else{
                        //     return response()->json(['header' => ['message' => 'you leave configuration not set up yet, please contact your admin', 'status' => 500],'data' => []],500);
                        // }

                        
                    }

// end offday / oncall


                    //get the user requested
                    $WHOIAM =  \Input::get('key');
                    $WHOIAM  =  base64_decode($WHOIAM);
                    $EXP_WHOIAM = explode('-',$WHOIAM);  
                    $EXP_WHOIAM =  $EXP_WHOIAM[1];

                    $sup  = \DB::SELECT("select * from emp_supervisor where employee_id = '$RES[name]' and supervisor = '$EXP_WHOIAM' ");
                    $hr = \DB::SELECT("select * from view_nonactive_login where employee_id = '$RES[name]' and (lower(role_name) like '%human resource%' or lower(role_name) like '%hr%') ");
                    $user = ($sup != null  ? 'sup' : ( $hr != null ? 'hr' : null));

		     //for suspension
		    $hrx = \DB::SELECT("select * from view_nonactive_login where employee_id = '$EXP_WHOIAM' and (lower(role_name) like '%human resource%' or lower(role_name) like '%hr%' or  lower(role_name) like '%superuser%' ) ");
                    $userx = ($hrx != null ? $hrx : null);
                    
                    if($TYP_ID == 2 and isset($RES['date']['last_date']) and $RES['date']['last_date'] != '-' ){
                        $CHK_DATE = $this->check_request_date($i['data'],$RES['date']['last_date']);
                    }else{
                        $CHK_DATE = $this->check_request_date($i['data']);
                    }

                 

                    //return  $TYP_ID;
                    //
                    

                    if($CHK_DATE['data'] ==  1 ){
                        try{                    


                            $year  =  date('Y');
                            $LC_it  =  \DB::SELECt("select local_it from emp where employee_id =  '$RES[name]' ");

                            $LC_it  =  ($LC_it[0]->local_it == 1 ? 'expat'   : 'local');

                            if($TYP_ID != 10){
                            $balance_day  = $RES['DayOffBetween'] - $RES['remaining_day'] - 1;
                            $RES['DayOffBetween'] += 1;

                            $path = "hrms_upload/leave_request";
                            $rename  = '';
                            $comment_id  = '';

                           
                                if(isset($file) && $file != null){    
                                    //RETURN 1;
                                    // if($TYP_ID ==  7){

                                    //     $get_berea =  \DB::select("select * from  leave_request where ");
                                    // }
                                    if($TYP_ID == 7){
                                       //RETURN 'A'; 
                         
                                       $total_days =   $RES['remaining_day'] -  $RES['DayOffBetween'];
                                         if(!isset($RES['LeaveBrea'])){
                                            return response()->json(['header' => ['message' => "you can't request leave bereavement", 'status' => 500],'data' => []],500);
                                         }
                                         $id_bereavement = $RES['LeaveBrea'];
                                         // print_r("insert  into leave_request(employee_id,leave_type,from_,to_,day_off,status_id,dayoff_between,created_at,taken_day,year,balance_day)
                                         //                    values ('$RES[name]',$TYP_ID,'$RES[From]','$RES[To]',$RES[DOB],1,$RES[DayOffBetween],now(),$RES[DayOffBetween],'$year',$total_days)");
                                         $result  = \DB::SELECT("insert  into leave_request(employee_id,leave_type,from_,to_,status_id,day_off,Dayoff_between,created_at,year,taken_day,balance_day)
                                                            values ('$RES[name]',$TYP_ID,'$RES[From]','$RES[To]',1,$RES[DOB],$RES[DayOffBetween],now(),'$year',$RES[DayOffBetween],$total_days)");
                                    }elseif($TYP_ID == 12){
                                  	if($userx  ==  null){

					   return response()->json(['header' => ['message' => 'SUSPENSION  REQUEST can only request  by superuser', 'status' => 500 ],'data' =>  []],500);

					}else{
                                        	$result  = \DB::SELECT("insert  into leave_request(employee_id,add_on,leave_type,from_,to_,day_off,status_id,Dayoff_between,created_at,year,taken_day,balance_day)
                                                            values ('$RES[name]',$RES[suspension],12,'$RES[From]','$RES[To]',$RES[DOB],1,$RES[DayOffBetween],now(),'$year',$RES[DayOffBetween],$balance_day)"); 
					}
                                    }else{

                                        $balance_day =  $RES['remaining_day'] -  $RES['DayOffBetween']; 

                                        $result  = \DB::SELECT("insert  into leave_request(employee_id,leave_type,from_,to_,day_off,status_id,Dayoff_between,created_at,year,taken_day,balance_day)
                                                            values ('$RES[name]',$TYP_ID,'$RES[From]','$RES[To]',$RES[DOB],1,$RES[DayOffBetween],now(),'$year',$RES[DayOffBetween],$balance_day)"); 
                                    }


                                    $rename = "leave_request_".str_random(6).".".$file->getClientOriginalExtension();
                                    \Input::file('file')->move(storage_path($path),$rename);
                                    
                                    $comment  =  \DB::SELECT("select id from  leave_request order by id desc limit 1");
                                    $comment_id  = $comment[0]->id;

                                    $insertV2  =  \DB::SELECT("insert into command_center(type_id,master_type,request_id,comment,employee_id,filename,path,created_at) 
                                                               values($TYP_ID,2,$comment_id,'$RES[comment]','$RES[name]','$rename','$path',now())");
                                }else{
                                    //RETURN 2;


                                    $result  = \DB::SELECT("insert  into leave_request(employee_id,leave_type,from_,to_,day_off,status_id,Dayoff_between,created_at,year,taken_day,balance_day)
                                                            values ('$RES[name]',$TYP_ID,'$RES[From]','$RES[To]',$RES[DOB],1,$RES[DayOffBetween],now(),'$year',$RES[DayOffBetween],$balance_day)");

                                    $comment  =  \DB::SELECT("select id from  leave_request order by id desc limit 1");
                                    $comment_id  = $comment[0]->id;
                                    
                                    $rename  = null;
                                    $path  =  null;

                                    $insertV2  =  \DB::SELECT("insert into command_center(type_id,master_type,request_id,comment,employee_id,filename,path,created_at) 
                                                               values($TYP_ID,2,$comment_id,'$RES[comment]','$RES[name]','$rename','$path',now())");
                                }
                            }


                            if($RES['IncludeAdo'] == 'Yes' and $TYP_ID == 2){
                               // RETURN 3;
                                $leave_type  = 10;
                                $db_chechk_exist  = \DB::SELECT("select balance_day from  leave_request where leave_type = 10  and employee_id =  '$RES[name]' and year = year(now()) order by id  DESC limit 1");
                                if(isset($db_chechk_exist[0]->balance_day)){
                                    $select   =  $db_chechk_exist[0]->balance_day;
                                }

                                foreach ($RES as $key => $value) {
                                    if(in_array($key,$check_isset)){
                                       

                                       $gets =  substr($key, -1); 
                                       $_mod  =  'datev'.$gets;
                                       $select -=  1;
                                       $insert = \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,status_id,created_at,taken_day,year,balance_day) 
                                                              value ('$RES[name]',10,'$RES[$key]','$RES[$_mod]',1,now(),1,year(now()),$select)");     
                                       

                                       
                                       $comment  =  \DB::SELECT("select id from  leave_request order by id desc limit 1");
                                       $comment_id  = $comment[0]->id;
                                       
                                       $insertV2  =  \DB::SELECT("insert into command_center(type_id,master_type,request_id,comment,employee_id,filename,path,created_at) 
                                                         values($TYP_ID,10,$comment_id,'$RES[comment]','$RES[name]','$rename','$path',now())");

                                       $model->notif($comment_id,$TYP_ID,$RES['name'],$LC_it,$user);
                                    }
                                }

                                  
                                return response()->json(['header' => ['message' => 'success insert leave request with Accumulation Day Off', 'status' => 200],'data' => []],200);
                            }

                            if($TYP_ID == 10){
                                //RETURN 4;
                                $path = "hrms_upload/leave_request";
                                $leave_type  = 10;
                                if(isset($file) && $file != null){  
                                   $rename = "leave_request_".str_random(6).".".$file->getClientOriginalExtension();
                                   \Input::file('file')->move(storage_path($path),$rename);
                                }

                                $db_chechk_exist  = \DB::SELECT("select balance_day from  leave_request where leave_type = 10  and employee_id =  '$RES[name]' and year = year(now()) ");
                                if(isset($db_chechk_exist[0]->balance_day)){
                                   $select   =  $db_chechk_exist[0]->balance_day;
                                }

                                foreach ($RES as $key => $value) {
                                    if(in_array($key,$check_isset)){
                                       $gets =  substr($key, -1); 
                                       $_mod  =  'datev'.$gets;
                                       $select -=  1;
                                       $insert = \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,status_id,created_at,taken_day,year,balance_day) 
                                                              value ('$RES[name]',10,'$RES[$key]','$RES[$_mod]',1,now(),1,year(now()),$select)");     
                                       
                                       $comment  =  \DB::SELECT("select id from  leave_request order by id desc limit 1");
                                       $comment_id  = $comment[0]->id;
                                       
                                       $insertV2  =  \DB::SELECT("insert into command_center(type_id,master_type,request_id,comment,employee_id,filename,path,created_at) 
                                                         values($TYP_ID,2,$comment_id,'$RES[comment]','$RES[name]','$rename','$path',now())");

                                       $model->notif($comment_id,$TYP_ID,$RES['name'],$LC_it,$user);
                                    }
                                }

                                return response()->json(['header' => ['message' => 'success insert Accumulation Day Off', 'status' => 200],'data' => []],200);
                            }

                            
                         

                           
                            //$addFEATURE =  $this->ADDFEATURE($TYP_ID,"leave");
                            
                            //return $arr  = [$comment_id,$TYP_ID,$RES['name'],$LC_it,$user];
                            
                            if($RES['IncludeAdo'] == 'Yes' || $TYP_ID ==  10){
                              //KUDOS
                            }else{
                              $model =  $model->notif($comment_id,$TYP_ID,$RES['name'],$LC_it,$user);
                            }

                            if($model   ==  'success'){
                                return response()->json(['header' =>[ 'message' =>  'success appplied request', 'status' =>  200],'data' => []] ,200);
                            }else{
                                return response()->json(['header' => ['messsage' =>  $model, 'status' => 500],'data' => []],500);
                            }
                        }catch(\Exeception $e){

                        }
                    }else{
                        return response()->json(['header' =>  ['message' => $CHK_DATE['message'], 'status' => 500],'data' => []],500);
                    } 
                }
            }
            
            

        }else{
            $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }

    // public function  ADDFEATURE($id,$master){
    //       // $att  = ['time','over','under','late','early','ob','train','change','swap']; 
    //       // $leav = ['vacation','ADO','enhnace','birth','sick','maternity','paternity','bereavement','marriage','emmergency','suspension','dayoff'];

    //        if($master == "leave"){
    //             if($id ==  2){
    //                 $res  = 'vacation';  
    //             }
    //             if($id ==  3){
    //                 $res  = 'enhance';  
    //             }
    //             if($id ==  4){
    //                 $res  = 'sick';  
    //             }
    //             if($id ==  5){
    //                 $res  = 'maternity';  
    //             }
    //             if($id ==  6){
    //                 $res  = 'paternity';  
    //             }
    //             if($id ==  7){
    //                 $res  = 'bereavement';  
    //             }
    //             if($id ==  8){
    //                 $res  = 'marriage';  
    //             }
    //             if($id ==  9){
    //                 $res  = 'dayoff';  
    //             }
    //             if($id ==  10){
    //                 $res  = 'ADO';  
    //             }
    //             if($id ==  11){
    //                 $res  = 'emmergency';  
    //             }
    //             if($id ==  12){
    //                 $res  = 'suspension';  
    //             }

    //        }

    //        if($master == "schedule"){
    //             if($id ==  1){
    //                 $res  = 'train';  
    //             }
    //             if($id ==  2){
    //                 $res  = 'ob';  
    //             }
    //             if($id ==  3){
    //                 $res  = 'over';  
    //             }
    //             if($id ==  4){
    //                 $res  = 'under';  
    //             }
    //             if($id ==  5){
    //                 $res  = 'maternity';  
    //             }
    //             if($id ==  6){
    //                 $res  = 'swap';  
    //             }
    //             if($id ==  7){
    //                 $res  = 'late';  
    //             }
    //             if($id ==  8){
    //                 $res  = 'early';  
    //             }
    //             if($id ==  9){
    //                 $res  = 'time';  
    //             }
    //        }

    //        return $res;    
    // }

}