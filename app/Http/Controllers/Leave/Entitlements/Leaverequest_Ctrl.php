<?php namespace Larasite\Http\Controllers\Leave\Entitlements;
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;
use Larasite\Model\Leave\Entitlements\Leaverequest_Ctrl_term ;
use Exeception;
use DateTime;

class Leaverequest_Ctrl extends Controller {

    protected $form = "72";

    private  $rule_ado = [
                'date1'   => 'required|date',
                'date2'   => 'required|date',
                'date3'   => 'required|date',
                'date4'   => 'required|date',
                'ado1'    => 'required|integer',
                'ado2'    => 'required|integer',
                'ado3'    => 'required|integer',
                'ado4'    => 'required|integer',
                'nextday' => 'required|date'
              ];

    public function get_suspension(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $name = \Input::get('name');
            // $db = \DB::SELECT("select infraction.id ,infraction.infraction_name,infraction.days as sanDate,(
            //     case 
            //     when exists (select id from leave_request where employee_id = '$name' and leave_type = 12 and add_on = infraction.id)
            //     then
            //         (select sanDate)
            //         -
            //         (select sum(dayoff_between) from leave_request where employee_id = '$name' and leave_type = 12 and add_on = infraction.id)
            //     else
            //         (select sanDate)
            //     end
            // )as days from infraction,leave_request where infraction.employee_id  = '$name'  and sanction = 4 group by infraction.infraction_name");

            $db = \DB::SELECT("select infraction.id ,infraction.infraction_name,infraction.days as sanDate,infraction.days as days, infraction.sanction_date from infraction where infraction.employee_id  = '$name'  and infraction.sanction = 4 group by infraction.infraction_name");
            if($db == null){
                $message = "no suspension found";    
                $status = 500;
            }else{
                $status = 200;
                $message = "Suspension found";    
            }
             $data = $db;
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }

    public function DayOffBetween(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $i =  \Input::all();
            // $date1 =  strtotime($i['From']);
            // $date2 =  strtotime($i['To']);
            // $date = date('d',($date2-$date1));

            $from  =  $i['From'];
            $to  =  $i['To'];
            $id  = $i['employee_id'];

             $select  =  \DB::SELECT("select count(att_schedule.schedule_id) as count  from  att_schedule,attendance_work_shifts  where employee_id  =  '$id' and (date > '$from' and  date  < '$to' ) and att_schedule.shift_id  = attendance_work_shifts.shift_id and lower(attendance_work_shifts.shift_code) = 'do'   ");


               // $comp1 = \DB::SELECT("select * from att_schedule as ass, attendance_work_shifts as aws where ass.date >= '$i[From]' and ass.date <= '$i[To]' and  ass.shift_id = aws.shift_id and lower(aws.shift_code) =  'do' and ass.employee_id = '$i[employee_id]' ");
            // $comp2 = \DB::SELECT("select from_,to_ from leave_request where from_ >= '$i[From]' and to_ <= '$i[To]' and status_id = 2 and employee_id = '$i[employee_id]' ");
            // $comp3 = \DB::SELECT("select count(from_) as count from leave_request where from_ >= '$i[From]' and to_ <= '$i[To]' and status_id = 2 and leave_type = 9 and employee_id ='$i[emplo;yee_id]' ");
            // $count =  count($comp1);

            // if($count > 1){
            //     foreach ($comp2 as $key => $value) {
            //         $count1 = count($comp1);
            //         for($i =  0; $i < $count1;  $i++){
            //             if($value->from_ <= $comp1[$i]->date && $value->to_ >= $comp1[$i]->date){
            //                     unset($comp1[$key]);
            //             }
            //         }       
            //     }

            //     $final_cal = count($comp1) + $comp3[0]->count;
            // }else{
            //     $final_cal = $comp3[0]->count;
            // }
                
             if($select != null and isset($select[0]->count)){
                $final_cal =  $select[0]->count;
             }else{
                $final_cal = 0;
             }

            return  [['result' => $final_cal]];
            
          
            // return $dob  = \DB::SELECT("call viewDOB('2017-12-01','2017-12-12','2016054') ");
            // }
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }

    public function baseBirth(){                
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $key = \Input::get('key');
            $encode = base64_decode($key);
            $explode = explode("-",$encode);
            $att = $explode[1];
            $employee = \Input::get('employee_id');
            $get_date  =  \DB::SELECT("select concat((year(now())),'-',(substr(emp.date_of_birth,6,10))) as tg from emp  where employee_id  = '$employee' ");
            //$get_date  =  \DB::SELECT("select emp.date_of_birth as tg from emp  where employee_id  = '$att'");
            if(isset($get_date[0]->tg) && $get_date[0]->tg != null ){
                if(isset($get_date[0]->tg)){
                    return $get_date;
                }else{
                    return [[1]];
                }
            }else{
                return [[1]];
            }

        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }   
    }

    public function check_ado(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $date1 = \Input::get("date1");
            $date2 = \Input::get("date2");
            $date3 = \Input::get("date2");
            $date4 = \Input::get("date4");
            $_names = \Input::get('name');
            $_employee  =  \Input::get('employee_id');
            // and is_numeric($_names)

            //create manipulatian
            if(isset($date1) &&   $date1 != null){
                $date1 = \Input::get("date1");
            }else{
                $date1  = "date1";
            }

            //date  2 manipulation 
            if(isset($date2) && $date2 !=  null){
                $date2 = \Input::get("date2");
            }else{
                $date2  = "date2";
            }

            //date  3 manipulation 
            if(isset($date3)  &&  $date3   !=  null){
                $date3 = \Input::get("date3");
            }else{
                $date3  = "date3";
            }

            //date  4 manipulation 
            if(isset($date4)  &&   $date4  !=  null){
                $date4 = \Input::get("date4");
            }else{
                $date4  = "date4";
            }

            $m_array =  [$date1,$date2,$date3,$date4];

            $o_array  =  count($m_array);
            $m_m_array = count(array_unique($m_array));


           //return [$o_array, $m_m_array];         
            if($o_array !=  $m_m_array){
                
                return Response()->json(['header' => ['message' =>'Error :There is   duplicate date' ,'status' => 500],'data' => []],500);
            }   

            if(isset($_names) and is_numeric($_names)){
                $nsame =   $_names;
            }else{
                $name =  $_employee;
            }


            // $var = "date";
            // $count = 4;
            // $check  =  0;
            // $arr   = [] ;
            // for($i = 1; $i <= $count; $i++){
            //     $data = $var.$i;
            //     if(isset($$data)){
            //          $get_date  =  $$data;
            foreach ($m_array as $key => $value) {

                //check  input date  
                $m_explode  = explode('-',$value);
                if(isset($m_explode[1])){
                     $get_date  = $value;    
                     $data = \DB::SELECT("select attendance_work_shifts.shift_code from att_schedule,attendance_work_shifts where att_schedule.employee_id = '$name' 
                                          and att_schedule.shift_id = attendance_work_shifts.shift_id and att_schedule.date = '$get_date' limit 1 ");
                     if(isset($data[0]->shift_code)  && strtolower($data[0]->shift_code) ==  'do'){
                         //KUDOS
                     }else{
                         $check +=  1;
                         $arr[]  =  $get_date;
                     }
                }
            }


            if(!isset($check)){
                    $message = "DO found in schedule"; $status = "200"; $data = [];
                    return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
            }else{
                    $gt =   implode($arr);
                    return  end($gt);
                    $message = "DO not found in schedule (" .end($gt). ")"; $status = "500"; $data = [];
                    return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
                    
            }
    

        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }

    public function nearest_day(){
         /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
         return \Response::json(['header'=>['message'=>"nearest day found",'status'=>200],'data'=>null],200);
        if($access[1] == 200){
            $arr =[];
            $date1 = \Input::get('date1');
            $date2 = \Input::get('date2');
            $date3 = \Input::get('date3');
            $date4 = \Input::get('date4');
            
            if(isset($date1)){
                $arr[] = $date1;
            } 
            if(isset($date2)){
                $arr[] = $date2;
            } 
            if(isset($date3)){
                $arr[] = $date3;
            } 
            if(isset($date4)){
                $arr[] = $date4;
            }  

            $name =  \Input::get('employee_id');
            $empx  =  \Input::get('name');

            if(!isset($name)  && $name == null){
                $name = $empx;
            }
            $max = (max($arr));
            $min = (min($arr));      
            $count =  count($arr);
            $in = implode("','",$arr);
            $in  = "'".$in."'";

            $rest_day = \Input::get('nextday');
           
            $db = \DB::SELECT("select att_schedule.date from att_schedule,attendance_work_shifts where att_schedule.employee_id = '$name' 
                               and att_schedule.shift_id = attendance_work_shifts.shift_id and att_schedule.date not in ($in) and lower(attendance_work_shifts.shift_code) = 'do' 
                               and att_schedule.date < '$min' and att_schedule.date > '$rest_day' order by att_schedule.date DESC limit $count");
            $total = count($db);
            if($db == null  || $total < $count){
                $db = \DB::SELECT("select att_schedule.date from att_schedule,attendance_work_shifts where att_schedule.employee_id = '$name' 
                               and att_schedule.shift_id = attendance_work_shifts.shift_id and att_schedule.date not in ($in) and lower(attendance_work_shifts.shift_code) = 'do' 
                               and att_schedule.date > '$min' and att_schedule.date < date_add('$rest_day',interval 1 month)  order by att_schedule.date DESC limit $count");
            }

            if($db != null){
                sort($db);
                $result[] = [
                            "date1" => (isset($db[0]->date) ? $db[0]->date : ""),
                            "date2" => (isset($db[1]->date) ? $db[1]->date : ""),
                            "date3" => (isset($db[2]->date) ? $db[2]->date : ""),
                            "date4" => (isset($db[3]->date) ? $db[3]->date : ""),
                            ];
                return \Response::json(['header'=>['message'=>"nearest day found",'status'=>200],'data'=>$result],200);
            }else{
                return \Response::json(['header'=>['message'=>'data not found','status'=>500],'data'=>[]],500);
            }
            
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }

    public function index(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $key = \Input::get('key');
            $encode = base64_decode($key);
            $explode = explode("-",$encode);
            $att = $explode[1];
            $data_emp = \DB::SELECT("select supervisor from emp_supervisor where employee_id='$att' ");
            $data_hr = \DB::SELECT("select role_id from ldap where employee_id= '$att' ");
            if($data_emp != null){
                $data_emp = [];
            }elseif($data_hr != null){
                $data_hr_ex = $data_hr[0]->role_id;
                $role = \DB::SELECT("select  role_name from role where role_id = $data_hr_ex");
                if($role != null){
                    $data_emp = [];
                }
            }
            

            $key = \Input::get("key");
                    $encode = base64_decode($key);
                    $explode = explode("-",$encode);
                     $explode_id = $explode[1];
                    //$supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$explode_id' ");
                    //$hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$explode_id' AND t2.role_name = 'hr'");
                   $userEmp  = \DB::SELECT("select role.role_name from role,ldap where ldap.employee_id =  '$explode_id' and ldap.role_id = role.role_id ");
                    if($userEmp[0]->role_name != "Regular employee user"){
                        $supervisor = "hr";
                        $db = \DB::SELECT("select id,leave_type from leave_type where id not in (1)");
                    }else{
                        $supervisor =  "user";
                        $db = \DB::SELECT("select id,leave_type from leave_type where id not in (1,12)");
                    }
                    $emp_data = \DB::SELECT("CALL search_employee_by_id('$explode_id')");


                        $data_exp =
                         [
                         "employee_name" => $emp_data[0]->name,
                         "employee_id" => $explode_id,
                         "status" => $supervisor
                        ];

                    //$message = "success"; $data = $data_exp; $status =  200;
            $data = ["data" => $db, "status" => $data_exp];
            return $request->index($data, $access[3]);
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }


    public function index_shift()
    {
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

        if($access[1] == 200){
            $request = new leaverequest_Model;
            $shift = \DB::SELECT("CALL view_workshift_fixed_schedule");
            foreach($shift as $key => $value){
                $data[] =  ["id" => $value->shift_id, "shif_code" => $value->shift];
            }
            return $access;
            return  $request->index($data,$access[3]);
        }
        else{

            $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access[3]],$status);
        }

    }


    public function autocomplete(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $name = \Input::get('name');
            $input = \Input::all();
            $rule = ["name" => "required|Regex:/^[A-Za-z]+$/"];
            $validation = \Validator::make($input,$rule);
            $db = \DB::SELECT("CALL Search_emp_by_name ('$name')");

            return $request->search($validation,$db,$access[3]);
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }

    public function get_bereavement(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
        
            $request = new leaverequest_Model;
            $now = date('Y-m-d');
            $name = \Input::get('name');
            $brea  = \Input::get('LeaveBrea');
            $remaining = "";
            $base_emp = \DB::SELECT("select  * from emp where employee_id = '$name' ");
            $localit = $base_emp[0]->local_it;
            if(!isset($base_emp) || $base_emp == null){
                return $request->getMessage("failed",'department not found',500,$access[3]);
            }else{
                $department = $base_emp[0]->department;
            }

            //check contract
            $select_contract =  \DB::SELECT("select * from leave_bereavement where applied_for = $localit and department = $department ");
            $contract = $select_contract[0]->entitlement_start;

            if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$name' "); }
            if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$name' "); }
            if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$name' "); }
            if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$name' "); }

            $contract_start =  $contract_start[0]->date;
            $valRet = date_diff(date_create($now),date_create($contract_start));
            $valRet = $valRet->format("%a");
            $select_request = \DB::SELECT("select sum(datediff(to_,from_)+1)  as total from leave_request where employee_id = '$name' and leave_type = 7  and comment = '$brea' ");
            $job_history = \DB::SELECT("select * from job_history where employee_id = '$name'");

            if(isset($job_history[0]->job) && $job_history[0]->job != null ){

               $job_history = $job_history[0]->job;
               $tenure = ( $valRet >= 270 && $valRet <= 364 ? 1 : ceil($valRet/365));
               $select_contract =  \DB::SELECT("select days from leave_bereavement,leave_bereavement_detail where leave_bereavement.applied_for = $localit and leave_bereavement.department = $department and leave_bereavement.id = leave_bereavement_detail.bereavement_id  and leave_bereavement_detail.relation_to_deceased = '$brea' ");
               
               if(isset($select_contract[0]->days) &&  $select_contract[0]->days != null){
                 if($select_request[0]->total  != null){
                   
                    $remaining = $select_contract[0]->days - $select_request[0]->total;
                 }else{
                    $remaining = $select_contract[0]->days;
                 }
                 
               }

               $supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$name' ");
                    if($supervisor != null){
                        $supervisor = "supervisor";
                    }else{
                        $supervisor =  "user";
                    }


                    $last_date = \DB::SELECT("select leave_request.from_,leave_request.created_at  from leave_request,leave_type where leave_request.employee_id= '$name' and leave_request.leave_type = leave_type.id and leave_type.leave_type = 7 order by leave_request.id desc limit 1");
                    if(isset($last_date) && $last_date != null){
                        if($last_date[0]->from_ != null){
                            $last_date = $last_date[0]->from_;
                        }else{
                            $last_date = '-' ;
                        }
                    }

                    $date = ["last_date" => $last_date, "max_date_req" => null, "for_request" => null];
                    $data_exp= [
                            "remaining_day" => $remaining,
                            "status" => $supervisor,
                            "date" => $date
                      ];
                     return  $request->index($data_exp,$access[3]);
            }else{
                return $request->getMessage("failed",'job type not found for this user',500,$access[3]);  
            }                         

            return $request->search($validation,$db,$access[3]);
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }   
    }

     public function get_typebereavement(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
         
            $request = new leaverequest_Model;
            $name = \Input::get('name');
            $empx =  \DB::SELECT(" select *  from emp where employee_id = '$name' ");
            $remaining = '';
            $localit = $empx[0]->local_it;
            if(!isset($empx[0]->department)){
                return $request->getMessage("failed",'department not found',500,$access[3]);  
            }else{
                $department = $empx[0]->department;
                $bereavement = \DB::SELECT("select relation_to_deceased from  leave_bereavement,leave_bereavement_detail where department = $department and applied_for =  $localit and leave_bereavement.id = leave_bereavement_detail.bereavement_id");

                if($bereavement == null){
                    return $request->getMessage("failed",'leave bereavement not set up',500,$access[3]);  
                }else{
                
                    return  $request->index($bereavement,$access[3]); 
                }
            }
         }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        } 
     }

    public  function request_remaining(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $input = \Input::all();
            $name = \Input::get('name');
            $type = \Input::get('LeaveType');
            $now  =  date('Y-m-d');

            $rule = [
                'name' => 'required',
                'LeaveType' => 'required',
                ];


            $validation = \Validator::make($input,$rule);

            if($validation->fails()){
                $check = $validation->errors()->all();
                return $request->getMessage("failed",$check,500,$access[3]);
            }else{
                $type  = \Input::get('LeaveType');
                $name = \Input::get('name');

                //check type by categories


                //Training Period , Probationary Period, contract_start, Fixed Term Contract;
                //if vacation
                if($type === "Vacation Leave"){
                    $checkuser =  \DB::SELECT("select * from emp where employee_id  = '$name' ");
                    $department =  $checkuser[0]->department;
                    $applied_for = $checkuser[0]->local_it;
                    $applied_for =  ($applied_for !=  1 ? 2: 1);



                    //check from database leave_vacation
                    $vacation  = \DB::SELECT("select entitlement_start from leave_vacation, leave_vacation_detail where leave_vacation.id  =  leave_vacation_detail.vacation_id and department = $department and applied_for =  $applied_for limit 1");
                    if(isset($vacation) and $vacation != null){
                        $entitle =   $vacation[0]->entitlement_start;

                        if($entitle == 'Training Period'){ $start_date  =  \DB::SELECT("select training_agreement as start_date from job_history where employee_id = '$name' "); }
                        if($entitle == 'Probationary Period'){ $start_date  =  \DB::SELECT("select probationary_start as start_date from job_history where employee_id = '$name' "); }
                        if($entitle == 'contract_start'){ $start_date  =  \DB::SELECT("select contract_start as start_date, emp_contract_start as start_from, emp_contract_end as end_from from job_history where employee_id = '$name' "); }
                        if($entitle == 'Fixed Term Contract'){ $start_date  =  \DB::SELECT("select fixed_term_contract as start_date from job_history where employee_id = '$name' "); }

                        //check if isset
                        if(isset($start_date[0]->start_from) and $start_date[0]->start_from != null and isset($start_date[0]->end_from) and $start_date[0]->end_from){
                           
                            if($start_from[0]->end_from < $now){
                                $data = []; $message =  'your contract has been expire';
                            }else{
                                if(isset($start_from[0]->start_date) and $start_from[0]->start_date != null){
                                     $date_now  = date_create($now);
                                     $date_new =  date_create($start_from[0]->start_date);
                                     $interval  = date_diff($date_now,$date_new);
                                     $interval  = $interval->format('%y');
                                     $year_now =  date('Y');
                                     //check if expat 
                                     //** check schedule  
                                     if($applied_for != 1){
                                        $interval  = ($interval  ==  0 ? 1 : $interval);
                                       
                                        $taken_day =   \DB::SELECT("select sum(taken_day)as  taken_day from leave_request where leave_type  = 2 and employee_id = '$name' and year(from_) =  '$year_now' and  year(to_) =  '$year_now' ");

                                        $schedule  = \DB::SELECT("select * from att_schedule where employee_id =  '$name' and  YEAR(date) = '$year_now' and MONTH(date) = 1");
                                        if($schedule != null){  
                                        
                                            //user already request  change do /vacation leave_vacation  
                                            $personal  = \DB::SELECT("select * from leave_table_personal  where employee_id = '$name' and year = '$year_now' ");
                                            if(isset($personal)  and $personal != null){
                                                $vacation_day  =  $personal[0]->vacation;
                                            }else{
                                                $check_leave_table  = \DB::SELECT("select days, offday_oncall from  leave_vacation,leave_vacation_detail where leave_vacation.id =   leave_vacation_detail.vacation_id 
                                                                                   and department = $department and applied_for =  $applied_for and year = $interval");
                                                if(isset($check_leave_table) and $check_leave_table != null){
                                                    $vacation_day = $check_leave_table[0]->days;
                                                }else{
                                                    $data = []; $message  = $message = 'leave request not provide 1 for you';
                                                }
                                            }
                                                //$date = ["last_date" => $last_date, "max_date_req" => $max_date_req, "for_request" => $for_request];

                                            $total_day_left  =  $vacation_day -  $taken_day[0]->taken_day;
                                            $last_date =  \DB::SELECT("select created_at from leave_request where leave_type =  2 and employee_id =  $employee_id and year(from_) = '$year_now' and  year(to_) = '$year_now'");
                                            $max_date_req = date("Y-m-d", strtotime("$last_date[0]->created_at +4 month"));
                                            $for_request = date("Y-m-d", strtotime("$last_date[0]->created_at +1 month"));
                                        }else{     
                                            $get_start_from = \DB::SELECT("select month(date) as month from att_schedule  from year(date) = '$year_now' ASC LIMIT 1");
                                            $get_start_from  =  $get_start_from[0]->month;

                                            $get_start_from =  (((12-$get_start_from) * 24) / 12);
                                            $total_day_left  =  $get_start_from -  $taken_day[0]->taken_day;
                                            $last_date =  \DB::SELECT("select created_at from leave_request where leave_type =  2 and employee_id =  $employee_id and year(from_) = '$year_now' and  year(to_) = '$year_now'");
                                            $max_date_req = date("Y-m-d", strtotime("$last_date[0]->created_at +4 month"));
                                            $for_request = date("Y-m-d", strtotime("$last_date[0]->created_at +1 month")); 
                                        }
                                     }else{
                                     //if not expat
                                        $personal  = \DB::SELECT("select * from leave_table_personal  where employee_id = '$name' and year = '$year_now' ");
                                        if(isset($personal)  and $personal != null){
                                            $vacation_day  =  $personal[0]->vacation;
                                        }else{
                                            return $check_leave_table  = \DB::SELECT("select days, offday_oncall from  leave_vacation,leave_vacation_detail where leave_vacation.id =   leave_vacation_detail.vacation_id 
                                                                                   and department = $department and applied_for =  $applied_for and year = $interval");
                                            if(isset($check_leave_table) and $check_leave_table != null){
                                                $vacation_day = $check_leave_table[0]->days;
                                            }else{
                                                $data = []; $message  = $message = 'leave request not provide for you';
                                            }
                                        }
                                    }
                                }else{
                                    $data  = []; $message  = 'not found';
                                }
                            }
                        }else{
                            
                                 $date_now  = date_create($now);
                                 $date_new =  date_create($start_date[0]->start_date);
                                 $interval  = date_diff($date_now,$date_new);
                                 $interval  = $interval->format('%y');
                                 $year_now =  date('Y');
                                 //check if expat 
                                 //** check schedule  
                                 if($applied_for != 1){
                                    $interval  = ($interval  ==  0 ? 1 : $interval);
                                   
                                    $taken_day =   \DB::SELECT("select sum(taken_day)as  taken_day from leave_request where leave_type  = 2 and employee_id = '$name' and year(from_) =  '$year_now' and  year(to_) =  '$year_now' ");

                                    $schedule  = \DB::SELECT("select * from att_schedule where employee_id =  '$name' and  YEAR(date) = '$year_now' and MONTH(date) = 1");
                                    if($schedule != null){
                                    
                                        //user already request  change do /vacation leave_vacation  
                                        $personal  = \DB::SELECT("select * from leave_table_personal  where employee_id = '$name' and year = '$year_now' ");
                                        if(isset($personal)  and $personal != null){
                                            $vacation_day  =  $personal[0]->vacation;
                                        }else{
                                            $check_leave_table  = \DB::SELECT("select days, offday_oncall from  leave_vacation,leave_vacation_detail where leave_vacation.id =   leave_vacation_detail.vacation_id 
                                                                               and department = $department and applied_for =  $applied_for and year = $interval");
                                            if(isset($check_leave_table) and $check_leave_table != null){
                                                $vacation_day = $check_leave_table[0]->days;
                                            }else{
                                                $data = []; $message  = $message = 'leave request not provide for you';
                                            }
                                        }
                                            //$date = ["last_date" => $last_date, "max_date_req" => $max_date_req, "for_request" => $for_request];

                                        $total_day_left  =  $vacation_day -  $taken_day[0]->taken_day;
                                        $last_date =  \DB::SELECT("select created_at from leave_request where leave_type =  2 and employee_id =  $employee_id and year(from_) = '$year_now' and  year(to_) = '$year_now'");
                                        $max_date_req = date("Y-m-d", strtotime("$last_date[0]->created_at +4 month"));
                                        $for_request = date("Y-m-d", strtotime("$last_date[0]->created_at +1 month"));
                                    }else{     
                                        $get_start_from = \DB::SELECT("select month(date) as month from att_schedule  from year(date) = '$year_now' ASC LIMIT 1");
                                        $get_start_from  =  $get_start_from[0]->month;

                                        $get_start_from =  (((12-$get_start_from) * 24) / 12);
                                        $total_day_left  =  $get_start_from -  $taken_day[0]->taken_day;
                                        $last_date =  \DB::SELECT("select created_at from leave_request where leave_type =  2 and employee_id =  $employee_id and year(from_) = '$year_now' and  year(to_) = '$year_now'");
                                        $max_date_req = date("Y-m-d", strtotime("$last_date[0]->created_at +4 month"));
                                        $for_request = date("Y-m-d", strtotime("$last_date[0]->created_at +1 month")); 
                                    }
                                 }else{
                                 //if not expat
                                    $personal  = \DB::SELECT("select * from leave_table_personal  where employee_id = '$name' and year = '$year_now' ");
                                    if(isset($personal)  and $personal != null){
                                        $vacation_day  =  $personal[0]->vacation;
                                    }else{
                                        $check_leave_table  = \DB::SELECT("select days, offday_oncall from  leave_vacation,leave_vacation_detail where leave_vacation.id =   leave_vacation_detail.vacation_id 
                                                                           and department = $department and applied_for =  $applied_for and year = $interval");
                                        if(isset($check_leave_table) and $check_leave_table != null){
                                            $vacation_day = $check_leave_table[0]->days;
                                        }else{
                                            $data = []; $message  = $message = 'leave request not provide for you';
                                        }
                                    }
                                        //$date = ["last_date" => $last_date, "max_date_req" => $max_date_req, "for_request" => $for_request];

                                    $total_day_left  =  $vacation_day -  $taken_day[0]->taken_day;
                                    $last_date =  \DB::SELECT("select created_at from leave_request where leave_type =  2 and employee_id =  $employee_id and year(from_) = '$year_now' and  year(to_) = '$year_now'");
                                    $max_date_req = date("Y-m-d", strtotime("$last_date[0]->created_at +4 month"));
                                    $for_request = date("Y-m-d", strtotime("$last_date[0]->created_at +1 month"));
                                }
                        }
                    }else{

                        $data = []; $message = 'leave request not provide for you';
                    }
                }else{
                    $data  = []; $message  = "test";
                }
            }
                
            return $arr = ['data' => $data, "message" => $message];
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }




    public  function request_remaining1(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $input = \Input::all();
            $name = \Input::get('name');
            $type = \Input::get('LeaveType');

            $rule = [
                'name' => 'required',
                'LeaveType' => 'required',
                ];


            $validation = \Validator::make($input,$rule);

            if($validation->fails()){
                $check = $validation->errors()->all();
                return $request->getMessage("failed",$check,500,$access[3]);
            }else{

                $now = date("Y-m-d");
                $remaining = '';
                //---------------------------------vacation
                if($type == "Vacation Leave" ){

                    $cal = \DB::SELECT("select * from all_vacation where empx = '$name' order by ids DESC limit 1");
                    if($cal != null){
                        //return $request->getMessage("failed",'job type not found for this user',500,$access[3]); 
                        $remaining =  $cal[0]->total_days;
                       
                    
                    }else{

                        $arrs = [];
                        foreach ($cal as $key => $value) {
                            if($value->empx == $name && $value->leave_type == $type &&  $value->status == 2) {
                                $arrs[] =  $cal[$key];
                            }                   
                        }

                        if($arrs != null and end($arrs)->status != 1){
                         
                            $final_data = end($arrs);
                            $remaining = $final_data->days - $final_data->balance_leaves;                       
                        }else{
                                //local
                            $base_emp = \DB::SELECT("select  * from emp where employee_id = '$name' ");
                            $localit = $base_emp[0]->local_it;
                            if(!isset($base_emp[0]->department) || $base_emp[0]->department == null){
                                return $request->getMessage('department not found',[],500,$access[3]);
                            }else{
                                $department = $base_emp[0]->department;
                            }
                               
                            //check contract
                            $select_contract =  \DB::SELECT("select * from leave_vacation where applied_for = $localit and department = $department ");
                            if(!isset($select_contract[0]->entitlement_start)){
                                 return $request->getMessage("failed not found leave table",[],500,$access[3]);
                            }else{
                                $contract = $select_contract[0]->entitlement_start;    
                            }
                            
                            if($contract == 'Annual Contract'){
                                $contract  =  "Fixed Term Contract";
                            }

                            if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$name' "); }
                            if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$name' "); }
                            if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$name' "); }
                            if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$name' "); }

                            $contract_start =  $contract_start[0]->date;
                            $valRet = date_diff(date_create($now),date_create($contract_start));
                            $valRet = $valRet->format("%a");
                         
                            if($localit == 2 || $localit == 3 )
                            {
                                $job_history = \DB::SELECT("select * from job_history where employee_id = '$name'");
                                if(isset($job_history[0]->job) || $job_history[0]->job != null ){
                                    $job_history = $job_history[0]->job;
                                    $tenure = ( $valRet >= 270 && $valRet <= 364 ? 1 : ceil($valRet/365));
                                    $select_contract =  \DB::SELECT("select days from leave_vacation,leave_vacation_detail where leave_vacation.applied_for = $localit and leave_vacation.department = $department and leave_vacation.id = leave_vacation_detail.vacation_id and leave_vacation_detail.year =  $tenure ");
                                    $remaining = $select_contract[0]->days;
                                }else{
                                    return $request->getMessage("failed",'job type not found for this user',500,$access[3]);  
                                }
                            }else{
                                $job_history = \DB::SELECT("select * from job_history where employee_id = '$name'");
                                    if(isset($job_history[0]->job) || $job_history[0]->job != null ){
                                        $job_history = $job_history[0]->job;
                                        if($valRet < 365){
                                           
                                            $explode_contract = explode("-",$contract_start);
                                            $explode_contract = intval($explode_contract[1]);

                                            $remaining = ($explode_contract * 24) / 12;
                                        }else{
                                            //$tenure = ceil($valRet/365);
                                            $tenure = ( $valRet >= 270 && $valRet <= 364 ? 1 : ceil($valRet/365));
                                        }
                                        //$tenure = ( $valRet >= 270 && $valRet <= 364 ? 1 : ceil($valRet/365));
                                        
                                        $select_contract =  \DB::SELECT("select days from leave_vacation,leave_vacation_detail where leave_vacation.applied_for = $localit and leave_vacation.department = $department and leave_vacation.id = leave_vacation_detail.vacation_id and leave_vacation_detail.year =  $tenure  ");
                                        $remaining = $select_contract[0]->days;
                                    }else{
                                        return $request->getMessage("failed",'job type not found for this user',500,$access[3]);  
                                    }
                            }
                        }
                        
                    }
                }

                //---------------------------------birthday
                if($type == "Birthday Leave"){
                    $year = date('Y');
                    $arr =[];
                    $check = \DB::SELECT("select * from leave_request where employee_id = '$name' and leave_type = 1");
                    if(isset($check) and $check != null ){
                        foreach ($check as $key => $values){
                             if($values->leave_type  == 1 and $values->employee_id == $name ){
                                    $arr[] = $check[$key];
                             }
                        }
                        if($arr != null){
                            foreach ($arr as $key => $value) {
                                 $explode = explode('-',$value->from_);
                                    if($explode[0] ==  $year){
                                        $remaining = 0;
                                    }
                            }
                           
                        }else{
                            $remaining = 1;
                        }
                    }else{
                        $remaining = 1;
                    }
                }
                
                //---------------------------------"Enhance Vacation Leave"
                if($type == "Enhance Vacation Leave"){
                    $job = \Db::SELECT("select job from job_history where employee_id = '$name' ");
                    if($job == null){
                         return $request->getMessage("please insert job title first",[],500,$access[3]);
                    }else{
                        $jobs  = $job[0]->job;
                        $select_tabel = \DB::SELECT("select * from leave_enhance where job_tittle = $jobs");
                        if(!isset($select_tabel[0]->days) ){
                             return $request->getMessage("failed please set up for leave first",[],500,$access[3]);
                        }else{
                            $cal = \DB::SELECT("CALL view_all_leave_balance()");
                            if($cal != null){
                                $arr = [];
                                foreach ($cal as $key => $value) {
                                    if($value->empx ==  $name && $value->leave_type == "Enhance Vacation Leave"  ){
                                        $arr[] =  $cal[$key];
                                    }
                                }
                            }

                            if(isset($arr) && $arr != null){
                                $end = end($arr);
                                $remaining = $end->days - $end->balance_leaves;
                            }else{
                            
                                $remaining = $select_tabel[0]->days;
                            }
                            
                        }
                    }  
                }


                //-----------------------------------"Sick Leave"
                if($type == "Sick Leave"){
                    //$cal = \DB::SELECT("CALL view_all_leave_balance()");
                    $cal = \DB::SELECT("select * from all_sick where empx = '$name' order by ids DESC limit 1 ");
                    if($cal == null){
                            //local
                            $base_emp = \DB::SELECT("select  * from emp where employee_id = '$name' ");
                            $localit = $base_emp[0]->local_it;
                            if(!isset($base_emp[0]->department) || $base_emp[0]->department == null){
                                return $request->getMessage("failed",'department not found',500,$access[3]);
                            }else{
                                $department = $base_emp[0]->department;
                            }

                            //check contract
                            $select_contract =  \DB::SELECT("select * from leave_sick where applied_for = $localit and department = $department ");
                            $contract = $select_contract[0]->entitlement_start;

                            if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$name' "); }
                            if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$name' "); }
                            if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$name' "); }
                            if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$name' "); }

                            $contract_start =  $contract_start[0]->date;
                            $valRet = date_diff(date_create($now),date_create($contract_start));
                            $valRet = $valRet->format("%a");

                            $job_history = \DB::SELECT("select * from job_history where employee_id = '$name'");
                            if(isset($job_history[0]->job) || $job_history[0]->job != null ){
                                $job_history = $job_history[0]->job;
                                $tenure = ( $valRet >= 270 && $valRet <= 364 ? 1 : ceil($valRet/365));
                                $select_contract =  \DB::SELECT("select days from leave_sick,leave_sick_detail where leave_sick.applied_for = $localit and leave_sick.department = $department and leave_sick.id = leave_sick_detail.leave_sick_id and leave_sick_detail.year = $tenure ");
                                if(isset($select_contract[0]->days)){
                                    $remaining = $select_contract[0]->days;
                                }else{
                                    return $request->getMessage('remaining day not found',[],500,$access[3]);    
                                }
                            }else{
                                return $request->getMessage('job title not found for this user',[],500,$access[3]);  
                            }
                    
                    }else{
                        // $arrs = [];
                        // foreach ($cal as $key => $value) {
                        //  if($value->empx == $name and $value->leave_type == $type && $value->status == 2 ) {
                        //      $arrs[] =  $cal[$key];
                        //  }                   
                        // }
                        
                        // if($arrs != null and end($arrs)->status != 1){
                        //  $final_data = end($arrs);
                     //     $remaining = $final_data->days - $final_data->balance_leaves;                       
                        // }else{
                             // $base_emp = \DB::SELECT("select  * from emp where employee_id = '$name' ");
                       //      $localit = $base_emp[0]->local_it;
                       //      if(!isset($base_emp[0]->department) || $base_emp[0]->department == null){
                       //          return $request->getMessage('department not found',"failed",500,$access[3]);
                       //      }else{
                       //          $department = $base_emp[0]->department;
                       //      }

                       //      //check contract
                       //      $select_contract =  \DB::SELECT("select * from leave_sick where applied_for = $localit and department = $department ");
                       //      if(isset($select_contract[0]->entitlement_start)){
                       //          $contract = $select_contract[0]->entitlement_start;
                       //      }else{
                       //          return \Response::json(['header'=>['message'=>'please set up leave_sick in leave table or contact your admin','status'=>500],'data'=>[], "access"=>$access[3]],500);
                       //          //return $request->getMessage("failed",'please set up leave_sick in leave table or contact your admin',500,$access[3]);  
                       //      }
                            $select_contract =  \DB::SELECT("select * from leave_sick where applied_for = (select local_it from emp where employee_id = '$name' ) and department = (select department from emp where employee_id = '$name' )");
                            $contract = $select_contract[0]->entitlement_start;

                            if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$name' "); }
                            if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$name' "); }
                            if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$name' "); }
                            if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$name' "); }

                            $contract_start =  $contract_start[0]->date;
                            $valRet = date_diff(date_create($now),date_create($contract_start));
                            $valRet = $valRet->format("%a");

                            $job_history = \DB::SELECT("select * from job_history where employee_id = '$name'");
                            if(isset($job_history[0]->job) || $job_history[0]->job != null ){
                                //$job_history = $job_history[0]->job;
                                // $tenure = ( $valRet >= 270 && $valRet <= 364 ? 1 : ceil($valRet/365));
                                // $select_contract =  \DB::SELECT("select days from leave_sick,leave_sick_detail where leave_sick.applied_for = $localit and leave_sick.department = $department and leave_sick.id = leave_sick_detail.leave_sick_id");
                                 $remaining =  $cal[0]->days_vl_expat;
                            }else{
                                return $request->getMessage("failed",'job title not found for this user',500,$access[3]);  
                            }
                        } 
                    }                    
                    

                //-----------------------------------"Maternity Leave"
                if($type ==  "Maternity Leave"){
                    $select = \DB::SELECT("select * from emp where employee_id =  '$name'");

                    if(isset($select[0]) && $select[0]->gender == "male"){
                            return $request->getMessage("only for female",[],500,$access[3]);
                    }else{
                        $check = \DB::SELECT("select * from leave_request  where employee_id = '$name' and leave_type = 5 and status_id = 2");
                            if($check != null){
                                $date = $check[0]->from_;
                                $explode = explode("-", $date);
                                $now = date('Y');

                                if($explode[0] == $now){
                                    return $request->getMessage('already taken for this year',[],500,$access[3]);
                                    //return $request->getMessage('already taken for this year',[],500,$access[3]);
                                }
                            }else{
                                    $remaining = 150;
                            }
                    }
                     
                  
                }

                //----------------------------------"Paternity Leave"
                if($type == "Paternity Leave"){
                    $base_emp = \DB::SELECT("select  * from emp where employee_id = '$name' ");
                    $localit = $base_emp[0]->local_it;

                    $select = \DB::SELECT("select * from emp where employee_id =  '$name'");
                    if(isset($select[0]) && $select[0]->gender != "male"){
                        return \Response::json(['header'=>['message'=>'only for expectant fathers','status'=>500],'data'=>[], "access"=>$access[3]],500);
                        //return $request->getMessage("failed",'only for expectant fathers',500,$access[3]);  
                    }
                     $cal = \DB::SELECT("CALL view_all_leave_balance()");
                     $arr = [];

                     if($cal != null){
                        foreach ($cal as $key => $value) {
                            if($value->empx == '$name'  and $value == "Vacation Leave"){
                                $arr[] = $cal[$key];
                            }
                        }            
                    }
                 
                    if($cal == null){

                        //local
                        $base_emp = \DB::SELECT("select  * from emp where employee_id = '$name' ");
                        $localit = $base_emp[0]->local_it;
                        if(!isset($base_emp) || $base_emp == null){
                            return $request->getMessage("failed",'department not found',500,$access[3]);
                        }else{
                            $department = $base_emp[0]->department;
                        }

                        //check contract
                        $select_contract =  \DB::SELECT("select * from leave_vacation where applied_for = $localit and department = $department ");
                        $contract = $select_contract[0]-> entitlement_start;

                        if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$name' "); }
                        if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$name' "); }
                        if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$name' "); }
                        if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$name' "); }

                        $contract_start =  $contract_start[0]->date;
                        $valRet = date_diff(date_create($now),date_create($contract_start));
                        $valRet = $valRet->format("%a");

                        if($localit == 2 || $localit == 3 )
                        {
                            $job_history = \DB::SELECT("select * from job_history where employee_id = '$name' ");
                            if(isset($job_history[0]->job) || $job_history[0]->job != null ){
                                $job_history = $job_history[0]->job;
                                $tenure = ( $valRet >= 270 && $valRet <= 364 ? 1 : ceil($valRet/365));
                                $select_contract =  \DB::SELECT("select days from leave_vacation,leave_vacation_detail where leave_vacation.applied_for = $localit and leave_vacation.department = $department and leave_vacation.id = leave_vacation_detail.vacation_id ");
                                $remaining = $select_contract[0]->days;
                            }else{
                                return $request->getMessage("failed",'job type not found for this user',500,$access[3]);  
                            }
                        }else{
                            $job_history = \DB::SELECT("select * from job_history where employee_id = '$name'");
                            if(isset($job_history[0]->job) || $job_history[0]->job != null ){
                                $job_history = $job_history[0]->job;
                                if($valRet < 365){
                                    $explode_contract = explode("-",$contract_start);
                                    $explode_contract = intval($explode_contract[1]);

                                    $tenure = ($explode_contract * 24) / 12;
                                }else{
                                    $tenure = ceil($valRet/365);
                                }
                                $tenure = ( $valRet >= 270 && $valRet <= 364 ? 1 : ceil($valRet/365));
                                $select_contract =  \DB::SELECT("select days from leave_vacation,leave_vacation_detail where leave_vacation.applied_for = $localit and leave_vacation.department = $department and leave_vacation.id = leave_vacation_detail.vacation_id ");
                                $remaining = $select_contract[0]->days;
                            }else{
                                $request->getMessage("failed",'job type not found for this user',500,$access[3]);  
                            }
                        }
                    
                    }else{
                        $final_data = end($cal);
                        $remaining = $final_data->days - $final_data->balance_leaves;                      
                    }

                    if($remaining != 0 || $remaining <= 5){
                        if($arr != null){
                            $arrs = end($arr);
                            $total =  5 + ($arr->days - $arr->balance_leaves);
                        }else{
                            $total = 5 + 7;
                        }
                    }

                    $remaining = $total;
                }

                if($type == "Marriage Leave"){
                    $emp = \DB::SELECT("select *from emp where employee_id = '$name' ");
                    $expat = \DB::SELECT("select * from vacation_leave_expat where empx = '$name' ");
                    $select = \DB::SELECT("CALL  view_all_leave_balance() ");
                    $arr = [];

                    $local = $emp[0]->local_it;

                    if($select != null){
                        foreach ($select as $key => $value) {
                            if($value->empx == $name and $value->status == 2 && $value->leave_type == "Vacation Leave" ){
                                $arr[] = $select[$key];
                            } 
                        }
                    }
                    if(isset($arr) and $arr != null){
                        $end = end($arr);
                        $vacation = $end->days -  $end->balance_leaves;
                        if($vacation > 12){
                            $vacation = 12;
                        }
                    }else{
                        $vacation = 0;
                    }            
                        if($local == 1){
                            if($vacation != 0){
                                $remaining = 7 + $vacation;
                            }else{
                                $remaining = 10;
                            }
                        }else{
                            $remaining = 10;
                        }
                }


                if($type == "Emergency Leave"){
                    $remaining = "Not Limited";
                }

                if($type == "offday_oncall"){
                    $remaining = 1;
                }



                $supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$name' ");
                if($supervisor != null){
                    $supervisor = "supervisor";
                }else{
                    $supervisor =  "user";
                }


                $last_date = \DB::SELECT("select leave_request.from_,leave_request.created_at  from leave_request,leave_type where leave_request.employee_id= '$name' and leave_request.leave_type = leave_type.id and leave_type.leave_type = '$type' ");
                if(isset($last_date) && $last_date != null){
                    if($last_date[0]->from_ != null){
                        $last_date = $last_date[0]->created_at;
                        $max_date_req = date("Y-m-d", strtotime("$last_date +4 month"));
                        $for_request = date("Y-m-d", strtotime("$last_date +1 month"));
                    }
                }else{
                        //$today = date("Y-m-d");
                        //$last_date = date("Y-m-d", strtotime("$today +1 month"));
                        $last_date = '-' ;
                        $max_date_req = "-";
                        $for_request = "-";
                   
                }

                $date = ["last_date" => $last_date, "max_date_req" => $max_date_req, "for_request" => $for_request];
                $data_exp= [
                        "remaining_day" => $remaining,
                        "status" => $supervisor,
                        "date" => $date
                      ];
                

                return $request->getMessage("success",$data_exp,200,$access[3]);
            }
         
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }

    }

    public function insert_ado($data,$type_id,$name){
      $employee_id = $data['employee_id'];
      $ado1 =  (isset($data['ado1']) && $data['ado1'] != null ? $data['ado1'] : 0 );
      $ado2 =  (isset($data['ado2']) && $data['ado2'] != null ? $data['ado2'] : 0 );
      $ado3 =  (isset($data['ado3']) && $data['ado3'] != null ? $data['ado3'] : 0 );
      $ado4 =  (isset($data['ado4']) && $data['ado4'] != null ? $data['ado4'] : 0 ); 

      $date1 =  (isset($data['date1']) && $data['date1'] != null ? $data['date1'] : 0 );
      $date2 =  (isset($data['date2']) && $data['date2'] != null ? $data['date2'] : 0 );
      $date3 =  (isset($data['date3']) && $data['date3'] != null ? $data['date3'] : 0 );
      $date4 =  (isset($data['date4']) && $data['date4'] != null ? $data['date4'] : 0 );       

      $datev1 =  (isset($data['datev1']) && $data['datev1'] != null ? $data['datev1'] : 0 );
      $datev2 =  (isset($data['datev2']) && $data['datev2'] != null ? $data['datev2'] : 0 );
      $datev3 =  (isset($data['datev3']) && $data['datev3'] != null ? $data['datev3'] : 0 );
      $datev4 =  (isset($data['datev4']) && $data['datev4'] != null ? $data['datev4'] : 0 ); 
    
       try{
          if($date1 != 0 && $datev1 != 0 && $ado1 != 0){
            \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,comment) values('$employee_id',10,'$date1','$datev1','$ado1')");
          }
          if($date2 != 0 && $datev2 != 0 && $ado2 != 0){
            \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,comment) values('$employee_id',10,'$date2','$datev2','$ado2')");
          }
          if($date3 != 0 && $datev3 != 0 && $ado3 != 0){
            \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,comment) values('$employee_id',10,'$date3','$datev3','$ado3')");
          }
          if($date4 != 0 && $datev4 != 0 && $ado4 != 0){
            \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,comment) values('$employee_id',10,'$date4','$datev4','$ado4')");
          }
        }catch (Exception $e) {
            return $e;
        }
    }



    public  function request_leave(){

        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $year = date('Y');
            $request = new leaverequest_Model;
            $json = \Input::get("data");
            $file = \Input::file('file');
            $data = json_decode($json,1);

            if(isset($json)){
                if(isset($data['data']['LeaveType'])){
                    $type =  $data['data']['LeaveType'];
                    $leave_type = \DB::select("select leave_type,id from leave_type where leave_type='$type' ");
                    $type_id = $leave_type[0]->id;
                }else{
                    return $request->getMessage("please select leave type first",[],500,$access[3]);
                }

                if(isset($data['data']['name'])){
                    $name =  $data['data']['name'];
                }else{
                    $name = null;
                }

                if(isset($data['data']['remaining_day'])){
                    $remaining_day =  $data['data']['remaining_day'];
                }else{
                    $remaining_day = null;
                }
                
                if($type != "Accumulation Day Off"){
                    if(isset($data['data']['name']) && isset($data['data']['From']) && isset($data['data']['To'])){
                        $from =  $data['data']['From'];
                        $to = $data['data']['To'];
                    }else{
                        return $request->getMessage("Please fill form data before submit data",[],500,$access[3]);
                    }
                }

                if($type != "Accumulation Day Off"){
                    if(isset($data['data']['DayOffBetween'])){
                        $dob = $data['data']['DayOffBetween'];
                    }else{
                        return $request->getMessage("Day Off Between not found",[],500,$access[3]);
                    }
                }

                if(!isset($data['data']['aggred'])){
                    return $request->getMessage("please check form aggrement before submit data",[],500,$access[3]);
                }else{
                    $aggree = $data['data']['aggred'];
                }

                if(isset($data['data']['comment'])){
                    $comment = $data['data']['comment'];
                }else{
                        $comment = NULL;
                }

                if(isset($data['data']['LeaveBrea'])){
                    $breave = $data['data']['LeaveBrea'];
                }else{
                    $breave = NULL;
                }
                (isset($data['data']['IncludeAdo']) ? $ado_x = $data['data']['IncludeAdo'] : $ado_x = "No" );
            }else{
                $data = \Input::all();
                if(isset($data['LeaveType'])){
                    $type =  $data['LeaveType'];
                    $leave_type = \DB::select("select leave_type,id from leave_type where leave_type='$type' ");
                    $type_id = $leave_type[0]->id;
                }else{
                    return $request->getMessage("please choose of the item in LeaveType",[],500,$access[3]);
                }

                if(isset($data['name'])){
                  $remaining_day =  $data['remaining_day'];
                }else{
                     $remaining_day = null;
                }

                if(isset($data['name'])){
                  $name =  $data['name'];
                }else{
                  return $request->getMessage("please insert name before submit data",[],500,$access[3]);
                }

                if($type !=  "Accumulation Day Off"){
                    if(isset($data['From']) && isset($data['To']) ){
                        $from =  $data['From'];
                        $to = $data['To'];
                    }else{
                        return $request->getMessage("please fill out all data before submit data ",[],500,$access[3]);
                    }
                }


                if(isset($data['DayOffBetween'])){
                    $dob = $data['DayOffBetween'];
                }else{
                     $dob = NULL;
                }

                if(!isset($data['aggred'])){
                    return $request->getMessage("please check form aggrement before submit data",[],500,$access[3]);
                }else{
                    $aggree = $data['aggred'];
                }

                if(isset($data['comment'])){
                    $comment = $data['comment'];
                }else{
                    $comment = NULL;
                }

                if(isset($data['LeaveBrea'])){
                    $breave = $data['LeaveBrea'];
                }else{
                    $breave = NULL;
                }
                
                (isset($data['IncludeAdo']) ? $ado_x = $data['IncludeAdo'] : $ado_x = "No" );
            }

            if(isset($json)){
                    $data = json_decode($json,1);
                    $data = $data['data'];
            }
             
             if(isset($dob)){
                $dob = $dob;
                $select_emp = \DB::SELECT("select * from emp where employee_id = '$name' ");
                $lc = $select_emp[0]->local_it;
            }
             //return $type;
            if($type == "Accumulation Day Off"){
                return $this->insert_ado($data,$type_id,$name);
            }


             $message = json_encode($this->domain($name,$from,$to,$dob,$type));
             if($message == '"ok"'){
                
                $rename = '';
                $path = "hrms_upload/leave_request";

                $retVal =   ($type == 'Birthday Leave' ? 1 : 
                        ($type == 'Vacation Leave' ? 2 : (
                            $type == 'Enhance Vacation Leave' ? 3 : (
                                $type == 'Sick Leave' ? 4 : (
                                    $type == "Maternity Leave" ? 5: (
                                    $type ==  'Paternity Leave' ? 6 : (
                                $type == 'Bereavement Leave' ? 7 : (
                            $type == 'Marriage Leave' ? 8 : (
                        $type == 'offday_oncall' ? 9 : (
                        $type == 'Suspension' ? 12 : (
                    $type == 'Accumulation Day offday_oncall' ? 'adoLeave' : 'emerLeave')))))))))));

                if($retVal == 'emerLeave'){
                    $retVal = 11;
                }
                $check_already_exist = \DB::SELECT("select * from leave_request where employee_id = '$name' and from_ = '$from' and to_ = '$to' and leave_type = $retVal");

                if($check_already_exist != null){

                        $messagex = "request already exist";
                        $status = 500;
                        return \Response::json(['header'=>['message'=>$messagex,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);          
                }

                if(isset($file) && $file != null){
                     $validation = \Validator::make(['file' => $file] , ['file' => 'required|max:1000']);
                     if($validation->fails()){
                        $messagex = "file more than 1 MB";
                        $status = 500;
                     return \Response::json(['header'=>['message'=>$messagex,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);   
                    } 
                }
                
                if(isset($file) && $file != null){    
                    $rename = "leave_request__".str_random(6).".".$file->getClientOriginalExtension();
                    \Input::file('file')->move(storage_path($path),$rename);
                }

                if($type_id == 7){
                    if($breave == null){
                        $messagex = "Please choose one of items for leave beravement";
                        $status = 500;
                        return \Response::json(['header'=>['message'=>$messagex,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);
                    }else{
                        $db_data = \DB::SELECT("insert into leave_request (employee_id,leave_type,from_,to_,dayoff_between,status_id,comment) values ('$name',$type_id,'$from','$to',$dob,1,'$breave')");
                        $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                        $data_id_ex = $data_ex[0]->id;
                        $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$rename','$path','$comment')");
                            
                        $messagex = "Success apply";
                        $status = 200;
                        return \Response::json(['header'=>['message'=>$messagex,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);
                    }
                }elseif($type_id == 6){
                    if($dob > $remaining_day){
                        $messagex = "Please set up days not more than".$remaining_day;
                        $status = 500;
                    }else{
                        if($dob > 7 == 1){
                            $db_data = \DB::SELECT(" insert into leave_request (employee_id,leave_type,from_,to_,dayoff_between,status_id) values ('$name',6,'$from','$to',$dob,1)"); 
                            $remaining_day =  $remaining_day - $dob;
                            $db_data = \DB::SELECT(" insert into leave_request (employee_id,leave_type,from_,to_,dayoff_between,status_id) values ('$name',2,'$from','$to',$remaining_day,1)");

                            $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                            $data_id_ex = $data_ex[0]->id;
                            $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$rename','$path','$comment')");
                            
                            $messagex = "Success apply";
                            $status = 200;
                            return \Response::json(['header'=>['message'=>$messagex,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);
                        }else{
                            $db_data = \DB::SELECT("insert into leave_request (employee_id,leave_type,from_,to_,dayoff_between,status_id,comment) values ('$name',$type_id,'$from','$to',$dob,1,'$breave')");
                            $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                            $data_id_ex = $data_ex[0]->id;
                            $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$rename','$path','$comment')");
                                
                            $messagex = "Success apply";
                            $status = 200;
                            return \Response::json(['header'=>['message'=>$messagex,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);
                        }
                    }   

                        return \Response::json(['header'=>['message'=>$messagex,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);
                }elseif($type_id == 8 and $lc == 1){
                    if($dob > $remaining_day){
                        $messagex = "Please set up days not more than".$remaining_day;
                        $status = 500;
                    }else{
                        if($dob > 7){
                                $db_data = \DB::SELECT(" insert into leave_request (employee_id,leave_type,from_,to_,dayoff_between,status_id) values ('$name',8,'$from','$to',$dob,1)"); 
                                $remaining_day =  $remaining_day - $dob;
                                $db_data = \DB::SELECT(" insert into leave_request (employee_id,leave_type,from_,to_,dayoff_between,status_id) values ('$name',2,'$from','$to',$remaining_day,1)");

                                $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                                $data_id_ex = $data_ex[0]->id;
                                $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$rename','$path','$comment')");
                                
                                $messagex = "Success apply";
                                $status = 200;
                                return \Response::json(['header'=>['message'=>$messagex,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);
                        }else{
                            $db_data = \DB::SELECT("insert into leave_request (employee_id,leave_type,from_,to_,dayoff_between,status_id,comment) values ('$name',$type_id,'$from','$to',$dob,1,'$breave')");
                            $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                            $data_id_ex = $data_ex[0]->id;
                            $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$rename','$path','$comment')");
                                
                            $messagex = "Success apply";
                            $status = 200;
                            return \Response::json(['header'=>['message'=>$messagex,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);
                        }   

                        return \Response::json(['header'=>['message'=>$messagex,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);
                    }
                }else{
                    if($type_id == 1){
                        $dob = 1;
                    }
                    if($ado_x == "Yes"){
                        $this->insert_ado($data,$type_id,$name);
                        $db_data = \DB::SELECT(" insert into leave_request (employee_id,leave_type,from_,to_,dayoff_between,status_id) values ('$name',$type_id,'$from','$to',$dob,1)"); 
                    }else{
                        
                        if($type ==  'Suspension'){
                            $inf_name = \Input::get('suspension');
                            if(!isset($inf_name) || $inf_name ==  null){
                                 return \Response::json(['header'=>['message'=>'suspension not found','status'=>500],'data'=>[], "access"=>$access[3]],500);
                            }else{
                            $id_inf = \DB::SELECT("select id from infraction where infraction_name = '$inf_name' ");
                            $id_inf = $id_inf[0]->id;
                            $db_data = \DB::SELECT(" insert into leave_request (employee_id,leave_type,from_,to_,dayoff_between,status_id,comment) values ('$name',$type_id,'$from','$to',$dob,1,$id_inf)");
                        
                            }
                        }else{
                      
                            $db_data = \DB::SELECT(" insert into leave_request (employee_id,leave_type,from_,to_,dayoff_between,status_id) values ('$name',$type_id,'$from','$to',$dob,1)"); 
                        }
                    }


                    $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                    $data_id_ex = $data_ex[0]->id;
                    $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$rename','$path','$comment')");
                        
                    $messagex = "Success apply";
                    $status = 200;
                    return \Response::json(['header'=>['message'=>$messagex,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);
                }

                
                
            }else{
                 $status = 500; $data=[];
                 return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>[], "access"=>$access[3]],$status);
            }}

        
    }

    public function domain($employee_id,$from,$to,$dob,$type){
 
        $brain_fuck = \DB::SELECT("CALL view_all_leave_balance");
        $local = \DB::SELECT("select local_it from emp where employee_id = '$employee_id' ");
        $local = $local[0]->local_it;
      
        $arr = [];

        //exploit data get prime employee ------------  
        foreach ($brain_fuck as $key => $value) {
            if($value->leave_type == $type and $value->empx == $employee_id){
                $arr[] = $brain_fuck[$key];
                $local = $value->localit;
            }
        }
        //---------------------------------------------

        $retVal =   ($type == 'Birthday Leave' ? 'birthleave' : 
                        ($type == 'Vacation Leave' ? 'vacLeave' : (
                            $type == 'Enhance Vacation Leave' ? 'enLeave' : (
                                $type == 'Sick Leave' ? 'sickLeave' : (
                                    $type == "Maternity Leave" ? 'maternityLeave': (
                                    $type ==  'Paternity Leave' ? 'patLeave' : (
                                $type == 'Bereavement Leave' ? 'bereavementLeave' : (
                            $type == 'Marriage Leave' ? 'marLeave' : (
                        $type == 'Suspension' ? 'suspen' : (
                        $type == 'offday_oncall' ? 'ocLeave' : (
                    $type == 'Accumulation Day Off' ? 'adoLeave' : 'emerLeave')))))))))));
        
        //----------------------------------------------
        
        if($type == 'Vacation Leave' || $type == 'Sick Leave' ||  $type == 'Bereavement Leave' ){

                $emp = \DB::SELECT("select * from emp where employee_id = $employee_id");
                $department = $emp[0]->department;
                if(!isset($department) || $department == null){ return $message = 'please set up department';}
                else{

                    if($type == "Sick Leave"){$check = \DB::SELECT("select * from leave_sick where department = $department and applied_for = $local ");}
                    if($type == 'Vacation Leave'){$check = \DB::SELECT("select * from leave_vacation where department = $department and applied_for = $local ");}
                    if($type == "Bereavement Leave"){$check = \DB::SELECT("select * from leave_bereavement where department = $department and applied_for = $local ");}

                    if($check == null){
                        return $message = 'leave tabel not set up for department';
                    }elseif(!isset($check[0]->entitlement_start) || $check[0]->entitlement_start != null){

                        $entilited = $check[0]->entitlement_start;
                        if($entilited == "Training Period"){
                            $check_exist = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$employee_id'  ");
                        }elseif($entilited == "contract_start"){
                            $check_exist = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$employee_id' ");
                        }elseif($entilited == "Probationary Period"){
                            $check_exist = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$employee_id' ");
                        }else{
                            $check_exist = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$employee_id' ");
                        }

                       if(isset($check_exist) || $check_exist  != null){
                            $check_exist = $check_exist[0]->date;
                            $now = date('Y-m-d');
                            $date1 = new \DateTime($check_exist);
                            $now = new \DateTime($now);
                            
                            $date2 = date_diff($now, $date1);
                            $date2 = $date2->days;
                            
                            $tenure = ceil($date2/365)-1;
                            if($type == "Sick Leave"){$balance_day = \DB::SELECT("select * from leave_sick,leave_sick_detail where leave_sick.department = $department 
                                                                                  and leave_sick.applied_for = $local 
                                                                                  and leave_sick.id = leave_sick_detail.leave_sick_id 
                                                                                  and leave_sick_detail.year = $tenure ");}
                            if($type == 'Vacation Leave'){$balance_day = \DB::SELECT("select * from leave_vacation,leave_vacation_detail where leave_vacation.department = $department 
                                                                                      and leave_vacation.applied_for = $local
                                                                                      and leave_vacation.id = leave_vacation_detail.vacation_id 
                                                                                      and leave_vacation_detail.year = $tenure");}
                            if($type == "Bereavement Leave"){$balance_day = \DB::SELECT("select * from leave_bereavement,leave_bereavement_detail where leave_bereavement.department = $department 
                                                                                         and leave_bereavement.applied_for = $local 
                                                                                         and leave_bereavement.id = leave_bereavement_detail.bereavement_id
                                                                                         ");}

                            if(!isset($balance_day[0]->days)){
                                return $message = "please set days leave for request in leave table, or contact admin";
                            }else{

                                 $data = ['employee_id' => $employee_id, 'from' => $from, 'to' => $to, 'dob' => $dob, 'type' => $type, 'data' => $arr, 'local' => $local,
                                          'tenure' => $tenure , 'balance_day' => $balance_day, 'entitlement_start' => $balance_day[0]->entitlement_start];
                                 $this->$retVal($data);
                            }

                            //$balance_day = $balance_day[0]->days;
                        }else{
                            return $message = 'please set up you contract start first';
                        }
                    }else{
                        return $message = 'please set up your contract date';
                    }
                }
        }

        //----------------------------------------------
        $data = ['employee_id' => $employee_id, 'from' => $from, 'to' => $to, 'dob' => $dob, 'type' => $type, 'data' => $arr, 'local' => $local];
        return $this->$retVal($data);
    }

    public function suspen($data){
        $now =  date('Y-m-d');
        $result = date_diff(date_create($now),date_create($data['from']));
        $result = $result->format("%a");
        if($data['dob'] > 1){ 
            return $message = "more than 1 days";
        }else if($result > 0 && $result < 2 ){
             return $message = "request allowed one day before due date";
        }else{
            return $message = 'ok';
        }

    }

    public function emerLeave($datas){
                // if(isset($json)){
                //     $data = json_decode($json,1);
                //     $data = $datas['data'];
                // }
                if($datas['dob'] == null){
                    return $message = "day of Between can't zero day ,please make a distance start date and end date of request, and try again ";
                    $status = 500;
                }else{
                    //from param
                    // $from =  $data['From'];
                            
                    // //to param
                    // $to = $data['To'];

                    // $validation = \Validator::make(
                    //         ['name' => $name,'LeaveType' => $type,"aggred"  => $aggree ,"from" => $from, "to" => $to], 
                    //         ['name' => 'required|integer','LeaveType' => "required|string","aggred"  => "required"]
                    //         );

                    // if($validation->fails()){
                    //     $check = $validation->errors()->all();
                    //     return  $request->getMessage("failed",$check, 500, $access[3]);
                    // }else{
                        // if($data['aggred']  != 1){
                        //     return  $message = "please tick form aggrement before submitted ";
                        // }else{
                           return $message = 'ok';
                        //}
                    //}
                }
             
    }

    public function birthleave($data){
        //return $data['dob'];
        $cal = \DB::SELECT("call view_all_leave_balance()");
        $arr = [];
        if($cal != null){
            foreach ($cal as $key => $value) {
                if($value->leave_type == "Birthday Leave" and $value->empx == $data['employee_id']){
                    $arr[] = $cal[$key];
                }
            }

            $now = date('Y');
            if($arr !=  null){
                foreach ($arr as $key => $value) {
                   
                    $explode = explode('-',$value->froms);
                    if($explode[0] ==  $now){
                        return $message ="You have to use the time off, please create a new request in the years ahead.";
                    }else{
                        return $message ="ok";
                    }
                }
            }else{
                return $message ="ok";
            }
        }else{
            if($data['dob'] > 1){
                return $message ="can't more than one day";
            }else{
                return $message = "ok";
            }
        }
    }

    public function vacleave($data){
       
        if($data['local'] == 1 and $data['data'] != []){
            
            $now = date('Y-m-d');
            
            //----------------------------------
            $ids = count($data['data']);
            $idx = $data['data'][$ids - 1]->ids; 
            $created_at = \DB::SELECT("select created_at  from leave_request where id = $idx and leave_type = 2 order by id desc limit 1");
            $created_at = $created_at[0]->created_at;
            //----------------------------------

            //-----------------------------------
            $balance = $data['data'][$ids - 1]->days; 
            $days = ($data['data'][$ids - 1]->days) - ($data['data'][$ids - 1]->balance_leaves);
            //-----------------------------------
            $now = date('Y-m-d');
            $valRet = date_diff(date_create($data['to']),date_create($data['from']));
            $valMax = date_diff(date_create($now),date_create($data['from']));
            $valCreated = date_diff(date_create($created_at),date_create($now));

            //--------------------------------
            $valRet = $valRet->format("%a");
            $valMax = $valMax->format("%a");
            $valCreatedOpt = $valCreated->format("%R");
            $valCreated = $valCreated->format("%a");
            //---------------------------------

           // print_r($valCreatedOpt);
            $explode = explode('-',$data['from']);
            
            //-------------------------------------
            $get_data = \DB::SELECT("select * from all_vacation where empx = '$data[employee_id]' ");
            if(!isset($get_data[0]->depart_name) || $get_data[0]->depart == null ){
                $message = 'please fill department';
            }elseif(!isset($get_data[0]->data) || $get_data[0]->data == null){
                $message = 'leave table missing for contract date';
            }elseif($explode[2] > 20){
                $message = 'please request before 20th';
            }elseif( $valRet < 4){
                $message = '5++ days for every request'; 
            }elseif($valMax < 60){
                $message = 'lower than 2 month';
            }elseif ($valMax > 120) {
                $message = 'more than 4 months';
            // }elseif ($valCreatedOpt == "-"){
            //     $message = "Oops, request before can't lower last before";
            }elseif ($valCreated <= 30){
                $message = "request can only do it for every 30 days, your last request  at ".$created_at;
            }elseif($days < $valRet){
                $message = "can't request more than * "+ $days;
            }else{
                $message = 'ok';
            }

            //-------------------------------------

                    return  $message;

            //--------------------------------
        }

         if($data['local'] == 1 and $data['data'] == []){
           // '$data[employee_id]';
            //$check = \DB::SELECT("select * from leave_vacation where department = $department and applied_for = $local ");
            
            $explode = explode('-',$data['from']);
            $now = date('Y-m-d');
            $valRet = date_diff(date_create($data['to']),date_create($data['from']));
            $valMax = date_diff(date_create($now),date_create($data['from']));
            // $valCreated = date_diff(date_create($created_at),date_create($now));

            //--------------------------------
            $valRet = $valRet->format("%a");
            $valMax = $valMax->format("%a");
            // $valCreatedOpt = $valCreated->format("%R");
            // $valCreated = $valCreated->format("%a");
            $check = \DB::SELECT("select * from leave_vacation where department = (select department from emp where employee_id =  '$data[employee_id]' ) and applied_for = (select local_it from emp where employee_id =  '$data[employee_id]' ) ");
            if($check == null){
                return $message = 'leave tabel not set up for department';
            }elseif(!isset($check[0]->entitlement_start) || $check[0]->entitlement_start != null){
                $entilited = $check[0]->entitlement_start;
                if($entilited == "Training Period"){
                    $check_exist = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$data[employee_id]'  ");
                }elseif($entilited == "contract_start"){
                    $check_exist = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$data[employee_id]' ");
                }elseif($entilited == "Probationary Period"){
                    $check_exist = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$data[employee_id]' ");
                }else{
                    $check_exist = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$data[employee_id]' ");
                }

                if(isset($check_exist) || $check_exist  != null){
                    $check_exist = $check_exist[0]->date;
                    $now = date('Y-m-d');
                    $date1 = new \DateTime($check_exist);
                    $now = new \DateTime($now);
                    
                    $date2 = date_diff($now, $date1);
                    $date2 = $date2->days;
                    
                    $tenure = ceil($date2/365)-1;
                    $balance_day = \DB::SELECT("select * from leave_vacation,leave_vacation_detail where leave_vacation.department = (select department from emp where employee_id =  '$data[employee_id]') 
                                                and leave_vacation.applied_for =  (select local_it from emp where employee_id =  '$data[employee_id]')
                                                and leave_vacation.id = leave_vacation_detail.vacation_id 
                                                and leave_vacation_detail.year = $tenure");
                                  
                    if(!isset($balance_day[0]->days)){
                       return $message = "please set days leave for request in leave table, or contact admin";
                    }
                    $balance_day = $balance_day[0]->days;
                }else{
                     return $message = 'plase set up you contract start first';
                }
            $job = \DB::SELECT("select * from job_history where employee_id =  '$data[employee_id]' ");
            //$get_data = \DB::SELECT("select * from emp where department = '$data[employee_id]' ");
            if(!isset($check) || $check == null ){
                $message = 'please fill department';
            }elseif(!isset($check_exist) || $check_exist == null){
                $message = 'leave table missing for contract date';
            }elseif($explode[2] > 20){
                $message = 'please request before 20th';
            }elseif( $valRet < 4){
                $message = '5++ days for every request'; 
            }elseif($valMax < 60){
                $message = 'lower than 2 month';
            }elseif ($valMax > 120) {
                $message = 'more than 4 months';
            }elseif($balance_day < $valRet){
                $message = "can't request more than * "+ $days;
            }else{
                $message = 'ok';
            }

            //-------------------------------------

                    return  $message;

            //--------------------------------
            }
        }

        if($data['local'] != 1 and $data['data'] != []){

            $now = date('Y-m-d');
            $days = $data['balance_day']; 
           //-----------------------------------

            $valRet = date_diff(date_create($data['to']),date_create($data['from']));
            $valMax = date_diff(date_create($now),date_create($data['from']));
            
            //--------------------------------
            $valRet = $valRet->format("%a");
            $valMax = $valMax->format("%a");
            //---------------------------------

            $explode = explode('-',$data['from']);

            //-------------------------------------
            $get_data = \DB::SELECT("select * from vacation_leave_local where empx = '$data[employee_id]' ");
            if(!isset($get_data[0]->depart) || $get_data[0]->depart == null ){
                $message = 'please fill department';
            }elseif(!isset($data['entitlement_start']) ||$data['entitlement_start'] == null){
                $message = 'leave table missing for your department or contract_start for your account not created yet';
            }elseif($explode[2] > 20){
                $message = 'please request before 20th';
            }elseif($valMax < 60){
                $message = 'lower than 2 month';
            }elseif ($valMax > 120) {
                $message = 'more than 4 months';
            }elseif( $valRet < 5){
                $message = '5++ days for every request'; 
            }elseif($days < $valRet){
                $message = "can request more than * "+ $days;
            }else{
                $message = 'ok';
            }   

            return $message;        
        }

        if($data['local'] != 1 and $data['data'] == []){
            $now = date('Y-m-d');
            $days = $data['balance_day']; 
            $valRet = date_diff(date_create($data['to']),date_create($data['from']));
            $valMax = date_diff(date_create($now),date_create($data['from']));

            $valRet = date_diff(date_create($data['to']),date_create($data['from']));
            $valMax = date_diff(date_create($now),date_create($data['from']));
            
            //--------------------------------
            $valRet = $valRet->format("%a");
            $valMax = $valMax->format("%a");
            $explode = explode('-',$data['from']);
            $get_data = \DB::SELECT("select * from emp where employee_id = '$data[employee_id]' ");
            if(!isset($get_data[0]->department) || $get_data[0]->department == null ){
                $message = 'please fill department';
            }elseif(!isset($data['entitlement_start']) ||$data['entitlement_start'] == null){
                $message = 'leave table missing for your department or contract_start for your account not created yet';
            }elseif($explode[2] > 20){
                $message = 'please request before 20th';
            }elseif($valMax < 60){
                $message = 'lower than 2 month';
            }elseif ($valMax > 120) {
                $message = 'more than 4 months';
            }elseif($days < $valRet){
                $message = "can request more than * "+ $days;
            }else{
                $message = 'ok';
            }

            return $message;
            
        }
    }

    public function enLeave($data){

        if($data['data'] != null){
           $datax =  end($data['data']);
           $date_by = \DB::SELECT("select sum(datediff(to_,from_)) as date from leave_request where employee_id = '$data[employee_id]' and leave_type = 3 ");
           $balance_day = \DB::SELECT("select balance_left as balance from all_enhance where empx = '$data[employee_id]' order by ids DESC limit 1 ");
          
           if(isset($balance_day[0]->balance)){
             $balance  =  $balance_day[0]->balance;
           }else{
             $balance  =  0;
           }        
           print_r($date_by[0]->date);
           if(isset($date_by[0]->date)){
                $balance_date=  $date_by[0]->date; 
           }else{
                 $balance_date = 0;
           }
           $calculate = $balance ;
           if($data['dob'] > $calculate){
             $message = 'not allowed days request left '.$calculate.' days';
           }else{
             $message = 'ok';
           }
        }else{
            $select = \DB::SELECT("select le.days from leave_enhance as le, job_history as jh where jh.job = le.job_tittle and jh.employee_id =  '$data[employee_id]' ");
            if(isset($select[0]->days)){
                if($data['dob'] > $select[0]->days){
                    $message = 'not allowed days request left '.$calculate.' days';
                }else{
                    $message = 'ok';
                }
            }else{
                $message =  'please complete add department or leave table for your job title before request';
            }
        }
        return $message;
    }

    public function sickLeave($data){
      
        if($data['data'] ==  []){
            $datax = end($data['data']);
            //return $datax->status;
            if($data['local'] != 1){
                $left_over = $datax->days - $datax->balance_leaves;
            }else{
               //local
                $now = date('Y-m-d');
                $base_emp = \DB::SELECT("select  * from emp where employee_id = '$data[employee_id]' ");
                $localit = $base_emp[0]->local_it;
                if(!isset($base_emp) || $base_emp == null){
                    return $message  = 'department not found';
                }else{
                    $department = $base_emp[0]->department;
                }

                //check contract
                $select_contract =  \DB::SELECT("select * from leave_sick where applied_for = $localit and department = $department ");
                $contract = $select_contract[0]-> entitlement_start;

                if($contract == "Training Period" ){ $contract_start = \DB::SELECT("select training_agreement as date from job_history where job_history.employee_id = '$data[employee_id]' "); }
                if($contract == "Probationary Period" ){ $contract_start = \DB::SELECT("select probationary_start as date from job_history where job_history.employee_id = '$data[employee_id]' "); }
                if($contract == "contract_start" ){ $contract_start = \DB::SELECT("select contract_start_date as date from job_history where job_history.employee_id = '$data[employee_id]' "); }
                if($contract == "Fixed Term Contract" ){ $contract_start = \DB::SELECT("select fixed_term_contract as date from job_history where job_history.employee_id = '$data[employee_id]' "); }

                $contract_start =  $contract_start[0]->date;
                $valRet = date_diff(date_create($now),date_create($contract_start));
                $valRet = $valRet->format("%a");

                $job_history = \DB::SELECT("select * from job_history where employee_id = '$data[employee_id]'");
                if(isset($job_history[0]->job) || $job_history[0]->job != null ){
                    $job_history = $job_history[0]->job;
                    $tenure = ( $valRet >= 270 && $valRet <= 364 ? 1 : ceil($valRet/365));
                    $select_contract =  \DB::SELECT("select days from leave_sick,leave_sick_detail where leave_sick.applied_for = $localit and leave_sick.department = $department and leave_sick.id = leave_sick_detail.leave_sick_id and leave_sick_detail.year = $tenure ");
                    $left_over = $select_contract[0]->days;
                }
            }
            //$left_over;

            if($data['dob'] > $left_over){
               $message = 'please set lower day for request';
            }else{
                $message = "ok";
            }
        }else{
            //$db   = \DB::SELECT("select * from all_sick where emp =  '$data[employee_id]' where localit != 1 order by ids DESC limit 1 ");
                
            if($data['dob'] > $data['data'][0]->days){
               $message = 'please set lower day for request'; 
            }else{
                $message = "ok";
            }
        }

        return $message;
    }

    public function maternityLeave($data){
        $date = date('Y-m-d');
        $dataz = date_diff(date_create($data['to']),date_create($data['from']));
        $dataz = $dataz->format("%a");
        $gender =  \DB::SELECT("select gender from emp where employee_id =  '$data[employee_id]' ");
        $valRet = date_diff(date_create($date),date_create($data['from']));
        if($data != null){
            if($dataz > 150){
                $message = 'request lower than 150';
            }else{
                if($gender[0]->gender == 'male'){
                    $message = 'request not for male';
                }else{
                    $message = 'ok';
                }       
            }  

        }elseif($valRet->format("%a") < 60){
            $message = "request before 2 months";
        }else{
            $message = 'ok';
        }

        return $message;
    }

    public function patLeave($data){
        $brain_fuck = \DB::SELECT("select * from paternity_leave_balance where empx = '$data[employee_id]' ");
        $date = date('Y-m-d');
        $gender =  \DB::SELECT("select gender from emp where employee_id =  '$data[employee_id]' ");
        $valRet = date_diff(date_create($date),date_create($data['from']));

        if($valRet->format("%a") < 180){
            $message = 'request must avail before 6 month';
        }else if($data['local']  == 1){
            if($brain_fuck != null){
                if($data['dob'] > 7 and $data['dob'] <= 12){
                    $check_date = end(\DB::SELECT("select  tenure_date_vl_expat fom vaction_leave_expat where employee_id =  '$data[employee_id]' "));
                    if(!isset($check_date) && $check_date[0]->tenure_date_vl_expat != null ){
                            $result = $check_date[0]->tenure_date_vl_expat + $brain_fuck[0]->balance_leaves;
                            if($result < $data['dob']){
                                $message = 'vl + pl combination not enough day';
                            }else{
                                $message = 'ok';
                            } 
                    }
                }elseif($data['dob'] >= 12){
                    $message = 'more than terms and condition allowed';
                }else{
                    $message = 'ok';
                }               
            }else{
                if($data['dob'] <= 12){
                    $message = 'ok';
                }else{
                    $message = 'not allowed';
                }
            }
        }else if($data['local']  != 1){
                if($data['dob'] >= 7){
                    $message = 'not allowed';
                }else{
                    $message = 'ok';
                }
        }

        return $message;
    }

    public function marLeave($data){
        $date = date('Y-m-d');
        $valRet = date_diff(date_create($date),date_create($data['from']));
        if($valRet->format("%a") < 60){
            $message = 'must apply 2 month before due date';
        }else if($valRet->format("%a") > 120){
            $message = "must apply maximum 4 month before due date";
        }elseif($data['dob'] > 19){
            $message = 'not allowed';
        }elseif($data['local'] == 1 ){
            if($data['dob'] > 10){
                $message = 'not allowed';
            }else{
                $message  = 'ok';
            }
        }elseif($data['local'] == 2){
            if($data['dob'] > 19){
                $message = "not allowed";
            }else{
                $check_date = end(\DB::SELECT("select  tenure_date_vl_expat fom vaction_leave_expat where employee_id =  '$data[employee_id]' "));
                if(!isset($check_date) && $check_date[0]->tenure_date_vl_expat != null ){
                    $result =  7 + $brain_fuck[0]->balance_leaves;
                    if($result < $data['dob']){
                        $message = 'vl + pl combination not enough day';
                    }else{
                        $message = 'ok';
                    } 
                }else{
                    $message = 'not allowed';
                }
            }
        }else{
            $message = 'ok';
        }

        return $message =  'ok';
    }

    public function ocLeave($data){
        if($data['dob'] > 1){
            return $message = "can't more than one day";
        }else{
            $now = date('Y-m-d');
            $valCreated = date_diff(date_create($now),date_create($data['from']));
            $valRet = $valCreated->format("%a");

            if($valRet < 7){
                $message = "day off /on call must submitted once week before due date";
            }else{
                $message = 'ok';
            }

            return $message;
        }
    }   

    public function adoLeave($data){
        return $message = 'ok';
    }

    public function bereavementLeave($data){
     
        $employee = $data['employee_id'];
        $type = $data['type'];
        $local = $data['local'];
        $department = \DB::SELECT("select department from emp where employee_id = '$employee' ");
        if($department != null){
             $department = $department[0]->department;
         }else{
            return $message = "please insert your";
         }
       
        $cal = \DB::SELECT("call view_all_leave_balance()");

        $arr_local = [];
        $arr_expat = [];
        if($cal != null){
            foreach ($cal as $key => $value) {
                if($value->localit == 1 and $value->leave_type == $type){
                        $arr_expat[] = $cal[$key]; 
                }
                if($value->localit != 1 and $value->leave_type == $type){
                        $arr_local[] = $cal[$key]; 
                }
            }
        }

        if($local == 1){
            
            if($arr_expat != null){
                $end = end($arr_expat);
                if($data['dob'] > ($end->days - $end->balance_leaves)){
                    return $message = "request can't more than ".($end->days - $end->balance_leaves);
                }else{
                    return $message = "ok";
                }
            }else{
                $check = \DB::SELECT("select leave_bereavement_detail.days from leave_bereavement,leave_bereavement_detail where leave_bereavement.applied_for = $local 
                                      and leave_bereavement.department = $department and leave_bereavement.id = leave_bereavement_detail.bereavement_id");
               
                if($check != null){
                    $check = $check[0]->days;
                     if($data['dob'] > $check){
                         return $message = "request can't more than ".($end->days - $end->balance_leaves);
                     }else{
                        return $message = "ok";
                     }
                }else{
                    return $message = "please set up table";
                }
            }
        }

        if($local != 1){
            if($arr_local != null){
                $end = end($arr_local);
               
                if($data['dob'] > ((int)$end->days - (int)$end->balance_leaves)){
                    return $message = "request can't more than ".($end->days - $end->balance_leaves);
                }else{
                    return $message = "ok";
                }
            }else{
               
                $check = \DB::SELECT("select leave_bereavement_detail.days from leave_bereavement,leave_bereavement_detail where leave_bereavement.applied_for = $local 
                                      and leave_bereavement.department = $department and leave_bereavement.id = leave_bereavement_detail.bereavement_id");
                 if($check != null){
                    $check = $check[0]->days;
                     if($data['dob'] > $check){
                         return $message = "request can't more than ".($end->days - $end->balance_leaves);
                     }else{
                        return $message = "ok";
                     }
                }else{
                    return $message = "please set up table";
                }
            }
        }
    }

}
