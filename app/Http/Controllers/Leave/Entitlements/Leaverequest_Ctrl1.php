<?php namespace Larasite\Http\Controllers\Leave\Entitlements;
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;
use Larasite\Model\Leave\Entitlements\Leave_term as terms;
use Exeception;

class Leaverequest_Ctrl extends Controller {

    protected $form = "72";

    private  $rule_ado = [
                'date1' => 'required|date',
                'date2' => 'required|date',
                'date3' => 'required|date',
                'date4' => 'required|date',
                'ado1' => 'required|integer',
                'ado2' => 'required|integer',
                'ado3' => 'required|integer',
                'ado4' => 'required|integer',
                'nextday' => 'required|date'
              ];

    public function index(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $key = \Input::get('key');
            $encode = base64_decode($key);
            $explode = explode("-",$encode);
            $att = $explode[1];
            $data_emp = \DB::SELECT("select supervisor from emp_supervisor where employee_id='$att' ");
            $data_hr = \DB::SELECT("select role_id from ldap where employee_id= '$att' ");
            if($data_emp != null){
                $data_emp = [];
            }elseif($data_hr != null){
                $data_hr_ex = $data_hr[0]->role_id;
                $role = \DB::SELECT("select  role_name from role where role_id = $data_hr_ex");
                if($role != null){
                    $data_emp = [];
                }
            }
            $db = \DB::SELECT("select id,leave_type from leave_type ");

            $key = \Input::get("key");
    				$encode = base64_decode($key);
    				$explode = explode("-",$encode);
    				$explode_id = $explode[1];
    				//$supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$explode_id' ");
    				//$hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$explode_id' AND t2.role_name = 'hr'");
    			    $userEmp  = \DB::SELECT("select role.role_name from role,ldap where ldap.employee_id =  '$explode_id' and ldap.role_id = role.role_id ");
        //             if($supervisor != null){
    				// 	$supervisor = "supervisor";
    				// }elseif($hr != null){
    				// 	$supervisor = "hr";
    				// }else{
    				// 	$supervisor =  "user";
    				// }
                
                    if($userEmp[0]->role_name != 'user' || $userEmp[0]->role_name != 'user employee'){
                        $supervisor = "hr";
                    }else{
                        $supervisor =  "user";
                    }
    				$emp_data = \DB::SELECT("CALL search_employee_by_id('$explode_id')");


    					$data_exp =
    					 [
    					 "employee_name" => $emp_data[0]->name,
    					 "employee_id" => $explode_id,
    					"status" => $supervisor
    					];

    				//$message = "success"; $data = $data_exp; $status =  200;
            $data = ["data" => $db, "status" => $data_exp];
            return $request->index($data, $access[3]);
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }


    public function index_shift()
    {
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

        if($access[1] == 200){
            $request = new leaverequest_Model;
            $shift = \DB::SELECT("CALL view_workshift_fixed_schedule");
            foreach($shift as $key => $value){
                $data[] =  ["id" => $value->shift_id, "shif_code" => $value->shift];
            }
            return  $request->index($data,$access[3]);
        }
        else{
            $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
        }

    }


    public function autocomplete(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $name = \Input::get('name');
            $input = \Input::all();
            $rule = ["name" => "required|Regex:/^[A-Za-z]+$/"];
            $validation = \Validator::make($input,$rule);
            $db = \DB::SELECT("CALL Search_emp_by_name ('$name')");

            return $request->search($validation,$db,$access[3]);
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }


    public  function request_remaining(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $input = \Input::all();
            $name = \Input::get('name');
            $type = \Input::get('LeaveType');

            $rule = [
                'name' => 'required',
                'LeaveType' => 'required',
                ];


            $validation = \Validator::make($input,$rule);

            if($validation->fails()){
                $check = $validation->errors()->all();
                return $request->getMessage("failed",$check,500,$access[3]);
            }else{
                $supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$name' ");
                if($supervisor != null){
                    $supervisor = "supervisor";
                }else{
                    $supervisor =  "user";
                }

                $type = \DB::SELECT(" select id from leave_type where leave_type='$type' ");
                $type_ex = $type[0]->id;
                $data = \DB::SELECT("CALL view_leave_request_remainingday('$name',$type_ex)");

                $check_re = \DB::SELECT("select created_at from leave_request where employee_id='$name' and leave_type=$type_ex order by id desc limit 1");
                $data_exp = [];
                if($check_re != null){
                  $check_re = $check_re[0]->created_at;
                  $explode = explode("-",$check_re);
                  if($check_re != date('Y-m-d')){
                    $check_re = date('Y-m-d');
                  }
                  $req_for = date('Y-m-d', strtotime("+4 months", strtotime($check_re)));
                  $min_req_for = date('Y-m-d', strtotime("+2 months", strtotime($check_re)));
                //  $dates = date($explode[2],$explode[1],$explode[0], strtotime('+2months'));

                  $date = ["last_date" => $check_re, "max_date_req" => $min_req_for, "for_request" =>$req_for];
                }
                if($data != null){
                    foreach ($data as $key => $value) {
                        $data_exp= [
                        "remaining_day" => $value->remaining_day,
                        "status" => $supervisor,
                        "date" => $date
                      ];
                    }
                }else{
                    $data_exp = [];
                }

                return $request->getMessage("success",$data_exp,200,$access[3]);
            }
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }

    }

    public  function request_leave(){
         /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
             /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $year = date('Y');
            $request = new leaverequest_Model;
            $json = \Input::get("data");
            $file = \Input::file('file');
            $data = json_decode($json,1);

            if(isset($json)){
                if(isset($data['data']['LeaveType'])){
                    $type =  $data['data']['LeaveType'];
                    $leave_type = \DB::select("select leave_type,id from leave_type where leave_type='$type' ");
                    $type_id = $leave_type[0]->id;
                }else{
                    return $request->getMessage("please select leave type first",[],500,$access[3]);
                }

                if(isset($data['data']['name'])){
                    $name =  $data['data']['name'];
                }else{
                    $name = null;
                }

                if(isset($data['data']['remaining_day'])){
                    $remaining_day =  $data['data']['remaining_day'];
                }else{
                    $remaining_day = null;
                }
                
                if($type != "Accumulation Day Off"){
                    if(isset($data['data']['name']) && isset($data['data']['From']) && isset($data['data']['To'])){
                        $from =  $data['data']['From'];
                        $to = $data['data']['To'];
                    }else{
                        return $request->getMessage("Please fill form data before submit data",[],500,$access[3]);
                    }
                }

                if($type != "Accumulation Day Off"){
                    if(isset($data['data']['DayOffBetween'])){
                        $dob = $data['data']['DayOffBetween'];
                    }else{
                        return $request->getMessage("Day Off Between not found",[],500,$access[3]);
                    }
                }

                if(!isset($data['data']['aggred'])){
                    return $request->getMessage("please check form aggrement before submit data",[],500,$access[3]);
                }else{
                    $aggree = $data['data']['aggred'];
                }

                if(isset($data['data']['comment'])){
                    $comment = $data['data']['comment'];
                }else{
                $comment = NULL;
                                                }
                (isset($data['data']['IncludeAdo']) ? $ado_x = $data['data']['IncludeAdo'] : $ado_x = "No" );
            }else{
                $data = \Input::all();
                if(isset($data['LeaveType'])){
                    $type =  $data['LeaveType'];
                    $leave_type = \DB::select("select leave_type,id from leave_type where leave_type='$type' ");
                    $type_id = $leave_type[0]->id;
                }else{
                    return $request->getMessage("please choose of the item in LeaveType",[],500,$access[3]);
                }

                if(isset($data['name'])){
                  $remaining_day =  $data['remaining_day'];
                }else{
                     $remaining_day = null;
                }

                if(isset($data['name'])){
                  $name =  $data['name'];
                }else{
                  return $request->getMessage("please insert name before submit data",[],500,$access[3]);
                }

                if($type !=  "Accumulation Day Off"){
                    if(isset($data['From']) && isset($data['To']) ){
                        $from =  $data['From'];
                        $to = $data['To'];
                    }else{
                        return $request->getMessage("please fill out all data before submit data ",[],500,$access[3]);
                    }
                }


                if(isset($data['DayOffBetween'])){
                    $dob = $data['DayOffBetween'];
                }else{
                     $dob = NULL;
                }

                if(!isset($data['aggred'])){
                    return $request->getMessage("please check form aggrement before submit data",[],500,$access[3]);
                }else{
                    $aggree = $data['aggred'];
                }

                if(isset($data['comment'])){
                    $comment = $data['comment'];
                }else{
                    $comment = NULL;
                }
                
                (isset($data['IncludeAdo']) ? $ado_x = $data['IncludeAdo'] : $ado_x = "No" );
            }

            $check_localit = \DB::SELECT("select local_it from emp where employee_id = '$name' ");
            $localit = ($check_localit[0]->local_it == 2 || $check_localit[0]->local_it == 3 ? 'local' :  'expat');


             if($type == "Vacation Leave" && $localit == "expat"){

             }   
             if($type == "Vacation Leave" && $localit == "local"){

             }
             if($type == "Emergency Leave"){
                if(isset($json)){
                    $data = json_decode($json,1);
                    $data = $data['data'];
                }

                if($dob == null){
                    $message = "day of Between can't zero day ,please make a distance start date and end date of request, and try again ";
                    $status = 500;
                }else{
                    //from param
                    $from =  $data['From'];
                            
                    //to param
                    $to = $data['To'];

                    $validation = \Validator::make(
                            ['name' => $name,'LeaveType' => $type,"aggred"  => $aggree ,"from" => $from, "to" => $to], 
                            ['name' => 'required|integer','LeaveType' => "required|string","aggred"  => "required"]
                            );

                    if($validation->fails()){
                        $check = $validation->errors()->all();
                        return  $request->getMessage("failed",$check, 500, $access[3]);
                    }else{
                        if($data['aggred']  != 1){
                            return  $request->getMessage("failed","please tick form aggrement before submitted ", 500, $access[3]);
                        }
                    }
                }
             }
             if($type == "Sick Leave" && $localit == "local"){
                if(isset($json)){
                    $data = json_decode($json,1);
                    $data = $data['data'];
                }

                if($dob == null){
                    $message = "day of Between can't zero day ,please make a distance start date and end date of request, and try again ";
                    $status = 500;
                }else{
                    //from param
                    $from =  $data['From'];
                            
                    //to param
                    $to = $data['To'];

                    $validation = \Validator::make(
                            ['name' => $name,'LeaveType' => $type,"aggred"  => $aggree ,"from" => $from, "to" => $to], 
                            ['name' => 'required|integer','LeaveType' => "required|string","aggred"  => "required"]
                            );

                    if($validation->fails()){
                        $check = $validation->errors()->all();
                        return  $request->getMessage("failed",$check, 500, $access[3]);
                    }else{
                        if($data['aggred']  != 1){
                            return  $request->getMessage("failed","please tick form aggrement before submitted ", 500, $access[3]);
                        }
                    }
                }

                
                
             }
             if($type == "Sick Leave" && $localit == "expat"){

             }
             if($type == "Paternity Leave" && $localit == "local"){

             }
             if($type == "Paternity Leave" && $localit == "expat"){

             }
             if($type == "Bereavement Leave" && $localit == "local"){

             }
             if($type == "Bereavement Leave" && $localit == "expat"){

             }
             if($type == "Marriage Leave" && $localit == "local"){

             }
             if($type == "Marriage Leave" && $localit == "expat"){

             }


        }else{
            $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }