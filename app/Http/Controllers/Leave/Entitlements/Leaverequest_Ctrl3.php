<?php namespace Larasite\Http\Controllers\Leave\Entitlements;
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;
use Larasite\Model\Leave\Entitlements\Leave_term as terms;
use Exeception;

class Leaverequest_Ctrl extends Controller {

    protected $form = "72";

    private  $rule_ado = [
                'date1' => 'required|date',
                'date2' => 'required|date',
                'date3' => 'required|date',
                'date4' => 'required|date',
                'ado1' => 'required|integer',
                'ado2' => 'required|integer',
                'ado3' => 'required|integer',
                'ado4' => 'required|integer',
                'nextday' => 'required|date'
              ];

    public function index(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $key = \Input::get('key');
            $encode = base64_decode($key);
            $explode = explode("-",$encode);
            $att = $explode[1];
            $data_emp = \DB::SELECT("select supervisor from emp_supervisor where employee_id='$att' ");
            $data_hr = \DB::SELECT("select role_id from ldap where employee_id= '$att' ");
            if($data_emp != null){
                $data_emp = [];
            }elseif($data_hr != null){
                $data_hr_ex = $data_hr[0]->role_id;
                $role = \DB::SELECT("select  role_name from role where role_id = $data_hr_ex");
                if($role != null){
                    $data_emp = [];
                }
            }
            $db = \DB::SELECT("select id,leave_type from leave_type ");

            $key = \Input::get("key");
    				$encode = base64_decode($key);
    				$explode = explode("-",$encode);
    				$explode_id = $explode[1];
    				//$supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$explode_id' ");
    				//$hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$explode_id' AND t2.role_name = 'hr'");
    			    $userEmp  = \DB::SELECT("select role.role_name from role,ldap where ldap.employee_id =  '$explode_id' and ldap.role_id = role.role_id ");
        //             if($supervisor != null){
    				// 	$supervisor = "supervisor";
    				// }elseif($hr != null){
    				// 	$supervisor = "hr";
    				// }else{
    				// 	$supervisor =  "user";
    				// }
                
                    if($userEmp[0]->role_name != 'user' || $userEmp[0]->role_name != 'user employee'){
                        $supervisor = "hr";
                    }else{
                        $supervisor =  "user";
                    }
    				$emp_data = \DB::SELECT("CALL search_employee_by_id('$explode_id')");


    					$data_exp =
    					 [
    					 "employee_name" => $emp_data[0]->name,
    					 "employee_id" => $explode_id,
    					"status" => $supervisor
    					];

    				//$message = "success"; $data = $data_exp; $status =  200;
            $data = ["data" => $db, "status" => $data_exp];
            return $request->index($data, $access[3]);
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }


    public function index_shift()
    {
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');

        if($access[1] == 200){
            $request = new leaverequest_Model;
            $shift = \DB::SELECT("CALL view_workshift_fixed_schedule");
            foreach($shift as $key => $value){
                $data[] =  ["id" => $value->shift_id, "shif_code" => $value->shift];
            }
            return  $request->index($data,$access[3]);
        }
        else{
            $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, "access"=>$access],$status);
        }

    }


    public function autocomplete(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $name = \Input::get('name');
            $input = \Input::all();
            $rule = ["name" => "required|Regex:/^[A-Za-z]+$/"];
            $validation = \Validator::make($input,$rule);
            $db = \DB::SELECT("CALL Search_emp_by_name ('$name')");

            return $request->search($validation,$db,$access[3]);
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }
    }


    public  function request_remaining(){
        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $request = new leaverequest_Model;
            $input = \Input::all();
            $name = \Input::get('name');
            $type = \Input::get('LeaveType');

            $rule = [
                'name' => 'required',
                'LeaveType' => 'required',
                ];


            $validation = \Validator::make($input,$rule);

            if($validation->fails()){
                $check = $validation->errors()->all();
                return $request->getMessage("failed",$check,500,$access[3]);
            }else{

                $check_request = \DB::SELECT("select dayoff_between from leave_request,leave_type where leave_request.employee_id = '$name' and leave_request.status_id = 2 and leave_type.leave_type = '$type' and leave_request.leave_type = leave_type.id  order by leave_request.id DESC limit 1 ");
               
                $check_localit = \DB::SELECT("select local_it from emp where employee_id = '$name' ");
                $check_contract =  \DB::SELECT("select contract_start_date,fixed_term_contract from job_history where employee_id =  '$name' ");
                $start = $check_contract[0]->contract_start_date;
                $end   = $check_contract[0]->fixed_term_contract;
                $diff = date_diff(date_create($start),date_create($end));
                $diff = $diff->format("%a");
                $tenure = ( $diff >= 1  && $diff <= 365 ? 1 : ( $diff  >= 366 && $diff <= 730 ?  2 : ($diff >= 731 && $diff <= 1098 ? 3 : 4 )));
                $vacDays = ($tenure == 1 ? 24 : ($tenure == 2 ? 32 : ($tenure == 3 ? 34 : 42)));
                $maxTake = ($tenure == 1 ? 2  : ($tenure == 2 ? 3 : ($tenure == 3 ? 4 : 4)));

                

                $localit = ($check_localit[0]->local_it == 2 || $check_localit[0]->local_it == 3 ? 'local' :  'expat');
            
                if(isset($check_request) && $check_request == null){
                    if($type == "Vacation Leave" ){
                        if($tenure == 1){
                            $remaining = 24;
                        }else if($tenure == 2){
                            $remaining = 32;
                        }else if($tenure == 3){
                            $remaining = 34;
                        }else{
                            $remaining = 42;
                        }
                    }else if($type == "Maternity Leave" && $localit ==  "expat"){
                        $remaining = 150;
                    }else if($type == "Paternity Leave" && $localit ==  "expat"){
                        $vl = \DB::SELECT("select  dayoff_between from leave_request where employee_id = '$name'  and status_id = 2  and leave_type = 2 order by id DESC limit 1");
                        if($vl ==  null){
                            $remaining =   12;
                        }else{
                            if($vl != null || $vl != null){
                                $vl = $vl[0]->dayoff_between;
                                $remaining = 7 + int($vl);
                                if($remaining > 12){
                                    $remaining = 12;
                                }
                            }
                        }
                    }else if($type == "Paternity Leave" && $localit ==  "local"){
                        $remaining = 7; 
                    }else if($type == "Marriage Leave" && $localit ==  "expat"){
                        $vl = \DB::SELECT("select  dayoff_between from leave_request where employee_id = '$name'  and status_id = 2  and leave_type = 2 order id DESC  by limit 1");
                        if($vl ==  null){
                            $remaining =  19;
                        }else{
                            if($vl != null || $vl != null){
                                $vl = $vl[0]->dayoff_between;
                                $remaining = 7 + int($vl);
                                if($remaining > 19){
                                    $remaining = 19;
                                }
                            }
                        }
                    }else if($type == "Marriage Leave" && $localit ==  "local"){
                         $remaining =  10;
                    }else{
                        $remaining = 99;
                    }

                }else{

                    $check_request = \DB::SELECT("select dayoff_between from leave_request,leave_type where leave_request.employee_id = '$name' and leave_request.status_id = 2 and leave_type.leave_type = '$type' and leave_request.leave_type = leave_type.id  order by leave_request.id DESC limit 1 ");                    
                    if(isset($check_request) && $check_request != null)
                    {
                        $val = $check_request[0]->dayoff_between;
                    }else{
                        $val = 0;
                    }
                    
                    
                    if($type == "Vacation Leave" ){
                        if($tenure == 1){
                            $remaining = 24 - int($val);
                        }else if($tenure == 2){
                            $remaining = 32 - int($val);
                        }else if($tenure == 3){
                            $remaining = 34 - int($val);
                        }else{
                            $remaining = 42 - int($val);
                        }
                    }else if($type == "Maternity Leave" && $localit ==  "expat"){
                        $remaining = 150 - int($val);
                    }else if($type == "Paternity Leave" && $localit ==  "expat"){
                        $vl = \DB::SELECT("select  dayoff_between from leave_request where employee_id = '$name'  and status_id = 2  and leave_type = 2 order by id DESC limit 1");
                        if($vl ==  null){
                            $remaining =   12 - int($val);
                        }else{
                            if($vl != null || $vl != null){
                                $vl = $vl[0]->dayoff_between;
                                $remaining = 12 - int($vl) -  int($val);
                                if($remaining > 12){
                                    $remaining = 12;
                                }
                            }
                        }
                    }else if($type == "Paternity Leave" && $localit ==  "local"){
                        $remaining = 7 - int($val); 
                    }else if($type == "Marriage Leave" && $localit ==  "expat"){
                        $vl = \DB::SELECT("select  dayoff_between from leave_request where employee_id = '$name'  and status_id = 2  and leave_type = 2 order by id DESC limit 1");
                        if($vl ==  null){
                            $remaining =  19 - int($val);
                        }else{
                            if($vl != null || $vl != null){
                                $vl = $vl[0]->dayoff_between;
                                $remaining = 19 - int($vl) - int($val);
                                if($remaining > 19){
                                    $remaining = 19;
                                }
                            }
                        }
                    }else if($type == "Marriage Leave" && $localit ==  "local"){
                         $remaining =  10 - int($val);
                    }else{
                        $remaining = 99;
                    }
                }   

                $supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$name' ");
                if($supervisor != null){
                    $supervisor = "supervisor";
                }else{
                    $supervisor =  "user";
                }


                $last_date = \DB::SELECT("select leave_request.from_,leave_request.created_at  from leave_request,leave_type where leave_request.employee_id= '$name' and leave_request.leave_type = leave_type.id and leave_type.leave_type = '$type' order by leave_request.id desc limit 1");
                if(isset($last_date) && $last_date != null){
                    if($last_date[0]->from_ != null){
                        $last_date = $last_date[0]->from_;
                    }else{
                        $last_date = null;
                    }
                }

                $date = ["last_date" => $last_date, "max_date_req" => null, "for_request" => null];
                $data_exp= [
                        "remaining_day" => $remaining,
                        "status" => $supervisor,
                        "date" => $date
                      ];
                // if($check_re != null){
                //   $check_re = $check_re[0]->created_at;
                //   $explode = explode("-",$check_re);
                //   if($check_re != date('Y-m-d')){
                //     $check_re = date('Y-m-d');
                //   }
                //   $req_for = date('Y-m-d', strtotime("+4 months", strtotime($check_re)));
                //   $min_req_for = date('Y-m-d', strtotime("+2 months", strtotime($check_re)));
                // }


                // $type = \DB::SELECT(" select id from leave_type where leave_type='$type' ");

                // $type_ex = $type[0]->id;
                // $data = \DB::SELECT("CALL view_leave_request_remainingday('$name',$type_ex)");

                // $check_re = \DB::SELECT("select created_at from leave_request where employee_id='$name' and leave_type=$type_ex order by id desc limit 1");
                // $data_exp = [];

                //  $dates = date($explode[2],$explode[1],$explode[0], strtotime('+2months'));

                //   $date = ["last_date" => $check_re, "max_date_req" => $min_req_for, "for_request" =>$req_for];
                // }
                // if($data != null){
                //     foreach ($data as $key => $value) {
                //         $data_exp= [
                //         "remaining_day" => $remaining,
                //         "status" => $supervisor,
                //         "date" => $date
                //       ];
                //     }
                // }else{
                //     $data_exp = [];
                // }

                return $request->getMessage("success",$data_exp,200,$access[3]);
            }
         
        }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
            return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
        }

    }

    public function insert_ado($data){
        $file = \Input::file('file');
        if(isset($file)){
        if(isset($data['data']['date1'])){$date1 =  $data['data']['date1'];}else{$date1 = null;}
        if(isset($data['data']['date2'])){$date2 =  $data['data']['date2'];}else{$date2 = null;}
        if(isset($data['data']['date3'])){$date3 =  $data['data']['date3'];}else{$date3 = null;}
        if(isset($data['data']['date4'])){$date4 =  $data['data']['date4'];}else{$date4 = null;}

        if(isset($data['data']['ado1'])){$ado1 =  $data['data']['ado1'];}else{$ado1 = null;}
        if(isset($data['data']['ado2'])){$ado2 =  $data['data']['ado2'];}else{$ado2 = null;}
        if(isset($data['data']['ado3'])){$ado3 =  $data['data']['ado3'];}else{$ado3 = null;}
        if(isset($data['data']['ado4'])){$ado4 =  $data['data']['ado4'];}else{$ado4 = null;}

        if(isset($data['data']['nextday'])){$nextday =  $data['data']['nextday'];}else{$nextday = null;}
        }else{
            if(isset($data['date1'])){$date1 =  $data['date1'];}else{$date1 = null;}
            if(isset($data['date2'])){$date2 =  $data['date2'];}else{$date2 = null;}
            if(isset($data['date3'])){$date3 =  $data['date3'];}else{$date3 = null;}
            if(isset($data['date4'])){$date4 =  $data['date4'];}else{$date4 = null;}
            if(isset($data['ado1'])){$ado1 =  $data['ado1'];}else{$ado1 = null;}
            if(isset($data['ado2'])){$ado2 =  $data['ado2'];}else{$ado2 = null;}
            if(isset($data['ado3'])){$ado3 =  $data['ado3'];}else{$ado3 = null;}
            if(isset($data['ado4'])){$ado4 =  $data['ado4'];}else{$ado4 = null;}
            if(isset($data['nextday'])){$nextday =  $data['nextday'];}else{$nextday = null;}
        }

        $ado = [
                ["date" => $date1, "ado_name" => $ado1],
                ["date" => $date2, "ado_name" => $ado2],
                ["date" => $date3, "ado_name" => $ado3],
                ["date" => $date4, "ado_name" => $ado4]
            ];

        $validation = \Validator::make(
            ['date1' => $date1,
            'date2' => $date2,
            'date3' => $date3,
            'date4' => $date4,
            'ado1' => $ado1,
            'ado2' => $ado2,
            'ado3' => $ado3,
            'ado4' => $ado4],

            ['date1' => 'required|date',
            'date2' => 'required|date',
            'date3' => 'required|date',
            'date4' => 'required|date',
            'ado1' => 'required|integer',
            'ado2' => 'required|integer',
            'ado3' => 'required|integer',
            'ado4' => 'required|integer']

            );

        if($validation->fails()){
            $check = $validation->errors()->all();
            return  $request->getMessage($check,[], 500, $access[3]);
        }else{
            
            $count = count($ado)-1;
            for($i = 0; $i <=$count; $i++){
                if($i == 0){
                    $name_ado = "1st";
                }elseif($i == 1){
                    $name_ado = "2nd";
                }elseif($i == 2){
                    $name_ado = "3rd";
                }else{
                    $name_ado = "4th";
                }

                $date_ado = $ado[$i]['date'];
                $new_ado = $ado[$i]['ado_name'];
                $db = \DB::SELECT("call insert_leave_ado($type_id,'$name','$name_ado','$nextday',$new_ado)");
            }
        }
    }



    public  function request_leave(){

        /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
        if($access[1] == 200){
            $year = date('Y');
            $request = new leaverequest_Model;
            $json = \Input::get("data");
            $file = \Input::file('file');
            $data = json_decode($json,1);

            if(isset($json)){
				if(isset($data['data']['LeaveType'])){
					$type =  $data['data']['LeaveType'];
                    $leave_type = \DB::select("select leave_type,id from leave_type where leave_type='$type' ");
                    $type_id = $leave_type[0]->id;
				}else{
					return $request->getMessage("please select leave type first",[],500,$access[3]);
				}

                if(isset($data['data']['name'])){
                    $name =  $data['data']['name'];
                }else{
                    $name = null;
                }

                if(isset($data['data']['remaining_day'])){
                    $remaining_day =  $data['data']['remaining_day'];
                }else{
                    $remaining_day = null;
                }
				
                if($type != "Accumulation Day Off"){
					if(isset($data['data']['name']) && isset($data['data']['From']) && isset($data['data']['To'])){
                        $from =  $data['data']['From'];
						$to = $data['data']['To'];
					}else{
						return $request->getMessage("Please fill form data before submit data",[],500,$access[3]);
					}
				}

                if($type != "Accumulation Day Off"){
      				if(isset($data['data']['DayOffBetween'])){
                        $dob = $data['data']['DayOffBetween'];
                    }else{
      					return $request->getMessage("Day Off Between not found",[],500,$access[3]);
      				}
                }

                if(!isset($data['data']['aggred'])){
                    return $request->getMessage("please check form aggrement before submit data",[],500,$access[3]);
                }else{
                    $aggree = $data['data']['aggred'];
                }

    			if(isset($data['data']['comment'])){
                    $comment = $data['data']['comment'];
    			}else{
				$comment = NULL;
												}
                (isset($data['data']['IncludeAdo']) ? $ado_x = $data['data']['IncludeAdo'] : $ado_x = "No" );
            }else{
                $data = \Input::all();
				if(isset($data['LeaveType'])){
					$type =  $data['LeaveType'];
                    $leave_type = \DB::select("select leave_type,id from leave_type where leave_type='$type' ");
                    $type_id = $leave_type[0]->id;
				}else{
					return $request->getMessage("please choose of the item in LeaveType",[],500,$access[3]);
				}

                if(isset($data['name'])){
                  $remaining_day =  $data['remaining_day'];
                }else{
                     $remaining_day = null;
                }

                if(isset($data['name'])){
                  $name =  $data['name'];
                }else{
                  return $request->getMessage("please insert name before submit data",[],500,$access[3]);
                }

				if($type !=  "Accumulation Day Off"){
					if(isset($data['From']) && isset($data['To']) ){
                        $from =  $data['From'];
                        $to = $data['To'];
					}else{
						return $request->getMessage("please fill out all data before submit data ",[],500,$access[3]);
					}
				}


                if(isset($data['DayOffBetween'])){
                    $dob = $data['DayOffBetween'];
                }else{
                     $dob = NULL;
                }

                if(!isset($data['aggred'])){
                    return $request->getMessage("please check form aggrement before submit data",[],500,$access[3]);
                }else{
                    $aggree = $data['aggred'];
                }

				if(isset($data['comment'])){
                    $comment = $data['comment'];
				}else{
					$comment = NULL;
				}
                
                (isset($data['IncludeAdo']) ? $ado_x = $data['IncludeAdo'] : $ado_x = "No" );
            }

            $check_localit = \DB::SELECT("select local_it from emp where employee_id = '$name' ");
            $localit = ($check_localit[0]->local_it == 2 || $check_localit[0]->local_it == 3 ? 'local' :  'expat');
            

            if($type == "Vacation Leave" && $localit == "expat"){
              
                //encode param
                if(isset($json)){
                    $data = json_decode($json,1);
                    $data = $data['data'];
                }

                if($dob == null){
                    $message = "day of Between can't zero day ,please make a distance start date and end date of request, and try again ";
                    $status = 500;
                }else{
                    //from param
                    $from =  $data['From'];
                            
                    //to param
                    $to = $data['To'];

                    $validation = \Validator::make(
                            ['name' => $name,'LeaveType' => $type,"aggred"  => $aggree ,"from" => $from, "to" => $to], 
                            ['name' => 'required|integer','LeaveType' => "required|string","aggred"  => "required"]
                            );

                    if($validation->fails()){
                        $check = $validation->errors()->all();
                        return  $request->getMessage("failed",$check, 500, $access[3]);
                    }else{
                        if($data['aggred']  != 1){
                            return  $request->getMessage("failed","please tick form aggrement before submitted ", 500, $access[3]);
                        }
                    }

                    try{

                        //chekc local orr expat
                        $check_localit = \DB::SELECT("select local_it from emp where employee_id = '$name' ");
                        $localit = ($check_localit[0]->local_it == 2 || $check_localit[0]->local_it == 3 ? 'local' :  'expat');

                        //check contract star and end
                        $check_contract =  \DB::SELECT("select contract_start_date,fixed_term_contract from job_history where employee_id =  '$name' ");
                        
                        if(!isset($check_contract[0]->contract_start_date) && !isset($check_contract[0]->fixed_term_contract)){
                            $message = 'please insert contract start and end before take a request';
                            $status = 500;
                        }else{

                            // contract start 
                            $start = $check_contract[0]->contract_start_date;
                            //contract end
                            $end   = $check_contract[0]->fixed_term_contract;
                            
                            //arg1 & check date from contract with reuest 
                            $arg = ($start < $from ? true : false);
                            $arg1 = ($end > $to ? true : false);
                            
                            //if  true
                            if($arg == true && $arg1 == true){
                                
                                //get tenure vor vacation leave minimal

                                $diff = date_diff(date_create($start),date_create($end));
                                $diff = $diff->format("%R%a days");
                                //modify coz : datediff return e.g +277 days, only ned operator and day
                                $diff = explode(" ",$diff);
                                $diff = $diff[0];
                                $diffoperator = substr($diff[0],0,1); 
                                $diff = substr($diff,1); 

                                if($diffoperator == "+"){
                                    $tenure = ( $diff >= 1  && $diff <= 365 ? 1 : ( $diff  >= 366 && $diff <= 730 ?  2 : ($diff >= 731 && $diff <= 1098 ? 3 : 4 )));

                                    //make vacation condition minimum
                                    $vacDays = ($tenure == 1 ? 24 : ($tenure == 2 ? 32 : ($tenure == 3 ? 34 : 42)));
                                    $maxTake = ($tenure == 1 ? 2  : ($tenure == 2 ? 3 : ($tenure == 3 ? 4 : 4)));
                                    //request vacation days 
                                    $check_vacDays = date_diff(date_create($from),date_create($to));
                                    $check_vacDays = $check_vacDays->format("%a");
                                    
                                    $message =  ( $check_vacDays > $vacDays ? 'please create request lower than '.$vacDays." days" : null );
                                    
                                    if($message == null){
                                        //query check request exist or not
                                        $query = \DB::SELECT("select count(employee_id) as count from leave_request where employee_id = '$name' ");
                                        if(isset($query[0]->count) && $query[0]->count == $maxTake){
                                            //..........

                                            $check_req = \DB::select("select created_at from leave_request where employee_id='$name' and leave_type=$type_id and from_='$from' and to_='$to' ");
                                            if($check != null){
                                                $status = 500;
                                                $message = "request already created before";
                                            }else{
                                                //$db_data = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob)"
                                                $rename = '';
                                                $path = "hrms_upload/leave_request";
                                                if(isset($file) && $file != null){
                                                    $validation = \Validator::make(['file' => $file] , ['file' => 'required|max:1000']);
                                                    $rename = "leave_request__".str_random(6).".".$file->getClientOriginalExtension();
                                                    \Input::file('file')->move(storage_path($path),$rename);
                                                  
                                                    if($validation->fails()){
                                                        $messsage = "file more than 1 MB";
                                                        $status = 500;          
                                                    }else{
                                                        $db_data = \DB::SELECT("CALL insert into leave_request(employee_id,leave_type,from_,dayoff_between,status_id) values ('$name',$type_id,'$from','$to',$dob,3)");
                                                        $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                                                        $data_id_ex = $data_ex[0]->id;
                                                        $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$imageName','$path','$comment')");
                                                        
                                                        $message = "Oops , you request more than 2 times in one year of your tenure, rejected automatically";
                                                        $status = 500;
                                                    }
                                                }else{
                                                    $db_data = \DB::SELECT("CALL insert into leave_request(employee_id,leave_type,from_,dayoff_between,status_id) values ('$name',$type_id,'$from','$to',$dob,3)");
                                                    $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                                                    $data_id_ex = $data_ex[0]->id;
                                                    $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$imageName','$path','$comment')");

                                                    $message = "Oops , you request more than 2 times in one year of your tenure, rejected automatically";
                                                    $status = 500;
                                                }
                                            }
                                        }else{
                                            if($localit == 'expat'){
                                                $dateNow = date("Y-m-d");
                                                $dateExpatReq = date_diff(date_create($dateNow),date_create($from));
                                                $dateExpatReq = $dateExpatReq->format("%R%a days");
                                                
                                                //check reuest time x_time < 2 month and time_x > 4 month
                                                //modify coz : datediff return e.g +277 days, only nedd operator and day
                                                $dateExpatReq = explode(" ",$dateExpatReq);
                                                $dateExpatReq = $dateExpatReq[0];
                                                $dateExpatReqOperator = substr($dateExpatReq,0,1); 
                                                $dateExpatReq = substr($dateExpatReq,1);

                                                if($dateExpatReqOperator == "+"){
                                                    $message = ($dateExpatReq >= 1 && $dateExpatReq <= 60 ? 'for expat employee request should be 2 month before date request' : ( $dateExpatReq >  120 ? 'for expat employee should be less than 4 month before request applied' : null));
                                                }

                                                if($dateExpatReqOperator == "-"){
                                                    $message = "can't lower that currency date";
                                                }
                                                if($message == null){

                                                    //check date request each application request
                                                    $query_firstRequestDate = \DB::SELECT("select created_at,from_ from leave_request where employee_id = '$name' order by id DESC");
                                                   
                                                    if(isset($query_firstRequestDate[0]->created_at)){
                                                        $firstRequestDate =  explode(" ",$query_firstRequestDate[0]->created_at);
                                    
                                                        //check if application created less tahn 5 days compare to last application
                                                        $message = ($dob < 5 ? "can't apply request lower 5 days for each question" : null);
                                                        if($message == null){
                                                    
                                                            $formRequestDate = explode(" ",$query_firstRequestDate[0]->created_at);

                                                            //each application form_ minimum more than 30 days
                                                            $formRequestDate = date_diff(date_create($dateNow),date_create($formRequestDate[0]));
                                                            $formRequestDate = $formRequestDate->format("%R%a days");
                                                            $formRequestDateOperator = substr($formRequestDate[0],0,1); 
                                                            $formRequestDate = substr($formRequestDate,1);

                                                            $message = ( $formRequestDateOperator == "-" ? "can't apply request lower than last request created before ".$query_firstRequestDate[0]->created_at : null);
                                                            if($message == null){

                                                                $rename = '';
                                                                $path = "hrms_upload/leave_request";
                                                                if(isset($file) && $file != null){
                                                                    $validation = \Validator::make(['file' => $file] , ['file' => 'required|max:1000']);
                                                                    $rename = "leave_request__".str_random(6).".".$file->getClientOriginalExtension();
                                                                    \Input::file('file')->move(storage_path($path),$rename);
                                                                  
                                                                    if($validation->fails()){
                                                                        $messsage = "file more than 1 MB";
                                                                        $status = 500;          
                                                                    }else{
                                                                        $db_data = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob)");
                                                                        $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                                                                        $data_id_ex = $data_ex[0]->id;
                                                                        $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$rename','$path','$comment')");
                                                                        
                                                                        $message = "Success apply application";
                                                                        $status = 200;
                                                                    }
                                                                }else{
                                                                  
                                                                    $db_data = \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,day_off,created_at,taken_day,year,balance_day,dayoff_between) values('$name',$type_id,'$from','$to',0,now(),$dob,$year,$remaining_day,' ')");
                                                                    $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                                                                    $data_id_ex = $data_ex[0]->id;
                                                                    $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$rename','$path','$comment')");

                                                                    $message = "Success apply application";
                                                                    $status = 200;
                                                                }
                                                               
                                                            }else{
                                                                $status = 500;
                                                            }
                                                        }else{  
                                                            $status = 500;
                                                        }
                                                    }
                                                    else{
                                                        $remain = $remaining_day - $dob;
                                                        if($remain >= 0){
                                                            $rename = '';
                                                            $path = "hrms_upload/leave_request";
                                                            if(isset($file) && $file != null){
                                                                $validation = \Validator::make(['file' => $file] , ['file' => 'required|max:1000']);
                                                               
                                                                if($validation->fails()){
                                                                    $messsage = "file more than 1 MB";
                                                                    $status = 500;          
                                                                }else{
                                                                     $rename = "leave_request__".str_random(6).".".$file->getClientOriginalExtension();
                                                                     \Input::file('file')->move(storage_path($path),$rename);
                                                              
                                                                    

                                                                    $db_data = \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,day_off,created_at,taken_day,year,balance_day) values 
                                                                                          ('$name',$type_id,'$from','$to',$remaining,now(),$remaining_day,$year)");
                                                                    //$db_data = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$remaining_day)");
                                                                    $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                                                                    $data_id_ex = $data_ex[0]->id;
                                                                    $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$rename','$path','$comment')");
                                                                    
                                                                    $message = "Success apply application";
                                                                    $status = 200;
                                                                }

                                                            }else{
                                                               //return $arrx = [$name,$type_id,$from,$to,$remaining_day]; 
                                                                //$db_data = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$remaining_day)");
                                                                $dayofbetween = \DB::SELECT("select date from  att_schedule where employee_id='$name'  and (date between '$from' and '$to')");
                                                                if($dayofbetween != null){
                                                                    $between = json_encode($dayoff_between);
                                                                }else{
                                                                    $between =  ' ';
                                                                }
                                                                $db_data = \DB::SELECT("insert into leave_request(employee_id,leave_type,from_,to_,day_off,created_at,taken_day,year,balance_day,dayoff_between) values('$name',$type_id,'$from','$to',0,now(),$dob,$year,$remaining_day,'$between')");
                                                                $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                                                                $data_id_ex = $data_ex[0]->id;
                                                                $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$rename','$path','$comment')");

                                                                $message = "Success apply application";
                                                                $status = 200;
                                                            }
                                                        }else{
                                                            $message = "can't request remaining_day deduction day of balance lower than zero";
                                                            $status = 500;

                                                        }
                                                    }
                                                }else{
                                                    $status = 500;
                                                }
                                            }
                                        } 
                                    }else{
                                        $status = 500;
                                    }

                                }else{
                                    $message = "please check contarct date start and contarct date applied correctly";
                                    $status = 500;
                                } 

                            //if false
                            }else{
                                $message = ($arg == false ? "please check date request form can't lower than contract start" : ''); 
                                $message .= ($arg1 == false ? "please check date request to form can't higher than contract end " : '');
                                $status = 500;
                            } 
                        }
                    }catch(\Exception $e){
                        $message = $e; $status = 500;
                    }
                }
            }

            if($type == "Vacation Leave" && $localit == "local"){
                if(isset($json)){
                    $data = json_decode($json,1);
                    $data = $data['data'];
                }

                if($dob == null){
                    $message = "day of Between can't zero day ,please make a distance start date and end date of request, and try again ";
                    $status = 500;
                }else{
                     //from param
                    $from =  $data['From'];
                            
                    //to param
                    $to = $data['To'];

                    //check_date 
                    $explode = explode('-',$from);
                    if($explode > 20){
                        $message = "can't request after 20th";
                        $status = 500;
                    }else{
                        $check_contract =  \DB::SELECT("select contract_start_date,fixed_term_contract from job_history where employee_id =  '$name' ");
                        
                        if(!isset($check_contract[0]->contract_start_date) && !isset($check_contract[0]->fixed_term_contract)){
                            $message = 'please insert contract start and end before take a request';
                            $status = 500;
                        }else{

                            // contract start 
                            $start = $check_contract[0]->contract_start_date;
                            //contract end
                            $end   = $check_contract[0]->fixed_term_contract;
                            
                            //arg1 & check date from contract with reuest 
                            $arg = ($start < $from ? true : false);
                            $arg1 = ($end > $to ? true : false);
                            
                            //if  true
                            if($arg == true && $arg1 == true){
                                
                                //get tenure vor vacation leave minimal

                                $diff = date_diff(date_create($start),date_create($end));
                                $diff = $diff->format("%R%a days");
                                //modify coz : datediff return e.g +277 days, only ned operator and day
                                $diff = explode(" ",$diff);
                                $diff = $diff[0];
                                $diffoperator = substr($diff[0],0,1); 
                                $diff = substr($diff,1); 

                                if($diffoperator == "+"){
                                    $tenure = ( $diff >= 1  && $diff <= 365 ? 1 : ( $diff  >= 366 && $diff <= 730 ?  2 : ($diff >= 731 && $diff <= 1098 ? 3 : 4 )));

                                    //make vacation condition minimum
                                    $vacDays = ($tenure == 1 ? 24 : ($tenure == 2 ? 32 : ($tenure == 3 ? 34 : 42)));
                                    $maxTake = ($tenure == 1 ? 2  : ($tenure == 2 ? 3 : ($tenure == 3 ? 4 : 4)));
                                    //request vacation days 
                                    $check_vacDays = date_diff(date_create($from),date_create($to));
                                    $check_vacDays = $check_vacDays->format("%a");
                                    
                                    $message =  ( $check_vacDays > $vacDays ? 'please create request lower than '.$vacDays." days" : null );
                                    
                                    if($message == null){
                                        //query check request exist or not
                                        $query = \DB::SELECT("select count(employee_id) as count from leave_request where employee_id = '$name' ");
                                        if(isset($query[0]->count) && $query[0]->count == $maxTake){
                                            //..........

                                            $check_req = \DB::select("select created_at from leave_request where employee_id='$name' and leave_type=$type_id and from_='$from' and to_='$to' ");
                                            if($check != null){
                                                $status = 500;
                                                $message = "request already created before";
                                            }else{
                                                //$db_data = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob)"
                                                $rename = '';
                                                $path = "hrms_upload/leave_request";
                                                if(isset($file) && $file != null){
                                                    $validation = \Validator::make(['file' => $file] , ['file' => 'required|max:1000']);
                                                    $rename = "leave_request__".str_random(6).".".$file->getClientOriginalExtension();
                                                    \Input::file('file')->move(storage_path($path),$rename);
                                                  
                                                    if($validation->fails()){
                                                        $messsage = "file more than 1 MB";
                                                        $status = 500;          
                                                    }else{
                                                        $db_data = \DB::SELECT("CALL insert into leave_request(employee_id,leave_type,from_,dayoff_between,status_id) values ('$name',$type_id,'$from','$to',$dob,3)");
                                                        $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                                                        $data_id_ex = $data_ex[0]->id;
                                                        $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$imageName','$path','$comment')");
                                                        
                                                        $message = "Oops , you request more than 2 times in one year of your tenure, rejected automatically";
                                                        $status = 500;
                                                    }
                                                }else{
                                                    $db_data = \DB::SELECT("CALL insert into leave_request(employee_id,leave_type,from_,dayoff_between,status_id) values ('$name',$type_id,'$from','$to',$dob,3)");
                                                    $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                                                    $data_id_ex = $data_ex[0]->id;
                                                    $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$imageName','$path','$comment')");

                                                    $message = "Oops , you request more than 2 times in one year of your tenure, rejected automatically";
                                                    $status = 500;
                                                }
                                            }
                                        }else{
                                            if($localit == 'expat'){
                                                $dateNow = date("Y-m-d");
                                                $dateExpatReq = date_diff(date_create($dateNow),date_create($from));
                                                $dateExpatReq = $dateExpatReq->format("%R%a days");
                                                
                                                //check reuest time x_time < 2 month and time_x > 4 month
                                                //modify coz : datediff return e.g +277 days, only nedd operator and day
                                                $dateExpatReq = explode(" ",$dateExpatReq);
                                                $dateExpatReq = $dateExpatReq[0];
                                                $dateExpatReqOperator = substr($dateExpatReq,0,1); 
                                                $dateExpatReq = substr($dateExpatReq,1);

                                                if($dateExpatReqOperator == "+"){
                                                    $message = ($dateExpatReq >= 1 && $dateExpatReq <= 60 ? 'for expat employee request should be 2 month before date request' : ( $dateExpatReq >  120 ? 'for expat employee should be less than 4 month before request applied' : null));
                                                }else{
                                                    $message =  "request can't lower than current date";
                                                }
                                                if($message == null){

                                                    //check date request each application request
                                                    $query_firstRequestDate = \DB::SELECT("select created_at,from_ from leave_request where employee_id = '$name' order by id DESC");
                                                   
                                                    if(isset($query_firstRequestDate[0]->created_at)){
                                                        $firstRequestDate =  explode(" ",$query_firstRequestDate[0]->created_at);
                                                        
                                                        //each application created_at minimum 5 days
                                                        $firstRequestDate = date_diff(date_create($dateNow),date_create($firstRequestDate[0]));
                                                        $firstRequestDate = $firstRequestDate->format("%R%a days");
                                                        $firstRequestDateOperator = substr($firstRequestDate[0],0,1); 
                                                        $firstRequestDate = substr($diff,1);

                                                        //check if application created less tahn 5 days compare to last application
                                                        $message = ($firstRequestDateOperator == "-" ? "can't apply request lower than last request created" : null);
                                                        if($message == null){
                                                            $formRequestDate = explode(" ",$query_firstRequestDate[0]->from_);

                                                            //each application form_ minimum more than 30 days
                                                            $formRequestDate = date_diff($dateNow,$formRequestDate[0]);
                                                            $formRequestDate = $formRequestDate->format("%R%a days");
                                                            $formRequestDateOperator = substr($formRequestDate[0],0,1); 
                                                            $formRequestDate = substr($formRequestDate,1);

                                                            $message = ( $formRequestDateOperator == "-" ? "can't apply request lower than last request created" : null);
                                                            if($message != null){

                                                                $rename = '';
                                                                $path = "hrms_upload/leave_request";
                                                                if(isset($file) && $file != null){
                                                                    $validation = \Validator::make(['file' => $file] , ['file' => 'required|max:1000']);
                                                                    $rename = "leave_request__".str_random(6).".".$file->getClientOriginalExtension();
                                                                    \Input::file('file')->move(storage_path($path),$rename);
                                                                  
                                                                    if($validation->fails()){
                                                                        $messsage = "file more than 1 MB";
                                                                        $status = 500;          
                                                                    }else{
                                                                        $db_data = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob)");
                                                                        $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                                                                        $data_id_ex = $data_ex[0]->id;
                                                                        $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$imageName','$path','$comment')");
                                                                        
                                                                        $message = "Success apply application";
                                                                        $status = 200;
                                                                    }
                                                                }else{
                                                                    $db_data = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob)");
                                                                    $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
                                                                    $data_id_ex = $data_ex[0]->id;
                                                                    $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$imageName','$path','$comment')");

                                                                    $message = "Success apply application";
                                                                    $status = 200;
                                                                }
                                                                $message = "success";
                                                            }else{
                                                                $status = 500;
                                                            }
                                                        }else{  
                                                            $status = 500;
                                                        }
                                                    }else{
                                                        $message = "success";
                                                    }
                                                }else{
                                                    $status = 500;
                                                }
                                            }
                                        } 
                                    }else{
                                        $status = 500;
                                    }

                                }else{
                                    $message = "please check contarct date start and contarct date applied correctly";
                                    $status = 500;
                                } 

                            //if false
                            }else{
                                $message = ($arg == false ? "please check date request form can't lower than contract start" : ''); 
                                $message .= ($arg1 == false ? "please check date request to form can't higher than contract end " : '');
                                $status = 500;
                            } 
                        }
                    }

                }
            }

            if($type == "Emergency Leave" && $localit == "local"){
                if(isset($json)){
                    $data = json_decode($json,1);
                    $data = $data['data'];
                }

                if($dob == null ){
                    $message = "day of Between can't zero day ,please make a distance start date and end date of request, and try again ";
                    $status = 500;
                }else{
                    if(!isset($file) && $dob > 1){
                        $message = "if you want request Emergency leave more than one day please attach medical file";
                        $status = 500;
                    }else{

                    }
                }
            }

            if($type == "Sick Leave" ){
                 if(isset($json)){
                    $data = json_decode($json,1);
                    $data = $data['data'];
                }

                if($dob == null ){
                    $message = "day of Between can't zero day ,please make a distance start date and end date of request, and try again ";
                    $status = 500;
                }

                if(!isset($file) == null ){
                    $message = "request must be upload include with medical report";
                    $status = 500;
                }

                $key = \Input::get('key');
                $encode = base64_decode($key);
                $explode = explode("-",$encode);
                $att = $explode[1];
                $ldap = \DB::SELECT("select role_name from ldap,role where ldap.employee_id = '$att' and  ldap.role_id=role.role_id");
                $ldap = $ldap[0]->role_name;
                $explode = explode(" ",$ldap);

                $state = '';
                foreach ($explode as $key => $value) {
                    if(strtolower($value) == "hr"){
                        $state = true;
                    }
                }

                if($state == null){
                    $message = "request only can do it by HR an related";
                    $status = 500;
                }else{
                    
                }
            }

            if($type == "Maternity Leave"){
                 if(isset($json)){
                    $data = json_decode($json,1);
                    $data = $data['data'];
                }
                $key = \Input::get('key');
                $encode = base64_decode($key);
                $explode = explode("-",$encode);
                $att = $explode[1];
                $ldap = \DB::SELECT("select role_name from ldap,role where ldap.employee_id = '$att' and  ldap.role_id=role.role_id");
                $ldap = $ldap[0]->role_name;
               
                if($dob == null ){
                    $message = "day of Between can't zero day ,please make a distance start date and end date of request, and try again ";
                    $status = 500;
                }
                $date = date('Y-m-d');
                $from =  $data['From'];
                 //to param
                $to = $data['To'];
                $diff = date_diff(date_create($from),date_create($to));
                $diffa = $diff->format("%a");
                $diffb = $diff->format("%R");
                if($diffb == '-' || $diffa > 60){
                    $message = "please make a sure date you input correct ";
                    $status = 500;
                }


                if($localit == "expat"){
                    if($ldap == "Admin-Correct"){
                        if($dob > 150){
                             $message = "maximum day taken 150 days only ";
                             $status = 500;
                        }else{

                        }
                    }else{
                        $message = "only admin can request for expat";
                        $status =500;
                    }
                }else{
                    $state = '';
                    foreach ($explode as $key => $value) {
                        if(strtolower($value) == "hr"){
                            $state = true;
                        }
                    }

                    if($state ==  true){
                        $message = "";
                    }else{
                        $message = "only hr can request for local";
                        $status =500;
                    }
                }
            }

            if($type == "Paternity Leave"){
                 if(isset($json)){
                    $data = json_decode($json,1);
                    $data = $data['data'];
                }

                $key = \Input::get('key');
                $encode = base64_decode($key);
                $explode = explode("-",$encode);
                $att = $explode[1];

                $male = \DB::SELECT("select gender from emp='$att' ");
                if($male[0]->gender != "male"){
                    $message = "only male can request";
                    $status = 500;
                }else if($dob == null ){
                    $message = "day of Between can't zero day ,please make a distance start date and end date of request, and try again ";
                    $status = 500;
                }else if($dob > 7){
                    $message = "maximum day taken 7 days or less";
                    $status = 500;
                }else{
                    $date = date('Y-m-d');
                    $from =  $data['From'];
                     //to param
                    $to = $data['To'];
                    $diff = date_diff(date_create($from),date_create($to));
                    $diffa = $diff->format("%a");
                    $diffb = $diff->format("%R");
                    if($diffb == '-' || $diffa > 60){
                        $message = "please make a sure date you input correct ";
                        $status = 500;
                    }
                }

            }

            if($type == "Bereavement Leave"){
                 if(isset($json)){
                    $data = json_decode($json,1);
                    $data = $data['data'];
                }
            }



            return $message;


            //end vacation

            // if($message == null & $status == null){
            //     $message = $access[0]; $status = $access[1]; $data=$access[2];
            //     return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
            // }else if(!isset($data) || $data == null){
            //     return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>[]],$status);
            // }else{
            //     return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
            // }

         //    if($type == "Accumulation Day Off" )
         //        {
         //            $validation = \Validator::make(
         //                ['name' => $name,'LeaveType' => $type,"aggred"  => $aggree],
         //                ['name' => 'required|integer','LeaveType' => "required|string","aggred"  => "required"]
         //                );
         //        }else{
         //            $validation = \Validator::make(
         //                ['name' => $name,'LeaveType' => $type,'From' => $from,'To'    => $to,'DayOffBetween' => $dob,"aggred"  => $aggree],
         //                ['name' => 'required|integer','LeaveType' => "required|string",'From' => 'required|date','To'    => 'required|date','DayOffBetween' => 'required|integer',"aggred"  => "required"]
         //                );
         //            }

         //            if($validation->fails()){
         //                $check = $validation->errors()->all();
         //                return  $request->getMessage("failed",$check, 500, $access[3]);
         //            }else{

         //                if($type  == "Vacation Leave" || $type  == "Enhance Vacation Leave"){
         //                    if($aggree == "1" ){

         //                        if($ado_x == "Yes"){
         //                            $file = \Input::file('file');
         //                            if(isset($file)){
         //                            if(isset($data['data']['date1'])){$date1 =  $data['data']['date1'];}else{$date1 = null;}
         //                            if(isset($data['data']['date2'])){$date2 =  $data['data']['date2'];}else{$date2 = null;}
         //                            if(isset($data['data']['date3'])){$date3 =  $data['data']['date3'];}else{$date3 = null;}
         //                            if(isset($data['data']['date4'])){$date4 =  $data['data']['date4'];}else{$date4 = null;}

         //                            if(isset($data['data']['ado1'])){$ado1 =  $data['data']['ado1'];}else{$ado1 = null;}
         //                            if(isset($data['data']['ado2'])){$ado2 =  $data['data']['ado2'];}else{$ado2 = null;}
         //                            if(isset($data['data']['ado3'])){$ado3 =  $data['data']['ado3'];}else{$ado3 = null;}
         //                            if(isset($data['data']['ado4'])){$ado4 =  $data['data']['ado4'];}else{$ado4 = null;}

         //                            if(isset($data['data']['nextday'])){$nextday =  $data['data']['nextday'];}else{$nextday = null;}
         //                          }else{
									// if(isset($data['date1'])){$date1 =  $data['date1'];}else{$date1 = null;}
         //                            if(isset($data['date2'])){$date2 =  $data['date2'];}else{$date2 = null;}
         //                            if(isset($data['date3'])){$date3 =  $data['date3'];}else{$date3 = null;}
         //                            if(isset($data['date4'])){$date4 =  $data['date4'];}else{$date4 = null;}
									// if(isset($data['ado1'])){$ado1 =  $data['ado1'];}else{$ado1 = null;}
         //                            if(isset($data['ado2'])){$ado2 =  $data['ado2'];}else{$ado2 = null;}
         //                            if(isset($data['ado3'])){$ado3 =  $data['ado3'];}else{$ado3 = null;}
         //                            if(isset($data['ado4'])){$ado4 =  $data['ado4'];}else{$ado4 = null;}
									// if(isset($data['nextday'])){$nextday =  $data['nextday'];}else{$nextday = null;}
         //                          }
         //                            $ado = [
         //                                    ["date" => $date1, "ado_name" => $ado1],
         //                                    ["date" => $date2, "ado_name" => $ado2],
         //                                    ["date" => $date3, "ado_name" => $ado3],
         //                                    ["date" => $date4, "ado_name" => $ado4]
         //                                ];

         //                            $validation = \Validator::make(
         //                                ['date1' => $date1,
         //                                'date2' => $date2,
         //                                'date3' => $date3,
         //                                'date4' => $date4,
         //                                'ado1' => $ado1,
         //                                'ado2' => $ado2,
         //                                'ado3' => $ado3,
         //                                'ado4' => $ado4],

         //                                ['date1' => 'required|date',
         //                                'date2' => 'required|date',
         //                                'date3' => 'required|date',
         //                                'date4' => 'required|date',
         //                                'ado1' => 'required|integer',
         //                                'ado2' => 'required|integer',
         //                                'ado3' => 'required|integer',
         //                                'ado4' => 'required|integer']

         //                                );

         //                            if($validation->fails()){
         //                                $check = $validation->errors()->all();
         //                                return  $request->getMessage($check,[], 500, $access[3]);
         //                            }else{
         //                                $image = \Input::file('file');
         //                                if (isset($image)){
         //                                    $imageName = $image->getClientOriginalName();
         //                                    $ext = \Input::file('file')->getClientOriginalExtension();
         //                                    $path = "hrms_upload/leave_request";
         //                                    $size = $image->getSize();

         //                                    $req_image = $request->check_image($ext,$size,$image,$path,$access[3]);
         //                                    if($req_image != null && $req_image == "size"){
         //                                        return $request->getMessage("failed image format supported but size of image more than 1 MB",[],200,$access);
         //                                    }
         //                                    if($req_image != null && $req_image == "format"){
         //                                        return $request->getMessage("unsupported format",[],200,$access);
         //                                    }

         //                                }else{
         //                                    $imageName =  "NULL";
         //                                    $path = "NULL";
         //                                }

         //                                if(!isset($comment)){
         //                                    $comment = "NULL";
         //                                }

         //                                $check_req = \DB::select("select created_at from leave_request where employee_id='$name' and leave_type=$type_id and from_='$from' and to_='$to' ");
         //                                if($check_req != null){
         //                                  return  $request->getMessage("Request already created at ".$check_req[0]->created_at,[], 500, $access[3]);
         //                                }
         //                                $count = count($ado)-1;
         //                                for($i = 0; $i <=$count; $i++){
         //                                    if($i == 0){
         //                                        $name_ado = "1st";
         //                                    }elseif($i == 1){
         //                                        $name_ado = "2nd";
         //                                    }elseif($i == 2){
         //                                        $name_ado = "3rd";
         //                                    }else{
         //                                        $name_ado = "4th";
         //                                    }

         //                                    $date_ado = $ado[$i]['date'];
         //                                    $new_ado = $ado[$i]['ado_name'];
         //                                    $db = \DB::SELECT("call insert_leave_ado($type_id,'$name','$name_ado','$nextday',$new_ado)");
         //                                }

         //                                $db = \DB::SELECT("call insert_leave_ado($type_id,'$name','$name_ado','$nextday',null)");
         //                                $db_data = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob)");+
         //                                $data_ex = \Db::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
         //                                $data_id_ex = $data_ex[0]->id;
         //                                $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$imageName','$path','$comment')");

         //                                if(isset($db) && isset($db_data)){
         //                                    if($db_data != null){
									// 			if(isset($db_data[0]->Message)){
									// 				$data = $db_data[0]->Message;
									// 			}else{
									// 				$data = $db_data;
									// 			}

         //                                    }else{
         //                                        $data = [];
         //                                    }
         //                                  return  $request->getMessage("Success",$data, 200, $access[3]);
         //                                }
         //                                                                    }
         //                        }else{
         //                                $image = \Input::file('file');
         //                                if (isset($image)){
                                            
         //                                    $imageName = $image->getClientOriginalName();
         //                                    $ext = \Input::file('file')->getClientOriginalExtension();
         //                                    $path = "hrms_upload/leave_request";
         //                                    $size = $image->getSize();

         //                                    $rename = "leave_request__".str_random(6).".".$image->getClientOriginalExtension();

         //                                      //if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
         //                                      if($size > 104857600){
         //                                        return \Response::json(['header'=>['message'=>'Image size only support up to 10 MB','status'=>500],'data'=>null],500);
         //                                      }else{
         //                                        \Input::file('file')->move(storage_path($path),$rename);
         //                                      }
         //                                    // }else{
         //                                    //     return \Response::json(['header'=>['message'=>'Unsupported format file','status'=>500],'data'=>null],500);
         //                                    // }

         //                                    $imageName = $rename;

         //                                }else{
         //                                    $imageName =  "NULL";
         //                                    $path = "NULL";
         //                                }

         //                                if(!isset($comment)){
         //                                    $comment = "NULL";
         //                                }
         //                                $check_req = \DB::select("select created_at from leave_request where employee_id='$name' and leave_type=$type_id and from_='$from' and to_='$to' ");
         //                                if($check_req != null){
         //                                  return  $request->getMessage("Request already created at ".$check_req[0]->created_at,[], 500, $access[3]);
         //                                }
         //                                //$db = \DB::SELECT("call insert_leave_ado($type_id,'$name','$name_ado','$nextday',null)");
         //                                $db_data = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob)");
         //                                $data_ex = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
         //                                $data_id_ex = $data_ex[0]->id;
         //                                $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$imageName','$path','$comment')");
         //                                if(isset($data_ex) && isset($db_data) && isset($data_id_ex)){
         //                                  //return $db_data;
									// 													if(isset($db_data)){
         //                                        if(isset($db_data[0]->Message)){
									// 																$data = $db_data;
									// 																$message = "Error";
									// 																$status = 200;
									// 															}else{
									// 																$data = $db_data;
									// 																$message = "Success";
									// 																$status = 200;
									// 															}
         //                                    }else{
         //                                        $data = [];
									// 															$message = "Erorr";
									// 															$status =  500;
         //                                    }
         //                                    return  $request->getMessage($message,$data, $status, $access[3]);
         //                                }else{
         //                                    return  $request->getMessage("Failed to insert data",[], 500, $access[3]);
         //                                }
         //                        }
         //                      }
         //                }elseif($type  == "Accumulation Day Off"){

         //                    if($aggree == 1 ){

         //                      $file = \Input::file('file');
         //                      if(isset($file)){
         //                      if(isset($data['data']['date1'])){$date1 =  $data['data']['date1'];}else{$date1 = null;}
         //                      if(isset($data['data']['date2'])){$date2 =  $data['data']['date2'];}else{$date2 = null;}
         //                      if(isset($data['data']['date3'])){$date3 =  $data['data']['date3'];}else{$date3 = null;}
         //                      if(isset($data['data']['date4'])){$date4 =  $data['data']['date4'];}else{$date4 = null;}

         //                      if(isset($data['data']['ado1'])){$ado1 =  $data['data']['ado1'];}else{$ado1 = null;}
         //                      if(isset($data['data']['ado2'])){$ado2 =  $data['data']['ado2'];}else{$ado2 = null;}
         //                      if(isset($data['data']['ado3'])){$ado3 =  $data['data']['ado3'];}else{$ado3 = null;}
         //                      if(isset($data['data']['ado4'])){$ado4 =  $data['data']['ado4'];}else{$ado4 = null;}

         //                      if(isset($data['data']['nextday'])){$nextday =  $data['data']['nextday'];}else{$nextday = null;}
         //                    }else{
         //                      if(isset($data['date1'])){$date1 =  $data['date1'];}else{$date1 = null;}
         //                      if(isset($data['date2'])){$date2 =  $data['date2'];}else{$date2 = null;}
         //                      if(isset($data['date3'])){$date3 =  $data['date3'];}else{$date3 = null;}
         //                      if(isset($data['date4'])){$date4 =  $data['date4'];}else{$date4 = null;}
         //                      if(isset($data['ado1'])){$ado1 =  $data['ado1'];}else{$ado1 = null;}
         //                      if(isset($data['ado2'])){$ado2 =  $data['ado2'];}else{$ado2 = null;}
         //                      if(isset($data['ado3'])){$ado3 =  $data['ado3'];}else{$ado3 = null;}
         //                      if(isset($data['ado4'])){$ado4 =  $data['ado4'];}else{$ado4 = null;}
         //                      if(isset($data['nextday'])){$nextday =  $data['nextday'];}else{$nextday = null;}
         //                    }
         //                        $ado = [
         //                                ["date" => $date1, "ado_name" => $ado1],
         //                                ["date" => $date2, "ado_name" => $ado2],
         //                                ["date" => $date3, "ado_name" => $ado3],
         //                                      ["date" => $date4, "ado_name" => $ado4]
         //                            ];

         //                        $validation = \Validator::make(
         //                            ['date1' => $date1,
         //                            'date2' => $date2,
         //                            'date3' => $date3,
         //                            'date4' => $date4,
         //                            'ado1' => $ado1,
         //                            'ado2' => $ado2,
         //                            'ado3' => $ado3,
         //                            'ado4' => $ado4],

         //                            ['date1' => 'required|date',
         //                            'date2' => 'required|date',
         //                            'date3' => 'required|date',
         //                            'date4' => 'required|date',
         //                            'ado1' => 'required|integer',
         //                            'ado2' => 'required|integer',
         //                            'ado3' => 'required|integer',
         //                            'ado4' => 'required|integer']

         //                            );

         //                        if($validation->fails()){
         //                            $check = $validation->errors()->all();
         //                            return  $request->getMessage($check,[], 500, $access[3]);
         //                        }else{
         //                            $image = \Input::file('file');
         //                            if (isset($image)){

         //                                $imageName = $image->getClientOriginalName();
         //                                $ext = \Input::file('file')->getClientOriginalExtension();
         //                                $path = "hrms_upload/leave_request";
         //                                $size = $image->getSize();

         //                                $rename = "Leave_Request".str_random(6).".".$image->getClientOriginalExtension();

         //                                 // if($ext == "ext" || $ext == "exe" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
         //                                  if($size > 104857600){
         //                                    return \Response::json(['header'=>['message'=>'Image size only support up to 10 MB','status'=>500],'data'=>null],500);
         //                                  }else{
         //                                    \Input::file('file')->move(storage_path($path),$rename);
         //                                  }
         //                                // }else{
         //                                //     return \Response::json(['header'=>['message'=>'Unsupported format file','status'=>500],'data'=>null],500);
         //                                // }
         //                                $imagename = $rename;
         //                            }else{
         //                                $imageName =  "NULL";
         //                                $path = "NULL";
         //                            }

         //                            if(!isset($comment)){
         //                                $comment = "NULL";
         //                            }

         //                            // $check_req = \DB::select("select created_at from leave_request where employee_id='$name' and leave_type=$type_id and ");
         //                            // if($check_req != null){
         //                            //   return  $request->getMessage("Request already created at ".$check_req[0]->created_at,[], 500, $access[3]);
         //                            // }

         //                            $count = count($ado)-1;
         //                            for($i = 0; $i <=$count; $i++){
         //                                if($i == 0){
         //                                    $name_ado = "1st";
         //                                }elseif($i == 1){
         //                                    $name_ado = "2nd";
         //                                }elseif($i == 2){
         //                                    $name_ado = "3rd";
         //                                }elseif($i == 3){
         //                                    $name_ado = "4th";
         //                                }else{
         //                                    $name_ado = null;
         //                                }

         //                                $date_ado = $ado[$i]['date'];
         //                                $new_ado = $ado[$i]['ado_name'];
         //                                $db = \DB::SELECT("call insert_leave_ado($type_id,'$name','$name_ado','$nextday',$new_ado)");
         //                            }
         //                            $db = \DB::SELECT("call insert_leave_ado($type_id,'$name','$name_ado','$nextday',$new_ado)");
         //                            $from = "NULL";
         //                            $to = "NULL";
         //                            $dob = "NULL";
         //                            $db_data = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob)");
         //                            $data_id = \DB::SELECT("select id from leave_request where employee_id='$name' order by id DESC LIMIT 1");
         //                            $data_req = $data_id[0]->id;
         //                            $comment = \DB::SELECT("CALL insert_leave_request_comment($data_req,'$name','$imageName','$path','$comment')");
         //                            if(isset($db) && isset($db_data)){
         //                                if($db_data != null){
         //                                    $data = $db_data[0]->error;
         //                                }else{
         //                                    $data = [];
         //                                }
         //                                return  $request->getMessage("Success",$data, 200, $access[3]);
         //                            }
         //                        }
         //                    }else{
         //                        return  $request->getMessage("failed",[], 500, $access[3]);
         //                    }
         //                 }else{

         //                    $image = \Input::file('file');
         //                    if (isset($image)){
         //                        $imageName = $image->getClientOriginalName();
         //                        $ext = \Input::file('file')->getClientOriginalExtension();
         //                        $path = "hrms_upload/leave_request";
         //                        $size = $image->getSize();

         //                        $rename = "Leave_Request_".str_random(6).".".$image->getClientOriginalExtension();

         //                        //  if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
         //                          if($size > 104857600){
         //                            return \Response::json(['header'=>['message'=>'Image size only support up to 10 MB','status'=>500],'data'=>null],500);
         //                          }else{
         //                            \Input::file('file')->move(storage_path($path),$rename);
         //                          }
         //                        // }else{
         //                        //     return \Response::json(['header'=>['message'=>'Unsupported format file','status'=>500],'data'=>null],500);
         //                        // }
         //                        $imageName = $rename;

         //                    }else{
         //                        $imageName =  "NULL";
         //                        $path = "NULL";
         //                    }

         //                    if(!isset($comment)){
         //                        $comment = "NULL";
         //                    }

         //                    $check_req = \DB::select("select created_at from leave_request where employee_id='$name' and leave_type=$type_id and from_='$from' and to_='$to' ");
         //                    if($check_req != null){
         //                      return  $request->getMessage("Request already created at ".$check_req[0]->created_at,[], 500, $access[3]);
         //                    }

         //                    $db = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob)");
         //                    $data_ex = \Db::SELECT("select id from leave_request where employee_id='$name' order by id DESC limit 1");
         //                    $data_id_ex = $data_ex[0]->id;
         //                    $comment = \DB::SELECT("CALL insert_leave_request_comment($data_id_ex,'$name','$imageName','$path','$comment')");
         //                    if(isset($db)){
         //                      if(isset($db[0]->error)){
         //                        $data = $db[0]->error;
         //                      }else{
         //                        $data = [];
         //                      }
         //                    }else{
         //                        $data = [];
         //                    }
         //                    return  $request->getMessage("success to apply request ",[], 200, $access[3]);
         //                }
         //            }


         //    }else{
         //    $message = $access[0]; $status = $access[1]; $data=$access[2];
         //    return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
         //    }
        }
    }
}
