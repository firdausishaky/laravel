<?php namespace Larasite\Http\Controllers\Leave\Entitlements;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Library\show_approver;
use Larasite\Model\Leave\Report\Report_Model;
class Report_Ctrl extends Controller {

	/*
	employee autocomplete :
	 url : hrms.dev/api/leave/Report?key=VXdRUlhXMjJoSkRQSndKeEdmZ2FDa3djZVlXMEd6eE41dW84eERrai0yMDE0ODg4
	parameter : name = character


	*/

	protected $form = "70";

	public function viewEmp()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$Report = new Report_Model;
			$input = \Input::all();
			$name = \Input::get('name');
			$rule = [
					'name' => 'required|Regex:/^[A-Za-z]+$/'
					];
			$validation = \Validator::make($input,$rule);
			if($validation->fails()){
				$check = $validation->errors()->all();
				return $Report->getMessage('input format invalidate',$check,500,$access[3]);
			}else{
				$query = \DB::SELECT("CALL Search_emp_by_name('$name')");
				return $Report->index($query,$access[3]);
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function index(){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$Report = new Report_Model;
			$department = \DB::SELECT("select * from department where id<>1 ");
			$job = \DB::SELECT("select id,title from job");
			$leave = \DB::SELECT("select id,leave_type from leave_type");
			$data = ["department" => $department, "job" => $job, "leave" => $leave];
			return $Report->index($data,$access[3]);
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function report()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		$show_approver  = new show_approver;
		if($access[1] == 200){
			$Report = new Report_Model;
			$input = \Input::all();
			$name = \Input::get('name');
			$generate = \Input::get('generate');
			$date_report = \Input::get('date');
			$query_y = [];
			if($generate == 1){
				$rule = [
					'generate' => 'required|integer',
					'name' => 'required|integer',
					'generate' => 'required|integer',
					];
				$validation = \Validator::make($input,$rule);

				if(!isset($validator)){
					// $final  = \DB::SELECT("select (concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name))as employee, leave_request.employee_id as emp_id leave_type.name as leave_type,
					// 					   leave_request.leave_type as type_id , leave_request.created_at as DateTaken, count(select * from leave_request where employee_id = (select emp_id) and leave_type = (select type_id)) as taken_day,
					// 					   (select sum(date_diff(to_,from_)+1) from leave_request where employee_id = (select emp_id) and leave_type =  (select type_id)) as EntitlementDay, 

					// 					   from leave_request,emp,leave_type where leave_request.employee_id =  emp.employee_id and leave_request.leave_type = leave_type.id ");


						/*$final[]  = \DB::SELECT("select 
							leave_request.add_on,
							leave_request.approval,
							leave_request.approver,
							leave_request.balance_day,
							leave_request.created_at,
							leave_request.day_off,
							leave_request.dayoff_between,
							leave_request.employee_id,
							leave_request.from_,
							leave_request.id,
							leave_type.leave_type,							
							leave_request.status_id,
							leave_request.taken_day,
							leave_request.from_,
							leave_request.to_,
							2 as master_type,
							concat(leave_request.from_,' - ',leave_request.to_) as schedule,
							leave_request.year,
							att_status.status,
							concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as EmployeeName
							from leave_request,att_status,leave_type,emp where leave_request.leave_type =  leave_type.id and emp.employee_id  = leave_request.employee_id and att_status.status_id = leave_request.status_id and leave_request.status_id != 1")[0];  */
					// $final  = \DB::SELECT("CALL view_all_leave_balance");
					$final = \DB::SELECT("select 
										leave_request.id,
										leave_request.add_on,
										leave_request.approval,
										leave_request.approver,
										leave_request.balance_day,
										leave_request.created_at,
										leave_request.day_off,
										leave_request.dayoff_between,
										leave_request.employee_id,
										leave_request.from_,
										leave_request.leave_type,
										leave_type.leave_type,							
										leave_request.status_id,
										leave_request.taken_day,
										leave_request.from_,
										leave_request.to_,
										2 as master_type,
										concat(leave_request.from_,' - ',leave_request.to_) as schedule,
										leave_request.year,
										att_status.status,
										concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as EmployeeName
										from leave_request,att_status,leave_type,emp 
										where 
										leave_request.leave_type =  leave_type.id and 
										emp.employee_id  = leave_request.employee_id and 
										att_status.status_id = leave_request.status_id and 
										leave_request.status_id = 2 and
										leave_request.employee_id = '$name'");
					/*return [$final];
					foreach ($final as $key => $value) {
						if($value->employee_id != $name){
							unset($final[$key]);
						}
					}*/

					if($final == null){
						return \Response::json(['header'=>['message'=>'Empty data','status'=>500],'data'=>[]],500);
					}else{
						$date =  date('Y').'-12-31';

						function Find($arr){
							foreach ($arr as $keyss => $valuess) {
								$obj_key=array_keys($valuess);
				   				if($valuess[$obj_key[0]] == 1){ return $obj_key[0]; }
				   			}
						}

						foreach ($final as $key => $value) {
							$lock   =  $value;	
							$idn = $value->id;
							// $select = \DB::SELECT("select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as approval from leave_request,emp where id = $idn and emp.employee_id =  leave_request.approval ");							
							// if(isset($select[0]->approval)){
							// 	$appr = $select[0]->approval;
							// }else{
							// 	$appr = "";
							// }

							if(true){
								if(isset($date_report)  and  $date_report!= null){
										$year_now  = $date_report;
									}else{
										$year_now  = date('Y');
								}

								//return "select * from leave_request  where employee_id = '$name' and year = '$year_now' order by id ASC limit 1";
								$select =    \Db::SELECT("select * from leave_request  where employee_id = '$name' and year = '$year_now' order by id ASC limit 1");
								
								if(!isset($select[0]->balance_day)  and  !isset($select[0]->taken_day)){
										$query= ["value" => [], "date" => $year_now];

										return $Report->getMessage('Data not exist',$query,200,$access[3]);
									
									///return response()->json(['header' => ['message' => "can't found your record",  'status' => 200],'data' => []],200);
								}

								$now = strtotime($select[0]->to_); // or your date as well
								$your_date = strtotime($select[0]->from_);
								$datediff = $now - $your_date;

								$select[0]->taken_day = (round($datediff / (60 * 60 * 24)))+1;

								$get_balance  =  $select[0]->balance_day - 1 + $select[0]->taken_day;
								
								$appr = [];

								if($value->approver  !=  null and $value->approver != 0){
									$pool  = \Db::SELECT("select * from pool_request  where id_req =  $idn and  master_type  = 2 ");
									if(count($pool) > 0){
										$pool =  $pool[0]->json_data;
										$pool =    json_decode($pool,1);

										//$dtaken =  $lock->from_.' - '.$lock->to_;
										//if($dtaken == "2018-08-04 - 2018-08-14"){

											$types = ["leave_type"=>$lock->leave_type." Leave"];
											$dt = $show_approver->show_data($pool,$types);
											$arr_key = array_keys($dt['flow_app']);
											if(isset($dt['flow_app'])){
												for ($x=0; $x < count($dt['flow_app']); $x++) { 
													if($dt['flow_app'][$arr_key[$x]]['name'] == "Super User"){ $dt['flow_app'][$arr_key[$x]]['job_approval'] = 'SU'; }

													$exist = array_search($dt['flow_app'][$arr_key[$x]]['name']." (".$dt['flow_app'][$arr_key[$x]]['job_approval'].")", $appr);
													if(gettype($exist) == 'boolean'){
														if($dt['flow_app'][$arr_key[$x]]['name']){
															$appr[$x] = $dt['flow_app'][$arr_key[$x]]['name']." (".$dt['flow_app'][$arr_key[$x]]['job_approval'].")";
														}
													}
												}
												$appr = implode(", ", $appr);
											}
									}
									/*$pool  = \Db::SELECT("select * from pool_request  where id_req =  $idn and  master_type  = 2 ")[0]->json_data;
									$pool =    json_decode($pool,1);

									//$dtaken =  $lock->from_.' - '.$lock->to_;
									//if($dtaken == "2018-08-04 - 2018-08-14"){

										$types = ["leave_type"=>$lock->leave_type." Leave"];
										$dt = $show_approver->show_data($pool,$types);
										$arr_key = array_keys($dt['flow_app']);
										if(isset($dt['flow_app'])){
											for ($x=0; $x < count($dt['flow_app']); $x++) { 
												if($dt['flow_app'][$arr_key[$x]]['name'] == "Super User"){ $dt['flow_app'][$arr_key[$x]]['job_approval'] = 'SU'; }

												$exist = array_search($dt['flow_app'][$arr_key[$x]]['name']." (".$dt['flow_app'][$arr_key[$x]]['job_approval'].")", $appr);
												if(gettype($exist) == 'boolean'){
													if($dt['flow_app'][$arr_key[$x]]['name']){
														$appr[$x] = $dt['flow_app'][$arr_key[$x]]['name']." (".$dt['flow_app'][$arr_key[$x]]['job_approval'].")";
													}
												}
											}
											$appr = implode(", ", $appr);
										}
*/
									//}
									/*	// DI GANTI DENGAN LIB
								   	$get_first = $pool['req_flow'];
								   	if(isset($get_first['employee_requestor'])){
								   		// employee dibuatkan request oleh  atasan
								   		// hanya spv dan hr / it director
								   		if($get_first['employee_requestor'][1] == 'hr'){
								   			$appr_id[] = Find($pool['sup']);
								   		}elseif($get_first['employee_requestor'][1] == 'sup'){
								   			$appr_id[] = Find($pool['hr']);
								   		}
								   		$appr_id[]=$name;
								   	}else{
								   		if(isset($get_first['hr'])){
								   			if(count($get_first['hr'])>0){
								   				$appr_id[] = Find($pool['hr']);
								   				
								   			}
								   		}
								   		elseif(isset($get_first['sup'])){
								   			if(count($get_first['sup'])>0){
								   				$appr_id[] = Find($pool['sup']);
								   			}
								   		}
								   	}
								   	$ids="";
								   	for ($i=0; $i < count($appr_id); $i++) { 
								   		if(strlen($ids)==0){
								   			$ids.="'".$appr_id[$i]."'";
								   		}else{
								   			$ids.=",'".$appr_id[$i]."'";
								   		}
								   	}
								   	$q=\DB::SELECT("select distinct emp.first_name, emp.middle_name, emp.last_name from emp where employee_id in($ids)");
								   	$appr=[];
								   	for ($i=0; $i < count($q); $i++) { 
								   		$names = $q[$i]->first_name;
								   		if($q[$i]->middle_name){
								   			$names.=" ".$q[$i]->middle_name;
								   		}
								   		if($q[$i]->last_name){
								   			$names.=" ".$q[$i]->last_name;
								   		}
								   		if($names!="!o_ @ d"){
								   			array_push($appr,$names);
								   		}

								   	}
								   	$appr=implode(", ", $appr);
								   	// diganti dengan LIB*/

								 //   	return [$appr];
									// foreach ($get_first as $key => $value) {
									// 	if($value  == 1){
									// 	 	$user[]  =  $key;
									// 	}else{
									// 		if($value ==  2){
									// 			$user[]  =  $key;
									// 		}
									// 	}
									// }

									/**
									 *cari approver 
									 *  
									 */
									//return $user;
									// $appr = "";
									// for ($i=0; $i < count($user); $i++) { 
									// 	# code...
									// 	$second_step   = $pool[$user[$i]];
									// 	return [$second_step];
									// 	$selected = \DB::SELECT("select t1.supervisor,concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name) as employee
									// 					from  emp_supervisor t1
									// 					left join  emp t2 on t2.employee_id=t1.supervisor
									// 					where  t1.employee_id='$name' order by t1.employee_id;");
									// 	//return [$selected];
									// 	if(count($selected) > 0){
									// 		foreach ($selected as $key => $value) {
									// 			if(isset($appr[$i-1]) != $value->employee){

									// 				$appr[] = $value->employee;
									// 			}
									// 		}
									// 	}else{
									// 		$appr = "Superuser";
									// 	}
									// }
		
									// foreach ($second_step as $key => $value) {
									// 	if($value[$user.'_stat']  == 1){
									// 		foreach ($value as $keyd => $valued) {
									// 			if(is_numeric($keyd)){
									// 				$select = \DB::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_employee from emp where employee_id  = '$keyd' ")[0]->name_employee;
									// 				$select  =  $select.'  ( '. strtoupper($user).' )';
									// 			}
									// 		}
									// 	}
									// }
								}else{
									return 1;
								}	
								$now = strtotime($lock->to_); // or your date as well
								$your_date = strtotime($lock->from_);
								$datediff = $now - $your_date;

								$lock->taken_day = (round($datediff / (60 * 60 * 24)))+1;
								
								$final_data[] = [	
									'leave_type' => $lock->leave_type,
									'DateTaken' => $lock->from_.' - '.$lock->to_,
									'taken_day' => $lock->taken_day,
									'EntitlementDay' =>  /*$get_balance*/ null,//abs($lock->days - $lock->balance_leaves ),
									'RemainingDay' => /*$lock->balance_day - 1*/ null,
									'approver' => $appr,
								];
							}

						}
						
					}
						
					}else{
						return \Response::json(['header'=>['message'=>'Empty data','status'=>200],'data'=>[]],200);
					}
			}else{
					
			}
			

				if($final_data  != null){
						$query= ["value" => $final_data, "date" => $year_now ];
					}else{
						$query= ["value" => [], "date" => $date];

						return $Report->getMessage('Data not exist',$query,200,$access[3]);
					}

				return $Report->index($query,$access[3]);

		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	// public function report()
	// {
	// 	$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	// 	if($access[1] == 200){
	// 		$Report = new Report_Model;
	// 		$input = \Input::all();
	// 		$name = \Input::get('name');
	// 		$generate = \Input::get('generate');
	// 		$date_report = \Input::get('date');
	// 		$query_y = [];
	// 		if($generate == 1){
	// 			$rule = [
	// 				'generate' => 'required|integer',
	// 				'name' => 'required|integer',
	// 				'generate' => 'required|integer',
	// 				];
	// 			$validation = \Validator::make($input,$rule);

	// 			if(!isset($validator)){
	// 				// $final  = \DB::SELECT("select (concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name))as employee, leave_request.employee_id as emp_id leave_type.name as leave_type,
	// 				// 					   leave_request.leave_type as type_id , leave_request.created_at as DateTaken, count(select * from leave_request where employee_id = (select emp_id) and leave_type = (select type_id)) as taken_day,
	// 				// 					   (select sum(date_diff(to_,from_)+1) from leave_request where employee_id = (select emp_id) and leave_type =  (select type_id)) as EntitlementDay, 

	// 				// 					   from leave_request,emp,leave_type where leave_request.employee_id =  emp.employee_id and leave_request.leave_type = leave_type.id ");


	// 					/*$final[]  = \DB::SELECT("select 
	// 						leave_request.add_on,
	// 						leave_request.approval,
	// 						leave_request.approver,
	// 						leave_request.balance_day,
	// 						leave_request.created_at,
	// 						leave_request.day_off,
	// 						leave_request.dayoff_between,
	// 						leave_request.employee_id,
	// 						leave_request.from_,
	// 						leave_request.id,
	// 						leave_type.leave_type,							
	// 						leave_request.status_id,
	// 						leave_request.taken_day,
	// 						leave_request.from_,
	// 						leave_request.to_,
	// 						2 as master_type,
	// 						concat(leave_request.from_,' - ',leave_request.to_) as schedule,
	// 						leave_request.year,
	// 						att_status.status,
	// 						concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as EmployeeName
	// 						from leave_request,att_status,leave_type,emp where leave_request.leave_type =  leave_type.id and emp.employee_id  = leave_request.employee_id and att_status.status_id = leave_request.status_id and leave_request.status_id != 1")[0];  */
	// 				// $final  = \DB::SELECT("CALL view_all_leave_balance");
	// 				$final = \DB::SELECT("select 
	// 									leave_request.id,
	// 									leave_request.add_on,
	// 									leave_request.approval,
	// 									leave_request.approver,
	// 									leave_request.balance_day,
	// 									leave_request.created_at,
	// 									leave_request.day_off,
	// 									leave_request.dayoff_between,
	// 									leave_request.employee_id,
	// 									leave_request.from_,
	// 									leave_request.leave_type,
	// 									leave_type.leave_type,							
	// 									leave_request.status_id,
	// 									leave_request.taken_day,
	// 									leave_request.from_,
	// 									leave_request.to_,
	// 									2 as master_type,
	// 									concat(leave_request.from_,' - ',leave_request.to_) as schedule,
	// 									leave_request.year,
	// 									att_status.status,
	// 									concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as EmployeeName
	// 									from leave_request,att_status,leave_type,emp 
	// 									where 
	// 									leave_request.leave_type =  leave_type.id and 
	// 									emp.employee_id  = leave_request.employee_id and 
	// 									att_status.status_id = leave_request.status_id and 
	// 									leave_request.status_id = 2 and
	// 									leave_request.employee_id = '$name'");
	// 				/*return [$final];
	// 				foreach ($final as $key => $value) {
	// 					if($value->employee_id != $name){
	// 						unset($final[$key]);
	// 					}
	// 				}*/

	// 				if($final == null){
	// 					return \Response::json(['header'=>['message'=>'Empty data','status'=>500],'data'=>[]],500);
	// 				}else{
	// 					$date =  date('Y').'-12-31';
	// 					foreach ($final as $key => $value) {
	// 						$lock   =  $value;	
	// 						$idn = $value->id;
	// 						// $select = \DB::SELECT("select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as approval from leave_request,emp where id = $idn and emp.employee_id =  leave_request.approval ");							
	// 						// if(isset($select[0]->approval)){
	// 						// 	$appr = $select[0]->approval;
	// 						// }else{
	// 						// 	$appr = "";
	// 						// }

	// 						if(isset($date_report)  and  $date_report!= null){
	// 								$year_now  = $date_report;
	// 							}else{
	// 								$year_now  = date('Y');
	// 						}

	// 						//return "select * from leave_request  where employee_id = '$name' and year = '$year_now' order by id ASC limit 1";
	// 						$select =    \Db::SELECT("select * from leave_request  where employee_id = '$name' and year = '$year_now' order by id ASC limit 1");
							
	// 						if(!isset($select[0]->balance_day)  and  !isset($select[0]->taken_day)){
	// 								$query= ["value" => [], "date" => $year_now];

	// 								return $Report->getMessage('Data not exist',$query,200,$access[3]);
								
	// 							///return response()->json(['header' => ['message' => "can't found your record",  'status' => 200],'data' => []],200);
	// 						}

	// 						$get_balance  =  $select[0]->balance_day - 1 + $select[0]->taken_day;
							
	// 						if($value->approver  !=  null and $value->approver != 0){
	// 							$pool  = \Db::SELECT("select * from pool_request  where id_req =  $idn and  master_type  = 2 ")[0]->json_data;
	// 							$pool =    json_decode($pool,1);

	// 						   	$get_first = $pool['req_flow'];
	// 							foreach ($get_first as $key => $value) {
	// 								if($value  == 1){
	// 								 	$user  =  $key;
	// 								}else{
	// 									if($value ==  2){
	// 										$user  =  $key;
	// 									}
	// 								}
	// 							}

	// 							/**
	// 							 *cari approver 
	// 							 *  
	// 							 */
	// 							$appr = "";
	// 							$second_step   = $pool[$user];
								
	// 							$selected = \DB::SELECT("select t1.supervisor,concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name) as employee
	// 											from  emp_supervisor t1
	// 											left join  emp t2 on t2.employee_id=t1.supervisor
	// 											where  t1.employee_id='$name' order by t1.employee_id;");
	// 							//return [$selected];
	// 							if(count($selected) > 0){
	// 								foreach ($selected as $key => $value) {
	// 									if(strlen($appr) < 2){
	// 										$appr = $value->employee;
	// 									}else{
	// 										$appr .= ", ".$value->employee;
	// 									}
	// 								}
	// 							}else{
	// 								$appr = "Superuser";
	// 							}
	// 							// foreach ($second_step as $key => $value) {
	// 							// 	if($value[$user.'_stat']  == 1){
	// 							// 		foreach ($value as $keyd => $valued) {
	// 							// 			if(is_numeric($keyd)){
	// 							// 				$select = \DB::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_employee from emp where employee_id  = '$keyd' ")[0]->name_employee;
	// 							// 				$select  =  $select.'  ( '. strtoupper($user).' )';
	// 							// 			}
	// 							// 		}
	// 							// 	}
	// 							// }
	// 						}else{
	// 							return 1;
	// 						}	
							
	// 						$final_data[] = [	
	// 							'leave_type' => $lock->leave_type,
	// 							'DateTaken' => $lock->from_.' - '.$lock->to_,
	// 							'taken_day' => $lock->taken_day,
	// 							'EntitlementDay' =>  /*$get_balance*/ null,//abs($lock->days - $lock->balance_leaves ),
	// 							'RemainingDay' => /*$lock->balance_day - 1*/ null,
	// 							'approver' => $appr,
	// 						];
	// 					}
						
	// 				}
						
	// 				}else{
	// 					return \Response::json(['header'=>['message'=>'Empty data','status'=>500],'data'=>[]],500);
	// 				}
	// 		}else{
					
	// 		}
			

	// 			if($final_data  != null){
	// 					$query= ["value" => $final_data, "date" => $year_now ];
	// 				}else{
	// 					$query= ["value" => [], "date" => $date];

	// 					return $Report->getMessage('Data not exist',$query,200,$access[3]);
	// 				}

	// 			return $Report->index($query,$access[3]);

	// 	}else{
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 	}
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	// }




}
