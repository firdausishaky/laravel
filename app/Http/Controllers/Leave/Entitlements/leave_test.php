<?
public  function request_leave(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$request = new leaverequest_Model;
			$json = \Input::get('data');
			
			if($json == null){
				$json = \Input::all();
				$type = $json['LeaveType'];
				$leave_type = \DB::select("select leave_type,id from leave_type where leave_type='$type' ");
				$type_id = $leave_type[0]->id;

					$validation = \Validator::make(

							['name' => $json['name']],
							['LeaveType'	=> $json['LeaveType']],
							['From' => $json['From']],
							['To' 	=> $json['To']],
							['DayOffBetween' => $json['DayOffBetween']],
							["aggred"  => $json['aggred']],

							['name' => 'required|integer'],
							['LeaveType'	=> "required|string"],
							['From' => 'required|date'],
							['To' 	=> 'required|date'],
							['DayOffBetween' => 'required|integer'],
							["aggred"  => "required"]
							);
					if($validation->fails()){
						$check = $validation->errors()->all();
						return $request->getMessage("please fill required from before submit data",$check,500,$access[3]);
					}
					if($aggred == "1"){
						$db = \DB::SELECT("CALL insert_leave_request('$json[name]',$type_id,'$json[From]','$json[To]',$json[DayOffBetween],'$json[comment]','-','-')");
						return  $request->getMessage("success",null, 200, $access[3]);
					}else{
						return  $request->getMessage("failed to insert data, please fill form aggree before submit data",null, 200, $access[3]);
					}
				}else{
					$json = \Input::get("data");
					$file = \Input::file('file');
					$data = json_decode($json_string,1);

					$type =  $data['data']['LeaveType'];
					$leave_type = \DB::select("select leave_type,id from leave_type where leave_type='$type' ");
					$type_id = $leave_type[0]->id;
					$name =  $data['data']['name'];
					$from =  $data['data']['From'];
					$to = $data['data']['To'];
					$dob = $data['data']['DayOffBetween'];
					$aggree = $data['data']['aggred'];
					$comment = $data['data']['comment'];

					$validation = \Validator::make(
						['name' => $name],
						['LeaveType'	=> $type],
						['From' => $from],
						['To' 	=> $to],
						['DayOffBetween' => $dob],
						["aggred"  => $aggree],


						['name' => 'required|integer'],
						['LeaveType'	=> "required|string"],
						['From' => 'required|date'],
						['To' 	=> 'required|date'],
						['DayOffBetween' => 'required|integer'],
						["aggred"  => "required"]
						);

					if($validation->fails()){
						$check = $validation->errors()->all();
						return  $request->getMessage($check,[], 500, $access[3]);
					}else{
						if($type  == "Vacation Leave"){
							$ado = $json['data']['IncludeAdo'];
							
							if($aggree == 1 ){
								if($ado == "Yes"){
									$date1 =  $data['data']['date1'];
									$date2 =  $data['data']['date2'];
									$date3 =  $data['data']['date3'];
									$date4 =  $data['data']['date4'];
									$ado1 =   $data['data']['ado1'];
									$ado2 =   $data['data']['ado2'];
									$ado3 =   $data['data']['ado3'];
									$ado4 =   $data['data']['ado4'];
									$nextday = $data['data']['nextday'];
									$ado = [
											["date" => $date1, "ado_name" => $ado1],
											["date" => $date2, "ado_name" => $ado2],
										   	["date" => $date3, "ado_name" => $ado3],
										          ["date" => $date4, "ado_name" => $ado4]
										];

									$validation = \Validator::make(
										['date1' => $date1],
										['date2' => $date2],
										['date3' => $date3],
										['date4' => $date4],
										['ado1' => $ado1],
										['ado2' => $ado2],
										['ado3' => $ado3],
										['ado4' => $date4],

										['date1' => 'required|date'],
										['date2' => 'required|date'],
										['date3' => 'required|date'],
										['date4' => 'required|date'],
										['ado1' => 'required|integer'],
										['ado2' => 'required|integer'],
										['ado3' => 'required|integer'],
										['ado4' => 'required|integer']

										);

									if($validation->fails()){
										$check = $validation->errors()->all();
										return  $request->getMessage($check,[], 500, $access[3]);
									}else{
										$image = \Input::file('file');
										if (isset($image)){
											$imageName = $image->getClientOriginalName();
											$ext = \Input::file('file')->getClientOriginalExtension();
											$path = "/hrms_upload/leave_request";
											$size = $image->getSize();
											$request->check_image($ext,$size,$image,$path,$access[3]);
										}else{
											$imageName =  "-";
											$path = "-";
										}

										if(!isset($comment)){
											$comment = "-";
										}
										$count = count($ado)-1;
										for($i = 0; $i <=$count; $i++){
											if($i == 0){
												$name_ado = "1st";
											}elseif($i == 1){
												$name_ado = "2nd";
											}elseif($i == 2){
												$name_ado = "3rd";
											}else{
												$name_ado = "4th";
											}

											$date_ado = $ado[$i]['date'];
											$new_ado = $ado[$i]['ado_name'];
											$db = \DB::select("call insert_leave_ado($type_id,'$name','$name_ado','$date_ado',$new_ado)");
										}
										$db = \DB::select("call insert_leave_ado($type_id,'$name','$name_ado','$date_ado',$new_ado)");
										$db = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob,'$comment','$path','$imageName')");
									}
								}else{
									$image = \Input::file('file');
									if (isset($image)){
										$imageName = $image->getClientOriginalName();
										$ext = \Input::file('file')->getClientOriginalExtension();
										$path = "/hrms_upload/leave_request";
										$size = $image->getSize();
										$request->check_image($ext,$size,$image,$path,$access[3]);
									}else{
										$imageName =  "-";
										$path = "-";
									}

									if(!isset($comment)){
										$comment = "-";
									}

									$count = count($ado)-1;
									for($i = 0; $i <=$count; $i++){
										if($i == 0){
											$name_ado = "1st";
										}elseif($i == 1){
											$name_ado = "2nd";
										}elseif($i == 2){
											$name_ado = "3rd";
										}else{
											$name_ado = "4th";
										}

										$date_ado = $ado[$i]['date'];
										$new_ado = $ado[$i]['ado_name'];
										$db = \DB::select("call insert_leave_ado($type_id,'$name','$name_ado','$date_ado',$new_ado)");
									}
									$db = \DB::select("call insert_leave_ado($type_id,'$name','$name_ado','$date_ado',$new_ado)");
									$db = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob,'$comment','$path','$imageName')");
								}
							}

						}elseif($type  == "Accumulation Day Off"){
							$date1 =  $data['data']['date1'];
							$date2 =  $data['data']['date2'];
							$date3 =  $data['data']['date3'];
							$date4 =  $data['data']['date4'];
							$ado1 =   $data['data']['ado1'];
							$ado2 =   $data['data']['ado2'];
							$ado3 =   $data['data']['ado3'];
							$ado4 =   $data['data']['ado4'];
							$nextday = $data['data']['nextday'];
							$ado = [["date" => $date1, "ado_name" => $ado1],
									      ["date" => $date2, "ado_name" => $ado2],
									      ["date" => $date3, "ado_name" => $ado3],
									      ["date" => $date4, "ado_name" => $ado4]
								];

							$validation = \Validator::make(
								['date1' => $date1],
								['date2' => $date2],
								['date3' => $date3],
								['date4' => $date4],
								['ado1' => $ado1],
								['ado2' => $ado2],
								['ado3' => $ado3],
								['ado4' => $date4],

								['date1' => 'required|date'],
								['date2' => 'required|date'],
								['date3' => 'required|date'],
								['date4' => 'required|date'],
								['ado1' => 'required|integer'],
								['ado2' => 'required|integer'],
								['ado3' => 'required|integer'],
								['ado4' => 'required|integer']

								);

								if($validation->fails()){
									$check = $validation->errors()->all();
									return  $request->getMessage($check,[], 500, $access[3]);
								}else{
									$image = \Input::file('file');
									if (isset($image)){
										$imageName = $image->getClientOriginalName();
										$ext = \Input::file('file')->getClientOriginalExtension();
										$path = "/hrms_upload/leave_request";
										$size = $image->getSize();
										$request->check_image($ext,$size,$image,$path,$access[3]);
									}else{
										$imageName =  "-";
										$path = "-";
									}

									if(!isset($comment)){
										$comment = "-";
									}
									$count = count($ado)-1;
									for($i = 0; $i <=$count; $i++){
										if($i == 0){
											$name_ado = "1st";
										}elseif($i == 1){
											$name_ado = "2nd";
										}elseif($i == 2){
											$name_ado = "3rd";
										}else{
											$name_ado = "4th";
										}

										$date_ado = $ado[$i]['date'];
										$new_ado = $ado[$i]['ado_name'];
										$db = \DB::select("call insert_leave_ado($type_id,'$name','$name_ado','$date_ado',$new_ado)");
									}
										$db = \DB::select("call insert_leave_ado($type_id,'$name','$name_ado','$date_ado',$new_ado)");
										$db = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob,'$comment','$path','$imageName')");
								}
						 }else{
							$image = \Input::file('file');
							if (isset($image)){
								$imageName = $image->getClientOriginalName();
								$ext = \Input::file('file')->getClientOriginalExtension();
								$path = "/hrms_upload/leave_request";
								$size = $image->getSize();
								$request->check_image($ext,$size,$image,$path,$access[3]);
							}else{
								$imageName =  "-";
								$path = "-";
							}

							if(!isset($comment)){
								$comment = "-";
							}
							$count = count($ado)-1;
							for($i = 0; $i <=$count; $i++){
								if($i == 0){
									$name_ado = "1st";
								}elseif($i == 1){
									$name_ado = "2nd";
								}elseif($i == 2){
									$name_ado = "3rd";
								}else{
									$name_ado = "4th";
								}

								$date_ado = $ado[$i]['date'];
								$new_ado = $ado[$i]['ado_name'];
								$db = \DB::select("call insert_leave_ado($type_id,'$name','$name_ado','$date_ado',$new_ado)");
							}
							$db = \DB::select("call insert_leave_ado($type_id,'$name','$name_ado','$date_ado',$new_ado)");
							$db = \DB::SELECT("CALL insert_leave_request('$name',$type_id,'$from','$to',$dob,'$comment','$path','$imageName')");
						}
				
			else{
			{ $message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
			}