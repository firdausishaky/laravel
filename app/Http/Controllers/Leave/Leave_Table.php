<?php namespace Larasite\Http\Controllers\Leave;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Leave\leave_table\leaveTable_Model;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;

class Leave_Table extends Controller {
	protected $form = "";

	public function __construct(){

		//give access permission
		$key = \Input::get('key');
		$keys= base64_decode($key);
		$test = explode('-',$keys);
		$data = $test[1];

		$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
		$db_data =  $db[0]->local_it;

			if($db_data == 1){
				return $this->form = "55";
			}else{
				return $this->form = "54";
			}
	}


	public function insert_vacation_leave(){
	 	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$message = "Unauthorized"; $data = []; $status =  200;
			}else{
				 	$leave = new leaveTable_Model;
				 	$applied = \Input::get('Appliedfor');

				 	//check expat or local
				 	if($applied == "Expat"){
				 		$applied_ex = 1;
				 	}
				 	if($applied == "Local" ){
				 		$applied_ex = 2;
				 	}
				 	$id_department = \Input::get('Department');
				 	$ent_start = \Input::get('StartFrom');
				 	$ent_day = \Input::get('Entitlementdays');
				 	$subvacation = \Input::json('subvacation');
				 	$compare = $this->compare($subvacation);
				 	if($compare != null){
				 		 return $leave->getMessage("kindly check same year in row in when save data, same year recorded is ".$compare[0] ,500,null);
				 	}

				 	$count_subvacation = count($subvacation)-1;
				 	$off_x = \Input::get('off');

				 	//valdiation check
				 	if($applied == null){
				 	          return $leave->getMessage(" required field can't be null ",500,null);
				 	}if($id_department == null){
				 		return $leave->getMessage("required field can't be null ",500,null);
				 	}if($ent_day == null){
				 		return $leave->getMessage(" required field can't be null ",500,null);
				 	}if($ent_day == null){
				 		return $leave->getMessage(" required field can't be null ",500,null);
				 	}

				 	//check department and check vacation with same department  not  passed to insert record
					$department = \DB::SELECT("select name from department where id=$id_department and id <> 1");

					$check_department = \DB::SELECT("select * from leave_vacation  where department=$id_department and applied_for in (1,2) and department=$id_department and applied_for = $applied_ex");

					if($check_department != null){
						return $leave->getMessage("failed ".$department[0]->name." data already exist",500,null);
					}else{
						$insert = \DB::SELECT("CALL insert_leave_vacation($applied_ex,$id_department,'$ent_start','$ent_day',2)");
					}

					//insert sub data of vacation
					if(isset($subvacation)  && $subvacation != null ){
						$idx = \DB::SELECT("select id from leave_vacation order by id desc limit 1  ");
						$id_test = $idx[0]->id;
					 	for($i = 0; $i <= $count_subvacation; $i++){
						 	$year = $subvacation[$i]['year'];
						 	$days = $subvacation[$i]['days'];
						 	$off = $subvacation[$i]['offday_oncall'];
						 	if(isset($off_x)){
						 		$off_x = $off_x;
						 	}else{
						 		$off_x = 0;
						 	}
						 	$check_leave  = \DB::SELECT("select * from 	leave_vacation_detail where  vacation_id = $id_test and year = $year");
						 	if($check_leave == null || $check_leave = []){
						 		\DB::SELECT("CALL insert_leave_vacation_detail($id_test,$year,$days,$off_x,$off)");	
						 	}else{
						 		$message = 'insert data stopped there is duplicate year';
						 	}
						 	
						 }

						 $data_test = \DB::SELECT("select id from leave_vacation_detail where vacation_id=$id_test");
						 $count_test = count($data_test)-1;

						 for($i = 0; $i <= $count_test; $i++){
						 	$id_test_ex =  $data_test[$i]->id;
							\DB::SELECT("CALL Insert_leave_offday_oncall($id_test,$id_test_ex)");
						 	\DB::SELECT("CALL insert_leave_vacation_test($id_test,$id_test_ex)");
						 }


					}else{
						$subvacation = [];
					}

					// give rreturn back data
					$db_data = \DB::SELECT("select * from leave_vacation  order by id  desc limit 1 ");
					$applied = $this->check_employee($db_data[0]->applied_for);

					$data[] = ["id" => $db_data[0]->id,
						    "Appliedfor" =>"Applied For : ".$applied ,
						    "Department" =>"Department : ".$department[0]->name,
						    "StartFrom" =>"StartFrom : ".$db_data[0]->entitlement_start,
						    "Entitlementdays" =>"Entitlementdays : ".$db_data[0]->entitlement_day,
						     "subvacation" =>$subvacation
						 ];
					if(!isset($message) || $message ==  null){
						$message = "sucess insert data";
					}	 

					 return $leave->getMessage($message,200,$data);
			}
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}

	}

	public function destroy_vacation($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
			if($access[1] == 200){
				if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$message = "Unauthorized"; $data = []; $status =  200;
				}else{

					 	$leave = new leaveTable_Model;
						if($id != null){
							$explode_id = explode(",",$id);
							$count = count($explode_id)-1;

							for($i = 0; $i <= $count; $i++){
								$data = $explode_id[$i];
								$db = \DB::SELECT("CALL delete_leave_vacation($data)");
							}
							return $leave->getMessage("success",200,[]);
						}
					}
			}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}
	 }

	public function compare($data){
	 	$arr = [];
	 	$compare = [];
	 	foreach ($data as $key => $value) {
	 		$arr[] = ( isset($value['year']) ? $value['year']  : $value['relation_to_deceased'] );
	 	}
	 	$get = array_count_values($arr);
		
		foreach ($get as $key => $value) {
			if($value > 1){
				$compare[] = $key;
			}
		}
	 	return $compare;
	}

	public function update_vacation($id){
	 	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
						$message = "Unauthorized"; $data = []; $status =  200;
			}else{
				$leave = new leaveTable_Model;
			 	$applied = \Input::get('Appliedfor');

			 	if($applied == "Expat"){
			 		$applied_ex = 1;
			 	}
			 	if($applied == "Local" ){
			 		$applied_ex = 2;
			 	}

			 	$id_department = \Input::get('Department');
			 	$ent_start = \Input::get('StartFrom');
			 	$ent_day = \Input::get('Entitlementdays');
			 	$subvacation = \Input::json('subvacation');
			 	$count_subvacation = count($subvacation)-1;
			 	$compare = $this->compare($subvacation);
			 	
			 	if($compare != null){
			 		 return $leave->getMessage("kindly check same year in row in when save data, same year recorded is ".$compare[0] ,500,null);
			 	}

			 	if(ctype_digit($id_department)){
			 		$data_insert = $id_department;
			 	}else{
			 		$department_id = \DB::SELECT("select id from department where name='$id_department' ");
					$data_insert  = $department_id[0]->id;
			 	}

			 	if($leave->check_department($data_insert,$applied_ex,$id,"leave_vacation") == "no"){
			 		  return $leave->getMessage("can't update applied for". $applied ."and department already exist",500,null);
			 	}

			 	$department = \DB::SELECT("select name,id from department where id=$data_insert  ");

				$insert = \DB::SELECT("CALL update_leave_vacation($id,$applied_ex,$data_insert,'$ent_start','$ent_day')");
			 	$delete_all = \DB::SELECT("delete from leave_vacation_detail where vacation_id=$id");

			 	if(isset($subvacation)  && $subvacation != null ){
				 	for($i = 0; $i <= $count_subvacation; $i++){
				 		//$idx = \DB::SELECT("select id from leave_vacation order by id where id desc limit 1  ");
						$id_test = $id;
					 	$year = $subvacation[$i]['year'];
					 	$days = $subvacation[$i]['days'];
					 	$off = $subvacation[$i]['offday_oncall'];
					 	if(isset($off_x)){
					 		$off_x = $off_x;
					 	}else{
					 		$off_x = 0;
					 	}
					 
					 	\DB::SELECT("CALL insert_leave_vacation_detail($id,$year,$days,$off_x,$off)");
					 }
					 $data_test = \DB::SELECT("select id from leave_vacation_detail where vacation_id=$id");
					 $count_test = count($data_test)-1;

					 for($i = 0; $i < $count_test; $i++){
					 	$id_test_ex =  $data_test[$i]->id;
					 	\DB::SELECT("CALL insert_leave_vacation_test($id,$id_test_ex)");
					 }
				}

				$db_data = \DB::SELECT("select * from leave_vacation where id =  $id  order by id  desc limit 1 ");

				if(is_numeric($data_insert)){
					$department_id = \DB::SELECT("select name from department where id='$data_insert' ");
					if(!isset($department_id[0]->name)){
						$data_insert = null;
					}else{
						$data_insert  = $department_id[0]->name;
					}
				}

				$data[] = ["id" => $id,
					    "Appliedfor" =>"Applied For : ".$applied ,
					    "Department" =>"Department : ".$data_insert,
					    "StartFrom" =>"StartFrom : ".$db_data[0]->entitlement_start,
					    "Entitlementdays" =>"Entitlementdays : ".$db_data[0]->entitlement_day,
					    "subvacation" => $subvacation
					 ];

				 return $leave->index($data);
			}
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}
	}

	// public function insert_sick_leave(){
	//  	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	// 	if($access[1] == 200){
	// 			$leave = new leaveTable_Model;
	// 		 	$applied = \Input::get('Appliedfor');

	// 		 	if($applied == "Expat"){
	// 		 		$applied = 1;
	// 		 	}
	// 		 	if($applied == "Local" ){
	// 		 		$applied = 2;
	// 		 	}
	// 		 	$id_department = \Input::get('Department');
	// 		 	$ent_start = \Input::get('StartFrom');
	// 		 	$ent_day = \Input::get('Entitlementdays');
	// 		 	$subSick = \Input::json('subsick');
	// 		 	$compare = $this->compare($subSick);
	// 		 	if($compare != null){
	// 		 		 return $leave->getMessage("kindly check same year in row in when save data, same year recorded is ".$compare[0] ,500,null);
	// 		 	}
	// 		 	$count_subSick = count($subSick)-1;
	// 		 	$department = \DB::SELECT("select * from department where name='$id_department' ");
	// 		 	$department_ex = $department[0]->id;
	// 		 	$check_department = \DB::SELECT("select * from leave_sick  where department=$department_ex and applied_for in (1,2) and department=$department_ex and applied_for = $applied");

	// 			if($check_department != null){
	// 				return $leave->getMessage("failed ".$department[0]->name." data already exist",500,null);
	// 			}

	// 		 	if($applied == null){
	// 		 	          return $leave->getMessage(" required field can't be null ",500,null);
	// 		 	}if($id_department == null){
	// 		 		return $leave->getMessage("required field can't be null ",500,null);
	// 		 	}if($ent_day == null){
	// 		 		return $leave->getMessage(" required field can't be null ",500,null);
	// 		 	}if($ent_day == null){
	// 		 		return $leave->getMessage(" required field can't be null ",500,null);
	// 		 	}

	// 			$insert = \DB::SELECT("CALL insert_leave_sick($applied,$department_ex,'$ent_start','$ent_day')");

	// 			if(isset($subSick) && $subSick != null){
	// 				$id = \DB::SELECT("select id from leave_sick order by id desc limit 1 ");
	// 				$id_test = $id[0]->id;

	// 				for($i = 0; $i <= $count_subSick; $i++){
	// 				 	$year = $subSick[$i]['year'];
	// 				 	$days = $subSick[$i]['days'];
	// 					\DB::SELECT("CALL insert_leave_sick_detail($id_test,$year,$days)");
	// 				}

	// 				$data_test = \DB::SELECT("select * from leave_sick_detail where leave_sick_id=$id_test");
	// 				$count_test = count($data_test)-1;

	// 				for($i = 0; $i < $count_test; $i++){
	// 				 	$id_test_ex =  $data_test[$i]->id;
	// 				 	\DB::SELECT("CALL Insert_Leave_Sick_test($id_test,$id_test_ex)");
	// 				 }

	// 			}else{
	// 				$subSick = [];
	// 			}

	// 			$db_data = \DB::SELECT("select * from leave_sick  order by id  desc limit 1 ");
	// 			$applied = $this->check_employee($db_data[0]->applied_for);

	// 			$data[] = [ "id" => $db_data[0]->id,
	// 				    	"Appliedfor" =>"Applied For : ".$applied ,
	// 				    	"Department" =>"Department : ".$department[0]->name,
	// 				    	"StartFrom" =>"StartFrom : ".$db_data[0]->entitlement_start,
	// 				    	"Entitlementdays" =>"Entitlementdays : ".$db_data[0]->entitlement_day,
	// 				     	"subsick" => $subSick
	// 				 ];
	// 			 return $leave->index($data);
	// 	}else{ 
	// 	$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
	// 	}
	//  }

	//  public function update_sick_leave($id){
	//  	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	// 	if($access[1] == 200){
	// 		 	if($id != null){
	// 			 	$leave = new leaveTable_Model;
	// 			 	$applied = \Input::get('Appliedfor');

	// 			 	if($applied == "Expat"){
	// 			 		$applied_ex = 1;
	// 			 	}
	// 			 	if($applied == "Local" ){
	// 			 		$applied_ex = 2;
	// 			 	}

	// 			 	$id_department = \Input::get('Department');

	// 			 	if(ctype_digit($id_department)){
	// 		 			$data_insert = $id_department;
	// 		 		}else{
	// 		 			$department_id = \DB::SELECT("select id from department where name='$id_department' ");
	// 					$data_insert  = $department_id[0]->id;
	// 		 		}

	// 		 		//return $data_insert;
	// 			 	$ent_start = \Input::get('StartFrom');
	// 			 	$ent_day = \Input::get('Entitlementdays');
	// 			 	$subSick = \Input::json('subsick');
	// 			 	$compare = $this->compare($subSick);
	// 			 	if($compare != null){
	// 			 		 return $leave->getMessage("kindly check same year in row in when save data, same year recorded is ".$compare[0] ,500,null);
	// 			 	}
	// 			 	$count_subSick = count($subSick)-1;

	// 				if($leave->check_department($data_insert,$applied_ex,$id,"leave_sick") == "no"){
	// 		 			  return $leave->getMessage("can't update applied for". $applied ."and department already exist",500,null);
	// 		 		}

	// 				$insert = \DB::SELECT("CALL update_leave_sick($id,$applied_ex,$data_insert,'$ent_start','$ent_day')");
	// 			 	$delete_all = \DB::SELECT("delete  from leave_sick_detail where leave_sick_id=$id");

	// 			 	if(isset($subSick) && $subSick != null){
	// 					$id_test = $id;

	// 					for($i = 0; $i <= $count_subSick; $i++){
	// 					 	$year = $subSick[$i]['year'];
	// 					 	$days = $subSick[$i]['days'];
	// 						\DB::SELECT("CALL insert_leave_sick_detail($id_test,$year,$days)");
	// 					}

	// 					 $data_test = \DB::SELECT("select id from leave_sick_detail where leave_sick_id=$id_test");
	// 					 $count_test = count($data_test)-1;

	// 					 for($i = 0; $i < $count_test; $i++){
	// 					 	$id_test_ex =  $data_test[$i]->id;
	// 					 	\DB::SELECT("CALL Insert_Leave_Sick_test($id_test,$id_test_ex)");
	// 					 }

	// 				}

	// 				$db_data = \DB::SELECT("select * from leave_sick where id=$id order by id  desc limit 1 ");

	// 				if(is_numeric($data_insert)){
	// 					$department_id = \DB::SELECT("select name from department where id='$data_insert' ");
	// 					$data_insert  = $department_id[0]->name;
	// 				}

	// 				$result[] = ["id" => $id,
	// 					       "Appliedfor" =>"Applied For : ".$applied ,
	// 					       "Department" =>"Department : ".$data_insert,
	// 					       "StartFrom" =>"StartFrom : ".$db_data[0]->entitlement_start,
	// 				   	       "Entitlementdays" => "Entitlementdays : ".$db_data[0]->entitlement_day,
	// 					       "subsick" => $subSick
	// 					      ];

	// 				 return $leave->index($result);
	// 			}else{ return $leave->getMessage("id not found", 500, null);}
	// 	}else{ 
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
	// 	}
	//  }

 // 	public function destroy_sick($id){
 // 		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	// 	if($access[1] == 200){
	//  		$leave = new leaveTable_Model;
	// 		if($id != null){
	// 			$explode_id = explode(",",$id);
	// 			$count = count($explode_id)-1;
	// 			$i = 0;
	// 			for($i; $i <= $count; $i++){
	// 				$data = $explode_id[$i];
	// 				$db = \DB::SELECT("CALL Delete_Leave_Sick($data)");
	// 			}
	// 				return $leave->getMessage("success",200,[]);

	// 		 }
	// 	}else{ 
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
	// 	}
	// }

	public function  insert_bereavement(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
				$leave = new leaveTable_Model;
			 	$applied = \Input::get('Appliedfor');

			 	if($applied == "Expat"){
			 		$applied_ex = 1;
			 	}
			 	if($applied == "Local" ){
			 		$applied_ex= 2;
			 	}

			 	
			 	$id_department = \Input::get('Department');
			 	$ent_start = \Input::get('StartFrom');
			 	$ent_day = \Input::get('Entitlementdays');
			 	$subBereavement = \Input::json('subbereavement');
			 	$count_subBereavement = count($subBereavement)-1;
			 	$compare = $this->compare($subBereavement);
				if($compare != null){
				 	return $leave->getMessage("kindly check same realtion to deceased in row  when save data, same realtion to deceased recorded is ".$compare[0] ,500,null);
				}
			 	if(ctype_digit($id_department)){
			 		$data_insert = $id_department;
			 	}else{
			 		$department_id = \DB::SELECT("select id,name from department where name='$id_department' ");
					$data_insert  = $department_id[0]->id;
			 	}

			 	$department = \DB::SELECT("select id,name from department where id=$data_insert ");

				$check_department = \DB::SELECT("select * from leave_bereavement  where department=$data_insert and applied_for in (1,2) and department=$data_insert and applied_for = $applied_ex");
				//return  $check_department;
				if($check_department != null){
					return $leave->getMessage("failed ".$department[0]->name." data already exist",500,null);
				}

			 	if($applied == null){
			 	          return $leave->getMessage(" required field can't be null ",500,null);
			 	}if($id_department == null){
			 		return $leave->getMessage("required field can't be null ",500,null);
			 	}if($ent_day == null){
			 		return $leave->getMessage(" required field can't be null ",500,null);
			 	}if($ent_day == null){
			 		return $leave->getMessage(" required field can't be null ",500,null);
			 	}

				$insert = \DB::SELECT("CALL insert_leave_bereavement($applied_ex,$data_insert,'$ent_start','$ent_day')");

				if(isset($insert)){
					$idx = \DB::SELECT("select * from leave_bereavement order by id desc limit 1  ");
					$id_test = $idx[0]->id;
				}

			 	for($i = 0; $i <= $count_subBereavement; $i++){
				 	$relation = $subBereavement[$i]['relation_to_deceased'];
					$day = $subBereavement[$i]['days'];
					$day_with = $subBereavement[$i]['days_with_pay'];

					\DB::SELECT("CALL  insert_leave_bereavement_detail($id_test,'$relation',$day,$day_with)");
				}


				$data[] = ["id" => $idx[0]->id,
					    "Appliedfor" =>"Applied For : ".$applied ,
					    "Department" =>"Department : ".$id_department ,
					    "StartFrom" => "StartFrom : ".$idx[0]->entitlement_start,
					    "Entitlementdays" => "Entitlementdays : ".$idx[0]->entitlement_day,
					     "subbereavement" =>$subBereavement
					 ];
				 return $leave->index($data);
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}
	}

	public function update_bereavement($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
				if($id != null){
					$leave = new leaveTable_Model;
				 	$applied = \Input::get('Appliedfor');

				 	if($applied == "Expat"){
				 		$applied_ex = 1;
				 	}
				 	if($applied == "Local" ){
				 		$applied_ex = 2;
				 	}

					$id_department = \Input::get('Department');
				 	$ent_start = \Input::get('StartFrom');
				 	$ent_day = \Input::get('Entitlementdays');
				 	$subBereavement = \Input::json('subbereavement');
				 	$compare = $this->compare($subBereavement);
					if($compare != null){
					 	return $leave->getMessage("kindly check same realtion to deceased in row  when save data, same realtion to deceased recorded is ".$compare[0] ,500,null);
					}
				 	$count_subBereavement = count($subBereavement)-1;

				 	if($applied == null){
				 	          return $leave->getMessage(" required field can't be null ",500,null);
				 	}if($id_department == null){
				 		return $leave->getMessage("required field can't be null ",500,null);
				 	}if($ent_day == null){
				 		return $leave->getMessage(" required field can't be null ",500,null);
				 	}if($ent_day == null){
				 		return $leave->getMessage(" required field can't be null ",500,null);
				 	}

				 	if(ctype_digit($id_department)){
				 		$data_insert = $id_department;
				 	}else{
				 		$department_id = \DB::SELECT("select id,name from department where name='$id_department' ");
						$data_insert  = $department_id[0]->id;
				 	}

				 	$department = \DB::SELECT("select id,name from department where id=$data_insert ");

						$insert = \DB::SELECT("CALL Update_Leave_Bereavement($id,$applied_ex,$data_insert,'$ent_start','$ent_day')");
					 	$delete_all = \DB::SELECT("delete from leave_bereavement_detail where bereavement_id=$id");

				 	if(isset($insert) and  isset($delete_all)){
					 	for($i = 0; $i <= $count_subBereavement; $i++){
						$relation = $subBereavement[$i]['relation_to_deceased'];

						$day = $subBereavement[$i]['days'];
						$day_with = $subBereavement[$i]['days_with_pay'];

							\DB::SELECT("CALL  insert_leave_bereavement_detail($id,'$relation',$day,$day_with)");
						}
					}
					$db_data = \DB::SELECT("select * from leave_bereavement order by id  desc limit 1 ");

					//$department = \DB::SELECT("select name from department where id=$id_department ");

					$data[] = ["id" => $id,
						    "Appliedfor" =>"Applied For : ".$applied ,
						    "Department" =>"Department : ".$department_id[0]->name,
						    "StartFrom" => "StartFrom : ".$db_data[0]->entitlement_start,
						    "Entitlementdays" => "Entitlementdays : ".$db_data[0]->entitlement_day,
						     "subbereavement" => $subBereavement
						 ];

					 return $leave->index($data);
				}else{ return $leave->getMessage("id not found", 500, null);}
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}
	}

	public function delete_bereavement($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$leave = new leaveTable_Model;
			if($id != null){
				$explode_id = explode(",",$id);
				$count = count($explode_id)-1;
				$i = 0;
				for($i; $i <= $count; $i++){
					$data = $explode_id[$i];
					$db = \DB::SELECT("CALL Delete_leave_bereavement($data)");
				}
					return $leave->getMessage("success",200,null);

			}
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}
	}

	public function  insert_enchanced(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
				$leave = new leaveTable_Model;
				$input = \Input::json('subenhanced');

				$count = count($input)-1;
					for($i = 0; $i <= $count; $i++){
						$jobs = $input[$i]['title'];
						$days = $input[$i]['entitled_enhanced_days_leave'];
						if(ctype_digit($days)){
							return $leave->getMessage("days format incorrect", 500, null);
						}
						$data = \DB::SELECT("select id from job where title='$jobs' ");
						$job = $data[0]->id;
						$checkSameData = \DB::SELECT("select * from leave_enhance where job_tittle = $job");
						if($checkSameData != null){
							$dataz = \DB::SELECT('Call View_leave_enhanced');
							if($i != 0){
								$inline = $i+1;
								$message = "Success add several data , but data inline ". $inline ." and job title". $jobs ." already exist"; 
							}else{
								$message = "Error add data already exist in database for job title ".$jobs;
							}
							return $leave->getMessage($message,500,$dataz);
						}else{
							$db = \DB::SELECT("call insert_leave_enhance($job,$days)");	
						}
						
					}
					$dataz = \DB::SELECT('Call View_leave_enhanced');
					return $leave->getMessage("success",200,$dataz);
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}

	}

	public function update_enchanced(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
				$leave = new leaveTable_Model;
				$input = \Input::json('subenhanced');

				$count = count($input)-1;

				for($i = 0; $i <= $count; $i++){
					$job = $input[$i]['title'];
					$job_ex = \DB::SELECT("select id from job where title='$job' ");
					$job_ext = $job_ex[0]->id;
					$days = $input[$i]['entitled_enhanced_days_leave'];
					$ids = $input[$i]['id'];
					// if(!ctype_digit($days)){
					// 	return $leave->getMessage("days format incorrect", 500, null);
					// }
					$db = \DB::SELECT("call update_leave_enhance($job_ext,$days,$ids)");
				}
				$dataz = \DB::SELECT('Call View_leave_enhanced');
				return $leave->getMessage("success",200,$dataz);
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}

	}

	public function delete_enchanced($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$leave = new leaveTable_Model;
			if($id != null){
				$explode_id = explode(",",$id);
				$count = count($explode_id)-1;
				$i = 0;
				for($i; $i <= $count; $i++){
					$data = $explode_id[$i];
					$db = \DB::SELECT("CALL Delete_leave_enhanced($data)");
				}
					return $leave->getMessage("success",200,null);

			 }
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}
	}

	public function insert_sick_leave(){
	 	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
				$leave = new leaveTable_Model;
			 	$applied = \Input::get('Appliedfor');

			 	if($applied == "Expat"){
			 		$applied = 1;
			 	}
			 	else if($applied == "Local" ){
			 		$applied = 2;
			 	}else{
			 		if(count($applied) == 0 || $applied == null){
				 		$for = \Input::get('form_for');
				 		if($for == "2"){
				 			$applied = 2;		
				 		}else if($for == "1"){
				 			$applied = 1;		
				 		}else{
				 			return $leave->getMessage("failed, please checklist for expat or local",500,null);
				 		}
				 	}
			 	}
			 	$id_department = \Input::get('Department');
			 	$ent_start = \Input::get('StartFrom');
			 	$ent_day = \Input::get('Entitlementdays');
			 	
			 	$form_leave_type = \Input::get('form_leave_type');
			 	if(isset($form_leave_type) && $form_leave_type == 'Sick Leave'){
			 		$tables = 'leave_sick';
			 		$sub_nm = 'subsick';
			 		$subSick = \Input::json($sub_nm);
			 	}else if(isset($form_leave_type) && $form_leave_type == 'Maternity Leave'){
			 		$tables = 'leave_maternity';
			 		$sub_nm = 'submaternity';
			 		$subSick = \Input::json($sub_nm);
			 	}else if(isset($form_leave_type) && $form_leave_type == 'Paternity Leave'){
			 		$tables = 'leave_paternity';
			 		$sub_nm = 'subpaternity';
			 		$subSick = \Input::json($sub_nm);
			 	}else if(isset($form_leave_type) && $form_leave_type == 'Marriage Leave'){
			 		$tables = 'leave_marriage';
			 		$sub_nm = 'submarriage';
			 		$subSick = \Input::json($sub_nm);
			 	}else{
			 		return \Response::json(['header'=>['message'=>"Error Input",'status'=>500],'data'=>null],500);
			 	}
			 	$compare = $this->compare($subSick);
			 	if($compare != null){
			 		 return $leave->getMessage("kindly check same year in row in when save data, same year recorded is ".$compare[0] ,500,null);
			 	}
			 	$count_subSick = count($subSick)-1;
			 	$department = \DB::SELECT("select * from department where name='$id_department' ");
			 	$department_ex = $department[0]->id;
			 	$check_department = \DB::SELECT("select * from ".$tables." where department=$department_ex and applied_for in (1,2) and department=$department_ex and applied_for = $applied");

				if($check_department != null){
					return $leave->getMessage("failed ".$department[0]->name." data already exist",500,null);
				}

			 	if($applied == null){
			 	          return $leave->getMessage(" required field can't be null ",500,null);
			 	}if($id_department == null){
			 		return $leave->getMessage("required field can't be null ",500,null);
			 	}if($ent_day == null){
			 		return $leave->getMessage(" required field can't be null ",500,null);
			 	}if($ent_day == null){
			 		return $leave->getMessage(" required field can't be null ",500,null);
			 	}

				$insert = \DB::SELECT("CALL insert_".$tables."($applied,$department_ex,'$ent_start','$ent_day')");
				if(isset($subSick) && $subSick != null){
					//$id = \DB::SELECT("select id from ".$tables." order by id desc limit 1 ");
					$id_test = $insert[0]->id;

					for($i = 0; $i <= $count_subSick; $i++){
					 	$year = $subSick[$i]['year'];
					 	$days = $subSick[$i]['days'];
						\DB::SELECT("CALL insert_".$tables."_detail($id_test,$year,$days)");
					}

					$data_test = \DB::SELECT("select * from ".$tables."_detail where ".$tables."_id=$id_test");
					$count_test = count($data_test)-1;

					for($i = 0; $i < $count_test; $i++){
					 	$id_test_ex =  $data_test[$i]->id;
					 	\DB::SELECT("CALL insert_".$tables."_test($id_test,$id_test_ex)");
					 }

				}else{
					$subSick = [];
				}

				$db_data = $insert;
				$applied = $this->check_employee($db_data[0]->applied_for);

				$t = [ "id" => $db_data[0]->id,
					    	"Appliedfor" =>"Applied For : ".$applied ,
					    	"Department" =>"Department : ".$department[0]->name,
					    	"StartFrom" =>"StartFrom : ".$db_data[0]->entitlement_start,
					    	"Entitlementdays" =>"Entitlementdays : ".$db_data[0]->entitlement_day
					 ];

				$t[$sub_nm] = $subSick;
				
				$data[] = $t;
				 return $leave->index($data);
		}else{ 
		$message = $access[0]; $status = $access[1]; $data=$access[2];
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}
	 }

	public function update_sick_leave($id){
	 	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			 	if($id != null){

			 		$applied = \Input::get('Appliedfor');

				 	$leave = new leaveTable_Model;
				 	if(!isset($applied)){
			 			$for = \Input::get('form_for');
			 			if($for == 2){
			 				$applied == "Local";		
			 			}else if($for == 2){
			 				$applied == "Expat";		
			 			}else{
			 				return $leave->getMessage("failed, please checklist for expat or local",500,null);
			 			}
				 	}
				 	if($applied == "Expat"){
				 		$applied = 1;
				 	}
				 	if($applied == "Local" ){
				 		$applied = 2;
				 	}

				 	$id_department = \Input::get('Department');

				 	if(ctype_digit($id_department)){
			 			$data_insert = $id_department;
			 		}else{
			 			$department_id = \DB::SELECT("select id from department where name='$id_department' ");
						$data_insert  = $department_id[0]->id;
			 		}

			 		//return $data_insert;
				 	$ent_start = \Input::get('StartFrom');
				 	$ent_day = \Input::get('Entitlementdays');
				 	
				 	$form_leave_type = \Input::get('form_leave_type');
				 	
				 	if(isset($form_leave_type) && $form_leave_type == 'Sick Leave'){
			 			$tables = 'leave_sick';
			 			$subSick_nm = 'subsick';
			 			$subSick = \Input::json($subSick_nm);

				 	}else if(isset($form_leave_type) && $form_leave_type == 'Maternity Leave'){
				 		$tables = 'leave_maternity';
				 		$subSick_nm = 'submaternity';
				 		$subSick = \Input::json($subSick_nm);

				 	}else if(isset($form_leave_type) && $form_leave_type == 'Paternity Leave'){
				 		$tables = 'leave_paternity';
				 		$subSick_nm = 'subpaternity';
				 		$subSick = \Input::json($subSick_nm);

				 	}else if(isset($form_leave_type) && $form_leave_type == 'Marriage Leave'){
				 		$tables = 'leave_marriage';
				 		$subSick_nm = 'submarriage';
				 		$subSick = \Input::json($subSick_nm);

				 	}else{
				 		return \Response::json(['header'=>['message'=>"Error Input",'status'=>500],'data'=>null],500);
				 	}

				 	$compare = $this->compare($subSick);
				 	if($compare != null){
				 		 return $leave->getMessage("kindly check same year in row in when save data, same year recorded is ".$compare[0] ,500,null);
				 	}
				 	$count_subSick = count($subSick)-1;

					if($leave->check_department($data_insert,$applied,$id,$tables) == "no"){
			 			  return $leave->getMessage("can't update applied for". $applied ."and department already exist",500,null);
			 		}

					$insert = \DB::SELECT("CALL update_".$tables."($id,$applied,$data_insert,'$ent_start','$ent_day')");
				 	$delete_all = \DB::SELECT("delete  from ".$tables."_detail where ".$tables."_id=$id");

				 	if(isset($subSick) && $subSick != null){
						$id_test = $id;

						for($i = 0; $i <= $count_subSick; $i++){
						 	$year = $subSick[$i]['year'];
						 	$days = $subSick[$i]['days'];
							\DB::SELECT("CALL insert_".$tables."_detail($id_test,$year,$days)");
						}

						 $data_test = \DB::SELECT("select id from ".$tables."_detail where ".$tables."_id=$id_test");
						 $count_test = count($data_test)-1;

						 for($i = 0; $i < $count_test; $i++){
						 	$id_test_ex =  $data_test[$i]->id;
						 	\DB::SELECT("CALL Insert_Leave_Sick_test($id_test,$id_test_ex)");
						 }

					}

					$db_data = \DB::SELECT("select * from ".$tables." where id=$id order by id  desc limit 1 ");

					if(is_numeric($data_insert)){
						$department_id = \DB::SELECT("select name from department where id='$data_insert' ");
						$data_insert  = $department_id[0]->name;
					}
					$dbs= ["id" => $id,
					       "Appliedfor" =>"Applied For : ".$applied ,
					       "Department" =>"Department : ".$data_insert,
					       "StartFrom" =>"StartFrom : ".$db_data[0]->entitlement_start,
				   	       "Entitlementdays" => "Entitlementdays : ".$db_data[0]->entitlement_day
					    ];
					$dbs[$subSick_nm] = $subSick;
					$result[] = $dbs;


					 return $leave->index($result);
				}else{ return $leave->getMessage("id not found", 500, null);}
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}
	 }

 	public function destroy_sick($id,$type){
 		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
	 		$leave = new leaveTable_Model;

	 		$form_leave_type = $type;
	 		if(isset($form_leave_type) && $form_leave_type == 'Sick Leave'){
		 		$tables = 'leave_sick';
		 	}else if(isset($form_leave_type) && $form_leave_type == 'Maternity Leave'){
		 		$tables = 'leave_maternity';
		 	}else if(isset($form_leave_type) && $form_leave_type == 'Paternity Leave'){
		 		$tables = 'leave_paternity';
		 	}else if(isset($form_leave_type) && $form_leave_type == 'Marriage Leave'){
		 		$tables = 'leave_marriage';
		 	}else{
		 		return \Response::json(['header'=>['message'=>"Error Input",'status'=>500],'data'=>null],500);
		 	}
			if($id != null){
				$explode_id = explode(",",$id);
				$count = count($explode_id)-1;
				$i = 0;
				for($i; $i <= $count; $i++){
					$data = $explode_id[$i];
					$db = \DB::SELECT("CALL delete_".$tables."($data)");
				}
					return $leave->getMessage("success",200,[]);

			 }
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
		}
	}


	// END CRUD EVENT ###############

	public function check_employee($data = ""){

		if($data == 1){
			return"Expat";
		}
		if($data == 2){
			return "Local";
		}
		if($data == 0){
			return "Null";
		}
	}

	public function search_leave_vacation(){
		//$this->access;
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	 	if($access[1] == 200){

		$leave = new leaveTable_Model;
		$type = \Input::get('leave_type');
		$for = \Input::get('for');
		$data =[];

		if($type == "Enhance Leave"){
			$key = \Input::get('key');
			$keys= base64_decode($key);
			$test = explode('-',$keys);
			$empx = $test[1];

			$check_ldap =  \DB::SELECT("select t2.role_name from ldap t1,role t2 where t1.employee_id = '$empx' and t1.role_id = t2.role_id ");
			if(isset($check_ldap[0]->role_name)){
				$check_ldap =  explode(" ",strtolower($check_ldap[0]->role_name));

				// foreach ($check_ldap as $key => $value) {
				// 	if($value == "user"){
				// 		 return $leave->indexAcess('Unauthorize', 500, [],$access[3]);
				// 	}
				// }
			}
		}

		$filename ='app/config_depart.json';
		$fp = fopen(storage_path($filename),'w');
		$datas[] = ['for' => $for,'type' => $type];
                 	fwrite($fp, json_encode($datas));
                	fclose($fp);

		if($type != null ){
			$type =  $leave->search_null($type);
		} else{ return $leave->getMessage('failed to load data',500,null); }

		$key = \Input::get('key');
		$keys= base64_decode($key);
		$test = explode('-',$keys);
		$data = $test[1];

		$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
		$db_data =  $db[0]->local_it;

		($db_data == 1 ? $status = "expat" : $status = null);
		($db_data == 2 ? $status = "local" : $status = null);
		($db_data == 3 ? $status = "local_it" : $status = null);

		$data = $leave->db_query($type,$for,$status);

		$arg = [];
		$sub_base = [];
		
		if($type == 2){
           
            $count = count($data);
            for($i = 0;  $i < $count; $i++){
                $idx = $data[$i]['id'];
                $arg1 = count($data[$i]['subvacation']);
                for($j = 0; $j < $arg1; $j++){
                        $year = $data[$i]['subvacation'][$j]['year'];
                        if(!isset($idx)){
                        	$idx = 0;
                        }
                        if(!isset($year)){
                        	$year = 0;
                        }
                        $check_data = \DB::SELECT("select * from  all_vacation where id_vacation =  $idx  and  tenure_date_vl_expat = $year");
 
                        if($check_data != null){
                            $data[$i]['master'] = 'locked';
                            $data[$i]['subvacation'][$j]['subMaster'] = "locked";
                        }else{
                            $data[$i]['subvacation'][$j]['subMaster'] = "unlocked";
                        }
 
                        if(!isset($data[$i]['master'])){
                            $data[$i]['master'] = 'unlocked';
                        }
                }
            }
        }
 
        if($type == 4){
            $count = count($data);
            for($i = 0;  $i < $count; $i++){
                $idx = $data[$i]['id'];
                $arg1 = count($data[$i]['subsick']);

                for($j = 0; $j < $arg1; $j++){
                        $year = $data[$i]['subsick'][$j]['year'];
                        $check_data = \Db::SELECT("select * from  all_sick where ids =  $idx  and  tenure_date_vl_expat = $year");
 
                        if($check_data != null){
                            $data[$i]['master'] = 'locked';
                            $data[$i]['subsick'][$j]['subMaster'] = "locked";
                        }
 
                        if($check_data == null){
                            $data[$i]['subsick'][$j]['subMaster'] = "unlocked";
                        }
 
                        if(!isset($data[$i]['master'])){
                            $data[$i]['master'] = 'unlocked';
                        }
                }
            }
        }

        if($type == 7){
            $count = count($data);
            for($i = 0;  $i < $count; $i++){
                $idx = $data[$i]['id'];
                $arg1 = count($data[$i]['subbereavement']);
                for($j = 0; $j < $arg1; $j++){
                        $relation = $data[$i]['subbereavement'][$j]['relation_to_deceased'];
                        $check_data = \Db::SELECT("select * from  all_bereavement where bereavement_id =  $idx  and  relation_ = '$relation' ");
 
                        if($check_data != null){
                            $data[$i]['master'] = 'locked';
                            $data[$i]['subbereavement'][$j]['subMaster'] = "locked";
                        }
 
                        if($check_data == null){
                            $data[$i]['subbereavement'][$j]['subMaster'] = "unlocked";
                        }
 
                        if(!isset($data[$i]['master'])){
                            $data[$i]['master'] = 'unlocked';
                        }
                }
            }
        }

         if($type == 5){
            $count = count($data);
            for($i = 0;  $i < $count; $i++){
                $idx = $data[$i]['id'];
                $arg1 = count($data[$i]['submaternity']);
                for($j = 0; $j < $arg1; $j++){
                        $year = $data[$i]['submaternity'][$j]['year'];
                        $check_data = \Db::SELECT("select * from  all_maternity where ids =  $idx  and  tenure_date_vl_expat = $year");
 
                        if($check_data != null){
                            $data[$i]['master'] = 'locked';
                            $data[$i]['submaternity'][$j]['subMaster'] = "locked";
                        }
 
                        if($check_data == null){
                            $data[$i]['submaternity'][$j]['subMaster'] = "unlocked";
                        }
 
                        if(!isset($data[$i]['master'])){
                            $data[$i]['master'] = 'unlocked';
                        }
                }
            }
        }

        if($type == 6){
            $count = count($data);
            for($i = 0;  $i < $count; $i++){
                $idx = $data[$i]['id'];
                $arg1 = count($data[$i]['subpaternity']);
                for($j = 0; $j < $arg1; $j++){
                        $year = $data[$i]['subpaternity'][$j]['year'];
                        $check_data = \Db::SELECT("select * from  all_paternity where ids =  $idx  and  tenure_date_vl_expat = $year");
 
                        if($check_data != null){
                            $data[$i]['master'] = 'locked';
                            $data[$i]['subpaternity'][$j]['subMaster'] = "locked";
                        }
 
                        if($check_data == null){
                            $data[$i]['subpaternity'][$j]['subMaster'] = "unlocked";
                        }
 
                        if(!isset($data[$i]['master'])){
                            $data[$i]['master'] = 'unlocked';
                        }
                }
            }
        }

        if($type == 8){
            $count = count($data);
            for($i = 0;  $i < $count; $i++){
                $idx = $data[$i]['id'];
                $arg1 = count($data[$i]['submarriage']);
                for($j = 0; $j < $arg1; $j++){
                        $year = $data[$i]['submarriage'][$j]['year'];
                        $check_data = \Db::SELECT("select * from  all_marriage where ids =  $idx  and  tenure_date_vl_expat = $year");
 
                        if($check_data != null){
                            $data[$i]['master'] = 'locked';
                            $data[$i]['submarriage'][$j]['subMaster'] = "locked";
                        }
 
                        if($check_data == null){
                            $data[$i]['submarriage'][$j]['subMaster'] = "unlocked";
                        }
 
                        if(!isset($data[$i]['master'])){
                            $data[$i]['master'] = 'unlocked';
                        }
                }
            }
        }
	
		$message = "success";
		$status = 200;
		$access =  $access[3];
		}else{
	            $message = $access[0]; $status = $access[1]; $data=$access[2];
	       	}
	    return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access],'data'=>$data,'status' => $data,'sub' => $arg],$status);
	      //return $leave->indexAcess($message, $status, $data,$access);

	}

	public function index_vacation(){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){


		$leave = new leaveTable_Model;
		$key = \Input::get('key');
		$keys= base64_decode($key);
		$key_ex = explode('-',$keys);

		 $db =\DB::SELECT("select local_it from emp where employee_id=$key_ex[1]");
		 $key = \Input::get('key');
		 $keys= base64_decode($key);
		 $test = explode('-',$keys);
		 $data = $test[1];

		$check_ldap =  \DB::SELECT("select t2.role_name from ldap t1,role t2 where t1.employee_id = '$data' and t1.role_id = t2.role_id ");
			if(isset($check_ldap[0]->role_name)){
				$check_ldap =  explode(" ",strtolower($check_ldap[0]->role_name));

				foreach ($check_ldap as $key => $value) {
					if($value == "user"){
							$check_user =  "user";
						 //return $leave->indexAcess('Unauthorize', 500, [],$access[3]);
					}
				}
			}

			if(!isset($check_user)){
				$check_user =  "NONuser";
			}

		 $db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
		 $db_data =  $db[0]->local_it;

		 if($db_data === 1 ){ $status_ex = "expat" ;}
		 if($db_data === 2 || $db_data === 3 ){ $status_ex = "local" ;}
		 //if($db_data === 3 ){ $status_ex = "local_it" ;}

		 $dac = \DB::SELECT("select role_id from ldap where employee_id = '$data' ");
	
		 if($dac[0]->role_id == 1 || $dac[0]->role_id == 4 || $dac[0]->role_id == 16 || $dac[0]->role_id == 36){
			 $status_ex = "admin";
		 }
		 if($dac[0]->role_id == 8){
			 $status_ex = "hr";
		 }

		 if($db[0]->local_it != 1 ){
		 	$db = \DB::SELECT("select * from leave_type where id = 3");
		 }
		
		 if($db_data == 1 ){
		 	$db = \DB::SELECT("select * from leave_type where id in(2,4,7) ");
		 }

		  if($db == null){
		  	if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
				$message = "Unauthorized";
			}else{
				$message = "Data not exist";
			}
			$status = 200;
			$data =  [];
			$access = $access[3];
		}else if($db != null){
			if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
				$message = "Unauthorized";
			}else{
				$message ="success";
			}
			$status = 200;
			$data = $db;
			$access = $access[3];
		}else{
			if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
				$message = "Unauthorized";
			}else{
				$message = "failed to load " ;
			}
			$status = 500;
			$data = null;
			$access = $access[3];
		}
		}else{
	            $message = $access[0]; $status = $access[1]; $data=$access[2];
	    }
	       return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access],'data'=>$data,'status' => $status_ex,'check_user' => $check_user],$status);
	}

	public function index_jobtitile(){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$leave = new  leaveTable_Model;
			$db = \DB::SELECT("select id,title from job");
			return  $leave->index($db);
		}else{
			return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access],'data'=>$data,'status' => $status_ex],$status);
		}
	}

	public function department(){
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){

			$data = \DB::SELECT("select * from  department where id <> 1 ");
			$message = 'Succcess' ; $status = 200; 
		}else{
	        $message = $access[0]; $status = $access[1]; $data=$access[2];
	    }
	    return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access],'data'=>$data,'status' => $status],$status);
	}


}
