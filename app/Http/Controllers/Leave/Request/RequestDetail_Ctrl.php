<?php namespace Larasite\Http\Controllers\Leave\Request;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Larasite\Library\FuncAccess;
use Illuminate\Http\Request;

class RequestDetail_Ctrl extends Controller {
	protected $form = 66;
	public function leaveRequest()
	{
		$name = \Input::get('get');
		$leavetype = \Input::get('leavetype');
		$from = \Input::get('from');
		$to = \Input::get('to');
		$dayoff = \Input::get('dayoff');
		$comment = \Input::get('comment');

		$rule = [
			'name' => 'required | Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/'
		];

		$validator = \Validator::make(\Input::all(),$rule);
		if ($validator->fails()){
			$val = $validator->errors()->all();
			return \Response::json(['header'=>['message'=>$message[0],'status'=>500],'data'=>null],200);
		}
		else{
			$userID = \DB::select("CALL search_employee_by_id('$name')");
			if (!$userID){
				return \Response::json(['header'=>['message'=>'No employee','status'=>500],'data'=>null],200);}
			else{
				// $createRequest = \DB::select("CALL Insert_Training('$name',$type,'$start','$end','$comment')");
				return \Response::json(['header'=>['message'=>'Success','status'=>200],'data'=>['name'=>$userID]],200);
			}
		}
	}
}
