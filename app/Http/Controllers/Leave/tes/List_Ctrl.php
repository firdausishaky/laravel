<?php namespace Larasite\Http\Controllers\Leave\tes;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Library\FuncAccess;

class List_Ctrl extends Controller {
private $form = 71;
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$from = date("y-m-d",strtotime("-1 month"));
			$to = date("y-m-d",strtotime("+1 year, -1 month"));

			$depList = \DB::select("CALL view_department");
			if (!$depList)
				$dept = null;
			else
				$dept = $depList;
			$leaveList = \DB::select("CALL ");
			if (!$leaveList)
				$leave = null;
			else
				$leave = $leaveList;
			$showData = \DB::select("CALL ");
			if (!$showData)
				return \Response::json(['header'=>['message'=>'No leave record found in '.$from.' to .'.$to,'status'=>500],'data'=>null],500);
			else
				$data = [$from,$to,$leave,$dept,$showData];
				return return \Response::json(['header'=>['message'=>'Show record data','status'=>200],'data'=>$data],200);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	
	public function show($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$showDetail = \DB::select("CALL ");
			if (!$showDetail)
				return \Response::json(['header'=>['message'=>'Can not show datails','status'=>500],'data'=>null],500);
			else
				return return \Response::json(['header'=>['message'=>'Show record data','status'=>200],'data'=>$showDetail],200);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function update($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update');
		if($access[1] == 200){
			$i = \Input::all();
			$respondLeave = \DB::select("CALL ");
			if (!$respondLeave)
				return \Response::json(['header'=>['message'=>'Can not make confirmation to this leave','status'=>500],'data'=>null],500);
			else{
				return \Response::json(['header'=>['message'=>'Success confirm the leave','status'=>200],'data'=>$confirm],200);
			}
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete');
		if($access[1] == 200){
			$del = \DB::select("CALL ");
			if (!$del)
				return \Response::json(['header'=>['message'=>'Can not delete leave request','status'=>500],'data'=>null],500);
			else
				return \Response::json(['header'=>['message'=>'Delete leave success','status'=>200],'data'=>null],200);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
}