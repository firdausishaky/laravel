<?php namespace Larasite\Http\Controllers\Module;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Model\EmployeeInfo_Model;
use Larasite\Privilege;
class ChangingEmp_Ctrl extends Controller {




// SEARCH DOMAIN
	public function index(){
		$get_role = $this->check();	$model = new EmployeeInfo_Model; // SET MODEL
		$r = \Request::all();

		if($r['key'] && $r['search'] != null && $r['search'] != 'undefined'){
		$decode = base64_decode($r['key']);
		$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
		$name = $r['search'];
		}else{ return \Response::json('Access Denied',500); }
		
		if(isset($get_role['data'])){ // CHECK KEY
				$access = $this->check_type2($get_role['data'],1); // GET ACCESS
				if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS
					$show = \DB::select("SELECT employee_id , concat(first_name,' ',if(middle_name is null or middle_name = '',' ',middle_name),' ',last_name) as name from emp where first_name LIKE '$name%' and employee_id != '$id' and ad_username != ''" );
						if($show){
							$data=$show; $status=200;
						}else{ $data=[['name'=>'Data not found.']]; $status=404;}
				}else{ $message=$access['message'];	$status=$access['status']; $data=null; }
		}
		else{ $message = $get_role['message']; $status=401; $data=null;}
		return \Response::json($data,$status);
	}// END INDEX #########################################################################################


// SEARCH JOB #######################################
	public function Search($string){ 
		$check_lastName = strpos($string," "); 
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));

		if($last_name){
			if(isset($string)){
				$first_name = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select employee_id, concat(first_name,' ',middle_name,' ',last_name) as name 
						from emp where first_name = '$first_name' and middle_name LIKE '$last_name%' limit 5");
			}else{ return ['data'=>null,'status'=>500]; }
		}else{
			$data = \DB::select("SELECT employee_id, concat(first_name,' ',middle_name,' ',last_name) as name 
						from emp where first_name LIKE '$string%' limit 10");
			$status=200;
		}
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################

// SEARCH DOMAIN
	public function Search(){
		$string = \Input::get('employee');
		
			$search= $this->Search($string);
			if(isset($search) && $search != null && $search['status'] == 200){ $message='Get domain.'; $status=$search['status']; $data=$search['data']; }
			else{ $status=$search['status']; $data=$search['data']; $message='Job not found.'; }	
		
		
		return \Response::json($data,200);
	}
// END SEARCH DOMAIN

// CHECK ROLE ############################################################################
//PARAM $ROLE = PERMISSION USER
	public function check_role($role,$form)
	{
		$getModel = new Privilege;
		if(isset($role, $form)){
			return $getModel->check_role($role,$form);
		}
		else{
			$msg['warning'] = 'Unauthorized';
			$msg['status'] = 404;
			return $msg; 
		}
	}
//END CHECK ROLE #########################################################################
// CHECK ACCESS ###############################################################################
	public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(isset($q) != null){ //check isset key in session
				 
						 $r = array();
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } // end for store role to array.

						 if(isset($r['key'])){
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							//return $employee_id;
							 $q2 = $getModel->session_role($employee_id);
							 foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 } // end for store role to array.
						 }
						 else{
						 	return ['message'=>'ACCESS DENIED','data'=>null];
						 }
						

				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  $role['role'];
						$msg = 'ACCESS GRANTED !';	
					}else{
						$msg = 'ACCESS DENIED!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED!';
					$data = null;	
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;	
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		return ['message'=>$msg,'data'=>$data];
	}
// END CHECK ACCESS ############################################################################
}
