<?php namespace Larasite\Http\Controllers\Module;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*Model*/ 
use Larasite\Model\EmployeeInfo_Model;
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;

class EmployeeInfo_Ctrl extends Controller {
protected $add = 48;
protected $form = ['expat'=>17,'local'=>18];
protected $formx = "";
public function __construct(){

	//give access permission
	$key = \Input::get('key');
	$keys= base64_decode($key);
	$test = explode('-',$keys);
	$data = $test[1];

	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
	$db_data =  $db[0]->local_it;

		if($db_data == 1){
			return $this->formx = "17";
		}elseif($db_data == 2){
				return $this->formx = "18";
		}else{
			return $this->formx = null;
		}
}

	private function param_valid($REQ)
	{
		/*Param Search*/ $lc = $REQ['lc']; $inc = $REQ['inc'];
		if($inc == 'undefined' || $inc == NULL){ $inc = 1; }
		return ['inc'=>$inc,'lc'=>$lc];
	}
	private function check_id($id)
	{
		$rule = ['undefined',NULL,''];
		if(in_array($id, $rule)){ $data = ['ID Undefined.',500,NULL]; }
		else{ $data = false; } 
		return $data;
	}
	// TERMINATE
	public function get_datas($id){
	
			$type  = \Input::get('type');
			$employee = $id;
			if($type ==  1){
				$result = \DB::SELECT("select employee_id from emp where emp.employee_id LIKE '%$id%' ");
				$data = $result;
				$message =  ($result == null ? 'Not Found' : 'Success');
				$status =  ($message == 'Not Found' ? 200 : 200);

			}else{
				$search_supervisor = \DB::SELECT("select employee_id from emp  where first_name LIKE '%$employee%' OR middle_name LIKE '%$employee%' OR last_name LIKE '%$employee%'  limit 1 ");
				if(isset($search_supervisor[0]->employee_id) && $search_supervisor[0]->employee_id != null){
					$super = $search_supervisor[0]->employee_id;
					$data = \DB::SELECT("select emp.employee_id,concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as name from emp,emp_supervisor where emp.employee_id=emp_supervisor.employee_id  and emp_supervisor.supervisor =  '$super'  ");
					$message = 'success'; 
					$status =  200;
				}else{
					$data = [];
					$message= "Not Found, supervisor";
					$status = 200;
				}
				
			}
		// }else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}


	public function destroy($id)
	{
		/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; $access = $FuncAccess->AccessPersonal(['Personal'=>NULL,'Request'=>$REQ],$this->form,1);
		/*Model*/ $model = new EmployeeInfo_Model;
		if($access[1] = 200){		
			$check_id = $this->check_id($id);
			if($check_id){ $message = $check_id[0]; $status=$check_id[1]; $data=$check_id[2]; }
			else{
				$toArray = explode(",", $id);
				$get = $model->DestroyEMP($toArray);
				
				if($get['status']==200){
					$message='Delete Successfully.'; $status=$get['status']; $data=$get['data'];
				}else{ $message='Store not success.'; $status=500; $data=null; }	
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	// SEARCH DOMAIN
	public function Search()
	{
		$REQ = \Request::all();
		/*Model*/ $model = new EmployeeInfo_Model;
		if($crud[1] = 200){	

			if($REQ['key'] && $REQ['search'] != null && $REQ['search'] != 'undefined'){
				$decode = base64_decode($REQ['key']);
				$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
				$name = $REQ['search'];
			}else{ return \Response::json('Access Denied',500); }

			$getData = $model->searchEmp($name,$id);
			$data = $getData['data']; $status = $getData['status'];
			return \Response::json($data,$status);

		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
	}
	
	public function SearchByName()
	{
		$REQ = \Request::all();
		/*Model*/ $model = new EmployeeInfo_Model;
		
			if($REQ['key'] && $REQ['q'] != null && $REQ['q'] != 'undefined'){
				$decode = base64_decode($REQ['key']);
				$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
				$getData = $model->search_by_name($REQ['q'],$id);
				$data = $getData['data']; $status = $getData['status'];
				return \Response::json($data,$status);
			}else{ return \Response::json('Not Found.',200); }

	}
	
	public function SearchBySPV()
	{
		$REQ = \Request::all();
		/*Model*/ $model = new EmployeeInfo_Model;
		
			if($REQ['key'] && $REQ['q'] != null && $REQ['q'] != 'undefined'){
				$decode = base64_decode($REQ['key']);
				$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
				$getData = $model->search_by_spv($REQ['q'],$id);
				$data = $getData['data']; $status = $getData['status'];
				return \Response::json($data,$status);
			}else{ return \Response::json('Not Found.',200); }

	}
	
	// INDEX
	public function index() 
	{
		// /*Access*/$FRA = new FuncAccess; 
		// $crud = $FRA->Access(\Request::all(),$this->formx,'read');
		// if($crud[1] == 200){
			$perm = NULL;
			/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; /*Model*/ $model = new EmployeeInfo_Model;
			$department  =\Db::SELECT("select id,name from department where id not in(0,1)");		
			$job = \DB::SELECT("select id,title from job where id not in(0)");

			$expat = $this->form['expat']; $local = $this->form['local'];
			$getReq = $model->getReq($REQ);

			/*PARAM*/ //$param = $this->param_valid($getReq['param']);
			if(isset($REQ['lc'])){ $param['lc'] = intval($REQ['lc']); }
			if(isset($REQ['inc'])){ $param['inc'] = intval($REQ['inc']); }
			if(isset($REQ['depart'])){ $param['depart'] = intval($REQ['depart']); }
			if(isset($REQ['status'])){ $param['status'] = intval($REQ['status']); }
			if(isset($REQ['job'])){ $param['job'] = intval($REQ['job']); }
			if(isset($REQ['id'])){ $param['id'] = $REQ['id']; }
			if(isset($REQ['empId'])){ $param['id'] = $REQ['empId']; }
			if(isset($REQ['spv'])){ $param['spv'] = $REQ['spv']; }
			

			$access =  $FuncAccess->Access($REQ,$expat,'read');
			if($access[1] == 200){ 
				$access2 = $FuncAccess->Access($REQ,$local,'read');
				if($access2[1] == 200){
					$perm = 'ExpatLocal';
				}else{ 
					$perm = 'Expat'; 
				}
			}elseif( $FuncAccess->Access($REQ,$local,'read')[1] == 200 ){
			 	$perm = 'Local';
			}else{ $perm  = NULL; }
			if($perm != NULL){
				$check_action = $FuncAccess->check_role($getReq['roleBase'],$getReq['employee_id'],$this->add);
				//return (array)$check_action;
				$type = ['roleBase'		=>	$getReq['roleBase'],
						'employee_id' 	=>	$getReq['employee_id'],
						'depart' => $department,
						'job' => $job,
						'local_it'		=>	$getReq['typeLocal'],
						'action_create'	=>	$check_action['create']
						];
				//$getRecord = $model->ListOrderByLocal($type,$input);
				
				$getRecord =  $model->Lists($type,$param,$perm,$access[3]);
				return $getRecord;
				
				$message = $getRecord['message']; $status = $getRecord['status']; $data = $getRecord['data'];
			}
		else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}
	// SEARCH FILTER
	public function search_filter(){
		/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; $access = $FuncAccess->AccessPersonal(['Personal'=>NULL,'Request'=>$REQ],$this->form,1);
		/*Model*/ $model = new EmployeeInfo_Model;
		return 1;
		if($access[1] = 200){
			$getReq = $model->getReq($REQ);
			/*PARAM*/ $param = $this->param_valid($getReq['param']); $input['inc'] = $param['inc']; $input['lc'] = $param['lc'];
			
			$check_action = $FuncAccess->check_role($getReq['roleBase'],$getReq['employee_id'],$this->add);
			$type = ['roleBase'		=>	$getReq['roleBase'],
					'employee_id' 	=>	$getReq['employee_id'],
					'local_it'		=>	$getReq['typeLocal'],
					'action_create'	=>	$check_action['create']
					];
			$getRecord = $model->ListOrderByLocal($type,$input);
			$message = $getRecord['message']; $status = $getRecord['status']; $data = $getRecord['data'];

		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);	
	}
	/* EDIT */
	public function edit($id){ 
		$message = 'Page Not Found.'; $status = 404; $data=NULL;
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// public function postInformation(){
	// 	$i = \Input::all();
	// 	!isset($i['empName']) ? $i['empName'] = 0 : $i['empName'] = $i['empName'];
	// 	!isset($i['sName']) ? $i['sName'] = 0 : $i['sName'] = $i['sName'];
	// 	!isset($i['empId']) ? $i['empId'] = 0 : $i['empId'] =  $i['empId'];
		
	// 	if($i['empName'] != 0)
	// 	{	
	// 		$empName = $i['empName'];		
	// 		$empname = \DB::SELECT("select employee_id,concat(first_name,' ',middle_name,' ',last_name)as name from emp
	// 					where first_name like name or middle_name like name or last_name like '$name'");
	// 	}

	// 	if($i['sName']  != 0){	
	// 		$sName = $i['sName'];
	// 		$sName = \DB::SELECT("select emp.employee_id,concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name)as name from emp,emp_supervisor 
	// 				 where emp.employee_id = $sName group by employee_id");
	// 	}

	// 	if($i['empId'] != 0)
	// 	{
	// 		$empId = $i['empId'];	
	// 		$empId = \DB::SELECT("select employee_id,concat(first_name,' ',middle_name,' ',last_name)as name from emp
	// 					where employee_id like '$name'");
	// 	}	
	// }

	// public function getInformation(){
	// 	$jobTitle = \DB::SELECT("select job.id as jobId, job.title from job  union  select jobs_emp_status.id  as idStatus, jobs_emp_status.title from  jobs_emp_status");
	

	// 	if($i[''])
	// }
	// public function eInformation(){
	// 	$i = \Input::all();
	// 	!isset($i['empName']) ? $i['empName'] = 0 : $i['empName'] = $i['empName'];
	// 	!isset($i['sName']) ? $i['sName'] = 0 : $i['sName'] = $i['sName'];
	// 	!isset($i['empId']) ? $i['empId'] = 0 : $i['empId'] =  $i['empId'];
	// 	!isset($i['jobTitle']) ? $i['jobTitle'] = 0 : $i['jobTitle'] =  $i['jobTitle'];
	// 	!isset($i['empStat']) ? $i['empStat'] = 0 : $i['empStat'] = $i['empStat'];
	// 	!isset($i['depart']) ? $i['depart'] = 0 : $i['depart'] =  $i['depart'];
	// 	!isset($i['inc']) ? $i['inc'] = 2 : $i['inc'] =  $i['inc'];
	// 	!isset($i['category']) ? $i['category'] = 0 : $i['category'] =  $i['category'];

	// 	if($i['empName'] != 0)
	// 	{
	// 		$name = $i['empName'];	
	// 		$empname = \DB::SELECT("select employee_id,concat(first_name,' ',middle_name,' ',last_name)as name from emp
	// 					 where first_name like name or middle_name like name or last_name like '$name'");	
	// 	}

	// 	if($i['sName']  != 0){	
	// 		$sName = $i['sName'];
	// 		$sName = \DB::SELECT("select emp.employee_id,concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name)as name from emp,emp_supervisor where emp.employee_id = $sName group by employee_id");
	// 	}

	// 	if()

	// 	// if($i['empId'])
	// 	// {
	// 	// 	$empId =  $i['empId'];
			
	// 	// }
	// }	
// protected $add = 48;
// protected $form = ['expat'=>17,'local'=>18];
// protected $formx = "";
// public function __construct(){

// 	//give access permission
// 	$key = \Input::get('key');
// 	$keys= base64_decode($key);
// 	$test = explode('-',$keys);
// 	$data = $test[1];

// 	$db  = \DB::SELECT("select local_it from  emp where employee_id='$data' ");
// 	$db_data =  $db[0]->local_it;

// 		if($db_data == 1){
// 			return $this->formx = "17";
// 		}elseif($db_data == 2){
// 				return $this->formx = "18";
// 		}else{
// 			return $this->formx = null;
// 		}
// }

// 	private function param_valid($REQ)
// 	{
// 		/*Param Search*/ $lc = $REQ['lc']; $inc = $REQ['inc'];
// 		if($inc == 'undefined' || $inc == NULL){ $inc = 1; }
// 		return ['inc'=>$inc,'lc'=>$lc];
// 	}
// 	private function check_id($id)
// 	{
// 		$rule = ['undefined',NULL,''];
// 		if(in_array($id, $rule)){ $data = ['ID Undefined.',500,NULL]; }
// 		else{ $data = false; } 
// 		return $data;
// 	}
// 	// TERMINATE
// 	public function destroy($id)
// 	{
// 		/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; $access = $FuncAccess->AccessPersonal(['Personal'=>NULL,'Request'=>$REQ],$this->form,1);
// 		/*Model*/ $model = new EmployeeInfo_Model;
// 		if($access[1] = 200){		
// 			$check_id = $this->check_id($id);
// 			if($check_id){ $message = $check_id[0]; $status=$check_id[1]; $data=$check_id[2]; }
// 			else{
// 				$toArray = explode(",", $id);
// 				$get = $model->DestroyEMP($toArray);
				
// 				if($get['status']==200){
// 					$message='Delete Successfully.'; $status=$get['status']; $data=$get['data'];
// 				}else{ $message='Store not success.'; $status=500; $data=null; }	
// 			}
// 		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
// 	}
// 	// SEARCH DOMAIN
// 	public function Search()
// 	{
// 		$REQ = \Request::all();
// 		/*Model*/ $model = new EmployeeInfo_Model;
// 		if($crud[1] = 200){	

// 			if($REQ['key'] && $REQ['search'] != null && $REQ['search'] != 'undefined'){
// 				$decode = base64_decode($REQ['key']);
// 				$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
// 				$name = $REQ['search'];
// 			}else{ return \Response::json('Access Denied',500); }

// 			$getData = $model->searchEmp($name,$id);
// 			$data = $getData['data']; $status = $getData['status'];
// 			return \Response::json($data,$status);

// 		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $crud[3]],'data'=>$data],$status);
// 	}
// 	// INDEX

// 	public function getBegin(){
// 		/*Access*/$FRA = new FuncAccess; 
// 		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
// 		if($crud[1] == 200){	
// 			$job = \DB::SELECT("sleect id,title from job");
// 			$department = \DB::SELECT("select id, name from department where id <> 1");

// 			$data = ["job" => $job, "department" => $department];
// 			$message = "success"; $status = 200; 		
// 		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
// 	}

// 	public function checkname($id){
// 		/*Access*/$FRA = new FuncAccess; 
// 		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
// 		if($crud[1] == 200){	
// 			if(isset($id)){
// 				$name = \Input::get('name');
// 				$employee_id = \DB::SELECT("select employee_id,concat(first_name,' ',middle_name,' ',last_name)as name from emp
//                                             where first_name like '$name' or middle_name like '$name' or last_name like '$name' ");
// 				$arr = [];
// 				foreach ($employee_id as $key => $value) {
// 					$emp_id = $value->employee_id;
// 					$supervisor = \DB::SELECT("select id,employee_id from emp_supervisor where employee_id = '$id' and supervisor =  $emp_id");
// 					if($supervisor != null){
// 						$arr =  $employee_id[$key];
// 					}
// 				}
// 			}
// 		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);	
// 	} 


// 	public function index() 
// 	{
// 		/*Access*/$FRA = new FuncAccess; 
// 		$crud = $FRA->Access(\Request::all(),$this->formx,'read');
// 		if($crud[1] == 200){
// 			$perm = NULL;
// 			/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; /*Model*/ $model = new EmployeeInfo_Model;
			
// 			$expat = $this->form['expat']; $local = $this->form['local'];
// 			$getReq = $model->getReq($REQ);

// 			/*PARAM*/ $param = $this->param_valid($getReq['param']); $input['inc'] = $param['inc']; $input['lc'] = $param['lc'];

// 			$access =  $FuncAccess->Access($REQ,$expat,'read');
// 			if($access[1] == 200){ 
// 				$access2 = $FuncAccess->Access($REQ,$local,'read');
// 				if($access2[1] == 200){
// 					$perm = 'ExpatLocal';
// 				}else{ 
// 					$perm = 'Expat'; 
// 				}
// 			}elseif( $FuncAccess->Access($REQ,$local,'read')[1] == 200 ){
// 			 	$perm = 'Local';
// 			}else{ $perm  = NULL; }
			
// 			if($perm != NULL){
// 				$check_action = $FuncAccess->check_role($getReq['roleBase'],$getReq['employee_id'],$this->add);
// 				$type = ['roleBase'		=>	$getReq['roleBase'],
// 						'employee_id' 	=>	$getReq['employee_id'],
// 						'local_it'		=>	$getReq['typeLocal'],
// 						'action_create'	=>	$check_action['create']
// 						];
// 				//$getRecord = $model->ListOrderByLocal($type,$input);
				
			
// 				$getRecord =  $model->Lists($type,$input,$perm,$access[3]);
				
// 				$message = $getRecord['message']; $status = $getRecord['status']; $data = $getRecord['data'];
// 			}
// 		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
// 	}
// 	// SEARCH FILTER
// 	public function search_filter(){
// 		/*Function*/ $REQ = \Request::all(); $FuncAccess = new FuncAccess; $access = $FuncAccess->AccessPersonal(['Personal'=>NULL,'Request'=>$REQ],$this->form,1);
// 		/*Model*/ $model = new EmployeeInfo_Model;
// 		return 1;
// 		if($access[1] = 200){
// 			$getReq = $model->getReq($REQ);
// 			/*PARAM*/ $param = $this->param_valid($getReq['param']); $input['inc'] = $param['inc']; $input['lc'] = $param['lc'];
			
// 			$check_action = $FuncAccess->check_role($getReq['roleBase'],$getReq['employee_id'],$this->add);
// 			$type = ['roleBase'		=>	$getReq['roleBase'],
// 					'employee_id' 	=>	$getReq['employee_id'],
// 					'local_it'		=>	$getReq['typeLocal'],
// 					'action_create'	=>	$check_action['create']
// 					];
// 			$getRecord = $model->ListOrderByLocal($type,$input);
// 			$message = $getRecord['message']; $status = $getRecord['status']; $data = $getRecord['data'];

// 		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);	
// 	}
// 	/* EDIT */
// 	public function edit($id){ 
// 		$message = 'Page Not Found.'; $status = 404; $data=NULL;
// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
// 	}
}
