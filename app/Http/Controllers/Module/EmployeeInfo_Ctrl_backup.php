<?php namespace Larasite\Http\Controllers\Module;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Larasite\Model\EmployeeInfo_Model;
use Larasite\Privilege;
use Illuminate\Http\Request;

class EmployeeInfo_Ctrl extends Controller {

var $form_expat = 17;
var $form_local = 18; 
	/**
	 * Display a listing of the resource.
	 *PAGE ADMIN.EMPLOYEES LIST INFO
	 *FORM = 17 (EXPAT) & 18(LOCAL)
	 *Role = 1 ***Testing Postman
	 *MODEL EmployeeInfo_Model
	 * @return Response
	 */
	


public function FecthData(){
	$model = new EmployeeInfo_Model;
	$data = $model->FecthData();
	return \Response::json($data);
}

// SEARCH DOMAIN
	public function Search(){
		$get_role = $this->check();	$model = new EmployeeInfo_Model; // SET MODEL
		$r = \Request::all();

		if($r['key'] && $r['search'] != null && $r['search'] != 'undefined'){
		$decode = base64_decode($r['key']);
		$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
		$name = $r['search'];
		}else{ return \Response::json('Access Denied',500); }
		
		if(isset($get_role['data'])){ // CHECK KEY
				$access = $this->check_type2($get_role['data'],1); // GET ACCESS
				if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS
					$show = \DB::select("SELECT employee_id , concat(first_name,' ',if(middle_name is null or middle_name = '',' ',middle_name),' ',last_name) as name from emp where first_name LIKE '$name%' and employee_id != '$id' and ad_username != ''" );
						if($show){
							$data=$show; $status=200;
						}else{ $data=[['name'=>'Data not found']]; $status=200;}
				}else{ $message=$access['message'];	$status=$access['status']; $data=null; }
		}
		else{ $message = $get_role['message']; $status=401; $data=null;}
		return \Response::json($data,$status);
	}// END INDEX #########################################################################################
// END SEARCH DOMAIN

	public function index() // GET FIX***
	{
		$get_role = $this->check();
		
		if($get_role['data']['role']){ // CHECK KEY
				//return $get_role;
				$access = $this->check_type2($get_role['data']['employee_id'],$get_role['data']['role'],1); // GET ACCESS
				
				if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS
					$type = $this->check_local($get_role['data']['role'],$get_role['data']['employee_id']); // GET LOCAL
					
					$model = new EmployeeInfo_Model; // SET MODEL
					if($type['local_it'] == 1){
						$result = $model->ReadEmpInfoExpat(1,$type['employee_id'],null); // GET RESULT MODEL
						return $result;
						if(isset($result)){ $data = $result; $message='View data'; $status=200; } 
						else{ $data = null; $message='Data not found'; $status=404;	}
					}
					elseif($type['local_it'] == 2){
						$result = $model->ReadEmpInfoLocal(1,$type['employee_id'],null); // GET RESULT MODEL
						return $result;
						if(isset($result)){ $data = $result; $message='View data'; $status=200; } 
						else{ $data = null; $message='Data not found'; $status=404;	}
					}
					elseif($type['local_it'] == 3) {
						$result = $model->ReadEmpInfoLocalit(1,$type['employee_id'],null); // GET RESULT MODEL
						if(isset($result)){ $data = $result; $message='View data'; $status=200; } 
						else{ $data = null; $message='Data not found'; $status=404;	}
					}
					else{
						$message='Local it not set.'; $status=404; $data=null;
					}
				}else{ $message=$access['message'];	$status=$access['status']; $data=null; }
		}
		else{ // for not access or error
			$message = $get_role['message']; $status=401; $data=null;
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	/* EDIT ############################################################################################
	* PARAM $id
	* param = 1(ALL), 2(Sort)
	*/
	public function edit($id) // GET FIX***
	{
		$get_role = $this->check();
		if(isset($get_role['data'])){ // CHECK KEY
				$access = $this->check_type2($get_role['data'],1); // GET ACCESS
				if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS
					$type = $this->check_local($get_role['data']); // GET LOCAL
					$model = new EmployeeInfo_Model; // SET MODEL
					if($type['local_it'] == 1){
						$result = $model->ReadEmpInfoExpat(2,$type['employee_id'],$id); // GET RESULT MODEL
						if(isset($result)){ $data = $result; $message='View data'; $status=200; } 
						else{ $data = null; $message='Data not found'; $status=404;	}
					}
					elseif($type['local_it'] == 2){
						$result = $model->ReadEmpInfoLocal(2,$type['employee_id'],$id); // GET RESULT MODEL
						if(isset($result)){ $data = $result; $message='View data'; $status=200; } 
						else{ $data = null; $message='Data not found'; $status=404;	}
					}
					elseif($type['local_it'] == 3) {
						$result = $model->ReadEmpInfoLocalit(2,$type['employee_id'],$id); // GET RESULT MODEL
						if(isset($result)){ $data = $result; $message='View data'; $status=200; } 
						else{ $data = null; $message='Data not found'; $status=404;	}
					}
					else{
						$message='Local it not set.'; $status=404; $data=null;
					}
				}else{ $message=$access['message'];	$status=$access['status']; $data=null; }
		}
		else{ // for not access or error
			$message = $get_role['message']; $status=401; $data=null;
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}// END EDIT ##############################################################################################





	public function expat($role) // GET FIX***
	{
		$data_model = new EmployeeInfo_Model;
		$data_role = $this->check_role($role,17);
		//$data =['read'];
		if($data_role['read']){
			return $data_model->ReadEmpInfoExpat('All',null);
		}
		else{
			return null ;				
		}
	}
	public function local($role) // GET FIX***
	{
		$data_model = new EmployeeInfo_Model;
		$data_role = $this->check_role(1,18);
		if($data_role['read']){
			return$data_model->ReadEmpInfoLocal('All',null);
		}
		else{
			return null ;				
		}
	}

	public function local2($role) // GET FIX***
	{
		$check_type = $this->check_type($role);
		
		if($check_type){
			$data_perms = $this->check_role($role,17);
			if($data_perms && $data_perms['read']){
				$data_model = new EmployeeInfo_Model;
				return \Response::json( ['data'=>$data_model->ReadEmpInfo('All',null)] );
			}
		}
		else{
			$data_perms = $this->check_role($this->role,18);				
		}

		if($data_perms['read'] && $check){
			$data_model = new EmployeeInfo_Model;
			return \Response::json( ['data'=>$data_model->ReadEmpListInfo('All',null)] );
		}
		else{
			return \Response::json(['name'=>$data_perms['role_name'],'access'=>'Unauthorize','status'=>403]);
		}
	}
	/* ############################################
	*
	* CHECK TYPE ID EMPLOYEE EXPAT OR LOCAl 
	*/
	public function check_type($role,$show) // CHECK ROLE FIX***
	{
		//$token = 'dsd';//\Cookie::get('XSRF-TOKEN');
		$data_model = new EmployeeInfo_Model;
		if(isset($role) && $show == 'all'){

			$data_type = \DB::select("select a.id_employee_type FROM employees a,ldap b ,emp c WHERE a.employee_id = c.employee_id and b.role_id = '$role'");
			foreach ($data_type as $key) {
				$type['type'] = $key->id_employee_type;
			}
			
			if($type['type'] == 1){
				$message = 'View data'; $status = 200;
				$result = $data_model->ReadEmpInfoLocal('All',null);
			} // if type local, expat, locit
			elseif($type['type'] == 2){
				$message = 'View data'; $status =200;
				$result = $data_model->ReadEmpInfoLocal('All',null);
			}
			elseif($type['type'] == 3){
				$message = 'View data'; $data = 200;
				$result = $data_model->ReadEmpInfoLocal('All',null);
			}else{
				$message = 'Unauthorize'; $status = 200;
				$result =  null;
			}
			return ['header'=>['message'=>$message,'status'=>$status],'data'=>$result];
		} // if role and show all
		elseif(isset($role) && $show == 'sort'){
			if($type['type'] == 1){
				$message = 'View data'; $status = 200;
				$result = $data_model->ReadEmpInfoLocal('Sort',null);
			} // if type local, expat, locit
			elseif($type['type'] == 2){
				$message = 'View data'; $status =200;
				$result = $data_model->ReadEmpInfoLocal('Sort',null);
			}
			elseif($type['type'] == 3){
				$message = 'View data'; $data = 200;
				$result = $data_model->ReadEmpInfoLocal('Sort',null);
			}else{
				$message = 'Unauthorize'; $status = 200;
				$result =  null;
			}
			return ['header'=>['message'=>$message,'status'=>$status],'data'=>$result];
		} // if role found and show sort
		else{
			$msg['warning'] = 'Unauthorize';
			$msg['status'] = 404;
			return $msg; 
		} // else error access
	}// ###########################################################################

	/* ############################################
	*
	* CHECK ROLE OR PERMISSION EMPLOYEE 
	*/
	public function check_role($role,$employee_id,$form) // CHECK ROLE FIX***
	{
		$getModel = new Privilege;
		if(isset($role,$form)){
			return $getModel->check_role_personal2($role,$employee_id,$form);
		}
		else{
			$msg['warning'] = 'Unauthorize';
			$msg['status'] = 404;
			return $msg; 
		}
	}

	/** ################################################
	 * Show the form for creating a new resource.
	 * KEY ENCRYPT DENGAN BASE64(TOKEN-ROLE)
	 * @return Response
	 */
	public function create() // SLICE TOKEN BOTH ROLE FIX***
	{
		$encrypt = base64_encode($id."-hr.admin");
		$decode = base64_decode($encrypt);
		$str = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
		return $str;
	}

	/* CHECK LOCAL IT ###################################################################################
	* PARAM = ROLE
	*/
	private function check_local($role,$employee_id){
		if(isset($role)){
			$model = new EmployeeInfo_Model;
			$get = $model->Get_Type($role,$employee_id);
			foreach ($get as $key) {
				$type['local_it'] = $key->local_it;
				$type['employee_id'] = $key->employee_id;
			}
		}else{ $type = null; }
		return $type;
	} // END CHECK LOCAL IT ############################################################################

	// /* ######################################################
	// * CHECK KEY YANG DIKIRIM DI ROUTE API
	// * KEY DI ENCRYPT DENGAN BASE64(TOKEN-ROLE)
	// * IF TRUE ACCESS DENIED
	// */
	// 	public function check(){
	// 	$getModel = new Privilege;
	// 	$req = \Request::all();
	// 	if(isset($req['key'])){ // check isset $req

	// 		$q = $getModel->session_key($req['key']);
	// 		if(isset($q) != null){ //check isset key in session
				 
	// 					 $r = array();
	// 					 foreach ($q as $keys) {
	// 					 	$r['key'] = $keys->session_key;
	// 					 } // end for store role to array.

	// 					 if(isset($r['key'])){
	// 						$decode = base64_decode($r['key']);
	// 						$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
	// 						$key = substr($decode,0,strpos($decode,'-'));
	// 						//return $employee_id;
	// 						 $q2 = $getModel->session_role($employee_id);
	// 						 foreach ($q2 as $keys) {
	// 						 	$r['employee_id'] = $keys->employee_id;
	// 						 	$r['role_id'] = $keys->role_id;
	// 						 } // end for store role to array.
	// 					 }
	// 					 else{
	// 					 	return ['message'=>'ACCESS DENIED','data'=>null];
	// 					 }
						

	// 			if($r['employee_id'] == $employee_id){ // if type decode or encode
	// 				$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
	// 				foreach ($datas as $key) {
	// 					$role['role'] = $key->role_id;
	// 				}
	// 				if(isset($role['role'])){
	// 					//return $role['role'];
	// 					$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
	// 					$msg = 'ACCESS GRANTED !';	
	// 				}else{
	// 					$msg = 'ACCESS DENIED!';
	// 					$data = null;
	// 				}
	// 			}
	// 			else{
	// 				$msg = 'ACCESS DENIED!';
	// 				$data = null;	
	// 			}
	// 		}
	// 		else{
	// 			$msg = 'ACCESS DENIED !';
	// 			$data = null;	
	// 		}//end if check

	// 	}// end if isset $req
	// 	else{ // else message error if null
	// 		$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
	// 		$data = null;
	// 	}
		
	// 	return ['message'=>$msg,'data'=>$data];
	// }

	// /* CHECK TYPE AND ACCESS ########################################################################
	// * CHECK TYPE ID EMPLOYEE EXPAT OR LOCAl OR LOCAL_IT
	// * ACTION = 1(CREATE & READ), 2(EDIT & UPDATE), 3(DESTROY)
	// */
	// private function check_type2($role,$action) // CHECK ROLE FIX***
	// {
	// 	$model = new EmployeeInfo_Model;
	// 	$status_error = 401;
	// 	$not_access = 'Unauthorized';
	// 	$type = $this->check_local($role);

	// 	if(isset($type)){			
	// 		/* ########
	// 		* FOR EXPAT
	// 		*/
	// 		if($type['local_it'] == 1){
	// 			$check = $this->check_role($role,$this->form_expat);
	// 			if(isset($check['read']) && isset($action) == 1){ // CREATE
	// 				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
	// 			}
	// 			elseif(isset($check['read']) && isset($action) == 2){ // UPDATE
	// 				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
	// 			}
	// 			elseif(isset($check['read']) && isset($action) == 3){ // DESTROY
	// 				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
	// 			}
	// 			else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
	// 		} // if type local, expat, locit

	// 		/* ########
	// 		* FOR LOCAL
	// 		*/
	// 		elseif($type['local_it'] == 2){
	// 			$check = $this->check_role($role,$this->form_local);
	// 			if(isset($check['read']) && isset($action) == 1){ // CREATE
	// 				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
	// 			}
	// 			elseif(isset($check['read']) && isset($action) == 2){ // UPDATE
	// 				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
	// 			}
	// 			elseif(isset($check['read']) && isset($action) == 3){ // DESTROY
	// 				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
	// 			}
	// 			else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
	// 		}

	// 		/* ###########
	// 		* FOR LOCAL_IT
	// 		*/
	// 		elseif($type['local_it'] == 3){
	// 			$check = $this->check_role($role,$this->form_localit);
	// 			if(isset($check['read']) && isset($action) == 1){ // CREATE
	// 				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
	// 			}
	// 			elseif(isset($check['read']) && isset($action) == 2){ // UPDATE
	// 				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
	// 			}
	// 			elseif(isset($check['read']) && isset($action) == 3){ // DESTROY
	// 				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
	// 			}
	// 			else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
	// 		}else{
	// 			$message = $not_access; $status = $status_error;
	// 		}
	// 	} 
	// 	else{
	// 		$message = $not_access; $status = $status_error; // ROLE NOT FOUND
	// 	}
	// 	return ['message'=>$message,'status'=>$status];
	// } // CHECK TYPE AND ACCESS ####################################################################

		/* CHECK TYPE AND ACCESS ########################################################################
	* CHECK TYPE ID EMPLOYEE EXPAT OR LOCAl OR LOCAL_IT
	* ACTION = 1(CREATE & READ), 2(EDIT & UPDATE), 3(DESTROY)
	*/
	private function check_type2($employee_id,$role,$action) // CHECK ROLE FIX***
	{
		$model = new EmployeeInfo_Model;
		$status_error = 403;
		$not_access = 'Unauthorized';
		$type['data'] = $this->check_local($role,$employee_id);
		$type['local_it'] = $type['data']['local_it'];
		if($type['local_it']){

			/* ########
			* FOR EXPAT
			*/
			if($type['local_it'] == 1){
				$check = $this->check_role($role,$employee_id,$this->form_expat);
				
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error;}	// ERROR ACCESS
			} // if type local, expat, locit

			/* ########
			* FOR LOCAL
			*/
			elseif($type['local_it'] == 2){
				
				$check = $this->check_role($role,$employee_id,$this->form_local);
				
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
			}

			/* ###########
			* FOR LOCAL_IT
			*/
			elseif($type['local_it'] == 3){
				$check = $this->check_role($role,$this->form_local);
				
				if( $check['read'] != 0 && $check['create'] != 0 && isset($check['read'],$check['create']) && isset($action) == 1){ // CREATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 2){ // UPDATE
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 3){ // DESTROY
					$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
				}
				else{ $message = $not_access; $status=$status_error; }	// ERROR ACCESS
			}else{
				$message = $not_access; $status = $status_error;
			}
		} 
		else{
			$message = $not_access; $status = $status_error; // ROLE NOT FOUND
		}
		return ['message'=>$message,'status'=>$status];
	} // CHECK TYPE AND ACCESS ####################################################################


	// /* CHECK ROLE ###################################################################################
	// * PARAM = ROLE AND FORM
	// * CHECK USER ROLE YANG ACCESS SYSTEM
	// * MENGHASILKAN DATA ACCESS DAN ID
	// */
	// private function check_role($role,$form) // CHECK ROLE FIX***
	// {
	// 	$getModel = new Privilege;
	// 	if(isset($role, $form)){
	// 		return $getModel->check_role_personal($role,$form);
	// 	}
	// 	else{
	// 		$msg['warning'] = 'Unauthorize';
	// 		$msg['status'] = 404;
	// 		return $msg; 
	// 	}
	// }// END CHECK ROLE #####################################################################

// CHECK ACCESS ###############################################################################
	public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(isset($q) != null){ //check isset key in session
				 
						 $r = array();
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } // end for store role to array.

						 if(isset($r['key'])){
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							//return $employee_id;
							 $q2 = $getModel->session_role($employee_id);
							 foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 } // end for store role to array.
						 }
						 else{
						 	return ['message'=>'ACCESS DENIED','data'=>null];
						 }
						

				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
						$msg = 'ACCESS GRANTED !';	
					}else{
						$msg = 'ACCESS DENIED!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED!';
					$data = null;	
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;	
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		return ['message'=>$msg,'data'=>$data];
	}
// END CHECK ACCESS ############################################################################
}
