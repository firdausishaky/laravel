<?php namespace Larasite\Http\Controllers\Module;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*Model*/
use Larasite\Model\Master\PIM\ImmigrationIssuer_Model;
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;

class ImmigrationIssuer_Ctrl extends Controller {

protected $form = 47; 

	private function checkID($id)
	{
		$rule = ['undefined',NULL,''];
		if(in_array($id,$rule)){ $message = 'ID Undefined.'; $status = 500; $data = null; }
		else{ $message = 'OK.'; $status = 200; $data = NULL; }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
	private function set_valid()
	{ 
		$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
				'text_only'=>'Regex:/^[A-Za\-! ,\'\"\/@\.:\(\)]+$/',
				'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
		$rule =  ['title'=>"required|".$reg['text_num']."|min:2"];
		$valid =  \Validator::make(\Input::all(),$rule);
		if($valid->fails()){ $message = 'Required Input Failed.';  $status = 500; $data = NULL;}
		else{ $message = 'OK.'; $status = 200; $data = NULL; }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
	private function set_input(){ return ['title'=>\Input::get('title')];	 }
	
	// INDEX
	public function index() // GET FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new ImmigrationIssuer_Model;
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$data = []; $message = 'Unauthorized'; $status = 200;
			}else{
				$read = $model->ReadImmigrationIssuer('All',null);
				$message = $read['message']; $status=$read['status']; $data=$read['data'];
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}

	// STORE
	public function store() // POST FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/ $model = new ImmigrationIssuer_Model;
		if($access[1] == 200){
			$valid = $this->set_valid();
			if($valid['status'] == 500){ $message = $valid['message']; $status=$valid['status']; $data = $valid['data']; }
			else{
				$store = $model->StoreImmigrationIssuer($this->set_input());
				$message = $store['message']; $status=$store['status']; $data = $store['data'];	
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// SHOW
	public function show($id) // GET FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new ImmigrationIssuer_Model;
		if($access[1] == 200){
			$check = $this->checkID($id);
			if($check['status'] == 500){  $message = $check['message']; $status = $check['status']; $data = NULL;}
			else{ $message = 'Page Not Found.'; $status = 404; $data = NULL; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	// EDIT
	public function edit($id) //GET / HEAD FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new ImmigrationIssuer_Model;
		if($access[1] == 200){
			$check = $this->checkID($id);
			if($check['status'] == 500){  $message = $check['message']; $status = $check['status']; $data = NULL;}
			else{ $message = 'Page Not Found.'; $status = 404; $data = NULL; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// UPDATE
	public function update($id) // PUT FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new ImmigrationIssuer_Model;
		if($access[1] == 200){
			$check = $this->checkID($id);
			if($check['status'] == 500){  $message = $check['message']; $status = $check['status']; $data = NULL;}
			else{
				$valid = $this->set_valid();
				if($valid['status'] == 500){ $message = $valid['message']; $status=$valid['status']; $data = $valid['data']; }
				else{
					$store = $model->UpdateImmigrationIssuer($id,$this->set_input());
					$message = $store['message']; $status=$store['status']; $data = $store['data'];	
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}// END UPDATE

	// DELETE
	public function destroy($id) // DELETE FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete'); /*Model*/ $model = new ImmigrationIssuer_Model;
		if($access[1] == 200){
			$check = $this->checkID($id);
			if($check['status'] == 500){  $message = $check['message']; $status = $check['status']; $data = NULL;}
			else{
				$check = $this->checkID($id);
				if($check['status'] == 500){ $message = $check['message']; $status=$check['status']; $data = $check['data']; }
				else{
					$getid = explode(",", $id);
					$del = $model->DestroyImmigrationIssuer($getid);

					$message = $del['message']; $status = $del['status']; $data = NULL;
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);			
	} // END DELETE


// CHECK ACCESS ###############################################################################
	public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(isset($q) != null){ //check isset key in session
				 
						 $r = array();
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } // end for store role to array.

						 if(isset($r['key'])){
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							//return $employee_id;
							 $q2 = $getModel->session_role($employee_id);
							 foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 } // end for store role to array.
						 }
						 else{
						 	return ['message'=>'ACCESS DENIED','data'=>null];
						 }
						

				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
						$msg = 'ACCESS GRANTED !';	
					}else{
						$msg = 'ACCESS DENIED!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED!';
					$data = null;	
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;	
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		return ['message'=>$msg,'data'=>$data];
	}
// END CHECK ACCESS ############################################################################

}
