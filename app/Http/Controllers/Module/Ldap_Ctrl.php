<?php namespace Larasite\Http\Controllers\Module;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use adLDAP\adLDAP;

use Larasite\Model\Ldap_Model;
use Larasite\Privilege;
use Illuminate\Http\Request;

class Ldap_Ctrl extends Controller {

var $form_id = 12;
	/**
	 * Display a listing of the resource.
	 *PAGE 201-Module.LDAP_SETTING
	 *ID = 47
	 *MODEL Immigration_Model
	 * @return Response
	 */

	/** ###################################################################################################
	* TUGAS : MEMBERIKAN DATA UNTUK TABLE PADA SETIAP PAGE
	*/
	public function index() // GET FIX***
	{

		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);

		if($get_role['data']['role']){
			if($data_role['read']!=0 && $data_role['read']!=null){
				$model = new Ldap_Model;
				$datas = $model->Read_Ldap();
				if($datas){
					// $data = $datas;
					// $message = 'Show Records Data.';
					// $status = 200;
					$file = storage_path()."/module_config.json";
					$data = file_get_contents($file, 0, null, null);
					$json = strip_tags(str_replace("jQuery.fs['scoreboard'].data =","",$data));
					$json_output = json_decode($json);

					//get data
					foreach ($json_output as $key) {
						$a['ldap_domain'] = $key->ldap_domain;
						$a['ldap_username'] = $key->ldap_username;
						$a['ldap_password'] = $key->ldap_password;
						$a['ldap_account_suffix'] = $key->ldap_account_suffix;
						$a['ldap_basedn'] = $key->ldap_basedn;
						$a['ldap_port'] = $key->ldap_port;
						$a['ldaps_port'] = $key->ldaps_port;
						$a['ldap_protocol'] = $key->ldap_protocol;
						$a['ldap_ssl'] = $key->ldap_ssl;
						$a['ldap_tls'] = $key->ldap_tls;
						$a['ldap_sso'] = $key->ldap_sso;
						$a['ldap_recursive_group'] = $key->ldap_recursive_group;
						$a['ldap_primary_group'] = $key->ldap_primary_group;
					}
					// SETUP
					try{
						$ldap = new adLDAP(array('base_dn'=>$a['ldap_basedn'],'account_suffix'=>$a['ldap_account_suffix']));

						$ldap->setAdminUsername($a['ldap_username']);
						$ldap->setAdminPassword($a['ldap_password']);
						$ldap->setPort($a['ldap_port']);
						$ldap->setDomainControllers(array($a['ldap_domain']));
						$ldap->setAccountSuffix($a['ldap_account_suffix']);
						$ldap->setRealPrimaryGroup($a['ldap_primary_group']);
						$ldap->setRecursiveGroups($a['ldap_recursive_group']);
						$ldap->setUseSSL($a['ldap_ssl']);
						$ldap->setUseSSO($a['ldap_sso']);
						$ldap->setUseTLS($a['ldap_tls']);

						if($ldap->authenticate("hr.admin","asdf1234*")){
							return 1;
						}
					}catch(adLDAPException $e){
						return $e;
					}
				}else{
					$message = 'Empty Data Records.';
					$status = 200;
					$data = $datas;
				}
			}
			else{
				$message = 'Unauthorized';
				$status = 401;
				$data = null;
			}
			return \Response::json(['header'=>['message'=>$message],'data'=>$data], $status);
		}
		else{
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]], 403);
		}
	}

	/** ###################################################################################################
	* FUNGSI CHECK PERMISSION ROLE
	* PARAMETER ROLE DAN ID FORM
	*/
	public function check_role($role,$employee_id,$form) // CHECK ROLE FIX***
	{
		$getModel = new Privilege;
		if(isset($role, $form)){
			return $getModel->check_role_personal2($role,$employee_id,$form);
		}
		else{
			$msg['warning'] = 'Unauthorize';
			$msg['status'] = 404;
			return $msg;
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() // SLICE TOKEN BOTH ROLE FIX***
	{
		$encrypt = base64_encode($id."-hr.admin");
		$decode = base64_decode($encrypt);
		$str = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
		return $str;
	}
	public function store_upload(){
		$file = \Input::file('file');
		if(!$file){
			return $this->store();
		}
		else{
		}
	}

	/** ###################################################################################################
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() // POST FIX***
	{
		$model = new ImmigrationIssuer_Model;
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);

		// #CHECK KEY
		if($get_role['data']['role']){
			// #CHECK PERMISSION ROLE
			if($data_role['create']!=0 && $data_role['create'] !=null){
				$title = \Input::get('title'); $descript = \Input::get('descript');
				// # CHECK INPUT NULL OR NOT
				if(isset($title)){
					// # CHECK STORE
					if($model->StoreImmigrationIssuer($title,$descript)){
						$status = 200;
						$message = 'Store Data Successfully.';
						$getid = \DB::select("Select * from immigration_issuer  order by id desc limit 1");
						$data = $getid[0];
					}
					else{
						$status = 400;
						$message = 'Store Not Successfully.';
						$data = null;
					}

				}
				else{
					$status = 406;
					$message = 'Failed Store Data.';
					$data = null;
				}
			}
			else{
				$status = 401;
				$message = 'Unauthorized.';
				$data  = null;
			}
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
		else{
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403);
		}
	}

	/** ###################################################################################################
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 * PARAMETER ID
	 */
	public function show($id) // GET FIX***
	{
		/*
		* Show data for table SystemUser
		* Model/Model_UserManagement.php
		*/
		$data_model = new ImmigrationIssuer_Model;
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);

		if($get_role['data']['role']){
			if($data_role['read']!=0 && $data_role['read']!=null){
				if($data_model->ReadImmigrationIssuer('Sort',$id)){
					$status = 200;
					$data = $data_model->ReadImmigrationIssuer('Sort',$id)[0];
					$message = 'Show Data Selected.';
				}
				else{
					$status = 404;
					$data = null;
					$message = 'Data not found';
				}
			}
			else{
				$message = 'Unauthorize';
				$data = null;
				$status = 401;
			}
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
		else{
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403);
		}
	}

	/** ###################################################################################################
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) //GET / HEAD FIX**
	{
		$model = new ImmigrationIssuer_Model;
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data'],$this->form_id);

		if(isset($get_role['data'])){
			if(isset($data_role['create'])){

				if($model->ReadImmigrationIssuer('Sort',$id)){
					$status = 200;
					$data = $model->ReadImmigrationIssuer('Sort',$id)[0];
					$message = 'Show Data Selected.';
				}
				else{
					$status = 404;
					$data = null;
					$message = 'Data not found';
				}
			}
			else{
				$message = 'Unauthorize';
				$data = null;
				$status = 401;
			}
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
		else{
			$msg = 'Unauthorize';
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403);
		}

	}

	/**###################################################################################################
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 * UPDATE FIX
	 */
	public function update($id) // PUT FIX***
	{
		$model = new ImmigrationIssuer_Model;
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);

		// #CHECK  KEY API
		if($get_role['data']['role']){
			// # CHECK PERMISSION PAGE
			if($data_role['update']!=null && $data_role['update']!=0){
				$title = \Input::get('title'); $descript = \Input::get('descript');
				// # CHECK INPUT EMPTY OR NOT
				if(isset($title)){
					//C #CHECK MODEL
					if($model->UpdateImmigrationIssuer($id,$title,$descript)){
						$message = 'Update Successfully.';
						$status = 200;
					}
					else{
						$message = 'Data not found';
						$status = 406;
					}
				}
				else{
					$message = 'Input Empty';
					$status = 406;
				}
			}
			else{
				$message = 'Unauthorize' ;
				$status = 401;
			}
			return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
		}
		else{
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403);
		}
	}// END UPDATE


	/** ###################################################################################################
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) // DELETE FIX**
	{

		$model = new ImmigrationIssuer_Model;
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);
		$getid = explode(",", $id);
		if($get_role['data']['role']){
			if($data_role['delete']!=0 && $data_role['delete']!=null){
				if($id == 'undefined'){
					return \Response::json(['header'=>['message'=>'Undefined','status'=>500]],500);
				}

				if($model->DestroyImmigrationIssuer($getid)){
					$message = 'Destroy Successfully.';
					$status = 200;
				}
				else{
					$message = 'Data not found.';
					$status = 406;
				}
			}
			else{
				$message = 'Unauthorized' ;
				$status = 401;
			}
			return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
		}
		else{
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403);
		}
	} // END DELETE


// CHECK ACCESS ###############################################################################
	public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(isset($q) != null){ //check isset key in session

						 $r = array();
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } // end for store role to array.

						 if(isset($r['key'])){
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							//return $employee_id;
							 $q2 = $getModel->session_role($employee_id);
							 foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 } // end for store role to array.
						 }
						 else{
						 	return ['message'=>'ACCESS DENIED','data'=>null];
						 }


				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
						$msg = 'ACCESS GRANTED !';
					}else{
						$msg = 'ACCESS DENIED!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED!';
					$data = null;
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		return ['message'=>$msg,'data'=>$data];
	}
// END CHECK ACCESS ############################################################################

}
