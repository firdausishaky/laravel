<?php namespace Larasite\Http\Controllers\Module;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
/*Model*/
use Larasite\Privilege;
use Larasite\Model\Master\PIM\TerminationReason_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;

class TerminationReasons_Ctrl extends Controller {

protected $form = 15;

	private function check_id($id)
	{
		$rule= ['undefined',NULL,''];
		if(in_array($id,$rule)){ $data = ['message'=>'ID undefined.','status'=>500,'data'=>NULL]; }
		else{ $data = ['message'=>'ID OK.','status'=>200,'data'=>NULL]; }
		return $data;
	}
	private function set_valid()
	{
		$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-_! ,\'\"\/@\.:\(\)]+$/',
			'text_only'=>'Regex:/^[A-Za\-! ,\'\"\/@\.:\(\)]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
		$rule =  ['title'=>'required|'.$reg['text_num']];
		$valid =  \Validator::make(\Input::all(),$rule); return $valid;
	}
	private function set_input(){ return ['title'=>\Input::get('title')]; }

	public function index() // GET FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new TerminationReason_Model;
		if($access[1] == 200){
				if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$data = []; $message = 'Unauthorized'; $status = 200;
				}else{
					$data = TerminationReason_Model::get();
					if($data){
						$datas = $data; $message = 'Termination Reasons : Show Records Data.'; $status = 200;
					}else{ $datas = $data; $message = 'Termination Reasons : Empty Records Data.'; $status = 200; }
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}
	// STORE
	public function store() // POST FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/ $model = new TerminationReason_Model;
		if($access[1] == 200 and $access[3]['create'] != 0){
			$valid = $this->set_valid();
			if($valid->fails()){ $message='Required Input Failed.'; $status = 500; $data=NULL;}
			else{
				$store = $model->StoreTerminationReasons($this->set_input());
				$message = $store['message']; $status=$store['status']; $data = $store['data'];
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	// UPDATE
	public function update($id) // PUT FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new TerminationReason_Model;
		if($access[1] == 200){
			$check_id = $this->check_id($id);
			if($check_id['status'] == 500){$message = $check_id['message']; $status = $check_id['status']; $data = NULL;}
			else{
				$valid = $this->set_valid();
				if($valid->fails()){ $message='Required Input Failed.'; $status = 500; $data=NULL;}
				else{
					$update = $model->UpdateTerminationReasons($id,$this->set_input());
					$message = $update['message']; $status=$update['status']; $data = $update['data'];
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}// END UPDATE

	// Destroy
	public function destroy($id) // DELETE FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new TerminationReason_Model;
		if($access[1] == 200){
			$check_id = $this->check_id($id);
			if($check_id['status'] == 500){$message = $check_id['message']; $status = $check_id['status']; $data = NULL;}
			else{
				$getid = explode(",", $id);
				$del=$model->DestroyTerminationReasons($getid);
				$message = $del['message']; $status = $del['status']; $data = NULL;
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	} // END DELETE
}
