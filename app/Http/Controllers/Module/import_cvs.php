<?php namespace Larasite\Http\Controllers\Module;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
class import_cvs extends Controller {


protected  $form = 14;

protected $fill = [		'first_name',
						'middle_name',
						'last_name',
						'ad_username',
						'local_it',
						'employee_id',
						'department',
						'sss',
						'philhealth',
						'hdmf',
						'rtn',
						'mid',
						'tin',
						'gender',
						'marital_status',
						'nationality',
						'date_of_birth',
						'place_of_birth',
						'religion',
						'permanent_address',
						'permanent_city',
						'permanent_state',
						'permanent_zip',
						'permanent_country',
						'present_address',
						'present_city',
						'present_state',
						'present_zip',
						'present_country',
						'home_telephone',
						'mobile',
						'work_email',
						'personal_email'];
	
	/* Display a listing of the resource.*/
	private function import_csv($path,$filename){
	set_time_limit(0);
	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'store');
      	if($access[1] == 200){
		try{
			$csv =  $path."/".$filename;
			$pdo = \DB::connection()->getpdo();
			$format = "LOAD DATA INFILE %s INTO TABLE emp FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' ESCAPED BY '\"' LINES TERMINATED BY '\\n' IGNORE 0 LINES(`first_name`,`middle_name`,`last_name`,`ad_username`,`local_it`,`employee_id`,`department`,`sss`,`philhealth`,`hdmf`,`maiden_name`,`hmo_account`,`hmo_numb`,`rtn`,`mid`,`tin`,`gender`,`marital_status`,`nationality`,`date_of_birth`,`place_of_birth`,`religion`,`permanent_address`,`permanent_city`,`permanent_state`,`permanent_zip`,`permanent_country`,`present_address`,`present_city`,`present_state`,`present_zip`,`present_country`,`home_telephone`,`mobile`,`work_email`,`personal_email`)";
			$query = sprintf($format, $pdo->quote($csv));
			$import = $pdo->exec($query);
			return $import;	
		}catch(Exception $e){
			 return 'Caught exception: '. $e->getMessage(). "\n";
		}
		
	}
      else{
         $message = $access[0]; $status = $access[1]; $data=$access[2];
       }
       return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
       }

	private function check_nation($string){
		$r = \DB::table('nationality')->where('title','=',strtolower($string))->get(['id']);
		if($r){
			foreach ($r as $key) { $data = $key->id; }
		}else{ $data = null; }
		return $data;
	}

	private function insert_department($string){
		$r = \DB::table('department')->insert(['name'=>$string]);
		if($r){
			$get = \DB::table('department')->where(['name'=>$string])->get(['id']);
			foreach ($get as $key) { $data = $key->id; }
		}else{ $data = null; }
		return $data;
	}
	
	public function parse_csv($path,$filename)
	{
						$csv =  $path."/".$filename;
						$temp = array_map('str_getcsv', file($csv));
						array_shift($temp);
						
						$i = 0; $j=0;
						while($i < count($temp)){

							foreach ($temp[$i] as $key) {
							 	if($j == 32){ // end kolom
							 		if(strpos($key,'"')){ $key = explode('"',$key)[0]; }
							 		$data[$this->fill[$j]] = $key;
							 		$j = 0;	
							 	}else{
							 		if($j == 15){ // nationality
								 		$nation = $this->check_nation($key);
								 		if($nation){ $data[$this->fill[$j]] = $nation; }
							 		}
							 		elseif($j == 6){ // sub_unit
							 			$department = $this->insert_department($key);
								 		if($department){ $data[$this->fill[$j]] = $department; }
							 		}
							 		else{
							 			$data[$this->fill[$j]] = $key;
							 		}
							 		$j++;
							 	}
						 	}
						 	$i++;
						 	$r[$i] = $data;
						 	\DB::table('emp')->insert($data);
						}
		if(isset($r)){
			return $r;
		}else{ return null; };
	}

	public function getfile(){
		 $get_role = $this->check();
		if(isset($get_role['data']['role'])){
			 $files = 'xls_example.xlsx';
			$path = storage_path()."/".$files;
			$file = \File::get($path);
			$type = \File::mimeType($path);
			return \Response::download($path,'example.xlsx',['Content-Type'=>$type]);
		}else{
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>401]],401);
		}
	}

	/* CHECK ROLE OR PERMISSION EMPLOYEE  */
	public function check_role($role,$employee_id,$form) // CHECK ROLE FIX***
	{
		$getModel = new Privilege;
		if(isset($role,$form)){
			return $getModel->check_role_personal2($role,$employee_id,$form);
		}
		else{
			$msg['warning'] = 'Unauthorize'; $msg['status'] = 404;
			return $msg; 
		}
	}


	public function index_granted(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$message = 'Unauthorized'; $status = 200; $access = $access[3];
			}else{
				$message = 'Not View'; $status = 200; $access = $access[3];
			}
			
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
			return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access], $data = [] ],$status);
	}

	public function index()
	
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$message = 'Unauthorized'; $status = 403; $access = $access[3];
			}else{
				$message = 'Not View'; $status = 403; $access = $access[3];
			}	
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
			return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $access], $data = [] ],$status);
	}
	public function create()
	{
		$message = 'Not View'; $status = 403;
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],403);
	}

	function check_val($val){
		$val_regex = array('hdmf','tin','sss','mid','rtn','philhealth');
		$t = array_keys($val); $i = 0;
		
		while($i < count($val)){
			
			if(in_array($t[$i], $val_regex)){
				
				$d = str_replace(' ', '', $val[$t[$i]]);
				if($t[$i] == 'hdmf'){
					$d = (integer)$d;
					$d = "$d";
				}
				$val[$t[$i]] =  $d;
				if(!$d){ $val[$t[$i]] = null; }
			
			}else{
				if($t[$i] == 'employee_type'){
					$val['local_it'] = $val[$t[$i]];
					unset($val[$t[$i]]);
					if(!$val['local_it']){ $val['local_it'] = null; }
				}
				elseif(!$val[$t[$i]]){ $val[$t[$i]] = null; }
			}
			$i++;
		}
		return $val;
	}

	public function store()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/

		if($access[1] == 200){
			
			// if ($access['create'] == 0 and $access['delete'] == 0 and $access['read'] == 0 and $access['update'] == 0 ){
			// 		$message = 'Unauthorized'; $status = 403; $access = $access[3];
			// }
			$get_role = $this->check();
			if($get_role['data']['role']){
				// CHECK KEY DAN CHECK ROLE IF TRUE
				$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form);
				// CHECK PERMISSION CREATE
				if($data_role['create'] != 0 && $data_role['create']!=null){
					$data = \Input::get('employeelist');
					
					foreach ($data as $key => $value) {
						   $db = \DB::SELECT("select employee_id from emp where employee_id = $value[employee_id] ");
						   if($db != null){

						   	return \Response::json(['header'=>['message'=>'error employee id already exist '. $value['employee_id'] .' ,please make a sure data with same employee id already remove from .xls file ','status'=>500 ], 'data' => [$db] ],500);
						   }
			     
					        if($value['nationality'] != null){
					        	$db = \DB::table('nationality')->where('title',$value['nationality'])->get();
					            if($db == null){
					            		$country = $value['nationality'];
					            		$keyVal = $key+1;
					             		return \Response::json(['header'=>['message'=>'Undefined nationality '.$country.' in line data '.$keyVal,'status'=>500 ], 'data' => [] ],500);
					             }else{ $value['nationality'] = $db[0]->id; }
							}else{ $value['nationality'] = null; }
						   	
						    $val = $this->check_val($value);
						    
					        $explode = explode('-',$val['date_of_birth']);
					        if(isset($explode[0]) and isset($explode[1]) and isset($explode[2]) ){
					        	if(strlen($explode[0]) == 4){
					        		$val['date_of_birth']  =  $explode[0].'-'.$explode[1].'-'.$explode[2];		
					        	}
					        	if(strlen($explode[2]) == 4){
					        		
					        		$val['date_of_birth']  =  $explode[2].'-'.$explode[0].'-'.$explode[1];	
					        	}
					        }else{
					        	return \Response::json(['header'=>['message'=>'invalid input format date_of_birth','status'=>500 ], 'data' => [] ],500);
					        }

					        $reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,_\'\"\/@\.:\(\)]+$/',
					        	'text_persian'=>'Regex:/^[A-Za-z-ZñÑ0-9\-! ,_\'\"\/@\.:\(\)]+$/',
								'text'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
								'text3'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)\pL\pN_-]+$/u',
							    'text2'=>'Regex:/^[A-Za-z-\^ ]+$/',
							    //'date_reg' => 'Regex:/^((0?[1-9]|1[012])[-.](0?[1-9]|[12][0-9]|3[01])[-.](19|20)?[0-9]{2})*$/',
						        'num'=>'Regex:/^[0-9-\^ ]+$/',
						        'hdmf'=>'Regex:/^[0-9\- ]+$/',
							   	'hmo'=>'Regex:/^[A-Za-z0-9-\ ]+$/',
							   	'twit'=>'Regex:/^[A-Za-z0-9_ ]{1,16}$/',
							   	'test1' =>  'Regex:/^[A-Za-z0-9\d\-_\s]+$/i',
							   	'address' => 'Regex:/^[A-Za-z0-9\-!,\'\"\/@\.: &\(\)]+$/'
							];
					             $valid = \Validator::make( $val,
					             			          ['first_name' => $reg['text_persian'],
					             			          'middle_name' => $reg['text_persian'],
					             			          'last_name' => $reg['text_persian'],
					             			          'ad_username' => $reg['text'],
					             			          'biometric' => 'numeric',
					             			          'employee_type' => 'numeric',
					             			          'employee_id' =>  $reg['text_num'],
					             			          'maiden' =>  $reg['text'],
					             			          'hdmf' =>  $reg['hdmf'],
					             			          'sss' => $reg['num'],
					             			          'rtn' =>  $reg['num'],
					             			          'philhealth' =>  $reg['num'],
					             			          'mid' =>  $reg['num'],
					             			          'tin' =>  $reg['hmo'],
					             			           'hmo_account' =>$reg['text_num'],
					             			           'hmo_numb' =>  $reg['text_num'],
					             			          'gender' =>  $reg['text'],
					             			          'marital_status' =>  $reg['text'],
					             			          'nationality' =>  $reg['num'],
					             			          'date_of_birth' => $reg['text_num'],
					             			          'religion' =>  $reg['text_num'],
					             			          'place_of_birth' =>  $reg['text_num'],
					             			          'permanent_addreas' =>  $reg['text_persian'],
					             			          'permanent_city' =>  $reg['text_persian'],
					             			          'permanent_state' =>  $reg['text_persian'],
					             			          'present_addreas' =>  $reg['text_persian'],
					             			          'present_city' =>  $reg['text_persian'],
					             			          'present_zip' =>  $reg['num'],
					             			          'present_country' =>  $reg['text'],
					             			          'personal_email' =>  $reg['text_num'],
					             			          'work_email' =>  $reg['text_num'],
					             			          'home_telephone' =>  $reg['num'],
					             			          'mobile' =>  $reg['num']
					             			          ]);
						if($valid->fails()){
							$check  = $valid->errors()->all();
							 $explode = implode($check);
							//return response()->json(['header' => ['message' => $explode ,'status' => 200], 'data' => $data],500);
							return response()->json(['header'=>['message'=>$explode,'status'=>500 ], 'data' => [] ],500);
						}else{ $insert = \DB::table('emp')->insert($val);
						 }
					}
					$message = 'Success to upload data'; $status = 200; 
			
				}
				else{ $message = 'Unauthorized'; $status = 403; $access = $access[3];	}
			}else{ $message = $get_role['message']; $status = 401; $access = $access[3];	 }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
			return \Response::json(['header'=>['message'=>$message,'status'=>$status ], 'data' => [] ],$status);
		}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$message = 'Not View';
		$status = 403;
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],403);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$message = 'Not View';
		$status = 403;
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],403);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$message = 'Not View';
		$status = 403;
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],403);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$message = 'Not View';
		$status = 403;
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],403);
	}

// CHECK ACCESS ###############################################################################
	public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(isset($q) != null){ //check isset key in session
				 
						 $r = array();
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } // end for store role to array.

						 if(isset($r['key'])){
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							//return $employee_id;
							 $q2 = $getModel->session_role($employee_id);
							 foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 } // end for store role to array.
						 }
						 else{
						 	return ['message'=>'ACCESS DENIED','data'=>null];
						 }
						

				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
						$msg = 'ACCESS GRANTED !';	
					}else{
						$msg = 'ACCESS DENIED!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED!';
					$data = null;	
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;	
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		return ['message'=>$msg,'data'=>$data];
	}
// END CHECK ACCESS ############################################################################

}
