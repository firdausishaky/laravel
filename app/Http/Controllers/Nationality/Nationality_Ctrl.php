<?php namespace Larasite\Http\Controllers\Nationality;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Larasite\Model\Master\Nationalities\Nationalities_Model;
use Larasite\Privilege;
use Illuminate\Http\Request;
/*MyClass*/
use Larasite\Library\FuncAccess;

class Nationality_Ctrl extends Controller {

protected $form = 10;
protected $rule = ['undefined',NULL,''];
	private function check_id($id)
	{
		$rule = ['undefined',NULL,''];
		if(in_array($id,$rule)){ $data = ['message'=>'ID Undefined.','status'=>500,'data'=>null];  }
		else{ $data = ['message'=>'ID Valid.','status'=>200,'data'=>null]; } return $data;
	}
	private function set_valid()
	{
		$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text_only'=>'Regex:/^[A-Za\-! ,\'\"\/@\.:\(\)]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
		$rule =  ['title'=>'required|'.$reg['text_num']];
		$valid =  \Validator::make(\Input::all(),$rule); return $valid;
	}
	public function getPim()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Nationalities_Model;
		if($access[1] == 200){
				$status = \DB::SELECT("select jobs_emp_status.id, jobs_emp_status.title from jobs_emp_status");
				$jobTitle = \DB::SELECT("select id,title from job");
				$department = \DB::SELECT("select id,name from department");

				$data = ['status' => $status, "job" => $jobTitle, "department" => $department];
				if($data){
					$message ="success get data  status, job, title";
					$status = "200";
				}
		 }else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		 return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	// INDEX
	public function index() // GET FIX***
	{

		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Nationalities_Model;
		if($access[1] == 200){
				$datas = Nationalities_Model::get();
				if($datas){$data = $datas ; $message = 'Nationalities : Show Records Data.'; $status = 200; }
				else{ $data = null ; $message = 'Nationalities : Empty Records Data.'; $status = 200; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);

	}
	// STORE
	public function store()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/ $model = new Nationalities_Model;
		if($access[1] == 200){
			$valid = $this->set_valid();
			if($valid->fails()){ $message='Required Input Failed.'; $status=500; $data=null;}
			else{
				$title = \Input::get('title'); 
				if(in_array($title,$this->rule)){  $message='Required Input Failed.'; $status=500; $data=null;  }
				else{	
					$store = $model->StoreNationalities($title);
					$message=$store['message']; $status=$store['status']; $data=$store['data'];
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	// SHOW
	public function show($id) // GET FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Nationalities_Model;
		if($access[1] == 200){
			if($model->ReadNationalities('Sort',$id)){
				$status = 200; $data = $model->ReadNationalities('Sort',$id)[0];	$message = 'Show Data.';
			}else{ $status = 404; $data = null; $message = 'Data not found'; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	// EDIT
	public function edit($id) //GET / HEAD FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Nationalities_Model;
		if($access[1] == 200){
			if($model->ReadNationalities('Sort',$id)){
				$status = 200; $data = $model->ReadNationalities('Sort',$id)[0];	$message = 'Show Data.';
			}else{ $status = 404; $data = null; $message = 'Data not found'; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	// UDPATE
	public function update($id) // PUT FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new Nationalities_Model;
		if($access[1] == 200){
			$check_id  = $this->check_id($id);
			if($check_id['status'] == 500){ $message=$check_id['message']; $status=$check_id['status']; $data=$check_id['data']; }
				
				$valid  = $this->set_valid();
				if($valid->fails()){ $message='Required Input Failed'; $status=500; $data=NULL; }
				else{
					$title = \Input::get('title');
					$update = $model->UpdateNationalities($id,$title);
					$message = $update['message']; $status=$update['status']; $data = $update['data'];
				}

		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete'); /*Model*/ $model = new Nationalities_Model;
		if($access[1] == 200){
			$check_id  = $this->check_id($id);
			if($check_id['status'] == 500){ $message=$check_id['message']; $status=$check_id['status']; $data=$check_id['data']; }
			else{
				$getid = explode(",",$id);
				$del = $model->DestroyNationalities($getid);
				
				if($del['status'] == 200){$message = $del['message']; $status = 200; $data=null;
				}else{ $message = $del['message']; $status = 500; $data=null;}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
}
