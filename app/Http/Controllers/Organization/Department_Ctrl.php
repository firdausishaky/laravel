<?php namespace Larasite\Http\Controllers\Organization;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;

use Larasite\Model\Master\Organization\Department_Model;
use Larasite\Privilege;
use Larasite\UserTest as User;
use JWTAuth;
use Auth;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
class Department_Ctrl extends Controller {

protected $form = 6;
private function check_id($id)
{
	$rule = ['undefined',NULL,''];
	if(in_array($id, $rule)){ return 500; }else{ return 200; }
}
private function set_valid()
{ 
	$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text_only'=>'Regex:/^[A-Za\-! ,\'\"\/@\.:\(\)]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
	$rule =  ['name'=>'required|'.$reg['text_num']]; 
	$valid =  \Validator::make(\Input::all(),$rule); return $valid; 
}
private function set_input(){ $name = \Input::get('name'); return $name;	 }

// INDEX [GET]
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Department_Model;
		if($access[1] == 200){
				if($access[3]['create'] == 0 && $access[3]['read']  == 0 && $access[3]['update'] == 0  && $access[3]['delete'] == 0){
					return \Response::json(['header'=>['message'=>'Unauthorized','status'=>'200', 'access'=>$access[3]],'data'=>[]],200);
				}else{

				$datas = \DB::SELECT("select id,name,parent from department where id > 1");
				foreach ($datas as $key => $value) {
					if(isset($value->parent) && $value->parent != null ){
						$master = $value->parent;
						if($master == 1){
							$datas[$key]->parent = "Root";
							$datas[$key]->stat = "inUse";
						}else if($master == 0){
							$datas[$key]->parent = "N/A";
							$datas[$key]->stat = "unUse";
						}else{
							$master = \DB::SELECT("select name from department where id = $master limit 1");
							if($master[0]->name != null){
								$datas[$key]->parent = $master[0]->name;
								$datas[$key]->stat = "inUse";
							}
						}				
					}else{
						$datas[$key]->parent = "N/A";
						$datas[$key]->stat = "unUse";
					}
				}
				if($datas){ $message = 'Status : Show Records Data.'; $status = 200; $data = $datas;}
				else{$message = 'Status : Empty Records Data.'; $status = 200; $data = null;}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}
// STORE [POST]
	public function store() // POST
	{
		/*Access*/$FRA = new FuncAccess;
		 $access = $FRA->Access(\Request::all(),$this->form,'create');
		  /*Model*/ $model = new Department_Model;
		if($access[1] == 200){
			$input = $this->set_input(); $valid = $this->set_valid();
			if($valid->fails()){
				$message = 'Required Input Failed.'; $status=406; $data=null; 
			}else{
				$Store = $model->Store_Department($this->set_input());
				$data = $Store[2]; $message = $Store[0]; $status = $Store[1];
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);	
	}
// SHOW [GET]
	public function show($id) // GET ID 
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = \DB::table('jobs_emp_status');
		$crud = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
				$get = $data_table->find($id);
				if($get){ $message = 'Show Reacords Data.'; $status = 200; $data = $get; }
				else{ $message = 'Empty Records Data.';	$status = 404;	$data = null; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);	
	}
// EDIT [GET]
	public function edit($id) //GET / HEAD
	{	
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'index'); /*Model*/ $model = \DB::table('jobs_emp_status');
		$crud = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
				$get = $data_table->find($id);
				if($get){ $message = 'Show Reacords Data.'; $status = 200; $data = $get; }
				else{ $message = 'Empty Records Data.';	$status = 404;	$data = null; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);	
	}

//UPDATE [POST]
	public function update($id)
	{  
		/*Access*/$FRA = new FuncAccess; 
		$access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new Department_Model;
		$crud = $FRA->Access(\Request::all(),$this->form,'update');
		if($access[1] == 200){
			$input = $this->set_input();
			$valid = $this->set_valid();
			if($valid->fails()){
				$message = 'Required Input Failed.'; $status=406; $data=null; 
			}else{
				$Update = $model->Update_Department($id,$this->set_input());
				$data = $Update[2]; $message = $Update[0]; $status = $Update[1];
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);	
	}
// DELETE [POST]
	public function destroy($id) // DELETE
	{
		// CHECK KEY API IN ROUTING
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete'); /*Model*/ $model = new Department_Model;
		$crud = $FRA->Access(\Request::all(),$this->form,'destroy');

		if($access[1] == 200){
				$toArray = explode(",", $id);
				$data = $model->Delete_Department($toArray);
				if(!empty($data['status']) && $data['status'] == 200){ 
					$message=$data['message'];	$status=$data['status'];	
				}else{	$message=$data['message'];	$status=$data['status']; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $crud[3]],'data'=>$data],$status);
	}

	public function jwt_test(Request $request){
		// $credentials = \Input::only('email');

		// try {
		//    $user = User::create($credentials);
		// } catch (Exception $e) {
		//    return \Response::json(['error' => 'User already exists.'], \HttpResponse::HTTP_CONFLICT);
		// }

		// $token = \JWTAuth::fromUser($user);

		//return \Response::json(compact('token'));
		//return (array)Auth::user();	
		return \Session::all();
  	}
}