<?php namespace Larasite\Http\Controllers\Organization;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Model\Organization_Model;
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
class GeneralInformation_Ctrl extends Controller {


protected $form = 5;
protected $Extfile = ["title"=>"general_information","extend"=>["gif","png","jpg","jpeg"]];
protected $dir = "/hrms_upload/general_information";


// SET VALID
private function set_valid($input)
{
	$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'num'=>'Regex:/^[0-9-\^ ]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];

	$rule = ['name'=>'required|'.$reg['text_num'].'|min:5',
			'phihealth_numb'=>'numeric',
			'pag_big_numb'=>'numeric',
			'phone'=>$reg['num'],
			'email'=>'email',
			'bir_numb'=>'numeric',
			'sss_numb'=>'numeric',
			'rdo_numb'=>'numeric',
			'fax'=>$reg['num'],
			'address1'=>$reg['text_num'],
			'address2'=>$reg['text_num'],
			'state'=>$reg['text_num'],
			'country'=>$reg['text'],
			'city'=>$reg['text_num'],
			'zip'=>$reg['num']
			];
	$valid = \Validator::make($input,$rule);
	return $valid;
}
// SET INPUT
	private function input(){
		$input = [
			'name'				=>	\Input::get('name'),
			'phihealth_numb'	=>	\Input::get('phihealth_numb'),
			'pag_big_numb'		=>	\Input::get('pag_big_numb'),
			'phone'				=>	\Input::get('phone'),
			'email'				=>	\Input::get('email'),
			'bir_numb'			=>	\Input::get('bir_numb'),
			'sss_numb'			=>	\Input::get('sss_numb'),
			'rdo_numb'			=>	\Input::get('rdo_numb'),
			'fax'				=>	\Input::get('fax'),
			'address1'			=>	\Input::get('address1'),
			'address2'			=>	\Input::get('address2'),
			'state'				=>	\Input::get('state'),
			'country'			=>	\Input::get('country'),
			'city'				=>	\Input::get('city'),
			'zip'				=>	\Input::get('zip')
		];
		return $input;
	}

// GET FILE
	public function ViewFile(){
			$check = \DB::table('general_information')->get(['filename','path']);
			
			if($check){
				foreach ($check as $key) { $filename = $key->filename; $paths = $key->path; }
				$path = storage_path().$paths.'/'.$filename;
				$file = \File::get($path);
				$type = \File::mimeType($path);
				$header = ['Content-Description : File Transfer',
						'Content-Disposition : attachment;filename='.basename($type),
						'Content-Transfer-Encoding : binary','Expires : 0','Cache-Control : must-revalidate','Pragma : public'];
				$header['Content-Type']=$type;
				return  \Response::make($file,200,$header);
			}else{ return \Response::json('File Not Found',404); }
	}
// View File
// GET FILE
	public function get_file(){
		
		try { $getfile = \DB::table('general_information')->get(['filename','path']); } 
		catch (Exception $e) { return null; }

		foreach ($getfile as $key) { $file['path'] = $key->path; $file['filename'] = $key->filename; }
		$getmime = storage_path().$this->dir.'/'.$file['filename']; // UBAH PHP.INI ENABLE php_fileinfo.dll untuk memberikan izin file image dapat di download;
		$file = \File::get($getmime); $type = \File::mimeType($getmime);
		return \Response::make($file,200,['Content-Type'=>$type,'Content-Description'=>'File Transfer','Content-Disposition' =>'attachment;filename='.basename($getmime),
											'Content-Transfer-Encoding' => 'binary','Expires' => 0,'Cache-Control' => 'must-revalidate']);		
	}
// INDEX 
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Organization_Model;
		if($access[1] == 200){

				$result = $model->Read_GI($access[3]);
				$data = $result['data']; $message = $result['message']; $status = $result['status']; $access = $result['access'];

		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access' => $result['access']],'data'=>$data],$status);
	}
// STORE 
	public function store(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/ $model = new Organization_Model;
		if($access[1] == 200){
			$message='Page Not Found.'; $status=404; $data=NULL;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// UPDATE
	public function update($id) // PUT FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Organization_Model;
		if($access[1] == 200){
				$file=\Input::file('file'); 
				$get = $this->set_input(); 
				$radio = $get['radio']; 
				$input = $get['data'];
				if(isset($id) && isset($file)){ // jika hanya update file
						$valid = $this->set_valid($input);
						if($valid->fails()){ $message='Required Input Failed.'; $status=500; $data=null; }
						else{
							$image = $file->getClientOriginalName();
							$ext = $file->getClientOriginalExtension();
							$size = $file->getSize();
							$path = "/hrms_upload/general_information";
							if($ext == "jpg" || $ext == "png" || $ext == "JPG" || $ext == "PNG"){
								if($size > 1000000){
									$message = "failed image format supported but size of image than 1 MB"; $data = [] ; $status= 500; $access = $access[3];
								}else{
									$image = 'General_information'.str_random(6).'.'.$ext;
									$data = $file->move(storage_path($path),$image);
									$update = \DB::SELECT("update general_information set path='$path',filename ='$image' where id='$id' ");
									$data =  \DB::SELECT("select * from general_information");
									$message = "success upload image"; $data = [] ; $status= 200; $access = $access[3];

									//$message = "success"; $data = $data ; $status= 500; $access = $access[3];
								}
							}else{
								$message = "Only support png and jpeg"; $data = [] ; $status= 200; $access = $access[3];
							}
						}
				}elseif($radio == "1"){
					$valid = $this->set_valid($input);
					if($valid->fails()){ $message='Required Input Failed.'; $status=500; $data=null; }
					else{
						$update = $model->Update_GI($id,$input);
						$message = $update['message']; $status=$update['status']; $data=$update['data'];
					}
				}elseif($radio == "2"){
					$valid = $this->set_valid($input);
					if($valid->fails()){ $message='Required Input Failed.'; $status=500; $data=null; }
					else{
						$input['filename'] = NULL; 
						$input['path'] = NULL;
						$update = $model->Update_GI($id,$input);
						$message = $update['message']; $status=$update['status']; $data=$update['data'];
					}
				}else{ $message = 'Input Empty.'; $status = 406; $data = null;}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
//SHOW
	public function show($id) // GET FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Organization_Model;
		if($access[1] == 200){
				$message='Page Not Found.'; $status=404; $data=null;		
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// EDIT
	public function edit($id) //GET / HEAD FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new Organization_Model;
		if($access[1] == 200){
				$message='Page Not Found.'; $status=404; $data=null;		
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// DESTROY
	public function destroy($id) // DELETE
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete'); /*Model*/ $model = new Organization_Model;
		if($access[1] == 200){
				$message='Page Not Found.'; $status=404; $data=null;		
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// MAKE ID
		public function make(){
		$kode_tahun = "ORGS"; $kode_index = "0001" ; $q = '';
		$result = \DB::select("SELECT id FROM general_information where id like 'ORGS%' ORDER BY id ASC");
		foreach ($result as $key) { $q = $key->id; }

		if($q == ''){ return $auto = "".$kode_tahun."".$kode_index.""; }
		else{
			if(substr($q ,7, 1) <= 9 && substr($q ,4, 3) =='000')
			{
				if(substr($q ,7, 1) == '9') { $tempcode1 = substr($q ,7, 7) + 1; return $auto = "ORGS00".$tempcode1; }
				else{ $tempcode1 = substr($q , 7 , 7) + 1; return $auto = "ORGS000".$tempcode1; }
			} 
			elseif(substr($q ,6,2) <= 99 && substr($q ,4,2 ) == '00'){
				if(substr($q ,6,2 ) == '99') { $tempcode1 = substr($q ,6, 6) + 1; return $auto = "ORGS0".$kode; }
				else{ $tempcode1 = substr($q ,6, 6) + 1; return	$auto = "ORGS00".$kode; }
			}
			elseif(substr($q ,5,3) <= 999 && substr($q ,4,1 ) == '0'){
				if(substr($q ,5,3 ) == '999'){ $tempcode1 = substr($q ,5, 5) + 1; return	$auto = "ORGS".$kode; }
				else{ $tempcode1 = substr($q ,5, 5) + 1; return	$auto = "ORGS0".$kode; }
			}
			elseif(substr($q ,4, 4) <= 9999){
				if(substr($q ,4,4 ) == '9999'){ return	$auto = 'ID tidak dapat dibentuk.'; }
				else{ $tempcode1 = substr($q ,4, 4) + 1; $kode = $tempcode1  + 1; return $auto = "".$kode_tahun."".$kode.""; }
			}
			else{ return	$auto = 'ID CANNOT BUILD'; }
		}//## END AUTONUMB
	}
// CLASS UPLOAD AND VAR CONST
	protected $subtitle = 'general_information';
	private function Move($id,$file,$input,$type){
		$model = new Organization_Model; /*Upload*/ $FuncUpload = new FuncUpload;
		$check = $model->check_file_id($id);
		print_r($check);
		if($type == true){ // true for update
			if($check){ $move = $FuncUpload->move_file($check,$this->Extfile['title']); } 
				$up = $this->Upload($id,$file,$input,true);
				$message=$up[0]; $status=$up[1]; $data=$up[2];
		}else{ $up = $this->Upload($id,$file,$input,false); $message=$up[0]; $status=$up[1]; $data=$up[2]; }
		return [$message,$status,$data];
	}
	private function Upload($id,$file,$input,$type){
		$model = new Organization_Model; /*Upload*/ $FuncUpload = new FuncUpload; $dir = $this->dir;
		$upload =  $FuncUpload->upload($this->dir,$file,$this->Extfile);
		if(isset($upload['filename']) && $upload['status'] == 200){
			if($type == true){
				$input['filename'] = $upload['filename']; $input['path'] = $upload['path'];
				Organization_Model::where('id','=',$id)->update($input);
				$file->move(storage_path($upload['path']),$upload['filename']);
				$message='Updated Successfully.'; $status = 200; $data=Organization_Model::find($id);
			}else{
				Organization_Model::insert(['filename'=>$upload['filename'],'path'=>$upload['path']]);
				$message = $upload['message']; $data = ['id'=>$model->GetID($upload['filename']),'filename'=>$upload['filename']]; $status = 200;
			}
		}else{ $message=$upload['message']; $status=$upload['status']; $data=null;}
		return [$message,$status,$data];
	}
	private function set_input(){
		$data = array(); // tampung input to array
		$dataString = \Input::get('data');
		$data = json_decode($dataString,1);
		
		if(isset($data['name'])){ $result['name'] = $data['name']; }
		else{ $result['name'] = null; }
		
		if(isset($data['phihealth_numb'])){ $result['phihealth_numb'] = $data['phihealth_numb']; }
		else{ $result['phihealth_numb']  = null; }

		if(isset($data['pag_big_numb'])){ $result['pag_big_numb'] = $data['pag_big_numb']; }
		else{ $result['pag_big_numb']  = null; }

		if(isset($data['phone'])){ $result['phone'] = $data['phone']; }
		else{ $result['phone']  = null; }
		
		if(isset($data['email'])){ $result['email'] = $data['email']; }
		else{ $result['email']  = null; }
		
		if(isset($data['bir_numb'])){ $result['bir_numb'] = $data['bir_numb']; }
		else{ $result['bir_numb']  = null; }
		
		if(isset($data['sss_numb'])){ $result['sss_numb'] = $data['sss_numb']; }
		else{ $result['sss_numb']  = null;  }
		
		if(isset($data['rdo_numb'])){ $result['rdo_numb'] = $data['rdo_numb'];  }
		else{ $result['rdo_numb']  = null; }
		
		if(isset($data['fax'])){ $result['fax'] = $data['fax']; }
		else{ $result['fax']  = null;  }

		if(isset($data['address1'])){ $result['address1'] = $data['address1']; }
		else{ $result['address1']  = null; }
		
		if(isset($data['address2'])){ $result['address2'] = $data['address2']; }
		else{ $result['address2']  = null;  }

		if(isset($data['city'])){ $result['city'] = $data['city']; }
		else{ $result['city']  = null; }
		
		if(isset($data['state'])){ 	$result['state'] = $data['state'];  }
		else{ $result['state']  = null;  }
		
		if(isset($data['zip'])){ $result['zip'] = $data['zip'];  }
		else{ $result['zip']  = null; }
		
		if(isset($data['country'])){ $result['country'] = $data['country']; }
		else{ $result['country']  = null; }

		if(isset($data['bank_number'])){ $result['bank_number'] = $data['bank_number']; }
		else{ $result['bank_number']  = null; }

		if(isset($data['bank_name'])){ $result['bank_name'] = $data['bank_name']; }
		else{ $result['bank_name']  = null; }

		if(isset($data['radio'])){ $radio = $data['radio']; return ['data'=>$result,'radio'=>$radio]; }

		return ['data'=>$result,'radio'=>NULL];
	}
}