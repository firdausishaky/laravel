<?php namespace Larasite\Http\Controllers\Organization;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;

// GET MODEL ###############
use Larasite\Model\OrganizationStruct_Model;
use Larasite\Privilege;

class OrganizationStructure_Ctrl extends Controller {

	protected $form = 6;

	/** INDEX ################################################################################
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	private function ModeltoJSON($dir,$data,$fileName,$modeFile){
		$make = fopen($dir.$fileName, $modeFile);
		fwrite( $make ,$data );
		fclose($make);
		return $make;
	}

	public function get_file($filename){
		$path = storage_path().'\hrms_json\\'.$filename;
		$file = \File::get($path);
		$type = \File::mimeType($path);
		return \Response::make($file,200,['Content-Type'=>$type]);
	}
	public function index()
	{
		$model = new OrganizationStruct_Model;
		$get_role = $this->check();
		
		if( isset($get_role['data']['role']) ){
			$access = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form);
			if( $access['read'] != 0 && $access['read'] != null ){
				
				$result = $model->Read_Structure();
				if($result){
					//$filename = 'structure.json';
					//$json = json_encode($result);
					//$this->ModeltoJSON(storage_path().'/hrms_json/',$json,$filename,'w');
					//$message='View data.';	$status=200;  $data=$result;
					return \Response::json($result,200);
				}else{ $message='Data not found.';	$status=404;  $data=null; }
			}else{ $message='Unauthorized.';	$status=403;  $data=null; }
		}else{ $message=$get_role['message'];	$status=401;  $data=null; }

		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		
	}//END INDEX ################################################################################




	/**CREATE ################################################################################
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}//END CREATE ################################################################################



	/** STORE ################################################################################
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	private function check_parent_ins($id){
		$get = \DB::select("SELECT id,parent_id from sub_unit where id = $id");
		foreach ($get as $key) {
			$data['id'] = $key->parent_id;
		}
		if($data['id'] == 1){
			$status = 200;
		}else{
			$get2 = \DB::select("SELECT id,parent_id from sub_unit where id = $data[id] ");
		}
	}
	public function store()
	{
		$model = new OrganizationStruct_Model;
		$get_role = $this->check();
		$input = $this->set_input();
		if($get_role['data']['role']){
			$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form);
			if($data_role['create'] != null && $data_role['create'] !=0 ){
				// # CHECK INPUT NULL OR NOT
				$input_store = $input['data'];
				
				if(isset($input) && $input['status'] == 200 && $input_store['parent_id'] != null){
					//$input2 = $input['data'];
					
					$store = $model->Store_Structure($input_store);
					
					if($store){
						$status = $store['status']; $message = $store['message']; $data = $store['data'];	
					}
					else{ $status = $store['status']; $message = $store['message']; $data = $store['data'];	 }
				}else{ $status = 406; $message = 'Input not null.';	$data = null;}
			}else{ $status = 401; $message = 'Unauthorized.'; $data  = null; }
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
		else{
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403);
		}
	}//END STORE ################################################################################



	/**SHOW ################################################################################
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}//END SHOW ################################################################################




	/**EDIT ################################################################################
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}//END EDIT ################################################################################



	/** UPDATE ###################################################################################
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$model = new OrganizationStruct_Model;
		$get_role = $this->check();
		$input = $this->set_input();
		if($get_role['data']['role']){
			$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form);
			if($data_role['update']!= null && $data_role['update'] !=0){
				// # CHECK INPUT NULL OR NOT
				if(isset($input) && $input['status'] == 200 && OrganizationStruct_Model::find($id)){
					$update = $model->Update_Structure($id,$input['data']);
					
					if($update['status'] == 200){
						$status = $update['status']; $message = $update['message']; $data = $update['data'];	
					}
					else{ $status = $update['status']; $message = $update['message']; $data = $update['data'];	 }
				}else{ $status = 406; $message = 'Input not null.';	$data = null;}
			}else{ $status = 401; $message = 'Unauthorized.'; $data  = null; }
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
		else{
			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403);
		}
	}//END UPDATE #################################################################################




	/** DESTROY ####################################################################################
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$get_role = $this->check();
		$data = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form);

		$destroy_model = new OrganizationStruct_Model;
		if($get_role['data']['role']){
			if($data['delete']!=null && $data['delete']!= 0){
				$data = $destroy_model->Destroy_Struct($id);

				if($data == 200 ){
					$message = 'Delete Successfully.';
					$status = $data;
				}else{
					$message = 'Delete Failed';
					$status = $data;
				}
			}
			else{
				$message = 'Unauthorized';
				$status = 401;
			}
			return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
		}
		else{ return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403); }
	}// END DESTROY ################################################################################





	/* CHECK INPUT ################################################################################################################
	* Param $id = USE FIND ID FOR UPDATE.
	* PARAM $TYPE = 1(STORE), 2(UPDATE).
	*/
	private function set_input(){
		$data = array(); // tampung input to array
		$data['sub_unit'] 		= \Input::get('sub_unit');
		$data['parent_id'] = \Input::get('parent_id');
		$data['descript']	= \Input::get('descript');
		if(isset($data['sub_unit'],$data['parent_id'])){
					
			$status=200; $result = $data;
		}else{ $status=500; $result=null; }	
		return ['status'=>$status,'data'=>$result];
	}
	// CHECK INPUT VALUE #################################################################################

	/* CHECK ROLE ###################################################################################
	* PARAM = ROLE AND FORM
	* CHECK USER ROLE YANG ACCESS SYSTEM
	* MENGHASILKAN DATA ACCESS DAN ID
	*/
	private function check_role($role,$employee_id,$form) // CHECK ROLE FIX***
	{
		$getModel = new Privilege;
		//$user 		= base64_decode(\Cookie::get('id'));
		if(isset($role, $form)){
			return $getModel->check_role_personal2($role,$employee_id,$form);
		}
		else{
			$msg['warning'] = 'Unauthorize';
			$msg['status'] = 404;
			return $msg; 
		}
	}// END CHECK ROLE #####################################################################

	/** CHECK KEY ###########################################################################
	* FUNGSI CHECK KEY API
	* GET REQUEST KEY
	*/
	// CHECK ACCESS ###############################################################################
	public function check(){
		$getModel = new Privilege;	$r = array();	$req = \Request::all();
		if(isset($req['key'])){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if(!empty($q)){ //check isset key in session
			//#########################################################################################
						 foreach ($q as $keys) {
						 	$r['key'] = $keys->session_key;
						 } 
							$decode = base64_decode($r['key']);
							$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
							$key = substr($decode,0,strpos($decode,'-'));
							$q2 = $getModel->session_role($employee_id);
							foreach ($q2 as $keys) {
							 	$r['employee_id'] = $keys->employee_id;
							 	$r['role_id'] = $keys->role_id;
							 }				
			//#########################################################################################	
				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) {
						$role['role'] = $key->role_id;
					}
					if(isset($role['role'])){
						//return $role['role'];
						$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']];
						$msg = 'ACCESS GRANTED !';	
					}else{
						$msg = 'ACCESS DENIED!';
						$data = null;
					}
				}
				else{
					$msg = 'ACCESS DENIED!';
					$data = null;	
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;	
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		return ['message'=>$msg,'data'=>$data];
	}
// END CHECK ACCESS ############################################################################
}
