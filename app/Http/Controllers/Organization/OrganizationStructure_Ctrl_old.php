<?php namespace Larasite\Http\Controllers\Organization;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;

// GET MODEL ###############
//use Larasite\Model\OrganizationStruct_Model;
use Larasite\Model\Master\Organization\ORGStructure_Model as OrganizationStruct_Model;
use Larasite\Model\Master\Organization\Department_Model;
use Larasite\Privilege;
// MyClas
use Larasite\Library\FuncAccess;

class OrganizationStructure_Ctrl extends Controller {

	protected $form = 6;

	
	private function set_valid()
	{
		$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/'];
		$rule = ['sub_unit'=>"required|".$reg['text_num'],'descript'=>$reg['text_num'],'parent_id'=>'numeric'];
		$valid = \Validator::make(\Input::all(),$rule);
		return $valid;
	}

	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new OrganizationStruct_Model;
		if($access[1] == 200){
				if ($access[3]['create'] == 0 && $access[3]['delete'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 ){
					$message = "Unauthorized";
					$data = [];
					$status = 200;
					$access = $access[3];
					//return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access],'data'=>$data],$status);
				}else{
					$result = \DB::select("select id from sub_unit where id !=1");
					if($result){
						$result = $model->Read_Structure();
						//return $result;
						if($result){ $data['organization'] = $result; $data['department'] = \DB::select("call view_department"); $status = 200; $message = 'Show records data.'; }
						else{ $message='Empty records data.';	$status=200;  $data=null; }
					}else{ 
						$result = \DB::select("select id,sub_unit,descript,parent_id from sub_unit");
					 	foreach ($result as $first) {
					 		$datas = ['id'=>$first->id,'parent_id'=>$first->parent_id,'sub_unit'=>$first->sub_unit,'count'=>0,'sub'=>null];	
					 	}
					 	$data = $datas; $message = 'Show records data.'; $status = 200;
					}
				}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
		
	}//END INDEX ################################################################################

	private function check_parent_ins($id){
		$get = \DB::select("SELECT id,parent_id from sub_unit where id = $id");
		foreach ($get as $key) { $data['id'] = $key->parent_id; }
		if($data['id'] == 1 || $data['id'] == null){ $status = 200; }
		else{ $status = 500; }
		return $status;
	}

	public function store()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/ $model = new OrganizationStruct_Model;
		if($access[1] == 200){
			$valid = $this->set_valid();
			if($valid->fails()){ $message = 'Required input failed.'; $status=500; $data=null; }
			else{
				$input_store = $this->set_input();	
				$check = $this->check_parent_ins($input_store['parent_id']);
				if($check == 200){	
					$store = $model->Store_Structure($input_store);		
					$status = $store['status']; $message = $store['message']; $data = $store['data'];	
				}else{ $status = 406; $message = 'Input not null or only three level.';	$data = null;}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
			
	}//END STORE ################################################################################

	// UPDATE ##################################################################################
	public function update($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new OrganizationStruct_Model;
		if($access[1] == 200){
			$valid = $this->set_valid();
			if($valid->fails()){ $message = 'Required input failed.'; $status=500; $data=null; }
			else{
				if(OrganizationStruct_Model::find($id)){
					$update = $model->Update_Structure($id,$this->set_input());
					$status = $update['status']; $message = $update['message']; $data = $update['data'];
				}else{ $status = 406; $message = 'ID Undefined.';	$data = null;}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}//END UPDATE #################################################################################

	// DESTROY ####################################################################################
	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete'); /*Model*/ $model = new OrganizationStruct_Model;
		if($access[1] == 200){
				$data = $model->Destroy_Struct($id);
				
				if($data == 200 ){ $message = 'Delete Successfully.'; $status = $data;
				}else{ $message = 'Delete Failed'; $status = $data; }

		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}// END DESTROY ################################################################################

	// CHECK INPUT #################################################################################
	private function set_input(){
		$data = array(); // tampung input to array
		$data['sub_unit'] 		= \Input::get('sub_unit');
		$data['parent_id'] = \Input::get('parent_id');
		$data['descript']	= \Input::get('descript');
		if(!isset($data['sub_unit'],$data['parent_id'])){ $data = false; }
		return $data;
	}

	public function show($id)
	{
		$message='Forbaidden.';	$status=401;  $data=null;
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}//END SHOW ################################################################################
	public function create()
	{
		$message='Forbaidden.';	$status=401;  $data=null;
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}//END CREATE ################################################################################

	public function edit($id)
	{
		$message='Forbaidden.';	$status=401;  $data=null;
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}//END EDIT ################################################################################
}
