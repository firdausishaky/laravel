<?php namespace Larasite\Http\Controllers;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Privilege;
use Illuminate\Http\Request;

class PrivilegeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($role,$form)
	{
		$getModel = new Privilege;
		$token_ori = \Cookie::get('XSRF-TOKEN');
		$user 		= base64_decode(\Cookie::get('id'));
		if($token_ori){
			
			$permissions = \DB::select("select a.role_name,d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
						from role a, permissions c, form d
						where 
						c.role_id = a.role_id and
						c.form_id = d.form_id and
						c.form_id =  '$form' and 
						a.role_id = '$role' ;");
			//$userdata = '';
			foreach ($permissions as $key) {
				$userdata['role_name'] = $key->role_name;
				$userdata['form_name'] = $key->form_name;
				$userdata['create'] = $key->create;
				$userdata['read'] = $key->read;
				$userdata['update'] = $key->update;
				$userdata['delete'] = $key->delete;
			}
			return $userdata;
			
		}
		else{
			$msg['warning'] = 'Harap login terlebih dahulu';
			$msg['status'] = 404;
			return $msg; 
		}


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$role = 1;//\Input::get('role');
		$form = 5;//\Input::get('form');
		$data = $this->index($role,$form);
		if(!$data['create']){
			//$a = action('Admin\UserManagement\SystemUserCtrl@show', ['role' => $role,'form'=>$form]);	
			return \Route::controller('Admin\UserManagement\SystemUserCtrl@show'); //\Redirect::to('Admin\UserManagement\SystemUserCtrl@show', ['role' => $role,'form'=>$form]);
		}else{
			return \Response::json(['status'=>404,'msg'=>'Anda tidak mempunyai izin']);
		}
			//return $redirect()->action('App\Http\Controllers\MainCtrl@create', ['role' => $role,'form'=>$form]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$permissions = $this->index();
		if($permissions['create']){
			return \Response::json(['create' => $permissions['create']]);	
		}else{
			return \Response::json(['create'=>200]);
		}
	}

	/**a
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		//
		$permissions = $this->index();
		if($permissions['read']){
			return \Response::json(['read' => $permissions['read']]);	
		}else{
			return \Response::json(['read'=>200]);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$permissions = $this->index();
		if($permissions['update']){
			return \Response::json(['edit'=>$permissions['update']]);	
		}else{
			return \Response::json(['edit'=>200]);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		//
		$permissions = $this->index();
		//$permissions['update'];
		if($permissions['update']){
			return \Response::json(['update' => $permissions['update']]);	
		}else{
			return \Response::json(['update'=>200]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$permissions = $this->index();
		if($permissions['delete']){
			return \Response::json(['delete' => $permissions['delete']]);	
		}else{
			return \Response::json(['delete'=>200]);
		}
	}

}
