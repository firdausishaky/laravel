<?php namespace Larasite\Http\Controllers\Qualification;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\Master\Qualification\Education_Model as Qualification_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;

class Education_Ctrl extends Controller {

protected $form = 8;
	
private function check_id()
{
	$rule = ['undefined',NULL,''];
	if(in_array($rule,$id)){ $data = ['message'=>'ID Undefined.','status'=>500,'data'=>null];  }
	else{ $data = ['message'=>'ID Valid.','status'=>200,'data'=>null]; } return $data;
}
private function set_valid()
{
	$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text_only'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
	$rule =  ['title'=>'required|'.$reg['text_only']]; 
	$valid =  \Validator::make(\Input::all(),$rule); return $valid;
}
private function set_input(){ return ['title'=>\Input::get('title')]; }

	// INDEX
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Qualification_Model;
		if($access[1] == 200){
	 		if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
	 			$message = "Unauthorized"; $status=200; $data=[];
	 		}
			$datas = Qualification_Model::get();
			if($datas){ $data = $datas; $message = 'Education : Show Records Data.'; $status = 200; 
			}else{ $data = null; $message = 'Education : Empty Records Data.'; $status = 404;  }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}

	// STORE
	public function store()
	{
		/*Access*/$FRA = new FuncAccess;
		 $access = $FRA->Access(\Request::all(),$this->form,'read');
		 /*Model*/ $model = new Qualification_Model;
		if($access[1] == 200){
			$valid = $this->set_valid();
			if($valid->fails()){ $message='Required Input Failed'; $status=500; $data=NULL; }
			else{	
				$store = $model->StoreEdu($this->set_input());
				$message = $store['message']; $status=$store['status']; $data=$store['data'];
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,'access' => $access[3]],"data" => $data],$status);
	}

	// SHOW
	public function show($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Qualification_Model;
		if($access[1] == 200){
			$message='Page Not Found.'; $status=404; $data=NULL;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}

	// EDIT
	public function edit($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Qualification_Model;
		if($access[1] == 200){
			$message='Page Not Found.'; $status=404; $data=NULL;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}

	// UPDATE

	// public function getMessage($message,$status,$data,$access){
	// 	return response()->json(["header" => ["message" =>$message, "status" => $status, "access" => $access], "data" => $data],$status);
	// }

	public function update($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new Qualification_Model;
		if($access[1] == 200){
			$input = \Input::get('title');
			$valid = \Validator::make(["title" => $input, "id" => $id],["title" => "required|alpha", "id" => "required"]);
			if($valid->fails()){
				$check = $valid->errors()->all();
				//return $this->getMessage($check,500,[],$access[3]);
				return \Response::json(['header'=>['message'=>$check,'status'=>500, "access" => $access[3]],'data'=>[], 'access' => $access[3]],500);
			}else{
				$db = \DB::SELECT("call update_education($id,'$input')");
				if(isset($db[0]->status) && $db[0]->status == "error"){
					$message = $db[0]->message; $status =500; $data =[]; 
				}
				if(isset($db[0]->status) && $db[0]->status == "used"){
					$message = $db[0]->message; $status =500; $data =[]; 
				}
				if($db){
					$message = "Update education success"; $status = 200; $data = $db;
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, 'access' => $access[3]],$status);	
	}

	// DEL
	public function destroy($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete'); /*Model*/ $model = new Qualification_Model;
		if($access[1] == 200){
			$toArray = explode(",", $id);
			$delete = $model->DestroyEdu($toArray);	
			$message=$delete['message']; $status=$delete['status']; $data=$delete['data'];
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data, 'access' => $access[3]],$status);
	} // END DELETE
}
