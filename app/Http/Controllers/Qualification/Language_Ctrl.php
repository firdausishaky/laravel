<?php namespace Larasite\Http\Controllers\Qualification;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\Master\Qualification\Language_Model;
/*MyClass*/
use Larasite\Library\FuncAccess;

class Language_Ctrl extends Controller {

protected $form = 8;
	private function check_id($id)
	{
		$rule= ['undefined',NULL,''];
		if(in_array($id,$rule)){ $data = ['message'=>'ID undefined.','status'=>500,'data'=>NULL]; }
		else{ $data = ['message'=>'ID OK.','status'=>200,'data'=>NULL]; }
		return $data;
	}
	private function set_valid()
	{
		$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text_only'=>'Regex:/^[A-Za\-! ,\'\"\/@\.:\(\)]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
		$rule =  ['title'=>'required|'.$reg['text_num']];
		$valid =  \Validator::make(\Input::all(),$rule); return $valid;
	}
	private function set_input(){ return ['title'=>\Input::get('title')]; }

	// INDEX
	public function index() // GET FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Language_Model;
		if($access[1] == 200){
				$data = Language_Model::get();
				if($data){
					$datas = $data; $message = 'Language : Show Records Data.'; $status = 200;	
				}else{ $datas = $data; $message = 'Language : Empty Records Data.'; $status = 200; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}
	// STORE
	public function store() // POST FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/ $model = new Language_Model;
		if($access[1] == 200){
			$valid = $this->set_valid();
			if($valid->fails()){ $message='Required Input Failed.'; $status=500; $data=null; }
			else{
				$store = $model->StoreLang($this->set_input());
				$status = $store['status']; $message = $store['message']; $data = $store['data'];
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}//END STORE

	// SHOW
	public function show($id) // GET FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Language_Model;
		if($access[1] == 200){
				$data = Language_Model::find($id);
				if($data){
					$datas = $data; $message = 'Language : Show Records Data.'; $status = 200;	
				}else{ $datas = $data; $message = 'Language : Empty Records Data.'; $status = 200; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}// END SHOW

	// EDIT
	public function edit($id) //GET / HEAD FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new Language_Model;
		if($access[1] == 200){
				$data = Language_Model::find($id);
				if($data){
					$datas = $data; $message = 'Language : Show Records Data.'; $status = 200;	
				}else{ $datas = $data; $message = 'Language : Empty Records Data.'; $status = 200; }
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	} // END EDIT

	// UPDATE
	public function update($id) // PUT FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new Language_Model;
		if($access[1] == 200){
			$check_id  = $this->check_id($id);
			if($check_id['status'] == 500){ $message=$check_id['message']; $status=$check_id['status']; $data=null;}
			else{				
					$valid = $this->set_valid();
					if($valid->fails()){ $message='Required Input Failed'; $status=500; $data=null; }
					else{
						$update = $model->UpdateLang($id,$this->set_input());
						$message = $update['message']; $status = $update['status']; $data=$update['data'];
				}	}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	// DESTROY
	public function destroy($id) // DELETE FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete'); /*Model*/ $model = new Language_Model;
		if($access[1] == 200){
			$check_id  = $this->check_id($id);
			if($check_id['status'] == 500){ $message=$check_id['message']; $status=$check_id['status']; $data=null;}
			else{
				$toArray = explode(",", $id);
				$data = $model->DestroyLang($toArray);
				if(!empty($data['status']) && $data['status'] == 200){ 
					$message=$data['message'];	$status=$data['status'];	
				}else{	$message=$data['message'];	$status=$data['status']; }
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
			
	} // END DELETE
}
