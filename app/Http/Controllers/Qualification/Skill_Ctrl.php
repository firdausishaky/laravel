<?php namespace Larasite\Http\Controllers\Qualification;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Model\Master\Qualification\Skills_Model as Qualification_Model;
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;
class Skill_Ctrl extends Controller {


protected $form = 7;
protected $page404 = ['Page Not Found.',404,NULL];

private function set_valid()
{ 
	$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text_only'=>'Regex:/^[A-Za\-! ,\'\"\/@\.:\(\)]+$/',
			'twit'=>'regex:/^[A-Za-z0-9_]{1,15}$/'];
	$rule =  ['title'=>"required|".$reg['text_num']."|min:2",'descript'=>$reg['text_num']];
	$valid =  \Validator::make(\Input::all(),$rule);
	if($valid->fails()){
		$message = 'Required Input Failed.'; $status = 500; $data = null;
	}else{ $message = NULL; $status = 200; $data = null; }
	return ['message'=>$message,'status'=>$status,'data'=>$data];
}
private function set_input(){ return ['title'=>\Input::get('title'),'descript'=>\Input::get('descript')];	 }

	// INDEX
	public function index() // GET FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/$model = new Qualification_Model;
		if($access[1] == 200){
				$datas = $model->ReadSkill(1,null,$access[3]);
				$data = $datas['data']; $message = $datas['message']; $status = $datas['status']; 
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}
	// STORE
	public function store() // POST FIX***
	{
		/*Access*/$FRA = new FuncAccess;
		 $access = $FRA->Access(\Request::all(),$this->form,'create'); 
		 /*Model*/$model = new Qualification_Model;
		if($access[1] == 200){
			$valid = $this->set_valid();
			if($valid['status'] == 500){ $message = $valid['message']; $status = $valid['status']; $data = $valid['data']; }
			else{
				$store = $model->StoreSkill($this->set_input());
				$message = $store['message']; $status = $store['status']; $data = $store['data'];	
			}		
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	} 
	// SHOW
	public function show($id) // GET FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/$model = new Qualification_Model;
		if($access[1] == 200){
			$message = $this->page404[0]; $status = $this->page404[1]; $data = $this->page404[2];
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	} 
	// EDIT
	public function edit($id) //GET / HEAD FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/$model = new Qualification_Model;
		if($access[1] == 200){
			$message = $this->page404[0]; $status = $this->page404[1]; $data = $this->page404[2];
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}
	// UPDATE
	public function update($id) // PUT FIX
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/$model = new Qualification_Model;
		if($access[1] == 200){
			$valid = $this->set_valid();
			if($valid['status'] == 500){ $message = $valid['message']; $status = $valid['status']; $data = $valid['data']; }
			else{
				$store = $model->UpdateSkill($id,$this->set_input());
				$message = $store['message']; $status = $store['status']; $data = $store['data'];
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
	// DELETE
	public function destroy($id) // DELETE FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete'); /*Model*/$model = new Qualification_Model;
		if($access[1] == 200){
			$explode = explode(",",$id);
			foreach ($explode as $key => $value) {
				$db = \DB::SELECT("call delete_skill($value) ");
				if(isset($db[0]->status) && $db[0]->status == "error"){
					return \Response::json(['header'=>['message'=>$db[0]->message,'status'=>500],'data'=>[] ],500);
				}
			}
			$message = "Delete Skill Success"; $status = 200; $data = [];
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	} // END DELETE
}
