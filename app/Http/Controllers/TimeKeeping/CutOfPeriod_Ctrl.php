<?php namespace Larasite\Http\Controllers\TimeKeeping;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Larasite\Model\TimeKeeping\CutOfPeriod_Model;
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
use Larasite\Library\FuncParseInput;
class CutOfPeriod_Ctrl extends Controller {
 

 protected $form = 53;
 // VIEW COLUMNS AND  TYPE DATA;
 public function getColumns(){ $model = new CutOfPeriod_Model; return  $model->getColumns(); }
 
// SEARCH DOMAIN
	public function search($search){

		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new CutOfPeriod_Model;
		if($access[1] == 200){
			$r = \Request::all();
			$show = \DB::select("SELECT shift_code from attendance_work_shifts where shift_code LIKE '$search%'" );
				if($show){
					$data = 'Shift code must be unique'; $status=500;
				}else{ $data=NULL; $status=200;}
				return \Response::json($data,$status);
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}
 	// SET VALIDATION
  	private function set_valid(){
  		$model = new CutOfPeriod_Model ; $val = $model->getColumns(false); 
  		$input = \Request::only($val['rule']);
  		$rule = ['shift_code'=>'required|alpha|min:2','_from'=>'required','_to'=>'required','hour_per_day'=>'numeric'];
  		$valid = \Validator::make($input,$rule);
  		if($valid->fails()){ $data = ['Required input failed.',500,NULL]; }
  		else{ $data = ['OK.',200,$this->ParseInput($input)]; }
  		return $data;
  	}
  	// CHECK ID
  	
  	private function CheckID($id)
  	{	
  		$rule = ['','undefined',NULL];
  		if(in_array($id,$rule)){ $data = ['ID Undefined.',500,NULL]; }
  		else{ $data = ['OK.',200,$id]; }
  		return $data;
  	}

 	// CRUD FUNCTION
	public function index() // GET FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new CutOfPeriod_Model;
		if($access[1] == 200){

				$datas = $model->CALL('view',NULL);
				$data = $datas[2]; $message = $datas[0]; $status = $datas[1]; 
			
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);	
	} // END INDEX
// CREATE
	public function create() //GET
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/ $model = new CutOfPeriod_Model;
		if($access[1] == 200){
				$message = 'Page Not Found.'; $status = 404; $data = null;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}// END CREATE
// STORE
	public function store() // POST FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/ $model = new CutOfPeriod_Model;
		if($access[1] == 200){
			$valid = $this->set_valid();
			if($valid[1] == 500){ $message=$valid[0]; $status=$validp[1]; $data=$valid[2]; }
			else{ 
				$store = $model->CALL('store',$valid[2]);
				$message = $store[0]; $status = $store[1]; $data = $store[2]; // RES STORE
			 }				
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// STORE
	public function update($id) // POST FIX***
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new CutOfPeriod_Model;
		if($access[1] == 200){
			$check = $this->CheckID($id);
			if($check[1] == 500){ $message = $check[0]; $status = $check[1]; $data = $check[2];}
			else{
				$valid = $this->set_valid();
				if($valid[1] == 500){ $message = $valid[0]; $status = $valid[1]; $data = $valid[2]; }
				else{
					$update = $model->CALL('update',$valid[2]);
					return $update;
					$message = $update[0]; $status = $update[1]; $data = $update[2];
				}
			}
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);	
	} // END STORE

// SHOW
	public function show($id) //GET / HEAD FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new CutOfPeriod_Model;
		if($access[1] == 200){
				$message = 'Page Not Found.'; $status = 404; $data = null;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);	
	}// END SHOW
// EDIT
	public function edit($id) //GET / HEAD FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new CutOfPeriod_Model;
		if($access[1] == 200){
				$message = 'Page Not Found.'; $status = 404; $data = null;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}// END EDIT
// DELETE
	public function destroy($id) // DELETE FIX**
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new CutOfPeriod_Model;
		if($access[1] == 200){
			
				$get = explode(",",$id);
				$i = 1; $a=NULL;
				foreach ($get as $key) {
					if($i == count($get)){ $a .= $key; }
					else{ $a .= $key.",";}
					$i++;
				}
				$data = $model->CALL('delete'," '".$a."'");
				return $data;
				$message=$data[0];	$status=$data[1]; $data = null;
		}else{ $message = $access[0]; $status = $access[1]; $data=$access[2]; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	} // END DELETE
// CHECK ROLE
}
