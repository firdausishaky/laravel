<?php namespace Larasite\Http\Controllers\TimeKeeping;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Model\TimeKeeping\HolidaySetup_Model as Holiday;
use Larasite\Privilege;
use Auth;
use DB;
use Larasite\Library\FuncAccess;
class HolidaySetup_Ctrl extends Controller {


protected $form = 52;
	public function index()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			$data =[];
			$start = date('Y-01-01');
			$end = date('Y-12-31');
			// view holiday from default date and year
			$holiday = \DB::select("CALL View_holiday('$start','$end')");
			if ($holiday){
				foreach ($holiday as $key) {
					if ($key->repeat_annually == 0){
						$repeat = "No";
					}else{
						$repeat = "Yes";
					}
					if ($key->type == 0){
						$type = "Regular";
					}else{
						$type = "Special Holiday";
					}
					// data from database
					$data[] = [
								'id' => $key->id,
								'name' => $key->name,
								'date' => $key->date,
								'repeat_annually' => $repeat,
								'type' => $type,
								'start' => $start,
								'month' => $key->month,
								'day' => $key->day,
								'week' => $key->week,
								'repeat_day' => $key->repeat_day,
								'end' => $end
					];
				}
				if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
					return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $data],200);
				}else{
					return response()->json(['header' => ['message' => 'Show record data' ,'status' => 200, "access" => $access[3]], 'data' => $data],200);
				}
			}
			else{
				if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
					return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, "access" => $access[3]], 'data' => $data],200);
				}else{
					return response()->json(['header' => ['message' => 'Holiday record is empty' ,'status' => 200,  "access" => $access[3]], 'data' => null],200);
				}
			}
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}

	public function store()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/
		if($access[1] == 200){
			$i = \Input::all();
			$repeat_annually = \Input::get('repeat_annually');
			$rule = ['name' => 'required '];
			$validator = \Validator::make(\Input::all(),$rule);
			$temp = [];
			if ($validator->fails()){
				$val = $validator->errors()->all();
				return response()->json(['header' => ['message' => $val[0] ,'status' => 500], 'data' => null],500);
			}
			// check type within regular or Special holiday
			if ($i['type'] == "Regular"){
				$i['type'] = 0;
			}
			else{
				$i['type'] = 1;
			}

			// check holiday name is exist or not
			$isExist = \DB::select("SELECT count(*) as 'found' , id from attendance_holiday where name = '$i[name]' and date = '$i[date]'");
			if ($isExist[0]->found > 0){
				
				return response()->json(['header' => ['message' => "Holiday name and date already exist" ,'status' => 500], 'data' => null],500);
			}else{
				if ($i['repeat_annually'] == "No" || $i['repeat_annually'] == "no"){

					$i['repeat_annually'] = 0;
					$insertHoliday = \DB::select("CALL Insert_Holiday('$i[name]','$i[date]',$i[repeat_annually],$i[type])");
					if ($insertHoliday[0]->Status != "OK"){
						return response()->json(['header' => ['message' => "Cannot insert new holiday" ,'status' => 500], 'data' => null],500);
					}else{
						//return $insertHoliday;
						foreach ($insertHoliday as $key) {

							// if ($repeat_annually == 0){
							// 	$repeat = "No";
							// }else{
							// 	$repeat = "Yes";
							// }
							if ($i['type']  == 0){
								$type = "Regular";
							}else{
								$type = "Special Holiday";
							}
							$temp[] = [
										'id' => $key->id,
										'name' => $key->name,
										'date' => $key->date,
										'repeat_annually' => $repeat_annually,
										'type' => $type
							];


						}

						$count = count($temp)-1;
						if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
							return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, 'access' => $access[3]], 'data' => $temp[$count]],200);
						}else{

							return response()->json(['header' => ['message' => 'Show record data' ,'status' => 200, 'access' => $access[3]], 'data' => $temp[$count]],200);
						}
					}
				}
				else{
					if($i['repeat_annually'] == 'no'){
						$i['repeat_annually'] = 0;
					}else{
						$i['repeat_annually'] = 1;
					}
					$year = date('Y');
					if($i['repeat_annually'] == 1){
						$month = $i['month'];
						$day = $i['repeat_day'];

						if($day  == " "){
							$day = '01';
						}
						$date = $year.'-'.$month.'-'.$day;

						if(isset($i['week']) && isset($i['day'])){
							$insertHolidayRepeat = \DB::select("CALL Insert_Holiday_repeat('$i[name]',$i[repeat_annually],$i[type],'$i[week]','$i[day]','$date')");
						}else{
							$insertHolidayRepeat = \DB::select("CALL Insert_Holiday_repeat('$i[name]',$i[repeat_annually],$i[type],NULL,NULL,'$date')");
						}
						// $insertHolidayRepeat = \DB::select("CALL Insert_Holiday_repeat('$data[name]',$data[repeat_annually],$data[type],$data[month],'$data[day]','$data[week]','$data[repeat_day]','$date')");
						if ($insertHolidayRepeat[0]->Status != "OK"){

							return response()->json(['header' => ['message' => "Can not insert new holiday" ,'status' => 500], 'data' => null],500);
						}
						else
						{

							$temp = [];
							foreach ($insertHolidayRepeat as $key) {
								if ($key->repeat_annually == 0){
									$repeat = "No";
								}else{
									$repeat = "Yes";
								}
								if ($key->type == 0){
									$type = "Regular";
								}else{
									$type = "Special Holiday";
								}

								if( $key->week === null){
									$key->week = "";
								}
								if( $key->day === null){
									$key->day = "";
								}
								$temp[] = [
											'id' => $key->id,
											'name' => $key->name,
											'date' => $date,
											'day' => $key->day,
											'week' => $key->week,
											'repeat_day' => $key->repeat_annually,
											'repeat_annually' => $repeat,
											'type' => $type
								];
							}
							$count = count($temp)-1;
							if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
								return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $temp[$count]],200);
							}else{
								return response()->json(['header' => ['message' => 'Show record data' ,'status' => 200, 'access' =>$access[3]], 'data' => $temp[$count]],200);
							}
						}
					}
				}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
			return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
		}
	}

	public function search(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); /*Model*/
		if($access[1] == 200){
			$evidensi = \Input::all();
			if($evidensi != null){
				$holiday = DB::SELECT("CALL View_holiday('$evidensi[start]','$evidensi[end]')");
				if ($holiday){
				foreach ($holiday as $key) {
					if ($key->repeat_annually == 0){
						$repeat = "No";
					}else{
						$repeat = "Yes";
					}
					if ($key->type == 0){
						$type = "Regular";
					}else{
						$type = "Special Holiday";
					}
					// data from database
					if( $key->week === null){
						$key->week = "";
					}
					if( $key->day === null){
						$key->day = "";
					}
					$data[] = [
								'id' => $key->id,
								'name' => $key->name,
								'date' => $key->date,
								'repeat_annually' => $repeat,
								'type' => $type,
								// 'start' => $evidensi['from'],
								'month' => $key->month,
								'day' => $key->day,
								'week' => $key->week,
								'repeat_day' => $key->repeat_day,
								// 'end' => $evidensi['to']
					];
				}
					if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
						return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $data],200);
					}else{
						return response()->json(['header' => ['message' => 'Show record data' ,'status' => 200, "access" => $access[3]], 'data' => $data],200);
					}
				}
				else
				{
				return response()->json(['header' => ['message' => 'Holiday record is empty' ,'status' => 200], 'data' => null],200);
			}
		}
	}
	else{
		$message = $access[0]; $status = $access[1]; $data=$access[2];
	}
	return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	}

	public function update($id)
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ //$model = new Holiday;
		if($access[1] == 200){
			$year = date('Y');
			$i = \Input::all();
			$rule = [
				'name' => 'required|Regex:/^[A-Za-z0-9 ]+$/',
			];
			$validator = \Validator::make(\Input::all(),$rule);
			if ($validator->fails()){
				$val = $validator->errors()->all();
				return response()->json(['header' => ['message' => $val[0] ,'status' => 500], 'data' => null],500);
			}
			else{

				if ($i['type'] == "Regular"){
					$i['type'] = 0;
				}
				else{
					$i['type'] = 1;
				}
				if ($i['repeat_annually'] == "No"){
					$i['repeat_annually'] = 0;
					$i['day'] = "";
					$i['week'] = "";
					$date = $i['date'];
				}else{
					
					$month = $i['date'];
					$day = $i['repeat_day'];
					
					$date = $year.'-'.$month.'-'.$day;	
						
					$i['repeat_annually'] = 1;
				}
				$explode =  explode("-",$date);
				if(count($explode) >= 3){
				
					$date =  $explode[0].'-'.$explode[1].'-'.$explode[2];
				}

			
				// updateHoliday;
				$isExist = \DB::select("SELECT count(*) as 'found', id from attendance_holiday where name = '$i[name]' and date = '$i[date]'");
				//return [$isExist,"SELECT count(*) as 'found', id from attendance_holiday where name = '$i[name]' and date = '$i[date]'"];
				if ($isExist[0]->found > 0){
					if($isExist[0]->id != (integer)$id){
						return response()->json(['header' => ['message' => "Holiday name and date already exist" ,'status' => 500], 'data' => null],500);
					}else{
						$updateHoliday = DB::SELECT("CALL Update_Holiday($id,'$i[name]','$date',$i[repeat_annually],$i[type],'$i[day]','$i[week]')");
						if ($updateHoliday){
							if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
								return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $data],200);
							}else{
								return response()->json(['header' =>['message' => 'update success', 'status' => 200, "access" => $access[3]],'data' => $updateHoliday[0]], 200);
							}
						}
						else{
							if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
								return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $data],200);
							}else{
								return response()->json(['header' =>['message' => 'Can not update holiday', 'status' => 500, "access" => $access[3]],'data' => null], 500);
							}
						}	
					}
					// return [$isExist[0]];
					// return response()->json(['header' => ['message' => "Holiday name and date already exist" ,'status' => 500], 'data' => null],500);
				}else{
					$updateHoliday = DB::SELECT("CALL Update_Holiday($id,'$i[name]','$date',$i[repeat_annually],$i[type],'$i[day]','$i[week]')");
					if ($updateHoliday){
						if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
							return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $data],200);
						}else{
							return response()->json(['header' =>['message' => 'update success', 'status' => 200, "access" => $access[3]],'data' => $updateHoliday[0]], 200);
						}
					}
					else{
						if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
							return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $data],200);
						}else{
							return response()->json(['header' =>['message' => 'Can not update holiday', 'status' => 500, "access" => $access[3]],'data' => null], 500);
						}
					}
				}
			}
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	public function destroy($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'delete');
		if($access[1] == 200){
			$ids = explode(",",$id);
			if($ids != null){
				foreach($ids as $delete_id){
					DB::SELECT("CALL Delete_Holiday($delete_id)");
				}
				if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
					return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $data],200);
				}else{
					return response()->json(['header' => ['message' => 'delete data success', 'status' => 200, "access" => $access[3]], 'data' => null], 200);
				}
			}else{
				if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
					return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $data],200);
				}else{
					return response()->json(['header' => ['message' => 'data id not found', 'status' => 500, "access" => $access], 'data' => null],500);
				}
			}
		}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return response()->header(['header' => ['message' => $message, 'status' => $status], 'data' => $data], $status);
	}
}


// public function store()
	// {
	// 	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create'); /*Model*/
	// 	if($access[1] == 200){
	// 		$i = \Input::all();
	// 		$repeat_annually = \Input::get('repeat_annually');
	// 		$rule = ['name' => 'required '];
	// 		$validator = \Validator::make(\Input::all(),$rule);
	// 		$temp = [];
	// 		if ($validator->fails()){
	// 			$val = $validator->errors()->all();
	// 			return response()->json(['header' => ['message' => $val[0] ,'status' => 500], 'data' => null],500);
	// 		}
	// 		// check type within regular or Special holiday
	// 		if ($i['type'] == "Regular"){
	// 			$i['type'] = 0;
	// 		}
	// 		else{
	// 			$i['type'] = 1;
	// 		}

	// 		// check holiday name is exist or not
	// 		// $isExist = \DB::select("CALL view_holiday_byname('$i[name]')");
	// 		// if ($isExist){
	// 		// 	return response()->json(['header' => ['message' => "Holiday name already exist" ,'status' => 500], 'data' => null],500);
	// 		// }
	// 		// else{

	// 			if ($i['repeat_annually'] == "No" || $i['repeat_annually'] == "no"){

	// 				$i['repeat_annually'] = 0;
	// 				$insertHoliday = \DB::select("CALL Insert_Holiday('$i[name]','$i[date]',$i[repeat_annually],$i[type])");
	// 				if ($insertHoliday[0]->Status != "OK"){
	// 					return response()->json(['header' => ['message' => "Can not insert new holiday" ,'status' => 500], 'data' => null],500);
	// 				}else{
	// 					//return $insertHoliday;
	// 					foreach ($insertHoliday as $key) {

	// 						// if ($repeat_annually == 0){
	// 						// 	$repeat = "No";
	// 						// }else{
	// 						// 	$repeat = "Yes";
	// 						// }
	// 						if ($i['type']  == 0){
	// 							$type = "Regular";
	// 						}else{
	// 							$type = "Special Holiday";
	// 						}
	// 						$temp[] = [
	// 									'id' => $key->id,
	// 									'name' => $key->name,
	// 									'date' => $key->date,
	// 									'repeat_annually' => $repeat_annually,
	// 									'type' => $type
	// 						];


	// 					}

	// 					$count = count($temp)-1;
	// 					if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
	// 						return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, 'access' => $access[3]], 'data' => $temp[$count]],200);
	// 					}else{

	// 						return response()->json(['header' => ['message' => 'Show record data' ,'status' => 200, 'access' => $access[3]], 'data' => $temp[$count]],200);
	// 					}
	// 				}
	// 			}
	// 			else{
	// 				if($i['repeat_annually'] == 'no'){
	// 					$i['repeat_annually'] = 0;
	// 				}else{
	// 					$i['repeat_annually'] = 1;
	// 				}
	// 				$year = date('Y');
	// 				if($i['repeat_annually'] == 1){
	// 					$month = $i['month'];
	// 					$day = $i['repeat_day'];

	// 					if($day  == " "){
	// 						$day = '01';
	// 					}
	// 					$date = $year.'-'.$month.'-'.$day;

	// 					if(isset($i['week']) && isset($i['day'])){
	// 						$insertHolidayRepeat = \DB::select("CALL Insert_Holiday_repeat('$i[name]',$i[repeat_annually],$i[type],'$i[week]','$i[day]','$date')");
	// 					}else{
	// 						$insertHolidayRepeat = \DB::select("CALL Insert_Holiday_repeat('$i[name]',$i[repeat_annually],$i[type],NULL,NULL,'$date')");
	// 					}
	// 					// $insertHolidayRepeat = \DB::select("CALL Insert_Holiday_repeat('$data[name]',$data[repeat_annually],$data[type],$data[month],'$data[day]','$data[week]','$data[repeat_day]','$date')");
	// 					if ($insertHolidayRepeat[0]->Status != "OK"){

	// 						return response()->json(['header' => ['message' => "Can not insert new holiday" ,'status' => 500], 'data' => null],500);
	// 					}
	// 					else
	// 					{

	// 						$temp = [];
	// 						foreach ($insertHolidayRepeat as $key) {
	// 							if ($key->repeat_annually == 0){
	// 								$repeat = "No";
	// 							}else{
	// 								$repeat = "Yes";
	// 							}
	// 							if ($key->type == 0){
	// 								$type = "Regular";
	// 							}else{
	// 								$type = "Special Holiday";
	// 							}

	// 							if( $key->week === null){
	// 								$key->week = "";
	// 							}
	// 							if( $key->day === null){
	// 								$key->day = "";
	// 							}
	// 							$temp[] = [
	// 										'id' => $key->id,
	// 										'name' => $key->name,
	// 										'date' => $date,
	// 										'day' => $key->day,
	// 										'week' => $key->week,
	// 										'repeat_day' => $key->repeat_annually,
	// 										'repeat_annually' => $repeat,
	// 										'type' => $type
	// 							];
	// 						}
	// 						$count = count($temp)-1;
	// 						if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
	// 							return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $temp[$count]],200);
	// 						}else{
	// 							return response()->json(['header' => ['message' => 'Show record data' ,'status' => 200, 'access' =>$access[3]], 'data' => $temp[$count]],200);
	// 						}
	// 					}
	// 				}
	// 			}
	// 		// }
	// 	}else{
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 		return \Response::json(['header'=>['message'=>$message,'status'=>$status, 'access'=>$access[3]],'data'=>$data],$status);
	// 	}
	// }

// public function update($id)
	// {
	// 	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ //$model = new Holiday;
	// 	if($access[1] == 200){
	// 		$year = date('Y');
	// 		$i = \Input::all();
	// 		$rule = [
	// 			'name' => 'required|Regex:/^[A-Za-z0-9 ]+$/',
	// 		];
	// 		$validator = \Validator::make(\Input::all(),$rule);
	// 		if ($validator->fails()){
	// 			$val = $validator->errors()->all();
	// 			return response()->json(['header' => ['message' => $val[0] ,'status' => 500], 'data' => null],500);
	// 		}
	// 		else{

	// 			if ($i['type'] == "Regular"){
	// 				$i['type'] = 0;
	// 			}
	// 			else{
	// 				$i['type'] = 1;
	// 			}
	// 			if ($i['repeat_annually'] == "No"){
	// 				$i['repeat_annually'] = 0;
	// 				$i['day'] = "";
	// 				$i['week'] = "";
	// 				$date = $i['date'];
	// 			}else{
					
	// 				$month = $i['date'];
	// 				$day = $i['repeat_day'];
					
	// 				$date = $year.'-'.$month.'-'.$day;	
						
	// 				$i['repeat_annually'] = 1;
	// 			}
	// 			$explode =  explode("-",$date);
	// 			if(count($explode) >= 3){
				
	// 				$date =  $explode[0].'-'.$explode[2].'-'.$explode[3];
	// 			}

			
	// 			// updateHoliday;
	// 			$updateHoliday = DB::SELECT("CALL Update_Holiday($id,'$i[name]','$date',$i[repeat_annually],$i[type],'$i[day]','$i[week]')");
	// 			if ($updateHoliday){
	// 				if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
	// 					return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $data],200);
	// 				}else{
	// 					return response()->json(['header' =>['message' => 'update success', 'status' => 200, "access" => $access[3]],'data' => $updateHoliday[0]], 200);
	// 				}
	// 			}
	// 			else{
	// 				if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
	// 					return response()->json(['header' => ['message' => 'Unauthorized' ,'status' => 200, $access => $access[3]], 'data' => $data],200);
	// 				}else{
	// 					return response()->json(['header' =>['message' => 'Can not update holiday', 'status' => 500, "access" => $access[3]],'data' => null], 500);
	// 				}
	// 			}
	// 		}
	// 	}
	// 	else{
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 	}
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	// }
