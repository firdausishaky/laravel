<?php namespace Larasite\Http\Controllers\TimeKeeping;
error_reporting(0);
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Larasite\Model\TimeKeeping\work_shift;
use Larasite\Privilege;
/*MyClass*/
use Larasite\Library\FuncAccess;
use Larasite\Library\FuncUpload;
use Larasite\Library\FuncDB;
use Larasite\Library\FuncParse as parse;
use DB;
use Input;

class WorkShift_Ctrl extends Controller {

	protected $form = 51;
	public function index(){
	  	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read'); 
	  	if($access[1] == 200){
	  		if($access[3]['create'] == 0 && $access[3]['read'] == 0 && $access[3]['update'] == 0 && $access[3]['delete'] == 0){
				return response()->json(['header'=>['message'=>'Unauthorized' , 'status' => 200, 'access'=>$access[3]],'data'=> []],200);
			}else{
		   		$data = DB::SELECT("CALL View_WorkShift");
		   		if($data){
		    		return response()->json(['header'=>['message'=>'Show record data', 'status' => 200, 'access'=>$access[3]],'data'=> $data],200);
		   		}
		   	}
	 	}
	 	else
	 	{
	 		$message = $access[0]; $status = $access[1]; $data=$access[2]; 
	 	}
	  	return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}

	// public function store(){
 //  		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');  
 //  		if($access[1] == 200){
 //      	$shift_code = \Input::get('shift_code');
 //     	$_from = \Input::get('_from');
 //     	$_to = \Input::get('_to');
 //     	$hour_per_day = \Input::get('hour_per_day');
	    
	//     $rule = [
	//        'shift_code' => 'required|Regex:/^[A-Za-z0-9 ]+$/',
	//        '_from' => 'required',
	//        '_to' => 'required',
	//        'hour_per_day' => 'required'];

 //     	$validate = \Validator::make(Input::all(),$rule);
 //     	if($validate->fails()){
	//       	$val = $validate->errors()->all();
	//       	return response()->json(['header' => ['message' => 'failed' , 'status' => $val[0]] , 'data' => null], 400);
 //     	}else{
 //     		$isExist = \DB::select("CALL View_WorkSift_byName('$shift_code')");
 //     		if ($isExist[0]->Status != "Empty"){
 //     			return response()->json(['header' => ['message' => 'Shift code already exist' ,'status' => 500], 'data' => null],500);
 //     		}
 //     		$timeIsExist = \Db::SELECT("select shift_id from attendance_work_shifts where _from = '$_from' and _to = '$_to' ");
 //     		if($timeIsExist[0]->shift_id != null){
 //     			return response()->json(['header' => ['message' => 'time schedule from or to already exist' ,'status' => 500], 'data' => null],500);	
 //     		}
 //     		else{
	// 	      	$data = DB::SELECT("CALL Insert_WorkShift('$shift_code','$_from','$_to','$hour_per_day')");
	// 	      	return response()->json(['header' => ['message' => 'success' ,'status' => 200], 'data' => $data[0]],200);
 //     		}
	//      }
 //  		}else{ 
 //   			$message = $access[0]; $status = $access[1]; $data=$access[2]; 
 //  		}
 //   		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
 // 	}

	// public function store(){
 //  		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');  
 //  		if($access[1] == 200){
 //      	$shift_code = \Input::get('shift_code');
 //     	$_from = \Input::get('_from');
 //     	$_to = \Input::get('_to');
 //     	$hour_per_day = \Input::get('hour_per_day');
	    
	//     $rule = [
	//        'shift_code' => 'required|Regex:/^[A-Za-z0-9 ]+$/',
	//        '_from' => 'required|date_format:H:i',
	//        '_to' => 'required|date_format:H:i',
	//        'hour_per_day' => 'required|numeric'];

 //     	$validate = \Validator::make(Input::all(),$rule);
 //     	if($validate->fails()){
	//       	$val = $validate->errors()->all();
	//       	return response()->json(['header' => ['message' => 'failed' , 'status' => $val[0]] , 'data' => null], 400);
 //     	}else{
 //     		$isExist = \DB::select("CALL View_WorkSift_byName('$shift_code')");
 //     		if ($isExist[0]->Status != "Empty"){
 //     			return response()->json(['header' => ['message' => 'Shift code already exist' ,'status' => 500], 'data' => null],500);
 //     		}
 //     		/*$timeIsExist = \Db::SELECT("select shift_id from attendance_work_shifts where _from = '$_from' and _to = '$_to' ");
 //     		if($timeIsExist[0]->shift_id != null){
 //     			return response()->json(['header' => ['message' => 'time schedule from or to already exist' ,'status' => 500], 'data' => null],500);	
 //     		}*/
 //     		else{
	// 	      	$data = DB::SELECT("CALL Insert_WorkShift('$shift_code','$_from','$_to','$hour_per_day')");
	// 	      	return response()->json(['header' => ['message' => 'success' ,'status' => 200], 'data' => $data[0]],200);
 //     		}
	//      }
 //  		}else{ 
 //   			$message = $access[0]; $status = $access[1]; $data=$access[2]; 
 //  		}
 //   		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
 // 	}

	public function store(){
  		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');  
  		if($access[1] == 200){
      	$shift_code = \Input::get('shift_code');
     	$_from = \Input::get('_from');
     	$_to = \Input::get('_to');
     	$hour_per_day = \Input::get('hour_per_day');
     	$color = \Input::get('colour');
	    
	    

	    $rule = [
	       'shift_code' => 'required|Regex:/^[A-Za-z0-9 ]+$/',
	       '_from' => 'required|date_format:H:i',
	       '_to' => 'required|date_format:H:i',
	       'hour_per_day' => 'required|numeric',
	       'colour' =>'required|Regex:/^[A-Za-z0-9 ,#]+$/'
	       ];

	    $i = Input::all();
	    if(!isset($i['hour_per_day'])){
	    	$i['hour_per_day'] = 0;

	    }
	    if(!isset($i['colour'])){
	    	$i['colour'] = "#000000";
	    }
	    $hour_per_day = $i['hour_per_day'];
	    $color = $i['colour'];

     	$validate = \Validator::make([
     		
     		"shift_code"=>$i['shift_code'],
     		"_from"=>$i['_from'],
     		"_to"=>$i['_to'],
     		"hour_per_day"=>$i['hour_per_day'],
     		"colour"=>$i['colour']

     		],$rule);
     	if($validate->fails()){
	      	$val = $validate->errors()->all();
	      	return response()->json(['header' => ['message' => $val[0]] , 'status' => 500 , 'data' => null], 500);
     	}else{
     		$isExist = \DB::select("CALL View_WorkSift_byName('$shift_code')");
     		if ($isExist[0]->Status != "Empty"){
     			return response()->json(['header' => ['message' => 'Shift code already exist' ,'status' => 500], 'data' => null],500);
     		}
     		/*$timeIsExist = \Db::SELECT("select shift_id from attendance_work_shifts where _from = '$_from' and _to = '$_to' ");
     		if($timeIsExist[0]->shift_id != null){
     			return response()->json(['header' => ['message' => 'time schedule from or to already exist' ,'status' => 500], 'data' => null],500);	
     		}*/
     		else{
		      	$data = DB::SELECT("CALL Insert_WorkShift('$shift_code','$_from','$_to','$hour_per_day','$color')");
		      	return response()->json(['header' => ['message' => 'success' ,'status' => 200], 'data' => $data[0]],200);
     		}
	     }
  		}else{ 
   			$message = $access[0]; $status = $access[1]; $data=$access[2]; 
  		}
   		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
 	}


 	public function destroy($shift_code){
  		/*Access*/
  		$FRA = new FuncAccess; 
  		$access = $FRA->Access(\Request::all(),$this->form,'update'); 
  		/*Model*/ $model = new work_shift;

   		if($shift_code != null){
  			$temp = $this->CheckID($shift_code);
  			if ($temp){
	  			foreach ($temp as $key) {
	  				$delete = \DB::select("CALL Delete_WorkShift($key)");
	  			}
	  			if ($delete[0]->Status == "Success"){
	       			return response()->json(['header' => ['message' => $delete[0]->message ,'status' => 200], 'data' => $data[0]],200);
	       		}
	       		else{
	       			return response()->json(['header' => ['message' => $delete[0]->message ,'status' => 500], 'data' => null],500);
	       		}
  			}
     	}
     	else{
    		return response()->json(['header' => ['message' => 'failed id not exist' ,'status' => 500], 'data' => null],500);
    	}
	}

	// public function update($id){
	// 	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new work_shift;
	// 	if($access[1] == 200){
	// 	// check if id exist 
	// 		if($id != null){
	// 			$shift_code = \Input::get('shift_code');
	// 			$from = \Input::get('_from');
	// 			$to = \Input::get('_to');
	// 			$hour_per_day = \Input::get('hour_per_day');
	// 			 $rule = [
	// 		       'shift_code' => 'required|Regex:/^[A-Za-z0-9 ]+$/',
	// 		       '_from' => 'required|date_format:H:i',
	// 		       '_to' => 'required|date_format:H:i',
	// 		       'hour_per_day' => 'required|numeric'];
	// 			$validate = \Validator::make(Input::all(),$rule);
	// 			//check validation
	// 			if($validate->fails()){
	// 				$val = $validate->errors()->all();
	// 				return response()->json(['header' => ['message' => 'failed' , 'status' => $val[0] ], 'data' => null], 400);
	// 			}else{
	// 				// $isExist = \DB::select("CALL View_Workshift_id($id)");
	// 				// if ($isExist){
	// 				// 	$temp = $isExist[0]->shift_code;
	// 				// }
	// 				// else{
	// 				// 	$temp = $shift_code;
	// 				// }
					
	// 				// $isExist = \DB::select("CALL View_Workshift_idname('$shift_code', $id)");
	// 				// if ($isExist[0]->Status == "Duplicate"){
	// 				// 	return response()->json(['header' => ['message' => 'Shift code already exist' ,'status' => 500], 'data' => null],500);
	// 				// }
	// 				$data = DB::SELECT("CALL Update_WorkShift('$shift_code','$from','$to','$hour_per_day',$id)");
	// 				return response()->json(['header' => ['message' => 'success' ,'status' => 200], 'data' => $data[0]],200);
	// 			}
	//    		}
 //  		}
 //  		else
 //  		{ 
 //   			$message = $access[0]; $status = $access[1]; $data=$access[2];
 //  		}
 //  		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
 // 	}

	public function update($id){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'update'); /*Model*/ $model = new work_shift;
		if($access[1] == 200){
		// check if id exist 
			if($id != null){
				$shift_code = \Input::get('shift_code');
				$from = \Input::get('_from');
				$to = \Input::get('_to');
				$hour_per_day = \Input::get('hour_per_day');
				$color = \Input::get('colour');
				 $rule = [
			       'shift_code' => 'required|Regex:/^[A-Za-z0-9 ]+$/',
			       '_from' => 'required|date_format:H:i',
			       '_to' => 'required|date_format:H:i',
			       'hour_per_day' => 'required|numeric',
			       'colour' =>'required|Regex:/^[A-Za-z0-9 ,#]+$/'
			       ];
				$validate = \Validator::make(Input::all(),$rule);
				//check validation
				if($validate->fails()){
					$val = $validate->errors()->all();
					return response()->json(['header' => ['message' => 'failed' , 'status' => $val[0] ], 'data' => null], 400);
				}else{
					// $isExist = \DB::select("CALL View_Workshift_id($id)");
					// if ($isExist){
					// 	$temp = $isExist[0]->shift_code;
					// }
					// else{
					// 	$temp = $shift_code;
					// }
					
					// $isExist = \DB::select("CALL View_Workshift_idname('$shift_code', $id)");
					// if ($isExist[0]->Status == "Duplicate"){
					// 	return response()->json(['header' => ['message' => 'Shift code already exist' ,'status' => 500], 'data' => null],500);
					// }
					$data = DB::SELECT("CALL Update_WorkShift('$shift_code','$from','$to','$hour_per_day',$id,'$color')");
					return response()->json(['header' => ['message' => 'success' ,'status' => 200], 'data' => $data[0]],200);
				}
	   		}
  		}
  		else
  		{ 
   			$message = $access[0]; $status = $access[1]; $data=$access[2];
  		}
  		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
 	}
 
 	// CHECK ID
  	private function CheckID($id)
  	{	
  		$rule = ['','undefined',NULL];
  		if(!(in_array($id,$rule))){ 
  			$data = explode(",",$id);
  			// $data = $id; 
  		}
  		else{ 
  			$data = "null"; 
  		}
  		return $data;
  	}
}