<?php namespace Larasite\Http\Controllers;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Larasite\Todo_Model
use JWTAuth;

class Todos_Ctrl extends Controller {

	public function __construct()
	{
		$this->middleware('jwt.auth',['except'=>['index']]);
	}

	public function index()
	{
		$user = JWTAuth::parseToken()->authenticate();
		$todos = Todo_Model::where('owner_id',$user->id)->get();
		return  $todos;
	}

	public function store(\Request $request)
	{
		$user = JWTAuth::parseToken()->authenticate();
		$newTodo = $request->all();
		$newTodo['owner_id'] = $user->id;
		return Todo_Model::create($newTodo);
		$todos = Todo_Model::where('owner_id',$user->id)->where('id',$id);
	}

	public function update($id)
	{
		$user = JWTAuth::parseToken()->authenticate();
		$todo = Todo_Model::where('owner_id',$user->id)->first();
		if($todo){
			$todo->is_done = $request->input('is_done');
			$todo->save();
			return $todo;
		}else{
			return \Response('Unauthorized',403);
		}
	}

	public function destroy($id)
	{
		$user = JWTAuth::parseToken()->authenticate();
		$todo = Todo_Model::where('owner_id',$user->id)->where('id',$id)->first();

		if($todo){
			Todo_Model::destroy($user->id);
			return \Response('Success',200);
		}else{
			return \Response('Unauthorized',403);
		}
	}
}
