<?php namespace Larasite\Http\Controllers\UserManage;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;

// SETUP MODEL
use Larasite\Model\CustomRole_Model;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
class CustomRole_Ctrl extends Controller {


protected $form_id = 2 ;
protected $form = 2 ;


	private function res_error(){
		return \Response::json(['header'=>['message'=>'data not exists'],'data'=>null],404);
	}

	public function get_base(){
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);
		$model = new CustomRole_Model;
		
		if(isset($get_role['data'])){
			if(isset($data_role['read']) && $data_role['read'] != 0){
				
				$result = $model->get_base();
				if($result){
					$data = $result; $status = 200; $message='Custome Role : View data';
				}else{ $message='Custome Role : Data not found.'; $status=200; $data=null; }
			
			}else{ $message = 'Unauthorized'; $status = 401; $data = null; }
		}else{ $message = $get_role['message']; $status = 403; $data = null; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data], $status);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'create');
		if($access[1] == 200){
				if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$data = []; $message = 'Unauthorized'; $status = 200;
				}else{
					$model = new CustomRole_Model;
					$result = $model->get_custome();
					if($result){
						$data = $result; $status = 200; $message='Custome Role : View data';
					}else{ $message='Custome Role : Data not found.'; $status=200; $data=null; }
				}
			}else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}
	public function create()
	{
		//
	}
	public function store()
	{
		$j=0; $i=0; $page = array();
		$model = new CustomRole_Model;

		$data_customes = json_decode(file_get_contents("php://input"));

		$rule = [
			"add_employee" => "required|numeric",
			"role_id" 		=> "required|numeric",
			"role_name" => "required|Regex:/^[A-Za-z0-9\-! ]+$/",
			"terminate" => "required|numeric"
		];

		$valid = \Validator::make([
				"add_employee" => $data_customes->data[0]->add_employee,
				"role_id" => $data_customes->data[0]->role_id,
				"role_name" => $data_customes->data[0]->role_name,
				"terminate" => $data_customes->data[0]->terminate,
			],$rule);
		if($valid->fails()){
			return $this->res_error();
		}
		$generate_id = $model->Get_ID();

		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);
		
		if(isset($get_role['data'])){
			if($data_role['create'] && $data_role['create'] != 0){
				
				$data_custome = json_decode(file_get_contents("php://input"));
				foreach ($data_custome as $jsons) {
					$datax['form'] =  $jsons[0]->form;
					$datax['role_name'] =  $jsons[0]->role_name;
					$datax['add_employee'] =  $jsons[0]->add_employee;
					$datax['terminate'] =  $jsons[0]->terminate;
					if($jsons[0]->localit == 'all'){ $datax['localit'] = 2; }
					elseif($jsons[0]->localit == 'local'){ $datax['localit'] = 1; }
					elseif($jsons[0]->localit == 'expat'){ $datax['localit'] = 0; }
					else{ $datax['localit'] = NULL; }
				}
				
				$store = $model->StoreCustome($datax,$generate_id,1);
				
				if($store['data'] && $store['status'] == 200){
					$message=$store['message']; $status=$store['status']; $data=$store['data'];
				}else{ $message=$store['message']; $status=$store['status']; $data=$store['data']; }

			}else{$message = 'Unauthorized';$status = 401;$data = null;}// end check role create
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
		else{ return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403);}
	}
	
	public function show($id)
	{	

		$id = (integer)$id;
		$rule = [ "id" => "required|Regex:/^[0-9-\^]+$/" ];

		$valid = \Validator::make(["id"=>$id],$rule);
		if($valid->fails()){
			return $this->res_error();
		}

		if(!is_numeric($id)){
			return $this->res_error();
		}


		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);
		$model = new CustomRole_Model;
		if($get_role['data']['role']){
			if($data_role['read'] && $data_role['read'] != 0){

			$result = $model->Read2Store($id);
			//return [$result,2];			

			//$result = $model->ReadCustome();
				if($result){
					$data = json_decode($result); $status = 200;
				}else{ $message=$result['message']; $status=$result['status']; $data=null; 
					return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data], $status);
				}
			
			}else{ $message = 'Unauthorized'; $status = 401; $data = null; }
		}else{ $message = $get_role['message']; $status = 403; $data = null; }
		return \Response::json($data, $status);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// SELECT ALL DATA
private function select_all_data($id){
	$toArray = explode(",", $id);
	$model = new CustomRole_Model;
	
	$destroy = $model->DestroyCustomeRole($toArray);
	
	if(isset($destroy) && $destroy['fix'] == 200) {$status =200;
	}else{$status = 500; }
	return $status;
}
	public function update_custome($id)
	{

		$j=0; $i=0; $page = array();
		$model = new CustomRole_Model;

		$data_customes = json_decode(file_get_contents("php://input"));
		
		$rule = [
			"add_employee" => "numeric",
			"role_id" 		=> "required|numeric",
			"role_name" => "required|Regex:/^[A-Za-z0-9\-! ]+$/",
			"terminate" => "required|numeric",
			"id"=> "numeric"
		];

		$valid = \Validator::make([
				"add_employee" => $data_customes->data[0]->add_employee,
				"role_id" => $data_customes->data[0]->role_id,
				"role_name" => $data_customes->data[0]->role_name,
				"terminate" => $data_customes->data[0]->terminate,
				"id"=>$id
			],$rule);
		if($valid->fails()){
			//return $this->res_error();
		}

		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);
		
		if(isset($get_role['data'])){
			if($data_role['create'] && $data_role['create'] != 0){
				
				if($model->UpdateRole($id)){
					$data_custome = json_decode(file_get_contents("php://input"));
					
					foreach ($data_custome as $jsons) {
						$datax['form'] =  $jsons[0]->form;
						$datax['role_name'] =  $jsons[0]->role_name;
						$datax['add_employee'] =  $jsons[0]->add_employee;
						$datax['terminate'] =  $jsons[0]->terminate;
						if($jsons[0]->localit == 'all'){ $datax['localit'] = 2; }
						elseif($jsons[0]->localit == 'local'){ $datax['localit'] = 1; }
						elseif($jsons[0]->localit == 'expat'){ $datax['localit'] = 0; }
						else{ $datax['localit'] = NULL; }
					}
					$store = $model->StoreCustome($datax,$id,2);
					
					if($store['data'] && $store['status'] == 200){
						$message=$store['message']; $status=$store['status']; $data=$store['data'];
					}else{ $message='Custome Role : Update Successfully.'; $status=$store['status']; $data=$store['data']; }	
				}else{
					$data_custome = json_decode(file_get_contents("php://input"));
					foreach ($data_custome as $jsons) {
						$datax['form'] =  $jsons[0]->form;
						$datax['role_name'] =  $jsons[0]->role_name;
						$datax['add_employee'] =  $jsons[0]->add_employee;
						$datax['terminate'] =  $jsons[0]->terminate;
						if($jsons[0]->localit == 'all'){ $datax['localit'] = 2; }
						elseif($jsons[0]->localit == 'local'){ $datax['localit'] = 1; }
						elseif($jsons[0]->localit == 'expat'){ $datax['localit'] = 0; }
						else{ $datax['localit'] = NULL; }
					}

					$store = $model->StoreCustome($datax,$id,1);
					
					if($store['data'] && $store['status'] == 200){
						$message=$store['message']; $status=$store['status']; $data=$store['data'];
					}else{ $message=$store['message']; $status=$store['status']; $data=$store['data']; }
				}
				
			}else{$message = 'Unauthorized';$status = 401;$data = null;}// end check role create
			// RESPONSE
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
		else{ return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403);}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$get_role = $this->check();
		$data = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);
		$model = new CustomRole_Model;
		if($get_role['data']['role']){
			if($data['delete'] != 0 or $data['delete'] != null){
				
				$toArray = explode(",", $id);

				$destroy = $model->DestroyCustomeRole($toArray);
				
				if(isset($destroy) && $destroy['fix'] == 200) { $message = $destroy['message']; $status =200;
				}else{ $message = $destroy['message']; $status = 500; }
			}
			else{$message = 'Unauthorized'; $status = 401; }
			return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
		}
		else{ return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403); }
	}
/* END CRUD */



	/* SETUP INPUT */
	public function SetInputRole($role_id){
		$input = array();

		$input['role_id']		= $role_id + 1;
		$input['role_name'] 	= \Input::get('role_name');
		if(empty($input['role_name'])){ $message='Input not null.'; $status=500; $data=null;
		}else{ $message='Input ready.'; $status=200; $data=$input;}
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
	public function SetInputPermission(){
		$input = array();

		$input['role_base']		= \Input::get('role_base');
		if(empty($input['role_base'])){ $message='Input not null.'; $status=500; $data=null;
		}else{ $message='Input ready.'; $status=200; $data=$input;}
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
/* END SETUP INPUT */

/* --- METHOD CHECK ACCESS ---*/ 
	
	/* CHECK ROLE */
	public function check_role($role,$employee_id,$form) // CHECK ROLE FIX***
	{
		$getModel = new Privilege;
		if(isset($role,$form)){return $getModel->check_role_personal2($role,$employee_id,$form);}
		else{$msg['warning'] = 'Unauthorize'; $msg['status'] = 404; return $msg; }
	}
/* END CHECK ROLE */
	/* CHECK ACCESS */
	public function check(){
		$getModel = new Privilege;	$r = array();	$req = \Request::all();
		if(isset($req['key'])){ 
			$q = $getModel->session_key($req['key']);
			if(!empty($q)){
						
						/* FETCH DATA KEY */
						foreach ($q as $keys) { $r['key'] = $keys->session_key; } // end for store role to array.
						$decode = base64_decode($r['key']);
						$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
						$key = substr($decode,0,strpos($decode,'-'));
							
						$q2 = $getModel->session_role($employee_id);
						foreach ($q2 as $keys) { $r['employee_id'] = $keys->employee_id; $r['role_id'] = $keys->role_id; } // end for store role to array.				

				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) { $role['role'] = $key->role_id; }

					if(isset($role['role'])){ $data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']]; $msg = 'ACCESS GRANTED !';	
					}else{ $msg = 'ACCESS DENIED!';$data = null;}
				}else{ $msg = 'ACCESS DENIED!'; $data = null;}
			}else{ $msg = 'ACCESS DENIED !';$data = null;}//end if check
		}else{ $msg = 'ACCESS DENIED, KEY URL NOT FOUND !'; $data = null;} // end if isset $req
		return ['message'=>$msg,'data'=>$data]; // RESPONSE
	}
/* END CHECK ACCESS KEY */


}
