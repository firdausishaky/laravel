<?php namespace Larasite\Http\Controllers\UserManage;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\SystemUser_Model;
use Larasite\Library\FuncAccess;
class SystemUser_Ctrl extends Controller {
protected $form_id = 1;
protected $form = 1;

public function checkID($id)
{
	$rule = ['undefined',NULL,''];
	if(in_array($id,$rule)){ $data = ['ID Undefined',500,NULL]; }
	else{ $data = ['OK',200,NULL]; }
	return $data;
}

/* --- METHOD CRUD --- */

	/* INDEX */
	public function index() // GET FIX***
	{	
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
		
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
					$message='Unauthorized'; $status=200; $data=null;
			}else{
				$data_table = new SystemUser_Model;
					$result = $data_table->Read_SystemUser(1,null);
					if(isset($data) != '[]' || isset($data['status']) == 200){
						$message = 'Views Data'; $status = 200; $data = $result['data'];
					}else{ $message='Data Problem'; $status=500; $data=null; }
			}
          		 }else{
          		 	 $message = $access[0]; $status = $access[1]; $data=$access[2];
               	}
               		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
               }
   	 
/* END INDEX */

	/* STORE */
	public function store() // POST FIX***
	{
		$model = new SystemUser_Model;
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);
		if(isset($get_role['data']['role'])){
			if(isset($data_role['create']) && $data_role['create'] != 0){


				$role_id = \Input::get('role');
				$input['active_access'] = \Input::get('status');
				$input['role_id'] = \Input::get('role');
				$input['employee_id'] = \Input::get('employee_id');
				if(isset($input) != null && $input['active_access'] != null){
				
					if($input['active_access'] == 'Enabled'){
						$input['active_access'] = 1;
					}else{ $input['active_access'] = 0; }
					
					$filter_id = $input['employee_id'];
					$filter = \DB::SELECT("select * from view_users where employee_id = '$filter_id' ");
					if($filter){
						$status = 500;$message = 'Data already exist.';$data = null;
					}else{
						$result = $model->Store_SystemUser($input); // store to model
						$id_ldap = $model->Get_ID($input['employee_id']);
						
						if($result){
							$status = 200; $message = 'Store Successfully.';
							$data_res = $model->Read_SystemUser(2,$id_ldap);
							$data = $data_res['data'];
							$message = 'Store Successfully.';
						}
						else{$status = 500;$message = 'Store Not Successfully.';$data = null;}
					}
				}else{$status = 500;$message = 'Input Null.';$data = null;}// end check input not null
			}else{$message = 'Unauthorized';$status = 401;$data = null;}// end check role create
			
			// RESPONSE
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}
		else{ return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403);}
	}
/* END STORE */

	/* SHOW */
	public function show($id) // GET FIX***
	{
		$data_model = new SystemUser_Model;
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);
		
		if(isset($get_role['data']['role'])){
			if(isset($data_role['create']) && $data_role['create'] != 0){
				if($data_model->Read_SystemUser('Sort',$id)){
					$status = 200;
					$data = $data_model->Read_SystemUser('Sort',$id)[0];
					$message = 'Show Data.';
				}
				else{$status = 404; $data = null; $message = 'Data not found';}
			}else{ $message = 'Unauthorized'; $data = null; $status = 401; }

			// RESPONSE
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}else{ return \Response::json(['header'=>['message'=>$get_role['message'],'access'=>$msg,'status'=>403]],403);}
	}
/* END SHOW */

	/* EDIT */
	public function edit($id) //GET / HEAD FIX**
	{
		$data_model = new SystemUser_Model;
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);
		
		if(isset($get_role['data']['role'])){
			if(isset($data_role['update']) && $data_role['update'] != 0){
				if($data_model->Read_SystemUser('Sort',$id)){
					$status = 200;
					$data = $data_model->Read_SystemUser('Sort',$id)[0];
					$message = 'Show Data Selected.';
				}
				else{$status = 404;$data = null;$message = 'Data not found';}
			}else{$message = 'Unauthorized';$data = null;$status = 401;}

			// RESPONSE
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}else{ return \Response::json(['header'=>['message'=>$get_role['message'],'access'=>$msg,'status'=>403]],403);} // END EDIT
	}
/* EDIT */

	/* UPDATE */
	public function update($id) // PUT FIX***
	{
		$model = new SystemUser_Model;
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);
		$input_status = \Input::get('status');
		if(isset($get_role['data']['role'])){

			if(isset($data_role['update']) != null && $data_role['update'] != 0){

				//$input = $this->SetInput();
				$input['employee_id'] = $model->Get_Emp(\Input::get('domain_name'));
				if(\Input::get('role')){ $input['role_id'] = \Input::get('role'); }
				$input['active_access'] = \Input::get('status');
				if(isset($input) != '[]' || $input['active_access'] != null){
					
					if($input['active_access'] == 'Enabled'){
						$input['active_access'] = 1;
					}else{ $input['active_access'] = 0; }
					$update = $model->Update_SystemUser($id,$input);
					if($update){
						$status = 200; $message='Update Successfully.';
						$data_res = $model->Read_SystemUser(2,$model->Get_ID($input['employee_id']));
						$data = $data_res['data'];
					}
					else{$status = 500;$message = 'Update Failed.'; $data=null;}
				}else{ $message = 'Error Input not null.'; $status = 406; $data=null; }
			}else{ $message = 'Unauthorized' ; $status = 401; $data=null;}
			return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
		}else{ return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403);} // END UPDATE
	}
/* END UPDATE */
	
	/* DESTROY */
	public function destroy($id) // DELETE
	{
		$get_role = $this->check();
		$data = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);
		$model = new SystemUser_Model;
		if(isset($get_role['data']['role'])){
			if(isset($data['delete'])){
				$toArray = explode(",", $id);
				$data = $model->Destroy_SystemUser($toArray);
				if(isset($data) == 200){ $message = 'Destroy Successfully.'; $status = $data;	
				}else{ $message = 'ID not found'; $status = 404;}
			}else{ $message = 'Unauthorized'; $status = 401;}
			
			// RESPONSE
			return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
		}else{ return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>403]],403); }
	}
/* END DESTROY */

	/* GET ROLE */ 
	// FOR SELECT Role //
	public function Get_Role(){ 
		$data = \DB::table('role')->get(['role_id','role_name']); $status=200;
		foreach ($data as $key => $value) {
			$arr = ["1","2","4","5","8","16","32"];
			if(in_array($value->role_id,$arr)){
				unset($data[$key]);
			}
		}
		return \Response::json($data, $status);
	}
/* END GET ROLE */

/* --- END METHOD CRUD --- */
// ############################################################################################
// #############################################################################################
/* --- METHOD SET INPUT */
	
	/* SET INPUT */
	public function SetInput(){
		$input = array(); $get_emp = \Input::get('name');
		$model = new SystemUser_Model;
		$input['employee_id']	= $model->Get_Emp($get_emp);
		$input['status'] 		= \Input::get('status');
		$input['role_name']		= \Input::get('role_name');

		$data2['domain_name'] 	= $model->Get_Name($get_emp);
		$data2['role_name']		= \Input::get('role_name');
		
		if(empty($input)){ $message='Input not null.'; $status=500; $data=null; $datas=null;
		}else{ $message='Input ready.'; $status=200; $data=$input; $datas = $data2;}
		
		return ['message'=>$message,'status'=>$status,'data'=>$data,'datas'=>$datas];
	}
/* END SET INPUT */ 
/* --- END METHOD SET INPUT --- */
// ##############################################################################
//###############################################################################
/* --- METHOD SEARCH  --- */

	/* SEARCH DOMAIN */
	public function Search_Domain(){
		$model = new SystemUser_Model;
		$string = \Input::get('domain');
		$search=$model->Search_Domain($string);
		if(isset($search) != null || $search['status'] == 200){ $message='Get domain.'; $status=$search['status']; $data=$search['data']; }
		else{ $status=$search['status']; $data=$search['data']; $message='Domain not found.'; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
/* END SEARCH DOMAIN */
/* --- END METHOD SEARCH --- */
//#############################################################################
//##############################################################################
/* --- METHOD CHECK ACCESS ---*/ 
	
	/* CHECK ROLE */
	public function check_role($role,$employee_id,$form) // CHECK ROLE FIX***
	{
		$getModel = new Privilege;
		if(isset($role,$form)){return $getModel->check_role_personal2($role,$employee_id,$form);}
		else{$msg['warning'] = 'Unauthorize'; $msg['status'] = 404; return $msg; }
	}
/* END CHECK ROLE */
	/* CHECK ACCESS */
	public function check(){
		$getModel = new Privilege;	$r = array();	$req = \Request::all();
		if(isset($req['key'])){ 
			$q = $getModel->session_key($req['key']);
			if(!empty($q)){
						
						/* FETCH DATA KEY */
						foreach ($q as $keys) { $r['key'] = $keys->session_key; } // end for store role to array.
						$decode = base64_decode($r['key']);
						$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
						$key = substr($decode,0,strpos($decode,'-'));
							
						$q2 = $getModel->session_role($employee_id);
						foreach ($q2 as $keys) { $r['employee_id'] = $keys->employee_id; $r['role_id'] = $keys->role_id; } // end for store role to array.				

				if($r['employee_id'] == $employee_id){ // if type decode or encode
					$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
					foreach ($datas as $key) { $role['role'] = $key->role_id; }

					if(isset($role['role'])){ $data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']]; $msg = 'ACCESS GRANTED !';	
					}else{ $msg = 'ACCESS DENIED!';$data = null;}
				}else{ $msg = 'ACCESS DENIED!'; $data = null;}
			}else{ $msg = 'ACCESS DENIED !';$data = null;}//end if check
		}else{ $msg = 'ACCESS DENIED, KEY URL NOT FOUND !'; $data = null;} // end if isset $req

		// RESPONSE
		return ['message'=>$msg,'data'=>$data];
	}
/* END CHECK ACCESS KEY */
//############################################################################
// SEARCH JOB #######################################
	// public function Search($string){ 
	// 	$check_lastName = strpos($string," "); 
	// 	$last_name = substr($string, $check_lastName+1, strpos($string,' '));

	// 	if($last_name){
	// 		if(isset($string)){
	// 			$first_name = substr($string, 0, strpos($string,' ')); $last = $last_name;
	// 			$data = \DB::select("select ad_username as domain_name 
	// 					from emp where domain LIKE '$string%' limit 10");
	// 		}else{ return ['data'=>null,'status'=>500]; }
	// 	}else{
	// 		$data = \DB::select("select ad_username as domain_name from emp where domain LIKE '$string%' limit 10");
	// 		$status=200;
	// 	}
	// 	return ['data'=>$data,'status'=>$status];

		// if($last_name){
		// 	if(isset($string)){
		// 		$first_name = substr($string, 0, strpos($string,' ')); $last = $last_name;
		// 		$data = \DB::select("select ad_username as domain_name 
		// 				from emp where domain LIKE '$string%' limit 10");
		// 	}
		// 	else{ $data = null; $status=500;}
		// }else{ $data = \DB::select("SELECT ad_username as domain from emp where first_name LIKE '$string%' limit 5"); $status = 200;}
		// if(isset($data) != null || isset($data) != '[]'){ $status=200; }else{ $data=null; $status=500; }
		// return ['data'=>$data,'status'=>$status];
	//}
// END SEARCH DOMAIN NAME ###################################
// SEARCH DOMAIN
	public function Searching(){
		$get_role = $this->check();	
		$r = \Request::all();
		if($r['key'] && $r['search'] != null && $r['search'] != 'undefined'){
		$decode = base64_decode($r['key']);
		$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
		$name = $r['search'];
		}else{ return \Response::json('Access Denied',500); }
		
		if(isset($get_role['data']['role'])){ // CHECK KEY
				$access = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$this->form_id);; // GET ACCESS
				if(isset($access['create']) && $access['create'] != 0){ // CHECK ACCESS
					$show = \DB::select("select employee_id ,ad_username as domain_name from emp where ad_username LIKE '$name%' and employee_id != '$id' and employee_id != '' ");
						if($show){
							$data=$show; $status=200;
						}else{ $data=[['domain_name'=>'Data not found.']]; $status=200;}
				}else{ $message=$access['message'];	$status=$access['status']; $data=null; }
		}
		else{ $message = $get_role['message']; $status=401; $data=null;}
		return \Response::json($data,$status);
	}// END INDEX #########################################################################################
// END SEARCH DOMAIN
}
