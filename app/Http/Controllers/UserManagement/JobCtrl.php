<?php namespace Larasite\Http\Controllers\UserManagement;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Privilege;
use Illuminate\Http\Request;
use Larasite\Model\Model_UserManagement;
use Larasite\Model\Jobs_Model;
use Illuminate\Support\Str;
use upload_class\upload_class;

class JobsCtrl extends Controller {

protected $form_id = 3;
	/**
	 * Display a listing of the resource.
	 *PAGE ADMIN.JOB
	 *ID = 3
	 *Role = 2 ***Testing Postman
	 *MODEL MODEL_USERMANAGEMENT
	 * @return Response
	 */
	
	public function index()
	{
		// startup
		//$check = \Cookie::get('auth');
		$check = \Cookie::get('XSRF-TOKEN');
		$role=1;$form=3;$data = $this->check_role($role,$form);
		if($data['create'] == 1 && $check){
			$data_table = new Model_UserManagement;
			return \Response::json(['name'=>$data['role_name'],'key'=>csrf_token(),'Table_Jobs'=>$data_table->ReadJob('All',null)]);
		}
		else{
			return \Response::json(['name'=>$data['role_name'],'access'=>'Unauthorize','status'=>403]);
		}
	}

	public function check_role($role,$form)
	{
		$getModel = new Privilege;
		$token = \Cookie::get('XSRF-TOKEN');
		//$user 		= base64_decode(\Cookie::get('id'));
		if($token){
			return $getModel->check_role($role,$form);
		}
		else{
			$msg['warning'] = 'Unauthorize';
			$msg['status'] = 404;
			return $msg; 
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		//$check = \Cookie::get('auth');
		$id = \Cookie::get('XSRF-TOKEN');
		$encrypt = base64_encode($id."-hr.admin");
		$decode = base64_decode($encrypt);
		$str = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
		return $str;
	}
	public function store_upload(){
		$file = \Input::file('file');
		if(!$file){
			return $this->store();
		}
		else{
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() // POST JOBS
	{
		//
		//$DB = new App\Model\Model_UserManagement;
		$check = \Cookie::get('XSRF-TOKEN');
		$data = $this->check_role(1,3);
		
		if($data['create'] && $data['create'] == 1){
			if($check){
				$data_model = new Model_UserManagement;
				// $path = public_path().'/hrms/upload/picture';
				// $allowed = $file->getClientOriginalExtension();
				// $ext = ['doc','docx','pdf'];
				// if( in_array($allowed,$ext)){
				// 	$file_name = 'testestest.doc';
				// 	$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);
				// 	$str = str_slug($slice_FileName,$separator = '_');
				// 	$file->move($path,$str.".".$allowed);
				// 	return "FILENAME RENAME : SUKSES : ".$str.".".$allowed;
				// }
				// else{
				// 	return 'error file type';
				// }
				// $name = $file->getClientMimeType();
				// $file->move($path,'asdsad.'.$name);
				//$data_model->JobStore($id,$title,$descript);
				
				$id = \Input::get('id'); $title = \Input::get('title'); $file = \Input::file('file');
				$up = new  upload_class;
				$name = $up->upload_job($file->getClientOriginalName());
				$data_file	= $name." ".$this->formatSizeUnits($file->getClientSize());
				$msg = $data['create'];
			}
			else{
				$msg = 'Unauthorize';
			}
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'status'=>200,'data_form'=>$data_file],200);
		}
		else{
			$msg = 'Unauthorize';
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'status'=>403],403);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) // GET
	{
		/*
		* Show data for table SystemUser
		* Model/Model_UserManagement.php
		*/
		$check = \Cookie::get('XSRF-TOKEN');
		$data = $this->check_role(1,3);
		if($data['read'] && $data['read'] == 1){
			if($check){
				$data_table = new Model_UserManagement;
				$msg = $data['read'];
			}
			else{
				$msg = 'Unauthorize';
			}
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'data'=>$data_table->ReadJob('Sort',$id),'status'=>200],200);
		}
		else{
			$msg = 'Unauthorize';
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'status'=>403],403);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) //GET / HEAD
	{
		$check = \Cookie::get('XSRF-TOKEN');
		$data = $this->check_role(1,3);
		
		if($data['update'] && $data['update'] == 1){
			if($check){
				$data_table = new Model_UserManagement;
				$msg = $data['update'];
			}
			else{
				$msg = 'Unauthorize';
			}
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'data'=>$data_table->TableJob('Sort',$id),'status'=>200],200);
		}
		else{
			$msg = 'Unauthorize';
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'status'=>403],403);
		}

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) // PUT
	{
		$check = \Cookie::get('XSRF-TOKEN');
		$data = $this->check_role(1,3);
		
		if($data['update'] && $data['update'] == 1){
			if($check){
				
				$data_table = new Model_UserManagement;
				$title = \Input::get('title');
				$descript = \Input::get('descript');
				$update = $data_table->JobUpdate($id,$title,$descript);
				$msg = $data['update'];
			}
			else{
				$msg = 'Unauthorize';
			}
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'status'=>200,'event'=>$update],200);
		}
		else{
			$msg = 'Unauthorize';
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'status'=>403],403);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) // DELETE
	{
		//
		$check = \Cookie::get('XSRF-TOKEN');
		$data = $this->check_role(1,3);
		if($data && $data['delete']==1){
			if($check){
				$destroy_model = new Model_UserManagement;
				$destroy_model->JobDestroy($id);
				$msg = $data['delete'];
			}
			else{
				$msg = 'Unauthorize';
			}
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'status'=>200],200);
		}
		else{
			$msg = 'Unauthorize';
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'status'=>403],403);
		}
	}

	public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
	}
}
