<?php namespace Larasite\Http\Controllers\UserManagement;


use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Privilege;
use Illuminate\Http\Request;
use Larasite\Model\Model_UserManagement;
use autonumb_class;
class SystemUserCtrl extends Controller {

var $form_id = 1;
var $status200 = 200;
var $status403 = 403;
var $status401 = 401;
var $status204 = 204;
var $message_get = 'View data.';
var $message_unauthorized = 'Unauthorized.';
var $message_store = 'Store data successfully.';
var $message_update = 'Update data successfully.';
var $message_destroy = 'Destroy data successfully.';

	/**
	 * Display a listing of the resource.
	 *PAGE ADMIN.SYSTEM USER && ADMIN.CUSTOM ROLE
	 *MODEL MODEL_USERMANAGEMENT
	 * @return Response
	 */

	

	public function post_json(){
		$data = \Input::get('data');
		return $data;
		$r = \Request::input('data');
		//$json_encode = json_encode($r);
		$json = strip_tags(str_replace("jQuery.fs['scoreboard'].data =","",$r));
		$json_decode = json_decode($json,true);
		return count($json_decode['permissions']);
	}


	private function ModeltoJSON($dir,$data,$fileName,$modeFile){
		$make = fopen($dir.$fileName, $modeFile);
		fwrite( $make , $data );
		fclose($make);
		return $make;
	}

	public function make_json(){
		$i = 1;
		$add = null;
		$terminate = null;
		$executive = \DB::select("select d.form_name as page, c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id = 1");
		$executive_value = \DB::select("select c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id = 1");
		$executive_page = \DB::select("select d.form_name
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id = 1");
		$administrator = \DB::select("select d.form_name as page, c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id = 2");
		$administrator_page = \DB::select("select d.form_name
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id = 2");
		$admin = \DB::select("select d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id = 4");
		$hr = \DB::select("select d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id = 8");
		$it_admin = \DB::select("select d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id = 16");
		$accounting = \DB::select("select d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id = 32");
		$accounting = \DB::select("select d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id = 32");
		$accounting_role = \DB::select("select distinct c.role_id,a.role_name
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id");
		$accounting_form = \DB::select("select c.role_id,a.role_name,c.form_id, d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id");
		$accounting_value = \DB::select("select c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id");

		foreach ($accounting_role as $key1) {
			
			$accounting_form = \DB::select("select c.form_id, d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and 
					c.role_id =  $key1->role_id ");
			foreach ($accounting_form as $key) {
				
				if($key->form_id == 48){ 
					if($key->create == 1){ $add = 1; }
					else{ $add = 0; } 
				}
				if($key->form_id == 49){ 
					if($key->create == 1){ $terminate = 1; }
					else{ $terminate = 0; } 
				}
					$value = ['create'=>$key->create,'read'=>$key->read,'update'=>$key->update,'delete'=>$key->delete];
					$page[] = ['form_id'=>$key->form_id,'form_name'=>$key->form_name,'action'=>$value];	
			}
			
			$data[] = array('role_id'=>$key1->role_id,'role_name'=>$key1->role_name,'add_employee'=>$add,'terminate'=>$terminate,'form'=>$page);	
		}
		
		// $data[] =
		// 			// array('role'=>'executive','content'=>[$executive]),
		// 			// array('role'=>'administrator','content'=>[$administrator]),
		// 			// array('role'=>'admin','content'=>[$admin]),
		// 			// array('role'=>'hr','content'=>[$hr]),
		// 			// array('role'=>'it_admin','content'=>[$it_admin]),
		// 			array('role'=>'accounting', 'content'=>[$accounting])
		// 		;
		//$json = json_encode($data);
		//$this->ModeltoJSON('hrms/json/UserManagement/',$json,'SystemUserRole.json','w');
		return \Response::json($data);
	}

	public function index(){ // GET / HEAD
		// startup
		$get_role = $this->check();
		$data_role = $this->check_role($get_role['data'],$this->form_id);
		if(isset($get_role['data'])){
			if(isset($data_role['read'])){

				$data_table = new Model_UserManagement;
				$custom_role =  $data_table->ReadSystemUser('All',null);
				if($custom_role){
					$data = $custom_role;
					$message = $this->message_get;
					$status = $this->status200;
				}
				else{
					$data = null;
					$message = 'Data not found.';
					$status = $this->status403;
				}
				
				return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data' => $data],$status);
			}
			else{
				return \Response::json(['header'=>['message'=>$this->message_unauthorized,'status'=>$this->status403]],$this->status403);	
			}
		}else{

			return \Response::json(['header'=>['message'=>$get_role['message'],'status'=>$this->status401]],$this->status401);	 
		}
	}

	public function check_role($role , $form_id)
	{
		$getModel = new Privilege;
		if(isset($role , $form_id)){
			return $getModel->check_role($role , $form_id);
		}
		else{
			$msg['warning'] = 'Unauthorize';
			$msg['status'] = 404;
			return $msg; 
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		//$check = \Cookie::get('auth');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() // POST
	{
		//
		$role = $this->check_role(2,1);
		if($role && $role['create'] == 1){
			if($check){
				$msg = $role['create'];
			}
			else{
				$msg = 'Unauthorize';
			}
			return \Response::json(['destroy'=>$msg,'status'=>200],200);
		}
		else{
			$msg = 'Unauthorize';
			return \Response::json(['msg'=>$msg,'status'=>403],403);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) // GET
	{
		/*
		* Show data for table SystemUser
		* Model/Model_UserManagement.php
		*/
		$check = \Cookie::get('XSRF-TOKEN');
		$data = $this->check_role(1,3);
		if($data['read'] && $data['read'] == 1){
			if($check){
				$data_table = new Model_UserManagement;
				$msg = $data['read'];
			}
			else{
				$msg = 'Unauthorize';
			}
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'data'=>$data_table->ReadJob('Sort',$id),'status'=>200],200);
		}
		else{
			$msg = 'Unauthorize';
			return \Response::json(['name'=>$data['role_name'],'access'=>$msg,'status'=>403],403);
		}
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) //GET / HEAD
	{
		//

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) // PUT
	{
		$check = \Cookie::get('XSRF-TOKEN');
		$role = $this->check_role(2,1);
		if($role && $role['update'] == 1){
			if($check){
				$msg = ['store'=>$role['update']];
			}
			else{
				$msg = 'Unauthorize';
			}
			return \Response::json(['destroy'=>$msg,'status'=>200],200);
		}
		else{
			$msg = 'Unauthorize';
			return \Response::json(['msg'=>$msg,'status'=>403],403);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) // DELETE
	{
		//
		$check = \Cookie::get('XSRF-TOKEN');
		$role = $this->check_role(1,1);
		if($role && $role['delete']==1){
			if($check){
				$msg = $role['delete'];
			}
			else{
				$msg = 'Unauthorize';
			}
			return \Response::json(['destroy'=>$msg,'status'=>200],200);
		}
		else{
			$msg = 'Unauthorize';
			return \Response::json(['destroy'=>$msg,'status'=>403],403);
		}
	}

	public function check(){
		$getModel = new Privilege;
		$req = \Request::all();
		if($req['key']){ // check isset $req

			$q = $getModel->session_key($req['key']);
			if($q){ //check isset key in session
				 
				 $r = array();
				 foreach ($q as $keys) {
				 	$r['key'] = $keys->session_key;
				 } // end for store role to array.

				$decode = base64_decode($r['key']);
				$role = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
				$key = substr($decode,0,strpos($decode,'-'));
				
				 $q2 = $getModel->session_role($role);
				 foreach ($q2 as $keys) {
				 	$r['role_id'] = $keys->role_id;
				 } // end for store role to array.

				if($r['role_id'] == $role){ // if type decode or encode
					$data =  $role;
					$msg = 'ACCESS GRANTED !';
				}
				else{
					$msg = 'ACCESS DENIED IN WEB!';
					$data = null;	
				}
			}
			else{
				$msg = 'ACCESS DENIED !';
				$data = null;	
			}//end if check

		}// end if isset $req
		else{ // else message error if null
			$msg = 'ACCESS DENIED, KEY URL NOT FOUND !';
			$data = null;
		}
		
		return ['message'=>$msg,'data'=>$data];

	}

}
