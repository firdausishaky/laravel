<?php namespace Larasite\Http\Controllers;

// use Mail;
// use App\User;
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Library\FuncAccess;

class dompdf_ctrl extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $restfull = true;
	

	private function slip_incentive($data){
		$tmp = '<!DOCTYPE html>
		<html>


		<head>

		</head>

		<style>
		table {
			border-collapse: collapse;
		}

		table, th, td {
			border: 1px solid black;
		}
		</style>

		<body style="padding: 20px;border-style: solid">
		<div>
		<table>
		<thead>
		<tr>
		<td><img src="http://10.129.1.12:8080/get-images/company/General_informationRyL5i5.jpg" alt="company"/></td>
		<td><h1>Incentive Slips</h1></td>

		</tr>
		</thead>
		<tbody>
		<tr >
		<td>Employee Name</td>

		<td>'.$data['employee_name'].'</td>

		</tr>
		<tr>
		<td>Periode Covered</td>

		<td>'.$data['periode_covered'].'</td>

		</tr>
		<tr>
		<td>Monthly Incentive</td>

		<td>'.$data['monthly_incentive'].'</td>

		</tr>
		<tr>
		<td>Food Allowance</td>

		<td>'.$data['food_allowance'].'</td>

		</tr>
		<tr>
		<td>ISO Allowance</td>

		<td>'.$data['iso_allowance'].'</td>

		</tr>
		<tr>
		<td>Dealer Incentive</td>

		<td>'.$data['dealer_incentive'].'</td>

		</tr>
		<tr>
		<td>Team Leader Incentive</td>

		<td>'.$data['team_leader_incentive'].'</td>

		</tr>
		<tr>
		<td>Absences</td>

		<td>'.$data['abs'].'</td>

		</tr>
		<tr>
		<td>Tardiness</td>

		<td>'.$data['tar'].'</td>

		</tr>
		<tr>
		<td>Disciplinary Action</td>

		<td>'.$data['disciplin'].'</td>

		</tr>
		<tr>
		<td>Adjustment</td>

		<td>'.$data['adjustment_payroll'].'</td>

		</tr>
		<tr>
		<td><b>Gross Pay</b></td>

		<td><b>'.$data['total'].'</b></td>

		</tr>
		<tr>
		<td>Deduction Monthly Incentive</td>

		<td>'.$data['deductions_monthly'].'</td>

		</tr>
		<tr>
		<td>Deduction Food Allowance</td>

		<td>'.$data['deductions_food'].'</td>

		</tr>
		<tr>
		<td>Deduction ISO Allowance</td>

		<td>'.$data['deductions_iso_allow'].'</td>

		</tr>
		<tr>
		<td>Deduction Dealer Incentive</td>

		<td>'.$data['deductions_dealer'].'</td>

		</tr>
		<tr>
		<td>Deduction Team Leader Incentive</td>

		<td>'.$data['deductions_team_leader'].'</td>

		</tr>
		<tr>
		<td><b>Net Pay</b></td>

		<td><b>'.$data['total'].'</b></td>

		</tr> 

		<tr>
		<td><b>Net Pay</b></td>

		<td><b>'.$data['total'].'</b></td>

		</tr> 
		<tr>
		<td><b>Received By</b></td>

		<td><b>Date</b></td>

		</tr> 
		<tr>
		<td><br><br><br></td>

		<td><br><br><br></td>

		</tr> 



		</tbody>
		</table>
		</div>



		</body>
		</html>';
		return $tmp;
	}

	public function slips_monthly($data){
		$tmp = '<!DOCTYPE html>
		<html>


		<head>

		</head>

		<style>
		table {
			border-collapse: collapse;
		}

		table, th, td {
			border: 1px solid black;
		}
		</style>

		<body style="padding: 20px; border-style: solid">
		<div align="center">
		<table>
		<thead>
		<tr>
		<td><img src="http://10.129.1.12:8080/get-images/company/General_informationRyL5i5.jpg" alt="company"/></td>
		<td><h1>Payroll Slips</h1></td>

		</tr>
		</thead>
		<tbody>
		<tr >
		<td>Employee Name</td>

		<td>'.$data['employee_name'].'</td>

		</tr>
		<tr>
		<td>Periode Covered</td>

		<td>'.$data['periode_covered'].'</td>

		</tr>
		<tr>
		<td>Basic Salary</td>

		<td>'.$data['basic_salary'].'</td>

		</tr>
		<tr>
		<td>Ecola</td>

		<td>'.$data['ecola_salary'].'</td>

		</tr>
		<tr>
		<td>Overtime</td>

		<td>'.$data['overtime'].'</td>

		</tr>
		<tr>
		<td>Night Differential</td>

		<td>'.$data['night_differential'].'</td>

		</tr>
		<tr>
		<td>Holiday Work Pay</td>

		<td>'.$data['total_holiday_pay'].'</td>

		</tr>
		<tr>
		<td>Leave With Pay</td>

		<td>'.$data['leave_with_pay'].'</td>

		</tr>
		<tr>
		<td>Absences</td>

		<td>'.$data['absences'].'</td>

		</tr>
		<tr>
		<td>Tardiness</td>

		<td>'.$data['tardiness'].'</td>

		</tr>
		<tr>
		<td>Adjustment</td>

		<td>'.$data['adjusments'].'</td>

		</tr>
		<tr>
		<td><b>Gross Pay</b></td>

		<td><b>'.$data['gross_pay'].'</b></td>

		</tr>
		<tr>
		<td>Withholding Tax</td>

		<td>'.$data['withholding_tax'].'</td>

		</tr>
		<tr>
		<td>SSS</td>

		<td>'.$data['sss_cont'].'</td>

		</tr>
		<tr>
		<td>Philhealth</td>

		<td>'.$data['philhealth_cont'].'</td>

		</tr>
		<tr>
		<td>HDMF</td>

		<td>'.$data['hdmf_cont'].'</td>

		</tr>

		<tr>
		<td>SSS Loan</td>

		<td>'.$data['sss_loan'].'</td>

		</tr>
		<tr>
		<td>HDMF Loan</td>

		<td>'.$data['hdmf_loan'].'</td>

		</tr>
		<tr>
		<td>HMO</td>

		<td>'.$data['hmo'].'</td>

		</tr>
		<tr>
		<td>Others</td>

		<td>'.$data['other'].'</td>

		</tr>
		<tr>
		<td>Total Deduction</td>

		<td>'.$data['total_deduction'].'</td>

		</tr>
		<tr>
		<td><b>Net Pay</b></td>

		<td><b>'.$data['net_pay'].'</b></td>

		</tr> 


		<tr>
		<td><b>Received By</b></td>

		<td><b>Date</b></td>

		</tr> 
		<tr>
		<td><br><br><br></td>

		<td><br><br><br></td>

		</tr> 



		</tbody>
		</table>
		</div>



		</body>
		</html>';
		return $tmp;
	}

	public function slips_bonus($data){
		$tmp = '<!DOCTYPE html>
		<html>


		<head>

		</head>

		<style>
		table {
			border-collapse: collapse;
		}

		table, th, td {
			border: 1px solid black;
		}
		</style>

		<body style="padding: 20px; border-style: solid">
		<div>
		<table>
		<thead>
		<tr>
		<td><img src="http://10.129.1.12:8080/get-images/company/General_informationRyL5i5.jpg" alt="company"/></td>
		<td><h1>Bonus Slip</h1></td>

		</tr>
		</thead>
		<tbody>
		<tr >
		<td>Employee Name</td>

		<td>'.$data['employee_name'].'</td>

		</tr>
		<tr>
		<td>Periode Covered</td>

		<td>'.$data['periode_covered'].'</td>

		</tr>
		<tr>
		<td>13th Month Pay</td>

		<td>'.$data['pay13th'].'</td>

		</tr>

		<tr>
		<td>Net Performance Bonus</td>

		<td>'.$data['net_performance_bonus'].'</td>

		</tr>
		<tr>
		<td>Management Bonus</td>

		<td>'.$data['management_bonus'].'</td>

		</tr>
		<tr>
		<td>Others</td>

		<td>'.$data['total_holiday_pay'].'</td>

		</tr>
		<tr>
		<td><b>Gross Pay</b></td>

		<td><b>'.$data['gross_pay'].'</b></td>

		</tr>
		<tr>
		<td colspan="2"><b>Deductions</b></td>

		<!-- <td>'.$data['absences'].'</td> -->

		</tr>
		<tr>
		<td>Withholding Tax</td>

		<td>'.$data['tardiness'].'</td>

		</tr>
		<tr>
		<td>HMO</td>

		<td>'.$data['adjusments'].'</td>

		</tr>
		<tr>
		<td><b>Others Deduction</b></td>

		<td><b>'.$data['gross_pay'].'</b></td>

		</tr>
		<tr>
		<td><b>Total Deduction</b></td>

		<td><b>'.$data['withholding_tax'].'</b></td>

		</tr>
		<tr>
		<td><b>Net Pay</b></td>

		<td><b>'.$data['sss_cont'].'</b></td>

		</tr>


		<tr>
		<td><b>Received By</b></td>

		<td><b>Date</b></td>

		</tr> 
		<tr>
		<td><br><br><br></td>

		<td><br><br><br></td>

		</tr> 



		</tbody>
		</table>
		</div>



		</body>
		</html>';
		return $tmp;
	}

	public function slips_sil($data){
		$tmp = '<!DOCTYPE html>
		<html>


		<head>

		</head>

		<style>
		table {
			border-collapse: collapse;
		}

		table, th, td {
			border: 1px solid black;
		}
		</style>

		<body style="padding: 20px; border-style: solid">
		<div>
		<table>
		<thead>
		<tr>
		<td><img src="http://10.129.1.12:8080/get-images/company/General_informationRyL5i5.jpg" alt="company"/></td>
		<td><h1>Bonus Slip</h1></td>

		</tr>
		</thead>
		<tbody>
		<tr >
		<td>Employee Name</td>

		<td>'.$data['employee_name'].'</td>

		</tr>
		<tr>
		<td>Periode Covered</td>

		<td>'.$data['periode_covered'].'</td>

		</tr>
		<tr>
		<td>13th Month Pay</td>

		<td>'.$data['pay13th'].'</td>

		</tr>

		<tr>
		<td>Net Performance Bonus</td>

		<td>'.$data['net_performance_bonus'].'</td>

		</tr>
		<tr>
		<td>Management Bonus</td>

		<td>'.$data['management_bonus'].'</td>

		</tr>
		<tr>
		<td>Others</td>

		<td>'.$data['total_holiday_pay'].'</td>

		</tr>
		<tr>
		<td><b>Gross Pay</b></td>

		<td><b>'.$data['gross_pay'].'</b></td>

		</tr>
		<tr>
		<td colspan="2"><b>Deductions</b></td>

		<!-- <td>'.$data['absences'].'</td> -->

		</tr>
		<tr>
		<td>Withholding Tax</td>

		<td>'.$data['tardiness'].'</td>

		</tr>
		<tr>
		<td>HMO</td>

		<td>'.$data['adjusments'].'</td>

		</tr>
		<tr>
		<td><b>Others Deduction</b></td>

		<td><b>'.$data['gross_pay'].'</b></td>

		</tr>
		<tr>
		<td><b>Total Deduction</b></td>

		<td><b>'.$data['withholding_tax'].'</b></td>

		</tr>
		<tr>
		<td><b>Net Pay</b></td>

		<td><b>'.$data['sss_cont'].'</b></td>

		</tr>


		<tr>
		<td><b>Received By</b></td>

		<td><b>Date</b></td>

		</tr> 
		<tr>
		<td><br><br><br></td>

		<td><br><br><br></td>

		</tr> 



		</tbody>
		</table>
		</div>



		</body>
		</html>';
		return $tmp;
	}
	

	
	public function generate()
	{	
		
		def("DOMPDF_ENABLE_REMOTE", false);
		$storages 	= storage_path("dompdf");
		// $storages = "";
		$input = \Input::all();

		$date 				= date("Y-m-d");
		$time 				= time();

		if($input['type_slip'] == "incentive_slip"){
			$template 			= $this->slip_incentive($input);
		}if($input['type_slip'] == "monthly_slip"){
			$template 			= $this->slips_monthly($input);
		}if($input['type_slip'] == "bonus_slip"){
			$template 			= $this->slips_bonus($input);
		}if($input['type_slip'] == "sil_slips"){
			$template 			= $this->slips_sil($input);
		}
		$period 			= str_replace(", ","_",$input['periode_covered']);
		$filename 			= $input['type_slip']."_".$period."_".$input['employee_name']."_".$date."_".$time.".pdf";
		$path 				= $storages."/slip_monthly";
		// $path = $storages;
		$save 				= $path."/".$filename;

		$pdf = \PDF::loadHTML($template)->setPaper('a4')->setWarnings(false)->save($save);

		return \Response::json(['header'=>['message'=>"Success Generate!",'status'=>200, "access" => true],'data'=>['filename'=>$filename,'path'=>$path,"dirfull"=>$save]],200);

	}

	public function send_email_pdf(){

		$input = \Input::all();
		$name_file = $input['name_file'];
		$employee_id = $input['employee_id'];
		// return response()->json([$name_file,$employee_id],200);


		$set_data  =  \DB::SELECT("CALL `email__GET`('$employee_id')");
		if(count($set_data) > 0){
			if(isset($set_data[0]->work_email)){
				$set_email = $set_data[0]->work_email;
			}else if(isset($set_data[0]->personal_email)){
				$set_email = $set_data[0]->personal_email;
			}


			$work_email = $set_data[0]->work_email;
			$personal_email = $set_data[0]->personal_email;
			$fullname = $set_data[0]->fullname;


			$data = array('name'=>$fullname,'email_work' => $work_email, 'email_personal'=>$personal_email, 'name_file' =>$name_file);


			\Mail::raw('test', function($message) use ($data){
				// $req = $data['request'];
				$message->to($data['email_work'])->subject("test");
				$storages 	= storage_path("dompdf");
				$path = $storages."/slip_monthly";
				$message->attach($path."/".$data['name_file']);
			});

			\Mail::raw('test', function($message) use ($data){
				// $req = $data['request'];
				$message->to($data['email_personal'])->subject("test");
				$storages 	= storage_path("dompdf");
				$path = $storages."/slip_monthly";
				$message->attach($path."/".$data['name_file']);;
			});
			return response()->json(['header' => ['message' =>  'success send file to email',  'status' => 200] ,'data' => []], 200);
			

			// $namex  =  $input['employee'];
			// $request =  $input['type'];
			// $email   =  $set_data;
		}else{
			return response()->json(['header' => ['message' =>  'success send email',  'status' => 200] ,'data' => []], 200);
		}
	}

}
