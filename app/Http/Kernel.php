<?php namespace Larasite\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	// protected $middleware = [
	// 	'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
	// 	'Illuminate\Cookie\Middleware\EncryptCookies',
	// 	'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
	// 	'Illuminate\Session\Middleware\StartSession',
	// 	'Illuminate\View\Middleware\ShareErrorsFromSession',
	// 	'Larasite\Http\Middleware\TestMiddleware',
	// 	//'Larasite\Http\Middleware\VerifyCsrfToken',
	// ];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	// protected $routeMiddleware = [
	// 	'auth' => 'Larasite\Http\Middleware\Authenticate',
	// 	'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
	// 	'guest' => 'Larasite\Http\Middleware\RedirectIfAuthenticated',
	// 	'csrf' => 'Larasite\Http\Middleware\VerifyCsrfToken',
	// 	'jwt.auth' => 'Tymon\JWTAuth\Middleware\GetUserFromToken',
 //        'jwt.refresh' => 'Tymon\JWTAuth\Middleware\RefreshToken'
	// ];
	
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		'Larasite\Http\Middleware\TestMiddleware',
		//'Larasite\Http\Middleware\VerifyCsrfToken',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' => 'Larasite\Http\Middleware\Authenticate',
		'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' => 'Larasite\Http\Middleware\RedirectIfAuthenticated',
		'csrf' => 'Larasite\Http\Middleware\VerifyCsrfToken',
		'jwt.auth' => 'Tymon\JWTAuth\Middleware\GetUserFromToken',
        'jwt.refresh' => 'Tymon\JWTAuth\Middleware\RefreshToken',
        'check_param' => 'Larasite\Http\Middleware\CheckParamRouteMiddleware',
        'list_request' => 'Larasite\Http\Middleware\ListRequestMiddleware',
        'check_payload' => 'Larasite\Http\Middleware\TestMiddleware',
	];

}
