<?php namespace Larasite\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Larasite\Http\Requests;
use Larasite\User;
use Larasite\Library\FuncAccess;
class CheckParamRouteMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	private function ip_set_URL($type){
		$get_ip = \Config::get('app.get_ip');
		return $get_ip[$type];
	}

	protected function jwt_decode(){

		$ip_set_URL = $this->ip_set_URL('uat_private');
		//$ip_set_URL = $this->ip_set_URL('uat_public');
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_PORT => "8080",
		  CURLOPT_URL => "http://$ip_set_URL:8080/jwt_decode",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => json_encode(["token"]),
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/json",
		    "postman-token: 00ee03bb-8102-ee60-0511-5eb18f440ab4"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
	}

	private function res_error(){
		return \Response::json(['header'=>['message'=>'data not exists::checkParam'],'data'=>null],404);
	}
	public function handle($request , Closure $next)
	{
		$ip_set_URL = $this->ip_set_URL('uat_private');

		try {
			$count_segments = count($request->segments());
			if($request->segment($count_segments) == 'post-auth' || $request->segment($count_segments) == 'get-token'){
				$request->merge(['ip'=>$request->getClientIp()]);
				//return \Response::json($request->input(),200);
			}else{

				// if(null != $request->header('authorization')){
				// 	$tkn = $request->header('authorization');
				// 	$tkn = explode(' ', $tkn);
				// 	$dt  = $tkn[1];
				// }

				//return \Response::json(['header'=>['message'=>'CHECK RECORD'],'data'=>\Input::get('name')],500);

				if(null != $request->header('authorization')){
					$tkn = $request->header('authorization');
					$tkn = explode(' ', $tkn);
					$dt  = $tkn[1];

					$curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_PORT => "8080",
					  CURLOPT_URL => "http://$ip_set_URL:8080/jwt_decode",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_HTTPHEADER => array(
					    "authorization: Bearer $dt",
					    "cache-control: no-cache",
					    "content-type: application/json",
					  ),
					));

					$res_curl = curl_exec($curl);
					$err = curl_error($curl);

					curl_close($curl);

					if ($err) {
						return \Response::json(['header'=>['message'=>'data not exists::checkParam1'],'data'=>null],404);
					  	return $this->res_error();
					} else {
						if($res_curl == 'unauthorize'){
							return \Response::json(['header'=>['message'=>$dt],'data'=>null],500);
							return $this->res_error();
						}
					}

					$dt = json_decode($res_curl);

					if($dt == 'unauthorize'){
						return \Response::json(['header'=>['message'=>$dt],'data'=>null],500);
						return $this->res_error();
					}

					if(isset($dt->message) && ($dt->message == 'Token unauthorize' || $dt->message == "Error verify token")){
						return \Response::json(['header'=>['message'=>'$dt->message , please re-login again2'],'data'=>null],500);
					}

					$count_segments = count($request->segments());
					if($request->segment($count_segments) == 'del-auth'){
						$request->merge(['key'=>$dt->data->ver]);
						$request->attributes->add(['keys'=>$dt->data->ver]);
						$request->merge(['customer_id'=>$dt->data->customer_id]);
					}else{
						$request->merge(['key'=>$dt->data->ver]);
						$request->attributes->add(['keys'=>$dt->data->ver]);
					}

					if(in_array('payroll', $request->segments())){
						$request->merge(['customer_id'=>$dt->data->customer_id]);
						$request->merge(['department_id'=>$dt->data->department_id]);
						$request->merge(['role_id'=>$dt->data->role_id]);
						$request->merge(['employee_id'=>$dt->data->employee_id]);
						$request->merge(['local_it'=>$dt->data->local_it]);
					}
					//return \Response::json(['header'=>['message'=>'data not exists::checkParam4'],'data'=>$res_curl],404);
					//$request->attributes->add(['key'=>$dt->data]);
					//return \Response::json(\Request::all());

				}else{
					//return \Response::json(['header'=>['message'=>'data not exists::checkParam3'],'data'=>null],404);
					//return $this->res_error();

					$dt = \Request::get('key');
					$curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_PORT => "8080",
					  CURLOPT_URL => "http://$ip_set_URL:8080/jwt_decode",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_HTTPHEADER => array(
					    "authorization: Bearer $dt",
					    "cache-control: no-cache",
					    "content-type: application/json",
					  ),
					));

					$res_curl = curl_exec($curl);
					$err = curl_error($curl);

					curl_close($curl);

					if ($err) {
						return \Response::json(['header'=>['message'=>'data not exists::checkParam1'],'data'=>null],404);
					  	return $this->res_error();
					} else {
						if($res_curl == 'unauthorize'){
							return \Response::json(['header'=>['message'=>$dt],'data'=>null],500);
							return $this->res_error();
						}
					}
					$dt = json_decode($res_curl);

					if($dt == 'unauthorize'){
						return \Response::json(['header'=>['message'=>$dt],'data'=>null],500);
						return $this->res_error();
					}

					if(isset($dt->message) && ($dt->message == 'Token unauthorize' || $dt->message == "Error verify token")){
						return \Response::json(['header'=>['message'=>"$dt->message , please re-login again"],'data'=>null],500);
					}

					$count_segments = count($request->segments());
					if($request->segment($count_segments) == 'del-auth'){
						$request->merge(['key'=>$dt->data->ver]);
						$request->attributes->add(['keys'=>$dt->data->ver]);
						$request->merge(['customer_id'=>$dt->data->customer_id]);
					}else{
						$request->merge(['key'=>$dt->data->ver]);
						$request->attributes->add(['keys'=>$dt->data->ver]);
					}

					if(in_array('payroll', $request->segments())){
						$request->merge(['customer_id'=>$dt->data->customer_id]);
						$request->merge(['department_id'=>$dt->data->department_id]);
						$request->merge(['role_id'=>$dt->data->role_id]);
						$request->merge(['employee_id'=>$dt->data->employee_id]);
						$request->merge(['local_it'=>$dt->data->local_it]);
					}
				}

				try {
					$token_key = \Request::get('key');
					$decode = base64_decode($token_key);
					$name = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
					$result = \DB::table('ldap')->where(["employee_id"=>$name])->get(["role_id","active_access","active"]);
					if(count($result) > 0){
						if($result[0]->role_id && $result[0]->active_access == 1 && $result[0]->active == 1){

							$ip = $request->getClientIp();
							$agent = $request->header("user-agent");
							$result_ses = \DB::table("session")->where(["session_key"=>$token_key,"clientIp"=>$ip,"info_agent"=>$agent])->get(["info_cookie"]);

							/*if(count($result_ses) > 0){

								$cook = $request->header("cookie");
								if($result_ses[0]->info_cookie){
									if(!strstr($result_ses[0]->info_cookie,"laravel_session=")){
										return \Response::json(['header'=>['message'=>'data not exists::checkParam4'],'data'=>null],404);
										return $this->res_error();
									}
								}else if(strstr($cook,"laravel_session=") && $result_ses[0]->info_cookie == null){
									\DB::table("session")->where(["session_key"=>$token_key,"clientIp"=>$ip,"info_agent"=>$agent])->update(["info_cookie"=>$cook]);
								}
							}else{
								return \Response::json(['header'=>['message'=>'data not exists::checkPara5'],'data'=>null],404);
								return $this->res_error();
							}*/
						}else{
							if(isset($dt->data->ver)){
								$token_key = $dt->data->ver;
								$decode = base64_decode($token_key);
								$name = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
								$result = \DB::table('ldap')->where(["employee_id"=>$name])->get(["role_id","active_access","active"]);
								if(count($result) > 0){
									if($result[0]->role_id && $result[0]->active_access == 1 && $result[0]->active == 1){
										$ip = $request->getClientIp();
										$agent = $request->header("user-agent");
										$result_ses = \DB::table("session")->where(["session_key"=>$token_key,"clientIp"=>$ip,"info_agent"=>$agent])->get(["info_cookie"]);
									}
								}
							}else{
								return \Response::json(['header'=>['message'=>'data not exists::checkParam6'],'data'=>null],404);
								return $this->res_error();
							}
						}
					}else{
						//return \Response::json(['header'=>['message'=>'data not exists::checkParam7'],'data'=>null],404);
						//return $this->res_error();
					}
				} catch (\Exception $e) {
					return [$e->getMessage(),1];
				}

				if(null != $request->route()->parameters()){
					$param 			= $request->route()->parameters();
					$key_param 		= array_keys($param);

					$keys = [];
					//return [$key_param];
					for($i=0; $i < count($key_param); $i++){

						if(strtolower($key_param[$i]) == 'personal' || strtolower($key_param[$i]) == 'id'){
							
							if(!is_numeric($param[$key_param[$i]])){

								$ext = explode(".", $param[$key_param[$i]]);
								
								if( count($ext) > 0 && in_array($ext[1], ["jpg","jpeg","gif","png","doc","pdf","xls"])){

								}else{								
									return \Response::json(['header'=>['message'=>'data not exists::checkParam8'],'data'=>null],404);
									$this->res_error();
								}
								
							}
						}
					}

					if(isset($param['filename']) && isset($param['id'])){
						if(strlen($param['filename']) < 5 && !strstr(strtolower($param['filename']),".jpg .jpeg .gif .png")){
							return \Response::json(['header'=>['message'=>'data not exists::checkParam9'],'data'=>null],404);
							$this->res_error();
						}
					}

					if(isset($param['years'])){
						if(!is_numeric($param['years'])){
							return \Response::json(['header'=>['message'=>'data not exists::checkParam10'],'data'=>null],404);
							$this->res_error();
						}
					}

					if(isset($param['month'])){
						if(!is_numeric($param['month'])){
							return \Response::json(['header'=>['message'=>'data not exists::checkParam11'],'data'=>null],404);
							$this->res_error();
						}
					}
				}
			}// end if($request->segment($count_segments) == 'post-auth')
		}catch(\Exception $e){
			return $e;
		}

		return $next($request);
	}

}
