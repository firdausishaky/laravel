<?php namespace Larasite\Http\Middleware;

use Closure;
/*use Illuminate\Http\Request;
use Larasite\Http\Requests;*/
class ListRequestMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */

	private function res_error(){
		return \Response::json(['header'=>['message'=>'data not exists'],'data'=>null],404);
	}

	private function get_subordinate($id){
		//$result = \DB::select("SELECT distinct employee_id from emp_supervisor where supervisor='$id'");
		$result = \DB::table("emp_supervisor")->where(["supervisor"=>$id])->get();
		if(count($result) > 0){
				$tmp = "'".$id."'";
			foreach ($result as $key => $value) {
				if($value->employee_id){
					$tmp .= ",'".$value->employee_id."'";
				}
			}
			return $tmp;	
		}else{ return null; }
		
	}

	public function handle($request , Closure $next)
	{

		// try {		

		// 	$subordinate = "";
		// 	if(null != $request->all() ){
				
		// 		$var 	= $request->all();
		// 		if(!$var['key']){ return $this->res_error(); }
				
		// 		$decode = base64_decode($var['key']);
		// 		$key = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
				
				
		// 		$rule = [
		// 			'employee_id' 		=> 'required|Regex:/^[0-9-\^ ]+$/|min:4|max:8',
		// 			'employee_name' 	=> 'Regex:/^[0-9-\^ ]+$/|min:4|max:8',
		// 			'key'				=> 'required|Regex:/^[0-9-\^ ]+$/|min:4|max:8'
		// 		];

				
		// 		$obj = ["employee_id"=>$var['employee_id'] ,"key"=>$key];
				
		// 		if(isset($var['employee_name'])){
		// 			$obj['employee_name'] = $var['employee_name'];
		// 			$subordinate = $this->get_subordinate($key);
		// 		}

		// 		$valid = \Validator::make($obj,$rule);
				
		// 		if($valid->fails()){ return $this->res_error(); }
		// 		else{
		// 			if($key != $var['employee_id']){ return $this->res_error(); }

		// 			if(isset($obj['employee_name'])){
		// 				$target_view = $var['employee_name'];
						
		// 				try {
							
		// 					$keys = $key;
		// 					$r = \DB::select("CALL get_role_param('$keys','$target_view')");
							
		// 					if(count($r) < 2){ $this->res_error(); }
		// 					foreach ($r as $keyx => $value) {
		// 						if($value->employee_id == $key){
		// 							if(!strstr(strtolower($value->role_name),"human resource hr hrd admin")){
		// 								if(!strstr($subordinate,$target_view)){ return $this->res_error(); }
		// 								//if($target_view != $key){ return $this->res_error(); }
		// 							}
		// 						}
		// 					}

		// 				} catch (Exception $e) {
		// 					return abort(404);
		// 				}
		// 			}
		// 		}
		// 	}
		// }catch(\Exception $e){
		// 	return abort(404);
		// }

			
		return $next($request);
	}

}
