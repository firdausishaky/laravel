<?php namespace Larasite\Http\Requests;

use Larasite\Http\Requests\Request;

class HolidaySetup extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'=>'required|alpha',
			'date'=>'date',
			'repeat_annually'=>'alpha',
			'type'=>'alpha',
			'month'=>'numeric',
			'day'=>'alpha'
		];
	}

}
