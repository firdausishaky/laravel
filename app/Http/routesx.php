<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
| $super  = \DB::SELECT("select * from emp,emp_supervisor where emp.employee_id  =  emp_supervisor.employee_id and emp_supervisor.supervisor = 'empID' and (emp.first_name like '%string%' or emp.middle_name like '%string%' and  emp.last_name like '%string%')");

*/


use Illuminate\Cookie\CookieJar;
use Larasite\Http\Requests;
use Illuminate\Support\Facades\Mail;

// STARTUP ########################################

Route::get('/',function(){ 

	//return \View::make('index'); 
	//$mail = new PHPMailer;

});
Route::filter('csrf', function(){ if (\Session::token() != \Input::get('auth')) { throw new Illuminate\Session\TokenMismatchException; } });
// GET IMAGE
Route::get('images/{id}/{filename}','Employees\PersonalPict_Ctrl@get_file');
Route::get('get-images/company/{filename}','Organization\GeneralInformation_Ctrl@ViewFile'); // for View File

/* ###########################################
* API HRMS #######################################
*/#############################################

// Route::get('/import', 'Attendance\ScheduleImport_Ctrl@import');
Route::group(array('prefix'=>'api'),function(){
   /* header('Access-Control-Allow-Origin', '*');
    header('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, DELETE');
    header('Access-Control-Allow-Headers','Origin, Content-Type,Accept , Content-Disposition, Accept, X-Request-With,X-API-KEY, Language, Access-Control-Request-Method');
    header('Access-Control-Allow-Credentials','true');*/
	// AUTHENTICATION ###############################################################
	Route::group(array('prefix'=>'login'),function(){
		Route::post('post-auth','AuthController@doLogin'); // proses authenticate
		Route::get('get-token','AuthController@token'); // get key startup
		Route::get('del-auth','AuthController@logout');
	});
	// END AUTHENTICATION


	// USER MANAGEMENT #################################################
	Route::group(array('prefix'=>'user-management'),function(){
		// SYSTEM USER
		Route::resource('/system-user','UserManage\SystemUser_Ctrl');
		Route::get('/system-user-json','UserManage\SystemUser_Ctrl@Get_Role');
		Route::get('/search-domain','UserManage\SystemUser_Ctrl@Searching');
		// CUSTOME ROLES
		Route::post('/custome-role-update/{id}','UserManage\CustomRole_Ctrl@update_custome');
		Route::resource('/custome-role','UserManage\CustomRole_Ctrl');
		Route::get('/custome-role-data','UserManage\CustomRole_Ctrl@get_base');
		Route::get('/custome-role-json','UserManage\CustomRole_Ctrl@Get_Role');
	});
	// END USER MANAGEMEN


	// CONFIG
	Route::group(['prefix'=>'config'],function(){
		Route::resource('/mail','Config\Mail');
		Route::post('/mail/update','Config\Mail@update');

		Route::resource('/ldap','Config\Ldap');
		Route::post('/ldap/update','Config\Ldap@update');
	});

	// JOBS #################################################`
	Route::group(array('prefix'=>'jobs'),function(){
		Route::resource('/jobs-title','Jobs\JobsTitle_Ctrl');
			Route::group(array('prefix'=>'jobs-title'),function(){
				//Route::post('/store','Jobs\JobsTitle_Ctrl@store');
				Route::post('/store','Jobs\jobtitle_addon@insert');
				Route::post('/update/{id}','Jobs\JobsTitle_Ctrl@update');
				Route::post('/cancel','Jobs\JobsTitle_Ctrl@cancel');

			});
		Route::get('/download/{filename}','Jobs\JobsTitle_Ctrl@download');
		Route::resource('/employment-status','Jobs\EmploymentStatus_Ctrl');
	});
	// END JOBS


	// ORGANIZATION #################################################
	Route::group(array('prefix'=>'organization'),function(){
		Route::resource('/general-information','Organization\GeneralInformation_Ctrl');
			Route::post('/general-information/store','Organization\GeneralInformation_Ctrl@store_title');
			Route::post('/general-information/update/{id}','Organization\GeneralInformation_Ctrl@update');
			Route::post('/general-information/cancel','Organization\GeneralInformation_Ctrl@store_title');
			Route::get('/general-information/get_file/download','Organization\GeneralInformation_Ctrl@get_file'); // for get file
		Route::resource('/structure','Organization\OrganizationStructure_Ctrl');
		Route::get('/structure-json/{filename}','Organization\OrganizationStructure_Ctrl@get_file');
		Route::resource('/department','Organization\Department_Ctrl');
	});
	// END ORGANIZATION


	// QUALIFICATIONS #################################################
	Route::group(array('prefix'=>'qualification'),function(){
		Route::resource('/skill','Qualification\Skill_Ctrl');
		Route::DELETE('/skill/delete/{id}','Qualification\Skill_Ctrl@destroy');
		Route::resource('/education','Qualification\Education_Ctrl');
		Route::post('/education/update/{id}','Qualification\Education_Ctrl@update');
		Route::resource('/language','Qualification\Language_Ctrl');
	});
	// END QUALIFICATIONS


	// NATIONALITIES #################################################
	Route::group(array('prefix'=>'nationalities'),function(){
		Route::resource('/form-nationalities','Nationality\Nationality_Ctrl');
		Route::get('/getPIM','Nationality\Nationality_Ctrl@getPim');
	});
	// END NATIONALITIES


	// MODULE 201 #################################################
	Route::group(array('prefix'=>'module'),function(){
		// terminate
		Route::post('/employee_data/{id}','Module\EmployeeInfo_Ctrl@get_datas');
		Route::resource('termination-reasons','Module\TerminationReasons_Ctrl'); // terminate

		Route::get('employee-info-search','Module\EmployeeInfo_Ctrl@Search'); //changing employee feature
		Route::get('employee-info/search-by-name','Module\EmployeeInfo_Ctrl@SearchByName'); //changing employee feature
		Route::get('employee-info/search-by-spv','Module\EmployeeInfo_Ctrl@SearchBySPV'); //changing employee feature
		Route::group(['prefix'=>'employee-info'],function(){
			Route::resource('/','Module\EmployeeInfo_Ctrl'); // employee info / view all employee
			Route::delete('/{id}','Module\EmployeeInfo_Ctrl@destroy');
			Route::resource('add/employee','Employees\AddEmployee_Ctrl');
			Route::post('add/employee/store','Employees\AddEmployee_Ctrl@store2');
		});

		Route::resource('immigration-issuer','Module\ImmigrationIssuer_Ctrl');
		Route::resource('import-csv','Module\import_cvs');
		Route::get('permission','Module\import_cvs@index_granted');
		Route::get('import-csv-getfile','Module\import_cvs@getfile'); // for download example CSV file

		Route::group(array('prefix'=>'document-template'),function(){
			Route::get('/','DocumentTemplate\DocumentTemplate_Ctrl@index'); // DOCUMENT TEMPLATE
			Route::delete('/{id}','DocumentTemplate\DocumentTemplate_Ctrl@destroy'); // DOCUMENT TEMPLATE
			Route::post('/','DocumentTemplate\DocumentTemplate_Ctrl@store'); // DOCUMENT TEMPLATE
			Route::post('/edit/{id}','DocumentTemplate\DocumentTemplate_Ctrl@update'); // DOCUMENT TEMPLATE
			Route::get('/get-field/{param}','DocumentTemplate\DocumentTemplate_Ctrl@Get_Binding'); // DOCUMENT BIND
			Route::get('/get-data','DocumentTemplate\DocumentTemplate_Ctrl@Get_Table'); // DOCUMENT Table
		});
		Route::group(['prefix'=>'ldap'],function(){
			Route::resource('/','Module\Ldap_Ctrl');
		});

	});
	// END MODULE 201


	// EMPLOYEES LIST DETAILS  ##############################################
	Route::group(array('prefix'=>'employee-details'),function(){
		Route::post('/picture/{id}','Employees\PersonalPict_Ctrl@store_pict');//store
		Route::post('/picture/update/{id}','Employees\PersonalPict_Ctrl@update_pict');//update
		Route::resource('/picture','Employees\PersonalPict_Ctrl');//delete
		Route::get('/picture/{id}/{filename}','Employees\PersonalPict_Ctrl@download');

		// CONTACT ###
		Route::resource('/contact','Employees\Contact_Ctrl');
		Route::post('/contactv2/{id}','Employees\Contact_Ctrl@update');
		// Route::post('/contact-store/{id}','Employees\Contact_Ctrl@store');
		// PERSONAL ###
		Route::resource('/personal','Employees\Personal_Ctrl');
			Route::group(array('prefix'=>'personal'),function(){
				//PERSONAL PICTURE
				// PERSONAL ATTACHMENT
				Route::resource('/attach','Employees\PersonalAttachment_Ctrl');
				Route::post('/attach/store1/{id}','Employees\PersonalAttachment_Ctrl@store1');
				Route::post('/attach/store2/{id}','Employees\PersonalAttachment_Ctrl@store2');
				Route::post('/attach/update/{id}','Employees\PersonalAttachment_Ctrl@update_attach');
				Route::get('/attach/{id}/{filename}','Employees\PersonalAttachment_Ctrl@download');
			});
		// EMERGENCY ###
		Route::resource('/emergency','Employees\Emergency_Ctrl');
			Route::group(array('prefix'=>'emergency'),function(){
				Route::post('/store/{id}','Employees\Emergency_Ctrl@store');
			});
		// DEPENDENT ###
		Route::resource('/dependent','Employees\Dependent_Ctrl');
			Route::group(array('prefix'=>'dependent'),function(){
				Route::post('/store/{id}','Employees\Dependent_Ctrl@store');
			});
		// IMMIGRATION ###
		Route::resource('/immigration','Employees\Immigration_Ctrl');
			Route::group(array('prefix'=>'immigration'),function(){
				Route::post('/store1/{id}','Employees\Immigration_Ctrl@store');
				Route::post('/store2/{id}','Employees\Immigration_Ctrl@store_title');
				Route::post('/update/{id}/{emp}','Employees\Immigration_Ctrl@update_file');
				Route::get('/{id}/{filename}','Employees\Immigration_Ctrl@download'); // load image
			});
		// QUALIFICATION ##########
		Route::group(array('prefix'=>'qualification'),function(){
			// EXP ###############################################################
			Route::resource('/work-exp','Employees\Qualification\WorkExp_Ctrl');
			Route::post('/work-exp/store/{id}','Employees\Qualification\WorkExp_Ctrl@store');

			// SKILL ############################################################
			Route::resource('/skill','Employees\Qualification\Skill_Ctrl');
			Route::post('/skill/delete/{id}','Employees\Qualification\Skill_Ctrl@destroy');
			Route::post('/skill/store/{id}','Employees\Qualification\Skill_Ctrl@store');
			Route::post('/skill/searching','Employees\Qualification\Skill_Ctrl@Search');

			// LANGUAGE #########################################################
			Route::resource('/lang','Employees\Qualification\Language_Ctrl');
			Route::post('/lang/store/{id}','Employees\Qualification\Language_Ctrl@store');
			Route::post('/lang/searching','Employees\Qualification\Language_Ctrl@Search');

			// EDUCATION
			Route::resource('/education','Employees\Qualification\Education_Ctrl');
			Route::post('/education/store/{id}','Employees\Qualification\Education_Ctrl@store');

			// SEMINAR
			Route::resource('/seminar','Employees\Qualification\Seminar_Ctrl');
			Route::get('seminar/{id}/{filename}','Employees\Qualification\Seminar_Ctrl@download');
			Route::post('/seminar/store2/{id}','Employees\Qualification\Seminar_addon@store');
		//	Route::post('/seminar/store2/{id}','Employees\Qualification\addon_file@store_string');
			Route::post('/seminar/update/{id}','Employees\Qualification\Seminar_addon@update');

		});
		// REPORT TO ###############
		Route::group(array('prefix'=>'report-to'),function(){
			//SUPERVISOR
			Route::resource('/supervisor','Employees\ReportTo\Supervisor_Ctrl');
			Route::post('/supervisor/store/{id}','Employees\ReportTo\Supervisor_Ctrl@store_db');
			Route::get('/supervisor/searching/{id}','Employees\ReportTo\Supervisor_Ctrl@Search');
			//SUBORDINATE
			Route::resource('/subordinate','Employees\ReportTo\SubOrdinate_Ctrl');
			Route::post('/subordinate/store/{id}','Employees\ReportTo\SubOrdinate_Ctrl@store_db');
			Route::get('/subordinates/searching/{id}','Employees\ReportTo\SubOrdinate_Ctrl@Search');
		});
		// JOB HISTORY ##########
		// FIX
		Route::group(array('prefix'=>'job-history'),function(){
			Route::get('/termination/{id}','Employees\JobHistory\EmpTerminate_Ctrl@show');
			Route::resource('/emp','Employees\JobHistory\JobHistory_Ctrl');
			Route::get('/{id}/{filename}','Employees\JobHistory\JobHistory_Ctrl@download');
			Route::post('emp/store1/{id}','Employees\JobHistory\JobHistory_Ctrl@store');
			Route::post('emp/store2/{id}','Employees\JobHistory\JobHistory_Ctrl@store_string');
			Route::post('emp/update/{id}','Employees\JobHistory\JobHistory_Ctrl@update_history');
			Route::post('emp/subroot','Employees\JobHistory\JobHistory_Ctrl@subRoot');

			Route::post('termination/confirm/{id}','Employees\JobHistory\EmpTerminate_Ctrl@store');
			Route::get('termination/get-data/{id}','Employees\JobHistory\EmpTerminate_Ctrl@show_emp');
			Route::get('emp/create-doc/{employee_id}/{id}','Employees\JobHistory\JobHistory_Ctrl@Create_Doc');
		});
		// MANAGE SALARY #########
		Route::group(array('prefix'=>'manage-salary'),function(){
			Route::resource('/emp','Employees\Salary\Salary_Ctrl');
			Route::get('/emp-log/{id}','Employees\Salary\Salary_Ctrl@index');
			Route::post('emp/store/{id}','Employees\Salary\Salary_Ctrl@store');
		});

	});// END EMPLOYEES LIST DETAILS ###########################################

	Route::group(['prefix'=>'timekeeping'],function(){
		Route::group(['prefix'=>'biometric-setup'],function(){

			Route::get("index","Config\Biometric_inb@index");
			Route::post("authen","Config\Biometric_inb@test_authen");
			Route::get("delete","Config\Biometric_inb@delete");
			Route::post("insert_time","Config\Biometric_inb@insert_time");
			Route::post("insert_setting","Config\Biometric_inb@insert_setting");
			Route::get("test","Config\Biometric_inb@test_ftp");
			Route::get("export","Config\Biometric_inb@convert_to_txt");
			Route::get("json","Config\Biometric_inb@json");

			//biometric setup
			Route::post("insert","Config\Biometric@store");
			Route::post("update/{id}","Config\Biometric@update_ex");
			Route::delete("destroy/{id}","Config\Biometric@destroy_ex");
			//tes data
			Route::get("db","Config\Biometric_inb@db_query");
		});// END GROUP BIOMETRIC GROUP
		Route::resource('/work-shift','TimeKeeping\WorkShift_Ctrl');
		Route::get('/work-shift/{search}/search','TimeKeeping\WorkShift_Ctrl@search');
		Route::get('/getcolom','TimeKeeping\WorkShift_Ctrl@getColumns');

		Route::resource('/holiday-setup','TimeKeeping\HolidaySetup_Ctrl');
		// Route::post('/holiday-update/{$id}','TimeKeeping\HolidaySetup_Ctrl@update');
		Route::post('/search', 'TimeKeeping\HolidaySetup_Ctrl@search');
		Route::resource('/cut-of-period','TimeKeeping\CutOfPeriod_Ctrl');
	});// END TIMEKEEPING GROUP

	Route::group(['prefix'=>'attendance'],function(){

		// *********************************ATTENDANCE CUT-OFF MENU*********************************
			Route::group(['prefix'=>'cutOff'],function(){
				Route::post('/searchCutOff','Attendance\CutOff\AttendanceCutOffRecord@search');
				Route::post('/cutOff_data','Attendance\CutOff\AttendanceCutOffRecord@cuttingOff');
				Route::get('/view','Attendance\CutOff\AttendanceCutOffRecord@index');
				Route::get('/searchEmpName','Attendance\CutOff\AttendanceCutOffRecord@getName');


				//ATTENDANCE CUT OFF RECORD
				Route::post('/views','Attendance\CutOff\Attendance_CutOff@view');
				Route::post('/checkDate','Attendance\CutOff\AttendanceCutOffPeriod@checkfield');
				Route::get('/index','Attendance\CutOff\Attendance_CutOff@index');
				Route::post('/insert_comment','Attendance\CutOff\Attendance_CutOff@insert_comment');


				//ARTTENDACE CUT OFF PERIODE
				Route::get('/vPeriode','Attendance\CutOff\AttendanceCutOffPeriod@index');
				Route::post('/ePeriode','Attendance\CutOff\AttendanceCutOffPeriod@edit');
				Route::post('/sPeriode','Attendance\CutOff\AttendanceCutOffPeriod@search');
				// Generate
				Route::get('/generate','Attendance\CutOff\Attendance_CutOff@generate');

			});


		// *********************************ATTENDANCE SCHEDULE MENU*********************************
			Route::group(['prefix'=>'schedule'],function(){
				Route::post('/get-time', 'Attendance\EmployeeRecords_Ctrl@getTimeDetail');
				Route::post('/record', 'Attendance\EmployeeRecords_Ctrl@index');
				Route::post('/api_check', 'Attendance\EmployeeRecords_Ctrl@api_check');
				Route::post('/record2', 'Attendance\EmployeeRecords_Ctrl@index2');
				Route::post('/update/detail', 'Attendance\EmployeeRecords_Ctrl@updateDetail');

				//fix schedule
				Route::get('/indexFix','Attendance\FixedSchedule_Ctrl@index');
				Route::get('/indexFixShift','Attendance\FixedSchedule_Ctrl@index_shift');
				Route::post('/indexFixShiftV2','Attendance\FixedSchedule_Ctrl@index_shiftV2');
				Route::post('getName','Attendance\FixedSchedule_Ctrl@getName');
				Route::post('getView','Attendance\FixedSchedule_Ctrl@getView');
				Route::post('insertFix','Attendance\FixedSchedule_Ctrl@insert_fix');
				Route::post('updateFix','Attendance\FixedSchedule_Ctrl@update_fix');
				Route::get('getDepartment','Attendance\FixedSchedule_Ctrl@index_department');
				Route::delete('deleteFix/{id}','Attendance\FixedSchedule_Ctrl@delete_fix');
				
				//notification
				Route::get('/insert_notif','Attendance\notification@insert_notif');
				Route::get('/send_stat','Attendance\notificationV2@read_stat');
				Route::get('/read_notif','Attendance\notificationV3@read_notif');
				Route::get('/count_notif','Attendance\notificationV3@count_notif');
				Route::get('/gotcha','Attendance\notificationV2@get_chat');
				Route::post('/onMessage','Attendance\notificationV2@sent_message');
				// Route::post('/approve_notif','Attendance\notification@approve');
				// Route::post('/reject_notif','Attendance\notification@reject');
				Route::post('/approve_notif','Attendance\notificationV3@approve');
				Route::post('/reject_notif','Attendance\notificationV3@reject');
				Route::post('/cancle_notif','Attendance\notificationV3@cancle');


				//notification mod
				Route::get('/insert_notif','Attendance\notification@insert_notif');
			});
			// SCHEDULE IMPORT
			Route::group(['prefix'=>'schedule-import'],function(){
				Route::resource('/','Attendance\ScheduleImport_Ctrl');
				Route::get('/getfile','Attendance\ScheduleImport_Ctrl@getfile');
				Route::post('insertxls','Attendance\ScheduleImport_Ctrl');
				Route::post('importDataXLS','Attendance\ScheduleImport_Ctrl@importDataXLS');
				Route::post('','Attendance\ScheduleImport_Ctrl');
				Route::post('/xls','Attendance\ScheduleImport_Ctrl@import');
				Route::get('/api_permission','Attendance\ScheduleImport_Ctrl@api_permission');
			});

		// *********************************ATTENDANCE REQUEST MENU*********************************
			// TRAINING REQUEST
			Route::group(['prefix'=>'training'],function(){
				Route::post('/request','Attendance\ScheduleRequest\TrainingRequest_Ctrl@createTrainingRequest');
				Route::post('/filter','Attendance\ScheduleRequest\TrainingRequest_Ctrl@filter');
				Route::get('/search/{string}','Attendance\ScheduleRequest\TrainingRequest_Ctrl@findName');
				Route::get('api_check','Attendance\ScheduleRequest\TrainingRequest_Ctrl@api_check');
			});
			// OVERTIME REQUEST
			Route::group(['prefix'=>'overtime'],function(){
				Route::get('/api_check', 'Attendance\ScheduleRequest\Overtime_Ctrl@api_check');
				Route::post('/request', 'Attendance\ScheduleRequest\Overtime_Ctrl@createOvertimeRequest');
				Route::post('/work-hour', 'Attendance\ScheduleRequest\Overtime_Ctrl@currentWorkHour');
			});
			// UNDERTIME REQUEST
			Route::group(['prefix'=>'undertime'],function(){
				Route::post('/request', 'Attendance\ScheduleRequest\Overtime_Ctrl@createUndertimeRequest');
				Route::post('/work-hour', 'Attendance\ScheduleRequest\Overtime_Ctrl@currentWorkHour');
			});
			// CHANGE / SWAP REQUEST
			Route::group(['prefix'=>'change-swap-shift'],function(){
				Route::post('/work', 'Attendance\ScheduleRequest\SwapShift_ctrl@tes');
				Route::post('/request', 'Attendance\ScheduleRequest\SwapShift_ctrl@changeSwapShift');
				Route::post('/work-hour', 'Attendance\ScheduleRequest\SwapShift_ctrl@work');
				Route::post('/shift-by-current', 'Attendance\ScheduleRequest\SwapShift_ctrl@WorkHourCurrentDate');
				Route::get('/default-change-shift', 'Attendance\ScheduleRequest\SwapShift_ctrl@index');
				Route::post('swap_value', 'Attendance\ScheduleRequest\SwapShift_ctrl@swap_value');
			});
			// ATTENDANCE REQUEST LIST
			Route::group(['prefix'=>'schedule-request-detail'],function(){
				Route::get('/view', 'Attendance\ScheduleRequestList_Ctrl@index');
				Route::post('/search', 'Attendance\ScheduleRequestList_Ctrl@search');
				Route::post('/export', 'Attendance\ScheduleRequestList_Ctrl@exportXLS');

				// Route::get('/excel','Leave\Report\Report_Ctrl@generate');

				Route::post('/update', 'Attendance\ScheduleRequestDetail\RequestDetailUpdate_Ctrl@update');
				Route::post('/edit/{id}', 'Attendance\ScheduleRequestDetail\RequestDetailUpdate_Ctrl@edit');
				Route::post('/save/{id}', 'Attendance\ScheduleRequestDetail\RequestDetailUpdate_Ctrl@save_request');
				Route::get('/getfile/{id}', 'Attendance\ScheduleRequestDetail\RequestDetailUpdate_Ctrl@getfile');
				Route::post('/index', 'Attendance\ScheduleRequestDetail\RequestDetailUpdate_Ctrl@index2');
				Route::post('/approve/{id}', 'Attendance\ScheduleRequestDetail\RequestDetailUpdate_Ctrl@approve');
				Route::post('/reject/{id}', 'Attendance\ScheduleRequestDetail\RequestDetailUpdate_Ctrl@reject');
				Route::post('/cancel/{id}', 'Attendance\ScheduleRequestDetail\RequestDetailUpdate_Ctrl@cancel');
			});// END SCHEDULE REQUEST DETAILS

		// *********************************ATTENDANCE CONFIGURATION MENU*********************************



		Route::group(['prefix'=>'schedule-list'],function(){
			Route::get('/view', 'Attendance\UnapprovedSchedule_Ctrl@index_schedulelist');
			Route::post('/search_schedule', 'Attendance\UnapprovedSchedule_Ctrl@search_schedule');
			Route::get('/viewShift', 'Attendance\ScheduleList_Ctrl@index_shiftcode');
		});// END SCHEDULE LIST GROUP
		Route::group(['prefix'=>'unapproved-schedule'],function(){
			Route::get('/view', 'Attendance\ScheduleList_Ctrl@index');
			Route::post('/approve', 'Attendance\ScheduleList_Ctrl@approve_all');
			Route::get('/viewWorkShift', 'Attendance\ScheduleList_Ctrl@index_shift');

			Route::post('/search', 'Attendance\ScheduleList_Ctrl@search');
			Route::post('/update', 'Attendance\ScheduleList_Ctrl@change_workshift');

		});// END UN-APPROVED SCHEDULE GROUP
		//MANUAL INPUT ATTENDANCE
		Route::group(['prefix'=>'manual-input'], function(){
			Route::get('view','Attendance\ManualInput_Ctrl@index');
			Route::post('search','Attendance\ManualInput_Ctrl@search_data');
			Route::post('insert','Attendance\ManualInput_Ctrl@insert_data');

		});

		//END MANUAL INPUT ATTENDANCE
		Route::group(['prefix'=>'schedule-request'],function(){
			Route::post('update-overtime/{id}', 'Attendance\ScheduleRequestList_Ctrl@update');
		});// END SCHEDULE REQUEST GROUP
	}); // END ATTENDANCE GROUP

	Route::group(['prefix'=>'leave'],function(){
		// Emp Entitlements
		Route::resource('/Emp','Leave\Entitlements\EmployeeEntitlements_Ctrl@index');
		Route::post('adjust','Leave\Entitlements\EmployeeEntitlements_Ctrl@adjust');
		Route::post('confirm','Leave\Entitlements\EmployeeEntitlements_Ctrl@confirm');
		Route::post('viewEmp','Leave\Entitlements\EmployeeEntitlements_Ctrl@viewEmp');
		Route::post('searchEmp','Leave\Entitlements\EmployeeEntitlements_Ctrl@search');

		//leave_table
		//all
		Route::get('/viewJobTitle','Leave\Leave_Table@index_jobtitile');
		Route::resource('EnchanceLeave','Leave\Leave_Table@index');
		Route::get('/vacation','Leave\Leave_Table@department');
		Route::post('/viewVacation','Leave\Leave_Table@search_leave_vacation');

		//vacation
		Route::get('/viewVacation','Leave\Leave_Table@index_vacation');
		Route::post('/addVacation','Leave\Leave_Table@insert_vacation_leave');
		Route::delete('/deleteVacation/{id}','Leave\Leave_Table@destroy_vacation');
		Route::post('/updateVacation/{id}','Leave\Leave_Table@update_vacation');

		//sick
		Route::post('sickLeave','Leave\Leave_Table@insert_sick_leave');
		Route::post('sickUpdate/{id}','Leave\Leave_Table@update_sick_leave');
		Route::delete('sickDelete/{id}','Leave\Leave_Table@destroy_sick');

		//bereavement vacation
		Route::post('bereavementLeave','Leave\Leave_Table@insert_bereavement');
		Route::post('bereavementUpdate/{id}','Leave\Leave_Table@update_bereavement');
		Route::delete('bereavementDelete/{id}','Leave\Leave_Table@delete_bereavement');

		//enchanment
		Route::post('enchancedLeave','Leave\Leave_Table@insert_enchanced');
		Route::post('updateEnchanced/{id}','Leave\Leave_Table@update_enchanced');
		Route::delete('deleteEnchanced/{id}','Leave\Leave_Table@delete_enchanced');

		//Leave Report
		Route::get('viewNameEmp','Leave\Entitlements\Report_Ctrl@viewEmp');
		Route::get('view','Leave\Entitlements\Report_Ctrl@index');
		Route::post('searchReport','Leave\Entitlements\Report_Ctrl@report');
		Route::get('/list', 'Leave\tes\List_Ctrl@index');

		//Leave List
		Route::get('empList','Leave\Entitlements\LeaveList_Ctrl@viewEmp');
		Route::get('getList','Leave\Entitlements\LeaveList_Ctrl@index');
		Route::post('searchList','Leave\Entitlements\LeaveList_Ctrl@search');
		Route::post('approveList/{id}','Leave\Entitlements\LeaveList_Ctrl@approve');
		Route::post('rejectList/{id}','Leave\Entitlements\LeaveList_Ctrl@reject');
		Route::post('canceledList/{id}','Leave\Entitlements\LeaveList_Ctrl@canceled');
		Route::post('searchDetail/{id}','Leave\Entitlements\LeaveList_Ctrl@leave_request_detail');
		Route::get('getFile/{id}', 'Leave\Entitlements\LeaveList_Ctrl@getfile');
		Route::post('save_request/{id}', 'Leave\Entitlements\LeaveList_Ctrl@save_request');

		//request
		Route::post('requestDetail','Leave\Entitlements\Leaverequest_Ctrl@leave_request_detail');
		Route::post('getDOB','Leave\Entitlements\Leaverequest_Ctrl@DayOffBetween');
		Route::post('gettg','Leave\Entitlements\Leaverequest_Ctrl@baseBirth');
		Route::resource('Request','Leave\Entitlements\LeavereMasterCtrl@index');
		Route::resource('RequestBrea','Leave\Entitlements\LeavereMasterCtrl@get_bereavement');
		Route::resource('RequestSus','Leave\Entitlements\Leaverequest_Ctrl@get_suspension');
		Route::post('RequestRemainBrea','Leave\Entitlements\Leaverequest_Ctrl@get_bereavement');
		Route::resource('autoRequest','Leave\Entitlements\Leaverequest_Ctrl@autocomplete');
		Route::post('saving_request','Leave\Entitlements\LeavereMasterCtrl@request_leave');
		Route::get('requestSchedule','Leave\Entitlements\Leaverequest_Ctrl@index_shift');
		Route::post('remaining','Leave\Entitlements\LeavereMasterCtrl@remain');
		Route::post('check_do','Leave\Entitlements\Leaverequest_Ctrl@check_ado');
		Route::post('nearest_day','Leave\Entitlements\Leaverequest_Ctrl@nearest_day');

		//addon
		//Route::post('saving_request','Leave\Entitlements\LeaveRequest_mod@validation');

	});// END LEAVE GROUP

	Route::post('test', 'Attendance\EmployeeRecords_Ctrl@testing');
	Route::group(['prefix'=>'process'],function(){

	});// END PROCESS GROUP

	Route::group(['prefix'=>'infraction'],function(){
		Route::get('index','Infraction\Add_Ctrl@index');
		Route::post('search','Infraction\List_Ctrl@search');
		Route::post('update','Infraction\List_Ctrl@update_infraction');
		Route::get('delete/{id}','Infraction\List_Ctrl@delete_infraction');
		Route::post('insert','Infraction\Add_Ctrl@create');
		Route::post('detail/{id}','Infraction\List_Ctrl@detail');
	});// END INFRACTION GROUP


});// END PAGES


Route::get('ses',function(CookieJar $cookie, Response $res){
	$reg = \Session::regenerate('res');
	return (array)$reg;
});

Route::post('asdf','Organization\Department_Ctrl@jwt_test');

Route::post('jwt/register', 'JwtAuth_Ctrl@Registered');
Route::post('jwt/authenticate', 'JwtAuth_Ctrl@authenticate');
Route::get('jwt/authenticate/user', 'JwtAuth_Ctrl@getAuthenticatedUser');
Route::resource('jwt/todo', 'TodoController');

Route::get('asdf2',function(){
	});
