<?php namespace Larasite\Http\Controllers\Attendance;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\Attendance\Request\EmployeeRecordDetail_Model as record;
use Illuminate\Http\Request;
use Larasite\Library\FuncAccess;

class EmployeeRecords_Ctrl extends Controller {
	private $form = 56;
	// VIEW ATTENDANCE RECORD FOR EMPLOYEE*********************************
	public function api_check(){
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		if($access[1] == 200){
			if ($access[3]['create'] == 0 and $access[3]['delete'] == 0 and $access[3]['read'] == 0 and $access[3]['update'] == 0 ){
				$message = "Unauthorized"; $data = []; $status =  200;
			}else{

			//$request = new leaverequest_Model;
				$key = \Input::get("key");
				$encode = base64_decode($key);
				$explode = explode("-",$encode);
				$explode_id = $explode[1];
				$supervisor = \DB::SELECT("select supervisor from emp_supervisor where supervisor='$explode_id' ");
				$hr = \DB::select("SELECT t1.employee_id FROM ldap t1 INNER JOIN role t2 ON t2.role_id = t1.role_id WHERE t1.employee_id = '$explode_id' AND t2.role_name = 'hr'");
				if($supervisor != null){
					$supervisor = "supervisor";
				}elseif($hr != null){
					$supervisor = "hr";
				}else{
					$supervisor =  "user";
				}
				$emp_data = \DB::SELECT("CALL search_employee_by_id('$explode_id')");


				$data_exp =
				[
					"employee_name" => $emp_data[0]->name,
					"employee_id" => $explode_id,
					"status" => $supervisor
				];

				$message = "success"; $data = $data_exp; $status =  200;
			}
		}else{ 
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status,"access" => $access[3]],'data'=>$data],$status);
	}

	public function index(){

		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		$model = new record;
		$date_req = null;
		
		if($access[1] == 200){
			if(\Request::get('name')){
				$name = \Request::get('name');
				$dates = \Request::get('datess');
				$date_req = \Request::get('selected_dates');
				$rt = \Request::get('requestType');
			}else{			
				$name = \Input::get('name');
				$dates = \Input::get('datess');
				$date_req = \Input::get('selected_dates');
				$rt = \Input::get('requestType');
			}

			
			$rule = [
				'name' => 'required | Regex:/^[A-Za-z0-9 ]+$/',
			];
			$validator = \Validator::make(['name'=>$name],$rule);
			
			if($validator->fails()){
				$val = $validator->errors()->all();
				return response()->json(['header' => ['message' => $val[0], 'status' => 500 ], 'data' => []], 500 );
			}else{
				
				// throw user input to model to get attendance record
				$select_date = [];
				if($date_req && $dates){
					$select_date = \DB::SELECT("CALL get_schedule_date('$name')");
					if(count($select_date) > 0){
						$tmp = [];
						foreach ($select_date as $key => $value) {
							$tmp[]=$value->date_digit;
						}

						foreach ($select_date as $key => $value) {
							if($select_date[$key]->date_digit == $dates){
								$select_date[$key]->default = true;
							}else{
								$select_date[$key]->default = false;
							}
						}

						if(!in_array($dates, $tmp)){
							$dates = $select_date[count($select_date)-1]->date_digit;
							$select_date[count($select_date)-1]->default = true;
						}

					}
				}else if(!$date_req && $dates){
					$select_date = \DB::SELECT("CALL get_schedule_date('$name')");
					if(count($select_date) > 0){
						$tmp = [];
						foreach ($select_date as $key => $value) {
							$tmp[]=$value->date_digit;
						}

						foreach ($select_date as $key => $value) {
							if($select_date[$key]->date_digit == $dates){
								$select_date[$key]->default = true;
							}else{
								$select_date[$key]->default = false;
							}
						}

						if(!in_array($dates, $tmp)){
							$dates = $select_date[count($select_date)-1]->date_digit;
							$select_date[count($select_date)-1]->default = true;
						}

					}
				}

				//return [$name,$dates,null,$date_req, $select_date, \Input::get('datess'), \Request::get('datess')];
				$getRecordDetail = $model->getDetail($name,$dates,null,$date_req);
				
				if(count($getRecordDetail['data']) > 0){
					$dt = $getRecordDetail['data'];
					$getRecordDetail['data'] = ['schedule'=>$dt,'date_range'=>$select_date];
					$getRecordDetail['datas'] = ['schedule'=>$dt,'date_range'=>$select_date];
				 	// if($date_req){
				 	// }
				}
				return $getRecordDetail;
				
				return response()->json(['header' => ['message' => $getRecordDetail['message'], 'status' => $getRecordDetail['status'], 'access'=>$access[3] ], 'data' => $getRecordDetail['data']], $getRecordDetail['status']);
			}
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}

	// public function index(){
	// 	// for ($i=1; $i < 30; $i++) {
	// 	// 	$date = date('2015-12-'.$i);
	// 	// 	\DB::select("insert into att_schedule (employee_id, date, shift_id, status) values ('2015001','$date',3,1)");
	// 	// 	# code...
	// 	// }
	// 	// return 0;
	// 	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	// 	$model = new record;
	// 	if($access[1] == 200){
	// 		$name = \Input::get('name');
	// 		$dates = \Input::get('datess');
	// 		$rule = [
	// 			'name' => 'required | Regex:/^[A-Za-z0-9 ]+$/',
	// 		];
	// 		$validator = \Validator::make(\Input::all(),$rule);

	// 		if($validator->fails()){
	// 			$val = $validator->errors()->all();
	// 			return response()->json(['header' => ['message' => $val[0], 'status' => 500 ], 'data' => null], 500 );
	// 		}else{
	// 			// throw user input to model to get attendance record
	// 			 $select_date = \DB::SELECT("CALL get_schedule_date('$name')");
	// 			 if(count($select_date) > 0){
	// 			 	$tmp = [];
	// 			 	foreach ($select_date as $key => $value) {
	// 			 		$tmp[]=$value->date_digit;
	// 			 	}

	// 			 	foreach ($select_date as $key => $value) {
	// 			 		if($select_date[$key]->date_digit == $dates){
	// 			 			$select_date[$key]->default = true;
	// 			 		}else{
	// 			 			$select_date[$key]->default = false;
	// 			 		}
	// 			 	}

	// 			 	if(!in_array($dates, $tmp)){
	// 			 		$dates = $select_date[count($select_date)-1]->date_digit;
	// 			 		$select_date[count($select_date)-1]->default = true;
	// 			 	}

	// 			 }

	// 			 $getRecordDetail = $model->getDetail($name,$dates);
	// 			 // if($name == '2018030'){
	// 			 // 	return $getRecordDetail;
	// 			 // }

	// 			 try {			 	
	// 				 if(count($getRecordDetail['data']) > 0){
	// 				 	$dt = $getRecordDetail['data'];
	// 				 	$getRecordDetail['data'] = ['schedule'=>$dt,'date_range'=>$select_date];
	// 				 }
	// 			 } catch (\Exception $e) {
	// 			 	return $getRecordDetail;
	// 			 }
	// 			 return $getRecordDetail;
	// 			return response()->json(['header' => ['message' => $getRecordDetail['message'], 'status' => $getRecordDetail['status'], 'access'=>$access[3] ], 'data' => $getRecordDetail['data']], $getRecordDetail['status']);
	// 		}
	// 	}
	// 	else{
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 	}
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	// }

	// =======================================



	// public function checkApproveAttendance($name){
	// 			// throw user input to model to get attendance record
	// 			 $getRecordDetail = $model->getDetail($name);
	// 			 if($getRecordDetail){
	// 			 	return true;
	// 			 }else{ return false; }
	// }

	// UPDATE DETAIL EMPLOYEE ATTENDANCE RECORD*********************************
	/*public function whoiam($arg,$arg2){
		if($arg != $arg2){
			return false;
		}else{
			//lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%'
			$db  = \DB::SELECT("select * from view_nonactive_login where employee_id  = '$arg' and  (
				               lower(role_name) like '%user%' or lower(role_name) like '%regular%')");
			if($db ==  null){
				return true;
			}else{
				return false;
			}


		}
	}*/

	public function whoiam($arg,$arg2){
		if($arg != $arg2){
			return false;
		}else{
			//lower(role_name) like '%human resource%' or lower(role_name) like '%resource human%'
			$db  = \DB::SELECT("select * from view_nonactive_login where employee_id  = '$arg' and  (
				lower(role_name) like '%user%' or lower(role_name) like '%regular%')");
			if($db ==  null){
				if($arg == $arg2){
					return false;
				}else{
					return true;
				}
			}else{
				return false;
			}


		}
	}

	public function updateDetail()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		$model = new record;
		if($access[1] == 200){
			
			return 100;

			//id_requestor
			$key = \Input::get("key");
			$encode = base64_decode($key);
			$explode = explode("-",$encode);
			$explode_id = $explode[1];
			

			$i = \Input::all();
			$json = \Input::get('data');
			if(isset($json)){
				$i = json_decode($json,1);
			}else{
				$i = $i;
			}

			// backup 09/10/2018
			// allow duplicate request

			$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'num'=>'Regex:/^[0-9-\^ ]+$/',
			'time'=>'Regex:/^[0-9-\ ,\: ]+$/'];
					//,'home_telephone'=>$reg['num'],'mobile_telephone'=>$reg['num'],'work_telephone'=>$reg['num'],'address'=>$reg['text'],'city'=>$reg['text'],'state'=>$reg['text'],'zip'=>$reg['text_num'],'country'=>$reg['twit']
			$rule_time = [
				"date"=>"required|date",
				'employee_id'=>'required|'.$reg['num'].'|max:8',
				"requestType"=>"required|".$reg['text']."|max:25",
				/*"newtimeIn"=>$reg['time']."|max:6",
				"newtimeOut"=>$reg['time']."|max:6",*/
				"timeIn"=>$reg['time']."|max:6",
				"timeOut"=>$reg['time']."|max:6",
				"timeInStatus"=>$reg['text_num']."|max:6",
				"timeOutStatus"=>$reg['text_num']."|max:6",
				"comment"=>$reg['text_num']
				/*"date_timeIn"=>'required|date',
				"date_timeOut"=>'date'*/
			];
			$rule_late = [
				"date"=>"required|date",
				'employee_id'=>'required|'.$reg['num'].'|max:8',
				"requestType"=>"required|".$reg['text']."|max:30",
				"title"=>"required|".$reg['text']."|max:30",
				'Late'=>'required|'.$reg['num'],
				'EarlyOut'=>'required|'.$reg['num'],
				"comment"=>$reg['text_num'],


			];

			// $tmp_data = \Input::all();
			// if(isset($json)){
			// 	$tmp_data= $json;
			// }
			if($i['requestType'] == 'Time-in / Time-Out' ){
					// return \Response::json(\Input::all(),200);
				if(isset($i['newtimeIn'])){
					$rule_time["newtimeIn"] = $reg['time'].'|max:6';
					$rule_time["date_timeIn"] = 'required|date';
				}else{
					$i['date_timeIn'] = null;
					$i['newtimeIn'] = null;

				}
				if(isset($i['newtimeOut'])){
					$rule_time["newtimeOut"] = $reg['time']."|max:6";
					$rule_time["date_timeOut"] = 'required|date';
				}else{
					$i['date_timeOut'] = null;
					$i['newtimeOut'] = null;

				}

				$nextdate_con = 0;

				//$getRecordDetail = $model->getDetail($i['employee_id'],'',null,'2020-04-01');

				$bio = \DB::select("select * from biometrics_device where employee_id = '$i[employee_id]' and date='$i[date]'");

				if(count($bio) > 0){
					// biometric ada
					if($i['newtimeIn'] != null && $i['newtimeOut'] != null){
						
						$req_out = date("a",strtotime($i['newtimeOut']));
						$req_in = date("a",strtotime($i['newtimeIn']));

						if($req_in == 'am' && $req_out == 'pm' && strtotime($i['newtimeOut']) > strtotime("23:59") ){
							$nextdate_con = 1;
						}else if($req_in == 'pm' && $req_out == 'am'){
								$nextdate_con = 1;
						}

						if(strtotime($i['date_timeIn']) < strtotime($i['date_timeOut'])){
							$nextdate_con = 1;	
						}

					}else if($i['newtimeIn'] && !$i['newtimeOut']){

						$bio_out = date("a",strtotime($bio[0]->time_out));
						$req_in = date("a",strtotime($i['newtimeIn']));

						if($bio_out == 'am' && $req_in == 'pm' && strtotime($i['newtimeOut']) > strtotime("23:59")){
							$nextdate_con = 1;
						}else if($bio_out == 'pm' && $req_in == 'am'){
							$nextdate_con = 1;
						}

					}else if(!$i['newtimeIn'] && $i['newtimeOut']){
						$req_out = date("a",strtotime($i['newtimeOut']));
						$bio_in = date("a",strtotime($bio[0]->time_in));

						if($bio_in == 'am' && $req_out == 'pm' && strtotime($i['newtimeOut']) > strtotime("23:59") ){
							$nextdate_con = 1;
						}else if($bio_in == 'pm' && $req_out == 'am'){
							$nextdate_con = 1;
						}
					}
				}else{

					if($i['newtimeIn'] != null && $i['newtimeOut'] != null){

						$req_out = date("a",strtotime($i['newtimeOut']));
						$req_in = date("a",strtotime($i['newtimeIn']));

						if($req_out == 'pm' && $req_in == 'am' && strtotime($i['newtimeOut']) > strtotime("23:59")){
							$nextdate_con = 1;
						}else if($req_out == 'am' && $req_in == 'pm'){
							$nextdate_con = 1;
						}

						if(strtotime($i['date_timeIn']) < strtotime($i['date_timeOut'])){
							$nextdate_con = 1;	
						}


					}else{
						if(!$i['newtimeIn']){
							$message='Data not found.'; $status=500; $data=null;
							return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
						}
					}
				}

				$valid_time = \Validator::make($i,$rule_time);
				if($valid_time->fails()){
					$message='Data not found.'; $status=500; $data=null;
					return \Response::json(['header'=>['message'=>/*$message*/ $valid_time->Messages(),'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
				}else{
						$i['nextday_con'] = $nextdate_con;
					// if($nextdate_con > 0){
					// }
				}
			}else if($i['requestType'] == 'Early Out Exemption Request' || $i['requestType'] == 'Late Request' ){
				$valid_late = \Validator::make($i,$rule_late);
				if($valid_late->fails()){
					$message='Data not found.'; $status=500; $data=null;
					return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
				}
			}

			

			// return "ok";

			$respon_1 =  \Response::json(['header'=>['message'=>"Error : requestor only employee",'status'=>500, "access" => $access[3]],'data'=>[]],500);
			$hr = \DB::SELECT("select employee_id from  ldap, role where ldap.employee_id = '$explode_id' and ldap.role_id = role.role_id and (lower(role.role_name) like '%human%' or  '%human resource%' or '%hr%')");
			
			
			// backup 09/10/2018
			if($i['employee_id'] != $explode_id){
				if($explode_id != '2014888'){
					if(count($hr) == 0){
						return $respon_1;
					}
				}
			}

			if($i['requestType'] == 'Time-in / Time-Out' ){
				$check =  $this->whoiam($explode_id,$i['employee_id']);
				if($check == true){
					if(count($hr) == 0){
						return $respon_1;
					}
				}
			}
			if($i['requestType'] == 'Early Out Exemption Request' || $i['requestType'] == 'Late Request' ){
				$check =  $this->whoiam($explode_id,$i['employee_id']);

				if($check == true){
					if(count($hr) == 0){
						return $respon_1;
					}
				}else{
					$check_db  = \DB::SELECT("select * from emp where local_it = 1");
					if($check_db == null){
						if(count($hr) == 0 || $explode_id != '2014888'){
							return \Response::json(['header'=>['message'=>"Error : requestor only employee local",'status'=>500, "access" => $access[3]],'data'=>[]],500);
						}

					}
				}	
			}
			
			// check if input contain comment or not (set to "" if comment is not set)
			if (isset($i['comment'])){ $comment = $i['comment']; }
			else{ 
				$i['comment'] = null;
				$comment = $i['comment'];
			}

			// throw user input to model to update attendance record detail
			if(count($hr) > 0){
				$i['hr'] = true;
			}else{
				if($explode_id == '2014888'){
					$i['hr'] = true;
				}else{
					$i['hr'] = false;
				}
			}

			$update = $model->updateDetail($i,$comment);	
			//return $update;
			return response()->json(['header' => ['message' => $update['message'], 'status' => $update['status'], 'access'=>$access[3] ], 'data' => $update['data']], $update['status']);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}

	// public function updateDetail()
	// {
	// 	/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
	// 	$model = new record;
	// 	if($access[1] == 200){

	// 		//id_requestor
	// 		$key = \Input::get("key");
	// 		$encode = base64_decode($key);
	// 		$explode = explode("-",$encode);
	// 		$explode_id = $explode[1];


	// 		$i = \Input::all();
	// 		$json = \Input::get('data');
	// 		if(isset($json)){
	// 			$i = json_decode($json,1);
	// 		}else{
	// 			$i = $i;
	// 		}

	// 		// backup 09/10/2018
	// 		// allow duplicate request
	// 		$respon_1 =  \Response::json(['header'=>['message'=>"Error : requestor only employee",'status'=>500, "access" => $access[3]],'data'=>[]],500);
	// 		$hr = \DB::SELECT("select employee_id from  ldap, role where ldap.employee_id = '$explode_id' and ldap.role_id = role.role_id and (lower(role.role_name) like '%human%' or  '%human resource%' or '%hr%')");


	// 		// backup 09/10/2018
	// 		if($i['employee_id'] != $explode_id){
	// 			if($explode_id != '2014888'){
	// 				if(count($hr) == 0){
	// 			   	  return \Response::json(['header'=>['message'=>"Error : requestor only employee1",'status'=>500, "access" => $access[3]],'data'=>[]],500);
	// 			   	}
	// 			}
	// 		}

	// 		// if($i['requestType'] == 'Time-in / Time-Out' ){
	// 	 //   		if(count($hr) == 0){
	// 		//    		$check =  $this->whoiam($explode_id,$i['employee_id']);
	// 		//    		if($check == true){
	// 		// 	   		return \Response::json(['header'=>['message'=>"Error : requestor only employee2",'status'=>500, "access" => $access[3]],'data'=>[]],500);
	// 		// 	   	}
	// 		//    }
	// 		// }
	// 		if($i['requestType'] == 'Early Out Exemption Request' || $i['requestType'] == 'Late Request' ){
	// 		   $check =  $this->whoiam($explode_id,$i['employee_id']);

	// 		   if($check == true){
	// 		   	  if(count($hr) == 0){
	// 			   	  return \Response::json(['header'=>['message'=>"Error : requestor only employee3",'status'=>500, "access" => $access[3]],'data'=>[]],500);
	// 			   	}
	// 		   }else{
	// 		   	 $check_db  = \DB::SELECT("select * from emp where local_it = 1");
	// 		   	 if($check_db == null){
	// 		   	 	if(count($hr) == 0 || $explode_id != '2014888'){
	// 			   	  return \Response::json(['header'=>['message'=>"Error : requestor only employee local",'status'=>500, "access" => $access[3]],'data'=>[]],500);
	// 			   	}

	// 		   	 }
	// 		   }	
	// 		}

	// 		// check if input contain comment or not (set to "" if comment is not set)
	// 		if (isset($i['comment'])){ $comment = $i['comment']; }
	// 		else{ 
	// 			$i['comment'] = null;
	// 			$comment = $i['comment'];
	// 		}

	// 		// throw user input to model to update attendance record detail
	// 		if(count($hr) > 0){
	// 			$i['hr'] = true;
	// 		}else{
	// 			if($explode_id == '2014888'){
	// 				$i['hr'] = true;
	// 			}else{
	// 				$i['hr'] = false;
	// 			}
	// 		}
	// 		$update = $model->updateDetail($i,$comment);
	// 		return response()->json(['header' => ['message' => $update['message'], 'status' => $update['status'], 'access'=>$access[3] ], 'data' => $update['data']], $update['status']);
	// 	}
	// 	else{
	// 		$message = $access[0]; $status = $access[1]; $data=$access[2];
	// 	}
	// 	return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	// }

	// GET TIME AND SCHEDULE FOR DETAIL*************************************************
	public function getTimeDetail()
	{
		/*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
		$model = new record;
		if($access[1] == 200){
			$emp_id = \Input::json('employee_id');
			$date = \Input::json('date');
			$requestType = \Input::json('requestType');
			$requestType = strtolower($requestType);

			if (strpos($requestType,'time') !== false){
				$update = $model->getTimeInOut_model($emp_id,$date);
			}
			else if (strpos($requestType,'late') !== false){
				$requestType = "late";
				// return $data = [$emp_id,$date,$requestType];
				$update = $model->getTimeLateEarly_model($emp_id,$date,$requestType);
			}
			else if (strpos($requestType,'early') !== false){
				$requestType = "early";
				// return $data = [$emp_id,$date,$requestType];
				$update = $model->getTimeLateEarly_model($emp_id,$date,$requestType);
			}
			else{
				return response()->json(['header' => ['message' => 'Can not view employee record detail', 'status' => 500 , 'access'=>$access[3]], 'data' => null], 500);
			}
			return response()->json(['header' => ['message' => $update['message'], 'status' => $update['status'], 'access'=>$access[3] ], 'data' => $update['data'][0]], $update['status']);
		}
		else{
			$message = $access[0]; $status = $access[1]; $data=$access[2];
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status, "access" => $access[3]],'data'=>$data],$status);
	}

}
