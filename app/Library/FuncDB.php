<?php namespace Larasite\Library{

use Larasite\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class FuncDB {     
	
	public function get_data_columns($table,$type)
	{
		return $this->get_columns($table,$type);
	}
		// protected $typeCall = ['update'=>"CALL Update_WorkShift",
		// 					'store'=>"CALL Insert_WorkShift",
		// 					'view'=>"CALL View_WorkShift",
		// 					'delete'=>"CALL Delete_WorkShift"];
	public function get_columns($table,$type){
		$data = NULL;  $i = 0;
		$get =  \DB::select("show columns from $table where Field != 'updated_at' and Field != 'created_at' and Field != 'shift_id'"); 
		if($type == 'edit'){ $get =  \DB::select("show columns from $table where Field != 'updated_at' and Field != 'created_at'");  }

		while( $i < count($get)){
			foreach ($get[$i] as $key => $value) {
				if($key == 'Field'){ $rule[] = $value; }
				elseif($key == 'Type'){ $type[] = $value; }
			}
			$i++;
		}	
		return $rule;
	}

	public function call($action,$input)
	{
		if(isset($action['update'])){ $procedure = $action['update']; $message = 'Update Successfully.';}
		elseif(isset($action['store'])) { $procedure = $action['update']; $message = 'Store Successfully.';}
		elseif(isset($action['view'])) { $procedure = $action['view']; $message = 'Show records data.';}
		elseif(isset($action['delete'])) { $procedure = $action['delete']; $message = 'Delete Successfully.';}
		//else{ return throw new Exception("Error Method Call Prosedure", 1);}

		$get = \DB::select("$procedure(".$input.")");	
		
		if($get){
			foreach ($get as $key) { 
				if(isset($key->Message, $key->Status)){
					$message = $key->Message; $status = 500; $data = NULL;
				}else{ $message = $message; $status = 200; $data = $get;  }
			}
		}else{ $message = 'Empty records data.'; $status = 200; $data = NULL; }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
}
}