<?php namespace Larasite\Library{

use Larasite\Http\Requests;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\AddEmployee_Model;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

	class FuncUpload
	{
		protected $param = ['employee','job_history','manage_salary'];

			private function BuildQuery($arr,$table){
			$z = "SELECT ";
			$i = 1;
			if(isset($arr)){
				foreach ($arr as $key) {
					if($i == count($arr)){
						$z .= $key." FROM $table where employee_id = '#employee_id'";
					}else{ $z .= $key." , ";}
					$i++;
				}
			}else{
				$z = null;
			}
			return $z;
			} // END BUILD QUERY EMP

			private function check_param($field,$text,$employee_id)
			{
				
				if(isset($field['employee'])){
					$get = str_replace('#employee_id', $employee_id, $field['employee']['query']);
					$check = \DB::select($get);

					if($check){
						$r = (array) \DB::select($get)[0];
						$i=0;
						while($i < count($field['employee']['field'])){
							$f = $field['employee']['field'][$i];
							if($r[$f]){ $text = str_replace("#".$field['employee']['binding'][$i],$r[$f] , $text);	 }
							else{ $text = str_replace("#".$field['employee']['binding'][$i]," " , $text); }
							$i++;
						}	
					}else{
						$i=0;
						while($i < count($field['employee']['field'])){
							$f = $field['employee']['field'][$i];
							$text = str_replace("#".$field['employee']['binding'][$i]," " , $text);
							$i++;
						}
				}	}
				return $text;
			}

			private function Generate_Text($a,$text,$employee_id){
				if(isset($a['employee'])){
					$get = str_replace('#employee_id', $employee_id, $a['employee']['query']);
					$check = \DB::select($get);

					if($check){
						$r = (array) \DB::select($get)[0];
						$i=0;
						while($i < count($a['employee']['field'])){
							$f = $a['employee']['field'][$i];
							if($r[$f]){
								$text = str_replace("#".$a['employee']['binding'][$i],$r[$f] , $text);	
							}else{
								$text = str_replace("#".$a['employee']['binding'][$i]," " , $text);
							}
							$i++;
						}	
					}else{
						$i=0;
						while($i < count($a['employee']['field'])){
							$f = $a['employee']['field'][$i];
								$text = str_replace("#".$a['employee']['binding'][$i]," " , $text);
							$i++;
						}
					}	
				}
			
				if(isset($a['job_history'])){
					$get = str_replace('#employee_id', $employee_id, $a['job_history']['query']);
					$check = \DB::select($get);
					if($check){
						$r = (array) \DB::select($get)[0];
						$i=0;
						while($i < count($a['job_history']['field'])){
							$f = $a['job_history']['field'][$i];
							if(isset($r[$f])){
								$text = str_replace("#".$a['job_history']['binding'][$i],$r[$f] , $text);	
							}else{
								$text = str_replace("#".$a['job_history']['binding'][$i]," " , $text);
							}
							$i++;
						}	
					}else{
						$i=0;
						while($i < count($a['job_history']['field'])){
							$f = $a['job_history']['field'][$i];
								$text = str_replace("#".$a['job_history']['binding'][$i]," " , $text);
							$i++;
						}
					}
					
				}
				if(isset($a['manage_salary'])){
					$get = str_replace('#employee_id', $employee_id, $a['manage_salary']['query']);
					$check = \DB::select($get);
					if($check){
						$r = (array) \DB::select($get)[0];
						$i=0;
						while($i < count($a['manage_salary']['field'])){
							$f = $a['manage_salary']['field'][$i];
							if(isset($r[$f])){
								$text = str_replace("#".$a['manage_salary']['binding'][$i],$r[$f] , $text);	
							}else{
								$text = str_replace("#".$a['manage_salary']['binding'][$i]," " , $text);
							}
							$i++;
						}	
					}else{
						$i=0;
						while($i < count($a['manage_salary']['field'])){
							$f = $a['manage_salary']['field'][$i];
								$text = str_replace("#".$a['manage_salary']['binding'][$i]," " , $text);
							$i++;
						}
					}
				}
			return $text;
			} // END FUNCTION GENERATE

			private function parser_param($employee_id,$text){
				$arr = explode("#",$text);
				if($arr){
					foreach ($arr as $key) {

						if(strchr($key,'{{emp')){
							$first_bind = strchr($key,'{{');
							$bind = substr($first_bind,0,strlen(strstr($first_bind,'}}', true))+2);
							$d['employee']['binding'][] = substr($first_bind,0,strlen(strstr($first_bind,'}}', true))+2);
							
							$first_field = strchr($bind,'.');
							$d['employee']['field'][] = substr($first_field,1,strlen(strstr($first_field,'}}', true))-1);
							$d['employee']['query'] = $this->BuildQuery_Emp($d['employee']['field']);

						}
						elseif(strchr($key,'{{job_history')){
							$first_bind = strchr($key,'{{');
							$bind = substr($first_bind,0,strlen(strstr($first_bind,'}}', true))+2);
							$d['job_history']['binding'][] = substr($first_bind,0,strlen(strstr($first_bind,'}}', true))+2);
							
							$first_field = strchr($bind,'.');
							$d['job_history']['field'][] = substr($first_field,1,strlen(strstr($first_field,'}}', true))-1);
							$d['job_history']['query'] = $this->BuildQuery_Job($d['job_history']['field']);

						}elseif(strchr($key,'{{manage_salary')){
							$first_bind = strchr($key,'{{');
							$bind = substr($first_bind,0,strlen(strstr($first_bind,'}}', true))+2);
							$d['manage_salary']['binding'][] = substr($first_bind,0,strlen(strstr($first_bind,'}}', true))+2);
							
							$first_field = strchr($bind,'.');
							$d['manage_salary']['field'][] = substr($first_field,1,strlen(strstr($first_field,'}}', true))-1);
							$d['manage_salary']['query'] = $this->BuildQuery_Salary($d['manage_salary']['field']);

						}else{
							$d = '';
						}
					}	
				}else{ $d = ""; 
			}
				$generate = $this->Generate_Text($d,$text,$employee_id);
				return $generate;
			}// END PARSE PARAM

			private function HeaderFooter($text,$title,$margin,$size){
				//#wrapper { margin-left:'$margin[0]; margin-top:$margin[1]; margin-right:$margin[2]; margin-bottom:$margin[3]; padding:10px} 
				$date = "Date : ".date('d-M-Y');
				$image = storage_path()."/img.jpg";
				$jq = storage_path()."\jquery-1.10.2.js";
				$x_content = substr($size['x'],0,3) - 42;
				$start_html = "<html><head><title>'$title'</title>
							<style>	
									.page-break {
			    						page-break-after: always;
									}
									@page { margin:0; padding:0; }
									body { margin:0; padding:0; font-family:'Times New Roman', Georgia,serif; width:595px; height:842px;}
									p { letter-spacing :0.0420em; margin-left:0; margin-right:10pt; margin-top:1pt;} 
									#content p:first-letter{text-transform:capitalize; text-indent:50pt;}
									#header{padding:35px 0; height:130px; border:0px solid #000; padding-top:5px;}
									#sub-title{
											letter-spacing:1px; font-size:10px; font-weight:100; border-bottom:2px double #000;
										}
									#wrapper {position:relative; float:left; width:".$size['y']."; height:".$size['x']."; margin:0px 90px; padding:0px;} 
									#footer{
										text-align:center;
										padding-top:5px;
										font-size:10px; font-weight:bold;
										font-family:Arial,sans-serif;
										line:height:50%;
										letter-spacing:1.5px; border-top:solid 1px #333;
									}
									#content{ line-height:100%; height:".$x_content."px; border:0px solid #000; text-align:justify; margin-left:$margin[0]; margin-top:$margin[1]; margin-right:$margin[2]; margin-bottom:$margin[3]; padding:10px 0; font-family:'Times New Roman'; font-size:14px; } 
							</style>
							<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
							<script src='$jq'></script>
							</head><body><div id='wrapper'>";
				$end_html = "</div><script>var a = $('#content').height(); console.log(a);</script></body></html>";
				$header = "<div id='header'><center><img src='$image' width='158px' height='80px' style='margin-left:-15px; margin-top:30px; border-bottom:4px double;'/><br></center></div><div style='clear:both;'></div>";
				$footer = "<div style='clear:both;'></div><div id='footer'>Address : Build/Tower, Street/No | Phone : +(623)887-6617 | Fax : +(623)887-6571 | Email : info@leekie.com</div><div style='clear:both'><br></div><div class='page-break'></div>";
				$content = null;
				if(count($text) > 1){
					foreach ($text as $key) {
							$body = "<div id='content'>".$key."</div><div style='clear:both'><br></div>";
							$content .= $header."".$body."".$footer;
					}	
				}else{  
					$body = "<div id='content'>".$text."</div><div style='clear:both'><br></div>";
					$content .= $header."".$body."".$footer;
				}
				
				$contents = $start_html."".$content."".$end_html;
				return $contents;
			}// FUNC HEAD ANF FOOT


	}// END CLASS

	// NOT USE
	// private function BuildQuery_Emp($arr){
	// 		$z = "SELECT ";
	// 		$i = 1;
	// 		if(isset($arr)){
	// 			foreach ($arr as $key) {
	// 				if($i == count($arr)){
	// 					$z .= $key." FROM emp where employee_id = '#employee_id'";
	// 				}else{ $z .= $key." , ";}
	// 				$i++;
	// 			}
	// 		}else{
	// 			$z = null;
	// 		}
	// 		return $z;
	// 		} // END BUILD QUERY EMP
			
	// 		private function BuildQuery_Job($arr){
	// 			$z = "SELECT ";
	// 			$i = 1;
	// 			if($arr){
	// 				foreach ($arr as $key) {
	// 					if($i == count($arr)){
	// 						$z .= $key." FROM job_history WHERE employee_id = '#employee_id' ";
	// 					}else{ $z .= $key." , ";}
	// 					$i++;
	// 				}
	// 			}else{
	// 				$z = null;
	// 			}
	// 			return $z;
	// 		}// END BUILD QUERY JOB
			
	// 		private function BuildQuery_Salary($arr){
	// 			$z = "SELECT "; $i = 1;
	// 			if($arr){
	// 				foreach ($arr as $key) {
	// 					if($i == count($arr)){
	// 						$z .= $key." FROM manage_salary WHERE employee_id = '#employee_id' ";
	// 					}else{ $z .= $key." , ";}
	// 					$i++;
	// 				}
	// 			}else{ $z = null; }
	// 				return $z;
	// 		}// END BUILD QUERY SALARY
}