<?php namespace Larasite\Library{

use Larasite\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;
use Illuminate\Database\Eloquent\Model;
use \Datetime;
use \Dateinterval;
use \DatePeriod;

class calculation{
		public function calculation_attendance($arr){

			$value = $arr['value'];
			$fromx2 = $arr['fromx2'];
			$tox = $arr['tox'];
			$strtotime_formx = $arr['strtotime_formx'];
			$strtotime_tox = $arr['strtotime_tox'];
			$timeIn = $arr['timeIn'];
			$timeOuts = $arr['timeOuts'];
			$strtotime_TimeInInquire = $arr['strtotime_TimeInInquire'];
			$strtotime_TimeInBiometric = $arr["strtotime_TimeInBiometric"];
			$strtotime_TimeOutInquire = $arr["strtotime_TimeOutInquire"];
			$strtotime_TimeOutBiometric = $arr["strtotime_TimeOutBiometric"];
			$late_fix = $arr["late_fix"];
			$eot_fix2 = $arr["eot_fix2"];
			//$total_device_workhours = $arr["total_device_workhours"];

			$total_device_workhours = 0;

			$am_pm_sch_from 			= date("a",strtotime("$fromx2"));
			$am_pm_sch_to 				= date("a",strtotime("$tox"));

			if($value->date_inquire_absen){ // bio : (AM/PM, AM/AM, PM/PM) (NEXTDAY)
				
				if($am_pm_sch_from == 'pm' && $am_pm_sch_to == 'am'){ // SCH 23-12

					$strtotime_tox 		=  strtotime("+1 day",strtotime("$value->date $tox"));

					$am_pm_bio_in = date("a",strtotime($timeIn));
					$am_pm_bio_out = date("a",strtotime($timeOuts));

					if(($am_pm_bio_in == 'am' && $am_pm_bio_out == 'am') || ($am_pm_bio_in == 'pm' && $am_pm_bio_out == 'pm') || ($am_pm_bio_in == 'am' && $am_pm_bio_out == 'pm') ){

						$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInInquire;

						if($total_device_workhours > 0){ // benar in di nextday
							if($strtotime_TimeInInquire > $strtotime_formx){
								$mktimes_late_fix 	= mktime(0,($strtotime_TimeInInquire - $strtotime_formx)/60);
								$late_fix 			= date('H:i',$mktimes_late_fix);
							}else{ $late_fix=null; }
						}else{ // in di now
							$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInBiometric;

							if($strtotime_TimeInBiometric > $strtotime_formx){
								$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
								$late_fix 			= date('H:i',$mktimes_late_fix);
							}else{ $late_fix=null; }
						}

					}else if($am_pm_bio_in == 'pm' && $am_pm_bio_out == 'am'){

						$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInBiometric;

						if($strtotime_TimeInBiometric > $strtotime_formx){
							$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
							$late_fix 			= date('H:i',$mktimes_late_fix);
						}else{ $late_fix=null; }

					}

					if($strtotime_tox > $strtotime_TimeOutInquire){
						$mktimes_eot_fix2 	= mktime(0,($strtotime_tox - $strtotime_TimeOutInquire)/60);
						$eot_fix2 			= date('H:i',$mktimes_eot_fix2);
					}else{ $eot_fix2 = null; }
				
					
				}else if($am_pm_sch_from == 'am' && $am_pm_sch_to == 'pm'){ // SCH 01-23
					$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInBiometric;

					if($strtotime_TimeInBiometric > $strtotime_formx){
						$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
						$late_fix 			= date('H:i',$mktimes_late_fix);
					}else{ $late_fix=null; }

					if($strtotime_tox > $strtotime_TimeOutInquire){
						$mktimes_eot_fix2 	= mktime(0,($strtotime_tox - $strtotime_TimeOutInquire)/60);
						$eot_fix2 			= date('H:i',$mktimes_eot_fix2);
					}else{ $eot_fix2 = null; }

				}else{
					$am_pm_bio_in = date("a",strtotime($timeIn));
					$am_pm_bio_out = date("a",strtotime($timeOuts));
					if($am_pm_bio_in == 'pm' && $am_pm_bio_out == 'am'){					
						$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInBiometric;

						if($strtotime_TimeInBiometric > $strtotime_formx){
							$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
							$late_fix 			= date('H:i',$mktimes_late_fix);
						}else{ $late_fix=null; }

						if($strtotime_tox > $strtotime_TimeOutInquire){
							$mktimes_eot_fix2 	= mktime(0,($strtotime_tox - $strtotime_TimeOutInquire)/60);
							$eot_fix2 			= date('H:i',$mktimes_eot_fix2);
						}else{ $eot_fix2 = null; }
					}
				}

			}else{ // tanpa next day bio

				if($am_pm_sch_from == 'pm' && $am_pm_sch_to == 'am'){ // SCH 23-12
					$strtotime_tox 		=  strtotime("+1 day",strtotime("$value->date $tox"));

					$total_device_workhours 	= $strtotime_TimeOutBiometric - $strtotime_TimeInBiometric;
					
					if($strtotime_TimeInBiometric > $strtotime_formx){
						$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
						$late_fix 			= date('H:i',$mktimes_late_fix);
					}else{ $late_fix=null; }

					if($strtotime_tox > $strtotime_TimeOutBiometric){
						$mktimes_eot_fix2 	= mktime(0,($strtotime_tox - $strtotime_TimeOutBiometric)/60);
						$eot_fix2 			= date('H:i',$mktimes_eot_fix2);
					}else{ $eot_fix2 = null; }
				
				}else if(($am_pm_sch_from == 'am' && $am_pm_sch_to == 'pm') || ($am_pm_sch_from == 'pm' && $am_pm_sch_to == 'pm')){

					$total_device_workhours 	= $strtotime_TimeOutBiometric - $strtotime_TimeInBiometric;
					
					if($strtotime_TimeInBiometric > $strtotime_formx){
						$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
						$late_fix 			= date('H:i',$mktimes_late_fix);
					}else{ $late_fix=null; }

					if($strtotime_tox > $strtotime_TimeOutBiometric){
						$mktimes_eot_fix2 	= mktime(0,($strtotime_tox - $strtotime_TimeOutBiometric)/60);
						$eot_fix2 			= date('H:i',$mktimes_eot_fix2);
					}else{ $eot_fix2 = null; }
					
				}

			}

			$arr['value'] 						= $value;
			$arr['fromx2'] 						= $fromx2;
			$arr['tox'] 						= $tox;
			$arr['strtotime_formx'] 			= $strtotime_formx;
			$arr['strtotime_tox']				= $strtotime_tox;
			$arr['strtotime_TimeInInquire'] 	= $strtotime_TimeInInquire;
			$arr["strtotime_TimeInBiometric"] 	= $strtotime_TimeInBiometric;
			$arr["strtotime_TimeOutInquire"] 	= $strtotime_TimeOutInquire;
			$arr["strtotime_TimeOutBiometric"] 	= $strtotime_TimeOutBiometric;
			$arr["late_fix"] 					= $late_fix;
			$arr["eot_fix2"] 					= $eot_fix2;
			$arr["total_device_workhours"] 		= $total_device_workhours;
			return $arr;

		}//end function
		
		// fungsi untuk menampilkan data kehadiran
		public function att($types,$rangeDt,$job,$department,$localit,$paging = null){
			
			$training_temp = [];
			if($paging  == null){
				$paging_start =  0;
				$paging_end = 10;
			}else{
				$paging_start  = (($paging - 1) * 10) + 1;
				$paging_end = $paging * 10;
			}
			$jobs = "";
			$departments = "";
			$localitss = "";
			if($types == 'cut-off-record'){
		    	if(isset($job))
				{
					$jobs = $job;	
				}
				if(isset($department)){
					$departments = $department;
				}
				if(isset($localits)){
					$localitss = $localit;
				}
			}
			if($types == 'cut-off'){
		    	if(isset($job))
				{
					$jobs = "left join job_history t11 on  t11.employee_id = t2.employee_id ".$job;	
				}

				if(isset($department)){
					$departments = $department;
				}

				if(isset($localit)){
					$localitss = $localit;
				}
			}
			/*$querys = "SELECT distinct  t2.employee_id as employee_id, date_format(t1.date,'%Y-%m-%d') as date,
					concat(t2.first_name,' ',IFNULL(t2.middle_name,''),' ',IFNULL(t2.last_name,'')) as Name, 
					t2.first_name as first_name,
					t2.last_name as last_name,
					t3._from as fromx, 
					t3._to as tox,
					t3.shift_code as shift_code,
					COALESCE((select concat(date_format(attendance_work_shifts._from,'%H:%i'),'-',date_format(attendance_work_shifts._to,'%H:%i')) from attendance_work_shifts, att_change_shift where att_change_shift.employee_id = (t2.employee_id)  and att_change_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_change_shift.status_id = 2 and att_change_shift.new_shift = attendance_work_shifts.shift_id ORDER BY attendance_work_shifts.shift_id DESC limit 1 ),'99:99:99') as value,
					
					COALESCE((select att_change_shift.created_at from attendance_work_shifts, att_change_shift,att_schedule_request where att_change_shift.employee_id = (t2.employee_id) and att_schedule_request.request_id =  att_change_shift.id  and att_change_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 and att_change_shift.new_shift = attendance_work_shifts.shift_id ORDER BY att_change_shift.id DESC limit 1),'99:99:99') as date_changeShift,

					COALESCE((select att_training.start_ from att_schedule_request, att_training where att_training.employee_id = (t2.employee_id) and att_schedule_request.request_id =  att_training.id  and 
						att_training.start_ = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 ORDER BY att_training.id DESC limit 1),'99:99:99') as start_date_training,
					
					COALESCE((select att_training.end from att_schedule_request, att_training where att_training.employee_id = (t2.employee_id) and att_schedule_request.request_id =  att_training.id  and 
						att_training.start_ = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 ORDER BY att_training.id DESC limit 1),'99:99:99') as end_date_training,
					
					COALESCE((select att_training.updated_at from att_schedule_request, att_training where att_training.employee_id = (t2.employee_id) and att_schedule_request.request_id =  att_training.id  and 
						att_training.start_ = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 ORDER BY att_training.id DESC limit 1),'99:99:99') as approve_date_training,

					COALESCE((select att_swap_shift.created_at from att_schedule_request,att_swap_shift,attendance_work_shifts where att_swap_shift.employee_id = (t2.employee_id) and att_swap_shift.employee_id  = att_schedule_request.employee_id and att_swap_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 group by att_swap_shift.created_at ORDER BY att_swap_shift.swap_id DESC limit 1 ),'99:99:99') as date_swapShift,
					
					COALESCE((select concat(time_format(attendance_work_shifts._from,'%H:%i'),'-',time_format(attendance_work_shifts._to,'%H:%i')) from att_schedule_request,att_swap_shift,attendance_work_shifts where att_swap_shift.employee_id = (t2.employee_id) and att_swap_shift.employee_id  = att_schedule_request.employee_id and att_swap_shift.old_shift_id = attendance_work_shifts.shift_id  and att_swap_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 group by att_swap_shift.created_at ORDER BY attendance_work_shifts.shift_id DESC limit 1 ),'99:99:99') as schedule_swapShift,
				
						 date_format(t10.time_in,'%H:%i')as timeIn,date_format(t10.time_out,'%H:%i')as timeOut,
						 date_format(t10.date,'%Y-%m-%d') as date_absen, date_format(t10.inquire_date,'%Y-%m-%d') as date_inquire_absen,
						  t3.hour_per_day,t5.total_overtime,t5.total_overtime as total_overtimes,t6.total_overtime as OvertimeRestDay, t6.status_id as RestDay_App ,
			   	
						 t1.status,t10.input_ as ApproveStatus, t10.inquire_date as nextdays
						 FROM att_schedule t1
						 left join emp t2 on t2.employee_id=t1.employee_id $departments $localitss
						 $jobs
						 left join attendance_work_shifts t3 on t3.shift_id=t1.shift_id
						 left join biometrics_device t10 on t10.employee_id=t1.employee_id and t10.date=t1.date
						 left join att_overtime t5 on t5.employee_id=t1.employee_id and t5.date_str=t1.date and t5.status_id=2 and t3.shift_code !='DO'
						 left join att_overtime t6 on t6.employee_id=t1.employee_id and t6.date_str=t1.date and t6.status_id=2 and t3.shift_code ='DO'
						 left join biometrics_device t7 on t7.employee_id=t1.employee_id and t7.date=t1.date and t3._from !='00:00:00' and t3._to !='00:00:00' and date_format(str_to_date(time_to_sec(timediff(t7.time_out,t7.time_in))/60/60,'%l.%i'),'%i') !='00'
						 left join biometrics_device t8 on t8.employee_id=t1.employee_id and t3._from !='00:00:00'  and t8.time_in > t3._from and t8.date=t1.date 
						 left join biometrics_device t9 on t9.employee_id=t1.employee_id and t3._to !='00:00:00'and t9.time_out<t3._to  and t9.date=t1.date
						 where ";
		 	*/

			$querys = "SELECT distinct  t2.employee_id as employee_id, date_format(t1.date,'%Y-%m-%d') as date,
					concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name) as Name, 
					t2.first_name as first_name,
					t2.last_name as last_name,
					t3._from as fromx, 
					t3._to as tox,
					t3.shift_code as shift_code,
					t11.job,
					t2.department,
					t2.local_it,

					COALESCE((select concat(date_format(attendance_work_shifts._from,'%H:%i'),'-',date_format(attendance_work_shifts._to,'%H:%i')) from attendance_work_shifts, att_change_shift where att_change_shift.employee_id = (t2.employee_id)  and att_change_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_change_shift.status_id = 2 and att_change_shift.new_shift = attendance_work_shifts.shift_id ORDER BY attendance_work_shifts.shift_id DESC limit 1 ),'99:99:99') as value,
					
					COALESCE((select att_change_shift.created_at from attendance_work_shifts, att_change_shift,att_schedule_request where att_change_shift.employee_id = (t2.employee_id) and att_schedule_request.request_id =  att_change_shift.id  and att_change_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 and att_change_shift.new_shift = attendance_work_shifts.shift_id ORDER BY att_change_shift.id DESC limit 1),'99:99:99') as date_changeShift,

					COALESCE((select att_training.start_ from att_schedule_request, att_training where att_training.employee_id = (t2.employee_id) and att_schedule_request.request_id =  att_training.id  and 
						att_training.start_ = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 ORDER BY att_training.id DESC limit 1),'99:99:99') as start_date_training,
					
					COALESCE((select att_training.end from att_schedule_request, att_training where att_training.employee_id = (t2.employee_id) and att_schedule_request.request_id =  att_training.id  and 
						att_training.start_ = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 ORDER BY att_training.id DESC limit 1),'99:99:99') as end_date_training,
					
					COALESCE((select att_training.updated_at from att_schedule_request, att_training where att_training.employee_id = (t2.employee_id) and att_schedule_request.request_id =  att_training.id  and 
						att_training.start_ = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 ORDER BY att_training.id DESC limit 1),'99:99:99') as approve_date_training,

					COALESCE((select att_swap_shift.created_at from att_schedule_request,att_swap_shift,attendance_work_shifts where att_swap_shift.employee_id = (t2.employee_id) and att_swap_shift.employee_id  = att_schedule_request.employee_id and att_swap_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 group by att_swap_shift.created_at ORDER BY att_swap_shift.swap_id DESC limit 1 ),'99:99:99') as date_swapShift,
					
					COALESCE((select concat(time_format(attendance_work_shifts._from,'%H:%i'),'-',time_format(attendance_work_shifts._to,'%H:%i')) from att_schedule_request,att_swap_shift,attendance_work_shifts where att_swap_shift.employee_id = (t2.employee_id) and att_swap_shift.employee_id  = att_schedule_request.employee_id and att_swap_shift.old_shift_id = attendance_work_shifts.shift_id  and att_swap_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 group by att_swap_shift.created_at ORDER BY attendance_work_shifts.shift_id DESC limit 1 ),'99:99:99') as schedule_swapShift,
				
						 date_format(t10.time_in,'%H:%i')as timeIn,date_format(t10.time_out,'%H:%i')as timeOut,
						 date_format(t10.date,'%Y-%m-%d') as date_absen, date_format(t10.inquire_date,'%Y-%m-%d') as date_inquire_absen,
						  t3.hour_per_day,t5.total_overtime,t5.total_overtime as total_overtimes,t6.total_overtime as OvertimeRestDay, t6.status_id as RestDay_App ,
			   	
						 t1.status,t10.input_ as ApproveStatus, t10.inquire_date as nextdays
						 FROM att_schedule t1
						 left join emp t2 on t2.employee_id=t1.employee_id $departments $localitss
						 left join job_history t11 on  t11.employee_id = t2.employee_id	$jobs
						 left join attendance_work_shifts t3 on t3.shift_id=t1.shift_id
						 left join biometrics_device t10 on t10.employee_id=t1.employee_id and t10.date=t1.date
						 left join att_overtime t5 on t5.employee_id=t1.employee_id and t5.date_str=t1.date and t5.status_id=2 and t3.shift_code !='DO'
						 left join att_overtime t6 on t6.employee_id=t1.employee_id and t6.date_str=t1.date and t6.status_id=2 and t3.shift_code ='DO'
						 left join biometrics_device t7 on t7.employee_id=t1.employee_id and t7.date=t1.date and t3._from !='00:00:00' and t3._to !='00:00:00' and date_format(str_to_date(time_to_sec(timediff(t7.time_out,t7.time_in))/60/60,'%l.%i'),'%i') !='00'
						 left join biometrics_device t8 on t8.employee_id=t1.employee_id and t3._from !='00:00:00'  and t8.time_in > t3._from and t8.date=t1.date 
						 left join biometrics_device t9 on t9.employee_id=t1.employee_id and t3._to !='00:00:00'and t9.time_out<t3._to  and t9.date=t1.date
						 where ";
		 	
		 	if($types == 'cut-off'){
		 		$querys .= " t1.date in ($rangeDt) ";
		 		//$querys .= " t1.date in ('2017-03-30','2017-03-31') ";

				if(isset($job))
				{
					$querys .= $job;	
				}

		 		if(isset($localit)){
					$querys .= $localit;
				}


				if(isset($department)){
					$querys .= $department;
				}
		        $querys .=  " and t1.status = 2 group by t1.date,t2.employee_id ASC";
		 		/*$querys .= " t1.date in ($rangeDt) and t1.status = 2 and  t2.local_it in ($localit) and t1.status = 2 ";

				if($job  != null){
		            $querys .= " and t40.job = $job ";    
		        }

		        if($department != null){
		          $querys .=  " and t2.department = $department ";
		        }

		        $querys .=  " group by t1.date,t2.employee_id ASC";*/
		    }else if($types == 'cut-off-record'){
		  		if(isset($rangeDt)){
					$querys .= $rangeDt;
				}
		    	if(isset($job))
				{
					$querys .= $job;	
				}

				if(isset($department)){
					$querys .= $department;
				}

				if(isset($localit)){
					$querys .= $localit;
				}
				$querys .=  " and t1.status = 2 group by t1.date,t2.employee_id ASC";
		 	}else if($types == "view-attendance"){
		 		$querys .= "t1.employee_id='$rangeDt' and t1.status = 2 group by t1.date order by t1.date ASC";
		 	}else if($types == "view-attendance-perday"){
		 		$emp = $job;
		 		$querys .= "t1.employee_id='$emp' and t1.date = '$rangeDt' and t1.status = 2 group by t1.date order by t1.date DESC limit 1";
		 	}else{
		 		$querys .=  " group by t1.date,t2.employee_id ASC";
		 	}
		 	//return $querys;
			$query = \DB::SELECT($querys);
			//return var_dump($query);
			$sch_date = [];
			if($query !=  null){

				$query2 = [];
				foreach ($query as $key => $value) {
					if($value->employee_id && $value->Name){
						array_push($query2,$value);
					}
				}
				$query = $query2;
				foreach ($query as $key => $value) {

					$date_changeShift  =  '';
					if($value->date_changeShift  !=   '99:99:99'){
							$employee_q  =  $value->employee_id;
							$date_changeS =  $value->date_changeShift;
							$db =  \DB::SELECT("select asr.id ,concat(substring(aws._from,1,5),'-',substring(aws._to,1,5)) as data from att_schedule_request as asr ,att_change_shift as acs, 	attendance_work_shifts as aws  
							 	where asr.update_at = '$date_changeS' 
								and asr.request_id = acs.id 
							 	and acs.new_shift = aws.shift_id
							 	and asr.employee_id  = '$employee_q'
							 	and  asr.type_id = 5 
							 	and asr.status_id =  2 order by asr.id DESC");

							if($db != null){
						   		$date_changeShift = $db[0]->data;
							}else{
								$date_changeShift = '99:99:99';
							}

					}else{
						$date_changeShift  =  '99:99:99';
					}

					$query[$key]->schedule_changeShift =   $date_changeShift;

					/** schedule  **/

					$schedule  = '';
					if($query[$key]->schedule_changeShift !=  '99:99:99'  and  $query[$key]->date_changeShift != '99:99:99' ){ 
						if($query[$key]->tox == "00:00:00"){
							$schedule   =  $query[$key]->schedule_changeShift;
						}else{
							$schedule  =  substr($value->fromx,0,5).'-'.substr($value->tox,0,5); 
						}
						
						// if($value->date == "2018-06-02"){
						// 	return "a";
						// }
						$exp_sch = explode("-",$schedule);
						$query[$key]->tox = $tox 	= $exp_sch[1].":00";
						$query[$key]->fromx = $fromx	= $exp_sch[0].":00";
						$query[$key]->allow_changeShift = true;
					}
					else if($value->date_swapShift != '99:99:99' and  $value->schedule_swapShift  !=  '99:99:99' ){ 
						if($query[$key]->tox == "00:00:00"){
							$schedule  =  $value->schedule_swapShift; 
						}else{
							$schedule  =  substr($value->fromx,0,5).'-'.substr($value->tox,0,5); 
						}

						// if($value->date == "2018-06-02"){
						// 	return ["b",$schedule];
						// }
						$exp_sch = explode("-",$schedule);
						$query[$key]->tox = $tox 	= $exp_sch[1].":00";
						$query[$key]->fromx = $fromx	= $exp_sch[0].":00";
						$query[$key]->allow_swapShift = true;
					}else{ 
						$schedule  =  substr($value->fromx,0,5).'-'.substr($value->tox,0,5); 
						// if($value->date == "2018-06-02"){
						// 	return "c";
						// }
						if(isset($value->$schedule)){
							if($value->$schedule != "99:99:99" && $value->$schedule == $schedule){
								$exp_sch = explode("-",$schedule);
								$query[$key]->tox = $tox 	= $exp_sch[1].":00";
								$query[$key]->fromx = $fromx	= $exp_sch[0].":00";
							}else{
								$exp_sch = explode("-",$value->$schedule);
								$query[$key]->tox = $tox 	= $exp_sch[1].":00";
								$query[$key]->fromx = $fromx	= $exp_sch[0].":00";
							}
						}else{
							$exp_sch = explode("-",$schedule);
							$query[$key]->tox =  $tox 	= $exp_sch[1].":00";
							$query[$key]->fromx = $fromx	= $exp_sch[0].":00";
						}
					}

					if($value->start_date_training != "99:99:99" || $value->shift_code == 'T'){
						$a = strtotime($query[$key]->date_swapShift);
						$allow = true;
						if($query[$key]->date_changeShift != '99:99:99'){
							$b = strtotime($query[$key]->date_changeShift);
							if($a < $b){ $allow = false; }
						}elseif($query[$key]->date_swapShift != '99:99:99'){
							$b = strtotime($query[$key]->date_swapShift);
							if($a < $b){ $allow = false; }
						}
						if($value->shift_code == 'T'){
							$training_temp[] = $value->date;
							$allow = false;
						}					

						if($allow){					
							$begin = new DateTime($value->start_date_training);
							$end = new DateTime($value->end_date_training);
							$end = $end->modify( '+1 day' ); 

							$interval = new DateInterval('P1D');
							$daterange = new DatePeriod($begin, $interval ,$end);

							foreach($daterange as $date){	
								$training_temp[] = $date->format("Y-m-d");
							}
						}
						
					}

					
					$query[$key]->schedule =  $schedule;
					
					$query[$key]->schedule =  $schedule;
					if(in_array($value->shift_code, ['BL','T','TB','OB'])){
						$value->timeIn 	= '-';
						$value->timeOut = '-';
					}
					
					if(count($training_temp) > 0){
						$a =  array_search($value->date, $training_temp);
						if(gettype($a) == 'integer'){
							$query[$key]->training_req =  $training_temp[$a];
						}else{
							$query[$key]->training_req = false;
						}
					}else{
						$query[$key]->training_req = false;
					}
					// get data and time
					$sch_date[$key] = [
						'date' => $value->date,
						'time' => [
							'in' => $fromx,
							'out'=> $tox,
							'timeIn'=>$value->timeIn,
							'timeOut'=>$value->timeOut
						]
					];
					/** workhour  **/
					$workhours  = null;
					
					$timeIn =  $value->timeIn;
					//$fromx  =   $value->fromx;
					// $Eout = '';

					// $tox =  $value->tox;
					// $fromx  =  $value->fromx;
					$total_menit_workhours = null;
					$short_fix = null;
					$late_fix=null;
					$earlyOut_fix=null;
					$eot_fix2 = null;
					$overT_fix=null;
					if($value->timeIn != '-' and  $value->timeOut !=  '-' || $value->timeIn != null and  $value->timeOut !=  null){
					
						$timeIn  					.= ':00';		
						$fromx2    					= $fromx;
						$timeOuts 					= $value->timeOut;
						$timeOuts 					.= ":00";
						$date_inquire_absen 		= $value->date_inquire_absen;
						$dates 						= $value->date;
						$dates_Biometric 			= $value->date_absen;
						$strtotime_TimeInInquire 	= strtotime("$value->date_inquire_absen $timeIn");
						$strtotime_TimeInBiometric 	= strtotime("$value->date_absen $timeIn");

						$strtotime_formx 			= strtotime("$value->date $fromx2");
						$strtotime_tox 				= strtotime("$value->date $tox");

						$strtotime_TimeOutBiometric = strtotime("$value->date_absen $timeOuts");
						$strtotime_TimeOutInquire 	= strtotime("$value->date_inquire_absen $timeOuts");

						// ######################################################################################
						$am_pm_form 				= date("a",$strtotime_formx);
						$am_pm_to 					= date("a",$strtotime_tox);
						
						if($value->date_inquire_absen){

							// $am_pm_deviceFrom 				= date("a",$strtotime_TimeInInquire);
							// $am_pm_deviceTo					= date("a",$strtotime_TimeOutBiometric);
							$am_pm_deviceFrom 				= date("a",$strtotime_TimeInBiometric);
							$am_pm_deviceTo					= date("a",$strtotime_TimeOutInquire);
							
						}else{

							$am_pm_deviceFrom 				= date("a",$strtotime_TimeInBiometric);
							$am_pm_deviceTo					= date("a",$strtotime_TimeOutBiometric);

						}

						$am_pm_schFrom 				= date("a",$strtotime_formx);
						$am_pm_schTo				= date("a",$strtotime_tox);
						$am_pm_sch_from 			= date("a",strtotime("$fromx2"));
						$am_pm_sch_to 				= date("a",strtotime("$tox"));

						$splitOut_time 		= explode(":", $timeOuts);
						$splitOut_time_h 	= (integer)$splitOut_time[0];
						$splitOut_time_m 	= (integer)$splitOut_time[1];

						// CARI WORKHOURS DEVICE
						// if($am_pm_form == 'pm' && $am_pm_to == "am"){
						// 	$strtotime_tox 	= strtotime('+1 day' ,strtotime("$value->date $tox"));
						// }
						

						$LIB_CAL_ATT = $this->calculation_attendance([
							'value'							=> $value,
							'fromx2'						=> $fromx2,
							'tox'							=> $tox,
							'strtotime_formx'				=> $strtotime_formx,
							'strtotime_tox'					=> $strtotime_tox,
							'timeIn' 						=> $timeIn,
							'timeOuts' 						=> $timeOuts,
							'strtotime_TimeInInquire' 		=> $strtotime_TimeInInquire,
							"strtotime_TimeInBiometric" 	=> $strtotime_TimeInBiometric,
							"strtotime_TimeOutInquire" 		=> $strtotime_TimeOutInquire,
							"strtotime_TimeOutBiometric" 	=> $strtotime_TimeOutBiometric,
							"late_fix" 						=> $late_fix,
							"eot_fix2" 						=> $eot_fix2
							//"total_device_workhours" 		=> $total_device_workhours
						]);

						
						$value 							= $LIB_CAL_ATT['value'];
						$fromx2 						= $LIB_CAL_ATT['fromx2'];
						$tox 							= $LIB_CAL_ATT['tox'];
						$strtotime_formx 				= $LIB_CAL_ATT['strtotime_formx'];
						$strtotime_tox 					= $LIB_CAL_ATT['strtotime_tox'];
						$strtotime_TimeInInquire 		= $LIB_CAL_ATT['strtotime_TimeInInquire'];
						$strtotime_TimeInBiometric 		= $LIB_CAL_ATT["strtotime_TimeInBiometric"];
						$strtotime_TimeOutInquire 		= $LIB_CAL_ATT["strtotime_TimeOutInquire"];
						$strtotime_TimeOutBiometric 	= $LIB_CAL_ATT["strtotime_TimeOutBiometric"];
						$late_fix 						= $LIB_CAL_ATT["late_fix"];
						$eot_fix2 						= $LIB_CAL_ATT["eot_fix2"];
						$total_device_workhours 		= $LIB_CAL_ATT["total_device_workhours"];

						/*if($value->date == '2018-06-19' && $value->employee_id == "2015037"){
							return [$total_device_workhours];
						}*/
						$total_schedule_workhours 	= $strtotime_tox - $strtotime_formx;

						// schedule work
						$total_schedule_minutes 	= $total_schedule_workhours / 60;
						$split_schedule_hours 		= floor($total_schedule_minutes / 60);
						$split_schedule_minutes 	= ($total_schedule_minutes % 60);
						// device work
						$total_device_minutes 		= $total_device_workhours / 60;
						$split_device_minutes 		= ($total_device_minutes % 60);
						$split_device_hours 		= floor($total_device_minutes / 60);

						if($value->date_inquire_absen){
							$mktime_do 	= mktime(0,($strtotime_TimeOutBiometric-$strtotime_TimeInInquire)/60);
							$time_do 	= date("H:i",$mktime_do);
						}else{
							$mktime_do 	= mktime(0,($strtotime_TimeOutBiometric-$strtotime_TimeInBiometric)/60);
							$time_do 	= date("H:i",$mktime_do);
						}
						
						//if($value->date == "2018-08-01"){return [($total_device_workhours-$total_schedule_workhours)/60,$late_fix]; }
						
						
						// OVERTIME
						$tmp_m = 0;$tmp_h = 0;
						
						if($split_schedule_hours < $split_device_hours){

							if($split_schedule_minutes < $split_device_minutes){
								$tmp_m = $split_device_minutes - $split_schedule_minutes;
							}else{
								$tmp_m = $split_schedule_minutes - $split_device_minutes;
							}
							$tmp_h = $split_device_hours - $split_schedule_hours;
							if($tmp_m < 10){ $tmp_m = "0$tmp_m"; }
							if($tmp_h < 10){ $tmp_h = "0$tmp_h"; }
							
							$query[$key]->total_overtime = "$tmp_h:$tmp_m";
							if("$tmp_h:$tmp_m" == "00:00"){
								$query[$key]->total_overtime = "-";
							}
						}else{
							if($split_schedule_minutes < $split_device_minutes){ $tmp_m = $split_device_minutes - $split_schedule_minutes; }
							else{ $tmp_m = $split_schedule_minutes - $split_device_minutes; }
							if($tmp_m < 10){ $tmp_m = "0$tmp_m"; }
							if($tmp_h < 10){ $tmp_h = "0$tmp_h"; }
							
							$query[$key]->total_overtime = "$tmp_h:$tmp_m";
							if("$tmp_h:$tmp_m" == "00:00"){
								$query[$key]->total_overtime = "-";
							}
						}


						$time_work	= null;
						if($split_device_hours < 10){ $time_work = "0$split_device_hours:"; }
						else{ $time_work = "$split_device_hours:"; }

						if($split_device_minutes < 10){ $time_work .= "0$split_device_minutes"; }
						else{ $time_work .= "$split_device_minutes"; }

						

						// CARI TIMEOUT DEVICE FIX INQUERY
						$timeOut_device_fix 	= strtotime("+".$split_schedule_hours." hour",$strtotime_TimeInBiometric);


						/*
						* CARI EARLYOUT
						* earlyout not working training and dayoff
						*/
						if($timeOut_device_fix > $strtotime_TimeOutBiometric){
							$mktimes_earlyOut_fix 	= mktime(0,($timeOut_device_fix - $strtotime_TimeOutBiometric)/60);
							$earlyOut_fix 			= date("H:i",$mktimes_earlyOut_fix);
						}else{ $earlyOut_fix 	= null; }


						/* CARI SHORT
						*  short not working dayoff and training 
						*/
						if($total_device_workhours < $total_schedule_workhours){
							$mktimes_short_fix = mktime(0,($total_schedule_workhours - $total_device_workhours)/60);
							$short_fix = date('H:i',$mktimes_short_fix);
						}else{ $short_fix = null; }

						// OVERTIME FIX
						if($total_schedule_workhours < $total_device_workhours){
							$mktimes_overtime_fix = mktime(0,($total_device_workhours - $total_schedule_workhours)/60);
							$total_overtime_fix = date("H:i",$mktimes_overtime_fix);
						}else{ $total_overtime_fix = null; }
						$query[$key]->total_overtime = $total_overtime_fix;
						
						// $query[$key]['late_fix'] = $late_fix;
						// $query[$key]['short_fix'] = $short_fix;
						// $query[$key]['earlyout_fix'] = $earlyout_fix;
						// #######################################################################################
						

						$exp_strtotime_TimeInInquire 	= explode(":", $strtotime_TimeInInquire);
						$exp_strtotime_TimeInBiometric 	= explode(":", $strtotime_TimeInBiometric);
						$exp_strtotime_formx 			= explode(":", $strtotime_formx);
						$exp_strtotime_TimeOutBiometric = explode(":", $strtotime_TimeOutBiometric);
						$exp_timein 					= explode(":",$timeIn);
						$late_time 						= null;
						
						$mktimes_workhours_device 	= mktime(0,($total_device_workhours/60));
						// if($value->date == "2018-05-10"){
						// 	return $total_device_workhours;
						// }
						$workhours 					= /*date('H:i',$mktimes_workhours_device)*/ $time_work;
						
						//$workhours .=  ':00';

						if(strtotime($workhours) > strtotime(date('H:i',(strtotime($tox) -  strtotime($fromx)))) )
						{ $Eout =  0; }
						else
						{ 
							$timeOut  = $value->timeOut;
							$timeOut  = $timeOut.':00';

							$time1 =  substr(($query[$key]->schedule),6,5).':00';

							if(strtotime($time1) < strtotime($value->timeOut)){
								$Eout =  0;	
							}else{
								$Eout = date('H:i', strtotime($time1) - strtotime($value->timeOut));
							}

						}
					
					}else{ // Not Found Activity Device

						$Eout = '-';
						$workhours  =  null;
					}


					$query[$key]->WorkHours =  $workhours;
					if($query[$key]->shift_code == 'DO' && isset($timeIn) && isset($timeOuts)){
						
						if(isset($time_do)){
							$workhours = $time_do;
						}
						
						$query[$key]->WorkHours = $workhours;
						$query[$key]->OvertimeRestDay = $workhours;
					}
					

					$query[$key]->Short  =  $short_fix;

					//schedule mod
					// $query[$key]->schedule_mod =  concat((substring((select schedule),1,5)),':00');	
					$query[$key]->schedule_mod = substr($query[$key]->schedule,0,5).':00';
					//late; 
				
					// $late  =  0;
					if($value->date_inquire_absen){
						$jam_schedule = explode(":",$value->fromx);
						$jam_masuk_device = explode(":",$value->timeIn);
						
						$am_pm = date("a",strtotime($value->timeIn));

						if($am_pm == 'am'){
							$countings = strtotime($value->date_inquire_absen." ".$value->timeIn)-strtotime($value->date." ".$query[$key]->schedule_mod);
						}else{
							// if($value->date == "2018-05-07"){
							// 	return "disini";
							// }
							if(strtotime($value->date_absen." ".$value->timeIn) <= strtotime($value->date." ".$query[$key]->schedule_mod)){
								$late = null;
								$countings = null;
							}else{
								$countings = strtotime($value->date_absen." ".$value->timeIn)-strtotime($value->date." ".$query[$key]->schedule_mod);
							}
						}

						
						if(!$countings){
							$late = null;
						}else{
							$late = date('H:i',$countings); 
						}
						
					}else{				
						if(strtotime($query[$key]->schedule_mod) < strtotime($value->timeIn) && $value->timeOut != null){
							// if($value->date == "2018-05-07"){
							// 	return "2";
							// }
							$late = date('H:i',(strtotime($value->timeIn) -  strtotime($query[$key]->schedule_mod)));
						}else
							{ $late  = null;  }
					}
					$query[$key]->Late  =  $late_fix;
					//$query[$key]->Late  =  $late;

					//earlyOut  
					//$query[$key]->EarlyOut = $Eout;	
					$query[$key]->EarlyOut = $eot_fix2;	
					
					
				}
			}

			if(count($query) > 0){
				
				$c = count($query);
				$mod = $c % 10;
				$count = $c - $mod;
				// if($mod <= 10){
				// 	return $data = ['data'=>$query, 'status'=>500, 'message'=>'Record data is less than 1 month'];
				// }
				$j = 0 ;
				$last = $c - 20;

				$h_formx 	= "";
				$h_tox 		= "";
				// TESTING
				//if($value->date ==   '2017-12-25'){
					//return [$query[$key]];
				//}
				if(isset($query[$j]->fromx) && isset($query[$j]->tox)){										
					$h_formx = substr($query[$j]->fromx, 0,5);
					$h_tox = substr($query[$j]->tox, 0,5);
				}

				for ($i = 0; $i < $c; ) { // $c = 57 / $i until 56
					// $dt = substr($dt, 8);
					// first period
					try {

						// if($query[$i]->date == "2017-02-01" && $query[$i]->employee_id == "2016054"){
						// 	return [$query[$i]];
						// }
						if ($i <= 9){
							if(isset($query[$j+9]->date)){
								$between = $query[$j]->date.' <=> '.$query[$j+9]->date;
								$between = $between;
							}
						}
						// second period
						else if ($i >= 10 && $i <= 19){
							if(isset($query[$j+(19-$j)]->date)){
								$between2 = $query[$j+(10-$j)]->date.' <=> '.$query[$j+(19-$j)]->date;

								$between = $between2;
							}
						}
						// third period
						else{
							if(isset($query[$j+(20-$j)]->date)){							
								$date = date("d");

								$between3 = $query[$j+(20-$j)]->date.' <=> '.$query[$j+(($c-1)-$j)]->date;
								$between = $between3;
							}
						}


						if ($i >= $count-1){ // if $i = 50 then $i - $c-1
							if(isset($query[$c-1]->date)){							
								$between = $query[$i]->date.'<==>'.$query[$c-1]->date;
								$a = $mod;
							}
						}
						else{
							if(isset($query[$i+9]->date)){
								$between = $query[$i]->date.'<==>'.$query[$i+9]->date;
								$a = 10;
							}
						}
						for ($j=1; $j <= $a; $j++) {
							// create a mark, yes for workHour < hour_per_day, and no for other
							if ($query[$i]->WorkHours != null){
								// if($query[]->date  == '2017-09-02'){
								// 	return $arr  = [$query[$i]->WorkHours,$query[$i]->hour_per_day];
								// }	
								if ($query[$i]->WorkHours < $query[$i]->hour_per_day) {

									$emp_over  =  $query[$i]->employee_id;
									$date_str =  $query[$i]->date;
		 
							
									$check_overtime = \DB::SELECT("select * from att_overtime as ao,att_schedule_request as asr where ao.employee_id =  '$emp_over' and ao.date_str  = '$date_str' and ao.id  = asr.request_id and asr.status_id  = 2  ");

									
									if($check_overtime != null){
										$workStatus = "no";
									}else{
										$workStatus = "yes";
									} 
								}else{
									$emp_over  =  $query[$i]->employee_id;
									$date_str =  $query[$i]->date;
									$check_overtime = \DB::SELECT("select * from att_overtime as ao,att_schedule_request as asr where ao.employee_id =  '$emp_over' and ao.date_str  = '$date_str' and ao.id  = asr.request_id and asr.status_id  = 2  ");

									
									if($check_overtime != null){
										$workStatus = "no";
									}else{
										$workStatus = "yes";
									} 
								}  
							}
							else{
								$workStatus = "no";
							}
							/*
							//	Change the view to DO if schedule is 00:00-00:00
							//	Change the view to "-" if schedule is null
							*/
							
							if ($query[$i]->schedule == "00:00-00:00") {
								$date_modify = $query[$i]->date;
								$emp_modify = $query[$i]->employee_id;

								$give =\DB::SELECT("select aws.shift_code from att_schedule ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify' and ash.date = '$date_modify' and  ash.shift_id = aws.shift_id");
								//$check_workshift = \DB::SELECT("select * from att_schedule where ")
								
								if($query[$i]->date_changeShift != "99:99:99" and $query[$i]->status ==  2 && count($give) > 0){
									
									$give   = \DB::SELECT("select ash.id, aws.shift_code from att_change_shift ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify' and ash.date = '$date_modify' and ash.status_id = 2 order by ash.id DESC");  
									// if(count($give) == 0){
									// 	return ["select ash.id, aws.shift_code from att_change_shift ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify' and ash.date = '$date_modify' and ash.status_id = 2 order by ash.id DESC"];
									// }
									if(count($give) > 0){
										$schedule = $give[0]->shift_code;
									}
								}else if(count($give) > 0){
									// if(count($give) == 0){
									// 	return ["select ash.id, aws.shift_code from att_change_shift ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify' and ash.date = '$date_modify' and ash.status_id = 2 order by ash.id DESC",
									// 	"select aws.shift_code from att_schedule ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify' and ash.date = '$date_modify' and  ash.shift_id = aws.shift_id"];
									// }
									$schedule = $give[0]->shift_code;
								}

								if ($query[$i]->timeIn == null AND $query[$i]->timeOut == null) {
									$timeIn = "-";
									$timeOut = "-";
									$noTimeInStatus = "yes";
									$noTimeInStatus = "yes";
								}
								else if ($query[$i]->timeIn == null AND $query[$i]->timeOut != null) {
									$timeIn = "-";
									$timeOut = $query[$i]->timeOut;
									$noTimeInStatus = "yes";
									$noTimeInStatus = "no";
								}
								else if ($query[$i]->timeIn != null AND $query[$i]->timeOut != null) {
									$timeIn = $query[$i]->timeIn;
									$timeOut = $query[$i]->timeOut;
									$noTimeInStatus = "no";
									$noTimeInStatus = "no";
								}
								else {
									$timeIn = $query[$i]->timeIn;
									$timeOut = "-";
									$noTimeInStatus = "no";
									$noTimeInStatus = "yes";
								}
							}
							else {
								$schedule = $query[$i]->schedule;
								if ($query[$i]->timeIn == null) {
									$timeIn = "-";
									$noTimeInStatus = "yes";
								}
								else {
									$timeIn = $query[$i]->timeIn;
									$noTimeInStatus = "no";
								}

								if ($query[$i]->timeOut == null) {
									$timeOut = "-";
									$noTimeOutStatus = "yes";
								}
								else {
									$timeOut = $query[$i]->timeOut;
									$noTimeOutStatus = "no";
								}
							}
							
							if ($query[$i]->total_overtime == null) {
								$timeIns = strtotime($query[$i]->timeOut);
								$explode = explode('-',$query[$i]->schedule);
								$arg2    = strtotime($explode[1]);
								if($timeIns > $arg2){
									if($schedule == 'DO'){
										$totalOver = '-';
										
										//ini masalahnya   
										$mass = "yess";
									}else{
										if($query[$i]->timeOut == "00:00"){
											$query[$i]->timeOut = "24:00";
										}
										$time1x  =   explode(':',$query[$i]->timeOut);
										$time2x =  explode(':',$explode[1]);

										try {
										  //code causing exception to be thrown
											$time1xr  =   ($time1x[0] * 60) + $time1x[1];
											$time2xr  =   ($time2x[0] * 60) + $time2x[1];
										} catch(\Exception $e) {
											return 2276;
										  //exception handling
										}
										
										if(strlen(($time1xr - $time2xr) % 60) ==  1){
											$m	 = '0'.(($time1xr - $time2xr) % 60);
										}else{
											$m   = (($time1xr - $time2xr) % 60);
										}
										$totalOver =  /*floor(($time1xr - $time2xr) / 60).' : '.$m*/ $query[$i]->total_overtime;
									
									}

								$makeOver =  'yes';
								}else{
									$makeOver = 'no';
									$totalOver = "-";
								}
							}else {

								$floor   = floor($query[$i]->total_overtime);
								$decimal = $query[$i]->total_overtime - $floor;
								if($decimal != 0){
									$decimal = (60 * (10 * $decimal))/10;
								}else{
									$decimal = "00";
								}
								$ax = strlen($floor);

								$ax = ($ax == 1 ? '0'.$floor : $floor);
								$totalOver = /*$floor.':'.$decimal*/ $query[$i]->total_overtime;
								$makeOver  ='no';
							}


							if ($query[$i]->WorkHours == '00:00:00' || $query[$i]->WorkHours == null){
								 $workHour = "-"; $totalOver = "-"; 

							}
							else{

								if(strchr($query[$i]->WorkHours,'-')){
									
									$exp_schedule = explode('-', $query[$i]->schedule);
									$ts1 = strtotime($exp_schedule[0]);
									$ts2 = strtotime($exp_schedule[1]);
									$diff = abs($ts1 - $ts2) / 3600;
									if(strpos($diff,'.')){
										$e_menit = explode('.', "$diff");
										$tmp = "0.".$e_menit[1]; // build menit
										$b_menit = round(floatval($tmp) * 60 );
										if(strlen("$b_menit") == 2){
											$workHour =  $e_menit[0].":$b_menit";
										}else{
											$workHour =  $e_menit[0].":0$b_menit";
										}
									}else{
										$t = $diff/60;
										if(strchr("$t",".")){
											$diff = $diff/2;
										}
										$workHour = round($diff).":00";
									}
								}else{						
									 $workHour = $query[$i]->WorkHours;
									 $explode = explode(":",$workHour);
									 $hi = $explode[0];
									 if(substr($hi,0,1) == 0){
									 	$workHour =  substr($hi,1,1).':'.$explode[1];
									 }else{
									 	$workHour =  $hi.':'.$explode[1];
									 }
								}
							}		

							

							if($totalOver != "-" and $schedule == 'yes'){
								$timeIns = strtotime($query[$i]->timeOut);
								$explode = explode('-',$query[$i]->schedule);
								$arg2    = strtotime($explode[1]);
								$overRest = date('H:i',($timeIns - $arg2));
							}else if($query[$i]->timeIn and $query[$i]->timeOut){
								

								//$overRest = 
							}else{
								$overRest = "-";
							}

							// if($totalOver ==  "-"&& $overRest !="-"){
							// 	$totalOver = "-";
							// }

							if ($query[$i]->OvertimeRestDay == NULL){ 
								if(isset($mass) && $mass == "yess"){
									if($schedule == "DO"){
										$timeInss = strtotime($query[$i]->timeOut);
										$timeOuts = strtotime($query[$i]->timeIn);
										$overRest = date('H:i',($timeInss - $timeOuts));
										
										if($timeIns == false && $timeOuts == false){
											$overRest = "-";
										}else{
											$workHour = $overRest;
										}
									}
								}else{
									$overRest = "-";	
								}
								
							}else{
								$overRest = $query[$i]->OvertimeRestDay;
								$explode =  explode('.',$query[$i]->OvertimeRestDay);
								$floor =  floor($overRest);
								$check_floor = $overRest -  $floor;
								if($check_floor != 0){
									$overRest_decimal = (60 * (10 * $decimal))/10;
									$overRest = $explode[0].':'.$overRest_decimal;
								}else{
									$overRest = ((int)$explode[0]+1).':'.'00'; 
								}		
							}
							
							// if($query[]->date  == '2017-09-02'){
								// 	return $arr  = [$query[$i]->WorkHours,$query[$i]->hour_per_day];
								// }
							if ($query[$i]->Short == null) {$short = "-";}
							else{
								$time1 = intval(substr($query[$i]->Short,0,2));
								$time2 = intval(substr($query[$i]->Short,3,2));
								$short = ($time1 * 60) + $time2;
								if($short == 0){
									$short = '-';
								}else{
									$short =  $short;
								}

							}
							

							if ($query[$i]->ApproveStatus == null || $query[$i]->ApproveStatus == 0 ){ $status = "no"; }
							elseif ($query[$i]->ApproveStatus == 1) {$status = "yes";}
							else{ $status = "no";}

							/*
							*	COLOR_ATT_LATE	########################################################################################
							*/
							
							if ($query[$i]->Late == null || $query[$i]->Late == "0"){ 
								//if($query[$i]->date == "2018-05-01"){ return (array)($query[$i]->Late == 0); }
								$late = "-"; 
								$colorLate = 'black';
							}else{
									
								$date_late 	= $query[$i]->date;
								//$chkLate  	= \DB::SELECT("select * from att_schedule_request where employee_id  = '$name' and availment_date = '$date_late' and type_id  = 7 and status_id  = 2 order by id desc  limit 1");
								//$chkLate 	= \DB::SELECT("select asr.date_request, asr.availment_date, asr.approver, asr.status_id, al.late from att_schedule_request as asr, att_late as al where asr.employee_id  = '$name' and asr.availment_date = '$date_late' and asr.type_id  = 7 and asr.status_id  = 2 and al.late_id = asr.request_id order by asr.id desc  limit 1");
								$name = $query[$i]->employee_id;
								$date_late = $query[$i]->date;
								$chkLate 	= \DB::SELECT("select asr.date_request, asr.availment_date, asr.approver, asr.status_id, al.late from att_schedule_request as asr, att_late as al where asr.employee_id  = '$name' and asr.availment_date = '$date_late' and asr.type_id  = 7 and al.late_id = asr.request_id order by asr.id desc  limit 1");

								if(count($chkLate) <  1){
									$colorLate  = 'violet';
								}elseif(isset($chkLate[0])){
				
									if(/*$chkLate[0]->approver ==  null*/ $chkLate[0]->status_id == 1){
										$colorLate  = 'orange';
									}elseif($chkLate[0]->status_id == 2){
										
										$split_time_sch  	= explode(":",$query[$i]->fromx);
										$split_late			= explode(":",$chkLate[0]->late);

										$split_late_h = (integer)$split_late[0];
										$split_late_m = (integer)$split_late[1];
										$split_time_sch_h = (integer)$split_time_sch[0];
										$split_time_sch_m = (integer)$split_time_sch[1];

										if($split_late_h == 0){
											$tmp = $split_time_sch_m + $split_late_m;
											if($tmp > 60){
												$split_time_sch_h = $split_late_h + 1;
												$split_time_sch_m = $tmp-60;
											}else{
												$split_time_sch_m = $tmp;
											}
										}else{
											$tmp_h = $split_time_sch_h + $split_late_h;
											if($tmp_h >= 24){
												$split_time_sch_h = $tmp_h - 24;
											}else{
												$split_time_sch_h = $tmp_h;
											}
											
											$tmp_m = $split_time_sch_m + $split_late_m;
											if($tmp_m > 60){
												$split_time_sch_h = $split_late_h + 1;
												$split_time_sch_m = $tmp_m-60;
											}else{
												$split_time_sch_m = $tmp_m;
											}
										}

										if($split_time_sch_h < 10){
											$build = "0$split_time_sch_h:$split_time_sch_m:00";
										}else{
											$build = "$split_time_sch_h:$split_time_sch_m:00";
										}
										$date_com 		= $query[$i]->date;
										$time_device 	= $query[$i]->timeIn;

										$com_late = strtotime("$date_com $build");
										$com_device = strtotime("$date_com $time_device:00");

										if($com_device <= $com_late && $chkLate[0]->status_id == 2){
											$colorLate  	= 'green';
										}else{
											if($chkLate[0]->status_id == 1){
												$colorLate  	= 'orange';
											}else{
												$colorLate  	= 'violet';
											}
										}
										
									}elseif($chkLate[0]->status_id == 4) {
										$colorLate  = 'violet';
									}elseif($chkLate[0]->status_id == 3) {
										$colorLate  = 'red';
									}
								}else{
									$colorLate  = 'violet';
								}
									//if($value->date == "2018-06-29"){ return $query[$i]->Late; }
								  		$late = $query[$i]->Late;
								 // 		return $late;
									// }
							}


							if ($query[$i]->EarlyOut == null){ $earlyOut = "-";}
							else{ 

								// $a1 = new \DateTime("17:00:00.000");
								// $a2 = new \DateTime("18:55:00.000");
								// $b 	= date_diff($a2,$a1); 
								// $str =$b->format('%H:%I');

								
								$explode = explode(":",$query[$i]->EarlyOut);
								 	$earlyOut = strlen($explode[0]);
								 	if($earlyOut == 2 and $earlyOut != '00'){
								 		$ho = (int)$explode[0];
								 		$mi = (int)$explode[1];
								 		$ho = $ho * 60;
								 		$earlyOut = $ho + $mi ;
								 	}else if($earlyOut == 2 and $earlyOut == '00'){
								 		$mi = (int)$explode[1];
								 		$earlyOut =   $mi;
								 	}else{
								 		$earlyOut = "-";
								 	}
								if($earlyOut != '-'){
									//return var_dump($query[$i]);
								}
							}
							$h = substr($schedule, 6, strpos($schedule,":"));
							$m = substr($schedule, 9, strpos($schedule,10));
							//print_r($h."\n");
							$h1 = substr($timeOut, 0, strpos($timeOut,":"));
							$m1 = substr($timeOut, 3, strpos($timeOut,4));
							// create mark for the record that has been changed, yes for changed record
							if ($totalOver != null){
								if($makeOver == 'yes'){
									$overStatus = ( $overRest == "-" ?  "yes" : "no");
									if(isset($mass) && $mass == "yess"){
										$overStatus = "yes";
									}
									// $overStatus = "yes";
								}elseif($makeOver == "no") {
									$overStatus = "no";
								}
							}
							else
								$overStatus = "no";
								if($overRest !=  null && isset($query[$i]->RestDay_App)){
									if($query[$i]->RestDay_App == 2){
										$OTR_color =  'yes';	
									}else{
										$OTR_color =  'no';
									}
									
								}else{
									$OTR_color = "-";
								}


							// Compare time in and time out between attendance record and request
							$emp = $query[$i]->employee_id;
							$dt = $query[$i]->date;
							$_timeIN = $query[$i]->timeIn.':00';
							$_timeOUT = $query[$i]->timeOut.':00';

							// create mark for the record that has been changed, yes for changed record
							$tm = \DB::select("CALL View_TimeInOut_byDate_employee('$emp', '$dt')");
							$test[] = $dt;
							if ($tm != null) {
								if ($tm[0]->req_in != $_timeIN)
									$timeInStatus = "yes";
								else
									$timeInStatus = "no";

								if ($tm[0]->req_out != $_timeOUT)
									$timeOutStatus = "yes";
								else
									$timeOutStatus = "no";
							}
							else {
								$timeInStatus = "no";
								$timeOutStatus = "no";
							}
							if ($i == $c) {

								$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
								//return $data;
							}else{
								if($totalOver != '-'){
								
									if($workHour != "0:00"){
										// if($query[$i]->date  ==   '2017-08-22'){
										// 		return $totalOver;
										// }

										$totalOvers =  $totalOver;
									}else{
										
										$totalOvers = '-';
									}

									// if($totalOvers ==  '00:00' || $workHour == "0:00"){
									// 	$totalOvers = '-';
									// }
								}else{
									$totalOvers = '-';
								}

								if($overRest != "-" && $overRest != null && $totalOver != null && $totalOver != '-'){
									// if($query[$i]->date == "2018-02-26"){
									// 		return [$late,$overRest];
									// 	}
									$short = '-';
									$late = '-';
									$earlyOut = '-';
								}
								/*if($query[$i]->date=="2018-08-09"){
									return [$earlyOut,12,$query[$i],$overRest,$totalOver];
								}*/
								if(strlen($schedule) ==  2 && $query[$i]->fromx == "00:00:00"){
									$short = '-';
									$late = '-';
									$earlyOut = '-';
									$totalOvers = '-';
									$overRest = '-';

								}

								$empx = $query[$i]->employee_id;
								$date_request = $query[$i]->date;
								$latest_modified  =  \DB::SELECT("select from_,to_,leave_code from leave_request,leave_type where employee_id  = '$empx' and leave_request.leave_type = leave_type.id and leave_code not in('','ADO') and leave_request.status_id = 2");
								$latest_request  =  \DB::SELECT("select * from att_schedule_request as asr, att_training as atr where asr.status_id = 2 and asr.employee_id  = '$empx' and asr.request_id = atr.id and '$date_request' >= atr.start_ and '$date_request' <= atr.end and asr.type_id = 1");
								$latest_request1  =  \DB::SELECT("select * from att_schedule_request as asr, att_training  as atr where asr.status_id = 2 and asr.employee_id  = '$empx' and asr.type_id = 2 and asr.request_id  = atr.id and '$date_request' >= atr.start_ and '$date_request' <= atr.end limit 1");
								$terminated = \DB::SELECT("select * from emp_termination_reason where employee_id  = '$empx' ");
								//if($latest_request != null){
								//$continue_training  = \Db::SELECT("select * from ")
								
									if(isset($latest_request[0]->type_id)  ){
										$schedule =   'T';
									}
									if(isset($latest_request1[0]->type_id)  ){
										$schedule =   'OB';	
									}
								//}
								// if($query[$i]->date=="2018-08-07"){
								// 	return [$schedule,$latest_modified,"select from_,to_,leave_code from leave_request,leave_type where employee_id  = '$empx' and leave_request.leave_type = leave_type.id and leave_code not in('','ADO') and leave_request.status_id = 2 "];
								// }
								if($latest_modified != null){
									foreach ($latest_modified as $key => $value) {
										if($value->from_ <= $query[$i]->date && $query[$i]->date <= $value->to_){
											$schedule =   $value->leave_code;
										}	
									}
								}
								
								if($query[$i]->shift_code == "DO"){
									/*if(in_array($schedule, ['VL','BL','SL','EL','BE','ML','PL','MA'])){
										$schedule = "DO";
									}*/
									if(!in_array($schedule, ['OC','ADO'])){
										/*$schedule = "DO";*/
										$schedule = "DO";
									}
								}

								// HOLIDAY-CASE
								$query[$i]->check_holiday = null;
								$holiday = \DB::SELECT("select * from attendance_holiday");
									foreach ($holiday as $key => $value) {
										$holiday_date  =  $query[$i]->date;

										foreach ($sch_date as $keys => $values) {
											if($values['date'] == $holiday_date){
												$h_formx = $values['time']['in'];
												$h_tox = $values['time']['out'];
											}
										}
										if($holiday != null){

											if($holiday[$key]->repeat_annually == 1){
												if($holiday[$key]->day == null && $holiday[$key]->week == null){
													if($holiday[$key]->date == $holiday_date ){

														if($holiday[$key]->type == 1){
															// $schedule = "(H) $h_formx-$h_tox";
															 $query[$i]->check_holiday = "(SH)";
														}else{
															$query[$i]->check_holiday = "(RH)";
														}
													}
												}
												if($holiday[$key]->day != null && $holiday[$key]->week != null){
													
													$holiday[$key];
													$year =  date('Y');
													$ass_hole =  explode('-',$holiday[$key]->date);
													$month_name  =  date("F", mktime(0, 0, 0,(intval($ass_hole[1])), 10));
													$ass_day =  $holiday[$key]->day;

													if($holiday[$key]->week == 'first'){
														$week_day =  '+1';	
													}elseif($holiday[$key]->week == 'second'){
														$week_day =  '+2';
													}elseif ( $holiday[$key]->week == 'third'){
														$week_day = '+3';
													}elseif($holiday[$key]->week == 'fourth'){
														$week_day = '+4';
													}else{
													  $week_day = '+4';
													}
													$get_new_ass_hole  = date('Y-m-d',strtotime("$week_day week $ass_day $month_name $year"));
													
													if($holiday_date == $get_new_ass_hole){
												 		if($holiday[$key]->type == 1){
															// $schedule = "(H) $h_formx-$h_tox";
															 $query[$i]->check_holiday = "(SH)";
														}else{
															$query[$i]->check_holiday = "(RH)";
														}
														
												 	}
												}		
											}else{
												if($holiday[$key]->date == $query[$i]->date){
													if($holiday[$key]->type == 1){
														// $schedule = "(H) $h_formx-$h_tox";
														 $query[$i]->check_holiday = "(SH)";
													}else{
														$query[$i]->check_holiday = "(RH)";
													}
												}
											}
										}
									}
								

								if($terminated != null){
									$schedule = 'TER';
								}

								/* BACKUP 25092018 04:52 PM
								*if(!isset($colorLate)){
								*	$colorLate = 'red';
								}*/

								if($schedule  == 'DO'){
									$ti =  strtotime($timeIn.':00');
									$to =  strtotime($timeOut.':00');

									$overRest  = date('H:i',$to - $ti);
									if($overRest == '00:00'){
										$overRest  =   '-';
									}
								}else{
									$overRest = '-';
								}

								if($workStatus ==  'no'and  $schedule == 'DO'){
									$OTR_color = 'yes';							
								}else{
									$OTR_color = 'no';
								}
								$date_c = $query[$i]->date;
								$emp_c  = $query[$i]->employee_id;

								/*
								* COLOR_ATT_EARLYOUT #####################################################################################################
								*/

								/* BACKUP 25092018 06:12 PM
								*$query[$i]->early_c =  'black';
								*/
								//$query[$i]->early_c =  'violet';
								//if($query[$i]->date == "2018-09-27"){ return [$earlyOut]; }
								if($earlyOut  != null){
									$emp_c  = $query[$i]->employee_id;
									$date_c = $query[$i]->date;  
									
									/* BACKUP 25092018 06:13 PM
									* select * from  att_late, att_schedule_request where  att_late.type_id = 8 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id and att_schedule_request.approver = 2
									*/
									$early_c =  \DB::SELECT("select * from  att_late, att_schedule_request where  att_late.type_id = 8 and att_schedule_request.availment_date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id order by att_schedule_request.id DESC");

									if(count($early_c) > 0){
										$arr_early = [];
										//if($query[$i]->date=="2018-09-27"){return [$earlyOut,$total_early];}
										for ($idx1=0; $idx1 < count($early_c); $idx1++) { 
											
											$splits = explode(":",$early_c[$idx1]->early_out);
											$h = (integer)$splits[0] * 60;
											$total_early = $h+(integer)$splits[1];
											
											// if($query[$i]->date == "2018-10-01"){
											// 	return [$early_c,$earlyOut <= $total_early,$earlyOut,$total_early,"TEST",$splits];
											// }	
											if($earlyOut <= $total_early && $early_c[$idx1]->status_id == 2){
												$arr_early[$idx1] =  'green';
											}elseif($earlyOut <= $total_early && $early_c[$idx1]->status_id == 1){
												$arr_early[$idx1] =  'orange';	
											}elseif($earlyOut <= $total_early && $early_c[$idx1]->status_id == 4){
												$arr_early[$idx1] =  'violet';
											}elseif($earlyOut <= $total_early && $early_c[$idx1]->status_id == 3){
												$arr_early[$idx1] =  'red';
											}	
										}
										// if($earlyOut <= $total_early && $early_c[0]->status_id == 2){
										// 	$query[$i]->early_c =  'green';
										// }elseif($earlyOut <= $total_early && $early_c[0]->status_id == 1){
										// 	$query[$i]->early_c =  'orange';	
										// }elseif($earlyOut <= $total_early && $early_c[0]->status_id == 4){
										// 	$query[$i]->early_c =  'violet';
										// }elseif($earlyOut <= $total_early && $early_c[0]->status_id == 3){
										// 	$query[$i]->early_c =  'red';
										// }
										/*if($query[$i]->date == "2018-08-30"){
											return [$early_c,$query[$i]->early_c,$earlyOut <= $total_early,$earlyOut,$total_early,"TEST"];
										}*/
										if(in_array('orange', $arr_early)){
											$query[$i]->early_c = 'orange';
										}else{
											if(count($arr_early) > 0){
												$query[$i]->early_c = $arr_early[0];
											}else{
												$query[$i]->early_c =  'violet';		
											}
										}
									}else{
										$arr_early =  ['violet'];
										$query[$i]->early_c =  'violet';
									}
								}else{
									//$arr_early =  ['violet'];
									$query[$i]->early_c =  'violet';
								}


								

								if(strlen($schedule) >  4){
									$turns =  explode('-',$schedule);
									if(!isset($turns[1])){
									  $workStatus  =  "no";
									}else{
									$base_date1 =  $turns[0].':00';
									$base_date2 =  $turns[1].':00';

									$substract_1 =  $timeIn.':00';
									$substract_2 =  $timeOut.':00'; 

								 	$get_tot_date1   = strtotime($base_date2) -  strtotime($base_date1);
									$get_tot_date2   = strtotime($substract_2) - strtotime($substract_1); 
									
										if($get_tot_date1 <= $get_tot_date2  || $get_tot_date1 == $get_tot_date2){
											$workStatus  =  "no";
										}
									}
								}	

								//check color  time  in   out 
							 	// par timeInStatusC  and timeOutStatusC
								
								//if($query[$i]->date  ===  '2017-09-10'){
									if($timeIn   ==   '-'){
										$date_timeIn   = $query[$i]->date;
										$date_timeIn_employee  =  $query[$i]->employee_id;
									
										$check_time_orange  = \DB::SELECT("select t2.status_id, t1.req_in from  att_time_in_out_req as t1,att_schedule_request 
														   as t2,pool_request as t3 
														   where t1.date  =   '$date_timeIn'
														   and   t1.req_in_out_id =  t2.request_id and  t1.type_id = t2.type_id
														   and   t2.id = t3.id_req  and  t2.type_id   =  t3.type_id and t3.master_type = 1
														   and  t1.employee_id = '$date_timeIn_employee' order  by  t1.req_in_out_id DESC limit 1 ");

										if($check_time_orange  != null){
											///return  $check_time_orange;
											if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  ==  1){
												
													$timeInStatusC = 'orange';
													$timeIn  =   substr($check_time_orange[0]->req_in,0,5); 
											}else{
												$timeInStatusC = 'green';
											}
										}else{
											$timeInStatusC = 'red';
										}
										//if($check)
									}else{ $timeInStatusC = 'green'; }
								//}

								if($timeOut   ==   '-'){
									$date_timeOut   = $query[$i]->date;
									$date_timeOut_employee  =  $query[$i]->employee_id;
									$check_time_orange  = \DB::SELECT("select t2.status_id,t1.req_out from  att_time_in_out_req as t1,att_schedule_request 
																		as t2,pool_request as t3 
																		where t1.date  =   '$date_timeOut'
																		and   t1.req_in_out_id =  t2.request_id and  t1.type_id = t2.type_id
																		and   t2.id = t3.id_req  and  t2.type_id   =  t3.type_id and t3.master_type = 1
																		and  t1.employee_id = '$date_timeOut_employee' order  by  t1.req_in_out_id DESC limit 1 ");

									if($check_time_orange  != null){
										if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  == 1){
												$timeOutStatusC = 'orange';
												$timeOut  =   substr($check_time_orange[0]->req_out,0,5); 
										}else{
											$timeOutStatusC = 'green';
										}
									}else{
										$timeOutStatusC = 'red';
									}
									//if($check)
								}else{ $timeOutStatusC = 'green'; }

								//check_color_orange 

								//check colcor late
								/*if($colorLate == 'red'){
									if($query[$i]->date ==  '2017-03-05'){
								
										$date_late = $query[$i]->date;
										$nameX  =  $query[$i]->employee_id;
										$chkLate  = \DB::SELECT("select * from att_schedule_request where employee_id  = '$nameX' and availment_date = '$date_late' and type_id  = 7 and  status_id  = 1 order by id desc  limit 1");

										if($chkLate  !=  null){
											//if($chkLate[0]->approver  ==   null){
												$colorLate  = 'orange';
											//}
										}
									}
								}*/

								/*
								* COLOR_ATT_EARLYOUT 
								*/
								// if($query[$i]->early_c ==  'red'){
								// 	/*
								// 	* BACKUP 25092018 05:20 PM
								// 	*/
								// 	$early_c =  \DB::SELECT("select att_late.status_id from  att_late, att_schedule_request where  att_late.type_id = 8 and att_schedule_request.availment_date  = '$date_c' and 	att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id ORDER BY att_schedule_request.id DESC LIMIT 1");

								// 	if(count($early_c) > 0){
								// 		if($early_c[0]->status_id == 1){ $query[$i]->early_c =  'orange'; }
								// 		elseif($early_c[0]->status_id == 2){ $query[$i]->early_c =  'green'; }
								// 		elseif($early_c[0]->status_id == 3){ $query[$i]->early_c =  'violet'; }
								// 		elseif($early_c[0]->status_id == 4){ $query[$i]->early_c =  'red'; }

								// 	}
								// 	// if(count($early_c) > 0 && $early_c[0]->status_id == 1){
								// 	// 	$query[$i]->early_c =  'orange';
								// 	// }elseif(count($early_c) > 0 && $early_c[0]->status_id == 2){
								// 	// 	$query[$i]->early_c =  'green';
								// 	// }elseif(count($early_c) > 0 && $early_c[0]->status_id == 3){
								// 	// 	$query[$i]->early_c =  'red';
								// 	// }
								// }else{
								// 	/*
								// 	* BACKUP 25092018 05:20 PM
								// 	*/
								// 	$early_c =  \DB::SELECT("select att_late.status_id from  att_late, att_schedule_request where  att_late.type_id = 8 and att_schedule_request.availment_date  = '$date_c' and 	att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id ORDER BY att_schedule_request.id DESC LIMIT 1");

								// 	if(count($early_c) > 0){
								// 		if($early_c[0]->status_id == 1){ $query[$i]->early_c =  'orange'; }
								// 		elseif($early_c[0]->status_id == 2){ $query[$i]->early_c =  'green'; }
								// 		elseif($early_c[0]->status_id == 3){ $query[$i]->early_c =  'violet'; }
								// 		elseif($early_c[0]->status_id == 4){ $query[$i]->early_c =  'red'; }

								// 	}
								// }
								// if($query[$i]->date == "2018-05-13"){
								// 	return [$earlyOut,$total_early,$early_c,$query[$i]->early_c]; 
								// }

								//set color to  oramg  for  overtime and overtime dso
								//if  already input for  overtime  rest day parameter $OTR_color
								//If  already  input for  overtime   no  do    parameter $overtime_non_do


								//return $totalOvers;
								/*
								* COLOR_ATT_OVERTIME_NON_DO
								*/

								if($totalOvers != null){
									// if($query[$i]->date == "2018-09-02"){
									// 	return $totalOvers;
									// }
									$less_1h = substr($totalOvers,1,1);
									if((integer)$less_1h >= 1){
										$date_overtime  =  $query[$i]->date;
										$employee_overtime = $query[$i]->employee_id ;

										$search  = \DB::SELECT("select * from att_overtime as t1, att_schedule_request as t2
																where t1.employee_id  =  '$employee_overtime' 
																and  t1.date_str =  '$date_overtime' 
																and  t1.type_id =  t2.type_id  
																and  t1.id = t2.request_id
																order by t1.id desc limit 1 
																");	
										/*if($query[$i]->date == "2018-09-10"){
										*	return [$search,"overtime"];
										}*/
										/*
										* BACKUP 25092018 05:25 PM
										*/
										if($search  != null){
											if($search[0]->status_id == 1){
												$overtime_non_do  =  'orange';
											}elseif($search[0]->status_id == 2){
												$overtime_non_do  =  'green';
											}elseif($search[0]->status_id == 4){
												$overtime_non_do  =  'violet';
											}elseif($search[0]->status_id == 3){
												$overtime_non_do  =  'red';
											}


										}else{
											/*
											* BACKUP 25092018 05:25 PM
											* $overtime_non_do =   'red';
											*/	
											$overtime_non_do =   'violet';
										}
									}else{
										$overtime_non_do  =  'grey';
									}


								}else{
										/*
										* BACKUP 25092018 05:25 PM
										* $overtime_non_do =   'red';
										*/
									$overtime_non_do = 'grey';
								}

								/*
								* COLOR_ATT_OVERTIME_REST
								*/
								if($overRest != null){
									$date_overtime  =  $query[$i]->date;
									$employee_overtime = $query[$i]->employee_id ;

									if($schedule == 'DO'){									
										$search  = \DB::SELECT("select * from att_overtime as t1, att_schedule_request as t2
																where t1.employee_id  =  '$employee_overtime' 
																and  t1.date_str =  '$date_overtime' 
																and  t1.type_id =  t2.type_id  
																and  t1.id = t2.request_id
																order by t1.id desc limit 1 
																");	
										if($search  != null){
											if($search[0]->status_id == 1){
												$OTR_color  =  'orange';
											}else{
												$OTR_color  =  'green';
											}


										}else{
												$OTR_color =   'violet';
										}
									}else{
										$OTR_color  = 'violet';
									}
									/*if($query[$i]->date ==  '2018-09-29'){
										return [$overRest,$schedule,$OTR_color];
									}*/
								}else{
									$OTR_color  = 'violet';
								}

								//check  changeshift
								if(isset($query[$i]->date) && isset($query[$i]->allow_changeShift)){
									// if($query[$i]->date ==  '2017-09-14'){
									// $date_changeshift  =  $query[$i]->date;
									// $employee_changeshift = $query[$i]->employee_id ;

									// return $check_exist_change_shift   =  \DB::SELECT("select * from  att_change_shift as t1 ,  att_schedule_request  as t2,
									// 											attendance_work_shifts as  t3
									// 										    where t1.employee_id = '$employee_changeshift' and  date = '$date_changeshift'
									// 										    and  t1.type_id  =  t2.type_id 
									// 										    and  t1.id  = t2.request_id  
									// 										    and  t1.new_shift  =  t3.shift_id
									// 										    and t2.status_id  =  2 order  by  t1.id  desc limit 1");

									// return   $check_exist_change_shift;

									// }
									$date_changeshift  =  $query[$i]->date;
									$employee_changeshift = $query[$i]->employee_id ;

									
									$check_exist_change_shift   =  \DB::SELECT("select * from  att_change_shift as t1 ,  att_schedule_request  as t2,
																				attendance_work_shifts as  t3
																			    where t1.employee_id = '$employee_changeshift' and  date = '$date_changeshift'
																			    and  t1.type_id  =  t2.type_id 
																			    and  t1.id  = t2.request_id  
																			    and  t1.new_shift  =  t3.shift_id
																			    and t2.status_id  =  2 order  by  t1.id  desc limit 1");
									if($check_exist_change_shift != null){
										$list_code = ['DO','ADO','VL','BL','TB','SL','EL','BE','ML','PL','MA','OB'];

									

										if(in_array($check_exist_change_shift[0]->shift_code,$list_code)){
											$schedule =  $check_exist_change_shift[0]->shift_code;
											$late = '-';
											$earlyOut = '-';
											$short = '-';
										}else{
											$schedule  =  substr($check_exist_change_shift[0]->_from,0,5).'-'. substr($check_exist_change_shift[0]->_to,0,5);
										}
									}

								}
								//if($query[$i]->date == "2018-08-12"){ return [$late]; }
								//return [$query[$i]];
								try{
									if($query[$i]->training_req){
										if($query[$i]->shift_code == "T"){
											$schedule = 'T';
										}else if($query[$i]->shift_code == "OB"){
											$schedule = "OB";
										}
										$late = '-';
									}

									if($query[$i]->OvertimeRestDay != '99:99:99'){
										$overRest = $query[$i]->OvertimeRestDay;
									}
									if(strlen($query[$i]->total_overtimes) > 2){
										$totalOvers = $query[$i]->total_overtimes;
										if(substr($totalOvers,1,1) == ":"){
											$totalOvers = "0".$query[$i]->total_overtimes;
										}
									}
									
								}catch(\Exception $e){
									return [$e->getMessage()];
								}
								// if($query[$i]->date=="2018-09-14"){
								// 		return [$query[$i],$schedule];
								// }
								if(strlen($schedule) == 2 && $query[$i]->fromx == "00:00:00"){
									$late = '-';
									$earlyOut = '-';
									$short = '-';
								}
								// if($query[$i]->date=="2018-08-09"){
								// 	return [$earlyOut,12,$overRest];
								// }

								//if($late == 0 || $late == "0" || $late == "00:00"){
									try {
										//if($query[$i]->date == "2018-08-12"){ return [$late,strchr($late,":")]; }
										if(strchr($late,":")){
											$late_split	= explode(":",$late);
											$late = (intval($late_split[0]) * 60) + intval($late_split[1]);
										}else{
											$late = "-";
										}
										//if($query[$i]->date == "2018-08-12"){ return [$late,strchr($late,":")]; }
									} catch (\Exception $e) {
										
									}
									//if($query[$i]->date == "2018-08-12"){ return [$late,$late == "00:00",$late == "0", $late == 0]; }
									//$late = "-";
								/*}else{

								}*/
								/*if($late == "08:00"){
									return $query[$i];
								}*/
								//if($short == 0 || $short == "0" || $short == "00:00"){
									try {
										if(strchr($short,":")){
											$short_split	= explode(":",$short);
											$short = (intval($short_split[0]) * 60) + intval($short_split[1]);
										}else{
											$short = "-";		
										}
									} catch (\Exception $e) {
										
									}
									//if($query[$i]->date == "2018-08-12"){ return [$short,$short == "00:00",$short == "0", $short == 0]; }
									//$short = "-";
								//}
								/*if($query[$i]->employee_id == "2011006" && $query[$i]->date == "2018-09-27"){
									return $query[$i];
								}*/

								$temp[] = [
									'check_holiday'=>$query[$i]->check_holiday,
									'early_c' => $query[$i]->early_c,   
									'date' => $query[$i]->date,
									'nextday'=>$query[$i]->nextdays,
									'colorLate' => $colorLate,
									'employee_id' => $query[$i]->employee_id,
									'Name' => $query[$i]->Name,
									'first_name' => $query[$i]->first_name,
									'last_name' => $query[$i]->last_name,
									'schedule' => $schedule,
									'schedule_shift_code' => $query[$i]->shift_code,
									'timeIn' => $timeIn,
									'timeOut' => $timeOut,
									'noTimeIn' => $noTimeInStatus,
									'noTimeOut' => "yes",//$noTimeOutStatus,
									'WorkHours' => /*$workHour*/ ($workHour == 0 ? $workHour = '-' : $workHour = $workHour  ),
									'workStatus' => $workStatus,
									'total_overtime' => /*$totalOvers*/ ($totalOvers == 0 ? $totalOvers = '-' : $totalOvers = $totalOvers  ),
									'new_color_overtime' => $overtime_non_do,
									'overStatus' => $overStatus,
									'status' => $status,
									'OvertimeRestDay' => /*$overRest*/ ($overRest == 0 ? $overRest = '-' : $overRest = $overRest  ),
									'OvertimeRestDay_Color' => $OTR_color,
									'timeOutStatus' => $timeOutStatus,
									'timeInStatus' => $timeInStatus,
									'C_TimeIn' => $timeInStatusC,
									'C_TimeOut' => $timeOutStatusC,
									'Short' => $short /*($short == 0 ? $short = '-' : $short = $short  )*/,
									'Late' => $late /*($late == 0 ? $late = '-' : $late = $late  )*/,
									'EarlyOut' =>  ($earlyOut == 0 ? $earlyOut = '-' : $earlyOut = $earlyOut  ),
									'between' => $between
								];


							
								$i++;
							}
						}
					
					} catch (\Exception $e) {
						return ["message"=>$e->getMessage(),"line"=>$e->getLine(),"filename"=>$e->getFile(),1853,$i,$query[$i]];	
					}
				}


				// $count_temp  =  count($queryx);

				// if($count_temp  > 9){
				// 	$mod  =  $count_temp   % 10;
				// 	if($mod  != 0){
				// 		$get_paging  =  ($count_temp -  $mod) / 10;
				// 		$get_paging  += 1;   
				// 	}else{
				// 		$get_paging  =  $count_temp  / 10;
				// 	} 'paging' => $get_paging
				// }

				
				$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
			}
			else{
				$data = ['data'=>null, 'status'=>500, 'message'=>'Record data is empty'];
			}
			return $data;
		}

		public function action_button_behavior($value,$arrs,$idx,$stats){	
			$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
			if(isset($arrs['hrx_comp'])){ 
				$arr_idx = array_search($value,$arrs['hrx_comp']);
				if(gettype($arr_idx) == 'integer'){
					$subs = 'hr'; 
				}
			}if(isset($arrs['supx_comp'])){ 
				$arr_idx = array_search($value,$arrs['supx_comp']);
				if(gettype($arr_idx) == 'integer'){
					$subs = 'sup'; 
				}
			}if(isset($arrs['swapx_compx'])){ 
				$arr_idx = array_search($value,$arrs['swapx_compx']);
				if(gettype($arr_idx) == 'integer')
				$subs = 'swap'; 
			}

			if(isset($subs)){
				$i2=-1;		
				for ($i1=0; $i1 < count($arrs[$subs]); $i1++) {
					$keysx=array_keys($arrs[$subs][$i1]);
					
					if((string)$keysx[0]==$idx){
						//return $keysx;
						$i2=$i1;
					}else{
						$act = 'not';
					}
				}
				//return [$arrs[$subs][$i2]];			
				//for ($i=0; $i < count($arrs[$subs]); $i++) { 
					//if(isset($arrs[$subs][$i][$value])){
						// if($idx == 264){
						// 	return $arrs[$subs][$i];
						// }
					//return $arrs[$subs];
				//return $i2;
				if($i2==-1){
					//return 121;
					if(isset($arrs['req_flow']['employee_requestor'])){
						$requestor_id 	= $arrs['req_flow']['employee_requestor'][0];
						if($requestor_id == $explode_ex){
							if($stats == 1){
								$act = ["cancel"=>true,"approve"=>false,"reject"=>false];			
							}else{
								$act = ["cancel"=>false,"approve"=>false,"reject"=>false];			
							}
						}else{
							$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
						}
					}else{
						$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
					}
					
				}else{
						
						try {
							if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){

								if(isset($arrs['req_flow']['employee_requestor'])){
									$requestor_id 	= $arrs['req_flow']['employee_requestor'][0];
									$requestor_jobs = $arrs['req_flow']['employee_requestor'][1];

									if($idx == $requestor_id){
										try {
											if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){
												if($arrs[$subs][$i2][$subs.'_stat'] == 1){
													$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
												}else{
													$act = ["cancel"=>true,"approve"=>false,"reject"=>false];
												}		
											}
										} catch (\Exception $e) {
											$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
										}

										
									}else{
										try {
											if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){
												if($arrs[$subs][$i2][$subs.'_stat'] == 1){
													$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
												}else{
													$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
												}		
											}
										} catch (\Exception $e) {
											$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
										}
									}
									if($stats != 1){
										$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
									}

								}else{
									try {
										if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){
											if($arrs[$subs][$i2][$subs.'_stat'] == 1){
												$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
											}else{
												$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
											}		
										}
									} catch (\Exception $e) {
										$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
									}
									//return $act;
								}
							}
						} catch (\Exception $e) {
							$act=null;
						}
				}
						// if($arrs[$subs][$i2][$subs.'_stat'] == 1){
						// 	$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
						// }else{
						// 	if($idx == $arr)				
						// }
					//}
				//}
				
			}else{

				if(isset($arrs['req_flow']['employee_requestor'])){
					try{
						if($arrs['requestor_stat'][$idx] == 0 || $arrs['requestor_stat'][$idx] == 1){
							
							if($arrs['req_flow']['employee_approve'] == "x"){
								
								$act = ["cancel"=>false,"approve"=>false,"reject"=>false];								
							}else{
								
								$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
							}
							if($stats > 1){
								$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
							}
					
						}else{
							//$act = $arrs['requestor_stat'][$idx];
							// if($stats == 1){
							// 	$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
							// }else{
							// 	$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
							// }
							//$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
						}
					}catch(\Exception $e){
						$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
					}
				}else{	
					try{
						if($arrs['requestor_stat'][$idx] == 0 || $arrs['requestor_stat'][$idx] == 1){
							if($stats == 1){
								//$act = ["cancel"=>true,"approve"=>false,"reject"=>false];
								$act = ["cancel"=>true,"approve"=>false,"reject"=>false];
							}else{
								$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
							}
						}else{							
								if($stats == 1){
									//$act = ["cancel"=>true,"approve"=>false,"reject"=>false];
									$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
								}else{
									$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
								}
								// if($arrs['requestor_stat'][$idx] == 0){
								// }elseif($arrs['requestor_stat'][$idx] == 1){
								// 	$act = ["cancel"=>false,"approve"=>false,"reject"=>false];	
								// }
						}
					}catch(\Exception $e){
						
						$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
					}
				}
			}
			return $act;
		}


		public function action_button_behavior_leave($value,$arrs,$idx,$stats){	

			$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
			if(isset($arrs['hrx_comp'])){ 
				$arr_idx = array_search($value,$arrs['hrx_comp']);
				if(gettype($arr_idx) == 'integer'){
					$subs = 'hr'; 
				}
			}if(isset($arrs['supx_comp'])){ 
				$arr_idx = array_search($value,$arrs['supx_comp']);
				if(gettype($arr_idx) == 'integer'){
					$subs = 'sup'; 
				}
			}if(isset($arrs['swapx_compx'])){ 
				$arr_idx = array_search($value,$arrs['swapx_compx']);
				if(gettype($arr_idx) == 'integer')
				$subs = 'swap'; 
			}

			if(isset($subs)){
				$i2=-1;		
				for ($i1=0; $i1 < count($arrs[$subs]); $i1++) {
					$keysx=array_keys($arrs[$subs][$i1]);
					
					if((string)$keysx[0]==$idx){
						//return $keysx;
						$i2=$i1;
					}else{
						$act = 'not';
					}
				}
				//return [$arrs[$subs][$i2]];			
				//for ($i=0; $i < count($arrs[$subs]); $i++) { 
					//if(isset($arrs[$subs][$i][$value])){
						// if($idx == 264){
						// 	return $arrs[$subs][$i];
						// }
					//return $arrs[$subs];
				//return $i2;
				if($i2==-1){
					//return 121;
					if(isset($arrs['req_flow']['employee_requestor'])){
						$requestor_id 	= $arrs['req_flow']['employee_requestor'][0];
						if($requestor_id == $explode_ex){
							if($stats == 1){
								$act = ["cancel"=>true,"approve"=>false,"reject"=>false];			
							}else{
								$act = ["cancel"=>false,"approve"=>false,"reject"=>false];			
							}
						}else{
							$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
						}
					}else{
						$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
					}
					
				}else{
						
						try {
							if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){

								if(isset($arrs['req_flow']['employee_requestor'])){
									$requestor_id 	= $arrs['req_flow']['employee_requestor'][0];
									$requestor_jobs = $arrs['req_flow']['employee_requestor'][1];

									if($idx == $requestor_id){
										try {
											if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){
												if($arrs[$subs][$i2][$subs.'_stat'] == 1){
													$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
												}else{
													$act = ["cancel"=>true,"approve"=>false,"reject"=>false];
												}		
											}
										} catch (\Exception $e) {
											$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
										}

										
									}else{
										try {
											if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){
												if($arrs[$subs][$i2][$subs.'_stat'] == 1){
													$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
												}else{
													$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
												}		
											}
										} catch (\Exception $e) {
											$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
										}
									}
									if($stats != 1){
										$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
									}

								}else{
									try {
										if(gettype($arrs[$subs][$i2][$idx]) == 'integer'){
											if($arrs[$subs][$i2][$subs.'_stat'] == 1){
												$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
											}else{
												$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
											}		
										}
									} catch (\Exception $e) {
										$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
									}
									//return $act;
								}
							}
						} catch (\Exception $e) {
							$act=null;
						}
				}
						// if($arrs[$subs][$i2][$subs.'_stat'] == 1){
						// 	$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
						// }else{
						// 	if($idx == $arr)				
						// }
					//}
				//}
				
			}else{

				if(isset($arrs['req_flow']['employee_requestor'])){
					try{
						if($arrs['requestor_stat'][$idx] == 0 || $arrs['requestor_stat'][$idx] == 1){
							
							if($arrs['req_flow']['employee_approve'] == "x"){
								
								$act = ["cancel"=>false,"approve"=>false,"reject"=>false];								
							}else{
								
								$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
							}
							if($stats > 1){
								$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
							}
					
						}else{
							//$act = $arrs['requestor_stat'][$idx];
							// if($stats == 1){
							// 	$act = ["cancel"=>false,"approve"=>true,"reject"=>true];
							// }else{
							// 	$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
							// }
							//$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
						}
					}catch(\Exception $e){
						$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
					}
				}else{	
					try{
						if($arrs['requestor_stat'][$idx] == 0 || $arrs['requestor_stat'][$idx] == 1){
							if($stats == 1){
								//$act = ["cancel"=>true,"approve"=>false,"reject"=>false];
								$act = ["cancel"=>true,"approve"=>false,"reject"=>false];
							}else{
								$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
							}
						}else{							
								if($stats == 1){
									//$act = ["cancel"=>true,"approve"=>false,"reject"=>false];
									$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
								}else{
									$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
								}
								// if($arrs['requestor_stat'][$idx] == 0){
								// }elseif($arrs['requestor_stat'][$idx] == 1){
								// 	$act = ["cancel"=>false,"approve"=>false,"reject"=>false];	
								// }
						}
					}catch(\Exception $e){
						
						$act = ["cancel"=>false,"approve"=>false,"reject"=>false];
					}
				}
			}
			return $act;
		}
	
	}// END class

}