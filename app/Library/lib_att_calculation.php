<?php namespace Larasite\Library{

use Larasite\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class calculation{
	public function calculation_attendance($arr){

		$value = $arr['value'];
		$fromx2 = $arr['formx2'];
		$tox = $arr['tox'];
		$strtotime_formx = $arr['strtotime_formx'];
		$strtotime_tox = $arr['strtotime_tox'];
		$timeIn = $arr['timeIn'];
		$timeOuts = $arr['timeOuts'];
		$strtotime_TimeInInquire = $arr['strtotime_TimeInInquire'];
		$strtotime_TimeInBiometric = $arr["strtotime_TimeInBiometric"];
		$strtotime_TimeOutInquire = $arr["strtotime_TimeOutInquire"];
		$strtotime_TimeOutBiometric = $arr["strtotime_TimeOutBiometric"];
		$late_fix = $arr["late_fix"];
		$eot_fix2 = $arr["eot_fix2"];
		$total_device_workhours = $arr["total_device_workhours"];

		if($value->date_inquire_absen){ // bio : (AM/PM, AM/AM, PM/PM) (NEXTDAY)
			
			if($am_pm_sch_from == 'pm' && $am_pm_sch_to == 'am'){ // SCH 23-12

				$strtotime_tox 		=  strtotime("+1 day",strtotime("$value->date $tox"));

				$am_pm_bio_in = date("a",strtotime($timeIn));
				$am_pm_bio_out = date("a",strtotime($timeOuts));

				if(($am_pm_bio_in == 'am' && $am_pm_bio_out == 'am') || ($am_pm_bio_in == 'pm' && $am_pm_bio_out == 'pm') || ($am_pm_bio_in == 'am' && $am_pm_bio_out == 'pm') ){

					$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInInquire;

					if($total_device_workhours > 0){ // benar in di nextday
						if($strtotime_TimeInInquire > $strtotime_formx){
							$mktimes_late_fix 	= mktime(0,($strtotime_TimeInInquire - $strtotime_formx)/60);
							$late_fix 			= date('H:i',$mktimes_late_fix);
						}else{ $late_fix=null; }
					}else{ // in di now
						$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInBiometric;

						if($strtotime_TimeInBiometric > $strtotime_formx){
							$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
							$late_fix 			= date('H:i',$mktimes_late_fix);
						}else{ $late_fix=null; }
					}

				}else if($am_pm_bio_in == 'pm' && $am_pm_bio_out == 'am'){

					$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInBiometric;

					if($strtotime_TimeInBiometric > $strtotime_formx){
						$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
						$late_fix 			= date('H:i',$mktimes_late_fix);
					}else{ $late_fix=null; }

				}

				if($strtotime_tox > $strtotime_TimeOutInquire){
					$mktimes_eot_fix2 	= mktime(0,($strtotime_tox - $strtotime_TimeOutInquire)/60);
					$eot_fix2 			= date('H:i',$mktimes_eot_fix2);
				}else{ $eot_fix2 = null; }
			
				
			}else if($am_pm_sch_from == 'am' && $am_pm_sch_to == 'pm'){ // SCH 01-23

				$total_device_workhours 	= $strtotime_TimeOutInquire - $strtotime_TimeInBiometric;

				if($strtotime_TimeInBiometric > $strtotime_formx){
					$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
					$late_fix 			= date('H:i',$mktimes_late_fix);
				}else{ $late_fix=null; }

				if($strtotime_tox > $strtotime_TimeOutInquire){
					$mktimes_eot_fix2 	= mktime(0,($strtotime_tox - $strtotime_TimeOutInquire)/60);
					$eot_fix2 			= date('H:i',$mktimes_eot_fix2);
				}else{ $eot_fix2 = null; }

			}

		}else{ // tanpa next day bio

			//if(($am_pm_sch_from == 'pm' && $am_pm_sch_to == 'am') || ($am_pm_sch_from == 'am' && $am_pm_sch_to == 'pm')){ // SCH 23-12
				
				$total_device_workhours 	= $strtotime_TimeOutBiometric - $strtotime_TimeInBiometric;
				
				if($strtotime_TimeInBiometric > $strtotime_formx){
					$mktimes_late_fix 	= mktime(0,($strtotime_TimeInBiometric - $strtotime_formx)/60);
					$late_fix 			= date('H:i',$mktimes_late_fix);
				}else{ $late_fix=null; }

				if($strtotime_tox > $strtotime_TimeOutBiometric){
					$mktimes_eot_fix2 	= mktime(0,($strtotime_tox - $strtotime_TimeOutBiometric)/60);
					$eot_fix2 			= date('H:i',$mktimes_eot_fix2);
				}else{ $eot_fix2 = null; }
			
				// if($value->date == "2018-08-15"){
				// 	return $eot_fix2;
				// }
			//}

		}

		$arr['value'] 						= $value;
		$arr['formx2'] 						= $fromx2;
		$arr['tox'] 						= $tox;
		$arr['strtotime_formx'] 			= $strtotime_formx;
		$arr['strtotime_tox']				= $strtotime_tox;
		$arr['strtotime_TimeInInquire'] 	= $strtotime_TimeInInquire;
		$arr["strtotime_TimeInBiometric"] 	= $strtotime_TimeInBiometric;
		$arr["strtotime_TimeOutInquire"] 	= $strtotime_TimeOutInquire;
		$arr["strtotime_TimeOutBiometric"] 	= $strtotime_TimeOutBiometric;
		$arr["late_fix"] 					= $late_fix;
		$arr["eot_fix2"] 					= $eot_fix2;
		$arr["total_device_workhours"] 		= $total_device_workhours;
		return $arr;

	}//end function
}// END class