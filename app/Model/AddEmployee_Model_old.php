<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class AddEmployee_Model extends Model {

	protected $table = 'emp';
	protected $fill = [];
 public function Read_Emp($id){
 	if(isset($id)){
 		$result = \DB::table($this->table)->where('employee_id','=',$id)->get($this->fill);
 		if($result){ $message = 'Read Data.'; $status=200; $data=$result;}
 		else{ $message = 'Data not found.'; $status=404; $data=null; }
 	}
 	else{ $message = 'ID not found.'; $status=500; $data=null; }
 	return ['message'=>$message,'status'=>$status,'data'=>$data];
 }

// STORE #####
	public function Store_Emp($id,$input){
		if(isset($input) && $id){
			
			$get_input = $this->set_input($input);
			$get_input['employee_id'] = $id;
			$store = \DB::table('emp')->insert($get_input);
			if($store){ $message = 'Store Successfully.'; $status=200; $data=$id;}
			else{ $message = 'Store Failed.'; $status=500; $data = null;}
		}else{ 
			$get_input = $this->set_input($input);
			$store = \DB::table('emp')->insert($get_input);
			if($store){ $message = 'Store Successfully.'; $status=200; $data=$id;}
			else{ $message = 'Store Failed.'; $status=500; $data = null;}
		}
		
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
// END STORE ###

	
// SET INPUT ###
 private function set_input($input){
 	return([
 		'first_name'	=> $input['first_name'],
 		'middle_name'	=> $input['middle_name'],
 		'last_name'		=> $input['last_name'],
 		'local_it'		=> $input['local_it'],
 		'ad_username'	=> $input['ad_username'],
 		'employee_id'	=> $input['id']
 		]);
 }
 //END SET INPUT ###

 // CHECK ID ###############################################################
// PARAM ID
	public function check_file_id($id){
		$r = \DB::select("SELECT filename from emp_picture where id = '$id' ");
		if(!$r){ return null; }
		foreach ($r as $key) {
			$data = $key->filename;
		}
		return $data;
	}
// END CHECK ID ###########################################################


// CHECK ID ###############################################################
// PARAM ID
	public function check_file_id2($id){
		$r = \DB::select("SELECT filename from emp_details where id = '$id'");
		foreach ($r as $key) {
			$data = $key->filename;
		}
		return $data;
	}
// END CHECK ID ###########################################################

 // GET ID ###############################################
public function Get_emp($id){
		$data = \DB::select("Select employee_id from emp_details where id = $id");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID
// GET ID EMP
public function GetID($id,$file){
		//$id = array();
		if(isset($id) && $file == null){
			$data =  \DB::table($this->table)->where('employee_id','=',$id)->get(['employee_id']);
			return $data;
			if($data != '[]' && $data){ $id = null; }else{ foreach ($data as $key) { $id = $key->employee_id; } }
		}
		elseif($file && !$id){	
			$data =  \DB::table('emp_picture')->where('filename','=',$file)->get(['employee_id']);
			if($data){ $id = null; }else{ foreach ($data as $key) { $id = $key->employee_id; } }
		}else{ $id = null; }
		return $id;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################

// GET LOCALIT ##################################################

public function Get_local($id){
	$data = \DB::select("SELECT if(local_it = 1,'expat','local') as local_it 
							FROM emp 
							WHERE employee_id = '$id' ");
	if(!isset($data)){ $data = null;}
	return $data;
}
// GET LOCAL ACTION 
public function getLocalAction($id)
{
	$role = \DB::table('ldap')->where('employee_id','=',$id)->get(['role_id']);
	foreach ($role as $key) { $role_id = $key->role_id; }
	$data = \DB::table('_action_emp')->where('role_id','=',$role_id)->get(['type_local']);
	if($data != '[]' && $data){
		foreach ($data as $key){ $type_loc = $key->type_local; }	
	}else{
		$type_loc = null;
	}
	return $type_loc;
	
}
// END GET TYPE ################################################

// GET LOCALIT ##################################################
public function Set_local($id){
	$data = \DB::select("SELECT c.local_it 
							FROM emp c 
							WHERE c.employee_id = '$id' ");
	if(!isset($data)){ $data = null;}
	return $data;
}
// END GET TYPE ################################################


// MOVE FILE IN TRASH ########################################################################
	public function move_file($filename){
		$get_data = \DB::select("SELECT `filename`, `path` from emp_details where filename = '$filename' ");
		foreach ($get_data as $key) {
			$data['path'] = $key->path;
			$data['filename'] = $key->filename;
		}
		if(\Storage::disk('local')->exists($data['path']."/".$data['filename'])){
			\Storage::disk('local')->move($data['path']."/".$data['filename'], "/Trash/".$data['filename']);
			if( \Storage::disk('local')->exists("/Trash/".$data['filename']) ){
				$message = 'Update Successfully.';
				$status = 200;
			}else{
				\Storage::disk('local')->move("/Trash/".$data['filename'],$data['path']."/".$data['filename']);
				$message = 'gagal move file to trash';
				$status = 500;
			}
		}
		else{
			$message = 'File tidak ditemukan, atau file telah dihapus check folder trash';
			$status = 404;
		}
		return ['message'=>$message,'status'=>$status];
	}
// END MOVE FILE #################################################################################

	public function insert_dataemp($input){

		($input['lastname'] == null ? $ls = null : $ls = $input['lastname']);
		($input['firstname'] == null ? $fs = null : $fs = $input['firstname']);

		($input['username'] == null ? $username = null : $username = $input['username']);
		($input['autonumb'] == null ? $autonumb = null : $autonumb = $input['autonumb']);
		($input['local'] == null ? $local == null : $local = $input['local']);
		($input['middlename'] == null ? $md == null : $md = $input['middlename']);

		$image = \Input::file('file');
		if($image != null){
			$file_name = $image->getClientOriginalName();
			$file_ext = $image->getClientOriginalExtension();

			$size = $image->getSize();
		

			//list($width,$height,$type,$attr) = getimagesize($image);


			// if($width >= 200 || $height >= 200){
			// 	if($width >= 200){
			// 		$width = ["msg" =>"width", "size" => $width];
					
			// 	}

			// 	if($height >= 200){
			// 		$height = ["msg" =>"height", "size" => $height];
					
			// 	}
			// }else{
				$path = "hrms_upload/personal_picture";
			
				if ($file_ext == "jpg" || $file_ext == "png" ){		
					if($size >= 1048576){
						$size= [ "msg" => "size", "size" => $size ];
						return  $size;
					}else{
						
						$file_ecy = "emp_photograph_".str_random(6).$file_ext;
						$image->move(storage_path($path),$file_ecy);
						if($local == "local_it" ){ $local_d = 2; }
						if($local == "local" ){ $local_d = 3;};
						$insert_emp = \DB::SELECT("insert into emp(first_name,last_name,middle_name,ad_username,employee_id,local_it) 
									              values('$fs','$ls','$md','$username','$autonumb',$local_d)");
						$insert_pic =  \DB::SELECT("insert into emp_picture(employee_id,filename,path) values('$autonumb','$file_ecy','$path')");
						if(isset($insert_emp) && isset($insert_pic)){
							$data_view = \DB::SELECT("select emp.first_name,emp.last_name,emp.middle_name,emp.ad_username,emp.employee_id,emp.local_it,emp_picture.filename,emp_picture.path from emp,emp_picture where emp.employee_id='$autonumb' and emp_picture.employee_id='$autonumb' " );
							$success = ["msg" => "success", "data" => $data_view[0]];
							return $success;
						}else{
							$failed = ["msg" => "failed"];
							return $failed;
						}
					}
				}else{
					$data = ["msg" => "format" ,"ext" => $file_ext];
					return $data;
				}
			//}
		}else{
			if($local == "local_it" ){ $local_d = 2; }
			if($local == "local" ){ $local_d = 3;};
			$insert_emp = \DB::SELECT("insert into emp(first_name,last_name,middle_name,ad_username,employee_id,local_it) 
									              values('$fs','$ls','$md','$username','$autonumb',$local_d)");
			if(isset($insert_emp)){
				$data_view = \DB::SELECT("select emp.first_name,emp.last_name,emp.middle_name,emp.ad_username,emp.employee_id,emp.local_it from emp where emp.employee_id='$autonumb' " );
				$success = ["msg" => "success", "data" => $data_view[0]];
				return $success;
			}else{
				$success = ["msg" => "failed", "data" => []];
				return $success;
			}
		}
	}
}
