<?php namespace Larasite\Model\Attendance\Request;

use Illuminate\Database\Eloquent\Model;

//date_format(str_to_date(timediff(t8.time_in,t3._from),'%H:%i:%s'),'%H:%i')as Late,
/*
(
		  			 	case 
		  			 		when date_format(t8.time_in,'%H:%i') > date_format(t3._from,'%H:%i')
		  			 		then date_format(str_to_date(timediff(t10.time_out,t8.time_in),'%H:%i:%s'),'%H:%i')
		  			 		else date_format(str_to_date(timediff(t10.time_out,t3._from),'%H:%i:%s'),'%H:%i')
		  			 	end
		  			 ) as Late
 date_format(str_to_date(timediff(t3._to,t10.time_out),'%H:%i:%s'),'%H:%i')as EarlyOut,
		  			 */
//TIMEDIFF(TIMEDIFF(date_format(t3._to,'%H:%i'),date_format(t3._from,'%H:%i')),TIMEDIFF(date_format(t10.time_out,'%H:%i'),date_format(t3._from,'%H:%i'))) as Short,
//timediff(date_format(t10.time_out,'%H:%i'),date_format(t3._from,'%H:%i'))as WorkHours		

//short
//ceil((timediff(date_format(t3._to,'%H:%i'),date_format(t3._from,'%H:%i')) - (select WorkHours)) /100)  			         			
class EmployeeRecordDetail_Model extends Model {
	// VIEW RECORD DATA******************************************************************
	public function getDetail($name)
	{
		//$query = \DB::SELECT("CALL View_Attendance_record('$name')");
		$query = \DB::SELECT("SELECT distinct t2.employee_id, date_format(t1.date,'%Y-%m-%d') as date,concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name) as Name,
					 concat(date_format(t3._from,'%H:%i'),'-',date_format(t3._to,'%H:%i'))as schedule,
					 substring(date_format(t3._from,'%H:%i'),0,2) as froms1,
					  substring(date_format(t3._from,'%H:%i'),3,2) as froms2,
					  substring(date_format(t3._to,'%H:%i'),0,2) as to1,
					  substring(date_format(t3._to,'%H:%i'),3,2) as to2,
					 date_format(t10.time_in,'%H:%i')as timeIn,date_format(t10.time_out,'%H:%i')as timeOut,
					 (
					 	case 
					 		when date_format(t8.time_in,'%H:%i') >  date_format(t3._from,'%H:%i')
					 		then (timediff(date_format(t10.time_out,'%H:%i'),date_format(t10.time_in,'%H:%i')))
					 		else
					 		      (timediff(date_format(t10.time_out,'%H:%i'),date_format(t3._from,'%H:%i')))
					 		end
					 )as WorkHours,
					 t3.hour_per_day,t5.total_overtime,t6.total_overtime as OvertimeRestDay,
					 1 as Short,
		  			 date_format(str_to_date(timediff(t8.time_in,t3._from),'%H:%i:%s'),'%H:%i')as Late,
					 ceil((timediff(date_format(t3._to,'%H:%i'),date_format(t3._from,'%H:%i')) - (select WorkHours)) /100)  as EarlyOut,
        			 t1.status,t4.input_ as ApproveStatus
					 FROM att_schedule t1
					 left join emp t2 on t2.employee_id=t1.employee_id
					 left join attendance_work_shifts t3 on t3.shift_id=t1.shift_id
					 left join biometrics_device t10 on t10.employee_id=t1.employee_id and t10.date=t1.date
					 left join biometrics_device t4 on t4.employee_id=t1.employee_id and t4.date=t1.date /* and t3._from !='00:00:00' and t3._from !='00:00:00' */
					 left join att_overtime t5 on t5.employee_id=t1.employee_id and t5.date_str=t1.date and t5.status_id=2 and t3.shift_code !='DO'
					 left join att_overtime t6 on t6.employee_id=t1.employee_id and t6.date_str=t1.date and t6.status_id=2 and t3.shift_code ='DO'
					 left join biometrics_device t7 on t7.employee_id=t1.employee_id and t7.date=t1.date and t3._from !='00:00:00' and t3._to !='00:00:00' and date_format(str_to_date(time_to_sec(timediff(t7.time_out,t7.time_in))/60/60,'%l.%i'),'%i') !='00'
					 left join biometrics_device t8 on t8.employee_id=t1.employee_id and t3._from !='00:00:00'  and t8.time_in > t3._from and t8.date=t1.date 
					 left join biometrics_device t9 on t9.employee_id=t1.employee_id and t3._to !='00:00:00'and t9.time_out<t3._to  and t9.date=t1.date
					 where t1.employee_id='$name'
					 order by t1.date ;");

		// $data = ['data'=>$query, 'status'=>200, 'message'=>'View record data'];
		// return $data;
		if($query){
			$c = count($query);
			$mod = $c % 10;
			$count = $c - $mod;
			// if($mod <= 10){
			// 	return $data = ['data'=>$query, 'status'=>500, 'message'=>'Record data is less than 1 month'];
			// }
			$j = 0 ;
			$last = $c - 20;

			for ($i = 0; $i < $c; ) { // $c = 57 / $i until 56
				// $dt = substr($dt, 8);
				// first period
				if ($i <= 9){

					$between = $query[$j]->date.' <=> '.$query[$j+9]->date;
					$between = $between;
				}
				// second period
				else if ($i >= 10 && $i <= 19){

					$between2 = $query[$j+(10-$j)]->date.' <=> '.$query[$j+(19-$j)]->date;

					$between = $between2;
				}
				// third period
				else{
					$date = date("d");

					$between3 = $query[$j+(20-$j)]->date.' <=> '.$query[$j+(($c-1)-$j)]->date;
					$between = $between3;
				}


				if ($i >= $count-1){ // if $i = 50 then $i - $c-1
					$between = $query[$i]->date.'<==>'.$query[$c-1]->date;
					$a = $mod;
				}
				else{
					$between = $query[$i]->date.'<==>'.$query[$i+9]->date;
					$a = 10;
				}
				for ($j=1; $j <= $a; $j++) {
					// create a mark, yes for workHour < hour_per_day, and no for other
					if ($query[$i]->WorkHours != null){
						if ($query[$i]->WorkHours < $query[$i]->hour_per_day) $workStatus = "yes";
						else $workStatus = "no";
					}
					else{
						$workStatus = "no";
					}
					/*
					//	Change the view to DO if schedule is 00:00-00:00
					//	Change the view to "-" if schedule is null
					*/
					if ($query[$i]->total_overtime == null) {
						$timeIns = strtotime($query[$i]->timeOut);
						$explode = explode('-',$query[$i]->schedule);
						$arg2    = strtotime($explode[1]);
						if($timeIns > $arg2){

							$totalOver = date('H:i',($timeIns - $arg2));// $hox.':'.$mox;//intval($ho).':'.$mi;
							$makeOver =  'yes';
						}else{
							$makeOver = 'no';
							$totalOver = "-";
						}
					}
					else {
						$floor   = floor($query[$i]->total_overtime);
						$decimal = $query[$i]->total_overtime - $floor;
						if($decimal != 0){
							$decimal = (60 * (10 * $decimal))/10;
						}else{
							$decimal = "00";
						}
						$ax = strlen($floor);

						$ax = ($ax == 1 ? '0'.$floor : $floor);
						$totalOver = $floor.':'.$decimal;
						$makeOver  ='no';

						
						// if ($query[$i]->WorkHours < $query[$i]->total_overtime)
						// 	$totalOver = $query[$i]->WorkHours;
						// else
						//($query[$i]->total_overtime);
						//$h = explode('.',$query[$i]->total_overtime);
			
					

						// $totalOver = 0;
						// $totalOver = $query[$i]->total_overtime;
						// $t = intval($query[$i]->total_overtime);

						// $x = explode(".",($t));
						// $ax = (strlen($x[0]) == 1 ? "0".$x[0] : $x[0]);
						// $bx = (60 / (10 / $x[1]));

						//$totalOver = $ax.':'.$bx;
					}
					if ($query[$i]->schedule == "00:00-00:00") {
						$schedule = "DO";
						/* 	if there is no time in or time out recorded,
						// 	the text will be colored red
						// 	$noTimeInStatus = "yes" means it will be colored red
						*/
						if ($query[$i]->timeIn == null AND $query[$i]->timeOut == null) {
							$timeIn = "-";
							$timeOut = "-";
							$noTimeInStatus = "yes";
							$noTimeInStatus = "yes";
						}
						else if ($query[$i]->timeIn == null AND $query[$i]->timeOut != null) {
							$timeIn = "-";
							$timeOut = $query[$i]->timeOut;
							$noTimeInStatus = "yes";
							$noTimeInStatus = "no";
						}
						else if ($query[$i]->timeIn != null AND $query[$i]->timeOut != null) {
							$timeIn = $query[$i]->timeIn;
							$timeOut = $query[$i]->timeOut;
							$noTimeInStatus = "no";
							$noTimeInStatus = "no";
						}
						else {
							$timeIn = $query[$i]->timeIn;
							$timeOut = "-";
							$noTimeInStatus = "no";
							$noTimeInStatus = "yes";
						}
					}
					else {
						$schedule = $query[$i]->schedule;
						if ($query[$i]->timeIn == null) {
							$timeIn = "-";
							$noTimeInStatus = "yes";
						}
						else {
							$timeIn = $query[$i]->timeIn;
							$noTimeInStatus = "no";
						}

						if ($query[$i]->timeOut == null) {
							$timeOut = "-";
							$noTimeOutStatus = "yes";
						}
						else {
							$timeOut = $query[$i]->timeOut;
							$noTimeOutStatus = "no";
						}
					}

					// if ($query[$i]->timeIn == null) {
					// 	$timeIn = "-";
					// 	$noTimeInStatus = "yes";
					// }
					// else {
					// 	$timeIn = $query[$i]->timeIn;
					// 	$noTimeInStatus = "no";
					// }

					// if ($query[$i]->timeOut == null) {
					// 	$timeOut = "-";
					// 	$noTimeOutStatus = "yes";
					// }
					// else {
					// 	$timeOut = $query[$i]->timeOut;
					// 	$noTimeOutStatus = "no";
					// }

					if ($query[$i]->WorkHours == null){ $workHour = "-"; }
					else{
						 $workHour = $query[$i]->WorkHours;
						 $exploder = explode(":",$workHour);
						 $temp = $workHour;
						 $hi = $exploder[0];
						 if(substr($hi,0,1) == 0){
						 	$workHour =  substr($hi,1,1).':'.$exploder[1];
						 }else{
						 	$workHour =  $hi.':'.$exploder[1];
						 }
					}
					// if ($query[$i]->OvertimeRestDay == null) $overRest = "-";
					// else $overRest = $query[$i]->OvertimeRestDay;

					if($totalOver != "-" and $makeOver != 'yes'){
						$timeIns = strtotime($query[$i]->timeOut);
						$explode = explode('-',$query[$i]->schedule);
						$arg2    = strtotime($explode[1]);
						$overRest = date('H:i',($timeIns - $arg2));
					}else{
						$overRest = "-";
					}

					// if ($workHour != "-") {
					// 	$arg_1  =  strval(substr($query[$i]->schedule,0,2));
					// 	$arg_11 =  strval(substr($query[$i]->schedule,3,2));

					// 	$arg_1s  =  strval(substr($query[$i]->schedule,6,2));
					// 	$arg_11s =  strval(substr($query[$i]->schedule,9,2));


					// 	$arg2  =  substr($exploder[0],0,2);
					// 	$arg21 =  substr($workHour,3,2);
					// 	$short = 1 ;
					// 	// if($arg_1 == 0){
					// 	// 	$arg_1 = 1;
					// 	// 	if($arg_1s == 1){
					// 	// 		$hox  = 1;
					// 	// 	}else{
					// 	// 		$hox  = $arg_1s - $arg_1;
					// 	// 	} 
					// 	// }elseif($arg_1 > 12 && $arg_1s >= 0){
					// 	// 	$arg_1 = 24 - $arg_1;
					// 	// 	$hox  =  $arg_1 + $arg_1s;
					// 	// }else{
					// 	// 	$hox =  $arg_1s - $arg_1;
					// 	// }

					// 	// if($arg_11 == 0){
					// 	// 	$arg_11 = 60;
					// 	// }
					// 	// if($arg_11s == 0){
					// 	// 	$arg_11s = 60;
					// 	// }

					// 	// $min = abs($arg_11 - $arg_11s);
					// 	// $short  = $hox.':'.$min;

					// 	// $work1 =  substr($temp,0,2);
					// 	// $work2 =  substr($temp,3,2);

					// 	// if(intval($min) == 0){
					// 	// 	$min = 60;
					// 	// }

					// 	// if(intval($work2) == 0){
					// 	// 	$work2 = 60;
					// 	// }

					// 	// $mix = abs($min - $work2);

					// 	// if($work1 > $hox){
					// 	// 	$hox = $work1 - $hox ;
					// 	// 	$short = $hox.':'.$mix;
					// 	// }

					// 	// if($work1 == $hox){
					// 	// 	if($min > $work2){
					// 	// 		$short = $hox.':'.$mix;
					// 	// 	}
					// 	// }

					// 	//$short  = '-';

					// 	//$hox = (strlen($hox) == 1 ? '0'.$hox : $hox);
					// 	//$min = (strlen($min) == 1 ? '0'.$min : $min);

					// }else{
						
					// 	// $explode = explode(":", $query[$i]->Short);
					// 	//  	$short = strlen($explode[0]);
					// 	//  	if($short == 2){
					// 	//  		$ho = (int)$explode[0];
					// 	//  		$mi = (int)$explode[1];
					// 	//  		$ho = $ho * 60;
					// 	//  		$short = $ho + $mi;
					// 	//  	}else{
					// 	//  		$short = "-";
					// 	//  	}
					// 	//$short = ($query[$i]->Short/100);
					// 	$short = "-";//($query[$i]->Short);

					// }

					$short = "-";

					
					

					if ($query[$i]->ApproveStatus == null || $query[$i]->ApproveStatus == 0 ){ $status = "no"; }
					elseif ($query[$i]->ApproveStatus == 1) {$status = "yes";}
					else{ $status = "no";}

					if ($query[$i]->Late == null) $late = "-";
					else $late = $query[$i]->Late;

					if ($query[$i]->EarlyOut == null){ $earlyOut = "-";}
					else{ //$earlyOut = $query[$i]->EarlyOut;

						$explode = explode(":",$query[$i]->EarlyOut);
						 	$earlyOut = strlen($explode[0]);
						 	if($earlyOut == 2){
						 		$ho = intval(substr($query[$i]->EarlyOut,0,2));
						 		$mi = intval(substr($query[$i]->EarlyOut,3,2));
						 		$ho = $ho * 60;
						 		$earlyOut = $ho + $mi ;
						 	}else{
						 		$earlyOut = "-";
						 	}
					}

					$h = substr($schedule, 6, strpos($schedule,":"));
					$m = substr($schedule, 9, strpos($schedule,10));
					//print_r($h."\n");
					$h1 = substr($timeOut, 0, strpos($timeOut,":"));
					$m1 = substr($timeOut, 3, strpos($timeOut,4));
					//print_r($h1."\n".'---');
					// create mark for the record that has been changed, yes for changed record
					if ($totalOver != null){
						if($makeOver == 'yes'){
							$overStatus = "yes";
						}elseif($makeOver == "no") {
							$overStatus = "no";
						}
						// if ($m1 > $m){
						// 	if ($totalOver == "-")
						// 		$overStatus = "yes";
						// 	else
						// 		$overStatus = "no";
						// }
						// else{
						// 	if ($totalOver == "-")
						// 		$overStatus = "yes";
						// 	else
						// 		$overStatus = "no";
						// }
					}
					else
						$overStatus = "no";
					// Compare time in and time out between attendance record and request
					$emp = $query[$i]->employee_id;
					$dt = $query[$i]->date;
					$_timeIN = $query[$i]->timeIn.':00';
					$_timeOUT = $query[$i]->timeOut.':00';

					// create mark for the record that has been changed, yes for changed record
					$tm = \DB::select("CALL View_TimeInOut_byDate_employee('$emp', '$dt')");
					$test[] = $dt;
					if ($tm != null) {
						if ($tm[0]->req_in != $_timeIN)
							$timeInStatus = "yes";
						else
							$timeInStatus = "no";

						if ($tm[0]->req_out != $_timeOUT)
							$timeOutStatus = "yes";
						else
							$timeOutStatus = "no";
					}
					else {
						$timeInStatus = "no";
						$timeOutStatus = "no";
					}
					if ($i == $c) {
						$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
						return $data;
					}
					else{
						$temp[] = [
							'date' => $query[$i]->date,
							'employee_id' => $query[$i]->employee_id,
							'Name' => $query[$i]->Name,
							'schedule' => $schedule,
							'timeIn' => $timeIn,
							'timeOut' => $timeOut,
							'noTimeIn' => $noTimeInStatus,
							'noTimeOut' => "yes",//$noTimeOutStatus,
							'WorkHours' => $workHour,
							'workStatus' => $workStatus,
							'total_overtime' => $totalOver,
							'overStatus' => $overStatus,
							'status' => $status,
							'OvertimeRestDay' => $overRest,
							'timeOutStatus' => $timeOutStatus,
							'timeInStatus' => $timeInStatus,
							'Short' => $short,
							'Late' => $late,
							'EarlyOut' => $earlyOut,
							'between' => $between
						];

						$i++;
					}
				}
			}
			$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
		}
		else{
			$data = ['data'=>null, 'status'=>500, 'message'=>'Record data is empty'];
		}
		return $data;
	}
	// UPDATE DETAIL******************************************************************
	public function updateDetail($i,$comment)
	{
		if($i['requestType'] == "Late Request"){
				$i['requestType'] ="Late";
		}
		if($i['requestType'] == "Early Out Exemption Request"){
				$i['requestType'] ="Early Out";
		}

		$typeId = $this->requestTypeId($i['requestType']);

		if ($typeId == null){
			$data = ['data'=>null, 'status'=>500, 'message'=>'No '.$i['requestType'].' Request found'];
		}
		else{
			if ($typeId == 9){
				$data = $this->updateDetailTimeIn($i,$comment,$typeId);
			}
			else if ($typeId == 7){
				$data = $this->updateDetailLate($i,$comment,$typeId);
			}
			else if ($typeId == 8){
				$data = $this->updateDetailLate($i,$comment,$typeId);
			}
			else{
				$data = ['data'=>null, 'status'=>200, 'message'=>'Failed'];
			}
		}
		return $data;
	}
	// GET REQUEST TYPE ID*********************************************************
	public function requestTypeId($type)
	{
		$findType = \DB::select("CALL Search_type_by_name('$type')");
		if (!$findType)
			$id = null;
		else
			$id = $findType[0]->type_id;
		return $id;
	}

	// UPDATE TIME-IN / TIME OUT***************************************************
	public function updateDetailTimeIn($i,$comment,$typeId)
	{
		if (isset($i['newtimeIn'])) {
			$inStatus = "yes";
			$newTimeIn = $i['newtimeIn'];
		}
		else{
			$newTimeIn = $i['timeIn'];
			$inStatus = "no";
		}
		if (isset($i['newtimeOut'])) {
			$outStatus = "yes";
			$newtimeOut = $i['newtimeOut'];
		}
		else{
			$outStatus = "no";
			$newtimeOut = $i['timeOut'];
		}
		$i['timeInStatus'] = $inStatus;
		$i['timeOutStatus'] = $outStatus;
		$check_req = \DB::SELECT("select created_at from att_time_in_out_req where employee_id='$i[employee_id]' and date='$i[date]' and req_in='$newTimeIn' and req_out='$newtimeOut' and type_id=$typeId ");

		if($check_req != null){
			return $data = ["message" => "Already request at ".$check_req[0]->created_at, 'status' => 500, "data" => []];
		}else{
			$image = \Input::file('file');
				if (isset($image)){
					$imageName = $image->getClientOriginalName();
					$ext = \Input::file('file')->getClientOriginalExtension();
					$path = "hrms_upload/timeinout";
					$size = $image->getSize();

					$rename = "Timeinout_".str_random(6).".".$image->getClientOriginalExtension();

						if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
						if($size > 104857600){
							return $data = ['data'=>$i, 'status'=>500, 'message'=>'Image size only support up to 10 MB'];

						}else{
							\Input::file('file')->move(storage_path($path),$rename);
						}
					}else{
							return $data = ['data'=>$i, 'status'=>500, 'message'=>'Unsupported format file'];
					}

				}else{
					$path = " ";
					$rename = " ";
				}

			$update = \DB::select("CALL Insert_TimeInOut('$i[employee_id]', '$i[date]', '$newTimeIn', '$newtimeOut','$comment',$typeId)");
			if(isset($image)){
				$select = \DB::SELECT("select req_in_out_id from att_time_in_out_req where employee_id='$i[employee_id]' order by req_in_out_id DESC");
				$id_sel = $select[0]->req_in_out_id;
				$insert = \DB::SELECT("insert into command_center(schedule_request_id,type_id,comment,path,filename,employee_id) values($id_sel,9,'$comment','$path','$rename','$i[employee_id]')");
			}
			return $data = ['data'=>$i, 'status'=>200, 'message'=>'Successfully update '.$i['requestType'].' Request'];
		}
	}
	// UPDATE LATE******************************************************************
	public function updateDetailLate($i,$comment,$typeId)
	{
		// insert updated late / early out record
		if($i["title"] == null || $i["title"] == "-" || $i['date'] == null || $i['date'] == "-" ){
				return $data = ['data'=>null, 'status'=>500, 'message'=>'Not found late time'];
		}else{
				$check_req = \DB::SELECT("select * from att_time_in_out_req where employee_id='$i[employee_id]' and type_id=$typeId and date='$i[date]' ");
				if($check_req != null){
					return $data = ['data'=>null, 'status'=>500, 'message'=>'Already request for that time'];
				}
				$update = \DB::select("CALL Insert_Late ('$i[employee_id]', '$i[Late]', $typeId, '$i[date]')");
				$image = \Input::file('file');
					if (isset($image)){
						$imageName = $image->getClientOriginalName();
						$ext = \Input::file('file')->getClientOriginalExtension();
						$path = "hrms_upload/lateinout";
						$size = $image->getSize();

						$rename = "L_OR_E_".str_random(6).".".$image->getClientOriginalExtension();

							if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
							if($size > 104857600){
								return $data = ['data'=>$i, 'status'=>500, 'message'=>'Image size only support up to 10 MB'];

							}else{
								\Input::file('file')->move(storage_path($path),$rename);
							}
						}else{
								return $data = ['data'=>$i, 'status'=>500, 'message'=>'Unsupported format file'];
						}

					}else{
						$path = " ";
						$rename = " ";
					}

				if(isset($image)){
					$select = \DB::SELECT("select req_in_out_id from att_time_in_out_req where employee_id='$i[employee_id]' order by req_in_out_id DESC");
					$id_sel = $select[0]->req_in_out_id;
					if($typeId == 7){
						$insert = \DB::SELECT("insert into command_center(schedule_request_id,type_id,comment,path,filename,employee_id) values($id_sel,7,'$comment','null','null','$i[employee_id]')");
					}else{
						$insert = \DB::SELECT("insert into command_center(schedule_request_id,type_id,comment,path,filename,employee_id) values($id_sel,8,'$comment','null','null','$i[employee_id]')");

					}
				}

				if ($update){
					$data = ['data'=>$update, 'status'=>200, 'message'=>'Successfully insert Late Request'];
				}
				else{
					$data = ['data'=>null, 'status'=>500, 'message'=>'Can not update '.$typeId.' Request'];
				}
				return $data;
		}
	}
	// GET CURRENT SCHEDULE, RECORDED TIME IN / OUT*********************************
	// public function getTimeInOut_model($emp_id,$date)
	// {
	// 	$getTimein = \DB::select("CALL View_TimeInOut('$emp_id','$date')");
	// 	if ($getTimein == null){
	// 		$data = ['data'=>'null','status'=>500,'message'=>'No schedule found on '.$date];
	// 	}
	// 	else{
	// 		$data = ['data'=>$getTimein,'status'=>200,'message'=>'Show schedule records'];
	// 	}
	// 	return $data;
	// }

	// // GET CURRENT SCHEDULE FOR LATE / EARLY OUT++++++++++++++++++++++++++++++++++++++
	// public function getTimeLateEarly_model($emp_id,$date,$requestType)
	// {
	// 	$getTime = \DB::select("CALL view_late_earlyOut('$emp_id','$date', '$requestType')");
	// 	if ($getTime == null){
	// 		$data = ['data'=>null,'status'=>500,'message'=>'No schedule found on '.$date];
	// 	}
	// 	else{
	// 		$data = ['data'=>$getTime,'status'=>200,'message'=>'Show schedule records'];
	// 	}
	// 	return $data;
	// }
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// public function getDetail2($name)
	// {
	// 	$query = \DB::SELECT("CALL View_Attendance_record('$name')");
	// 	$c = count($query);
	// 	$mod = $c % 10;
	// 	$count = $c - $mod;

	// 	for ($i = 0; $i < $c; ) { // $c = 57 / $i until 56
	// 		if ($i >= $count-1){ // if $i = 50 then $i - $c-1
	// 			$between = $query[$i]->date.'<==>'.$query[$c-1]->date;
	// 			$a = $mod;
	// 		}
	// 		else{
	// 			$between = $query[$i]->date.'<==>'.$query[$i+9]->date;
	// 			$a = 10;
	// 		}
	// 		for ($j=1; $j <= $a; $j++) {
	// 			if ($i == $c-1) {
	// 				return $data;
	// 				$i = $c-1;
	// 			}
	// 			else{
	// 				$temp[] = [
	// 						'date' => $query[$i]->date,
	// 						'employee_id' => $query[$i]->employee_id,
	// 						'between' => $between
	// 						];
	// 				// $query[$i];
	// 				$i++;
	// 			}
	// 		}
	// 		$data[] = [$between=>$temp];
	// 		$temp = [];
	// 	}
	// 	return $data;
	// }
}
