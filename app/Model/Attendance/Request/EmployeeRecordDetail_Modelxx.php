<?php namespace Larasite\Model\Attendance\Request;

use Larasite\Http\Controllers\Attendance\notificationV3;
use Illuminate\Database\Eloquent\Model;
	         			
class EmployeeRecordDetail_Model extends Model {
	// VIEW RECORD DATA******************************************************************
	public function getDetail($name,$paging = null)
	{
		//$query = \DB::SELECT("CALL View_Attendance_record('$name')");
		if($paging  == null){
			$paging_start =  0;
			$paging_end = 10;
		}else{
			$paging_start  = (($paging - 1) * 10) + 1;
			$paging_end = $paging * 10;
		}

		$query = \DB::SELECT("SELECT distinct  t2.employee_id as employee_id, date_format(t1.date,'%Y-%m-%d') as date,
				concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name) as Name, 
				t3._from as fromx, 
				t3._to as tox,
				COALESCE((select concat(date_format(attendance_work_shifts._from,'%H:%i'),'-',date_format(attendance_work_shifts._to,'%H:%i')) from attendance_work_shifts, att_change_shift where att_change_shift.employee_id = (t2.employee_id)  and att_change_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_change_shift.status_id = 2 and att_change_shift.new_shift = attendance_work_shifts.shift_id),'99:99:99') as value,
				
				COALESCE((select att_change_shift.created_at from attendance_work_shifts, att_change_shift,att_schedule_request where att_change_shift.employee_id = (t2.employee_id) and att_schedule_request.request_id =  att_change_shift.id  and att_change_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 and att_change_shift.new_shift = attendance_work_shifts.shift_id limit 1),'99:99:99') as date_changeShift,

				COALESCE((select att_swap_shift.created_at from att_schedule_request,att_swap_shift,attendance_work_shifts where att_swap_shift.employee_id = (t2.employee_id) and att_swap_shift.employee_id  = att_schedule_request.employee_id and att_swap_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 group by att_swap_shift.created_at limit 1 ),'99:99:99') as date_swapShift,
				
				COALESCE((select concat(time_format(attendance_work_shifts._from,'%H:%i'),'-',time_format(attendance_work_shifts._to,'%H:%i')) from att_schedule_request,att_swap_shift,attendance_work_shifts where att_swap_shift.employee_id = (t2.employee_id) and att_swap_shift.employee_id  = att_schedule_request.employee_id and att_swap_shift.old_shift_id = attendance_work_shifts.shift_id  and att_swap_shift.date = (select date_format(t1.date,'%Y-%m-%d')) and att_schedule_request.status_id = 2 group by att_swap_shift.created_at limit 1 ),'99:99:99') as schedule_swapShift,
					 date_format(t10.time_in,'%H:%i')as timeIn,date_format(t10.time_out,'%H:%i')as timeOut,
					  t3.hour_per_day,t5.total_overtime,t6.total_overtime as OvertimeRestDay, t6.status_id as RestDay_App ,
		   	
					 t1.status,t10.input_ as ApproveStatus
					 FROM att_schedule t1
					 left join emp t2 on t2.employee_id=t1.employee_id
					 left join attendance_work_shifts t3 on t3.shift_id=t1.shift_id
					 left join biometrics_device t10 on t10.employee_id=t1.employee_id and t10.date=t1.date
					 left join att_overtime t5 on t5.employee_id=t1.employee_id and t5.date_str=t1.date and t5.status_id=2 and t3.shift_code !='DO'
					 left join att_overtime t6 on t6.employee_id=t1.employee_id and t6.date_str=t1.date and t6.status_id=2 and t3.shift_code ='DO'
					 left join biometrics_device t7 on t7.employee_id=t1.employee_id and t7.date=t1.date and t3._from !='00:00:00' and t3._to !='00:00:00' and date_format(str_to_date(time_to_sec(timediff(t7.time_out,t7.time_in))/60/60,'%l.%i'),'%i') !='00'
					 left join biometrics_device t8 on t8.employee_id=t1.employee_id and t3._from !='00:00:00'  and t8.time_in > t3._from and t8.date=t1.date 
					 left join biometrics_device t9 on t9.employee_id=t1.employee_id and t3._to !='00:00:00'and t9.time_out<t3._to  and t9.date=t1.date
					 where t1.employee_id='$name'
					 and t1.status = 2
					 group by t1.date
	 				 order by t1.date ASC 
	 				");

		if($query !=  null){
			foreach ($query as $key => $value) {
				$date_changeShift  =  '';
				if($value->date_changeShift  !=   '99:99:99'){
						$employee_q  =  $value->employee_id;
						$date_changeS =  $value->date_changeShift;
						$db =  \DB::SELECT("select concat(substring(aws._from,1,5),'-',substring(aws._to,1,5)) as data from att_schedule_request as asr ,att_change_shift as acs, 	attendance_work_shifts as aws  
						 	where asr.update_at = '$date_changeS' 
							and asr.request_id = acs.id 
						 	and acs.new_shift = aws.shift_id
						 	and asr.employee_id  = '$employee_q'
						 	and  asr.type_id = 5 
						 	and asr.status_id =  2");

						if($db != null){
					   		$date_changeShift = $db[0]->data;
						}else{
							$date_changeShift = '99:99:99';
						}

				}else{
					$date_changeShift  =  '99:99:99';
				}

				$query[$key]->schedule_changeShift =   $date_changeShift;

				/** schedule  **/

				$schedule  = '';
				if($query[$key]->schedule_changeShift !=  '99:99:99'  and  $query[$key]->date_changeShift != '99:99:99' )
					{ $schedule   =  $query[$key]->schedule_changeShift;    }
				else if($value->date_swapShift != '99:99:99' and  $value->schedule_swapShift  !=  '99:99:99' ) 
					{ $schedule  =  $value->schedule_swapShift; }
				else
					{ $schedule  =  substr($value->fromx,0,5).'-'.substr($value->tox,0,5); }

				$query[$key]->schedule =  $schedule;

				/** workhour  **/

				$workhours  = '';
				$timeIn =  $value->timeIn;
				$fromx  =   $value->fromx;
				// $Eout = '';

				$tox =  $value->tox;
				$fromx  =  $value->fromx;

				if($value->timeIn != '-' and  $value->timeOut !=  '-' || $value->timeIn != null and  $value->timeOut !=  null){
				
					$timeIn  .= ':00';		


					if(strtotime($timeIn) < strtotime($fromx)){
						$workhours  =  date('H:i',strtotime($value->timeOut) - strtotime($value->fromx)); 
					}else{
						$workhours  =  date('H:i',strtotime($value->timeOut) - strtotime($value->timeIn));
					}

					$workhours .=  ':00';

					// if($value->date ==  "2017-09-08"){
					// 	return $workhours;
					// }

						if(strtotime($workhours) > strtotime(date('H:i',(strtotime($tox) -  strtotime($fromx)))) )
						{ $Eout =  0; }
						else
						{ 
							$timeOut  = $value->timeOut;
							$timeOut  = $timeOut.':00';

							$time1 =  substr(($query[$key]->schedule),6,5).':00';

							if(strtotime($time1) < strtotime($value->timeOut)){
								$Eout =  0;	
							}else{
								$Eout = date('H:i', strtotime($time1) - strtotime($value->timeOut));
							}

						}
				
				}else{

					$Eout = '-';
					$workhours  =  null;
				}



				$query[$key]->WorkHours =  $workhours;

				 //short
				if($workhours  != null){
					//if($value->date ==   '2017-09-01'){
						$diff = date('H:i:s',(strtotime($tox) -  strtotime($fromx)));

						$arr  = [$workhours,$tox,$fromx,$diff];

						if(strtotime($workhours) > strtotime($diff)){
							$shortx  =  0;
						}else{
							$shortx =  date('H:i:s',strtotime($diff) -  strtotime($workhours));
						}

						
				}else{
					$shortx  = "null";
				}

				

				$query[$key]->Short  =  $shortx;

				//schedule mod
				// $query[$key]->schedule_mod =  concat((substring((select schedule),1,5)),':00');	
				$query[$key]->schedule_mod = substr($query[$key]->schedule,0,5).':00';

				//late; 
			
				// $late  =  0;
				if(strtotime($query[$key]->schedule_mod) < strtotime($value->timeIn) and $value->timeOut != null)
					{ $late = date('H:i',(strtotime($value->timeIn) -  strtotime($query[$key]->schedule_mod))); }
				else
					{ $late  = 0;  }

				$query[$key]->Late  =  $late;

				//earlyOut  
				$query[$key]->EarlyOut = $Eout;	
				
			}
		}
			
		if($query){
			$c = count($query);
			$mod = $c % 10;
			$count = $c - $mod;
			// if($mod <= 10){
			// 	return $data = ['data'=>$query, 'status'=>500, 'message'=>'Record data is less than 1 month'];
			// }
			$j = 0 ;
			$last = $c - 20;

			for ($i = 0; $i < $c; ) { // $c = 57 / $i until 56
				// $dt = substr($dt, 8);
				// first period
				if ($i <= 9){

					$between = $query[$j]->date.' <=> '.$query[$j+9]->date;
					$between = $between;
				}
				// second period
				else if ($i >= 10 && $i <= 19){

					$between2 = $query[$j+(10-$j)]->date.' <=> '.$query[$j+(19-$j)]->date;

					$between = $between2;
				}
				// third period
				else{
					$date = date("d");

					$between3 = $query[$j+(20-$j)]->date.' <=> '.$query[$j+(($c-1)-$j)]->date;
					$between = $between3;
				}


				if ($i >= $count-1){ // if $i = 50 then $i - $c-1
					$between = $query[$i]->date.'<==>'.$query[$c-1]->date;
					$a = $mod;
				}
				else{
					$between = $query[$i]->date.'<==>'.$query[$i+9]->date;
					$a = 10;
				}
				for ($j=1; $j <= $a; $j++) {
					// create a mark, yes for workHour < hour_per_day, and no for other
					if ($query[$i]->WorkHours != null){
						// if($query[]->date  == '2017-09-02'){
						// 	return $arr  = [$query[$i]->WorkHours,$query[$i]->hour_per_day];
						// }	
						if ($query[$i]->WorkHours < $query[$i]->hour_per_day) {

							$emp_over  =  $query[$i]->employee_id;
							$date_str =  $query[$i]->date;
 
					
							$check_overtime = \DB::SELECT("select * from att_overtime as ao,att_schedule_request as asr where ao.employee_id =  '$emp_over' and ao.date_str  = '$date_str' and ao.id  = asr.request_id and asr.status_id  = 2  ");

							
							if($check_overtime != null){
								$workStatus = "no";
							}else{
								$workStatus = "yes";
							} 
						}else{
							$emp_over  =  $query[$i]->employee_id;
							$date_str =  $query[$i]->date;
							$check_overtime = \DB::SELECT("select * from att_overtime as ao,att_schedule_request as asr where ao.employee_id =  '$emp_over' and ao.date_str  = '$date_str' and ao.id  = asr.request_id and asr.status_id  = 2  ");

							
							if($check_overtime != null){
								$workStatus = "no";
							}else{
								$workStatus = "yes";
							} 
						}  
					}
					else{
						$workStatus = "no";
					}
					/*
					//	Change the view to DO if schedule is 00:00-00:00
					//	Change the view to "-" if schedule is null
					*/
					
					if ($query[$i]->schedule == "00:00-00:00") {
						$date_modify = $query[$i]->date;
						$emp_modify = $query[$i]->employee_id;

						$give =\DB::SELECT("select aws.shift_code from att_schedule ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify' 
							               and ash.date = '$date_modify' and  ash.shift_id = aws.shift_id  ");
						//$check_workshift = \DB::SELECT("select * from att_schedule where ")
						
						if($query[$i]->date_changeShift !=  '99:99:99' and $query[$i]->status ==  2){
							
							$give   = \DB::SELECT("select aws.shift_code from att_change_shift ash ,attendance_work_shifts aws where ash.employee_id = '$emp_modify' 
							               and ash.date = '$date_modify' and  ash.new_shift = aws.shift_id ");  
							$schedule = $give[0]->shift_code;
						}else{
							$schedule = $give[0]->shift_code;
						}

						if ($query[$i]->timeIn == null AND $query[$i]->timeOut == null) {
							$timeIn = "-";
							$timeOut = "-";
							$noTimeInStatus = "yes";
							$noTimeInStatus = "yes";
						}
						else if ($query[$i]->timeIn == null AND $query[$i]->timeOut != null) {
							$timeIn = "-";
							$timeOut = $query[$i]->timeOut;
							$noTimeInStatus = "yes";
							$noTimeInStatus = "no";
						}
						else if ($query[$i]->timeIn != null AND $query[$i]->timeOut != null) {
							$timeIn = $query[$i]->timeIn;
							$timeOut = $query[$i]->timeOut;
							$noTimeInStatus = "no";
							$noTimeInStatus = "no";
						}
						else {
							$timeIn = $query[$i]->timeIn;
							$timeOut = "-";
							$noTimeInStatus = "no";
							$noTimeInStatus = "yes";
						}
					}
					else {
						$schedule = $query[$i]->schedule;
						if ($query[$i]->timeIn == null) {
							$timeIn = "-";
							$noTimeInStatus = "yes";
						}
						else {
							$timeIn = $query[$i]->timeIn;
							$noTimeInStatus = "no";
						}

						if ($query[$i]->timeOut == null) {
							$timeOut = "-";
							$noTimeOutStatus = "yes";
						}
						else {
							$timeOut = $query[$i]->timeOut;
							$noTimeOutStatus = "no";
						}
					}
					
					if ($query[$i]->total_overtime == null) {
						$timeIns = strtotime($query[$i]->timeOut);
						$explode = explode('-',$query[$i]->schedule);
						$arg2    = strtotime($explode[1]);
						if($timeIns > $arg2){
							if($schedule == 'DO'){
								$totalOver = '-';
								
								//ini masalahnya   
								$mass = "yess";
							}else{
								if($query[$i]->timeOut == "00:00"){
									$query[$i]->timeOut = "24:00";
								}
								$time1x  =   explode(':',$query[$i]->timeOut);
								$time2x =  explode(':',$explode[1]);

								$time1xr  =   ($time1x[0] * 60) + $time1x[1];
								$time2xr  =   ($time2x[0] * 60) + $time2x[1];
								
								if(strlen(($time1xr - $time2xr) % 60) ==  1){
									$m	 = '0'.(($time1xr - $time2xr) % 60);
								}else{
									$m   = (($time1xr - $time2xr) % 60);
								}
								$totalOver =  floor(($time1xr - $time2xr) / 60).' : '.$m;
							
							}

						$makeOver =  'yes';
						}else{
							$makeOver = 'no';
							$totalOver = "-";
						}
					}else {

						$floor   = floor($query[$i]->total_overtime);
						$decimal = $query[$i]->total_overtime - $floor;
						if($decimal != 0){
							$decimal = (60 * (10 * $decimal))/10;
						}else{
							$decimal = "00";
						}
						$ax = strlen($floor);

						$ax = ($ax == 1 ? '0'.$floor : $floor);
						$totalOver = $floor.':'.$decimal;
						$makeOver  ='no';
					}


					if ($query[$i]->WorkHours == '00:00:00' || $query[$i]->WorkHours == null){
						 $workHour = "-"; $totalOver = "-"; 

					}
					else{

						if(strchr($query[$i]->WorkHours,'-')){
							
							$exp_schedule = explode('-', $query[$i]->schedule);
							$ts1 = strtotime($exp_schedule[0]);
							$ts2 = strtotime($exp_schedule[1]);
							$diff = abs($ts1 - $ts2) / 3600;
							if(strpos($diff,'.')){
								$e_menit = explode('.', "$diff");
								$tmp = "0.".$e_menit[1]; // build menit
								$b_menit = round(floatval($tmp) * 60 );
								if(strlen("$b_menit") == 2){
									$workHour =  $e_menit[0].":$b_menit";
								}else{
									$workHour =  $e_menit[0].":0$b_menit";
								}
							}else{
								$t = $diff/60;
								if(strchr("$t",".")){
									$diff = $diff/2;
								}
								$workHour = round($diff).":00";
							}
						}else{						
							 $workHour = $query[$i]->WorkHours;
							 $explode = explode(":",$workHour);
							 $hi = $explode[0];
							 if(substr($hi,0,1) == 0){
							 	$workHour =  substr($hi,1,1).':'.$explode[1];
							 }else{
							 	$workHour =  $hi.':'.$explode[1];
							 }
						}
					}		

					

					if($totalOver != "-" and $schedule == 'yes'){
						$timeIns = strtotime($query[$i]->timeOut);
						$explode = explode('-',$query[$i]->schedule);
						$arg2    = strtotime($explode[1]);
						$overRest = date('H:i',($timeIns - $arg2));
					}else if($query[$i]->timeIn and $query[$i]->timeOut){
						

						//$overRest = 
					}else{
						$overRest = "-";
					}

					// if($totalOver ==  "-"&& $overRest !="-"){
					// 	$totalOver = "-";
					// }

					if ($query[$i]->OvertimeRestDay == NULL){ 
						if(isset($mass) && $mass == "yess"){
							if($schedule == "DO"){
								$timeInss = strtotime($query[$i]->timeOut);
								$timeOuts = strtotime($query[$i]->timeIn);
								$overRest = date('H:i',($timeInss - $timeOuts));
								
								if($timeIns == false && $timeOuts == false){
									$overRest = "-";
								}else{
									$workHour = $overRest;
								}
							}
						}else{
							$overRest = "-";	
						}
						
					}else{
						$overRest = $query[$i]->OvertimeRestDay;
						$explode =  explode('.',$query[$i]->OvertimeRestDay);
						$floor =  floor($overRest);
						$check_floor = $overRest -  $floor;
						if($check_floor != 0){
							$overRest_decimal = (60 * (10 * $decimal))/10;
							$overRest = $explode[0].':'.$overRest_decimal;
						}else{
							$overRest = ((int)$explode[0]+1).':'.'00'; 
						}		
					}
					

					if ($query[$i]->Short == null) {$short = "-";}
					else{
						$time1 = intval(substr($query[$i]->Short,0,2));
						$time2 = intval(substr($query[$i]->Short,3,2));
						$short = ($time1 * 60) + $time2;
						if($short == 0){
							$short = '-';
						}else{
							$short =  $short;
						}

					}
					

					if ($query[$i]->ApproveStatus == null || $query[$i]->ApproveStatus == 0 ){ $status = "no"; }
					elseif ($query[$i]->ApproveStatus == 1) {$status = "yes";}
					else{ $status = "no";}

					if ($query[$i]->Late == null || $query[$i]->Late == "0"){ $late = "-"; 


					}else{
							
						$date_late = $query[$i]->date;
						$chkLate  = \DB::SELECT("select * from att_schedule_request where employee_id  = '$name' and availment_date = '$date_late' and type_id  = 7 and status_id  = 2 order by id desc  limit 1");
						if($chkLate ==  null){
							$colorLate  = 'red';
						}elseif(isset($chkLate[0])){
		
							if($chkLate[0]->approver ==  null){
								$colorLate  = 'orange';
							}else{
								$colorLate  = 'green';
							}
						}else{
							$colorLate  = 'green';
						}
						 	$late = $query[$i]->Late;
						//}
					}

					if ($query[$i]->EarlyOut == null){ $earlyOut = "-";}
					else{ 

						// $a1 = new \DateTime("17:00:00.000");
						// $a2 = new \DateTime("18:55:00.000");
						// $b 	= date_diff($a2,$a1); 
						// $str =$b->format('%H:%I');
						
						$explode = explode(":",$query[$i]->EarlyOut);
						 	$earlyOut = strlen($explode[0]);
						 	if($earlyOut == 2 and $earlyOut != '00'){
						 		$ho = (int)$explode[0];
						 		$mi = (int)$explode[1];
						 		$ho = $ho * 60;
						 		$earlyOut = $ho + $mi ;
						 	}else if($earlyOut == 2 and $earlyOut == '00'){
						 		$mi = (int)$explode[1];
						 		$earlyOut =   $mi;
						 	}else{
						 		$earlyOut = "-";
						 	}
						if($earlyOut != '-'){
							//return var_dump($query[$i]);
						}
					}
					$h = substr($schedule, 6, strpos($schedule,":"));
					$m = substr($schedule, 9, strpos($schedule,10));
					//print_r($h."\n");
					$h1 = substr($timeOut, 0, strpos($timeOut,":"));
					$m1 = substr($timeOut, 3, strpos($timeOut,4));
					// create mark for the record that has been changed, yes for changed record
					if ($totalOver != null){
						if($makeOver == 'yes'){
							$overStatus = ( $overRest == "-" ?  "yes" : "no");
							if(isset($mass) && $mass == "yess"){
								$overStatus = "yes";
							}
							// $overStatus = "yes";
						}elseif($makeOver == "no") {
							$overStatus = "no";
						}
					}
					else
						$overStatus = "no";
						if($overRest !=  null && isset($query[$i]->RestDay_App)){
							if($query[$i]->RestDay_App == 2){
								$OTR_color =  'yes';	
							}else{
								$OTR_color =  'no';
							}
							
						}else{
							$OTR_color = "-";
						}


					// Compare time in and time out between attendance record and request
					$emp = $query[$i]->employee_id;
					$dt = $query[$i]->date;
					$_timeIN = $query[$i]->timeIn.':00';
					$_timeOUT = $query[$i]->timeOut.':00';

					// create mark for the record that has been changed, yes for changed record
					$tm = \DB::select("CALL View_TimeInOut_byDate_employee('$emp', '$dt')");
					$test[] = $dt;
					if ($tm != null) {
						if ($tm[0]->req_in != $_timeIN)
							$timeInStatus = "yes";
						else
							$timeInStatus = "no";

						if ($tm[0]->req_out != $_timeOUT)
							$timeOutStatus = "yes";
						else
							$timeOutStatus = "no";
					}
					else {
						$timeInStatus = "no";
						$timeOutStatus = "no";
					}
					if ($i == $c) {

						$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
						return $data;
					}else{
						if($totalOver != '-'){
						
							if($workHour != "0:00"){
								// if($query[$i]->date  ==   '2017-08-22'){
								// 		return $totalOver;
								// }

								$totalOvers =  $totalOver;
							}else{
								
								$totalOvers = '-';
							}

							// if($totalOvers ==  '00:00' || $workHour == "0:00"){
							// 	$totalOvers = '-';
							// }
						}else{
							$totalOvers = '-';
						}


						if($overRest != "-"){
							$short = '-';
							$late = '-';
							$earlyOut = '-';
						}

						if(strlen($schedule) ==  2){
							$short = '-';
							$late = '-';
							$earlyOut = '-';
							$totalOvers = '-';
							$overRest = '-';

						}

						$empx = $query[$i]->employee_id;
						$date_request = $query[$i]->date;
						$latest_modified  =  \DB::SELECT("select from_,to_,leave_code from leave_request,leave_type where employee_id  = '$empx' and leave_request.leave_type = leave_type.id and leave_code != '' and leave_request.status_id = 2");
						$latest_request  =  \DB::SELECT("select * from att_schedule_request as asr, att_training as atr where asr.status_id = 2 and asr.employee_id  = '$empx' and asr.request_id = atr.id and '$date_request' >= atr.start_ and '$date_request' <= atr.end and asr.type_id = 1");
						$latest_request1  =  \DB::SELECT("select * from att_schedule_request as asr, att_training  as atr where asr.status_id = 2 and asr.employee_id  = '$empx' and asr.type_id = 2 and asr.request_id  = atr.id and '$date_request' >= atr.start_ and '$date_request' <= atr.end limit 1");
						$terminated = \DB::SELECT("select * from emp_termination_reason where employee_id  = '$empx' ");
						//if($latest_request != null){
						//$continue_training  = \Db::SELECT("select * from ")
						
							if(isset($latest_request[0]->type_id)  ){
								$schedule =   'T';
							}
							if(isset($latest_request1[0]->type_id)  ){
								$schedule =   'OB';	
							}
						//}
						if($latest_modified != null){
							foreach ($latest_modified as $key => $value) {
								if($value->from_ <= $query[$i]->date && $query[$i]->date <= $value->to_){
									$schedule =   $value->leave_code;
								}	
							}
						}

						
						$holiday = \DB::SELECT("select * from attendance_holiday");
							foreach ($holiday as $key => $value) {
								$holiday_date  =  $query[$i]->date;
								if($holiday != null){
									if($holiday[$key]->repeat_annually == 1){
										if($holiday[$key]->day == null && $holiday[$key]->week == null){
											if($holiday[$key]->date == $holiday_date ){
												$schedule = 'H';
											}
										}
										if($holiday[$key]->day != null && $holiday[$key]->week != null){
											
											$holiday[$key];
											$year =  date('Y');
											$ass_hole =  explode('-',$holiday[$key]->date);
											$month_name  =  date("F", mktime(0, 0, 0,(intval($ass_hole[1])), 10));
											$ass_day =  $holiday[$key]->day;

											if($holiday[$key]->week == 'first'){
												$week_day =  '+1';	
											}elseif($holiday[$key]->week == 'second'){
												$week_day =  '+2';
											}elseif ( $holiday[$key]->week == 'third'){
												$week_day = '+3';
											}elseif($holiday[$key]->week == 'fourth'){
												$week_day = '+4';
											}else{
											  $week_day = '+4';
											}
											$get_new_ass_hole  = date('Y-m-d',strtotime("$week_day week $ass_day $month_name $year"));
											
											if($holiday_date == $get_new_ass_hole){
										 		$schedule = 'H';
												
										 	}
										}		
									}else{
										if($holiday[$key]->date == $query[$i]->date){
											$schedule = 'H';
										}
									}
								}
							}
						

						if($terminated != null){
							$schedule = 'TER';
						}			
						if(!isset($colorLate)){
							$colorLate = 'red';
						}

						if($schedule  == 'DO'){
							$ti =  strtotime($timeIn.':00');
							$to =  strtotime($timeOut.':00');

							$overRest  = date('H:i',$to - $ti);
							if($overRest == '00:00'){
								$overRest  =   '-';
							}
						}else{
							$overRest = '-';
						}

						if($workStatus ==  'no'and  $schedule == 'DO'){
							$OTR_color = 'yes';							
						}else{
							$OTR_color = 'no';
						}
						$date_c = $query[$i]->date;
						$emp_c  = $query[$i]->employee_id;

						if($earlyOut  != null){
							$emp_c  = $query[$i]->employee_id;
							$date_c = $query[$i]->date;  
							// print_r("select * from  att_late, att_schedule_request where  att_late.type_id = 7 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id and att_schedule_request.approver = 2");
							$early_c =  \DB::SELECT("select * from  att_late, att_schedule_request where  att_late.type_id = 8 and date  = '$date_c' and att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id and att_schedule_request.approver = 2");

							if($early_c != null){
								$query[$i]->early_c =  'green';
							}else{
								$query[$i]->early_c =  'red';
							}
						}else{
							$query[$i]->early_c =  'red';
						}

						if(strlen($schedule) >  4){
							$turns =  explode('-',$schedule);
							if(!isset($turns[1])){
							  $workStatus  =  "no";
							}else{
							$base_date1 =  $turns[0].':00';
							$base_date2 =  $turns[1].':00';

							$substract_1 =  $timeIn.':00';
							$substract_2 =  $timeOut.':00'; 

						 	$get_tot_date1   = strtotime($base_date2) -  strtotime($base_date1);
							$get_tot_date2   = strtotime($substract_2) - strtotime($substract_1); 
							
								if($get_tot_date1 <= $get_tot_date2  || $get_tot_date1 == $get_tot_date2){
									$workStatus  =  "no";
								}
							}
						}	

						//check color  time  in   out 
					 	// par timeInStatusC  and timeOutStatusC
						
						//if($query[$i]->date  ===  '2017-09-10'){
							if($timeIn   ==   '-'){
								$date_timeIn   = $query[$i]->date;
								$date_timeIn_employee  =  $query[$i]->employee_id;
							
								$check_time_orange  = \DB::SELECT("select t2.status_id, t1.req_in from  att_time_in_out_req as t1,att_schedule_request 
												   as t2,pool_request as t3 
												   where t1.date  =   '$date_timeIn'
												   and   t1.req_in_out_id =  t2.request_id and  t1.type_id = t2.type_id
												   and   t2.id = t3.id_req  and  t2.type_id   =  t3.type_id and t3.master_type = 1
												   and  t1.employee_id = '$date_timeIn_employee' order  by  t1.req_in_out_id DESC limit 1 ");

								if($check_time_orange  != null){
									///return  $check_time_orange;
									if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  ==  1){
										
											$timeInStatusC = 'orange';
											$timeIn  =   substr($check_time_orange[0]->req_in,0,5); 
									}else{
										$timeInStatusC = 'green';
									}
								}else{
									$timeInStatusC = 'red';
								}
								//if($check)
							}else{ $timeInStatusC = 'green'; }
						//}

						if($timeOut   ==   '-'){
							$date_timeOut   = $query[$i]->date;
							$date_timeOut_employee  =  $query[$i]->employee_id;
							$check_time_orange  = \DB::SELECT("select t2.status_id,t1.req_out from  att_time_in_out_req as t1,att_schedule_request 
																as t2,pool_request as t3 
																where t1.date  =   '$date_timeOut'
																and   t1.req_in_out_id =  t2.request_id and  t1.type_id = t2.type_id
																and   t2.id = t3.id_req  and  t2.type_id   =  t3.type_id and t3.master_type = 1
																and  t1.employee_id = '$date_timeOut_employee' order  by  t1.req_in_out_id DESC limit 1 ");

							if($check_time_orange  != null){
								if(isset($check_time_orange[0]) and  $check_time_orange[0]->status_id  == 1){
										$timeOutStatusC = 'orange';
										$timeOut  =   substr($check_time_orange[0]->req_out,0,5); 
								}else{
									$timeOutStatusC = 'green';
								}
							}else{
								$timeOutStatusC = 'red';
							}
							//if($check)
						}else{ $timeOutStatusC = 'green'; }

						//check_color_orange 

						//check colcor late
						if($colorLate == 'red'){
							if($query[$i]->date ==  '2017-03-05'){
						
								$date_late = $query[$i]->date;
								$nameX  =  $query[$i]->employee_id;
								$chkLate  = \DB::SELECT("select * from att_schedule_request where employee_id  = '$nameX' and availment_date = '$date_late' and type_id  = 7 and  status_id  = 1 order by id desc  limit 1");

								if($chkLate  !=  null){
									//if($chkLate[0]->approver  ==   null){
										$colorLate  = 'orange';
									//}
								}
							}
						}

						//check color early  out   
						if($query[$i]->early_c =  'red'){
						$early_c =  \DB::SELECT("select * from  att_late, att_schedule_request where  att_late.type_id = 8 and date  = '$date_c' and 	att_late.employee_id = '$emp_c' and  att_late.late_id  = att_schedule_request.request_id and att_schedule_request.status_id = 1");

							if($early_c != null){
								$query[$i]->early_c =  'orange';
							}
						}

						//set color to  oramg  for  overtime and overtime dso
						//if  already input for  overtime  rest day parameter $OTR_color
						//If  already  input for  overtime   no  do    parameter $overtime_non_do


						//return $totalOvers;
						if($totalOvers != null){
							$date_overtime  =  $query[$i]->date;
							$employee_overtime = $query[$i]->employee_id ;

							$search  = \DB::SELECT("select * from att_overtime as t1, att_schedule_request as t2
													where t1.employee_id  =  '$employee_overtime' 
													and  t1.date_str =  '$date_overtime' 
													and  t1.type_id =  t2.type_id  
													and  t1.id = t2.request_id
													order by t1.id desc limit 1 
													");	
							if($search  != null){
								if($search[0]->status_id == 1){
									$overtime_non_do  =  'orange';
								}else{
									$overtime_non_do  =  'green';
								}


							}else{
									 $overtime_non_do =   'red';
							}

						}else{
							$overtime_non_do = 'red';
						}

						if($overRest != null){
							$date_overtime  =  $query[$i]->date;
							$employee_overtime = $query[$i]->employee_id ;

							$search  = \DB::SELECT("select * from att_overtime as t1, att_schedule_request as t2
													where t1.employee_id  =  '$employee_overtime' 
													and  t1.date_str =  '$date_overtime' 
													and  t1.type_id =  t2.type_id  
													and  t1.id = t2.request_id
													order by t1.id desc limit 1 
													");	
							if($search  != null){
								if($search[0]->status_id == 1){
									$OTR_color  =  'orange';
								}else{
									$OTR_color  =  'green';
								}


							}else{
									$OTR_color =   'red';
							}

						}else{
							$OTR_color  = 'red';
						}

						//check  changeshift
						if(isset($query[$i]->date) ){
							// if($query[$i]->date ==  '2017-09-14'){
							// $date_changeshift  =  $query[$i]->date;
							// $employee_changeshift = $query[$i]->employee_id ;

							// return $check_exist_change_shift   =  \DB::SELECT("select * from  att_change_shift as t1 ,  att_schedule_request  as t2,
							// 											attendance_work_shifts as  t3
							// 										    where t1.employee_id = '$employee_changeshift' and  date = '$date_changeshift'
							// 										    and  t1.type_id  =  t2.type_id 
							// 										    and  t1.id  = t2.request_id  
							// 										    and  t1.new_shift  =  t3.shift_id
							// 										    and t2.status_id  =  2 order  by  t1.id  desc limit 1");

							// return   $check_exist_change_shift;

							// }
							$date_changeshift  =  $query[$i]->date;
							$employee_changeshift = $query[$i]->employee_id ;

							
							$check_exist_change_shift   =  \DB::SELECT("select * from  att_change_shift as t1 ,  att_schedule_request  as t2,
																		attendance_work_shifts as  t3
																	    where t1.employee_id = '$employee_changeshift' and  date = '$date_changeshift'
																	    and  t1.type_id  =  t2.type_id 
																	    and  t1.id  = t2.request_id  
																	    and  t1.new_shift  =  t3.shift_id
																	    and t2.status_id  =  2 order  by  t1.id  desc limit 1");
							if($check_exist_change_shift != null){
								$list_code = ['DO','ADO','VL','BL','TB','SL','EL','BE','ML','PL','MA','OB',];



								if(in_array($check_exist_change_shift[0]->shift_code,$list_code)){
									$schedule =  $check_exist_change_shift[0]->shift_code;
								}else{
									$schedule  =  substr($check_exist_change_shift[0]->_from,0,5).'-'. substr($check_exist_change_shift[0]->_to,0,5);
								}
							}

						}

						$temp[] = [
							'early_c' => $query[$i]->early_c,   
							'date' => $query[$i]->date,
							'colorLate' => $colorLate,
							'employee_id' => $query[$i]->employee_id,
							'Name' => $query[$i]->Name,
							'schedule' => $schedule,
							'timeIn' => $timeIn,
							'timeOut' => $timeOut,
							'noTimeIn' => $noTimeInStatus,
							'noTimeOut' => "yes",//$noTimeOutStatus,
							'WorkHours' => $workHour,
							'workStatus' => $workStatus,
							'total_overtime' => $totalOvers,
							'new_color_overtime' => $overtime_non_do,
							'overStatus' => $overStatus,
							'status' => $status,
							'OvertimeRestDay' => $overRest,
							'OvertimeRestDay_Color' => $OTR_color,
							'timeOutStatus' => $timeOutStatus,
							'timeInStatus' => $timeInStatus,
							'C_TimeIn' => $timeInStatusC,
							'C_TimeOut' => $timeOutStatusC,
							'Short' => $short,
							'Late' => $late,
							'EarlyOut' =>  ($earlyOut == 0 ? $earlyOut = '-' : $earlyOut = $earlyOut  ),
							'between' => $between
						];


					
						$i++;
					}
				}
			}


			// $count_temp  =  count($queryx);

			// if($count_temp  > 9){
			// 	$mod  =  $count_temp   % 10;
			// 	if($mod  != 0){
			// 		$get_paging  =  ($count_temp -  $mod) / 10;
			// 		$get_paging  += 1;   
			// 	}else{
			// 		$get_paging  =  $count_temp  / 10;
			// 	} 'paging' => $get_paging
			// }


			$data = ['data'=>$temp, 'status'=>200, 'message'=>'View record data'];
		}
		else{
			$data = ['data'=>null, 'status'=>500, 'message'=>'Record data is empty'];
		}
		return $data;
	}
	// UPDATE DETAIL******************************************************************
	public function updateDetail($i,$comment)
	{
		if($i['requestType'] == "Late Request"){
				$i['requestType'] ="Late";
		}
		if($i['requestType'] == "Early Out Exemption Request"){
				$i['requestType'] ="Early Out";
		}
		// return date('H:i',)
		$typeId = $this->requestTypeId($i['requestType']);

		if ($typeId == null){
			$data = ['data'=>null, 'status'=>500, 'message'=>'No '.$i['requestType'].' Request found'];
		}
		else{
			if ($typeId == 9){
				$data = $this->updateDetailTimeIn($i,$comment,$typeId);
			}
			else if ($typeId == 7){
				$data = $this->updateDetailLate($i,$comment,$typeId);
			}
			else if ($typeId == 8){
				$data = $this->updateDetailLate($i,$comment,$typeId);
			}
			else{
				$data = ['data'=>null, 'status'=>200, 'message'=>'Failed'];
			}
		}
		return $data;
	}
	// GET REQUEST TYPE ID*********************************************************
	public function requestTypeId($type)
	{
		$findType = \DB::select("CALL Search_type_by_name('$type')");
		if (!$findType)
			$id = null;
		else
			$id = $findType[0]->type_id;
		return $id;
	}

	// UPDATE TIME-IN / TIME OUT***************************************************
	public function updateDetailTimeIn($i,$comment,$typeId)
	{
		if (isset($i['newtimeIn'])) {
			$inStatus = "yes";
			$newTimeIn = $i['newtimeIn'];
		}
		else{
			$newTimeIn = $i['timeIn'];
			$inStatus = "no";
		}
		if (isset($i['newtimeOut'])) {
			$outStatus = "yes";
			$newtimeOut = $i['newtimeOut'];
		}
		else{
			$outStatus = "no";
			$newtimeOut = $i['timeOut'];
		}
		$i['timeInStatus'] = $inStatus;
		$i['timeOutStatus'] = $outStatus;
		$check_req = \DB::SELECT("select created_at from att_time_in_out_req where employee_id='$i[employee_id]' and date='$i[date]' and req_in='$newTimeIn' and req_out='$newtimeOut' and type_id=$typeId ");

		if($check_req != null){
			return $data = ["message" => "Already request at ".$check_req[0]->created_at, 'status' => 500, "data" => []];
		}else{
			$image = \Input::file('file');
				if (isset($image)){
					$imageName = $image->getClientOriginalName();
					$ext = \Input::file('file')->getClientOriginalExtension();
					$path = "hrms_upload/timeinout";
					$size = $image->getSize();

					$rename = "Timeinout_".str_random(6).".".$image->getClientOriginalExtension();

						if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
						if($size > 104857600){
							return $data = ['data'=>$i, 'status'=>500, 'message'=>'Image size only support up to 10 MB'];
						}else{
							\Input::file('file')->move(storage_path($path),$rename);
						}
					}else{
							return $data = ['data'=>$i, 'status'=>500, 'message'=>'Unsupported format file'];
					}

				}else{
					$path = " ";
					$rename = " ";
				}

			$update = \DB::select("CALL Insert_TimeInOut('$i[employee_id]', '$i[date]', '$newTimeIn', '$newtimeOut','$comment',$typeId)");
			$getLastId = \DB::SELECT("select * from att_schedule_request where employee_id =  '$i[employee_id]' order by id desc limit 1");
			$getLastId = $getLastId[0]->id;
			$modex  =  new notificationV3;
			$emp_lc  =  \DB::SELECT("select local_it from  emp  where employee_id =  '$i[employee_id]' ");
			if($emp_lc[0]->local_it == 1){
				$lc =  'expat';
			}else
	{			$lc = 'local';
			}
			// $select = \DB::SELECT("select req_in_out_id from att_time_in_out_req where employee_id='$i[employee_id]' order by req_in_out_id DESC");
			// $id_sel = $select[0]->req_in_out_id;
			$insert = \DB::SELECT("insert into command_center(type_id,master_type,request_id,comment,path,filename,employee_id) values(9,1,$getLastId,'$comment','$path','$rename','$i[employee_id]')");
			$check = $modex->notif($getLastId,9,$i['employee_id'],$lc,'user','attendance');	
			// if(isset($image)){
			// }
			return $data = ['data'=>$i, 'status'=>200, 'message'=>'Successfully update '.$i['requestType'].' Request'];
		}
	}
	// UPDATE LATE******************************************************************
	public function updateDetailLate($i,$comment,$typeId)
	{
		// insert updated late / early out record
		if($i["title"] == null || $i["title"] == "-" || $i['date'] == null || $i['date'] == "-" ){
				return $data = ['data'=>null, 'status'=>500, 'message'=>'Not found late time'];
		}else{
				if($i['requestType'] == 'Early Out'){
			   	 if($i['EarlyOut'] ==  "-" || $i['EarlyOut'] ==  "0"){
			   	 	return $data = ['data'=>$i, 'status'=>500, 'message'=>"Error : you don't have it any Early Out time"];
			   	 }
			   }

			   if($i['requestType'] == 'Late'){
			   	 if($i['Late'] ==  "-" || $i['Late'] ==  "0" ){
			   	 	return $data = ['data'=>$i, 'status'=>500, 'message'=>"Error : you don't have it any Late time"];	
			   	 }
			   }

			   if($i['requestType'] ==  'Late'){
			   		$typeX  = 7;
			   }else{
			   		$typeX =  8;
			   }
				
				// print_r("select * from att_late as al,att_schedule_request as asreq where al.employee_id='$i[employee_id]' and al.type_id=$typeX and al.date='$i[date]' and al.late_id =  asreq.request_id  and asreq.status_id in (1,2,5,6)");

				$check_req = \DB::SELECT("select * from att_late as al,att_schedule_request as asreq where al.employee_id='$i[employee_id]' and al.type_id=$typeX and al.date='$i[date]' and al.late_id =  asreq.request_id  and asreq.status_id in (1,2,5,6) ");

				$check_local_it  = \DB::SELECT("select * from emp where employee_id = '$i[employee_id]' ");
				if($check_req != null ){
					return $data = ['data'=>null, 'status'=>500, 'message'=>'Already request for that time'];
				}
				if($check_local_it[0]->local_it == 1){
					return $data = ['data'=>null, 'status'=>500, 'message'=>'Only for local employ'];
				}
				$minutex =  $i['Late'] % 60;
				$hourx  =  $i['Late'] -  $minutex;
				if(strlen($hourx) == 3){ $hourx = $hourx / 60;}
				if(strlen($hourx) == 1 ){$hourx = '0'.$hourx ;} 
				if(strlen($minutex) == 1) {$minutex = '0'.$minutex; }
				$i['Late']  =  $hourx.':'.$minutex.':00';
				
				$update = \DB::select("CALL Insert_Late ('$i[employee_id]', '$i[Late]', $typeId, '$i[date]')");
				$image = \Input::file('file');
					if (isset($image)){
						$imageName = $image->getClientOriginalName();
						$ext = \Input::file('file')->getClientOriginalExtension();
						$path = "hrms_upload/lateinout";
						$size = $image->getSize();

						$rename = "L_OR_E_".str_random(6).".".$image->getClientOriginalExtension();

							if($ext == "jpg" || $ext == "png" || $ext == "pdf" || $ext == "JPG" || $ext == "PNG" || $ext == "PDF"){
							if($size > 104857600){
								return $data = ['data'=>$i, 'status'=>500, 'message'=>'Image size only support up to 10 MB'];

							}else{
								\Input::file('file')->move(storage_path($path),$rename);
							}
						}else{
								return $data = ['data'=>$i, 'status'=>500, 'message'=>'Unsupported format file'];
						}

					}else{
						$path = " ";
						$rename = " ";
					}

				//if(isset($image)){
					//$select = \DB::SELECT("select req_in_out_id from att_time_in_out_req where employee_id='$i[employee_id]' order by req_in_out_id DESC");
					$select = \DB::SELECT("select * from att_schedule_request where employee_id='$i[employee_id]' and type_id = $typeId order by id DESC limit 1");
					
					$id_sel = $select[0]->id;
				    $modex  =  new notificationV3;
					

					if($typeId == 7){
						$insert = \DB::SELECT("insert into command_center(type_id,master_type,request_id,comment,path,filename,employee_id) values(7,1,$id_sel,'$comment','$path','$rename','$i[employee_id]')");
					}else{
						$insert = \DB::SELECT("insert into command_center(type_id,master_type,request_id,comment,path,filename,employee_id) values(8,1,$id_sel,'$comment','$path','$rename','$i[employee_id]')");
					}
				    
				    if($typeId == 7){
				    	$check = $modex->notif($id_sel,7,$i['employee_id'],'local','user','attendance');
				    }else{
				    	$check = $modex->notif($id_sel,8,$i['employee_id'],'local','user','attendance');
				    }
						
				//}

				//if ($update){
					if($typeId == 7){
						$data = ['data'=>$update, 'status'=>200, 'message'=>'Successfully insert Late Request'];
					}else{
						$data = ['data'=>$update, 'status'=>200, 'message'=>'Successfully early out Request'];
					}
				//}
				//else{
					//$data = ['data'=>null, 'status'=>500, 'message'=>'Can not update '.$typeId.' Request'];
				//}
				return $data;
		}
	}
	// GET CURRENT SCHEDULE, RECORDED TIME IN / OUT*********************************
	// public function getTimeInOut_model($emp_id,$date)
	// {
	// 	$getTimein = \DB::select("CALL View_TimeInOut('$emp_id','$date')");
	// 	if ($getTimein == null){
	// 		$data = ['data'=>'null','status'=>500,'message'=>'No schedule found on '.$date];
	// 	}
	// 	else{
	// 		$data = ['data'=>$getTimein,'status'=>200,'message'=>'Show schedule records'];
	// 	}
	// 	return $data;
	// }

	// // GET CURRENT SCHEDULE FOR LATE / EARLY OUT++++++++++++++++++++++++++++++++++++++
	// public function getTimeLateEarly_model($emp_id,$date,$requestType)
	// {
	// 	$getTime = \DB::select("CALL view_late_earlyOut('$emp_id','$date', '$requestType')");
	// 	if ($getTime == null){
	// 		$data = ['data'=>null,'status'=>500,'message'=>'No schedule found on '.$date];
	// 	}
	// 	else{
	// 		$data = ['data'=>$getTime,'status'=>200,'message'=>'Show schedule records'];
	// 	}
	// 	return $data;
	// }
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// public function getDetail2($name)
	// {
	// 	$query = \DB::SELECT("CALL View_Attendance_record('$name')");
	// 	$c = count($query);
	// 	$mod = $c % 10;
	// 	$count = $c - $mod;

	// 	for ($i = 0; $i < $c; ) { // $c = 57 / $i until 56
	// 		if ($i >= $count-1){ // if $i = 50 then $i - $c-1
	// 			$between = $query[$i]->date.'<==>'.$query[$c-1]->date;
	// 			$a = $mod;
	// 		}
	// 		else{
	// 			$between = $query[$i]->date.'<==>'.$query[$i+9]->date;
	// 			$a = 10;
	// 		}
	// 		for ($j=1; $j <= $a; $j++) {
	// 			if ($i == $c-1) {
	// 				return $data;
	// 				$i = $c-1;
	// 			}
	// 			else{
	// 				$temp[] = [
	// 						'date' => $query[$i]->date,
	// 						'employee_id' => $query[$i]->employee_id,
	// 						'between' => $between
	// 						];
	// 				// $query[$i];
	// 				$i++;
	// 			}
	// 		}
	// 		$data[] = [$between=>$temp];
	// 		$temp = [];
	// 	}
	// 	return $data;
	// }
}
