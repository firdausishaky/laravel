<?php namespace Larasite\Model\Attendance\Request;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class RequestDetail_Model extends Model {

	// view list of schedule request
	public function viewList($i)
	{
		$action = \DB::select("CALL View_Request('$i[from]', '$i[to]', $i[status])");
		if (!$action){
			$res = [
				'data' => null,
				'status' => 500,
				'message' => 'No schedule request found'
			];
		}
		else{
			// array_push($action, ['from'=>$i['from']], ['to'=>$i['to']]);
			$res = [
				'data' => $action,
				'status' => 200,
				'message' => 'Show schedule request list'
			];
		}
		return $res;
	}


	// CHECK FOR STATUS ID FOR STATUS
	public function status($st)
	{
		$st = strtolower($st);
		if ($st == "approve"){
			$status = 2;
		}
		else if ($st == "reject"){
			$status = 3;
		}
		else{
			$status = 1;
		}
		return $status;
	}

	// VIEW SCHEDULE LIST DETAIL FOR EDIT
	// public function editDetail($id, $type_id,$ex, $empID, $empReq = " ", $empswap = " ", $avail = " ")
	// {

	// 	$action = \DB::select("CALL View_DetailScheduleRequest($id, $type_id,'$empReq','$empswap','$avail')");
	// 	if($type_id == 8){

	// 	    if($action[0]->time_in != '-' and  $action[0]->time_out !=  '-' || $action[0]->time_in != null and  $action[0]->time_out	 !=  null){

	// 		$schedule =   explode(' ',$action[0]->schedule);
	// 		$fromx = $schedule[2].':00';
	// 		$tox  =  $schedule[4].':00';
	// 		$timeIn  = $action[0]->time_in.':00';
	// 		$timeOut  =  $action[0]->time_out.':00';


	// 		//return  $arr=[$timeIn,$fromx];
	// 		if(strtotime($timeIn) < strtotime($fromx)){
	// 			$workhours  =  date('H:i',strtotime($timeOut) - strtotime($fromx));
	// 		}else{
	// 			$workhours  =  date('H:i',strtotime($timeOut) - strtotime($timeIn));
	// 		}

	// 			$workhours .=  ':00';

	// 		if(strtotime($workhours) > strtotime(date('H:i',(strtotime($tox) -  strtotime($fromx)))) )
	// 		      {  $Eout =  0; }
	// 		else
	// 		      {

	// 			//$time1 =  substr(($query[$key]->schedule),6,5).':00';
	// 			//return $arr = [$tox,$timeOut];
	// 			if(strtotime($tox) < strtotime($timeOut)){
	// 				$Eout =  0;
	// 			}else{


	// 				 $Eout = date('H:i', strtotime($tox) - strtotime($timeOut));
	// 		       }

	// 		}

	// 	      if($Eout!=  null){
	// 		$explode_eout = explode(':',$Eout);
	// 		$eout_h = intval($explode_eout[0])  * 60;
	// 	      	$eout_m = $explode_eout[1];

	// 		$Eout = $eout_h + $eout_m;
	// 	      }
	// 	      $action[0]->earlyOut  =  $Eout;
	// 	   }
	// 	}

	// 	/*if($type_id == 3){
	// 		$getLast = count($action);
	// 		$action = [$action[$getLast-1]];
	// 	}*/
	// 	$pool  =  \DB::SELECT("select * from pool_request where employee_id =  '$empReq' and id_req = $id and master_type = 1 ");
	// 	$type_ids = $pool[0]->type_id;
	// 	/*if($action[0]->approval_name == null and  $action[0]->approver_name ==  null ){

	// 		$json =  json_decode($pool[0]->json_data,1);
	// 		$flow = $json['req_flow'];
	// 		$nil =  [];
	// 		$nil_key  = '';

	// 		foreach ($flow as $key => $value) {
	// 			if($value == 1){
	// 				foreach ($json[$key] as $x => $z) {
	// 					$final =  $json[$key][$x];
	// 					foreach ($final as $a=> $b) {
	// 						if(is_numeric($a)){
	// 							if($empID  == $a){
	// 								$action[0]->can_approve  = 'y';
	// 								$action[0]->can_reject   = 'y';
	// 								$action[0]->can_cancel   = 'n';
	// 							}
	// 						}
	// 					}
	// 				}
	// 		    }
	// 		}

	// 		if(!isset($action[0]->can_approve)){
	// 			if($empID  == $empReq){
	// 				$action[0]->can_approve  = 'n';
	// 				$action[0]->can_reject   = 'n';
	// 				$action[0]->can_cancel   = 'y';
	// 			}else{
	// 				$action[0]->can_approve  = 'n';
	// 				$action[0]->can_reject   = 'n';
	// 				$action[0]->can_cancel   = 'n';
	// 			}
	// 		}
	// 	}
	// 	else if($action[0]->approval_name != null and  $action[0]->approver_name ==  null ){
	// 		$json =  json_decode($pool[0]->json_data,1);
	// 		$flow = $json['req_flow'];
	// 		$nil =  [];
	// 		$nil_key  = '';

	// 		//print_r($action);
	// 		if($action[0]->status_id  != 2 && $action[0]->status_id  != 1){
	// 			$action[0]->can_approve  = 'n';
	// 			$action[0]->can_reject   = 'n';
	// 			$action[0]->can_cancel   = 'n';
	// 		}else{
	// 			foreach ($flow as $key => $value) {
	// 				if($value == 2){
	// 					foreach ($json[$key] as $x => $z) {
	// 					$final =  $json[$key][$x];
	// 						foreach ($final as $a=> $b) {
	// 							if(is_numeric($a)){
	// 								if($empID  == $a){
	// 									$action[0]->can_approve  = 'y';
	// 									$action[0]->can_reject   = 'y';
	// 									$action[0]->can_cancel   = 'n';
	// 								}
	// 							}
	// 						}
	// 					}
	// 			    }
	// 			}

	// 			if(!isset($action[0]->can_approve)){
	// 				if($token  == $empReq){
	// 					$action[0]->can_approve  = 'n';
	// 					$action[0]->can_reject   = 'n';
	// 					$action[0]->can_cancel   = 'y';
	// 				}else{
	// 					$action[0]->can_approve  = 'n';
	// 					$action[0]->can_reject   = 'n';
	// 					$action[0]->can_cancel   = 'n';
	// 				}
	// 			}
	// 		}
	// 	}else if($action[0]->approval_name != null and  $action[0]->approver_name !=  null){
	// 			$action[0]->can_approve  = 'n';
	// 			$action[0]->can_reject   = 'n';
	// 			$action[0]->can_cancel   = 'n';
	// 	}elseif($action[0]->approval_name == null and  $action[0]->approver_name !=  null){
	// 			$action[0]->can_approve  = 'n';
	// 			$action[0]->can_reject   = 'n';
	// 			$action[0]->can_cancel   = 'n';
	// 	}else{
	// 		//KUDOS
	// 	}*/

	// 	$icd  =  $action[0]->id;

	//    	$get_app_user =  /*\DB::SELECT("select * from  pool_request where id_req = $id and  master_type =  1  ")[0]->*/ $pool[0]->json_data;

	//    	/*
	//    	manipulate json  from pool request
	//    	 */

	//    $json_x  = json_decode($get_app_user,1);
	//    $json = $json_x;
	//    $flow_approval = [];
	//    $expat = false;
	//    $flows = $json['req_flow'];
	//    if(isset($json['local_it'])){
	// 		if($json['local_it'] == 'expat'){ $expat = true; }
	// 	}

	// 	$compare_times = ["sup"=>null,"hr"=>null,"employee"=>null];

	// 	foreach ($flows as $keys => $values) {
	// 		if($values != 0 && $values != "o" && $keys != 'employee_dates' && $keys != 'employee_times' && $keys != 'employee_requestor'){

	// 			$arr = ["job_approval"=>strtoupper($keys),"name"=>null,"date"=>null,"time"=>null,"status"=>'Pending'];

	// 			if($keys == 'employee'){
	// 				if($flows['employee_approve'] == 'x'){

	// 					$empids = $flows['employee'];
	// 					//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$empids' ")[0]->name_approval;
	// 					$select_emp = \DB::SELECT("CALL find_approval('$empids')")[0]->name_approval;
	// 					$arr['name'] = $select_emp;
	// 					$arr['date'] = $flows['employee_dates'];
	// 					$arr['time'] = strtoupper($flows["employee_times"]);
	// 					$arr['status'] = 'Approval';
	// 					$compare_times['employee'] = strtotime(date($flows['employee_dates']." ".explode(" ",$flows["employee_times"])[0]));

	// 				}else if($flows['employee_approve'] == 'v'){
	// 					$empids = $flows['employee'];
	// 					//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$empids' ")[0]->name_approval;
	// 					$select_emp = \DB::SELECT("CALL find_approval('$empids')")[0]->name_approval;
	// 					$arr['name'] = $select_emp;
	// 					$arr['date'] = $flows['employee_dates'];
	// 					$arr['time'] = strtoupper($flows["employee_times"]);
	// 					$arr['status'] = 'Reject';
	// 					$compare_times['employee'] = strtotime(date($flows['employee_dates']." ".explode(" ",$flows["employee_times"])[0]));
	// 				}
	// 				else if($flows[$keys."_approve"]=="c"){

	// 					foreach ($json[$keys] as $nm_idx => $values_nm) {
	// 						if($json[$keys][$nm_idx][$keys."_stat"] == 1){
	// 							$keyx = key($json[$keys][$nm_idx]);

	// 							//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
	// 							$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;
	// 							$arr['name'] = $select_emp;
	// 							$arr['date'] = $json[$keys][$nm_idx][$keys."_date"];
	// 							$arr['time'] = strtoupper($json[$keys][$nm_idx][$keys."_time"]);
	// 							$compare_times[$keys] = strtotime(date($json[$keys][$nm_idx][$keys."_date"]." ".explode(" ",$json[$keys][$nm_idx][$keys."_time"])[0]));
	// 							$arr['status'] = 'Cancel';

	// 						}
	// 					}
	// 				}
	// 			}else{
	// 				try{
	// 					if($flows[$keys."_approve"]=="x"){

	// 						foreach ($json[$keys] as $nm_idx => $values_nm) {
	// 							if($json[$keys][$nm_idx][$keys."_stat"] == 1){
	// 								$keyx = key($json[$keys][$nm_idx]);

	// 								//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
	// 								$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;
	// 								$arr['name'] = $select_emp;
	// 								$arr['date'] = $json[$keys][$nm_idx][$keys."_date"];
	// 								$arr['time'] = strtoupper($json[$keys][$nm_idx][$keys."_time"]);
	// 								$compare_times[$keys] = strtotime(date($json[$keys][$nm_idx][$keys."_date"]." ".explode(" ",$json[$keys][$nm_idx][$keys."_time"])[0]));
	// 								$arr['status'] = 'Approval';

	// 							}
	// 						}
	// 					}else if($flows[$keys."_approve"]=="v"){

	// 						foreach ($json[$keys] as $nm_idx => $values_nm) {
	// 							if($json[$keys][$nm_idx][$keys."_stat"] == 1){
	// 								$keyx = key($json[$keys][$nm_idx]);

	// 								//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
	// 								$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;
	// 								$arr['name'] = $select_emp;
	// 								$arr['date'] = $json[$keys][$nm_idx][$keys."_date"];
	// 								$arr['time'] = strtoupper($json[$keys][$nm_idx][$keys."_time"]);
	// 								$compare_times[$keys] = strtotime(date($json[$keys][$nm_idx][$keys."_date"]." ".explode(" ",$json[$keys][$nm_idx][$keys."_time"])[0]));
	// 								$arr['status'] = 'Reject';

	// 							}
	// 						}
	// 					}
	// 					else if($flows[$keys."_approve"]=="c"){

	// 						foreach ($json[$keys] as $nm_idx => $values_nm) {
	// 							if($json[$keys][$nm_idx][$keys."_stat"] == 1){
	// 								$keyx = key($json[$keys][$nm_idx]);

	// 								//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
	// 								$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;
	// 								$arr['name'] = $select_emp;
	// 								$arr['date'] = $json[$keys][$nm_idx][$keys."_date"];
	// 								$arr['time'] = strtoupper($json[$keys][$nm_idx][$keys."_time"]);
	// 								$compare_times[$keys] = strtotime(date($json[$keys][$nm_idx][$keys."_date"]." ".explode(" ",$json[$keys][$nm_idx][$keys."_time"])[0]));
	// 								$arr['status'] = 'Cancel';

	// 							}
	// 						}
	// 					}
	// 				}catch(\Exception $e){

	// 				}
	// 			}

	// 			$flow_approval[] = $arr;
	// 		}
	// 	}
	// 	$requestor = null;

	// 	if(isset($json['req_flow']['employee_requestor'])){
	// 		if($json['req_flow']['employee_requestor'][1] != 'superuser' && $json['req_flow']['employee_requestor'][0] != '2014888'){
	// 			$requestor_job = strtoupper($json['req_flow']['employee_requestor'][1]);
	// 			for ($i=0; $i < count($flow_approval); $i++) {
	// 				if($flow_approval[$i]['job_approval'] == $requestor_job){
	// 					if(isset($json['local_it'])){
	// 						if($json['local_it'] == "expat"){
	// 							if(strtolower($flow_approval[$i]['job_approval']) == 'hr'){
	// 								$requestor = $flow_approval[$i]['name']." (IT DIRECTOR)";
	// 							}else{
	// 								$requestor = $flow_approval[$i]['name']." (".$flow_approval[$i]['job_approval'].")";
	// 							}
	// 						}else{
	// 							$requestor = $flow_approval[$i]['name']." (".$flow_approval[$i]['job_approval'].")";
	// 						}
	// 					}else{
	// 						$requestor = $flow_approval[$i]['name']." (".$flow_approval[$i]['job_approval'].")";
	// 					}
	// 					if($type_id == 9 && strtolower($flow_approval[$i]['job_approval']) == 'hr'){
	// 						$hr_approve = [$flow_approval[$i],$i];
	// 					}
	// 					unset($flow_approval[$i]);
	// 				}
	// 			}
	// 		}else{
	// 			$idsu = $json['req_flow']['employee_requestor'][0];
	// 			$select_emp  	= \Db::SELECT("select distinct concat(IFNULL(emp.first_name,''),'',IFNULL(emp.middle_name,' '),'',IFNULL(emp.last_name,'')) as name_approval, role.role_name from emp, role, ldap where emp.employee_id =  '$idsu' and ldap.employee_id='$idsu' and role.role_id = ldap.role_id ")[0];

	// 			$requestor 		=  $select_emp->name_approval." (".$select_emp->role_name.")";
	// 		}
	// 	}else{
	// 		//if($compare_times['sup'] > $compare_times['hr']){ $sub = "SUP"; }else{ $sub = "HR"; }
	// 		//$requestor_job = $sub;
	// 		//$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;

	// 		for ($i=0; $i < count($flow_approval); $i++) {
	// 			if($flow_approval[$i]['job_approval'] == 'EMPLOYEE'){
	// 				$requestor = $flow_approval[$i]['name']." (".$flow_approval[$i]['job_approval'].")";
	// 				unset($flow_approval[$i]);
	// 			}
	// 			if(in_array($type_id, [7,8,9])){ // only HR
	// 				if($flow_approval[$i]['job_approval'] == 'SUP'){
	// 					unset($flow_approval[$i]);
	// 				}
	// 			}else if(in_array($type_id, [1,2,3,4,5,6])){ // NOT HR
	// 				if($flow_approval[$i]['job_approval'] == 'HR' || $flow_approval[$i]['job_approval'] == 'IT DIRECTOR'){
	// 					unset($flow_approval[$i]);
	// 				}
	// 			}
	// 		}

	// 	}

	// 	if(isset($hr_approve)){
	// 		unset($flow_approval[1]);
	// 		$flow_approval = [$hr_approve[0]];
	// 	}
	// 	$action[0]->flow_approval = $flow_approval;


	// 	/*	if($requestor){
	// 		$action[0]->requestBy = $requestor;
	// 	}else{
	// 		if(!isset($json['req_flow']['employee_requestor'])){
	// 			$select_emp = \DB::SELECT("CALL find_approval('$empReq')")[0]->name_approval;
	// 			$action[0]->requestBy = "$select_emp ( EMPLOYEE )";
	// 		}
	// 	}*/

	// 	if($requestor){
	// 		if(isset($json['req_flow']['employee_requestor'][0])){
	// 			$emp_request = $json['req_flow']['employee_requestor'][0];

	// 			$select_emp = \DB::SELECT("CALL find_approval('$emp_request')")[0]->name_approval;
	// 			$pos = strtoupper($json['req_flow']['employee_requestor'][1]);
	// 			$requestor = "$select_emp ( $pos )";
	// 		}
	// 		$action[0]->requestBy = $requestor;
	// 	}else{
	// 		if(!isset($json['req_flow']['employee_requestor'])){
	// 			$select_emp = \DB::SELECT("CALL find_approval('$empReq')")[0]->name_approval;
	// 			$action[0]->requestBy = "$select_emp ( EMPLOYEE )";
	// 		}else{
	// 			$emp_request = $json['req_flow']['employee_requestor'][0];

	// 			$select_emp = \DB::SELECT("CALL find_approval('$emp_request')")[0]->name_approval;
	// 			$pos = strtoupper($json['req_flow']['employee_requestor'][1]);
	// 			$action[0]->requestBy = "$select_emp ( $pos )";
	// 		}
	// 	}

	//    //check  leveling stat

	//    	foreach($json_x['req_flow'] as $key => $value)
	//    	{
	//    		$arrx[] =  $value;
	//    	}

	//   	if(in_array(2,$arrx)){
 //  			foreach($json_x['req_flow'] as $key => $value)
	// 	   	{
	// 	   		if($value  == 2 and $json_x['req_flow'][$key.'_approve'] ==  'x' ){
	// 	   			if(isset($json_x['action_2'])){

	// 					$date_c =  explode(' ',$json_x['action_2']['dateTime']);

	// 	   				$uix  =  $json_x['action_2']['employee_id'];
	// 	   				$db  = \DB::SELECT("select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as name from emp where employee_id = '$uix' ")[0]->name;
	// 	   				$action[0]->approver_name = $db;
	// 	   				$action[0]->approver_id = $uix;
	// 	   				$action[0]->approver_date  = $date_c[0];
	// 	   				$action[0]->approver_time =  $date_c[1];
	// 	   			}
	// 		   	}else{
	// 			    if(!isset($json_x['action_1']['dateTime'])){
	// 					$action[0]->approver_id  = null ;
	// 				   	$action[0]->approver_name = null;
	// 				   	$action[0]->approver_date  = null;
	// 		   			$action[0]->approver_time =  null;
	// 			    }else{
	// 			    	if(isset($json_x['action_1'])){
	// 						$date_c =  explode(' ',$json_x['action_1']['dateTime']);

	// 				   		$uix = $json_x['action_1']['employee_id'];
	// 				   		$db  = \DB::SELECT("select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as name from emp where employee_id = '$uix' ")[0]->name;
	// 				   		$action[0]->approval_id  =   $uix;
	// 				   		$action[0]->approval_name = $db;
	// 				   		$action[0]->aproval_date  = $date_c[0];
	// 		   				$action[0]->approval_time =  $date_c[1];
	// 			    	}
	// 			    }
	// 		   	}

	// 	   	}
	//   	}else{
	//   		foreach($json_x['req_flow'] as $key => $value)
	// 	   	{
	// 	   		if(!isset($json_x['action_1'])){
	// 	   			$action[0]->approver_id  = null ;
	// 			   	$action[0]->approver_name = null;
	// 			   	$action[0]->approver_date  = null;
	// 	   			$action[0]->approver_time =  null;
	// 	   		}else{
	// 	   			if(isset($json_x['action_1'])){
	// 			   		$date_c =  explode(' ',$json_x['action_1']['dateTime']);

	// 				   	$uix = $json_x['action_1']['employee_id'];
	// 				   	$db  = \DB::SELECT("select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as name from emp where employee_id = '$uix' ")[0]->name;
	// 				   	$action[0]->approver_id  = $uix ;
	// 				   	$action[0]->approver_name =   $db;
	// 				   	$action[0]->approver_date  = $date_c[0];
	// 		   			$action[0]->approver_time =  $date_c[1];
	// 	   			}

	// 	   		}
	// 	   	}
	//   	}

	//    	if($action[0]->approver_id !=  null){
	//    		$apr_id = $action[0]->approver_id;
	//    		$stat =  \Db::SELECT("select role_name from  view_nonactive_login where employee_id  = '$apr_id' ")[0]->role_name;
	//    		$action[0]->approver_stat = $stat;

	//    		$stat_explode  =  explode(' ',$stat);
	//    		if(in_array('user',$stat_explode)){
	//    			$action[0]->approver_stat = 'Supervisor';
	// 	   	}
	//    	}else{
	//    		$action[0]->approver_stat = null;
	//    	}

	//    	//if($catuion  )

	//    	if($action[0]->approval_id !=  null){
	//    		$apl_id = $action[0]->approval_id;
	//    		$stat =  \Db::SELECT("select role_name from  view_nonactive_login where employee_id  = '$apl_id' ")[0]->role_name;
	//    		$action[0]->approval_stat =  $stat;

	//    		$stat_explode  =  explode(' ',$stat);
	//    		if(in_array('user',$stat_explode)){
	//    			$action[0]->approver_stat = 'Supervisor';
	// 	   	}
	//    	}else{
	//    		$action[0]->approval_stat =  null;
	//    	}

	//    if($action[0]->id  !=  null){
	//    		$id  = $action[0]->id;
	//    		$findout  = \DB::select("select type_id,request_id from att_schedule_request where id = $id");
	//    		$req_id =  $findout[0]->request_id;
	//    		if($findout[0]->type_id  == 1 or $findout[0]->type_id  == 2){
	//    			$get_training  =   \DB::SELECT("select start_,end from att_training where id  =  $req_id");
	//    			$get_val_start = date_create($get_training[0]->start_);
	//    			$get_val_end   = date_create($get_training[0]->end);
	//    			$get_total_day  = date_diff($get_val_start,$get_val_end);
	//    			$action[0]->NumberOfDay = $get_total_day->d +1;
	//    		}else{
	//    			//KUDOS
	//    		}
	//    }

	// 	if($ex != null){
	// 		$com = \DB::SELECT("select command_center.id,command_center.filename,command_center.created_at,command_center.path,command_center.comment, concat(emp.first_name,' ',ifnull(emp.middle_name,''),' ',ifnull(emp.last_name,''))as employeeName   from command_center,emp where command_center.employee_id = '$empReq' and command_center.request_id=$id and emp.employee_id=command_center.employee_id and command_center.master_type=1 and command_center.type_id = $type_ids order by command_center.id ASC");

	// 		if(!isset($com) && $com == null){
	// 			$com = [];
	// 		}else{
	// 			foreach ($com as $key => $value) {
	// 				if($value->path != 'hrms_upload/timeinout'){
	// 					$com[$key]->path = null;
	// 					$com[$key]->filename = null;
	// 				}
	// 			}
	// 			//KUDOS
	// 		}
	// 	}

	// 	$action[0]->comment = $com;
	// 	return $action;
	// }

	public function editDetail($id, $type_id,$ex, $empID, $empReq = " ", $empswap = " ", $avail = " ")
	{

		$action = \DB::select("CALL View_DetailScheduleRequest($id, $type_id,'$empReq','$empswap','$avail')");
		
		if($type_id == 9){
			if(!isset($action[0]->date_in)){
				$action[0]->date_in = null;
			}else if(!isset($action[0]->date_out)){
				$action[0]->date_out = null;
			}
		}

		if($type_id == 8){

		    if($action[0]->time_in != '-' and  $action[0]->time_out !=  '-' || $action[0]->time_in != null and  $action[0]->time_out	 !=  null){

			$schedule =   explode(' ',$action[0]->schedule);
			$fromx = $schedule[2].':00';
			$tox  =  $schedule[4].':00';
			$timeIn  = $action[0]->time_in.':00';
			$timeOut  =  $action[0]->time_out.':00';


			//return  $arr=[$timeIn,$fromx];
			if(strtotime($timeIn) < strtotime($fromx)){
				$workhours  =  date('H:i',strtotime($timeOut) - strtotime($fromx));
			}else{
				$workhours  =  date('H:i',strtotime($timeOut) - strtotime($timeIn));
			}

				$workhours .=  ':00';

			if(strtotime($workhours) > strtotime(date('H:i',(strtotime($tox) -  strtotime($fromx)))) )
			      {  $Eout =  0; }
			else
			      {

				//$time1 =  substr(($query[$key]->schedule),6,5).':00';
				//return $arr = [$tox,$timeOut];
				if(strtotime($tox) < strtotime($timeOut)){
					$Eout =  0;
				}else{


					 $Eout = date('H:i', strtotime($tox) - strtotime($timeOut));
			       }

			}

		      if($Eout!=  null){
			$explode_eout = explode(':',$Eout);
			$eout_h = intval($explode_eout[0])  * 60;
		      	$eout_m = $explode_eout[1];

			$Eout = $eout_h + $eout_m;
		      }
		      $action[0]->earlyOut  =  $Eout;
		   }
		}

		/*if($type_id == 3){
			$getLast = count($action);
			$action = [$action[$getLast-1]];
		}*/
		$pool  =  \DB::SELECT("select * from pool_request where employee_id =  '$empReq' and id_req = $id and master_type = 1 ");

		$type_ids = $pool[0]->type_id;
		/*if($action[0]->approval_name == null and  $action[0]->approver_name ==  null ){

			$json =  json_decode($pool[0]->json_data,1);
			$flow = $json['req_flow'];
			$nil =  [];
			$nil_key  = '';

			foreach ($flow as $key => $value) {
				if($value == 1){
					foreach ($json[$key] as $x => $z) {
						$final =  $json[$key][$x];
						foreach ($final as $a=> $b) {
							if(is_numeric($a)){
								if($empID  == $a){
									$action[0]->can_approve  = 'y';
									$action[0]->can_reject   = 'y';
									$action[0]->can_cancel   = 'n';
								}
							}
						}
					}
			    }
			}

			if(!isset($action[0]->can_approve)){
				if($empID  == $empReq){
					$action[0]->can_approve  = 'n';
					$action[0]->can_reject   = 'n';
					$action[0]->can_cancel   = 'y';
				}else{
					$action[0]->can_approve  = 'n';
					$action[0]->can_reject   = 'n';
					$action[0]->can_cancel   = 'n';
				}
			}
		}
		else if($action[0]->approval_name != null and  $action[0]->approver_name ==  null ){
			$json =  json_decode($pool[0]->json_data,1);
			$flow = $json['req_flow'];
			$nil =  [];
			$nil_key  = '';

			//print_r($action);
			if($action[0]->status_id  != 2 && $action[0]->status_id  != 1){
				$action[0]->can_approve  = 'n';
				$action[0]->can_reject   = 'n';
				$action[0]->can_cancel   = 'n';
			}else{
				foreach ($flow as $key => $value) {
					if($value == 2){
						foreach ($json[$key] as $x => $z) {
						$final =  $json[$key][$x];
							foreach ($final as $a=> $b) {
								if(is_numeric($a)){
									if($empID  == $a){
										$action[0]->can_approve  = 'y';
										$action[0]->can_reject   = 'y';
										$action[0]->can_cancel   = 'n';
									}
								}
							}
						}
				    }
				}

				if(!isset($action[0]->can_approve)){
					if($token  == $empReq){
						$action[0]->can_approve  = 'n';
						$action[0]->can_reject   = 'n';
						$action[0]->can_cancel   = 'y';
					}else{
						$action[0]->can_approve  = 'n';
						$action[0]->can_reject   = 'n';
						$action[0]->can_cancel   = 'n';
					}
				}
			}
		}else if($action[0]->approval_name != null and  $action[0]->approver_name !=  null){
				$action[0]->can_approve  = 'n';
				$action[0]->can_reject   = 'n';
				$action[0]->can_cancel   = 'n';
		}elseif($action[0]->approval_name == null and  $action[0]->approver_name !=  null){
				$action[0]->can_approve  = 'n';
				$action[0]->can_reject   = 'n';
				$action[0]->can_cancel   = 'n';
		}else{
			//KUDOS
		}*/

		$icd  =  $action[0]->id;

	   	$get_app_user =  /*\DB::SELECT("select * from  pool_request where id_req = $id and  master_type =  1  ")[0]->*/ $pool[0]->json_data;

	   	/*
	   	manipulate json  from pool request
	   	 */

	   $json_x  = json_decode($get_app_user,1);
	   $json = $json_x;
	   $flow_approval = [];
	   $expat = false;
	   $flows = $json['req_flow'];
	   if(isset($json['local_it'])){
			if($json['local_it'] == 'expat'){ $expat = true; }
		}

		$compare_times = ["sup"=>null,"hr"=>null,"employee"=>null];
		if(!$flows){
			return ["select * from pool_request where employee_id =  '$empReq' and id_req = $id and master_type = 1 ",$pool];
		}
		foreach ($flows as $keys => $values) {
			if($values != 0 && $values != "o" && $keys != 'employee_dates' && $keys != 'employee_times' && $keys != 'employee_requestor'){

				$arr = ["job_approval"=>strtoupper($keys),"name"=>null,"date"=>null,"time"=>null,"status"=>'Pending'];

				if($keys == 'employee'){
					if($flows['employee_approve'] == 'x'){

						$empids = $flows['employee'];
						//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$empids' ")[0]->name_approval;
						$select_emp = \DB::SELECT("CALL find_approval('$empids')")[0]->name_approval;
						$arr['name'] = $select_emp;
						$arr['date'] = $flows['employee_dates'];
						$arr['time'] = strtoupper($flows["employee_times"]);
						$arr['status'] = 'Approval';
						$compare_times['employee'] = strtotime(date($flows['employee_dates']." ".explode(" ",$flows["employee_times"])[0]));

					}else if($flows['employee_approve'] == 'v'){
						$empids = $flows['employee'];
						//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$empids' ")[0]->name_approval;
						$select_emp = \DB::SELECT("CALL find_approval('$empids')")[0]->name_approval;
						$arr['name'] = $select_emp;
						$arr['date'] = $flows['employee_dates'];
						$arr['time'] = strtoupper($flows["employee_times"]);
						$arr['status'] = 'Reject';
						$compare_times['employee'] = strtotime(date($flows['employee_dates']." ".explode(" ",$flows["employee_times"])[0]));
					}
					else if($flows[$keys."_approve"]=="c"){

						foreach ($json[$keys] as $nm_idx => $values_nm) {
							if($json[$keys][$nm_idx][$keys."_stat"] == 1){
								$keyx = key($json[$keys][$nm_idx]);

								//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
								$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;
								$arr['name'] = $select_emp;
								$arr['date'] = $json[$keys][$nm_idx][$keys."_date"];
								$arr['time'] = strtoupper($json[$keys][$nm_idx][$keys."_time"]);
								$compare_times[$keys] = strtotime(date($json[$keys][$nm_idx][$keys."_date"]." ".explode(" ",$json[$keys][$nm_idx][$keys."_time"])[0]));
								$arr['status'] = 'Cancel';

							}
						}
					}
				}else{
					try{
						if($flows[$keys."_approve"]=="x"){

							foreach ($json[$keys] as $nm_idx => $values_nm) {
								if($json[$keys][$nm_idx][$keys."_stat"] == 1){
									
									$keyx = key($json[$keys][$nm_idx]);

									$req_emp = key ($json["requestor_stat"]);
									if($keyx != $req_emp){
										//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
										$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;
										$arr['name'] = $select_emp;
										$arr['date'] = $json[$keys][$nm_idx][$keys."_date"];
										$arr['time'] = strtoupper($json[$keys][$nm_idx][$keys."_time"]);
										$compare_times[$keys] = strtotime(date($json[$keys][$nm_idx][$keys."_date"]." ".explode(" ",$json[$keys][$nm_idx][$keys."_time"])[0]));
										$arr['status'] = 'Approval';
									}
									


								}
							}
						}else if($flows[$keys."_approve"]=="v"){

							foreach ($json[$keys] as $nm_idx => $values_nm) {
								if($json[$keys][$nm_idx][$keys."_stat"] == 1){
									$keyx = key($json[$keys][$nm_idx]);

									$req_emp = key ($json["requestor_stat"]);
									if($keyx != $req_emp){
									//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
									$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;
									$arr['name'] = $select_emp;
									$arr['date'] = $json[$keys][$nm_idx][$keys."_date"];
									$arr['time'] = strtoupper($json[$keys][$nm_idx][$keys."_time"]);
									$compare_times[$keys] = strtotime(date($json[$keys][$nm_idx][$keys."_date"]." ".explode(" ",$json[$keys][$nm_idx][$keys."_time"])[0]));
									$arr['status'] = 'Reject';
									}

								}
							}
						}
						else if($flows[$keys."_approve"]=="c"){

							foreach ($json[$keys] as $nm_idx => $values_nm) {
								if($json[$keys][$nm_idx][$keys."_stat"] == 1){
									$keyx = key($json[$keys][$nm_idx]);

									$req_emp = key ($json["requestor_stat"]);
									if($keyx != $req_emp){
									//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
									$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;
									$arr['name'] = $select_emp;
									$arr['date'] = $json[$keys][$nm_idx][$keys."_date"];
									$arr['time'] = strtoupper($json[$keys][$nm_idx][$keys."_time"]);
									$compare_times[$keys] = strtotime(date($json[$keys][$nm_idx][$keys."_date"]." ".explode(" ",$json[$keys][$nm_idx][$keys."_time"])[0]));
									$arr['status'] = 'Cancel';
									}

								}
							}
						}
					}catch(\Exception $e){
						
					}
				}

				$flow_approval[] = $arr;
			}
		}
		$requestor = null;

		if(isset($json['req_flow']['employee_requestor'])){
			if($json['req_flow']['employee_requestor'][1] != 'superuser' && $json['req_flow']['employee_requestor'][0] != '2014888'){
				$requestor_job = strtoupper($json['req_flow']['employee_requestor'][1]);
				for ($i=0; $i < count($flow_approval); $i++) {
					if($flow_approval[$i]['job_approval'] == $requestor_job){
						if(isset($json['local_it'])){
							if($json['local_it'] == "expat"){
								if(strtolower($flow_approval[$i]['job_approval']) == 'hr'){
									$requestor = $flow_approval[$i]['name']." (IT DIRECTOR)";
								}else{
									$requestor = $flow_approval[$i]['name']." (".$flow_approval[$i]['job_approval'].")";
								}
							}else{
								$requestor = $flow_approval[$i]['name']." (".$flow_approval[$i]['job_approval'].")";
							}
						}else{
							$requestor = $flow_approval[$i]['name']." (".$flow_approval[$i]['job_approval'].")";
						}
						if($type_id == 9 && strtolower($flow_approval[$i]['job_approval']) == 'hr'){
							$hr_approve = [$flow_approval[$i],$i];
						}
						unset($flow_approval[$i]);
					}
				}
			}else{
				$idsu = $json['req_flow']['employee_requestor'][0];
				$select_emp  	= \Db::SELECT("select distinct concat(emp.first_name,' ',IFNULL(emp.middle_name,''),' ',IFNULL(emp.last_name,'')) as name_approval, role.role_name from emp, role, ldap where emp.employee_id =  '$idsu' and ldap.employee_id='$idsu' and role.role_id = ldap.role_id ")[0];


				$requestor 		=  $select_emp->name_approval." (".$select_emp->role_name.")";
			}
		}else{
			//if($compare_times['sup'] > $compare_times['hr']){ $sub = "SUP"; }else{ $sub = "HR"; }
			//$requestor_job = $sub;
			//$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;

			for ($i=0; $i < count($flow_approval); $i++) {
				if($flow_approval[$i]['job_approval'] == 'EMPLOYEE'){
					$requestor = $flow_approval[$i]['name']." (".$flow_approval[$i]['job_approval'].")";
					unset($flow_approval[$i]);
				}
				if(in_array($type_id, [7,8/*,9*/])){ // only HR
					if($flow_approval[$i]['job_approval'] == 'SUP'){
						unset($flow_approval[$i]);
					}
				}else if(in_array($type_id, [1,2,3,4,5,6])){ // NOT HR
					if($flow_approval[$i]['job_approval'] == 'HR' || $flow_approval[$i]['job_approval'] == 'IT DIRECTOR'){
						unset($flow_approval[$i]);
					}
				}
			}

		}

		if(isset($hr_approve)){
			unset($flow_approval[1]);
			$flow_approval = [$hr_approve[0]];
		}
		$action[0]->flow_approval = $flow_approval;


		if($requestor){
			if(isset($json['req_flow']['employee_requestor'][0])){
				$emp_request = $json['req_flow']['employee_requestor'][0];

				$select_emp = \DB::SELECT("CALL find_approval('$emp_request')")[0]->name_approval;
				$pos = strtoupper($json['req_flow']['employee_requestor'][1]);
				$requestor = "$select_emp ( $pos )";
			}
			$action[0]->requestBy = $requestor;
		}else{
			if(!isset($json['req_flow']['employee_requestor'])){
				$select_emp = \DB::SELECT("CALL find_approval('$empReq')")[0]->name_approval;
				$action[0]->requestBy = "$select_emp ( EMPLOYEE )";
			}else{
				$emp_request = $json['req_flow']['employee_requestor'][0];

				$select_emp = \DB::SELECT("CALL find_approval('$emp_request')")[0]->name_approval;
				$pos = strtoupper($json['req_flow']['employee_requestor'][1]);
				$action[0]->requestBy = "$select_emp ( $pos )";
			}
		}

	   //check  leveling stat

	   	foreach($json_x['req_flow'] as $key => $value)
	   	{
	   		$arrx[] =  $value;
	   	}

	  	if(in_array(2,$arrx)){
  			foreach($json_x['req_flow'] as $key => $value)
		   	{
		   		if($value  == 2 and $json_x['req_flow'][$key.'_approve'] ==  'x' ){
		   			if(isset($json_x['action_2'])){

						$date_c =  explode(' ',$json_x['action_2']['dateTime']);

		   				$uix  =  $json_x['action_2']['employee_id'];
		   				$db  = \DB::SELECT("select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as name from emp where employee_id = '$uix' ")[0]->name;
		   				$action[0]->approver_name = $db;
		   				$action[0]->approver_id = $uix;
		   				$action[0]->approver_date  = $date_c[0];
		   				$action[0]->approver_time =  $date_c[1];
		   			}
			   	}else{
				    if(!isset($json_x['action_1']['dateTime'])){
						$action[0]->approver_id  = null ;
					   	$action[0]->approver_name = null;
					   	$action[0]->approver_date  = null;
			   			$action[0]->approver_time =  null;
				    }else{
				    	if(isset($json_x['action_1'])){
							$date_c =  explode(' ',$json_x['action_1']['dateTime']);

					   		$uix = $json_x['action_1']['employee_id'];
					   		$db  = \DB::SELECT("select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as name from emp where employee_id = '$uix' ")[0]->name;
					   		$action[0]->approval_id  =   $uix;
					   		$action[0]->approval_name = $db;
					   		$action[0]->aproval_date  = $date_c[0];
			   				$action[0]->approval_time =  $date_c[1];
				    	}
				    }
			   	}

		   	}
	  	}else{
	  		foreach($json_x['req_flow'] as $key => $value)
		   	{
		   		if(!isset($json_x['action_1'])){
		   			$action[0]->approver_id  = null ;
				   	$action[0]->approver_name = null;
				   	$action[0]->approver_date  = null;
		   			$action[0]->approver_time =  null;
		   		}else{
		   			if(isset($json_x['action_1'])){
				   		$date_c =  explode(' ',$json_x['action_1']['dateTime']);

					   	$uix = $json_x['action_1']['employee_id'];
					   	$db  = \DB::SELECT("select concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) as name from emp where employee_id = '$uix' ")[0]->name;
					   	$action[0]->approver_id  = $uix ;
					   	$action[0]->approver_name =   $db;
					   	$action[0]->approver_date  = $date_c[0];
			   			$action[0]->approver_time =  $date_c[1];
		   			}

		   		}
		   	}
	  	}

	   	if($action[0]->approver_id !=  null){
	   		$apr_id = $action[0]->approver_id;
	   		$stat =  \Db::SELECT("select role_name from  view_nonactive_login where employee_id  = '$apr_id' ")[0]->role_name;
	   		$action[0]->approver_stat = $stat;

	   		$stat_explode  =  explode(' ',$stat);
	   		if(in_array('user',$stat_explode)){
	   			$action[0]->approver_stat = 'Supervisor';
		   	}
	   	}else{
	   		$action[0]->approver_stat = null;
	   	}

	   	//if($catuion  )

	   	if($action[0]->approval_id !=  null){
	   		$apl_id = $action[0]->approval_id;
	   		$stat =  \Db::SELECT("select role_name from  view_nonactive_login where employee_id  = '$apl_id' ")[0]->role_name;
	   		$action[0]->approval_stat =  $stat;

	   		$stat_explode  =  explode(' ',$stat);
	   		if(in_array('user',$stat_explode)){
	   			$action[0]->approver_stat = 'Supervisor';
		   	}
	   	}else{
	   		$action[0]->approval_stat =  null;
	   	}

	   if($action[0]->id  !=  null){
	   		$id  = $action[0]->id;
	   		$findout  = \DB::select("select type_id,request_id from att_schedule_request where id = $id");
	   		$req_id =  $findout[0]->request_id;
	   		if($findout[0]->type_id  == 1 or $findout[0]->type_id  == 2){
	   			$get_training  =   \DB::SELECT("select start_,end from att_training where id  =  $req_id");
	   			$get_val_start = date_create($get_training[0]->start_);
	   			$get_val_end   = date_create($get_training[0]->end);
	   			$get_total_day  = date_diff($get_val_start,$get_val_end);
	   			$action[0]->NumberOfDay = $get_total_day->d +1;
	   		}else{
	   			//KUDOS
	   		}
	   }

		if($ex != null){
			$com = \DB::SELECT("select command_center.id,command_center.filename,command_center.created_at,command_center.path,command_center.comment, concat(emp.first_name,' ',ifnull(emp.middle_name,''),' ',ifnull(emp.last_name,''))as employeeName   from command_center,emp where command_center.request_id=$id and emp.employee_id=command_center.employee_id and command_center.master_type=1 and command_center.type_id = $type_ids order by command_center.id ASC");

			if(!isset($com) && $com == null){
				$com = [];
			}else{
				foreach ($com as $key => $value) {
					if(strtolower($com[$key]->path) == 'null'){
						$com[$key]->path = null;
						$com[$key]->filename = null;
					}
					// if($value->path != 'hrms_upload/timeinout'){
					// 	$com[$key]->path = null;
					// 	$com[$key]->filename = null;
					// }
				}
				//KUDOS
			}
		}

		$action[0]->comment = $com;
		return $action;
	}

	public function getUserRole($empID)
	{
		$hr = \DB::select("SELECT employee_id FROM ldap WHERE role_id = (SELECT role_id FROM role WHERE role_name = 'HR' OR role_name = 'hr' OR role_name = 'HRD' OR role_name = 'hrd') AND employee_id = '$empID'");
		if ($hr == null OR $hr == []){
			$supervisor = \DB::select("SELECT supervisor FROM emp_supervisor WHERE supervisor = '$empID'");
			if ($supervisor == null OR $supervisor == [])
				$role = "User";
			else
				$role = "Supervisor";
		}
		else if ($hr == null OR $hr == []){
			$supervisor = \DB::select("SELECT supervisor FROM emp_supervisor WHERE supervisor = '$empID'");
			if ($supervisor != null OR $supervisor != [])
				$role = "Supervisor";
			else
				$role = "User";
		}
		else if ($hr != null OR $hr != []){
			$supervisor = \DB::select("SELECT supervisor FROM emp_supervisor WHERE supervisor = '$empID'");
			if ($supervisor == null OR $supervisor == [])
				$role = "HR";
			else
				$role = "HR";
		}
		else{
			$role = "User";
		}
		return $role;
	}

	//
	public function noData()
	{
		$res = [
			'data'=> null,
			'status'=>500,
			'message'=> 'Can not update request'
		];
		return $res;
	}

	function  approve($id,$emp){

		$data = \DB::SELECT(" update att_schedule_request set status_id=2, approver='$emp' where id=$id ");
		if(isset($data)){
			return "success";
		}
	}

	function  reject($id,$emp){

		$data = \DB::SELECT(" update att_schedule_request set status_id=3, approver='$emp' where id=$id ");
		if(isset($data)){
			return "success";
		}
	}

	// UPDATE THE SCHEDULE REQUEST
	public function updateOvertime($i)
	{
		$status = $this->status($i['status']);
		$checkExist = \DB::select("CALL check_exist_emp_in_overtime('$i[requestBy]','$i[date]')");
		if (!$checkExist){
			$res = [
				'data'=> null,
				'status'=>500,
				'message'=> 'No schedule for employee '.$i['requestBy'].' on '.$i['date'].' found'
			];
			return $res;
		}
		else{
			$action = \DB::select("CALL Update_Overtime('$i[requestBy]', '$i[date]', '$i[comment]', $status)");
			$res = [
				'data'=> $action[0],
				'status'=>200,
				'message'=> 'Update '.$i['type'].' Request success'
			];
			return $res;
		}
	}
	public function updateUndertime($i)
	{
		$status = $this->status($i['status']);
		$checkExist = \DB::select("CALL check_exist_emp_in_undertime('$i[requestBy]','$i[date]')");
		if (!$checkExist){
			$res = [
				'data'=> null,
				'status'=>500,
				'message'=> 'No schedule for employee '.$i['requestBy'].' on '.$i['date'].' found'
			];
			return $res;
		}
		else{
			$action = \DB::select("CALL Update_Undertime('$i[requestBy]', '$i[date]', '$i[comment]', $status)");
			$res = [
				'data'=> $action[0],
				'status'=>200,
				'message'=> 'Update '.$i['type'].' Request success'
			];
			return $res;
		}
	}
	public function updateTraining($i)
	{
		$status = $this->status($i['status']);
		$checkExist = \DB::select("CALL check_exist_emp_in_training('$i[requestBy]','$i[date]')");
		if (!$checkExist){
			$res = [
				'data'=> null,
				'status'=>500,
				'message'=> 'No schedule for employee '.$i['requestBy'].' on '.$i['date'].' found'
			];
			return $res;
		}
		else{
			$action = \DB::select("CALL Update_Training('$i[requestBy]', '$i[date]', $status, '$i[comment]')");
			$res = [
				'data'=> $action[0],
				'status'=>200,
				'message'=> 'Update '.$i['type'].' Request success'
			];
			return $res;
		}
	}
	public function updateChange($i)
	{
		$status = $this->status($i['status']);
		$checkExist = \DB::select("CALL check_exist_emp_in_change('$i[requestBy]','$i[date]')");
		if (!$checkExist){
			$res = [
				'data'=> null,
				'status'=>500,
				'message'=> 'No schedule for employee '.$i['requestBy'].' on '.$i['date'].' found'
			];
			return $res;
		}
		else{
			$action = \DB::select("CALL Update_ChangeShift('$i[requestBy]', '$i[date]', $status, '$i[comment]')");
			$res = [
				'data'=> $action[0],
				'status'=>200,
				'message'=> 'Update '.$i['type'].' Request success'
			];
			return $res;
		}
	}

	public function updateSwap($i)
	{
		$status = $this->status($i['status']);
		$checkExist = \DB::select("CALL check_exist_emp_in_swap('$i[requestBy]','$i[date]')");
		if (!$checkExist){
			$res = [
				'data'=> null,
				'status'=>500,
				'message'=> 'No schedule for employee '.$i['requestBy'].' on '.$i['date'].' found'
			];
			return $res;
		}
		else{
			$action = \DB::select("CALL Update_SwapShift('$i[requestBy]', '$i[date]', $i[status], '$i[comment]')");
			$res = [
				'data'=> $action[0],
				'status'=>200,
				'message'=> 'Update '.$i['type'].' Request success'
			];
			return $res;
		}
	}

	public function updateTimein($i)
	{
		$status = $this->status($i['status']);
		$checkExist = \DB::select("CALL check_exist_emp_in_timein('$i[requestBy]','$i[date]')");
		if (!$checkExist){
			$res = [
				'data'=> null,
				'status'=>500,
				'message'=> 'No schedule for employee '.$i['requestBy'].' on '.$i['date'].' found'
			];
			return $res;
		}
		else{
			$action = \DB::select("CALL Update_Timein('$i[requestBy]', '$i[date]', $i[status], '$i[comment]')");
			$res = [
				'data'=> $action[0],
				'status'=>200,
				'message'=> 'Update '.$i['type'].' Request success'
			];
			return $res;
		}
	}

	public function updateLate($i)
	{
		$status = $this->status($i['status']);
		$checkExist = \DB::select("CALL check_exist_emp_in_late('$i[requestBy]','$i[date]')");
		if (!$checkExist){
			$res = [
				'data'=> null,
				'status'=>500,
				'message'=> 'No schedule for employee '.$i['requestBy'].' on '.$i['date'].' found'
			];
			return $res;
		}
		else{
			$action = \DB::select("CALL Update_Late('$i[requestBy]', '$i[date]', $i[status], '$i[comment]')");
			$res = [
				'data'=> $action[0],
				'status'=>200,
				'message'=> 'Update '.$i['type'].' Request success'
			];
			return $res;
		}
	}
}
