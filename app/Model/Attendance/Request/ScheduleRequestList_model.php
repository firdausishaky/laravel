<?php namespace Larasite\Model\Attendance\Request;

use Illuminate\Database\Eloquent\Model;

class ScheduleRequestList_model extends Model {

	// VIEW SCHEDULE REQUEST LIST
	public function viewList($i,$empID,$status,$dep,$reqType,$include)
	{

		// return $temp = [$i['from'], $i['to'], $status, $i['employee_id'], $dep, $reqType['type_id']];
		if(!isset($status) || $status == null){
			$status = 0;
		}
		if(!isset($i['employee_id']) || $i['employee_id'] == null){
			$i['employee_id'] = '';
		}
		if(!isset($reqType['type_id']) || $reqType['type_id'] == null){
			$reqType['type_id'] = 0;
		}
		if(!isset($dep) || $dep == null){
			$dep = 0;
		}
		if(!isset($include) || $include == null){
			$include = 0;
		}
		
		//var_dump([$i['from'],$i['to'], $status,$i['employee_id'], $dep, $reqType['type_id'],$include]);
		//$viewRequest = \DB::select("CALL View_Schedule_Request_List('$i[from]', '$i[to]', $status, '$i[employee_id]', $dep, $reqType[type_id],$include)");
		
		$query  = "select t1.id,t1.date_request,t1.availment_date,t6.employee_id as empreq,t6.swap_with as empswap,
				concat(t2.first_name,' ',t2.middle_name,' ',t2.last_name)as employee,t1.employee_id,
				t3.type_id,t3.type,t1.status_id,t4.status,t1.request_id,
				date_format(t1.update_at,'%Y-%m-%d') as date_, 
				date_format(t1.update_at,'%H:%s') as time_,t1.approval,t1.approver
				from att_schedule_request t1
				left join emp t2 on t2.employee_id=t1.employee_id
				left join att_type t3 on t3.type_id=t1.type_id
				left join att_status t4 on  t4.status_id=t1.status_id
				left join att_swap_shift t6 on t6.type_id=t1.type_id and t6.date=t1.availment_date";

		if($status != 0){
			$query .=  " and t1.status_id = ".$status;
		}

		if($i['employee_id'] != ''){
			$query .=  " and t2.employee_id = ".$i['employee_id'];
		}

		if($dep !=  0){
			$query .= " and  t2.department = ".$dep;
		}

		if($reqType['type_id'] != 0){
			$query .= " and t1.type_id = ".$reqType['type_id']; 
		}

		if($include != 0){
			$query .= " and t1.employee_id not in(select employee_id from emp_termination_reason)";
		}

		
		$viewRequest = \DB::SELECT($query);
		$empRole = $this->checkRole($empID);
		$userLoggedinName = $this->getName($empID);
		if (!$viewRequest){
			$temp = null;
		}
		else{
			// $rl = ['employee_role'=>$empRole];
			if (count($viewRequest) > 0){
				for ($i=0; $i < count($viewRequest); ++$i) {
					$viewRequest[$i]->employee_role = $empRole;
					$viewRequest[$i]->user_login = $userLoggedinName[0]->name;
				}
			}
			$temp = $viewRequest;
		}
		
		return $temp;
	}

	function myfunction($value,$key)
	{
		array_push($value, $key);
	}
	// GET CURENT LOGGED IN USER DEPARTMENT
	public function getEmpDepartment($id)
	{
		$dep = \DB::select("CALL view_department_by_employee_id('$id')");
		if ($dep == null){
			$res = 'All';
		}
		else{
			$res = ['id'=>$dep[0]->id, 'name'=>$dep[0]->name];
		}
		return $res;
	}
	// CHECK CURRENT LOGGED IN USER ROLE
	// public function checkRole($id)
	// {
	// 	$spv = \DB::select("SELECT supervisor FROM emp_supervisor WHERE supervisor = '$id'");
	// 	$hr = \DB::select("select * from view_nonactive_login where employee_id  = '$id' and (role_name  like '%hr%' or role_name  like '%human resource%' or role_name  like '%human resources%' or role_name  like '%hrd%')");
	// 	$adminex = \DB::SELECT("select role.role_name from ldap,role where ldap.employee_id=$id and ldap.role_id=role.role_id ");
	// 	if ($spv != null){
	// 		if(count($hr) > 0){
	// 			$role = "HRD";	
	// 		}else{
	// 			$role = "Supervisor";
	// 		}
	// 	}else if($hr != null){
	// 		$role = "HRD";
	// 	}elseif($adminex[0]->role_name == "adminEx" || $adminex[0]->role_name == "admin" || $adminex[0]->role_name == "adminIt new" ){
	// 		$role ="admin";
	// 	}elseif($adminex[0]->role_name == "Admin-Correct" || $adminex[0]->role_name == "adminEx" ){
	// 		$role ="admin_x";
	// 	}elseif($adminex[0]->role_name == "SUPERUSER"){
	// 		$role ="SUPERUSER";
	// 	}else{
	// 		$role = "User";
	// 	}
	// 	return $role;
	// }

	public function checkRole($id)
	{
		$spv = \DB::select("SELECT supervisor FROM emp_supervisor WHERE supervisor = '$id'");
		$hr = \DB::select("select * from view_nonactive_login where employee_id  = '$id' and (role_name  like '%hr%' or role_name  like '%human resource%' or role_name  like '%human resources%' or role_name  like '%hrd%')");
		$adminex = \DB::SELECT("select role.role_name from ldap,role where ldap.employee_id=$id and ldap.role_id=role.role_id ");

		if(count($adminex) > 0){
			$role_name = strtolower($adminex[0]->role_name);

			if(strstr($role_name, 'human') || strstr($role_name, 'hr')){
				$role = "HRD";
			}elseif (strstr($role_name, 'admin')) {
				$role ="admin";	
			}elseif (strstr($role_name, 'superuser')) {
				$role ="SUPERUSER";	
			}elseif (strstr($role_name, 'reguler employee user')) {
				$role ="User";	
			}
			else{
				if(count($spv) > 0){
					$role = "Supervisor";
				}else{
					$role = $role_name;
				}
			}
		}

		// if ($spv != null){
		// 	if(count($hr)>0){
		// 		$role = "HRD";
		// 	}else{
		// 		$role = "Supervisor";
		// 	}
		// 	//$role = "Supervisor";
		// }else if($hr != null){
		// 	$role = "HRD";
		// }elseif($adminex[0]->role_name == "adminEx" || $adminex[0]->role_name == "admin" || $adminex[0]->role_name == "adminIt new" ){
		// 	$role ="admin";
		// }elseif($adminex[0]->role_name == "Admin-Correct" || $adminex[0]->role_name == "adminEx" ){
		// 	$role ="admin_x";
		// }elseif($adminex[0]->role_name == "SUPERUSER"){
		// 	$role ="SUPERUSER";
		// }else{
		// 	$role = "User";
		// }
		return $role;
	}
	// GET CURRENT LOGGED IN USER NAME
	public function getName($empID)
	{
		$empName = \DB::select("CALL search_employee_by_id('$empID')");
		return $empName;
	}
	// GET DEPARTMENT LIST
	public function departmentList($id)
	{
		$empDep = $this->getEmpDepartment($id);
		if ($empDep['id'] == 126){
			if ($this->checkRole($id) == "HRD"){
				$department = \DB::select("SELECT id,name FROM department WHERE id != 1");
				$temp = ['id'=>0, 'name'=>'All'];
				array_unshift($department, $temp);
			}elseif($this->checkRole($id) == "admin"){
					$department = \DB::SELECT("select department.name,department.id from department,emp where emp.employee_id='$id' and emp.department=department.id ");
			}elseif($this->checkRole($id) == "admin_x"){
					$department = \DB::SELECT("select department.name,department.id from department,emp where emp.employee_id='$id' and emp.department=department.id ");
			}
			else{
				$department = [$empDep];
			}
		}
		else{
			$department = [$empDep];
		}
		return $department;
	}
	// GET REQUEST TYPE LIST
	public function requestList()
	{
		$request = \DB::select("SELECT type_id,type FROM att_type");
		$temp = ['type_id'=>0, 'type'=>'All'];
		array_unshift($request, $temp);
		return $request;
	}

	function getMessage($message = array(), $data = array(), $status_code = ""){
			return response()->json(['header' => ['message' => $message, 'status' => $status_code], 'data' => $data], $status_code);
		}

	function index($query = array()){
	if(!isset($query)){
		$message = 'failed to load data, query not found';
		$data = 'null';
		$status_code = 500;
		}else if($query != null){
			$message = "success";
			$data = $query;
			$status_code = 200;
		}else{
			$message = 'data not found';
			$data = 'null';
			$status_code = 200;
		}
	return $this->getMessage($message,$data,$status_code);
	}





}
