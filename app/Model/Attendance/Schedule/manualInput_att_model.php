<?php namespace  Larasite\Model\Attendance\Schedule;
 
use Illuminate\Database\Eloquent\Model;
 
class manualInput_att_model extends Model {
 
    public function getMessage($message = array(), $status = "", $data =  array(), $access = array()){
        return response()->json(['header' => ['message' => $message, 'status' => $status, 'access' => $access], 'data' => $data],$status);
    }
 
    private $token = "";
    private $id_employee = "";
 
    public function key($key = ""){
        $keys= base64_decode($key);
        $key_ex = explode('-',$keys);
        return $key_ex[1];
    }
 
    public function index($db = array(),$access = array(),$access = array()){
        if($db == null){
            return $this->getMessage("Data not exist" ,200, [], $access);
        }else if($db != null){
                   return $this->getMessage("success", 200,$db, $access);
        }else{
            return $this->getMessage("failed to load " ,500, null, $access);
        }
    }
 
    public function search($validation= array(), $db= array(),  $access = array()){
        if($validation->fails()){
            $check = $validation->errors()->all();
            return $this->getMessage("invalidate input format" ,500, $check, $access);
        }else{
            return $this->index($db, $access);
        }
    }

    public function search_employee($employee = ""){
            $data = \DB::SELECT("CALL search_employee_like('$employee')");
            return  $data[0]->employee_id;
    }

    public function get($access = array()){
        $department = \DB::SELECT("select * from department where id > 1");
        $department_ex =  array_merge($department,[ ['id' =>0, "name" => 'all']]);
        $job_title = \DB::SELECT("select id,title from job");
        $job =  array_merge($job_title,[ ['id' =>0, "title" => 'all']]);
       // $shift = \DB::SELECT("call view_workshift");
       // foreach ($shift as $data => $value) {
       //         $shift_data[] = ['shift_id' => $value->shift_id,'shift_code' => $value->shift_code ];
       // }
	    $shift = \DB::SELECT("CALL view_workshift_fixed_schedule");
        foreach ($shift as $data => $value) {
                $shift_data[] = ['shift_id' => $value->shift_id,'shift_code' => $value->shift ];
        }
           $shift_idex =  array_merge($shift_data,[ ['shift_id' =>0, "shift_code" => 'all']]);
         if ($access['create'] == 0 and $access['delete'] == 0 and $access['read'] == 0 and $access['update'] == 0 ){

            $datax = ['department' =>$department_ex , 'job_title' => $job, 'shift' => $shift_idex];
            return $this->getMessage("Unauthorized",200,$datax, $access);
         }else{
             $datax = ['department' =>$department_ex , 'job_title' => $job, 'shift' => $shift_idex];
             return $this->getMessage("success",200,$datax, $access);
         }
               
       
    }
 
   
}