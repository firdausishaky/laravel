<?php  namespace  Larasite\Model\Attendance\cutOff;

use Illuminate\database\eloquent\Model;

class Attendance_cutoffrecordModel{

		private $employee_id = "";

		public function indexAcess($data = array(),$access = array()){
			if($data == null){
				return $this->getMessageAccess("Data not exist", 200, $data, $access);
			}else if($data != null){
				return $this->getMessageAccess("Success", 200, $data, $access);
			}else{
				return $this->getMessageAccess("Failed to load data", 500, $data, $access);
			}
		}

		public function getMessageAccess($message = array(), $status = array(), $data =  array(), $access = array()){
			return response()->json(['header' => ['message' => $message, 'status' => $status, 'access' => $access], 'data' => $data],$status);
		}

		function searchAcess($validator = array(),$data =  array(), $access = array()){
			if($validator->fails()){
				$check = $validator->errors()->all();
				return $this->getMessageAccess('Invalidate input format', 500, $check, $access);
			}else{
				return $this->indexAcess("success",200,$data,$access);
			}
			return $this->getMessage("input data undefined",500,null,$access); 
		}


		function  search_attendance_record($date1,$date2,$name,$jobs){
			return $data =  \DB::SELECT("CALL view_att_cutoff('$date1','$date2',$name,$jobs)");
		}

		function validation($data = array()){
			if($data->fails()){
				return $check = $data->errors->all();
			}else{
				return "true";
			}

		}

		function checkColor($data){
			foreach($data as $key => $value){

				$employee_id = $value['employee_id'];
				$date = $value['date'];
				$name =  $value['Name'];
				$schedule = $value['schedule'];
				$timein = $value['timeIn']; 
				$timeout = $value['timeOut'];
				
				$clock_front = strtotime(substr($schedule,0,5));
				$clock_back = strtotime(substr($schedule,6,5));
				
				$total_schedule_time = $clock_back-$clock_front;

				$time_In = strtotime($value['timeIn']);
				$time_out  = strtotime($value['timeOut']);


				$workhour = $value['WorkHours'];
				$hour_per_day = $value['WorkHours']; //hour per day
				$total_overtime = $value['total_overtime'];
				$overtime  = $value['OvertimeRestDay'];
				$short = $value['Short'];
				$late = $value['Late'];
				$early_out = $value['EarlyOut'];

				if($schedule == "00:00-00:00"){
					$schedule = "DO";
					$schedule_time = "green_block";
				}else{
					$schedule_time = "no";
				}

				if($schedule == "DO"){
					$schedule_time = "green_block";
				}

				if($time_In == null){
					$time_In_ex = "yes";
				}else{
					$time_In_ex = "no";
				}

				if($time_out == null){
					$time_out_ex = "yes";
				}else{
					$time_out_ex = "no";
				}
				if(($time_out - $time_In) > ($clock_back-$clock_front)){
					$workhour_ex = "yes";
				}else{
					$workhour_ex = "no";
				}

				if($total_overtime != null){
					$overtime_ex = "yes";
				}else{
					$overtime_ex = "no";
				}

				if($schedule != null || $time_In != null){
					$restday_ex = "yes";
				}elseif ($time_out != null) {
					$restday_ex = "yes";
				}else{
					$restday_ex = "no";
				}

				if(($time_out - $time_out) < ($clock_back-$clock_front)){
					$short_ex = "yes";
				}else{
					$short_ex = "no";
				}
				if($time_out > $clock_back){
					$late_ex = "yes";
				}else{
					$late_ex = "no";
				}
				if($time_out < $clock_back){
					$early_out_ex = "yes";
				}else{
					$early_out_ex = "no";
				}

				$data_ex[] = [
						"id" => $employee_id,
						"date" => ($date ==  null ? $date = "-" : $date = $date),
						"Name" => ($name == null ? $name = "-": $name= $name),
						"schedule_ex" => $schedule_time,
						"schedule" => ($schedule == null ? $schedule = "-" : $schedule = $schedule) ,
						"time_In_ex" => $time_In_ex,
						"timein" => ($timein == null ? $timein = "-" : $timein = $timein),
						"time_out_ex" => $time_out_ex,
						"time_out" => ($timeout == null ? $timeout = "-" : $timeout = $timeout),
						"workhour_ex" => $workhour_ex,
						"workhour" => ($workhour == null ? $workhour = "-" : $workhour = $workhour),
						"overtime_ex" => $overtime_ex,
						"overtime" => ($total_overtime == null ? $total_overtime = "- " : $total_overtime = $total_overtime),
						"restday_ex" => $restday_ex,
						"retsday" => ($overtime == null ? $overtime = "-" : $overtime = $overtime),
						"short_ex" => $short_ex,
						"short" => ($short == null ? $short = "-" : $short = $short),
						"late_ex" => $late_ex,
						"late"  => ($late == null ? $late = "-" : $late = $late),
						"early_out_ex" => $early_out_ex,
						"early_out" => ($early_out == null ? $early_out = "-" : $early_out = $early_out )
					];
				// $data_ex[] = ["data_check" => ["time_In" => $time_In, "time_out" => $time_out], "data_app" => [$value]];

			}

			return $data_ex;
			
		}

		



	}

