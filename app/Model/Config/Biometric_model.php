<?php  Larasite\Model\Config;
use Illuminate\Database\Eloquent\Model;

class Biometric_model{

	private	$rule = array(
					'device_location' => 'required| Regex:/^[a-zA-Z]+$/',
					'device_number' => 'required| integer',
					'device_name' => 'required|Regex:/^[a-zA-Z0-9]+$/',
					);
	

	function getMessage($message = array(), $data = array(), $status_code=""){
		return response()->json(['header' =>['message' => $message, 'status' => $status_code], 'data' => $data],$status_code);
	}

	function index($query = array()){
		if($query != null){
			$message = "success";
			$data = $query;
			$status_code = 200;
		}else if($query == null){
			$message = "data is empty";
			$data = "null";
			$status_code = 200;
		}else{
			$message = 'failed to load data';
			$data = 'null';
			$status_code = 500;
		}
		return $this->getMessage($message,$data,$status_code);
	} 

	function update($id){
		$input = \Input::all();
		$device_number = \Input::get('device_number');
		$device_location = \Input::get('device_location');
		$device_name = \Input::get('device_name');
       		$validation = \Validator::make($input,$this->rule);
       	
		if($validation->fails()){
			$message = $validation->errors()->all();
			$db = null;
			$status_code = 500;
			return $this->getMessage($message,$db,$status_code);
		}else{
	
			$check = \DB::SELECT(" select * from biometrics_location where device_number = $device_number  and  device_location='$device_location' and device_name='$device_name' ");
				if($check != null){
					return $this->getMessage('Device device_number already exist ',null,200);
				}else{
				$db = \DB::SELECT("CALL update_biometrics_location($id,'$device_location','$device_name')");
				if(isset($db)){
					$query = \DB::SELECT("select * from biometrics_location where device_number=$id");
					return $this->getMessage('success',$query[0],200);
				}
			}
		}
	}
	

	function store(){
		$input = \Input::all();
		$device_location = \Input::get('device_locations');
		$device_locations = \Input::get('device_location');
		$device_number = \Input::get('device_number');
      	
      	           $validation = \Validator::make($input,$this->rule);

		if($validation->fails()){
			$message = $validation->errors()->all();
			$db = null;
			$status_code = 500;
			return $this->getMessage($message,$db,$status_code);
		}else{
				$check = \DB::SELECT(" select * from biometrics_location where device_device_number = $device_number  and  device_location='$device_locations' an device_device_location='$device_location' ");
				if($check != null){
					return $this->getMessage('Device device_number already exist ',null,200);
				}else{
					$db = \DB::SELECT("CALL Insert_biometrics_Device_location($device_number,'$device_location',$location_id)");
					if(isset($db)){
						$query = \DB::SELECT("CALL View_BiometricDeviceLogin");
						return $this->getMessage('success',$query,200);
						break;
					}else{
						return $this->getMessage('failed',null,500);
						break;
					}
				}
			}
		}
	

	

	function destroy($query = "",$id){
		//created 4 january 2015
		$input = explode(",",$id);
		$count = count($input);

		$i = 0;
		foreach ($input as $data){
			\DB::select("CALL $query($data)");
			$i += 1;
		}
		if($i == $count){
			$message  = 'success';
			$status_code = 200;
			$data = 'null';
		}else{
			$message = 'failed';
			$status_code = 500;
			$data = 'null';
		}
		return $this->getMessage($message,[],$status_code);
	}


}