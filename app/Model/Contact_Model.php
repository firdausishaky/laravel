<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class Contact_Model extends Model {

	protected $table = 'emp';
	protected $hidden = ['created_at','updated_at','employee_id'];
	protected $guarded = ['id'];
	protected $fill = ['employee_id',
						'permanent_address',
						'permanent_city',
						'permanent_state',
						'permanent_zip',
						'permanent_country',
						'present_address',
						'present_city',
						'present_state',
						'present_zip',
						'present_country',
						'home_telephone',
						'mobile',
						'work_email',
						'personal_email'
					];

// READ
	public function ReadContact($id){
		if($id == 'undefined'){ $data = null; $message='ID undefined.'; $status=500;}
		else{
			$result = \DB::table('emp')->where('employee_id','=',$id)->get($this->fill);
			$nation = \DB::table('nationality')->get();
			foreach ($result as $key) {
				foreach ($nation as $key_nation) {
					if($key_nation->id == $key->permanent_country){ $key->permanent_country = $key_nation->title; }
					if($key_nation->id == $key->present_country){ $key->present_country = $key_nation->title; }
				}
				$a[] = $key;
			}
			if($a){ $data = $a; $message='Read Data.'; $status=200; }
			else{ $data = null; $message='ID not found.'; $status=200;}	
		}
		return ['data'=>$data,'message'=>$message,'status'=>$status]; 
	}
//END READ

	public function StoreContact($input,$id){
		//return $input; 
	  	if((!isset($input['work_email']) || $input['work_email'] == "") && (!isset($input['personal_email']) || $input['personal_email'] == "") ){
			$message =	'Update failed, work email  or personal email , must  filled one of option email';  		
			$status=500; $data=null;
	  	}else{
	  		$store = \DB::table($this->table)->where('employee_id','=',$id)->update($input);
		 	if($store){ $message='Update Successfully.'; $status=200; $data = $this->ReadContact($id); }
		 	else{ $message = 'Update Failed.'; $status=500; $data=null; }	
	  	}
		
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}

	public function UpdateContact($input,$id){
			$r = \DB::table()->where('employee_id','=',$id)->update($input);

			if(isset($r) != null){ $message ='Update Successfully.'; $status=200; }
			else{ $message = null; $status=500;}
		return ['message'=>$message,'status'=>$status];
	}

	public function GetID(){
		$data = \DB::select("Select id from contact order by id DESC limit 1");
		return $data;
	}

	/* GET TYPE ID ##################################################
	* PARAM ROLE USER
	*/
	public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}// END GET TYPE ################################################

}
