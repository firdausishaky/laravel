<?php namespace Larasite\Model;
//hrms
use Illuminate\Database\Eloquent\Model;

class CustomRole_Model extends Model {

protected $i=1;
protected $local = ['expat','local','all'];

	public function get_custome(){
		$get = \DB::select("select role_id, role_name from role where role_id > 32");
		return $get;
	}
	public function get_base(){
		$get = \DB::select("select role_id, role_name from role where role_id <= 32");
		return $get;
	}
	public function UpdateRole($id)
	{
		\DB::table('permissions')->where('role_id','=',$id)->delete();
		return true;
	}
	public function ReadCustome(){
		$data = array();
		$page = array();
		$act_add = null;
		$act_terminate = null;
		$accounting_role = \DB::select("select distinct c.role_id,a.role_name
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id <= 32  ");
		foreach ($accounting_role as $key1) {
			$accounting_form = \DB::select("select distinct c.form_id, d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and 
					c.role_id =  $key1->role_id ");
			foreach ($accounting_form as $key) {
				
				if($key->form_id == 48){ 
					if($key->create == 1){ $act_add = 1; }
					else{ $act_add = 0; } 
				}
				if($key->form_id == 49){ 
					if($key->create == 1){ $act_terminate = 1; }
					else{ $act_terminate = 0; } 
				}
					$value = ['create'=>$key->create,'read'=>$key->read,'update'=>$key->update,'delete'=>$key->delete];
					$page[] = ['form_id'=>$key->form_id,'form_name'=>$key->form_name,'action'=>$value];	
			}
			
			$data[] = array('role_id'=>$key1->role_id,'role_name'=>$key1->role_name,'add_employee'=>$act_add,'terminate'=>$act_terminate,'form'=>$page);	
		}
		if($data){
		return ['message'=>'View data.','data'=>json_encode($data),'status'=>200];	
		}else{ return ['message'=>'Data nof found.','status'=>404,'data'=>null]; }
		
	} 
	// READ ACTION
	private function ReadAction($role_id){
		$get = \DB::table('_action_emp')->where('role_id','=',$role_id)->get(['type_local']);
		if($get){
			foreach ($get as $key) { $local = $this->local[$key->type_local]; }
		}else{ $local = null; }
		return $local;
	}
	public function Read2Store($role_id){
		$data = array();
		$page = array();
		$act_add = null;
		$page_hidden = [48,49];
		$act_terminate = null;
		$local_action = $this->ReadAction($role_id);
		$accounting_role = \DB::select("select distinct c.role_id,a.role_name
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and
					c.role_id = $role_id");
		//return $accounting_role;
		foreach ($accounting_role as $key1) {
			$accounting_form = \DB::select("select distinct c.form_id, d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
					from role a, permissions c, form d
					where 
					c.role_id = a.role_id and
					c.form_id = d.form_id and 
					c.role_id =  $key1->role_id ");
			foreach ($accounting_form as $key) {
				
				if($key->form_id == 48 || $key->form_name == 'add.employee'){ 
					if($key->create == 1){ $act_add = 1; }
					else{ $act_add = 0; } 
				}
				if($key->form_id == 49 || $key->form_name == 'terminate.employee'){ 
					if($key->create == 1){ $act_terminate = 1; }
					else{ $act_terminate = 0; } 
				}
				if(!in_array($key->form_id,$page_hidden))
				{  
					$value = ['create'=>$key->create,'read'=>$key->read,'update'=>$key->update,'delete'=>$key->delete];
					$page[] = ['form_id'=>$key->form_id,'form_name'=>$key->form_name,'action'=>$value];
				}	
			}
			
			$data[] = array('role_id'=>$key1->role_id,'role_name'=>$key1->role_name,'add_employee'=>$act_add,'terminate'=>$act_terminate,'localit'=>$local_action,'form'=>$page);	
		}		
			return json_encode($data);
	}

/* END READ INDEX */

/* ---- STORE ---- */
	private function get_rolename($id){
		$get = \DB::select("select role_name from role where role_id = $id");
		foreach ($get as $key) {
			$name = $key->role_name;
		}
		return $name;
	}
	/* STORE ( jika semua sudah fix) */
	public function StoreCustome($data_json,$id_custome2,$type){
		$i=0; $j=1;
		if($type == 2){
			$id_custome = $id_custome2;	
		}else{ $id_custome = $id_custome2 + 1; }
		
		//$base_name = $this->get_rolename($id_custome2);
		if(!$data_json){ $status=500; $message = 'Data null.'; $data = null;}

			$StoreRole = ['role_id'=>$id_custome,'role_name'=>$data_json['role_name'],'active_role'=>1];			
			foreach ($data_json['form'] as $forms){
				$StorePermissions[] = ['role_id'=>$id_custome,'form_id'=>$forms->form_id,'create'=>$forms->action->create,
										'read'=>$forms->action->read,'update'=>$forms->action->update,'delete'=>$forms->action->delete];

				if($forms->form_id == 47){
					if($data_json['add_employee'] == 1){
						$StorePermissions[] = ['role_id'=>$id_custome,'form_id'=>48,'create'=>1,
											'read'=>1,'update'=>1,'delete'=>1];	
					}
					if($data_json['terminate'] == 1){
						$StorePermissions[] = ['role_id'=>$id_custome,'form_id'=>49,'create'=>1,
											'read'=>1,'update'=>1,'delete'=>1];	
					}
				}
			}
			$STORE2DB = ['role'=>$StoreRole,'action'=>['add_employee'=>$data_json['add_employee'],'terminate'=>$data_json['terminate']],'type_local'=>$data_json['localit'],'permissions'=>$StorePermissions];
			
			$roll = $data_json['role_name'];
			$check_date = \DB::SELECT("select role_name from role where role.role_name = '$roll' ");
			if($check_date != null && $type == 1){
				return ['data'=>[],'message'=>'role name aleady exist please create new name before submit again','status'=>500];
			}else{
				$Store_Role = $this->StoreCustomeRole($STORE2DB['role'],$id_custome);
				if($Store_Role && $Store_Role['status']==200){

					$Store_Permissions = $this->StoreCustomePermission($STORE2DB['permissions'],$id_custome);
					if($Store_Permissions && $Store_Permissions['status']==200){
						
						$this->action_emp($id_custome,$STORE2DB['type_local']);
						$Store_Action = $this->StoreCustomeAction($STORE2DB['action'],$id_custome);
						
						if($Store_Action && $Store_Action['status'] == 200 ){
								$message='Store Successfully.'; $status=200; $data = $id_custome;				
						}else{ $message='Store Failed : A .'; $status=500; $data=null; }
					}else{ $message='Store Failed : B .'; $status=500; $data=null; }
				}else{ $message='Store Failed : C .'; $status=500; $data=null; }
			}	
			
			
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
	/* CREATE ACTION EMP */
	private function action_emp($id,$type_local)
	{
		if($type_local == 0 || !$type_local){
			$type_local = 0;
		}
		$get = \DB::table('_action_emp')->where('role_id','=',$id)->get();
		if($get){ \DB::table('_action_emp')->where('role_id','=',$id)->update(['type_local'=>$type_local]); }
		else{ \DB::table('_action_emp')->insert(['role_id'=>$id,'type_local'=>$type_local]); }
	}
	/* STORE NEW ROLE (step 1)*/
	public function StoreCustomeRole($storeRole,$id_custome){
		if(\DB::table('role')->where('role_id','=',$id_custome)->get()){ \DB::table('role')->where('role_id','=',$id_custome)->update($storeRole); return ['status'=>200]; }
		$insert = \DB::table('role')->insert($storeRole);
		if($insert){
			$status = 200;
		}else{ $status=500; }
		return ['status'=>$status];
	}
	/* STORE PERMISSION (step 2)*/
	public function StoreCustomePermission($storePermissions,$id_custome){
		//return ['status'=>200,'data'=>$storePermissions];
		$i=0;
		while($i <= count($storePermissions)-1){
			$insert[] = \DB::table('permissions')->insert($storePermissions[$i]);
			$i++;
		}
		if($insert){ $status=200;
		}else{ $status=500; }
		return ['status'=>$status];
	}
	/* STORE Action (step finish)*/
	public function StoreCustomeAction($storeAction,$id_custome){
		//return ['status'=>200,'data'=>$storeAction];
		$add_form = 48;
		$terminate_form = 49;
		$create = ['create'=>1,'read'=>1,'update'=>1,'delete'=>1];

			if($storeAction['add_employee'] == 1){
				$insert[] = \DB::table('permissions')->where('form_id','=',$add_form)->where('role_id','=',$id_custome)->update($create);
				$message = null; $status=200;
				if($storeAction['terminate'] == 1){
					$insert[] = \DB::table('permissions')->where('form_id','=',$terminate_form)->where('role_id','=',$id_custome)->update($create);
					$message = 'Action add and terminate'; $status=200;
				}else{ $message ='Action add employee'; $status=200; }
			}else{
				if($storeAction['terminate'] == 1){
					$insert[] = \DB::table('permissions')->where('form_id','=',$terminate_form)->where('role_id','=',$id_custome)->update($create);
					$message = 'Action terminate'; $status=200;
				}else{ $message = 'Employee tidak berubah.'; $status=200; }
			}

		return ['message'=>$message,'status'=>$status];
	}
/* END STORE PERMISSION*/
/* ---- END STORE ---- */


	/* UPDATE */
	public function UpdateCustomeRole($id){

	}
/* END UPDATE */
	/* DELETE */
	public function DestroyCustomeRole($id){
		// $i = 1;
		// $title = array();
		// $del = array();
		// $destroy = array();
		
		// foreach ($id as $key) {
		// 	$del= \DB::table('ldap')->where('role_id','=',$key)->get(['role_id']);
		// 	$name = \DB::select("select role_name from role where role_id = $key ");
			
		// 	foreach ($name as $key) {
		// 		$title = $key->role_name;
		// 	}

		// 	if($del){ // jika role telah terpakai		
		// 		$msg[$i] = $title.' : Role Cannot delete.';$status[$i]= 500;
		// 	}else{ // jika tidak
		// 		foreach($id as $key){ 
		// 			$destroy[] = \DB::table('permissions')->where('role_id','=',$key)->delete();
		// 			$destroy[] = \DB::table('role')->where('role_id','=',$key)->where('role_id','>',32)->delete();
		// 		}
		// 		if($destroy){

		// 			$msg[$i] = $title.' : Delete Successfully.'; $status[$i]=200;
		// 		}
		// 		else{
		// 			$msg[$i] = $title.' : Delete Failed'; $status[$i]=500;
		// 		}
				
		// 	}
		// 	$i++;
		// }
		// if(in_array(200,$status)){ // jika menemukan status 200 true
		// 	return ['message'=>$msg,'status'=>$status,'fix'=>200];	
		// }else{
		// 	return ['message'=>$msg,'status'=>$status,'fix'=>500];	
		// }
		$i = 1;
		$key_role=array(); $msg = array();
		$destroy = null;
		foreach ($id as $key) {
			$del = \DB::table('ldap')->where('role_id','=',$key)->get(['role_id']);
			$name = \DB::select("select role_name from role where role_id = $key ");
			foreach ($name as $keys) {
				$title = $keys->role_name;
			}
			if($del == false){
				array_push($key_role,$key);
				$status[$i]=200;	
			}else{array_push($msg,$title.' : Role Cannot delete.'); $status[$i]= 500;}
			$i++;
		}

		if(array_search(500, $status)){
			return ['message'=>$msg,'status'=>$status,'fix'=>500];

		}else{
			foreach ($key_role as $key) {
				\DB::table('permissions')->where('role_id','=',$key)->delete();
				\DB::table('_action_emp')->where('role_id','=',$key)->delete();
				\DB::table('role')->where('role_id','=',$key)->where('role_id','>',32)->delete();
			}
			return ['message'=>'Destroy Successfully.','status'=>200,'fix'=>200];	
		}
	}
/* END DELETE */
/* END CRUD */
// SET INPUT #######################
	public function SetInputRole($input){
		return([
			'role_name'=>$input['role_name'],
			'role_id'=>$input['role_id'],
			'active_role'=>1
		]);
	}
// END SET INPUT ####################
// NEW GET_ID ####################
	public function Get_ID(){
		$getid = array();
		$result = \DB::select("Select role_id from role order by role_id desc limit 1");
		foreach ($result as $key) {
			$getid = $key->role_id;
		}
		return $getid;
	}
// END NEW GETID #################

}


	// public function Read2(){
	// 	$act_add_employee = null;
	// 	$act_terminate = null;
	// 	$count_role = \DB::select("select distinct a.role_id, b.role_name as role_base from permissions a , role b where a.role_id = b.role_id");

	// 		foreach ($count_role as $roles) {
	// 		$role_data[$roles->role_id] = $roles->role_id;
	// 		$data_form = \DB::select("select 
	// 							c.perm_id as id,
	// 							c.role_id,
	// 							c.form_id,d.form_name as form
	// 							from role a, permissions c, form d
	// 							where 
	// 							c.role_id = a.role_id and
	// 							c.form_id = d.form_id and
	// 							c.role_id = $roles->role_id");
	// 			foreach ($data_form as $page) {
	// 				$data_permission = \DB::select("select  
	// 							c.`create`,c.`read`,c.`update`,c.`delete`
	// 							from role a, permissions c, form d
	// 							where 
	// 							c.role_id = a.role_id and
	// 							c.form_id = d.form_id and
	// 							c.form_id = $page->form_id and
	// 							c.role_id = $roles->role_id");
	// 				foreach ($data_permission as $action) {
	// 					if($page->form_id == 48){
	// 						$act_add_employee = $action->create;
	// 					}
	// 					if($page->form_id == 49){
	// 						$act_terminate = $action->create;
	// 					}

	// 					$permissions = ['create'=>$action->create,'read'=>$action->read,'update'=>$action->update,'delete'=>$action->delete];
	// 					$forms[$page->form] = ['id'=>$page->id,'form_id'=>$page->form_id,'role_id'=>$page->role_id,'page'=>$page->form,'permissions'=>[$permissions]];
	// 					$datas[$roles->role_id]=['id'=>$roles->role_id,
	// 											'role'=>$roles->role_base,
	// 											'add_employee'=>$act_add_employee,
	// 											'terminate'=>$act_terminate,
	// 											'form'=>['id'=>$page->id,'form_id'=>$page->form_id,'role_id'=>$page->role_id,'page'=>$page->form,
	// 											'action'=>['create'=>$action->create,'read'=>$action->read,'update'=>$action->update,'delete'=>$action->delete]]
	// 											];
	// 				}
	// 			}
	// 		}		
	// 		return $datas;
	// }

/* READ INDEX */
	// public function ReadCustomeRole(){
	// 	$act_add = null;
	// 	$act_terminate = null;
	// 	$accounting_role = \DB::select("select distinct c.role_id,a.role_name
	// 				from role a, permissions c, form d
	// 				where 
	// 				c.role_id = a.role_id and
	// 				c.form_id = d.form_id");
	// 	foreach ($accounting_role as $key1) {
	// 		$accounting_form = \DB::select("select c.form_id, d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
	// 				from role a, permissions c, form d
	// 				where 
	// 				c.role_id = a.role_id and
	// 				c.form_id = d.form_id and 
	// 				c.role_id =  $key1->role_id ");
	// 		foreach ($accounting_form as $key) {
				
	// 			if($key->form_id == 48){ 
	// 				if($key->create == 1){ $act_add = 1; }
	// 				else{ $act_add = 0; } 
	// 			}
	// 			if($key->form_id == 49){ 
	// 				if($key->create == 1){ $act_terminate = 1; }
	// 				else{ $act_terminate = 0; } 
	// 			}
	// 				$value = ['create'=>$key->create,'read'=>$key->read,'update'=>$key->update,'delete'=>$key->delete];
	// 				$page[] = ['form_id'=>$key->form_id,'form_name'=>$key->form_name,'action'=>$value];	
	// 		}
			
	// 		$data[] = array('role_id'=>$key1->role_id,'role_name'=>$key1->role_name,'add_employee'=>$act_add,'terminate'=>$act_terminate,'form'=>$page);	
	// 	}		
	// 		return $datas;
	// }