<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class Dependent_Model extends Model {

	protected $table = 'emp_dependent';

// READ ###########################################
public function Read_Dependent($employee_id,$id){
	if(isset($id)){
		//DATE_FORMAT(date_of_birth, '%d-%m-%Y') as
		//$r = \DB::table($this->table)->where('id','=',$id)->get(['id','name','relationship','date_of_birth','age','gender','hmo_account','hmo_numb']);
		$r = \DB::select("SELECT id, name, relationship, date_of_birth, age, gender,hmo_account,hmo_numb where id = $id");
		if(isset($r) != null){ $data=$r; $status=200; }else{ $data=null; $status=500; }
	}else{
		//$r = \DB::table($this->table)->where('employee_id','=',$employee_id)->get(['id','name','relationship','date_of_birth','age','gender','hmo_account','hmo_numb']);
		$r = \DB::select("SELECT id, name, relationship, date_of_birth, age, gender,hmo_account,hmo_numb from emp_dependent where employee_id = '$employee_id'");
		if(isset($r) != null){ $data=$r; $status=200; }else{ $data=null; $status=500; }
	}
	return ['data'=>$data,'status'=>$status];
}
// END READ ###########################################


//STORE #################################################
public function Store_Dependent($input){
	//if(isset($input['name'],$input['relationship'],$input['home_telephone'],$input['mobile_telephone'])){
	$store = \DB::table('emp_dependent')
				->insert([
					'name'=>$input['name'],
					'employee_id'=>$input['id'],
					'relationship'=>$input['relationship'],
					'age'=>$input['age'],
					'date_of_birth'=>$input['date_of_birth'],
					'hmo_account'=>$input['hmo_account'],
					'hmo_numb'=>$input['hmo_numb'],
					'gender'=>$input['gender']
					]);
				if(isset($store)){
					// if(isset($input['hmo_account'],$input['hmo_numb'])){
					// 	$message = \DB::table('emp_dependent')->where('hmo_numb','=',$input['hmo_numb'])->get(['id']);
					// }
					// elseif(isset($input['hmo_account'])){
					// 	$message = \DB::table('emp_dependent')->where('hmo_numb','=',$input['hmo_numb'])->get(['id']);
					// }
					// elseif(isset($input['hmo_numb'])){
					// 	$message = \DB::table('emp_dependent')->where('hmo_numb','=',$input['hmo_numb'])->get(['id']);
					// }
					// else{$message = null;}
					$message = \DB::table('emp_dependent')->where('name','=',$input['name'])->get(['id']);
				}else{$message = null;}
			return $message;
	}
// END STORE ############################################

//UPDATE #################################################
public function Update_Dependent($input,$id){
	$employee_id = $this->Get_emp($id);
	$update = \DB::table('emp_dependent')
				->where('id','=',$id)
				->update([
						'name' => $input['name'],
						'relationship' => $input['relationship'],
						'gender' => $input['gender'],
						'age' => $input['age'],
						'date_of_birth' => $input['date_of_birth'],
						'hmo_account' => $input['hmo_account'],
						'hmo_numb' => $input['hmo_numb'],
						'employee_id' => $employee_id
					]);
				//if(isset($update)){
					// if(isset($input['hmo_account'],$input['hmo_numb'])){
					// 	$message = \DB::table('emp_dependent')->where('hmo_numb','=',$input['hmo_numb'])->get(['id']);
					// }
					// elseif(isset($input['hmo_account'])){
					// 	$message = \DB::table('emp_dependent')->where('hmo_numb','=',$input['hmo_numb'])->get(['id']);
					// }
					// elseif(isset($input['hmo_numb'])){
					// 	$message = \DB::table('emp_dependent')->where('hmo_numb','=',$input['hmo_numb'])->get(['id']);
					// }
					// else{$message = null;}
					
				// }else{$message = null;}
		return $update;
	}
// END STORE ############################################

// DESTROY DEPENDENT ################################################################
// PARAM ID(INT)
	public function Destroy_Dependent($id){
			foreach($id as $key){
				 $destroy = \DB::table('emp_dependent')->where('id','=',$key)->	delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################


// GET ID ###############################################
public function Get_emp($id){
		$data = \DB::select("Select employee_id from emp_dependent where id = $id limit 1");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID ###########################################


// GET ID EMP ###############################################
public function GetID(){
		$data = \DB::select("Select id from emp_dependent order by id DESC limit 1");
		return $data;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
//PARAM ROLE USER
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################


}
