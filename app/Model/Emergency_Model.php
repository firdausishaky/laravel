<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class Emergency_Model extends Model {

	// HRMS.emp_emergency
	protected $table = 'emp_emergency';
	protected $hidden = ['created_at','updated_at'];
	protected $guarded = ['id'];
	protected $fill = ['id','name','relationship','home_telephone','mobile_telephone','work_telephone','address','city','state','zip','country'];

	private function set_input($input)
	{	
		return [
						'name'=>$input['name'],
						'employee_id'=>$input['id'],
						'relationship'=>$input['relationship'],
						'home_telephone'=>$input['home_telephone'],
						'mobile_telephone'=>$input['mobile_telephone'],
						'work_telephone'=>$input['work_telephone'],
						'address'=>$input['address'],
						'city'=>$input['city'],
						'state'=>$input['state'],
						'zip'=>$input['zip'],
						'country'=>$input['country']
					];
	}
	// READ EMERGENCY CONTACT FOR EXPAT #######################################################
	private function get_nation($id){
		$data = \DB::table('nationality')->where('id','=',$id)->get(['title']);
		foreach ($data as $key) {
			$title = $key->title;
		}
		return $title;
	}
	public function Read_Emergency($id){
		if(isset($id) != null){
			$i=0;
			$datas = \DB::table($this->table)->where('employee_id','=',$id)->get($this->fill);
			foreach($datas as $key){
				if($key == null){
					$key = null;
				}
			}
			if($datas){	 $data = $datas; $status = 200; }
			else{ $data = null ; $status=500; }
		}
		else{ $data = null; $status = 500; }
		return ['data'=>$data,'status'=>$status];
	}
	public function Readedit_Emergency($id){
		//if(isset($id) != null){
			$data = \DB::table($this->table)->where('id','=',$id)->get($this->fill);	
			if(!$data){ $data = null; $status=500;}
			else{ $status=200; }
		// }
		// else{
		// 	$data = \DB::table($this->table)->get();
		// 	if(!$data){ $data = null; $status=500;}
		// 	else{ $status=200; }
		// }
		return ['data'=>$data[0],'status'=>$status];
	}
// END READ EMERGENCY ######################################################################


// STORE EMERGENCY ###############################################################
	// public function StoreEmergency($input){
	// 	//if(isset($input['name'],$input['relationship'],$input['home_telephone'],$input['mobile_telephone'])){

	// 			$store = \DB::table('emp_emergency')->insert($this->set_input($input));
	// 			if(isset($store)){
	// 				$message = \DB::select("SELECT id from emp_emergency where employee_id = '".$input['id']."' order by id desc");
	// 			}else{$message = null;}
	// 		return $message;
	// }
	public function StoreEmergency($input){
		//if(isset($input['name'],$input['relationship'],$input['home_telephone'],$input['mobile_telephone'])){

				$store = \DB::table('emp_emergency')->insert($this->set_input($input));
				if(isset($store)){
					$message = $this->Read_Emergency($input['id']);
					//$message = \DB::select("SELECT id from emp_emergency where employee_id = '".$input['id']."' order by id desc");
				}else{$message = null;}
			return $message;
	}

	// UPDATE EMERGENCY ###############################################################
	public function UpdateEmergency($input,$id){
		$employee_id = $this->Get_emp($id);
		if(isset($employee_id) != null){
			$update = \DB::table('emp_emergency')->where('id','=',$id)
					->update($this->set_input($input));
			if(isset($update) != null){
				$status = 200; $message='Update Successfully.';
			}else{$status=500; $message='Updated Failed.';}
		}else{
			$status=500; $message='ID not found.';
		}
		return ['message'=>$message,'status'=>$status];		
	}
// END UPDATE ###########################################################################
	public function Get_emp($id){
		$r = \DB::select("SELECT employee_id from emp_emergency where id = $id");
		if(isset($r) != null){
			foreach ($r as $key) {
				$employee_id = $key->employee_id;
			}
		}else{$employee_id=null;}
		return $employee_id;
	}


	/* GET TYPE LOCAL IT ###########################################################################
	* 
	*/
	public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}

	/* CHECK ID ####################################################################################
	* Param = 1(Store). 2(update)
	*/
	public function Check_ID($type,$id){
		if(isset($type) == 1 && isset($id)){
			$check = \DB::table('emp')->where('employee_id','=',$id)->get(['employee_id','local_it']);	
		}
		elseif(isset($type) == 2 && isset($id)){
			$check = \DB::select("SELECT a.employee_id 
								FROM emp a, emp_details b,emp_emergency c
								where
								a.employee_id = b.employee_id and
								a.employee_id = c.employee_id and
								b.employee_id = c.employee_id and
								c.id = '$id'");
		}
		return $check;
	}// END CHECK ID ###############################################################################

// DESTROY JOB ################################################################
// FIX***
	public function Destroy_Emergency($id){
			foreach($id as $key){
				 $destroy = \DB::table('emp_emergency')->where('id','=',$key)->delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY JOB ###############################################################

















	// NOT USES ################################

	// READ ALL EMERGENCY CONTACT EXPAT #################################
	// public function ReadEmergency_Expat_All($employee_id){
	// 	$data = \DB::table('view_emergency_expat')->where('employee_id','!=',$employee_id)->get();	
	// 	if(!$data){ $data = null;}
	// 	return $data;	
	// }

	// // READ ALL EMERGENCY CONTACT LOCAL #################################
	// public function ReadEmergency_Local_All($employee_id){
	// 	$data = \DB::table('view_emergency_local')->where('employee_id','!=',$employee_id)->get();	
	// 	if(!$data){ $data = null;}
	// 	return $data;	
	// }

	// // READ ALL EMERGENCY CONTACT LOCAL_IT #################################
	// public function ReadEmergency_Localit_All($employee_id){
	// 	$data = \DB::table('view_emergency_localit')->where('employee_id','!=',$employee_id)->get();	
	// 	if(!$data){ $data = null;}
	// 	return $data;	
	// }
	// END READ ########################################################################

// READ EMERGENCY CONTACT FOR LOCAL #######################################################
// 	public function ReadEmergency_Local($id,$employee_id){
// 		if(isset($id)){
// 			$data = \DB::table('view_emergency_local')->where('id','=',$id)->get();	
// 			if(!$data){ $data = null;}
// 		}
// 		else{
// 			$data = \DB::select("select * from view_emergency_local where employee_id != $employee_id");	
// 			if(!$data){ $data = null;}
// 		}
// 		return $data;
// 	}

// // READ EMERGENCY CONTACT FOR LOCAL IT #######################################################
// 	public function ReadEmergency_Localit($id,$employee_id){
// 		if(isset($id)){
// 			$data = \DB::table('view_emergency_local')->where('id','=',$id)->get();	
// 			if(!$data){ $data = null;}
// 		}
// 		else{
// 			$data = \DB::select("select * from view_emergency_local where employee_id != $employee_id");	
// 			if(!$data){ $data = null;}
// 		}
// 		return $data;
// 	}
}
