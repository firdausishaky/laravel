<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class EmpImmigration_Model extends Model {

	protected $table = 'emp_immigration';
	protected $view = 'view_emp_immigration';
	protected $fill = ['id','immigration_numb','type_doc','issued_date','issued_by','expiry_date','comment','filename','path'];
	protected $fill_view = ['id','immigration_numb','type_doc','issued_date','issued_by','expiry_date','filename','comment'];
	// READ #######################################################
	private function immigration_issuer()
	{
		$data = \DB::table('immigration_issuer')->get();
		return $data;
	}
	public function Read_Immigration($id){
		$data = null;
		if(isset($id) != null){
			$immigration_issuer = $this->immigration_issuer();
			$datas = \DB::table($this->table)->where('employee_id','=',$id)->get($this->fill_view);
			foreach ($datas as $key) {
				if($key->issued_by){
					foreach ($immigration_issuer as $keys) {
						if($keys->id == $key->issued_by){
							$key->issued_by = $keys->title;
						}
					}
				}

				if($key->issued_date == '0000-00-00'){ $key->issued_date = NULL; }
				if($key->expiry_date == '0000-00-00'){ $key->expiry_date = NULL; }
				if($key->comment == "NULL"){ $key->expiry_date = NULL; }
				if($key->issued_by == "NULL"){ $key->expiry_date = NULL; }
				if($key->type_doc == "NULL"){ $key->expiry_date = NULL; }
				$data[] = $key;
			}
			if(!$data){ $data = null; $status=500;}
			else{ $status=200; }
		}
		else{
			$data = \DB::table($this->table)->where('employee_id','=',$id)->get($this->fill_view);
			if(!$data){ $data = null; $status=500;}
			else{ $status=200; }
		}
		return ['data'=>$data,'status'=>$status];
	}
	public function Readedit_Emergency($id){
		//if(isset($id) != null){
			$data = \DB::table($this->table)->where('id','=',$id)->get($this->fill_view);	
			if(!$data){ $data = null; $status=500;}
			else{ $status=200; }
		// }
		// else{
		// 	$data = \DB::table($this->table)->get();
		// 	if(!$data){ $data = null; $status=500;}
		// 	else{ $status=200; }
		// }
		return ['data'=>$data[0],'status'=>$status];
	}
// END READ EMERGENCY ######################################################################


// STORE EMERGENCY ###############################################################
	public function StoreImmigration($input,$id){
		$check_input = $this->set_input($input);
		if(!isset($check_input,$id)){ $messages =null; return $messages; }
		
		$store = \DB::table('emp_immigration')->where('id','=',$id)->update($check_input);
		if(isset($store)){ $message = 200;}
		else{$message = null;}
		return $message;
	}

	public function Store_Immigration($input,$id){
		$check_input = $this->set_input($input);
		if(!isset($check_input,$id)){ $messages =null; return $messages; }
		$check_input['employee_id'] = $id;
		$store = \DB::table('emp_immigration')->insert($check_input);
		if(isset($store)){ $message = 200;}
		else{$message = null;}
		return $message;
	}

	// UPDATE EMERGENCY ###############################################################
	public function UpdateImmigration($input,$id){
		//if(isset($input['name'],$input['relationship'],$input['home_telephone'],$input['mobile_telephone'])){
		$employee_id = $this->Get_emp($id);
		if(isset($employee_id) != null){
			$update = \DB::table('emp_immigration')->where('id','=',$id)->update($input);
			if(isset($update) != null){
				$status = 200; $message='Update Successfully.';
			}else{$status=500; $message='Updated Failed.';}
		}else{
			$status=500; $message='ID not found.';
		}
		return ['message'=>$message,'status'=>$status];		
	}
// END UPDATE ###########################################################################
	




	public function Check_issued($id){
		$r = \DB::select("SELECT id from immigration_issuer where id = $id");
		if($r){
			foreach ($r as $key) {
				$data = $key->id;
			}
		}else{ $data = null; }
		return $data;
	}

	public function Get_emp($id){
		$r = \DB::select("SELECT employee_id from emp_immigration where id = $id");
		if(isset($r) != null){
			foreach ($r as $key) {
				$employee_id = $key->employee_id;
			}
		}else{$employee_id=null;}
		return $employee_id;
	}


	/* GET TYPE LOCAL IT ###########################################################################
	* 
	*/
	public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}

	/* CHECK ID ####################################################################################
	* Param = 1(Store). 2(update)
	*/
	public function Check_ID($type,$id){
		if(isset($type) == 1 && isset($id)){
			$check = \DB::table('emp')->where('employee_id','=',$id)->get(['employee_id','local_it']);	
		}
		elseif(isset($type) == 2 && isset($id)){
			$check = \DB::select("SELECT a.employee_id 
								FROM emp a, emp_details b,emp_emergency c
								where
								a.employee_id = b.employee_id and
								a.employee_id = c.employee_id and
								b.employee_id = c.employee_id and
								c.id = '$id'");
		}
		return $check;
	}// END CHECK ID ###############################################################################

// DESTROY JOB ################################################################
// FIX***
	public function Destroy_Immigration($id){
			foreach($id as $key){
				 $destroy = \DB::table('emp_immigration')->where('id','=',$key)->	delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY JOB ###############################################################
 public function set_input($input){
 	return([
 			'immigration_numb'=>$input['immigration_numb'],
 			'type_doc'=>$input['type_doc'],
			'issued_by'=>$input['issued_by'],
			'issued_date'=>$input['issued_date'],
			'expiry_date'=>$input['expiry_date'],
			'comment'=>$input['comment']
 		]);
 }// end set input

 public function Set_InputPict($input){
		return([
				'employee_id'	=> $input['employee_id'] ,
				'filename'		=> $input['	filename'] ,
				'path'			=> $input['path']
			]);
	}

public function GetID($filename){
		$data = \DB::select("Select id from emp_immigration where filename = '$filename' limit 1");
		if(!$data){return NULL;}
		foreach ($data as $key) {
			$id = $key->id;
		}
		return $id;
	}

public function getstorid($string){
	$get = \DB::select("SELECT id from emp_immigration where immigration_numb = '$string'");
	return $get;	
} 

public function StoreFile($id,$input){
	if($id){ \DB::table('emp_immigration')->where('id','=',$id)->update($input); }
	else{ \DB::table('emp_immigration')->insert($input); }
}
// CHECK ID ###############################################################
// PARAM ID
	public function check_file_id($id){
		$r = \DB::select("SELECT filename from emp_immigration where id = '$id' ");
		if(!$r){ return NULL; }
		foreach ($r as $key) {
			$data = $key->filename;
		}
		return $data;
	}
// END CHECK ID ###########################################################

}
