<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class EmployeeInfo_Model extends Model {



	Protected $tables = 'emp_list_view';
	Protected $hiddden = ['employee_id'];

 	protected $i =0;
    public function FecthData($local,$id){
    	$get1 = \DB::select("SELECT employee_id, CONCAT(first_name,' ',IFNULL(middle_name,'')) AS first_name,
								   last_name AS last_name, IFNULL(NULL,place_of_birth) AS date_hire FROM emp WHERE local_it = $local and employee_id != '$id' ORDER BY  employee_id ASC");
    	foreach ($get1 as $key) {
    		$get2 = \DB::select("SELECT
								IFNULL(NULL,j.title) AS job,
								IFNULL(NULL,a.title) AS `status`,
								IFNULL(NULL,d.sub_unit) AS sub_unit
								FROM emp x , job_history e , sub_unit d , job j , jobs_emp_status a
								WHERE
								x.employee_id = e.employee_id
								AND e.sub_unit = d.id
								AND e.job = j.id
								AND e.status = a.id
								AND e.employee_id = '$key->employee_id' ");
	    		if($get2){ // check jika emp memiliki data job_history
	    			foreach ($get2 as $jobhis) {
	    				$job = $jobhis->job;
	    				$status = $jobhis->status;
	    				$sub_unit = $jobhis->sub_unit;
	    			}
	    		}else{ $job=null; $status=null; $sub_unit=null; }

    		$get3 = \DB::select("SELECT
									(SELECT concat(a.first_name,' ',IFNULL(a.middle_name,''),' ',IFNULL(a.last_name,''))
										FROM emp a, emp_supervisor b where a.employee_id = b.supervisor ) AS `supervisor`
									 FROM emp a , emp_supervisor b
									 WHERE
									 a.employee_id = b.employee_id and
									 a.employee_id = '$key->employee_id' ");
    			if($get3){ // check jika emp memiliki supervisor
	    			foreach ($get3 as $emp_supervisor) {
	    				$supervisor[] = $emp_supervisor->supervisor;
	    			}
	    			if(count($supervisor) > 1){
	    				$supervisor = implode(", ",$supervisor);
	    			}
	    		}else{ $supervisor=null; }

    		$data['data'][$this->i++] = ['employee_id'=>$key->employee_id,
    							'first_name'=>$key->first_name,
    							'last_name'=>$key->last_name,
    							'date_hire'=>$key->date_hire,
    							'job'=>$job,
    							'status'=>$status,
    							'sub_unit'=>$sub_unit,
    							'supervisor'=>$supervisor];
    	} return $data;
    }
    private function QueryParamsFilter($param2,$param1,$employee_id)
    {
 		if(isset($param2['depart'])){  $prm['depart'] = " AND b.sub_unit = $param2[depart] ";  }
		if(isset($param2['status'])){ $prm['status'] = " AND b.status = $param2[status] "; }
		if(isset($param2['job'])){ $prm['job'] = " AND b.job = $param2[job] "; }
    	
		$tmp="";
		if(isset($prm)){			
			foreach($prm as $keyss => $valuess){ $tmp .= $valuess; }
		}

		$QUERY = "SELECT DISTINCT a.employee_id, CONCAT(a.first_name,' ',IFNULL(a.middle_name,'')) AS first_name, a.last_name AS last_name FROM emp a";
		
		
		if(isset($param2['depart']) || isset($param2['job']) || isset($param2['status']) ){ $QUERY .= " , job_history b where a.employee_id = b.employee_id ".$tmp." AND "; }
		else if(isset($param2['spv'])){
				$QUERY .= " , job_history b , emp_supervisor x where a.employee_id = b.employee_id AND x.employee_id = a.employee_id and x.supervisor = $param2[spv] ".$tmp." AND ";
		}else{ 
			if(strlen($tmp)){ $QUERY .= " where ".$tmp." AND ";  }
			else{ $QUERY .= " where ".$tmp;  }
		}
		
		//return $param2;
		$res = null;
		if($param2['inc'] == 1){ 
				if(isset($param2['id'])){ $empid = " a.employee_id = $param2[id] "; }
				else{ $empid = " a.employee_id NOT in('2014999','2014888') ORDER BY  a.last_name ASC "; }
				$res = \DB::select($QUERY." a.local_it in($param1) and substr(a.last_name,1,1) != '(' and ".$empid); }
						
				/* $res = \DB::select("SELECT employee_id, CONCAT(first_name,' ',IFNULL(middle_name,'')) AS first_name,
					last_name AS last_name FROM emp WHERE local_it in($param1) and substr(last_name,1,1) != '('
					and employee_id NOT in('2014999','2014888') ORDER BY  last_name ASC"); } */
    	if($param2['inc'] == 2) { 
				if(isset($param2['id'])){ $empid = " a.employee_id = $param2[id] "; }
				else{ $empid = " a.employee_id NOT in($employee_id,'2014999','2014888') ORDER BY  a.last_name ASC "; }
				$res = \DB::select($QUERY." a.local_it in($param1) and ".$empid); }
				
				/* $res = \DB::select("SELECT employee_id, CONCAT(first_name,' ',IFNULL(middle_name,'')) AS first_name,
					last_name AS last_name FROM emp WHERE local_it in($param1) or substr(last_name,1,1) = '('
					and employee_id NOT in($employee_id,'2014999','2014888') ORDER BY last_name ASC"); } */
    	if($param2['inc'] == 3){ 
				if(isset($param2['id'])){ $empid = " a.employee_id = $param2[id] "; }
				else{ $empid = " (a.employee_id NOT in($employee_id,'2014999','2014888')) ORDER BY  a.last_name ASC "; }
				$res = \DB::select($QUERY." a.local_it in($param1) and last_name like '(%' and ".$empid);}
					
				/* $res = \DB::select("SELECT employee_id, CONCAT(first_name,' ',IFNULL(middle_name,'')) AS first_name,
					last_name AS last_name FROM emp WHERE local_it in($param1) and substr(last_name,1,1) = '('
					and (employee_id NOT in($employee_id,'2014999','2014888')) ORDER BY last_name ASC");} */
    	return $res;
    }

    public function parameterFilter($params,$employee_id){
    	$res = null;
 		if($params['lc'] == 1){
 			$param = '1,2,3';
 			$res = $this->QueryParamsFilter($params,$param,$employee_id);
 		}elseif($params['lc'] == 2){
 			$param = 1;
 			$res = $this->QueryParamsFilter($params,$param,$employee_id);
 		}elseif($params['lc'] == 3){
 			$param = 3;
 			//return $param;
 			$res = $this->QueryParamsFilter($params,$param,$employee_id);
 		}elseif($params['lc'] == 4){
 			$param = 2;
 			//return $param;
 			$res = $this->QueryParamsFilter($params,$param,$employee_id);
 		}elseif($params['lc'] == 5){
 			$param = '2,3';
 			//return $param;
 			$res = $this->QueryParamsFilter($params,$param,$employee_id);
 		}
 		return $res;
    }
    public function FecthAllData($local,$employee_id,$param){

    	if($param['lc'] != null && $param['inc'] != null){
    		$get1 = $this->parameterFilter($param,$employee_id);
			//return $get1;
    	}else{
    		$get1 = $this->parameterFilter(1,1,$employee_id);
    	}
    	//$get1 = \DB::select("SELECT employee_id, CONCAT(first_name,IFNULL(middle_name,'')) AS first_name,
		//					   last_name AS last_name FROM emp WHERE local_it in(1,2,3) and substr(last_name,1,1) != '(' and employee_id != '$employee_id' ORDER BY  last_name ASC");
    	//return $get1;
    	if($get1){
    		foreach ($get1 as $key) {
    		if($key->employee_id != '2014999' && $key->employee_id != '2014888'){
    			//old
    			$get2 = \DB::select("SELECT DISTINCT
								IF(b.contract_start_date = null or b.contract_start_date = '',null,b.contract_start_date) AS date_hire,
								IF(d.title = null or d.title = '',null,d.title) AS job,
								IF(e.title = null or e.title = '',null,e.title) AS `status`,
								IF(c.id = null or c.id = '',null,c.id) AS department
								FROM emp a , job_history b , department c , job d , jobs_emp_status e
								WHERE
								(a.employee_id = b.employee_id)
								AND c.id = b.sub_unit
								AND d.id = b.job
								AND e.id = b.status
								AND a.employee_id = '$key->employee_id' ");
	    		if($get2){ // check jika emp memiliki data job_history
	    			foreach ($get2 as $jobhis) {
	    				$job = $jobhis->job;
	    				$status = $jobhis->status;
	    				$sub_unit = $jobhis->department;
	    				$date_hire = $jobhis->date_hire;
	    			}
	    		}else{ $job=null; $status=null; $sub_unit=null; $date_hire=null;}

				//return $get2;
				
	    		$get3 = \DB::select("select concat(a.first_name,' ',IFNULL(a.middle_name,''),' ',IFNULL(a.last_name,'')) AS supervisor
									from emp a , emp_supervisor b where b.supervisor = a.employee_id and b.employee_id = '$key->employee_id'");
	    		//$sss[$this->i++] = $get3;
    			if($get3){ // check jika emp memiliki supervisor
    				$supervisor = [];
	    			foreach ($get3 as $emp_supervisor) {
	    				$supervisor[] = $emp_supervisor->supervisor;
	    			}
	    			if(count($supervisor) > 1){
	    				$supervisor = implode(", ",$supervisor);
	    			}
	    		}else{ $supervisor=null; }

	    		if($date_hire == "0000-00-00"){
	    			$date_hire = null;
	    		}
    			$data[$this->i++] = ['employee_id'=>$key->employee_id,
    							'first_name'=>$key->first_name,
    							'last_name'=>$key->last_name,
    							'date_hire'=>$date_hire,
    							'job'=>$job,
    							'status'=>$status,
    							'sub_unit'=>$this->check_department($sub_unit),
    							'supervisor'=>$supervisor];

    			}// end for
    		}

    	}// endif
    	else{ $data =null; }

    	return $data;
    }


    public function check_department($id){
    	  	if($id === null){
    	  		return null;
    	  	}else{
    			$data = \DB::SELECT("SELECT name FROM department WHERE id = $id ");
    			return $data[0]->name;
    		}
    }
	// PARAM 1(ALL) and 2(SORT)

	// EXPAT #######################################################
	public function ReadEmpInfoExpat($type,$employee_id,$id,$param){
		if($type == 1){ $data = $this->FecthAllData(1,$employee_id,$param);
			if(!$data){ $data = null; }
		}
		elseif($type == 2){ $data = \DB::select("select * from view_emp_expat where id = '$id' ");
			if(!$data){ $data = null; }
		}
		return $data;
	}
	// LOCAL #######################################################
	public function ReadEmpInfoLocal($type,$employee_id,$id,$param){

		if($type == 1){ $data = $this->FecthAllData(2,$employee_id,$param);
			if(!$data){ $data = null; }
		}
		elseif($type == 2){ $data = \DB::select("select * from view_emp_local where id = '$id' ");
			if(!$data){ $data = null; }
		}
		return $data;

	}
	// LOCALIT #######################################################
	public function ReadEmpInfoLocalit($type,$employee_id,$id,$param){
		if($type == 1){ $data = $this->FecthAllData(3,$employee_id,$param);
			if(!$data){ $data = null; }
		}
		elseif($type == 2){ $data = \DB::select("select * from view_emp_localit where id = '$id' ");
			if(!$data){ $data = null; }
		}
		return $data;

	}

	// UPDATE
	public function UpdateEmpListInfo($id,$title){
		$update = \DB::table($this->tables)->where(['id'=>$id])->update(['title'=>$title]);
		if($update){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg;
	}
	// FIX**
	public function StoreEmpListInfo($title){
		$store = \DB::table($this->tables)->insert(['title'=>$title]);
		if($store){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg;
	}

public function DestroyEMP($id){
		foreach ($id as $key) {
			$past_employee = $this->past_employee($key);
		}

		if($past_employee['status'] ==200){
			$data=['id'=>$id,'employee'=>$past_employee['message']];$message='Store Successfully.'; $status=200;
		}else{
				$data=null; $message='Failed Past Employee !'; $status=500;
		}
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
// DESTROY ################################################################
// PARAM ID(INT)
	public function DestroyEmpListInfo($id){
			foreach($id as $key){
				 $destroy = \DB::table($this->table)->where('id','=',$key)->delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################

	/* GET TYPE ID ##################################################
	* PARAM ROLE USER
	*/
	public function Get_Type($role,$employee_id){
		$data = \DB::select("SELECT c.local_it, c.employee_id
							FROM ldap b, emp c
							WHERE c.employee_id = b.employee_id and
							b.role_id = '$role' and c.employee_id = '$employee_id'");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}// END GET TYPE ################################################

	// SEARCH JOB #######################################
	public function Search($string){
		$check_lastName = strpos($string," ");
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));

		if($last_name){
			if(isset($string)){
				$first_name = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select employee_id, concat(first_name,' ',IFNULL(middle_name,''),' ',IFNULL(last_name,'')) as name
						from emp where first_name = '$first_name' and middle_name LIKE '$last_name%' limit 5");
			}else{ return ['data'=>null,'status'=>500]; }
		}else{
			$data = \DB::select("SELECT employee_id, concat(first_name,' ',IFNULL(middle_name,''),' ',IFNULL(last_name,'')) as name
						from emp where first_name LIKE '$string%' limit 10");
			$status=200;
		}
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################
		public function past_employee($id){
		//$name = \DB::select("select last_name, concat(first_name,' ',middle_name,' ',last_name,' (PAST EMPLOYEE).') as name from emp where employee_id = '$id' ");//
		$name = \DB::table('emp')->where('employee_id','=',$id)->get(['last_name']);

		if(isset($name) && $name != '[]'){
			foreach ($name as $key) {
				$last_name = $key->last_name;
			}
			\DB::table('emp')->where('employee_id','=',$id)->update(['last_name'=>"(DEL).".$last_name,'ad_username'=>null]);
			$message = '(DEL)'.$last_name; $status=200;
		}else{ $message='ID Undefined.'; $status=500; }
		return ['message'=>$message,'status'=>$status];
	}

	public function GetFilterEmp(){
		$i=1; $res['job'] = array(); $res['status'] = array(); $res['sub_unit'] = array();
		$job = \DB::table('job')->get(['id','title']);
		$status = \DB::table('jobs_emp_status')->get(['id','title']);
		$sub_unit = \DB::table('sub_unit')->get(['id','sub_unit']);
		//$res['job'] = array();
		foreach ($job as $jobs) {
			$t = ['id'=>$jobs->id,'title'=>$jobs->title];
			array_push($t,$res['job'][$i]);
			$i++;
		}
		foreach ($status as $statuss) {
			//$res['status'][$i] = ['id'=>$statuss->id,'title'=>$statuss->title];
			$t = ['id'=>$statuss->id,'title'=>$statuss->title];
			array_push($t,$res['status']);
			$i++;
		}
		foreach ($sub_unit as $sub_units) {
			//$res['sub_unit'][$i] = ['id'=>$sub_units->id,'title'=>$sub_units->sub_unit];
			$t = ['id'=>$sub_units->id,'title'=>$sub_units->sub_unit];
			array_push($t,$res['sub_unit']);
			$i++;
		}
		return $res;

	}
	public function getAct($role_id)
	{
		$get = \DB::table('_action_emp')->where('role_id','=',$role_id)->get();
		return $get;
	}
	public function getReq($r)
	{
		if(isset($r['key'])){
			$decode = base64_decode($r['key']);
			$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
			$get = \DB::select("SELECT a.local_it, b.role_id from emp a , ldap b where a.employee_id = b.employee_id and a.employee_id = '$id' ");
			if($get != '[]' && $get){
				foreach ($get as $key) {
					$type = [	'typeLocal' => $key->local_it,
								'roleBase' => $key->role_id,
								'employee_id'=>$id,
								'param'=>['inc'=>$r['inc'],'lc'=>$r['lc']]
							];
					
				}
			}else{ $type = null; }
		}else{ $type = null; }
		return $type;
	}
	public function ListOrderByLocal($type,$input)
	{
		$msg = ['success'=>['message'=>'List Employees : Show Records Data.','status'=>200],'error'=>['message'=>'List Employees : Empty Records Data.','status'=>200]];
		$checkAdd = $this->getAct($type['roleBase']);
		if(isset($type['action_create']) && $type['action_create'] == 1 && $checkAdd )
			{ $action_add = true; }
		else{ $action_add = false; }

			if($type['local_it'] == 1){
				if($input['lc'] != 0 && $input['inc'] != 0){
					$result = $this->ReadEmpInfoExpat(1,$type['employee_id'],null,$input); // GET RESULT MODEL
				}else{
					$result = $this->ReadEmpInfoExpat(1,$type['employee_id'],null,null); // GET RESULT MODEL
				}

				if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result['data']]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
				else{ $data = ['action_add'=>false,'list'=>null]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
			}
			elseif($type['local_it'] == 2){
				if($input['lc'] != 0 && $input['inc'] != 0){
					$result = $this->ReadEmpInfoLocal(1,$type['employee_id'],null,$input); // GET RESULT MODEL
				}else{
					$result = $this->ReadEmpInfoLocal(1,$type['employee_id'],null,null); // GET RESULT MODEL
				}

				if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result['data']]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
				else{ $data = ['action_add'=>false,'list'=>null]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
			}
			elseif($type['local_it'] == 3) {
				$result = $this->ReadEmpInfoLocalit(1,$type['employee_id'],null); // GET RESULT MODEL

				if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
				else{ $data = ['action_add'=>false,'list'=>null] ; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
			}
			else{ $message='Unauthorized'; $status=403; $data=null; }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}

	public function Lists($type,$input,$param,$access)
	{
		$msg = ['success'=>['message'=>'List Employees : Show Records Data.','status'=>200],'error'=>['message'=>'List Employees : Empty Records Data.','status'=>200]];
		$checkAdd = $this->getAct($type['roleBase']);
		$departmens  =\DB::SELECT("select id,name from department where id <> 1");		
		$jobs = \DB::SELECT("select id,title from job");
		$status = \DB::SELECT("select id,title from jobs_emp_status");
		 $addOn = ['department' => $departmens, 'job' => $jobs, 'status' => $status];

		if(isset($type['action_create']) && $type['action_create'] == 1 && $checkAdd )
			{ $action_add = true; }
		else{ $action_add = false; }

			if($param == 'ExpatLocal'){
				//$input['lc'] = 1; $input['inc'] = 1;
				if($input['lc'] == 0){ $input['lc'] = 1;  }
				elseif($input['lc'] == 1){

					$input['lc'] = 1;
				}elseif($input['lc'] == 2){

					$input['lc'] = 2;
				}elseif($input['lc'] == 3){

					$input['lc'] = 3; //$input['inc'] = 1;
				}elseif($input['lc'] == 4){

					$input['lc'] = 4; //$input['inc'] = 1;
				}elseif($input['lc'] == 5){

					$input['lc'] = 5; //$input['inc'] = 1;
				}else{
					$data = ['action_add'=>$action_add,'list'=>null,$addOn[0]] ; $message=$msg['error']['message']; $status=$msg['error']['status'];
					return ['message'=>$message,'status'=>$status,'data'=>$data,'department' => $departmens, 'job' => $jobs, 'status' => $status];
				}

				$result = $this->ReadEmpInfoExpat(1,$type['employee_id'],null,$input); // GET RESULT MODEL
				//return $result;
				if($access['create'] == 0 && $access['delete'] == 0 && $access['update'] == 0 && $access['read'] == 0){
					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>[],$addOn[0]]; $message="Unauthorized"; $status=500; }
					else{ $data = ['action_add'=>false,'list'=>null,'department' => $departmens, 'job' => $jobs, 'status' => $status]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
				}else{
					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result,'department' => $departmens, 'job' => $jobs, 'status' => $status]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
					else{ $data = ['action_add'=>false,'list'=>null,'department' => $departmens, 'job' => $jobs, 'status' => $status]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
				}
			}
			elseif($param == 'Expat'){

				//$input['lc'] = 2; $input['inc'] = 1;
				if($input['lc'] == 0){

					$input['lc'] = 2; //$input['inc'] = 1;
				}elseif($input['lc'] == 2){

					$input['lc'] = 2; //$input['inc'] = 1;
				}
				elseif($input['lc'] == 1){

				 	$input['lc'] = 2; //$input['inc'] = 1;
				}
				else{
					$data = ['action_add'=>$action_add,'list'=>null,$addOn[0]] ; $message=$msg['error']['message']; $status=$msg['error']['status'];
					return ['message'=>$message,'status'=>$status,'data'=>$data,'department' => $departmens, 'job' => $jobs, 'status' => $status];
				}

				$result = $this->ReadEmpInfoExpat(1,$type['employee_id'],null,$input); // GET RESULT MODEL
				//return $result;
				if($access['create'] == 0 && $access['delete'] == 0 && $access['update'] == 0 && $access['read'] == 0){
					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>[],$addOn[0]]; $message="Unauthorized"; $status=500; }
					else{ $data = ['action_add'=>false,'list'=>null,'department' => $departmens, 'job' => $jobs, 'status' => $status]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
				}else{
					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result,$addOn[0]]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
					else{ $data = ['action_add'=>false,'list'=>null,'department' => $departmens, 'job' => $jobs, 'status' => $status]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
				}
			}
			elseif($param == 'Local') {
				//$input['lc'] = 5; $input['inc'] = 1;
				if($input['lc'] == 0){

					$input['lc'] = 5; //$input['inc'] = 1;
				}elseif($input['lc'] == 5){

					$input['lc'] = 5; //$input['inc'] = 1;
				}elseif($input['lc'] == 4){

					$input['lc'] = 4; //$input['inc'] = 1;
				}elseif($input['lc'] == 3){

					$input['lc'] = 3; //$input['inc'] = 1;
				}else{
					$data = ['action_add'=>$action_add,'list'=>null,$addOn[0]] ; $message=$msg['error']['message']; $status=$msg['error']['status'];
					return ['message'=>$message,'status'=>$status,'data'=>$data,'department' => $departmens, 'job' => $jobs, 'status' => $status];
				}
				$result = $this->ReadEmpInfoLocal(1,$type['employee_id'],null,$input); // GET RESULT MODEL
				//return $result;
				if($access['create'] == 0 && $access['delete'] == 0 && $access['update'] == 0 && $access['read'] == 0){
					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>[],'department' => $departmens, 'job' => $jobs, 'status' => $status]; $message="Unauthorized"; $status=500; }
					else{ $data = ['action_add'=>false,'list'=>null,$addOn[0]]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
				}else{
					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result,'department' => $departmens, 'job' => $jobs, 'status' => $status]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
					else{ $data = ['action_add'=>false,'list'=>null,'department' => $departmens, 'job' => $jobs, 'status' => $status]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
				}
			}
			else{ $message='Unauthorized'; $status=403; $data=null; }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}

	public function searchEmp($name,$id)
	{
		$get = \DB::select("SELECT employee_id , concat(first_name,' ',IFNULL(middle_name,''),' ',IFNULL(last_name,'')) as name from emp where first_name LIKE '$name%' and employee_id != '$id' and ad_username != ''" );
		if($get){ $data = ['data'=>$get,'status'=>200]; }
		else{ $data = ['data'=>[["name" => 'Data Not Found.']],'status'=>200];  }
		return $data;
	}
	public function search_by_name($name,$id)
	{
		$get = \DB::select("SELECT employee_id , concat(first_name,' ',IFNULL(middle_name,''),' ',IFNULL(last_name,'')) as name from emp 
		where lower(concat(first_name,' ',IFNULL(middle_name,''),' ',IFNULL(last_name,''))) LIKE '$name%' and employee_id != '$id'" );
		if($get){ $data = ['data'=>$get,'status'=>200]; }
		else{ $data = ['data'=>[["name" => 'Data Not Found.']],'status'=>200];  }
		return $data;
	}
	
	public function search_by_spv($spv,$id)
	{
		$get = \DB::select("select a.employee_id, concat(first_name,' ',IFNULL(middle_name,''),' ',IFNULL(last_name,'')) as name from emp a, emp_supervisor b
where a.employee_id = b.supervisor and
lower(concat(first_name,' ',IFNULL(middle_name,''),' ',IFNULL(last_name,''))) like '$spv%'");
		if($get){ $data = ['data'=>$get,'status'=>200]; }
		else{ $data = ['data'=>[["name" => 'Data Not Found.']],'status'=>200];  }
		return $data;
	}

// 	Protected $tables = 'emp_list_view';
// 	Protected $hiddden = ['employee_id'];

//  	protected $i =0;
//     public function FecthData($local,$id){
//     	$get1 = \DB::select("SELECT employee_id, CONCAT(first_name,if(middle_name != null or middle_name != '','',middle_name)) AS first_name,
// 								   last_name AS last_name, IFNULL(NULL,place_of_birth) AS date_hire FROM emp WHERE local_it = $local and employee_id != '$id' ORDER BY  employee_id ASC");
//     	foreach ($get1 as $key) {
//     		$get2 = \DB::select("SELECT
// 								IFNULL(NULL,j.title) AS job,
// 								IFNULL(NULL,a.title) AS `status`,
// 								IFNULL(NULL,d.id) AS sub_unit
// 								FROM emp x , job_history e , department d , job j , jobs_emp_status a
// 								WHERE
// 								x.employee_id = e.employee_id
// 								AND e.sub_unit = d.id
// 								AND e.job = j.id
// 								AND e.status = a.id
// 								AND e.employee_id = '$key->employee_id' ");
// 	    		if($get2){ // check jika emp memiliki data job_history
// 	    			foreach ($get2 as $jobhis) {
// 	    				$job = $jobhis->job;
// 	    				$status = $jobhis->status;
// 	    				$sub_unit = $jobhis->sub_unit;
// 	    			}
// 	    		}else{ $job=null; $status=null; $sub_unit=null; }

//     		$get3 = \DB::select("SELECT
// 									(SELECT concat(a.first_name,if(a.middle_name != null or a.middle_name != '',a.middle_name,''),' ',a.last_name)
// 										FROM emp a, emp_supervisor b where a.employee_id = b.supervisor ) AS `supervisor`
// 									 FROM emp a , emp_supervisor b
// 									 WHERE
// 									 a.employee_id = b.employee_id and
// 									 a.employee_id = '$key->employee_id' ");
//     			if($get3){ // check jika emp memiliki supervisor
// 	    			foreach ($get3 as $emp_supervisor) {
// 	    				$supervisor = $emp_supervisor->supervisor;
// 	    			}
// 	    		}else{ $supervisor=null; }

//     		$data['data'][$this->i++] = ['employee_id'=>$key->employee_id,
//     							'first_name'=>$key->first_name,
//     							'last_name'=>$key->last_name,
//     							'date_hire'=>$key->date_hire,
//     							'job'=>$job,
//     							'status'=>$status,
//     							'sub_unit'=>$sub_unit,
//     							'supervisor'=>$supervisor];
//     	} return $data;
//     }
//     private function QueryParamsFilter($inc,$param1,$employee_id)
//     {
//     	if($inc == 1 || $inc!=2 && $inc !=3){ $res = \DB::select("SELECT employee_id, CONCAT(first_name,' ',IFNULL(middle_name,'')) AS first_name,
// 					last_name AS last_name FROM emp WHERE local_it in($param1) and substr(last_name,1,1) != '('
// 					and employee_id NOT in($employee_id,'2014999','2014888') ORDER BY  last_name ASC"); }
//     	elseif($inc == 2) { $res = \DB::select("SELECT employee_id, CONCAT(first_name,' ',IFNULL(middle_name,'')) AS first_name,
// 					last_name AS last_name FROM emp WHERE local_it in($param1) or substr(last_name,1,1) = '('
// 					and employee_id NOT in($employee_id,'2014999','2014888') ORDER BY last_name ASC"); }
//     	elseif($inc == 3){ $res = \DB::select("SELECT employee_id, CONCAT(first_name,' ',IFNULL(middle_name,'')) AS first_name,
// 					last_name AS last_name FROM emp WHERE local_it in($param1) and substr(last_name,1,1) = '('
// 					and (employee_id NOT in($employee_id,'2014999','2014888')) ORDER BY last_name ASC");}
//     	return $res;
//     }
//     public function parameterFilter($params,$employee_id){
//     	$res = null;
//  		if($params['lc'] == 1){
//  			$param = '1,2,3';
//  			$res = $this->QueryParamsFilter($params['inc'],$param,$employee_id);
//  		}elseif($params['lc'] == 2){
//  			$param = 1;
//  			$res = $this->QueryParamsFilter($params['inc'],$param,$employee_id);
//  		}elseif($params['lc'] == 3){
//  			$param = 3;
//  			//return $param;
//  			$res = $this->QueryParamsFilter($params['inc'],$param,$employee_id);
//  		}elseif($params['lc'] == 4){
//  			$param = 2;
//  			//return $param;
//  			$res = $this->QueryParamsFilter($params['inc'],$param,$employee_id);
//  		}elseif($params['lc'] == 5){
//  			$param = '2,3';
//  			//return $param;
//  			$res = $this->QueryParamsFilter($params['inc'],$param,$employee_id);
//  		}
//  		return $res;
//     }
//     public function FecthAllData($local,$employee_id,$param){

//     	if($param['lc'] != null && $param['inc'] != null){
//     		$get1 = $this->parameterFilter($param,$employee_id);
//     	}else{
//     		$get1 = $this->parameterFilter(1,1,$employee_id);

//     	}
//     	//$get1 = \DB::select("SELECT employee_id, CONCAT(first_name,IFNULL(middle_name,'')) AS first_name,
// 		//					   last_name AS last_name FROM emp WHERE local_it in(1,2,3) and substr(last_name,1,1) != '(' and employee_id != '$employee_id' ORDER BY  last_name ASC");
//     	//return $get1;
//     	if($get1){
//     		foreach ($get1 as $key) {
//     		if($key->employee_id != '2014999' && $key->employee_id != '2014888'){
//     			$get2 = \DB::select("SELECT
// 								IF(b.contract_start_date = null or b.contract_start_date = '',null,b.contract_start_date) AS date_hire,
// 								IF(d.title = null or d.title = '',null,d.title) AS job,
// 								IF(e.title = null or e.title = '',null,e.title) AS `status`,
// 								IF(c.id = null or c.id = '',null,c.id) AS sub_unit
// 								FROM emp a , job_history b , department c , job d , jobs_emp_status e
// 								WHERE
// 								a.employee_id = b.employee_id
// 								AND c.id = b.sub_unit
// 								AND d.id = b.job
// 								AND e.id = b.status
// 								AND a.employee_id = '$key->employee_id' ");
// 	    		if($get2){ // check jika emp memiliki data job_history
// 	    			foreach ($get2 as $jobhis) {
// 	    				$job = $jobhis->job;
// 	    				$status = $jobhis->status;
// 	    				$sub_unit = $jobhis->sub_unit;
// 	    				$date_hire = $jobhis->date_hire;
// 	    			}
// 	    		}else{ $job=null; $status=null; $sub_unit=null; $date_hire=null;}


// 	    		$get3 = \DB::select("select if(a.first_name = null or a.first_name = '',null,concat(a.first_name,'',a.middle_name,' ',a.last_name)) AS supervisor
// 									from emp a , emp_supervisor b where b.supervisor = a.employee_id and b.employee_id = '$key->employee_id'");
// 	    		//$sss[$this->i++] = $get3;
//     			if($get3){ // check jika emp memiliki supervisor
// 	    			foreach ($get3 as $emp_supervisor) {
// 	    				$supervisor = $emp_supervisor->supervisor;
// 	    			}
// 	    		}else{ $supervisor=null; }

//     			$data[$this->i++] = ['employee_id'=>$key->employee_id,
//     							'first_name'=>$key->first_name,
//     							'last_name'=>$key->last_name,
//     							'date_hire'=>$date_hire,
//     							'job'=>$job,
//     							'status'=>$status,
//     							'sub_unit'=>$sub_unit,
//     							'supervisor'=>$supervisor];

//     			}// end for
//     		}

//     	}// endif
//     	else{ $data =null; }

//     	return $data;
//     }
// 	// PARAM 1(ALL) and 2(SORT)

// 	// EXPAT #######################################################
// 	public function ReadEmpInfoExpat($type,$employee_id,$id,$param){
// 		if($type == 1){ $data = $this->FecthAllData(1,$employee_id,$param);
// 			if(!$data){ $data = null; }
// 		}
// 		elseif($type == 2){ $data = \DB::select("select * from view_emp_expat where id = '$id' ");
// 			if(!$data){ $data = null; }
// 		}
// 		return $data;
// 	}
// 	// LOCAL #######################################################
// 	public function ReadEmpInfoLocal($type,$employee_id,$id,$param){

// 		if($type == 1){ $data = $this->FecthAllData(2,$employee_id,$param);
// 			if(!$data){ $data = null; }
// 		}
// 		elseif($type == 2){ $data = \DB::select("select * from view_emp_local where id = '$id' ");
// 			if(!$data){ $data = null; }
// 		}
// 		return $data;

// 	}
// 	// LOCALIT #######################################################
// 	public function ReadEmpInfoLocalit($type,$employee_id,$id,$param){
// 		if($type == 1){ $data = $this->FecthAllData(3,$employee_id,$param);
// 			if(!$data){ $data = null; }
// 		}
// 		elseif($type == 2){ $data = \DB::select("select * from view_emp_localit where id = '$id' ");
// 			if(!$data){ $data = null; }
// 		}
// 		return $data;

// 	}

// 	// UPDATE
// 	public function UpdateEmpListInfo($id,$title){
// 		$update = \DB::table($this->tables)->where(['id'=>$id])->update(['title'=>$title]);
// 		if($update){
// 			$msg = 'success';
// 		}
// 		else{
// 			$msg = null;
// 		}
// 		return $msg;
// 	}
// 	// FIX**
// 	public function StoreEmpListInfo($title){
// 		$store = \DB::table($this->tables)->insert(['title'=>$title]);
// 		if($store){
// 			$msg = 'success';
// 		}
// 		else{
// 			$msg = null;
// 		}
// 		return $msg;
// 	}

// public function DestroyEMP($id){
// 		foreach ($id as $key) {
// 			$past_employee = $this->past_employee($key);
// 		}

// 		if($past_employee['status'] ==200){
// 			$data=['id'=>$id,'employee'=>$past_employee['message']];$message='Store Successfully.'; $status=200;
// 		}else{
// 				$data=null; $message='Failed Past Employee !'; $status=500;
// 		}
// 		return ['message'=>$message,'data'=>$data,'status'=>$status];
// 	}
// // DESTROY ################################################################
// // PARAM ID(INT)
// 	public function DestroyEmpListInfo($id){
// 			foreach($id as $key){
// 				 $destroy = \DB::table($this->table)->where('id','=',$key)->delete();
// 			}
// 			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
// 			else{$msg = null; }
// 		return ['message'=>$msg,'status'=>$status];
// 	}
// // END DESTROY ###############################################################

// 	/* GET TYPE ID ##################################################
// 	* PARAM ROLE USER
// 	*/
// 	public function Get_Type($role,$employee_id){
// 		$data = \DB::select("SELECT c.local_it, c.employee_id
// 							FROM ldap b, emp c
// 							WHERE c.employee_id = b.employee_id and
// 							b.role_id = '$role' and c.employee_id = '$employee_id'");
// 		if(!isset($data)){
// 			$data = null;
// 		}
// 		return $data;
// 	}// END GET TYPE ################################################

// 	// SEARCH JOB #######################################
// 	public function Search($string){
// 		$check_lastName = strpos($string," ");
// 		$last_name = substr($string, $check_lastName+1, strpos($string,' '));

// 		if($last_name){
// 			if(isset($string)){
// 				$first_name = substr($string, 0, strpos($string,' ')); $last = $last_name;
// 				$data = \DB::select("select employee_id, concat(first_name,' ',middle_name,' ',last_name) as name
// 						from emp where first_name = '$first_name' and middle_name LIKE '$last_name%' limit 5");
// 			}else{ return ['data'=>null,'status'=>500]; }
// 		}else{
// 			$data = \DB::select("SELECT employee_id, concat(first_name,' ',middle_name,' ',last_name) as name
// 						from emp where first_name LIKE '$string%' limit 10");
// 			$status=200;
// 		}
// 		return ['data'=>$data,'status'=>$status];
// 	}
// // END SEARCH DOMAIN NAME ###################################
// 		public function past_employee($id){
// 		//$name = \DB::select("select last_name, concat(first_name,' ',middle_name,' ',last_name,' (PAST EMPLOYEE).') as name from emp where employee_id = '$id' ");//
// 		$name = \DB::table('emp')->where('employee_id','=',$id)->get(['last_name']);

// 		if(isset($name) && $name != '[]'){
// 			foreach ($name as $key) {
// 				$last_name = $key->last_name;
// 			}
// 			\DB::table('emp')->where('employee_id','=',$id)->update(['last_name'=>"(DEL).".$last_name,'ad_username'=>null]);
// 			$message = '(DEL)'.$last_name; $status=200;
// 		}else{ $message='ID Undefined.'; $status=500; }
// 		return ['message'=>$message,'status'=>$status];
// 	}

// 	public function GetFilterEmp(){
// 		$i=1; $res['job'] = array(); $res['status'] = array(); $res['sub_unit'] = array();
// 		$job = \DB::table('job')->get(['id','title']);
// 		$status = \DB::table('jobs_emp_status')->get(['id','title']);
// 		$sub_unit = \DB::table('sub_unit')->get(['id','sub_unit']);
// 		//$res['job'] = array();
// 		foreach ($job as $jobs) {
// 			$t = ['id'=>$jobs->id,'title'=>$jobs->title];
// 			array_push($t,$res['job'][$i]);
// 			$i++;
// 		}
// 		foreach ($status as $statuss) {
// 			//$res['status'][$i] = ['id'=>$statuss->id,'title'=>$statuss->title];
// 			$t = ['id'=>$statuss->id,'title'=>$statuss->title];
// 			array_push($t,$res['status']);
// 			$i++;
// 		}
// 		foreach ($sub_unit as $sub_units) {
// 			//$res['sub_unit'][$i] = ['id'=>$sub_units->id,'title'=>$sub_units->sub_unit];
// 			$t = ['id'=>$sub_units->id,'title'=>$sub_units->sub_unit];
// 			array_push($t,$res['sub_unit']);
// 			$i++;
// 		}
// 		return $res;

// 	}
// 	public function getAct($role_id)
// 	{
// 		$get = \DB::table('_action_emp')->where('role_id','=',$role_id)->get();
// 		return $get;
// 	}
// 	public function getReq($r)
// 	{
// 		if(isset($r['key'])){
// 			$decode = base64_decode($r['key']);
// 			$id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
// 			$get = \DB::select("SELECT a.local_it, b.role_id from emp a , ldap b where a.employee_id = b.employee_id and a.employee_id = '$id' ");
// 			if($get != '[]' && $get){
// 				foreach ($get as $key) {
// 					$type = [	'typeLocal' => $key->local_it,
// 								'roleBase' => $key->role_id,
// 								'employee_id'=>$id,
// 								'param'=>['inc'=>$r['inc'],'lc'=>$r['lc']]
// 							];
// 				}
// 			}else{ $type = null; }
// 		}else{ $type = null; }
// 		return $type;
// 	}
// 	public function ListOrderByLocal($type,$input)
// 	{
// 		$msg = ['success'=>['message'=>'List Employees : Show Records Data.','status'=>200],'error'=>['message'=>'List Employees : Empty Records Data.','status'=>200]];
// 		$checkAdd = $this->getAct($type['roleBase']);
// 		if(isset($type['action_create']) && $type['action_create'] == 1 && $checkAdd )
// 			{ $action_add = true; }
// 		else{ $action_add = false; }

// 			if($type['local_it'] == 1){
// 				if($input['lc'] != 0 && $input['inc'] != 0){
// 					$result = $this->ReadEmpInfoExpat(1,$type['employee_id'],null,$input); // GET RESULT MODEL
// 				}else{
// 					$result = $this->ReadEmpInfoExpat(1,$type['employee_id'],null,null); // GET RESULT MODEL
// 				}

// 				if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result['data']]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
// 				else{ $data = ['action_add'=>false,'list'=>null]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
// 			}
// 			elseif($type['local_it'] == 2){
// 				if($input['lc'] != 0 && $input['inc'] != 0){
// 					$result = $this->ReadEmpInfoLocal(1,$type['employee_id'],null,$input); // GET RESULT MODEL
// 				}else{
// 					$result = $this->ReadEmpInfoLocal(1,$type['employee_id'],null,null); // GET RESULT MODEL
// 				}

// 				if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result['data']]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
// 				else{ $data = ['action_add'=>false,'list'=>null]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
// 			}
// 			elseif($type['local_it'] == 3) {
// 				$result = $this->ReadEmpInfoLocalit(1,$type['employee_id'],null); // GET RESULT MODEL

// 				if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
// 				else{ $data = ['action_add'=>false,'list'=>null] ; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
// 			}
// 			else{ $message='Unauthorized'; $status=403; $data=null; }
// 		return ['message'=>$message,'status'=>$status,'data'=>$data];
// 	}

// 	public function Lists($type,$input,$param,$access)
// 	{
// 		$msg = ['success'=>['message'=>'List Employees : Show Records Data.','status'=>200],'error'=>['message'=>'List Employees : Empty Records Data.','status'=>200]];
// 		$checkAdd = $this->getAct($type['roleBase']);
// 		if(isset($type['action_create']) && $type['action_create'] == 1 && $checkAdd )
// 			{ $action_add = true; }
// 		else{ $action_add = false; }

// 			if($param == 'ExpatLocal'){
// 				//$input['lc'] = 1; $input['inc'] = 1;
// 				if($input['lc'] == 0){ $input['lc'] = 1;  }
// 				elseif($input['lc'] == 1){

// 					$input['lc'] = 1;
// 				}elseif($input['lc'] == 2){

// 					$input['lc'] = 2;
// 				}elseif($input['lc'] == 3){

// 					$input['lc'] = 3; //$input['inc'] = 1;
// 				}elseif($input['lc'] == 4){

// 					$input['lc'] = 4; //$input['inc'] = 1;
// 				}elseif($input['lc'] == 5){

// 					$input['lc'] = 5; //$input['inc'] = 1;
// 				}else{
// 					$data = ['action_add'=>$action_add,'list'=>null] ; $message=$msg['error']['message']; $status=$msg['error']['status'];
// 					return ['message'=>$message,'status'=>$status,'data'=>$data];
// 				}

// 				$result = $this->ReadEmpInfoExpat(1,$type['employee_id'],null,$input); // GET RESULT MODEL
// 				//return $result;
// 				if($access['create'] == 0 && $access['delete'] == 0 && $access['update'] == 0 && $access['read'] == 0){
// 					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>[]]; $message="Unauthorized"; $status=500; }
// 					else{ $data = ['action_add'=>false,'list'=>null]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
// 				}else{
// 					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
// 					else{ $data = ['action_add'=>false,'list'=>null]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
// 				}
// 			}
// 			elseif($param == 'Expat'){

// 				//$input['lc'] = 2; $input['inc'] = 1;
// 				if($input['lc'] == 0){

// 					$input['lc'] = 2; //$input['inc'] = 1;
// 				}elseif($input['lc'] == 2){

// 					$input['lc'] = 2; //$input['inc'] = 1;
// 				}
// 				elseif($input['lc'] == 1){

// 				 	$input['lc'] = 2; //$input['inc'] = 1;
// 				}
// 				else{
// 					$data = ['action_add'=>$action_add,'list'=>null] ; $message=$msg['error']['message']; $status=$msg['error']['status'];
// 					return ['message'=>$message,'status'=>$status,'data'=>$data];
// 				}

// 				$result = $this->ReadEmpInfoExpat(1,$type['employee_id'],null,$input); // GET RESULT MODEL
// 				//return $result;
// 				if($access['create'] == 0 && $access['delete'] == 0 && $access['update'] == 0 && $access['read'] == 0){
// 					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>[]]; $message="Unauthorized"; $status=500; }
// 					else{ $data = ['action_add'=>false,'list'=>null]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
// 				}else{
// 					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
// 					else{ $data = ['action_add'=>false,'list'=>null]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
// 				}
// 			}
// 			elseif($param == 'Local') {
// 				//$input['lc'] = 5; $input['inc'] = 1;
// 				if($input['lc'] == 0){

// 					$input['lc'] = 5; //$input['inc'] = 1;
// 				}elseif($input['lc'] == 5){

// 					$input['lc'] = 5; //$input['inc'] = 1;
// 				}elseif($input['lc'] == 4){

// 					$input['lc'] = 4; //$input['inc'] = 1;
// 				}elseif($input['lc'] == 3){

// 					$input['lc'] = 3; //$input['inc'] = 1;
// 				}else{
// 					$data = ['action_add'=>$action_add,'list'=>null] ; $message=$msg['error']['message']; $status=$msg['error']['status'];
// 					return ['message'=>$message,'status'=>$status,'data'=>$data];
// 				}
// 				$result = $this->ReadEmpInfoLocal(1,$type['employee_id'],null,$input); // GET RESULT MODEL
// 				//return $result;
// 				if($access['create'] == 0 && $access['delete'] == 0 && $access['update'] == 0 && $access['read'] == 0){
// 					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>[]]; $message="Unauthorized"; $status=500; }
// 					else{ $data = ['action_add'=>false,'list'=>null]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
// 				}else{
// 					if(isset($result)){ $data = ['action_add'=>$action_add,'list'=>$result]; $message=$msg['success']['message']; $status=$msg['success']['status']; }
// 					else{ $data = ['action_add'=>false,'list'=>null]; $message=$msg['error']['message']; $status=$msg['error']['status'];	}
// 				}
// 			}
// 			else{ $message='Unauthorized'; $status=403; $data=null; }
// 		return ['message'=>$message,'status'=>$status,'data'=>$data];
// 	}

// 	public function searchEmp($name,$id)
// 	{
// 		$get = \DB::select("SELECT employee_id , concat(first_name,' ',if(middle_name is null or middle_name = '',' ',middle_name),' ',last_name) as name from emp where first_name LIKE '$name%' and employee_id != '$id' and ad_username != ''" );
// 		if($get){ $data = ['data'=>$get,'status'=>200]; }
// 		else{ $data = ['data'=>[["name" => 'Data Not Found.']],'status'=>200];  }
// 		return $data;
// 	}
}
