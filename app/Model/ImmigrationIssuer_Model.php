<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class ImmigrationIssuer_Model extends Model {

	//
	Protected $tables = 'immigration_issuer';
	Protected $fields = ['title'];
	Protected $hiddden = ['id'];


	// FORM ImmigrationIssuer #######################################################
	// FIX**
	public function ReadImmigrationIssuer($type,$id){
		if($type == 'All'){
			$data = \DB::select("select id , title
					from ".$this->tables." ");
			if(!$data){
				$data = null;
			}
		}
		elseif($type == 'Sort'){
			$data = \DB::select("select id , title 
					from ".$this->tables." where id = '$id' ");	
			if(!$data){
				$data = null;
			}

		}
		return $data;
	
	}
	// FIX**
	public function UpdateImmigrationIssuer($id,$title){
		$update = \DB::table($this->tables)->where(['id'=>$id])->update(['title'=>$title]);
		if($update){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg; 
	}
	// FIX**
	public function StoreImmigrationIssuer($title){
		$store = \DB::table($this->tables)->insert(['title'=>$title]);
		if($store){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg;
	}
	// FIX**
	public function DestroyImmigrationIssuer($id){
		foreach ($id as $key) {
			$destroy = \DB::table($this->tables)->where('id','=',$key)->delete();
		}
		if($destroy){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg;
	}

	// END FORM ImmigrationIssuer

}
