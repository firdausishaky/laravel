<?php namespace Larasite\Model\JobHistory;

use Illuminate\Database\Eloquent\Model;

class EmpTerminate_Model extends Model {

// READ ###########################################
public function Read_EmpTerminate($employee_id,$id){
	return ['data'=>null,'status'=>500];
	if(isset($id)){
		return 1;
	}else{
		$result = \DB::table('view_emp_lang')->where('employee_id','=',$employee_id)->get();
		
		if(isset($result) && $result){ $data=$result; $status=200; }else{ $data=null; $status=500; }
	}
	return ['data'=>$data,'status'=>$status];
}
// END READ ###########################################

public function ShowEmp($id){
	$show = \DB::select("SELECT a.id , a.employee_id, 
					CONCAT(b.first_name,'', if(b.middle_name = null or b.middle_name = '','',b.middle_name),' ',b.last_name) as employee ,
					c.title as termination_reason, a.note, a.termination_date 
					FROM emp_termination_reason a , emp b, termination_reasons c
					where a.employee_id = b.employee_id and a.termination = c.id and a.employee_id = '$id' ");
	return $show;
}


//STORE #################################################
public function Store_EmpTerminate($id,$input){
	// check job
		if($this->check_job($input['reason']) == 200){
			$Get_Input = $this->SetInput($input);
			$Get_Input['employee_id'] = $id;
			
			//$store = \DB::table('emp_termination_reason')->insert($Get_Input);
			$store = \DB::SELECT("CALL insert_emp_termination_reasons('$id',$Get_Input[termination],'$Get_Input[termination_date]','$Get_Input[note]' )");
			 if(isset($store) != null || isset($store) != '[]'){
			 	$past_employee = $this->past_employee($id);
				if($past_employee['status'] ==200){
					$data=['id'=>$this->GetID($Get_Input['employee_id']),'employee'=>$past_employee['message'],'termination_date'=>$Get_Input['termination_date']]; $message='Store Successfully.'; $status=200;
				}else{
					$data=null; $message='Failed Past Employee !'; $status=500;
				}
			}else{$data=null; $message='Store Failed !'; $status=500;}
		}else{ $data=null; $message='ID Undefined !'; $status=500; }
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
// iiiD STORE ############################################

//UPDATE #################################################
public function Update_EmpTerminate($input,$id){
	$employee_id = $this->Get_emp($id);
	$Get_Input = $this->SetInput($input);
	//$emp = $this->Get_emp();
	$Get_emp['employee_id'] = $input['employee_id'];
	$update = \DB::table('emp_termination_reason')
				->where('id','=',$id)
				->update($Get_Input);
	return $update;
	}
// END STORE ############################################

// DESTROY DEPENDENT ################################################################
// PARAM ID(INT)
	public function Destroy_EmpTerminate($id){
			foreach($id as $key){
				//$this->move_file($filename);
				$destroy = \DB::table('emp_termination_reason')->where('id','=',$key)->delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################


// GET ID ###############################################
public function Get_emp($id){
		$data = \DB::select("Select employee_id from emp_termination_reason where id = $id");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID ###########################################


// GET ID EMP ###############################################
public function GetID($id){
		$data = \DB::select("Select id from emp_termination_reason where employee_id = '$id' order by id DESC limit 1");
		foreach ($data as $key) {
			$id_immigration = $key->id;
		}
		return $id_immigration;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
//PARAM ROLE USER
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################

public function SetInput($input){
	return([
		'termination'=>$input['reason'],
		'termination_date'=>$input['termination_date'],
		'note'=>$input['note']
		]);
}// end SETINPUT
public function check_job($id){
	$get = \DB::table('emp_termination_reason')->where('id','=',$id)->get(['termination']);
	if(isset($get) != null | isset($get) != '[]'){
		$status=200; $data=$get;
	}else{ $status=500; $data=null; }
	return $status; // response status
}//

// SEARCH JOB #######################################
	public function Search($string){ 
		$check_lastName = strpos($string," "); 
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));
		
		if($last_name){
			if(isset($string)){
				$start = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select id,title from job where title = '$start' limit 5");
			}
			else{ $data = null; $status=500;}
		}else{ $data = \DB::select("SELECT id,title from job where title LIKE '$string%' limit 5"); $status = 200;}
		if(isset($data) != null || isset($data) != '[]'){ $status=200; }else{ $data=null; $status=500; }
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################

// PAST EMPLOYEE ###########################################
	public function past_employee($id){
		//$name = \DB::select("select last_name, concat(first_name,' ',middle_name,' ',last_name,' (PAST EMPLOYEE).') as name from emp where employee_id = '$id' ");//
		$name = \DB::table('emp')->where('employee_id','=',$id)->get(['last_name']);
		if(isset($name) && $name != '[]'){
			foreach ($name as $key) {
				$last_name = $key->last_name;
				// $past_employee = $key->name;
			}
			\DB::table('emp')->where('employee_id','=',$id)->update(['last_name'=>"(PAST).$last_name",'ad_username'=>null]);
			\DB::table('ldap')->where('employee_id','=',$id)->update(['active_access'=>0]);	
			$message=$last_name.' (PAST_EMPLOYEES).'; $status=200;
		}else{ $message='ID Undefined.'; $status=500; }
		return ['message'=>$message,'status'=>$status];
	}
// END PAST EMPLOYEE ######################################

	public function check_emp($id){
		$r = \DB::select("SELECT  employee_id from emp_termination_reason where employee_id = '$id' ");
		if(isset($r) && $r){
			foreach ($r as $key) {
			 	$employee_id = $key->employee_id;
			}
			$data = $employee_id; $status=500;
		}else{ $data=null; $status=200; }
		return ['data'=>$data,'status'=>$status];
	} 

}
