<?php namespace Larasite\Model\JobHistory;

use Illuminate\Database\Eloquent\Model;

class JobHistory_Model extends Model {


protected $table = 'job_history';


  // READ ###########################################
private function view_jobhistory($id){
	$jobs = \DB::table('job')->get();
	$status = \DB::table('jobs_emp_status')->get();
	$sub_unit = \DB::table('department')->get();
	$job_his = \DB::table('job_history')->where('employee_id','=',$id)->get();
	if(!$job_his){
		$data = null;
		return $data;
	}
	foreach ($job_his as $key) {
		$data['change_date']			= $key->created_at;
		$data['employee_id'] 			= $key->employee_id;
		$data['training_agreement']		= $key->training_agreement;
		$data['contract_start_date']	= $key->contract_start_date;
		$data['probationary_start']		= $key->probationary_start;
		$data['fixed_term_contract']	= $key->fixed_term_contract;
		$data['emp_contract_start']		= $key->emp_contract_start;
		$data['emp_contract_end']		= $key->emp_contract_end;
		if($key->job){
			foreach ($jobs as $key_job) {
				if($key->job == $key_job->id){
					$data['job'] = $key_job->title;
					$data['file_masterjob'] = $key_job->filename;
				} 
			}
		}else{ $data['job'] = null; }
		if($key->status){
			foreach ($status as $key_status) {
				if($key->status == $key_status->id){
					$data['status'] = $key_status->title;
				}
			}
		}else{ $data['status'] = null; }
		if($key->sub_unit){
			foreach ($sub_unit as $key_sub) {
				if($key->sub_unit == $key_sub->id){
					$sb = $key_sub->name;
					$data['sub_unit'] = $key_sub->name;

					$subName = $key_sub->name;
					
					$departs = \DB::SELECT("select id, name, parent from department  where name = '$subName' ");
					$depart = $departs[0]->parent;
					$depart2 = \DB::SELECT("select * from department  where id = $depart");

					$data['sub-department-id'] = null;
					if($depart == 0){
						$data['sub-department']  = 'notHave';
					}else if($depart == 1){
						$data['sub-department'] = 'subRoot';
					}else{
						$data['sub_unit'] = $depart2[0]->name;
						$data['sub-department-id'] = $key_sub;
						$data['sub-department'] = 'subDepart';
					}
				}
			}
		}else{ $data['status'] = null; }
		if($key->filename){
			$data['file_jobhistory'] = $key->filename;
		}else{ $data['file_jobhistory'] = null; }
	}
	return $data;
}
public function view_logjobhistory($id){
	$i=0;
	$jobs = \DB::table('job')->get();
	$status = \DB::table('jobs_emp_status')->get();
	$sub_unit = \DB::table('department')->get();
	$logjob_his = \DB::select("SELECT * FROM log_jobhistory where employee_id = '$id' order by id desc");
	if($logjob_his){
		foreach ($logjob_his as $key) {
		$data['change_date']			= $key->created_at;
		$data['employee_id'] 			= $key->employee_id;
		$data['emp_contract_start']		= $key->contract_start_date;
		$data['emp_contract_end']		= $key->contract_end_date;
		$data['probationary']		= $key->probationary;
		$data['training_agreement']		= $key->training_agreement;
		$data['contract']		= $key->fixed_term_contract;
		$data['contract_start_date']		= $key->contract;
		$data['id']		= $key->id;
		if($key->job){
			foreach ($jobs as $key_job) {
				if($key->job == $key_job->id){
					$data['job'] = $key_job->title;
				}
			}
		}else{ $data['job'] = null; }
		if($key->status){
			foreach ($status as $key_status) {
				if($key->status == $key_status->id){
					$data['status'] = $key_status->title;
				}
			}
		}else{ $data['status'] = null; }
		if($key->sub_unit){
			foreach ($sub_unit as $key_sub) {
				if($key->sub_unit == $key_sub->id){
					$data['sub_unit'] = $key_sub->name;
				}
			}
		}else{ $data['sub_unit'] = null; }

		$a[$i] = $data;
		$i++;
		}
	}else{
		$a = null;
	}

	return $a;
}

public function check_update($input,$id){
	//return 1290;
	$a = "SELECT * from job_history where employee_id = '$id' and ";
	foreach ($input as $key => $value) {
		if($value != '' || $value != null){
			$a .= "$key = '$value' and ";
		}
	}
	$datas = \DB::select(substr($a, 0,strlen($a)-4));
	return $datas;
}

public function Read_JobHistory($id){
	if(isset($id) && $id != 'undefined'){
		$template = null;
		$result = $this->view_jobhistory($id);
		if(isset($result) && $result){
			$current_data=$result;
			$template = $this->Read_Doc();
			//$result2 = \DB::select("select change_date,id,employee_id, status, training_agreement, probationary_start, contract_start_date, fixed_term_contract, emp_contract_start, emp_contract_end, job,status, sub_unit from view_logjobhistory where employee_id = '$id' order by id desc ");
			$result2 = \DB::table('job_history')->where('employee_id','=',$id)->get();
			if($result2){
				$log_data = $this->view_logjobhistory($id);

			}else{
				$log_data = [];
			}
			$status=200; $message='View Data.';
		}else{ $current_data=null;$log_data=[]; $status=500; $message='Data not found'; }

	}else{ $message='Data not found.'; $status=404; $log_data=[]; $current_data=null; }
	return ['data'=>['current_data'=>$current_data,'log_data'=>$log_data,'document_template'=>$template],'status'=>$status,'message'=>$message];
}
// END READ ###########################################

// STORE #####
	public function Store_JobHistory($id,$input){
		$store_input = $this->set_input($input);
		if($input['filename']){
			$store_input['filename'] = $input['filename']; 	$store_input['path'] = $input['path'];
		}
		$store_input['employee_id']=$id;
			if(\DB::table($this->table)->insert($store_input)){
				if(isset($input['sub_unit'])){
					if($input['sub_unit'] == null)
					{
						$input['sub_unit'] = " ";
					}
					$up = \DB::SELECT("update emp set department=$input[sub_unit] where employee_id='$id' ");
				}
				$res = $this->view_jobhistory($id);
				$message = 'Store Successfully.'; $status=200; $data=$res;
			}
			else{
				$message = 'Store Failed.'; $status=500; $data=null;
			}
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
// END STORE ###

// UPDATE #########
	public function Update_JobHistory($id,$input){
		$update_input = $this->set_input($input);
		if($input['filename']){
			$update_input['filename'] = $input['filename']; 	$update_input['path'] = $input['path'];
		}
		$update = \DB::table($this->table)->where('employee_id','=',$id)->update($update_input);
		if(isset($input['sub_unit'])){
			if($input['sub_unit'] == null)
			{
				$input['sub_unit'] = " ";
			}
			$up = \DB::SELECT("update emp set department=$input[sub_unit] where employee_id='$id' ");
		}
		if($update == true){
			$result = $this->view_jobhistory($id);
			$message='Update Successfully.'; $status=200; $data = $result; }
		else{
			$message='Update Failed.'; $status=500; $data = null;}
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
// END UPDATE #######

// DESTROY DEPENDENT ################################################################
// PARAM ID(INT)
	public function Destroy_JobHistory($id){
		$i=1;
			foreach($id as $key){
				$check_file = $this->check_file_id($key);
				if ($check_file) {
					$move = $this->move_file($check_file);
					$destroy = \DB::table('job_history')->where('id','=',$key)->delete();
				}else{ $destroy = \DB::table('job_history')->where('id','=',$key)->delete(); }
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################

// GET ACTION TERMINATE
	private function actTerminate($id)
	{
		$role = \DB::table('ldap')->where('employee_id','=',$id)->get(['role_id']);
		foreach ($role as $key) { $role_id = $key->role_id; }
		$data = \DB::table('_action_emp')->where('role_id','=',$role_id)->get(['type_local']);

		if($data != '[]' && $data){ foreach ($data as $key){ $type_loc = $key->type_local; } }
		else{ $type_loc = null; }

		return $type_loc;
	}
	public function getTerminate($data){
		$type_loc = $this->actTerminate($data['key']);
		$local_it = \DB::table('emp')->where('employee_id','=',$data['emp'])->get(['local_it']);
		foreach ($local_it as $key) { $emp_loc = $key->local_it; }

		if( $type_loc == 2 && $emp_loc ){ return true; }
		elseif($type_loc == 0 && $emp_loc == 1){ return true; }
		elseif($type_loc == 1){
			if($emp_loc == 2){ return true; }
			elseif($emp_loc == 3){ return true; }
		}else{
			return false;
		}

	}
// END GET ACTION TERMINATE

// SET INPUT ##################################################
public function set_input($input){
	return([
		'emp_contract_start'=> $input['emp_contract_start'],
		'emp_contract_end'=> $input['emp_contract_end'],
		'training_agreement'=> $input['training_agreement'],
		'probationary_start'=> $input['probationary_start'],
		'contract_start_date'=> $input['contract_start_date'],
		'fixed_term_contract'=> $input['fixed_term_contract'],
		'job'=> $input['job'],
		'status'=>$input['status'],
		'sub_unit'=>$input['sub_unit']
		]);
}
// END SET INPUT ################################################

// GET ID ###############################################
public function Get_emp($id){
		$data = \DB::select("Select employee_id from emp where employee_id = '$id'");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID ###########################################


// GET ID EMP ###############################################
public function GetID($id,$file){
	if(isset($file)){
		$data = \DB::select("Select id from $this->table where filename = '$file'");
		foreach ($data as $key) {
			$id_history = $key->id;
		}
	}
	else{
		$data = \DB::select("Select id from $this->table where employee_id = '$id' order by id DESC limit 1");
		foreach ($data as $key) {
			$id_history = $key->id;
		}
	}
		return $id_history;
	}

public function GetDATE($employee_id){
		$data = \DB::select("Select created_at from job_history where employee_id = '$employee_id'");
		foreach ($data as $key) {
			$date = $key->created_at;
		}
		return $date;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
//PARAM ROLE USER
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id
							FROM ldap b, emp c
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################
// CHECK ID ###############################################################
// PARAM ID
	public function check_file_id($id){
		$r = \DB::select("SELECT filename from job_history where id = '$id' ");
		if($r){
			foreach ($r as $key) {
			$data = $key->filename;
			}
		}else{
			$data = null;
		}

		return $data;
	}
// END CHECK ID ###########################################################

// MOVE FILE IN TRASH ########################################################################
	public function move_file($filename){
		$get_data = \DB::select("SELECT `filename`, `path` from job_history where filename = '$filename' ");
		foreach ($get_data as $key) {
			$data['path'] = $key->path;
			$data['filename'] = $key->filename;
		}
		if(\Storage::disk('local')->exists($data['path']."/".$data['filename'])){
			\Storage::disk('local')->move($data['path']."/".$data['filename'], "/Trash/".$data['filename']);
			if( \Storage::disk('local')->exists("/Trash/".$data['filename']) ){
				$message = 'Update Successfully.';
				$status = 200;
			}else{
				\Storage::disk('local')->move("/Trash/".$data['filename'],$data['path']."/".$data['filename']);
				$message = 'gagal move file to trash';
				$status = 500;
			}
		}
		else{
			$message = 'File tidak ditemukan, atau file telah dihapus check folder trash';
			$status = 404;
		}
		return ['message'=>$message,'status'=>$status];
	}
// END MOVE FILE #################################################################################

public function ambil_data_terbaru($id){
	$data = \DB::select("SELECT change_date ,job, status, sub_unit, emp_contract_start, emp_contract_end  from view_logjobhistory where employee_id = '$id' order by id desc limit 1");
	if($data){
		foreach ($data as $key) {
		$datas['change_date'] = $key->change_date;
		$datas['job'] = $key->job;
		$datas['status'] = $key->status;
		$datas['sub_unit'] = $key->sub_unit;
		$datas['emp_contract_end'] = $key->emp_contract_end;
		$datas['emp_contract_start'] = $key->emp_contract_start;
		}
	}else{ $datas = null; }

	return $datas;
}
public function ambil_data_terminate($id){
	$data = \DB::select(" SELECT emp_termination_reason.id,emp_termination_reason.termination_date,emp_termination_reason.note,termination_reasons.title from emp_termination_reason,termination_reasons where emp_termination_reason.employee_id = '$id' and termination_reasons.id=emp_termination_reason.termination ");
	if($data){
		foreach ($data as $key) {
			$r['id'] = $key->id;
			$r['termination_date'] = $key->termination_date;
			$r['note'] = $key->note;
			$r['title'] = $key->title;
		}
		$data=$r;
	}else{$data=null;}
	return $data;
}
public function Read_Doc(){
	$get = \DB::select("select id,template_name from document_template where link_to_section LIKE '%job%' and link_to_section LIKE '%JOB%'");
	if($get){
		// foreach ($get as $key) {
		// 	$data['id'][] = $key->id;
		// 	$data['template_name'][] = $key->template_name;
		// }
		$data = $get;
	}else{
		$data = null;
	}
	return $data;
}
	public function getReq($id)
	{
			//$get = \DB::select("SELECT a.local_it, b.role_id from emp a , ldap b where a.employee_id = b.employee_id and a.employee_id = '$id' ");
			$get = \DB::select("SELECT a.local_it from emp a where a.employee_id = '$id' ");
			if($get != '[]' && $get){
				foreach ($get as $key) {
					$type = [	'typeLocal' => $key->local_it,
								'employee_id'=>$id
							];
				}
			}else{ $type = null; }
		return $type;
	}
}
