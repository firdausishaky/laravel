<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class Jobs_Model extends Model {
protected $tables = ['job','job_emp_status'];
protected $hidden = ['created_at','updated_at'];


	public function get_id($title){
		$fetch = \DB::table('job')->where('title','=',$title)->get(['id']);
		foreach ($fetch as $key) {
			$getid = $key->id;
		}
		return $getid;
	}
	public function StoreString($input)
	{
		if(\DB::table($this->tables[0])->insert($input)){
			$data = ['Store Successfully.',200,$this->get_id($input['title'])];
		}else{ $data = ['Store Failed.',500,null]; }
		return $data;
	}

	public function ReadJob($type,$id){
		if($type == 1){
			try {
				$data = \DB::select("select id, title , descript, filename 
						from job where title != '' order by title asc");	
			} catch (Exception $e){	$data=$e;	}
		}
		elseif($type == 2){
				if(isset($id)){
					try {
						$r = \DB::select("select id, title, descript, filename 
							from job where id = '$id' limit 1 ");
						if(!empty($r)){	$data=$r;	}else{	$data=null;	}	
					} catch (Exception $e){	$data=$e;	}
				}
				else{	$data=null;	}
		}else{	$data=null;	}
		return $data;
	}
// END READJOB ##############################################################

public function fetch_id($id){
	$data = \DB::select("select a.title , b.id from emp_details b , job a where b.job_id = a.id and b.id = $id ");
	foreach ($data as $key) {
		$value = $key->title;
	}
	return $value;
}

// DESTROY JOB ################################################################
// FIX***
	public function DestroyJob($id){
		$i = 1;
		$key_job=array(); $msg = array();
		$destroy = null;
		foreach ($id as $key) {
			$del = \DB::table('job_history')->where('job','=',$key)->get(['job']);
			$name = \DB::table('job')->where('id','=',$key)->get(['title']);
			foreach ($name as $keys) {
				$title = $keys->title;
			}
			if($del == false){
				array_push($key_job,$key);
				$status[$i]=200;	
			}else{array_push($msg,$title.' : Job Cannot delete.'); $status[$i]= 500;}
			$i++;
		}

		if(array_search(500, $status)){
			return ['message'=>$msg,'status'=>$status,'fix'=>500];	
		}else{
			foreach ($key_job as $key) {
				\DB::table('job')->where('id','=',$key)->delete();	
			}
			return ['message'=>'Destroy Successfully.','status'=>200,'fix'=>200];	
		}
	}
// END DESTROY JOB ###############################################################


// CHECK ID ###############################################################
// PARAM ID
	public function check_file_id($id){
		if(!$id){ return null;  }
		$r = \DB::select("SELECT filename from job where id = '$id' ");
		foreach ($r as $key) {
			$data = $key->filename;
		}
		return $data;
	}
// END CHECK ID ###########################################################


// UPDATE JOB ##########################################################
	public function JobUpdate($id,$title,$descript,$filename,$path){
		
		if(isset($id,$filename)){
			\DB::table('job')->where('id','=',$id)->update(['filename'=>$filename,'path'=>$path]);
			$status = 1;
		}
		elseif(isset($id,$title,$descript)){
			\DB::table('job')->where('id','=',$id)->update(['title'=>$title,'descript'=>$descript]);
			$status = 1;	
		}
		elseif(isset($id,$title)){
			\DB::table('job')->where('id','=',$id)->update(['title'=>$title]);
			$status = 1;	
		}
		else{$status = 'gagal';}
		return $status;
	}
// END UPDATE ########################################################


// GET ID ###########################################################################
	public function GetID($filename){
		$data = \DB::select("Select id from job where filename = '$filename' limit 1");
		foreach ($data as $key) {
			$id = $key->id;
		}
		return $id;
	}
//END GET ID ###########################################################################


// END MODEL JOB #


// READ EMPLOYEMNT STATUS #######################################################
	public function Read_EmpStatus($type,$id){
		if($type == 1){
			$data = \DB::select("select id , title from jobs_emp_status");
			if(!$data){	$data = null; }
		}
		elseif($type == 2){
			$data = \DB::table("jobs_emp_status")->where('id','=',$id)->get(['id','title']);
			if(!$data){	$data = null; }
		}
		return $data;
	}
// END EMPLOYEMENT STATUS ##################################################

// UPDATED STATUS ##################################################
	public function Update_EmpStatus($id,$title){
		$update = \DB::table('jobs_emp_status')
					->where('id','=',$id)
					->update(['title'=>$title]);
		if($update){ $msg='success';	}
		else{ $msg=null; }
		return $msg;
	}
//END UPDATED STATUS ##################################################

//STORE STATUS ##################################################
	public function Store_EmpStatus($title){
		$store = \DB::table('jobs_emp_status')->insert(['title'=>$title]);
		if($store){ $msg = 'success'; }
		else{ $msg = null; }
		return $msg; 
	}
//END STORE STATUS ##################################################
	
// DESTROY STATUS ##################################################
	public function Destroy_EmpStatus($id){
		$i = 1;
		$del = array();
		foreach ($id as $key) {
			$del = \DB::table('job_history')->where('status','=',$key)->get(['status']);
		}
		if(!empty($del)){ $msg='Status cannot delete.'; $status=500; }
		else{
			foreach($id as $key){
				 $destroy = \DB::table('jobs_emp_status')->where('id','=',$key)->delete();
			}
			if($destroy){ $msg='Delete Successfully.'; $status=200; }
			else{ $msg='Error Code.'; $status=500; }
		}
		return ['message'=>$msg,'status'=>$status];
	}
//END DESTROY STATUS ##################################################
	public function getStore($input){
		$get = \DB::table('job')->where('title','=',$input['title'])->where('descript','=',$input['descript'])->get();
		foreach ($get as $key) {
			$data[] = $key;
		}
		return $data;
	}
	public function Find_Status($id){
		return \DB::table('jobs_emp_status')->find($id);
	}
}