<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class Ldap_Model extends Model {

	// FORM ImmigrationIssuer #######################################################
	// FIX**
	public function Read_Ldap(){
			$file = storage_path()."/module_config.json";
			$data = file_get_contents($file, 0, null, null);
			$json = strip_tags(str_replace("jQuery.fs['scoreboard'].data =","",$data));
			$json_output = json_decode($json);
			
			//get data
			foreach ($json_output as $key) {
				$a['ldap_domain'] = $key->ldap_domain;
				$a['ldap_username'] = $key->ldap_username;
				$a['ldap_password'] = $key->ldap_password;
				$a['ldap_account_suffix'] = $key->ldap_account_suffix;
				$a['ldap_basedn'] = $key->ldap_basedn;
				$a['ldap_port'] = $key->ldap_port;
				$a['ldaps_port'] = $key->ldaps_port;
				$a['ldap_protocol'] = $key->ldap_protocol;
				$a['ldap_ssl'] = $key->ldap_ssl;
				$a['ldap_tls'] = $key->ldap_tls;
				$a['ldap_sso'] = $key->ldap_sso;
				$a['ldap_recursive_group'] = $key->ldap_recursive_group;
				$a['ldap_primary_group'] = $key->ldap_primary_group;
			}
			return $a;
			// $json_encode = json_encode($json_output);
			// file_put_contents($file, $json_encode);
	
	}
	// FIX**
	public function UpdateImmigrationIssuer($id,$title){
		$update = \DB::table($this->tables)->where(['id'=>$id])->update(['title'=>$title]);
		if($update){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg; 
	}
	// FIX**
	public function StoreImmigrationIssuer($title){
		$store = \DB::table($this->tables)->insert(['title'=>$title]);
		if($store){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg;
	}
	// FIX**
	public function DestroyImmigrationIssuer($id){
		foreach ($id as $key) {
			$destroy = \DB::table($this->tables)->where('id','=',$key)->delete();
		}
		if($destroy){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg;
	}

	// END FORM ImmigrationIssuer

}
