<?php  namespace  Larasite\Model\Leave\Entitlements;

use Illuminate\database\eloquent\Model;

class LeaveList_Model{

	public function checkRole($id)
	{

		$role = \DB::select("SELECT supervisor FROM emp_supervisor where supervisor='$id' ");
		$hr = \DB::SELECT(" select role.role_name from ldap,role  where ldap.employee_id='$id' and role.role_id = ldap.role_id ");
		if ($role != null){
			$role = "SuperVisor";
		}elseif (isset($hr[0]->role_name) && $hr[0]->role_name == "hr") {
			$role = "hr";
		}else{
			$role = "Employee";
		}
		return $role;
	}

	public  function checkName($name){
		$name = \DB::SELECT("CALL Search_emp_by_name($name)");

		return  $name[0]->employee_id;
	}

	public function hr($id){
		$hr = \DB::SELECT(" select role.role_name from ldap,role where ldap.employee_id=$id and role.role_id=ldap.role_id ");
		return  $hr[0]->role_name;
	}




}
