<?php  namespace  Larasite\Model\Leave\Entitlements;

use Illuminate\database\eloquent\Model;

class Leave_term {

  public function birthday(){
    return 1;
  }

  public function vacation_local($name, $from,$to){

    $contract = \DB::SELECT("select emp_contract_start,emp_contract_end,fixed_term_contract from job_history where employee_id='$name' ");
    $emp_stat = \DB::SELECT("select local_it from emp where employee_id='$name'");

  //  return 1;
    //rule for local apply before 20th every month
    $froms = substr($from, 5,2);
    if($froms == 20){
      return $data = ["message" => "Failed, can't apply request, year of service less than 1 year" , $status => 500, "data" => []];
    }

    // rule local it more than 1 year and non it more than 9 months
    if(isset($contract[0]->emp_contract_start) && $contract[0]->emp_contract_start != null && isset($contract[0]->emp_contract_end) && $contract[0]->emp_contract_start != null){
        $contract_start = substr($contract[0]->emp_contract_start,0,4);
        $contract_end = substr($contract[0]->emp_contract_end,0,4);
        $total = abs($contract_start) - abs($contract_end);
        if(isset($emp_stat) && $emp_stat != null){
          if($emp_stat == 3){
            if($total < 1){
                return $data = ["message" => "Failed, can't apply request, year of service less than 1 year" , $status => 500, "data" => []];
            }
          }else{
            $contract_month_start = substr($contract[0]->emp_contract_start,5,2);
            $contract_month_end = substr($contract[0]->emp_contract_end,5,2);
            $total_month = abs($contract_start) - abs($contract_end);
            if($contract_start <= $contract_end){
              $contract_month_start = 12 - $contract_month_start;
              $total_month =  $contract_month_start + $contract_month_end;
              if($total_month <= 9){
                return $data = ["message" => "Failed, can't apply request, year of service less than 9 month" , $status => 500, "data" => []];
              }
            }
          }
        }

    }elseif(isset($contract[0]->fixed_term_contract)) {
      $contract_start = substr($contract[0]->fixed_term_contract,0,4);
      $contract_end = date('Y-m-d', strtotime("+12 months", strtotime($contract_fix)));
      $total = abs($contract_start) - abs($contract_end);
      if(isset($emp_stat) && $emp_stat != null){
        if($emp_stat == 3){
          if($total < 1){
              return $data = ["message" => "Failed, can't apply request, year of service less than 1 year" , $status => 500, "data" => []];
          }
        }else{
          $contract_month_start = substr($contract[0]->emp_contract_start,5,2);
          $contract_month_end = substr($contract[0]->emp_contract_end,5,2);
          $total_month = abs($contract_start) - abs($contract_end);
          if($contract_start <= $contract_end){
            $contract_month_start = 12 - $contract_month_start;
            $total_month =  $contract_month_start + $contract_month_end;
            if($total_month <= 9){
              return $data = ["message" => "Failed, can't apply request, year of service less than 9 month" , $status => 500, "data" => []];
            }
          }
        }
      }

    }else{
      return $data = "passed";
    }

  }

  // public function vacation_expat($name,$from,$to){
  //   //request each request more than 5 days
  //   $leave = \DB::SELECT("select created_at from leave_request where leave_type=2 and employee_id='$name' ")
  //   if($leave != null){
  //     $leave = $leave[0]->created_at;
  //     $leave5 = strtotime(date('Y-m-d', strtotime("+5 days", strtotime($leave))));
  //     $today = strtotime(date("Y-m-d"));
  //     if( $leave5 > $today ){
  //       return $data = ["message" => "Failed, can't apply request, last request ".$leave[0]->created_at." ,apply request after 5 days from last request" , $status => 500, "data" => []];
  //     }
  //   }
  //
  //   //request each request interval 30 days
  //   if($leave != null){
  //     $leave = \DB::SELECT("select created_at,from_,to_ from leave_request where leave_type=2 and employee_id='$name' order by id DESC limit 1");
  //     if(isset($leave[0]->from_) && isset($leave[0]->to_)){
  //       $leave30 = strtotime(date('Y-m-d', strtotime("+30 days", strtotime($leave[0]->to_))));
  //       $today = strtotime(date("Y-m-d"));
  //       if($leave30 > $today){
  //         return $data = ["message" => "Failed, can't apply request, last request ".$leave[0]->created_at." ,apply Between one request to other request more than 30 days from last request" , $status => 500, "data" => []];
  //       }
  //     }
  //   }
  //
  //   //check request will be available if request 2 month before submit dan maximal 4 months before submit
  //   if(isset($from) && isset(to)){
  //     $today = strtotime(date("Y-m-d"));
  //     $froms = strtotime($from);
  //     $leave4m = strtotime(date('Y-m-d', strtotime("-4 months", strtotime($from))));
  //     $leave2m = strtotime(date('Y-m-d', strtotime("+2 months", strtotime($from))));
  //
  //     if($from > $leave2m){
  //       return $data = ["message" => "Failed, can't apply request, last request ".$leave[0]->created_at." ,apply minimal 2 months before request" , $status => 500, "data" => []];
  //     }
  //     if($from <  $leave4m){
  //       return $data = ["message" => "Failed, can't apply request, last request ".$leave[0]->created_at." ,apply maximal 4 months before request" , $status => 500, "data" => []];
  //
  //     }
  //   }
  //
  //   return $data = "passed";
  //
  // }
  //
  // public function sick_local(){
  //   $contract = \DB::SELECT("select emp_contract_start,emp_contract_end,fixed_term_contract from job_history where employee_id='$name' ");
  //   $emp_stat = \DB::SELECT("select local_it from emp where employee_id='$name'");
  //
  //   // rule local it more than 1 year and non it more than 9 months
  //   if(isset($contract[0]->emp_contract_start) && $contract[0]->emp_contract_start != null && isset($contract[0]->emp_contract_end) && $contract[0]->emp_contract_start != null){
  //       $contract_start = substr($contract[0]->emp_contract_start,0,4);
  //       $contract_end = substr($contract[0]->emp_contract_end,0,4);
  //       $total = abs($contract_start) - abs($contract_end);
  //       if(isset($emp_stat) && $emp_stat != null){
  //         if($emp_stat == 3){
  //           if($total < 1){
  //               return $data = ["message" => "Failed, can't apply request, year of service less than 1 year" , $status => 500, "data" => []];
  //           }
  //         }else{
  //           $contract_month_start = substr($contract[0]->emp_contract_start,5,2);
  //           $contract_month_end = substr($contract[0]->emp_contract_end,5,2);
  //           $total_month = abs($contract_start) - abs($contract_end);
  //           if($contract_start <= $contract_end){
  //             $contract_month_start = 12 - $contract_month_start;
  //             $total_month =  $contract_month_start + $contract_month_end;
  //             if($total_month <= 9){
  //               return $data = ["message" => "Failed, can't apply request, year of service less than 9 month" , $status => 500, "data" => []];
  //             }
  //           }
  //         }
  //       }
  //
  //   }elseif(isset($contract[0]->fixed_term_contract)) {
  //     $contract_start = substr($contract[0]->fixed_term_contract,0,4);
  //     $contract_end = date('Y-m-d', strtotime("+12 months", strtotime($contract_fix)));
  //     $total = abs($contract_start) - abs($contract_end);
  //     if(isset($emp_stat) && $emp_stat != null){
  //       if($emp_stat == 3){
  //         if($total < 1){
  //             return $data = ["message" => "Failed, can't apply request, year of service less than 1 year" , $status => 500, "data" => []];
  //         }
  //       }else{
  //         $contract_month_start = substr($contract[0]->emp_contract_start,5,2);
  //         $contract_month_end = substr($contract[0]->emp_contract_end,5,2);
  //         $total_month = abs($contract_start) - abs($contract_end);
  //         if($contract_start <= $contract_end){
  //           $contract_month_start = 12 - $contract_month_start;
  //           $total_month =  $contract_month_start + $contract_month_end;
  //           if($total_month <= 9){
  //             return $data = ["message" => "Failed, can't apply request, year of service less than 9 month" , $status => 500, "data" => []];
  //           }
  //         }
  //       }
  //     }
  //   }else{
  //     return $data = "passed";
  //   }
  // }

  // public function sick_expat(){
  //   return "passed";
  // }
  //
  // public function maternity_local(){
  //
  // }
  //
  // public function maternity_expat(){
  //
  // }
  //
  // public function paternity_expat(){
  //
  // }
  //
  // public function paternity_local(){
  //
  // }
  //
  // public function bereavement_local(){
  //
  // }
  //
  // public function berevement_expat(){
  //
  // }

}
