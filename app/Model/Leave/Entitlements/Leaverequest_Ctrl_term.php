<?php namespace Larasite\Model\Attendance\Request;

use Illuminate\Database\Eloquent\Model;

class Leaverequest_Ctrl_term extends Controller {

	public function domain($employee_id,$from,$to,$dob,$type){
	
		$brain_fuck = \DB::SELECT("CALL view_all_leave_balance");

		$arr = ['Birthday Leave','Vacation Leave','Enhance Vacation Leave','Sick Leave',
				'Maternity Leave','Paternity Leave','Bereavement Leave','Marriage Leave',
				'offday_oncall','Accumulation Day Off','Emergency Leave']; 


		$leave_type = $arr[$type-1];
		$arr = [];
		$local = '';

		//exploit data get prime employee ------------  
		foreach ($leave_type as $key => $value) {
			if($value->leave_type == $leave_type and $value->empx == $employee_id){
				$arr = $leave_type[$key];
				$local = $value->localit;
			}
		}
		//---------------------------------------------

		$retVal = ( $type == 1 ? birthleave : 
						($type == 2 ? vacLeave : (
							$type == 3 ? enLeave : (
								$type == 4 ? sickLeave : (
									$type == 5 ? matLeave : (
										$type ==  6 ? patLeave : (
											$type == 7 ? marLeave : (
										$type == 8 ? offLeave : (
									$type == 9 ? ocLeave : (
								$type == 10 ? adoLeave :(
						$type == 11 ? emerLeave)))))))))));

		$data = ['employee_id' => $employee_id, 'from' => $from, 'to' => $to, 'dob' => $dob, 'type' => $type, 'data' => $arr, 'local' => $local];
		return $this->$retVal($data);
	}

	public function vacleave(data){

		if($data['local'] != 1){
			$now = date('Y-m-d');
			

			//----------------------------------
		 	$ids = count($data['data']);
		 	$ids = $data['data'][$ids - 1]->ids; 
		 	$created_at = \DB::SELECT("select created_at  from leave_request where id = $ids and leave_type = 2");
		 	//----------------------------------

		 	//-----------------------------------
		 	$balance = $data['data'][$ids - 1]->days; 
		 	$days = ($data['data'][$ids - 1]->days) - ($data['data'][$ids - 1]->balance_leaves);
		 	//-----------------------------------

			$valRet = date_diff(date_create($data['to']),date_create($data['from']));
		 	$valMax = date_diff(date_create($now),date_create($data['from']));
		 	$valCreated = date_diff(date_create($now),date_create($created_at));

		 	//--------------------------------
		 	$valRet = $valRet->format("%a");
		 	$valMax = $valMax->format("%a");
		 	$valCreatedOpt = $valCreated->format("%R");
		 	$valCreated = $valCreated->format("%a");
		 	//---------------------------------

		 	$explode = ('-',$from);

		 	//-------------------------------------
		 	$get_data = \DB::SELECT("select * from vacation_leave_local where employee_id = '$employee_id' ");
		 	if(!isset($get_data[0]->depart) || $get_data[0]->depart == null ){
		 		$message = 'please fill department';
		 	}elseif(!isset($get_data[0]->data) || $get_data[0]->data == null){
		 		$message = 'leave table missing for your department';
		 	}elseif($explode[3] > 20){
		 		$message = 'please request before 20th'
		 	}elseif($dob <= $valRet){
		 		$message = '5++ days for every request'; 
		 	}elseif($valMax < 60){
		 		$message = 'lower than 2 month';
		 	}elseif ($valMax > 120) {
		 		$message = 'more than 4 months';
		 	}elseif ($valCreatedOpt == "-"){
		 		$message = "Oops, try to cheat date request";
		 	}elseif ($valCreated <= 30){
		 		$message = "every request minimum 30 days";
		 	}elseif($days < $valRet){
		 		$message = "can request more than * "+ $days;
		 	}else{
		 		$message 'ok';
		 	}

		 	//-------------------------------------

		 			return  $message;

		 	//--------------------------------
		}
	}

	public function enLeave($data){
		if($data['data'] == null){
			$messgae =  'please complete add department before request';
		}else{
			$datax =  end($data['data']);
			if($dob > $data['data'][0]->balance_leaves ){
				$message = 'not allowed days request';
			}else{
				$message = 'ok';
			}
		}

		return $message;
	}

	public function sickLeave($data){
		$datax = \DB::SELECT(select * from leave_sick_balance);
		$end_data = end($datax);

		if($datax == null){
			$emp = \DB::SELECT("select * from emp where employee_id = '$data[employee_id]' ");
			if($emp[0]->department == null){
				$message = 'please insert departmnent';
			}elseif($emp[0]->department != null){
				$depart = $emp[0]->department;
				$check = \DB::SELECT("select * from leave_sick where department = $depart ");
				if($check == null){
					$message = 'leave tabel not set up for department';
				}elseif($check[0]->entitlement_day == null){
					$entilited = $check[0]->entitlement_day;
					if($entilited == "Training Period"){
						$check_exist = \DB::SELECT("select training_agreement from job_history where job_history.employee_id = '$data[employee_id]'  ");
					}elseif($entilited == "contract_start"){
						$check_exist = \DB::SELECT("select contract_start_date from job_history where job_history.employee_id = '$data[employee_id]' ");
					}elseif($entilited == "Probationary Period")
						$check_exist = \DB::SELECT("select probationary_start from job_history where job_history.employee_id = '$data[employee_id]' ");
					}else{
						$check_exist = \DB::SELECT("select fixed_term_contract from job_history where job_history.employee_id = '$data[employee_id]' ");
					}	

					if($check_exist == null){
						$message = 'please fill contract date';
					}else{
						$message = 'ok';
					}	

				}
			}else{
				if($data['dob'] > $end_data[0]->days){
					$message = 'please request lower days'; 
				}
			}

			return $message;
	}

	public function matLeave($data){
		$date = date('Y-m-d');
		$dataz = \DB::SELECT("select datediff($to_,$from_) where employee_id = '$data[employee_id]' ");
		$gender =  \DB::SELECT("select gender from emp where employee_id =  '$data[employee_id]' ");
		$valRet = date_diff(date_create($date),date_create($data['from']));
		if($data != null){
			if($dataz > 150){
				$message = 'request lower than 150';
			}elseif(
				if($gender[0]->gender == 'male'){
					$message = 'request not for male';
				}else{
					$message = 'ok'
				}		
			)		

		}elseif($valRet->format("%a") < 60){
			$message = "request before 2 months";
		}else{
			$message = 'ok';
		}

		return $message;
	}

	public function patLeave($data){
		$brain_fuck = \DB::SELECT("select * from paternity_leave_balance where employee_id = '$data[employee_id]' ");
		$date = date('Y-m-d');
		$dataz = \DB::SELECT("select datediff($to_,$from_) where employee_id = '$data[employee_id]' ");
		$gender =  \DB::SELECT("select gender from emp where employee_id =  '$data[employee_id]' ");
		$valRet = date_diff(date_create($date),date_create($data['from']));

		if($valRet->format("%a") < 180){
			$message = 'request must avail before 6 month';
		}else if($data['local']  == 1){
			if($brain_fuck != null){
				if($dob > 7 and $dob <= 12){
					$check_date = end(\DB::SELECT("select  tenure_date_vl_expat fom vaction_leave_expat where employee_id =  '$data[employee_id]' "));
					if(!isset($check_date) && $check_date[0]->tenure_date_vl_expat != null ){
							$result = $check_date[0]->tenure_date_vl_expat + $brain_fuck[0]->balance_leaves;
							if($result < $dob){
								$message = 'vl + pl combination not enough day';
							}else{
								$message = 'ok';
							} 
					}
				}elseif($dob >12){
					$message = 'more than terms and condition allowed';
				}else{
					$message = 'ok';
				}				
			}else{
				if($dob < 12){
					$message = 'ok';
				}else{
					$message = 'not allowed';
				}
			}
		}else if($data['local']  != 1){
				if($dob > 7){
					$message = 'not aloowed';
				}else{
					$message = 'ok';
				}
		}

		return $message
	}

	public function marLeave($data){
		$valRet = date_diff(date_create($date),date_create($data['from']));
		if($valRet->format("%a") < 60){
			$message = 'must apply 2 month before due date';
		}else if($valRet->format("%a") < 120){
			$message = "must apply maximum 4 month before due date";
		}elseif($data[$dob] > 19){
			$message = 'not allowed';
		}elseif($data['local'] == 1 ){
			if($dob > 10){
				$message = 'not allowed';
			}
		}elseif($data['local'] == 1{
			if($dob > 19){
				$message = "not allowed";
			}else{
				$check_date = end(\DB::SELECT("select  tenure_date_vl_expat fom vaction_leave_expat where employee_id =  '$data[employee_id]' "));
				if(!isset($check_date) && $check_date[0]->tenure_date_vl_expat != null ){
					$result =  7 + $brain_fuck[0]->balance_leaves;
					if($result < $dob){
						$message = 'vl + pl combination not enough day';
					}else{
						$message = 'ok';
					} 
				}else{
					$message = 'not allowed';
				}
			}
		}else{
			$message = 'ok';
		}

		return $message;
	}


	

}

// class Leaverequest_Ctrl_term extends Controller {

// 	function terms($from,$to,$dob,$request,$employee_id,$department,$leave_type){
// 		$data  = [];
// 		$select  = \DB::SELECT("select id from leave_type where leave_type = '$leave_type' ");
// 		$job_history = \DB::SELECT("select emp.local_it,job_history.training_agreement,job_history.probationary_start,job_history.contract_start_date,job_history.fixed_term_contract,emp.local_it from job_history where job_history.employee_id =  $employee_id  and job_history.employee_id=emp.employee_id ");
		
// 		$training = (!isset($job_history[0]->training_agreement) || $job_history[0]->training_agreement == null ? null : $job_history[0]->training_agreement);
// 		$proba = (!isset($job_history[0]->probationary_start) || $job_history[0]->probationary_start == null ? null : $job_history[0]->probationary_start); 
// 		$contract_start = (!isset($job_history[0]->contract_start_date) || $job_history[0]->contract_start_date == null ? null : $job_history[0]->contract_start_date);
// 		$fixed =  (!isset($job[0]->fixed_term_contract) || $job[0]->fixed_term_contract == null ? null : $job[0]->fixed_term_contract);

// 		if($select == null || $select == []){
// 			return $data['message'] = 'error id leave type not found';	
// 		}
		
// 		$diff = date_diff(date_create($to),date_create($from));
//         $diff = $diff->format("%a");
//         $operator = $diff->format("%R");

//         if($operator == "-"){
//         	return $data['message'] = "please make a sure from and start it'correct setup";
//         }

// 		//get data requirement
// 		$leave_code = $select[0]->id;
// 		$local = ($job_history[0]->local_it == 2 || $job_history[0]->local_it == 3 ? 2 :  1);

// 		//tabel
// 		$table = ($leave_code == 2 ? 'leave_vacation' :( $leave_code == 3 ? 'leave_enhance' : ($leave_code == 4 ? 'leave_sick' : 'leave_bereavement')));
// 		$table_detail = ($leave_code == 2 ? 'leave_vacation_detail' : ($leave_code == 4 ? 'leave_sick_detail' : 'leave_bereavement_detail'));

// 		if($leave_code ==  2 || $leave_code ==  4 || $leave_code ==  7 ){
// 			$vlneeded =\DB::SELECT("select id,applied_for,entitlement_start from $table where employee_id='$employee_id' and department = $department and applied_for = $local");
// 			if(!isset($vlneeded[0]) || $vlneeded == []){
// 				return $data['message'] = 'please set up leave tabel before requested';
			
// 			}else{
// 				$dateemp = $vlneeded[0]->entitlement_start;
// 				$entitlement 	 = (isset($vlneeded[0]->entitlement_start) == $vlneeded[0]->entitlement_start != null ? $vlneeded[0]->entitlement_start : null );
// 				if($entitlement != null){
// 					if($entitlement == "Training Period") $entitlement =  $training;
// 					if($entitlement == "Probationary Period") $entitlement =  $prob;
// 					if($entitlement == "contract_start") $entitlement =  $contract_start;
// 					if($entitlement == "Fixed Term Contract") $entitlement =  $fixed;

// 					if($entitlement != null){
// 						$date = date('Y');
// 						$entitlement = explode("-",$entitlement);
// 						$tenure = date_diff(date_create($to),date_create($entitlement[0]));
//        					$tenure = $tenure->format("%a");	
//        					$yearin = ( $tenure >= 1  && $tenure <= 365 ? 1 : ( $tenure  >= 366 && $tenure <= 730 ?  2 : ($tenure >= 731 && $tenure <= 1098 ? 3 : 4 )));
// 					}
// 				}else{
// 					if($entitlement == null){
// 						return $data['message'] = "can't found entitlement days";
// 					}
// 				}
// 			}
// 		}

// 		$idx = $vlneeded[0]->id;
		

// 		if($leave_code ==  2){
// 			$db_data =  \DB::SELECT("select year,days from $table_detail where vacation_id = $idx and year =  $yearin"  );

// 		}
// 		if($leave_code ==  4){
// 			$db_data =  \DB::SELECT("select days from $table_detail where leave_sick_id = $idx and year =  $yearin ");	
// 		}
// 		if($leave_code ==  7){
// 			$db_data =  \DB::SELECT("select relation_to_deceased from $table_detail where bereavement_id = $idx and year =  $yearin");	
// 		}

// 		$data = ['employee_id' => $employee_id, 'db' => $db_data, 'department' => $department, 'from' => $from, 'to' => $to, 'table' => $table, 
// 				 'dob' => $dob, 'request' => $request,
// 			     'table_detail' => $table_detail, 'leave_code' => $leave_code, 'tenure' => $yearin, 'localit' => $local, 'entitlement'=> $entitlement,
// 			     'message' =>'success']; 	

// 	}

// 	function vl($data){
// 		$data = [];
// 		$date = date('Y-m-d');

// 		//2 month
// 		if($data['localit'] ==  1){
// 			$diff = date_diff(date_create($data['to']),date_create($data['from']));
// 	        $diff = $diff->format("%a");	
// 	        if($dob <= 5){
// 	        	return $data['message']	= "minimum each aplication must above 5 days or more";
// 	        }
// 	        if($diff <= 60){
// 	        	return $data['message']	= "minimum application bettwen request date and current can't lower than 2 months";
// 	        }
// 	        if($diff >= 120  ){
// 	        	return $data['message']	= "minimum application bettwen request date and current can't more than 4 months";
// 	        }
// 	    }else{
// 	    	$diff = date_diff(date_create($date),date_create($data['entitlement']));
// 	        $diff = $diff->format("%a");
// 	        $job = \DB::SELECT("select * from job_history,job where job_history.employee_id =  $data[employee_id] and job_history.job = job.id")
	        
// 	        if($job != null || $job == []){
// 	        	return $data['message']	= "local employee must save job title before request";
// 	        }else{
// 	        	if($job[0]->job == 'TI' || $job[0]->job == 'ti'){
// 	        		if($diff <= 356){
// 	        			return $data['message'] = "For IT at least 1 year of service";						
// 	        		}
// 	        	}else{
// 	        		if($diff <= 270){
// 	        			return $data['message'] = "For non-IT at least 1 year of service";						
// 	        		}
// 	        	}
// 	        }

// 	        $dateNow = date('d');
// 	        if($dateNow > 20){
// 	        	return $data['message'] = "for local employee can't submitted above 20th ";
// 	        }

// 	    }

// 		$leave_request \DB::SELECT("select * from leave_request where employee_id =  $data[employee_id] order by id desc limit 1");
// 		if($leave_request != null || $leave_request = []){
// 			if($data['localit'] == 1){
// 				$created_at = \DB::SELECT("select created_at from leave_request where order by id DESC limit 1");
// 				$last_req = $created_at[0]->created_at
// 				$dates = date_diff(date_create($date),date_create($last_req));
// 	        	$dates = $diff->format("%a");

// 	        	if( $dates >= 5 && $dates <= 30){
// 	        		return $data['message'] = "for expat employee each application submitted minimum 5 days";
// 	        	}
// 			}
// 		}
// 	}		

// 	function pl($data){
// 		$date = date('Y-m-d');
// 		$gender =  \DB::SELECT("select gender  from emp where employee_id =  $data[employee_id] ");
// 		if($gender[0]->gender == "Female" ){
// 			return $data['message']	= "only male please";
// 		}

// 		if($data['localit'] == 1){	
// 			if($data['dob'] > 7){
// 				$check =  \DB::SELECT("select vl from vladoCustom where employee_id = $data[employee_id] and tenure =  $data[tenure] ");
// 				if($check[0]->vl <= 5){
// 					return $data['message']	= "can't  apply request vacation + paternity less than 5 days";				
// 				}  
// 			}
// 		}else{
// 			if($data['dob'] > 7){
// 				return $data['message']	= "can't more than 7 days";
// 			}
// 		}	
// 	}

// 	function bl($data){

// 	}

// 	function el(){

// 	}

// 	function ml($data){
// 		$date = date('Y-m-d');

// 		if($data['localit'] == 1){	
// 			if($data['dob'] > 7){
// 				$check =  \DB::SELECT("select vl from vladoCustom where employee_id = $data[employee_id] and tenure =  $data[tenure] ");
// 				if($check[0]->vl <= 12){
// 					return $data['message']	= "can't  apply request vacation + paternity less than 12 days";				
// 				}  
// 			}

// 			$date_req = date_diff(date_create($date),date_create($data['from']));
// 			$diff = $date_req->format("%a");

// 			if($diff > 120){
// 				return $data['message']	= "can't  apply request vacation + paternity above than  4 mo";
// 			}

// 			if($diff <= 60){
// 					return $data['message']	= "can't  apply request vacation + paternity less than 2 mo";
// 			}
// 		}else{
				
// 		}

		
// 	}

// 	function matl($data){
// 		$date = date('Y-m-d');
// 		$gender =  \DB::SELECT("select gender  from emp where employee_id =  $data[employee_id] ");
// 		if($gender[0]->gender == "Male" ){
// 			return $data['message']	= "only female please";
// 		}

// 		if($data['localit'] == 1){
// 			if($data['dob'] >= 150){
// 				return $data['message']	= "Oops, only allowed 150 days, no more";	
// 			}
// 		}else{
// 			$role = \DB::SELECT("select  role_name from ldap,role where ldap.employee_id = $data[employee_id] and ldap.role_id = role.role_id ")
// 			if(!isset($role)){
// 				return $data['message']	= "please check your active directory";	
// 			}else{
// 				$roles = 0;
// 				$role  = $role[0]->role_name;
// 				$explode = explode(" ",$role);
// 				foreach ($explode as $key => $value) {
// 					if($value == 'hr' || $value == 'HR'){
// 						$roles  += 1;
// 					}
// 				}

// 				if($roles == 0){
// 					return $data['message']	= "it only can request by HR";	
// 				}
// 			}

// 			$date_req = date_diff(date_create($date),date_create($data['from']));
// 			$diff = $date_req->format("%a");

// 			if($diff <= 60){
// 				return $data['message']	= "can't lower than 2 month before due date";
// 			}
// 		}

// 	}

// 	function sl($data){
// 		if($data['local_it'] == 1){
// 			$diff = date_diff(date_create($date),date_create($data['entitlement']));
// 	        $diff = $diff->format("%a");
// 	        $job = \DB::SELECT("select * from job_history,job where job_history.employee_id =  $data[employee_id] and job_history.job = job.id")
	        
// 	        if($job != null || $job == []){
// 	        	return $data['message']	= "local employee must save job title before request";
// 	        }else{
// 	        	if($job[0]->job == 'TI' || $job[0]->job == 'ti'){
// 	        		if($diff <= 356){
// 	        			return $data['message'] = "For IT at least 1 year of service";						
// 	        		}
// 	        	}else{
// 	        		if($diff <= 270){
// 	        			return $data['message'] = "For non-IT at least 1 year of service";						
// 	        		}
// 	        	}
// 	        }
// 		}
// 	}
// }