<?php namespace Larasite\Model\Leave\Entitlements;

use Illuminate\Database\Eloquent\Model;

class leaverequest_Model {

		private $key = " ";




		function getMessage($message = array(), $data = array(), $status_code = "", $access = array()){
			return response()->json(['header' => ['message' => $message, 'status' => $status_code, "access" =>$access ], 'data' => $data], $status_code);
		}

		public function index($db = array(), $access = array()){
			if($db == null){
				return $this->getMessage("Data not exist" , [],200);
			}else if($db != null){
				if ($access['create'] == 0 and $access['delete'] == 0 and $access['read'] == 0 and $access['update'] == 0 ){
					return $this->getMessage("Unauthorized",$db, 200, $access);
				}else{
					return $this->getMessage("success",$db, 200,$access);
				}
			}else{
				return $this->getMessage("failed to load " , null,500, $access);
			}
		}

		function search($validator = array(), $query = array(), $access = array()){
		if($validator->fails()){
			$check = $validator->errors()->all();
			return $this->getMessage('format input invalidate',null,500,$access);
		}else{

			return $this->index($query,$access);
		}
		return $this->getMessage("input data undefined",null,500,$access); 
		}


		function store($db= array(), $check = array()){
			if($check->fails()){
				$validation = $check->errors()->all();
				return $this->getMessage('invalidate input format',$validation,500);
			}else{
				if(isset($db)){
					return $this->getMessage('success',null,200);
				}else{
					return $this->getMessage('failed',null,500);
				}
			}
		}

		
		public function check_image($ext,$size,$image,$path,$access){
			if($ext == "jpg" || $ext == "png"){
				if($size > 1000000){
					return $data = "size";
					//return $this->getMessage("failed image format supported but size of image than 1 MB",[],200,$access);
				}else{
					$rename = 
					$image->move(storage_path($path),$image->getClientOriginalName());

				}
			}else{	
				$data = "format";
				//return $this->getMessage("unsupported format",[],200,$access);
			}
		}		

		public function check_imgPDF($ext,$size,$image,$path){
			$rename = str_random(6).".".$image->getClientOriginalExtension();

  			if($ext == "jpg" || $ext == "png"){
				if($size > 104857600){
					return $data = "size";
				}else{
					\Input::file('file')->move(storage_path($path),$rename);
					return $rename;
				}
			}elseif ($ext == "pdf" ){
				if($size > 1048576000){
					return  $data ="size";
				}else{
					\Input::file('file')->move(storage_path($path),$rename);
					return $rename;	
				}
			}else{
					return $data = "format";
			}
		}



}