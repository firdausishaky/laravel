<?php namespace Larasite;

use Illuminate\Database\Eloquent\Model;

class LeaveModel extends Model {

	public function index()
	{
		$type = \DB::select("CALL ");
		if (!$type){
			$data = null;
		}
		else{
			$data = $type;
		}
		return $data;
	}
	
	public function search($i)
	{
		$data = \DB::select("CALL ($i[type]),$i[for]");
		if (!$data){
			$data = null;
		}
		else{
			$data = $data;
		}
		return $data;
	}

	public function add($i)
	{
		if (isset($i['type'])){
			if ($i['type'] == "Vacation Leave"){
				$addVacation = \DB::select("CALL XXXXXXXX($i[for],$i[department],$i[entitlement_from],$i[entitlement_day],$i[years],$i[days],$i[off_day])");
				if (!$addVacation){
					$data = null;
				}
				else{
					$data = $addVacation;
				}
				return $data;
			}
			else if ($i['type'] == "Bereavement Leave"){
				$addBereavement = \DB::select("CALL xxxxx()");
				if (!$addBereavement){
					$data = null;
				}
				else{
					$data = $addBereavement;
				}
				return $data;
			}
			else if ($i['type'] == "Sick Leave"){
				$addSick = \DB::select("CALL xxxxx()");
				if (!$addSick){
					$data = null;
				}
				else{
					$data = $addSick;
				}
				return $data;
			}
			else if ($i['type'] == "Enhance Leave"){
				$addEnhance = \DB::select("CALL xxxxx()");
				if (!$addEnhance){
					$data = null;
				}
				else{
					$data = $addEnhance;
				}
				return $data;
			}
			else{
				return $data = null;
			}
		}
		else{
			return $data = null;
		}
	}

	public function update($id)
	{
		if (isse($i['type'])){
			if ($i['type'] == "Vacation Leave"){
				$updateVacation = \DB::select("CALL XXXXXXXX($i[for],$i[department],$i[entitlement_from],$i[entitlement_day],$i[years],$i[days],$i[off_day])");
				if (!$updateVacation){
					$data = null;
				}
				else{
					$data = $updateVacation;
				}
				return $data;
			}
			else if ($i['type'] == "Bereavement Leave"){
				$updateBereavement = \DB::select("CALL xxxxx()");
				if (!$updateBereavement){
					$data = null;
				}
				else{
					$data = $updateBereavement;
				}
				return $data;
			}
			else if ($i['type'] == "Sick Leave"){
				$updateSick = \DB::select("CALL xxxxx()");
				if (!$updateSick){
					$data = null;
				}
				else{
					$data = $updateSick;
				}
				return $data;
			}
			else if ($i['type'] == "Enhance Leave"){
				$updateEnhance = \DB::select("CALL xxxxx()");
				if (!$updateEnhance){
					$data = null;
				}
				else{
					$data = $updateEnhance;
				}
				return $data;
			}
			else{
				return $data = null;
			}
		}
		else{
			return $data = null;
		}
	}

	public function delete($id)
	{
		// $delete = \DB::select("CALL ");
		$rule = ['','undefined',NULL];
  		if(!(in_array($id,$rule))){ 
  			$temp = explode(",",$id);
  		}

  		foreach ($temp as $key) {
	  		$delete = \DB::select("CALL ($key)");
	  	}

		if ($delete[0]->Status == "Success"){
			$status = "ok";
		}
		else{
			$status = "error";
		}
		return $status;
	}
}