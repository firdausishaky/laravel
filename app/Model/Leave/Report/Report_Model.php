<?php  namespace  Larasite\Model\Leave\Report;
use Illuminate\database\eloquent\Model;

class Report_Model{

		private $key = " ";

		function getMessage($message = array(),$data = array(),$status_code = "", $access = array() ){
			return response()->json(['header' => ['message' => $message, 'status' => $status_code, "access" => $access], 'data' => $data], $status_code);
		}

		public function index($db = array(), $access = array()){
			if($db == null){
				return $this->getMessage("Data not exist" , [],200, $access);
			}else if($db != null){
				if ($access['create'] == 0 and $access['delete'] == 0 and $access['read'] == 0 and $access['update'] == 0 ){
					return $this->getMessage("Unauthorized",$db, 200, $access);
				}else{
					return $this->getMessage("success",$db, 200,$access);
				}
			}else{
				return $this->getMessage("failed to load " , null,500, $aceess);
			}
		}

		// function __construct(){
		// 	if(\Input::get('key')){
		// 	$input = \Input::get('key');
		// 	$data = explode("-",base64_decode($input));

		// 	$this->key = $data[
		// 	}
		// }

		function search($validator = array(), $query = array(), $access = array()){
			if(!isset($validator) || !isset($query)){
				if($validator->fails()){
					$check = $validator->errors()->all();
					return $this->getMessage('format input invalidate',$check,$null);
				}else{
					return $this->index($query,$access);
				}
			}
			return $this->getMessage("input data undefined",null,500,$access); 
		}


		function store($db= array(), $check = array()){
			if($check->fails()){
				$validation = $check->errors()->all();
				return $this->getMessage('invalidate input format',$validation,500);
			}else{
				if(isset($db)){
					return $this->getMessage('success',null,200,$access);
				}else{
					return $this->getMessage('failed',null,500,$access);
				}
			}
		}


}