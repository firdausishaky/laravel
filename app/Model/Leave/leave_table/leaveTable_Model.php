<?php namespace  Larasite\Model\Leave\leave_table;

use Illuminate\Database\Eloquent\Model;

class leaveTable_Model extends Model {

	public function getMessage($message = array(), $status = "", $data =  array()){
		return response()->json(['header' => ['message' => $message, 'status' => $status], 'data' => $data],$status);
	}

	public function getMessageAccess($message = array(), $status = array(), $data =  array(), $access = array()){
		return response()->json(['header' => ['message' => $message, 'status' => $status, 'access' => $access], 'data' => $data],$status);
	}

	private $token = "";
	private $id_employee = "";

	function key(){
		$key = \Input::get('key');
		$keys= base64_decode($key);
		$key_ex = explode('-',$keys);
		return $this->id_employee = $key_ex[1];
	}
	public function index($db = array()){
		if($db == null){
			return $this->getMessage("Data not exist" ,200, []);
		}else if($db != null){
			return $this->getMessage("success", 200,$db);
		}else{
			return $this->getMessage("failed to load ",500, null);
		}
	}

	public function indexAcess($message = array(), $status = array(), $data = array(),$access = array()){
		if($data == null){
			return $this->getMessageAccess($message, $status, $data, $access);
		}else if($data != null){
			return $this->getMessageAccess($message, $status, $data, $access);
		}else{
			return $this->getMessageAccess($message, $status, $data, $access);
		}
	}



	public function search($validation= array(), $db= array()){
		if($validation->fails()){
			$check = $validation->errors()->all();
			return $this->getMessage("invalidate input format" ,500, $check);
		}else{
			return $this->index($db);
		}
	}

	public function  search_null($type = ""){
		$query = \DB::SELECT("select id from leave_type where leave_type='$type' ");
		if(!$query){
			return 0;
		}else{
			return $query[0]->id;
		}
	}

	public function check_employee($data = ""){

		if($data == 1){
			return"Expat";
		}
		if($data == 2){
			return "Local";
		}
		if($data == 3){
			return "Null";
		}
	}
	public  function db_query($type = "", $for = "", $status = ""){

	        if($type == "2"){
	        		 $data = \DB::SELECT("CALL view_leave_vacation($type,$for)");

	 		 	if($data != null){
	        		// search child
		            foreach ($data  as $key => $value) {
	                  	$subdata =  ["year" => $value->year, "days" => $value->days, "offday_oncall" => $value->offday_oncall];
			        	$datax[$value->id][]= $subdata;
			        }

		            //search parent
		            foreach ($data  as $key => $value) {
		            	$subdata =  ["id" => $value->id, "LeaveRules" => $value->LeaveRules];
		                $datay[$value->id][]= $subdata;
		            }

		            //merge parent and child for happy life
               		foreach ($datax as $key => $value) {
                          		$explode = explode(".",$datay[$key][0]['LeaveRules']);
                          		$applied = $this->check_employee($explode[0]);
                          		if(isset($explode[0])){
                          			$department = preg_replace('/\s\s+/', ' ', $explode[1]);
                          		}else{
                          			$department = null;
                          		}

                          		if(isset($explode[1])){
                          			$ent_day = preg_replace('/\s\s+/', ' ', $explode[2]);
                          		}else{
                          			$ent_day = null;
                          		}

                          		if(isset($explode[2])){
                          			$ent_start = preg_replace('/\s\s+/', ' ', $explode[3]);
                          		}else{
                          			$ent_start = null;
                          		}

                          		// $ent_day = preg_replace('/\s\s+/', ' ', $explode[2]);
                          		// $ent_start = preg_replace('/\s\s+/', ' ', $explode[3]);

                          		$dataz[] = [
											"status" => $status,
										    "id"			 => $datay[$key][0]['id'],
							 			    "Appliedfor" 	=>"Applied For : ".ltrim($applied),
							 			    "Department" 	=>"Department : ".ltrim($department),
							 			    "StartFrom" 	=>"StartFrom : ".ltrim($ent_day),
										    "Entitlementdays" 	=>"Entitlementdays : ".ltrim($ent_start),
		 			     					"subvacation" 	=> $datax[$key]
		 			 						];
                			}

              	}else{ $dataz = [];}

	        }elseif($type == "4"){

	        	$data = \DB::SELECT("CALL View_leave_sick($type,$for)");
		        if($data != null){
		        // search child
			    foreach ($data  as $key => $value) {
			        $subdata =  ["year" => $value->year, "days" => $value->days];
					$datax[$value->id][]= $subdata;
			    }

			    //search parent
			    foreach ($data  as $key => $value) {
			        $subdata =  ["id" => $value->id, "LeaveRules" => $value->leave_rules];
			        $datay[$value->id][]= $subdata;
			    }

			    //merge parent and child for happy life
	            foreach ($datax as $key => $value) {
	                          		$explode = explode(".",$datay[$key][0]['LeaveRules']);
	                          		$applied = $this->check_employee($explode[0]);
	                          		$department = $explode[1];
	                          		$ent_day = $explode[2];
	                          		$ent_start = $explode[3];
	                          		$dataz[]  = ["status" => $status,"id" => $datay[$key][0]['id'], "Appliedfor" =>"Applied For : ".ltrim($applied), "Department" =>"Department : ".ltrim($department), "StartFrom"=>"StartFrom : ".ltrim($ent_day),"Entitlementdays" =>"Entitlementdays : ".ltrim($ent_start), "subsick" =>$datax[$key] ];
	            }
	        }else{ $dataz = [];}

	    	}elseif($type == "5"){ // maternity

	        	$data = \DB::SELECT("CALL view_leave_maternity($type,$for)");
		        if($data != null){
		        // search child
			    foreach ($data  as $key => $value) {
			        $subdata =  ["year" => $value->year, "days" => $value->days];
					$datax[$value->id][]= $subdata;
			    }

			    //search parent
			    foreach ($data  as $key => $value) {
			        $subdata =  ["id" => $value->id, "LeaveRules" => $value->leave_rules];
			        $datay[$value->id][]= $subdata;
			    }

			    //merge parent and child for happy life
	            foreach ($datax as $key => $value) {
	                          		$explode = explode(".",$datay[$key][0]['LeaveRules']);
	                          		$applied = $this->check_employee($explode[0]);
	                          		$department = $explode[1];
	                          		$ent_day = $explode[2];
	                          		$ent_start = $explode[3];
	                          		$dataz[]  = ["status" => $status,"id" => $datay[$key][0]['id'], "Appliedfor" =>"Applied For : ".ltrim($applied), "Department" =>"Department : ".ltrim($department), "StartFrom"=>"StartFrom : ".ltrim($ent_day),"Entitlementdays" =>"Entitlementdays : ".ltrim($ent_start), "submaternity" =>$datax[$key] ];
	            }
	        }else{ $dataz = [];}

	    	}elseif($type == "6"){ // paternity

	        	$data = \DB::SELECT("CALL view_leave_paternity($type,$for)");
		        if($data != null){
		        // search child
			    foreach ($data  as $key => $value) {
			        $subdata =  ["year" => $value->year, "days" => $value->days];
					$datax[$value->id][]= $subdata;
			    }

			    //search parent
			    foreach ($data  as $key => $value) {
			        $subdata =  ["id" => $value->id, "LeaveRules" => $value->leave_rules];
			        $datay[$value->id][]= $subdata;
			    }

			    //merge parent and child for happy life
	            foreach ($datax as $key => $value) {
	                          		$explode = explode(".",$datay[$key][0]['LeaveRules']);
	                          		$applied = $this->check_employee($explode[0]);
	                          		$department = $explode[1];
	                          		$ent_day = $explode[2];
	                          		$ent_start = $explode[3];
	                          		$dataz[]  = ["status" => $status,"id" => $datay[$key][0]['id'], "Appliedfor" =>"Applied For : ".ltrim($applied), "Department" =>"Department : ".ltrim($department), "StartFrom"=>"StartFrom : ".ltrim($ent_day),"Entitlementdays" =>"Entitlementdays : ".ltrim($ent_start), "subpaternity" =>$datax[$key] ];
	            }
	        }else{ $dataz = [];}

	        }elseif($type == "8"){ // MARRIAGE

	        	$data = \DB::SELECT("CALL view_leave_marriage($type,$for)");
		        if($data != null){
		        // search child
			    foreach ($data  as $key => $value) {
			        $subdata =  ["year" => $value->year, "days" => $value->days];
					$datax[$value->id][]= $subdata;
			    }

			    //search parent
			    foreach ($data  as $key => $value) {
			        $subdata =  ["id" => $value->id, "LeaveRules" => $value->leave_rules];
			        $datay[$value->id][]= $subdata;
			    }

			    //merge parent and child for happy life
	            foreach ($datax as $key => $value) {
	                          		$explode = explode(".",$datay[$key][0]['LeaveRules']);
	                          		$applied = $this->check_employee($explode[0]);
	                          		$department = $explode[1];
	                          		$ent_day = $explode[2];
	                          		$ent_start = $explode[3];
	                          		$dataz[]  = ["status" => $status,"id" => $datay[$key][0]['id'], "Appliedfor" =>"Applied For : ".ltrim($applied), "Department" =>"Department : ".ltrim($department), "StartFrom"=>"StartFrom : ".ltrim($ent_day),"Entitlementdays" =>"Entitlementdays : ".ltrim($ent_start), "submarriage" =>$datax[$key] ];
	            }
	        }else{ $dataz = [];}

	        }elseif($type == "7"){
	        		$data = \DB::SELECT("CALL View_leave_bereavement($type,$for)");
	        		 if($data != null){
		        		// search child
			            foreach ($data  as $key => $value) {
			            	$subdata =  ["relation_to_deceased" => $value->relation_to_deceased, "days" => $value->days, "days_with_pay" => $value->days_with_pay];
					$datax[$value->id][]= $subdata;
			            }
			           //search parent
			            foreach ($data  as $key => $value) {
			              	$subdata =  ["id" => $value->id, "LeaveRules" => $value->Leave_Rules];
			                     $datay[$value->id][]= $subdata;
			            }

			           //merge parent and child for happy life
	               		  foreach ($datax as $key => $value) {
	                          		$explode = explode(".",$datay[$key][0]['LeaveRules']);
	                          		$applied = $this->check_employee($explode[0]);
	                          		$department = $explode[1];
	                          		$department_data = \DB::SELECT("select name from department where id=$department");
	                          		$department_name = $department_data[0]->name;
	                          		$ent_day = $explode[2];
	                          		$ent_start = $explode[3];
	                          		$dataz[]  = ["status" => $status,"id" => $datay[$key][0]['id'], "Appliedfor" => "Applied For : ".ltrim($applied), "Department" =>"Department : ".ltrim($department_name),"StartFrom" =>"StartFrom : ".ltrim($ent_day),  "Entitlementdays" =>"Entitlementdays : ".ltrim($ent_start),  "subbereavement" => $datax[$key] ];
	                		}
	                	}else{ $dataz = [];}
	        }else{
	        		$dataz = \DB::SELECT('Call View_leave_enhanced');

	        }

                return $dataz;

        }

        public function check_department($department = "", $applied = "", $id ="", $table = ""){
        		$data = \DB::SELECT("select applied_for, department from $table where id = $id");
			if($data[0]->applied_for == $applied and $data[0]->department == $department){
				return "yes";
			}else{
				$leave =  \DB::SELECT("select applied_for,department from leave_vacation where department = $department and applied_for = $applied");
				if($leave != null){
					return "no";
				}
			}

        }

}
