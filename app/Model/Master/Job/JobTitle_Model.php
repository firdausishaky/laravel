<?php namespace Larasite\Model\Master\Job;

use Illuminate\Database\Eloquent\Model;

class JobTitle_Model extends Model {

	// Defined Eloquent Methods
	protected $table = 'job';
	protected $fillable = ['id','title','descript','filename'];
	protected $hidden = ['create_at','updated_at'];
	protected $guarded = ['id', 'path'];

	// Defined Class Model

/*
* FIND DATA
*
*/
public function getStore($input){
		$get = \DB::table('job')->where('title','=',$input['title'])->where('descript','=',$input['descript'])->get($this->fillable);
		foreach ($get as $key) {
			$data[] = $key;
		}
		return $data;
	}

// GET ID WITH TITLE INPUT
	public function get_id($title){
		$fetch = \DB::table('job')->where('title','=',$title)->get($this->fillable);
		//foreach ($fetch as $key) { $getid[] = $key; }
		return $getid;
	}
	public function get_id_file($title){
		$fetch = \DB::table('job')->where('filename','=',$title)->get($this->fillable);
		foreach ($fetch as $key) { $getid[] = $key; }
		return $getid;
	}
// GET ID WITH FILE
	public function GetID($filename){
		$data = \DB::select("Select id from job where filename = '$filename' limit 1");
		foreach ($data as $key) {
			$id = $key->id;
		}
		return $id;
	}
// CHECK FILE WITH ID
	public function check_file_id($id){
		if(!$id){ return null;  }
		$r = \DB::select("SELECT filename from job where id = '$id' ");
		foreach ($r as $key) {
			$data = $key->filename;
		}
		return $data;
	}
// FETCH ID
	public function fetch_id($id){
		$data = \DB::select("select a.title , b.id from emp_details b , job a where b.job_id = a.id and b.id = $id ");
		foreach ($data as $key) { $value = $key->title; }
		return $value;
	}

/*
* CRUD METHOD 
*
*/

// GET JOB DATA
	public function ReadJob($type,$id){
		if($type == 1){
			try {
					$data = \DB::select("select id, title , descript, filename 
						from job where title != '' order by title asc");	
			} catch (Exception $e){	$data=$e;	}
		}
		elseif($type == 2){
				if(isset($id)){
					try {
						$r = \DB::select("select id, title, descript, filename 
							from job where id = '$id' limit 1 ");
						if(!empty($r)){	$data=$r;}
						else{	$data=null;	}	
					}catch (Exception $e){	$data=$e;	}
				}
				else{	$data=null;	}
		}else{	$data=null;	}
		return $data;
	}
// STORE ONLY STRING
	public function Insert($input,$id)
	{
		if(isset($id)){
			if(isset($input['filename'])){
				$data = \DB::select("CALL insert_job(' ".$input['title']." ',' ".$input['descript']." ',' ".$input['filename']." ',' ".$input['path']." ')");
			}else{
				$data = \DB::select("CALL insert_job(' ".$input['title']." ',' ".$input['descript']." ',null,null)");
			}
			
			if($data){ $message = 'Store Successfully.'; $status = 200; $datas = $data[0]; }
			else{ $message = 'Store Failed.'; $status = 500; $datas = false; }
			return [$message,$status,$datas];
		}else{
			if(isset($input['filename'])){
				$data = \DB::select("CALL insert_job(' ".$input['title']." ',' ".$input['descript']." ',' ".$input['filename']." ',' ".$input['path']." ')");
			}else{
				$data = \DB::select("CALL insert_job(' ".$input['title']." ',' ".$input['descript']." ',null,null)");
			}
			
			if($data){ $message = 'Store Successfully.'; $status = 200; $datas = $data[0]; }
			else{ $message = 'Store Failed.'; $status = 500; $datas = false; }
			return [$message,$status,$datas];
		}
	}
// DESTROY
	public function DestroyJob($id){
		$i = 1;
		$key_job=array(); $msg = array();
		$destroy = null;
		foreach ($id as $key) {
			$del = \DB::table('job_history')->where('job','=',$key)->get(['job']);
			$name = \DB::table('job')->where('id','=',$key)->get(['title']);
			foreach ($name as $keys) {
				$title = $keys->title;
			}
			if($del == false){
				array_push($key_job,$key);
				$status[$i]=200;	
			}else{array_push($msg,$title.' : Job Cannot delete.'); $status[$i]= 500;}
			$i++;
		}

		if(array_search(500, $status)){
			return ['message'=>$msg,'status'=>$status,'fix'=>500];	
		}else{
			foreach ($key_job as $key) {
				\DB::table('job')->where('id','=',$key)->delete();	
			}
			return ['message'=>'Destroy Successfully.','status'=>200,'fix'=>200];	
		}
	}
// UPDATE
	public function JobUpdate($id,$title,$descript,$filename,$path){
		
		if(isset($id,$filename)){
			\DB::table('job')->where('id','=',$id)->update(['filename'=>$filename,'path'=>$path]);
			$status = 1;
		}
		elseif(isset($id,$title,$descript)){
			\DB::table('job')->where('id','=',$id)->update(['title'=>$title,'descript'=>$descript]);
			$status = 1;	
		}
		elseif(isset($id,$title)){
			\DB::table('job')->where('id','=',$id)->update(['title'=>$title]);
			$status = 1;	
		}
		else{$status = 'gagal';}
		return $status;
	}

	public function check_data($id,$input){

		if($input['radio'] == 2){
			$select =  \DB::SELECT("select * from job where id=$id");
			$filename = $select[0]->filename;
			array_map('unlink', glob("hrms_upload/job_attach/$filename"));
			$update = \DB::SELECT("update job set title='$input[title]', descript='$input[descript]', filename=null, path=null where id=$id ");
			if(isset($update)){
				return "ok";
			}else{
				return "not";
			}
		}elseif($input['radio'] == 3){
			$select =  \DB::SELECT("select * from job where id=$id");
			$filename = $select[0]->filename;
			if($filename == null){
				return "undefined";
			}else{
				array_map('unlink', glob("hrms_upload/job_attach/$filename"));
			}
			$image = $input['imageName'];
			$file_name = $input['image'];
			$file_ext = $input['ext'];
			$size = $input['size'];
			$path = "hrms_upload/job_attach";
			if ($file_ext == "pdf" ){
				
				if($size > 1048576000){
					return  $data ="size";
				}else{

					\Input::file('file')->move(storage_path($path),$filename);
				}
			}else{
				return $data = "format";
			}
			$update = \DB::SELECT("update job set title='$input[title]', descript='$input[descript]', filename='$filename', path='$path' where id=$id");
			if(isset($update)){
				return "ok";
			}else{
				return "not";
			}
		}else{
			$select = \DB::SELECT("select * from job where id=$id");
			$filename = $select[0]->filename ;
			$path = $select[0]->path;

			if($path != null || $filename != null){
				array_map('unlink', glob("hrms_upload/job_attach/$filename"));
				$image = $input['imageName'];
				$file_name = $input['image'];
				$file_ext = $input['ext'];
				$size = $input['size'];
				$path = "hrms_upload/job_attach";
				if ($file_ext == "pdf" ){
					if($size > 1048576000){
						return  $data ="size";
					}else{
						$data_file_name = str_random(6).".".$file_ext;
						\Input::file('file')->move(storage_path($path),$data_file_name);
					}
				}else{
					return $data = "format";
				}
				$title = $input['title'];
				$descript = $input['descript'];
				$update = \DB::SELECT("update job set title='$title', descript='$descript', filename='$data_file_name', path='$path' where id=$id");
			}else{
				$title = $input['title'];
				$descript = $input['descript'];
				$file_name = $input['image'];
				$path = "hrms_upload/job_attach";
				if($file_name == null){
					$title = $input['title'];
					$descript = $input['descript'];
					$update = \DB::SELECT("update job set title='$title', descript='$descript' where id=$id");
				}else{
					$image = $input['imageName'];
					$file_name = $input['image'];
					$file_ext = $input['ext'];
					$size = $input['size'];
					
					if ($file_ext == "pdf" ){
						
						if($size > 1048576000){
							return  $data ="size";
						}else{
							$data_file_name = str_random(6).".".$file_ext;
							\Input::file('file')->move(storage_path($path),$data_file_name);
						}
					}else{
						return $data = "format";
					}
					$title = $input['title'];
					$descript = $input['descript'];
					$update = \DB::SELECT("update job set title='$title', descript='$descript', filename='$data_file_name', path='$path' where id=$id");
					}
			}
			if(isset($update)){
				return "ok";
			}else{
				return "not";
			}
		}
	}

}
