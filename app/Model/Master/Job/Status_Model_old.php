<?php namespace Larasite\Model\Master\Job;

use Illuminate\Database\Eloquent\Model;

class Status_Model extends Model {

	// Defined Eloquent Methods
	protected $table = 'jobs_emp_status';
	protected $fillable = ['id','title'];
	protected $hidden = ['create_at','updated_at'];
	protected $guarded = ['id'];
	protected $messageBox = ['duplicate'=>['Status already in use.',500],'StoreSuccess'=>['Store Successfully.',200],'UpdateSuccess'=>['Update Successfully.',200],'StoreError'=>['Store Failed.',500],'UpdateError'=>['Update Failed',500]];
	// Defined Model Method

// READ
	public function Read_EmpStatus($type,$id){
		if($type == 1){
			$data = \DB::select("CALL view_emp_status('','')");
			if(!$data){	$data = []; }
		}
		elseif($type == 2){
			$data =  \DB::select("CALL view_emp_status('',$id)");
			if(!$data){	$data = []; }
		}
		return $data;
	}
// UPDATED
	public function Update_EmpStatus($id,$title){
		if($this->checkTitle($title)){

			$message = $this->messageBox['duplicate'][0]; $status = $this->messageBox['duplicate'][1]; $data=NULL;
		}else{ 
			if(\DB::table($this->table)->where('id','=',$id)->update($title) ){
				$message = $this->messageBox['UpdateSuccess'][0]; $status = $this->messageBox['UpdateSuccess'][1]; $data = $this->Read_EmpStatus(2,$id)[0];	
			}else{
				$message = $this->messageBox['UpdateError'][0]; $status = $this->messageBox['UpdateError'][1]; $data = NULL;
			}
		 }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
// CHECK TITLE
	private function checkTitle($string)
	{	
		$type = gettype($string);
		if($type == "array"){
			$string = $string['title'];	
		}else{
			$string= $string;
		}
		
		$get =  \DB::select("CALL view_emp_status('$string',' ')");
		if($get && $get != null ){ $data = [$get]; }elseif($get != null){ $data = [];}
		else{ $data = false; }
		return $data;
	}
//STORE
	public function Store_EmpStatus($title){
		

		$title = $title['title'];
		if($this->checkTitle($title)){
			$message = $this->messageBox['duplicate'][0]; $status = $this->messageBox['duplicate'][1]; $data=[];
		}else{ 
			if($query = \DB::select("CALL insert_emp_status('$title')")){
				$message = $this->messageBox['StoreSuccess'][0]; $status = $this->messageBox['StoreSuccess'][1]; $data = $query;	
			}else{
				$message = $this->messageBox['StoreError'][0]; $status = $this->messageBox['StoreError'][1]; $data = [];
			}
		 }
		return ['message'=>$message,'status'=>$status,'data'=>$data]; 
	}
// DESTROY
	public function Destroy_EmpStatus($id){
		$i = 1;
		$del = array();
		foreach ($id as $key) {
			$del = \DB::select("CALL delete_emp_status($key)");
		}
		if(!empty($del))
		{
			 $msg=$del[0]->message;
			 if($msg == "data has been delete from table"){
			 	return ['message'=>$msg,'status'=>200];
			 }else{
			 	return ['message'=>$msg,'status'=>500];
			 }
		}else{
			return ['message'=>"Can't delete data, something mising",'status'=>$status];
		}
		
	}

}
