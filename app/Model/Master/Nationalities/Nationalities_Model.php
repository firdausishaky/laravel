<?php namespace Larasite\Model\Master\Nationalities;

use Illuminate\Database\Eloquent\Model;

class Nationalities_Model extends Model {

	// Defined Eloquent Methods
	protected $table = 'nationality';
	protected $fillable = ['id','title'];
	protected $guarded = ['id'];
	protected $messageBox = ['duplicate'=>['Nationality already in use.',500],'StoreSuccess'=>['Store Successfully.',200],'UpdateSuccess'=>['Update Successfully.',200],'StoreError'=>['Store Failed.',500],'UpdateError'=>['Update Failed',500]];
	// Defined Model Methods

	private function Check($string)
	{
		$get = \DB::table($this->table)->where('title','=',$string)->get($this->fillable);
		return $get;
	}

// READ
	public function ReadNationalities($type,$id){
		if($type == 'All'){
			$data = \DB::select("select id , title from ".$this->table." ");
			if(!$data){ $data = null; }
		}
		elseif($type == 'Sort'){
			$data = \DB::select("select id , title from ".$this->table." where id = '$id' ");	
			if(!$data){ $data = null; }
		}
		return $data;
	}
// UPDATE
	public function UpdateNationalities($id,$title){
		if($this->Check($title)){
			$message = $this->messageBox['duplicate'][0]; $status = $this->messageBox['duplicate'][1]; $data=NULL;
		}else{ 
			if(\DB::table($this->table)->where('id','=',$id)->update(['title'=>$title]) ){
				$message = $this->messageBox['UpdateSuccess'][0]; $status = $this->messageBox['UpdateSuccess'][1]; $data = $this->ReadNationalities('Sort',$id)[0];	
			}else{
				$message = $this->messageBox['UpdateError'][0]; $status = $this->messageBox['UpdateError'][1]; $data = NULL;
			}
		 }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
// Store
	public function StoreNationalities($title){
		if($this->Check($title)){
			$message = $this->messageBox['duplicate'][0]; $status = $this->messageBox['duplicate'][1]; $data=NULL;
		}else{ 
			if(\DB::table($this->table)->insert(['title'=>$title]) ){
				$message = $this->messageBox['StoreSuccess'][0]; $status = $this->messageBox['StoreSuccess'][1]; $data = $this->Check($title)[0];	
			}else{
				$message = $this->messageBox['StoreError'][0]; $status = $this->messageBox['StoreError'][1]; $data = NULL;
			}
		 }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
// DESTROY
	public function DestroyNationalities($id){
		$i=0;
		foreach($id as $key){
				$C_title = \DB::table($this->table)->where('id','=',$key)->get(['title']);
				foreach ($C_title as $check_lang) {
				 	$check = \DB::table('emp')->where('nationality','=',$key)->get(['nationality']);
					$title = $check_lang->title;
				}
				if($check){ $msg[$i] = $title.', Data can not be deleted.'; $status[$i] = '500'; }
				else{ 
					$destroy = \DB::table($this->table)->where('id','=',$key)->delete();
					$msg[$i] = $title.', Delete Successfully.';
					$status[$i] = '200';
				}
				$i++;
		}
		$msgs = null;
		foreach ($msg as $key) { $msgs .= $key." \n "; }
		if(in_array('200',$status)){ $statuss = 200; }
		else{ $statuss = 500; }
		return ['message'=>$msgs,'status'=>$statuss];

	}

}
