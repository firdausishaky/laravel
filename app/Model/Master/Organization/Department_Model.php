<?php namespace Larasite\Model\Master\Organization;

use Illuminate\Database\Eloquent\Model;

class Department_Model extends Model {

	protected $table = 'department';

	private function duplicate($value)
	{
		$get = \DB::table($this->table)->where('name','=',$value)->get();
		if($get){ $message = 'Value already in use.'; $status = 500; }
		else{ $message = 'OK.'; $status = 200; }
		return [$message,$status];
	}

	public function Store_Department($input){
		$duplicate = $this->duplicate($input);
		if($duplicate[1] ==  200){
			$get = \DB::SELECT("CALL Insert_Department('$input',0)");
			if($get){
				$getID = \DB::table($this->table)->where('name','=',$input)->get(['id']);
				foreach ($getID as $key) { $data = $key->id; $message = 'Store Successfully.'; $status = 200; }
			}else{ $data = NULL; $message = 'Store Failed.'; $status = 500; }
		}else{
			$data = NULL; $message =$duplicate[0] ; $status = $duplicate[1];
		}
		return [$message,$status,$data]; 
	}

	public function Update_Department($id,$input){
		$duplicate = $this->duplicate($input);
		if($duplicate[1] ==  200){
			$get = \DB::SELECT("CALL Update_department('$input',$id)");
			if(isset($get)){
				$data = ['id' => $id, 'name' => $input]; $message = 'Update Successfully.'; $status = 200;
			}else{ $data = NULL; $message = 'Update Failed.'; $status = 500; }
		}else{
			$data = NULL; $message =$duplicate[0] ; $status = $duplicate[1];
		}
		return [$message,$status,$data]; 
	}

	public function Delete_Department($id){
		$i = 1;
		$key_job=array(); $msg = array();
		$destroy = null;
		foreach ($id as $key) {
			$del = \DB::table('job_history')->where('sub_unit','=',$key)->get(['sub_unit']);
		
				//$del = \DB::table('sub_unit')->where('sub_unit','=',$key)->get(['sub_unit']);
				$name = \DB::table($this->table)->where('id','=',$key)->get(['name']);
				foreach ($name as $keys) {
					$title = $keys->name;
				}
				if($del == false){
					array_push($key_job,$key);
					$status[$i]=200;	
				}else{array_push($msg,$title.' : Department Cannot delete.'); $status[$i]= 500;}
				$i++;
		}

		if(array_search(500, $status)){
			return ['message'=>$msg,'status'=>$status,'fix'=>500];	
		}else{
			foreach ($key_job as $key) {
				\DB::table($this->table)->where('id','=',$key)->delete();	
			}
			return ['message'=>'Destroy Successfully.','status'=>200,'fix'=>200];	
		}
	}

}