<?php namespace Larasite\Model\Master\Organization;

use Illuminate\Database\Eloquent\Model;

class GeneralInfo_Model extends Model {

	// Defined Eloquent Methods ###

	protected $table = 'general_information';
	protected $hidden = ['created_at','updated_at','path'];
	protected $field = "gi.name,gi.phihealth_numb,gi.bir_numb,gi.sss_numb,gi.pag_big_numb,gi.phone,gi.rdo_numb,gi.email,gi.fax,gi.address";

	// Defined Fetch Methods ###

// SET INPUT
	private function set_input($input)
	{
		$array = ['name'=>$input['name'],'phihealth_numb'=>$input['phihealth_numb'],'pag_big_numb'=>$input['pag_big_numb'],
				'phone'=>$input['phone'],'email'=>$input['email'],'bir_numb'=>$input['bir_numb'],'sss_numb'=>$input['sss_numb'],'rdo_numb'=>$input['rdo_numb'],
				'fax'=>$input['fax'],'address'=>$input['address']];
		if(isset($input['id'])){ $array['id'] = $input['id']; }
		return $array;
	}
// GET ID
	public function GetID($id){
		$r = \DB::select("select id from general_information where id = '$id'");
		$data = $r[0];
		return $data;
	}
// CHECK ID
	public function check_file_id($id){
		$r = \DB::select("SELECT filename from general_information where id = '$id' ");
		if(!$r){ return NULL; }
		foreach ($r as $key) { $data = $key->filename; }
		return $data;
	}

	// Defined Model Methods ###

// READ
	public function Read_GI($type,$id){
		$data = null;
		if($type == 1){
			$data = \DB::select("
				SELECT (SELECT COUNT(local_it) FROM   emp where local_it = 1 and employee_id not in('2014888','2014999') ) AS local,
				(SELECT COUNT(local_it) FROM  emp where local_it = 2 and employee_id not in('2014888','2014999')) AS expat,
        		$this->field from general_information gi
				");
			if(!$data){ $data = 0; }
		}
		elseif($type == 'Sort'){
			$data = \DB::select("
				SELECT (SELECT COUNT(id_employee_type) FROM   employees where id_employee_type = 1) AS local,
				(SELECT COUNT(id_employee_type) FROM   employees where id_employee_type = 2) AS expat,
        		$this->field, gi_attach.file_name as file_name, gi_attach.path as path from general_information gi, gi_attach where gi.id = gi_attach.gi_id and gi.id = '$id'
				");	
			if(!$data){ $data = 0; }
		}
		return $data;
	}
// STORE
	public function Store_GI($input){
		$store = \DB::table('general_information')->insert($this->set_input($input));
		if($store){ $msg = 'success'; }else{ $msg = 'fail'; }
		return $msg;
	}
// USES Update
	public function Update_GI($id,$input){
			$store = \DB::table('general_information')->where('id','=',$id)->update($this->set_input($input));
			if($store){ $msg = 'success'; }else{ $msg = 'fail'; }
		return $msg;
	}
// DESTROY JOB
	public function DestroyGI($id){
		$i = 1;
		$del = array();
			foreach($id as $key){ $destroy = \DB::table('general_information')->where('id','=',$key)->delete(); }
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{ $msg = null; }	
		return $msg;
	}
}
