<?php namespace Larasite\Model\Master\Organization;

use Illuminate\Database\Eloquent\Model;

class ORGStructure_Model extends Model {

	// Defined Eloquent Methods

	protected $table = 'sub_unit';
	protected $table1 = 'department';
	protected $fillable = ['id','sub_unit','descript','parent_id'];
	protected $guarded = ['id', 'parent_id'];

	// Defined Function

// Count Department Employee
	public function count_department_emp(){
		$q = "SELECT 
		concat(t1.sub_unit,' (', (select concat(sub_unit) from sub_unit where parent_id =1 ) ,')') as parent_1,
		t2.sub_unit as parent_2,
		t3.sub_unit as parent_3
		FROM `sub_unit` as t1
		left join sub_unit as t2 on t2.parent_id = t1.id
		left join sub_unit as t3 on t3.parent_id = t2.id
		WHERE t1.sub_unit  = 'Leekie.ent.inc'";
		
		$count = \DB::select($q);
		if(isset($count)){ $data = $count; }
		else{ $data = null; }
		return $data;
	}
// SET INPUT
	public function Set_Input($input){
		return[ 'descript'=>$input['descript'],'sub_unit'=>$input['sub_unit'],'parent_id'=>$input['parent_id']];
	}
// GET ID
	public function GetID($sub_unit){
		$get = \DB::select("SELECT id from department where sub_unit = '$sub_unit' order by id desc limit 1");
		if(!$get){ return NULL; }
		foreach ($get as $key) { $data = $key->id; }
		return $data;
	}

	// Defined Model Methods
	
	private function getName($value)
	{
		$get = \DB::table('department')->where('id','=',$value)->get(['name']);
		if($get){ foreach($get as $key){ $data = $key->name; } }
		else{ $data = null; }
		return $data;
	}
// READ
	public function Read_Structure(){
		// $i=1; $j=1; $k=0;
		// $get_count = \DB::select("select count(distinct parent_id) as count_parent from sub_unit where parent_id != '' "); // count all parent
		// $get = \DB::select("SELECT t1.id, t1.parent_id ,t1.sub_unit, count(t2.parent_id) as count 
		// 					FROM sub_unit AS t1 
		// 					LEFT JOIN sub_unit AS t2 ON t2.parent_id = t1.id
		// 					WHERE t2.parent_id = $i");
		// 		foreach ($get as $first) {
		// 		$get = \DB::select("SELECT DISTINCT t2.id , t2.parent_id ,t2.sub_unit, t2.descript
		// 							FROM sub_unit AS t1
		// 							LEFT JOIN sub_unit AS t2 ON t2.parent_id = t1.id 
		// 							WHERE t2.parent_id= $i");

		// 			$get_count = \DB::select("select count(distinct parent_id) as count_parent from sub_unit where parent_id != $i");
		// 			foreach ($get as $second) {
		// 				$get3 = \DB::select("SELECT DISTINCT t2.id , t2.parent_id ,t2.sub_unit, t2.descript
		// 								FROM sub_unit AS t1
		// 								LEFT JOIN sub_unit AS t2 ON t2.parent_id = t1.id
		// 								WHERE t2.parent_id= $second->id");
		// 				$r_count = \DB::select("SELECT count(t2.parent_id) as count from sub_unit as t1 left join sub_unit as t2 on t2.parent_id = t1.id where t2.parent_id = $second->id");
		// 				foreach ($r_count as $key) { $count = $key->count; }
		// 				foreach ($get3 as $keyx) {
		// 					$get3[$k] =  ['id'=>$keyx->id,'parent_id'=>$keyx->parent_id,'sub_unit'=>$this->getName($keyx->sub_unit),'descript'=>$keyx->descript];
		// 					$k++;
		// 				}
		// 				$seconds = $this->getName($second->sub_unit);
		// 				$firsts = $this->getName($first->sub_unit);

		// 				$ger1[$j] = ['id'=>$second->id,'parent_id'=>$second->parent_id,'sub_unit'=>$seconds,'descript'=>$second->descript,'count'=>$count,'sub'=>$get3];
		// 				$data = ['id'=>$first->id,'parent_id'=>$first->parent_id,'sub_unit'=>'Leekie Incoporated Inc.','count'=>$first->count,'sub'=>$ger1 ];
		// 				$j++;
		// 			 }
		// 			$i++;
		// 		}
		$i=1; $j=1; $k=0;
		$val  = \DB::SELECT("select * from department where id = 1");
		$get_count = \DB::select("select count(distinct parent) as count_parent from department where parent != '' "); // count all parent
		$get = \DB::select("SELECT t1.id, t1.parent ,t1.id, count(t2.parent) as count 
							FROM department AS t1 
							LEFT JOIN department AS t2 ON t2.parent = t1.id
							WHERE t2.parent = $i");
		foreach ($get as $first) {
		$get = \DB::select("SELECT DISTINCT t2.id , t2.parent ,t2.id
							FROM department AS t1
							LEFT JOIN department AS t2 ON t2.parent= t1.id 
							WHERE t2.parent= $i");

			$get_count = \DB::select("select count(distinct parent) as count_parent from department where parent != $i");
			foreach ($get as $second) {
				$get3 = \DB::select("SELECT DISTINCT t2.id , t2.parent ,t2.id
								FROM department AS t1
								LEFT JOIN department AS t2 ON t2.parent = t1.id
								WHERE t2.parent = $second->id");
				$r_count1 = \DB::select("SELECT count(department) as count from emp where department = $second->id");
				$r_count2 = \DB::select("SELECT count(department_id) as count from addOnDepartment where department_id = $second->id");
				$count = $r_count1[0]->count + $r_count2[0]->count; 
				//foreach ($r_count as $key) { $count = $key->count; }
				foreach ($get3 as $keyx) {
					//$r_count = \DB::select("SELECT count(department) as count from emp where department = $keyx->id");
					$r_count1 = \DB::select("SELECT count(department) as count from emp where department = $keyx->id");
					$r_count2 = \DB::select("SELECT count(department_id) as count from addOnDepartment where department_id = $keyx->id");
					$r_count = $r_count1[0]->count + $r_count2[0]->count;
					$get3[$k] =  ['id'=>$keyx->id,'parent_id'=>$keyx->parent,'sub_unit'=>$this->getName($keyx->id),'descript'=>null, 'count'=>$r_count];
					$k++;
				}
				$seconds = $this->getName($second->id);
				$firsts = $this->getName($first->id);
				if($count == null){
					$count = 0 ;
				}
				$ger1[$j] = ['id'=>$second->id,'parent_id'=>$second->parent,'sub_unit'=>$seconds,'descript'=>'null','count'=>$count,'sub'=>$get3];
				$data = ['id'=>$first->id,'parent_id'=>$first->parent,'sub_unit'=>$val[0]->name,'count'=>0,'sub'=>$ger1 ];
				$j++;
				}
			$i++;
		}
		return $data;
	}
// STORE
	public function Store_Structure($input){
		if(isset($input) && $input != null){
			$store_input =  $this->set_input($input);
			//return[ 'descript'=>$input['descript'],'sub_unit'=>$input['sub_unit'],'parent_id'=>$input['parent_id']];
			$parent = $store_input['sub_unit'];
			$id = $store_input['parent_id']; 

			//return ['parent' => $parent, $id => $id];
			$store = \DB::SELECT("update department set parent = $id where id = $parent ");
			//$store = \DB::table($this->table1)->insert($store_input);

			if($store == null){ $message = 'Store Successfully.'; $status=200; $data=$parent;}
			else{ $message ='Store Failed'; $status=500; $data=null; }
		}else{$message ='Input Null'; $status=500; $data=null;}
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
// UPDATE
	public function Update_Structure($id,$input){
		if(isset($input,$id) && $input != null && $id != null && $id != 'undefined'){
			$parent = $input['parent_id'];
			$sub = $input['sub_unit'];
			//$store = \DB::table($this->table)->where('id','=',$id)->update($this->set_input($input));
			$update  = \DB::select("update department set parent = 0 where id = $id");
			$store  = \DB::select("update department set parent = $parent where id = $sub");
			if($store == null){ $message = 'Update Successfully.'; $status=200; $data=$sub;}
			else{ $message ='Update Failed'; $status=500; $data=null; }
		}else{$message ='Input Null'; $status=500; $data=null;}
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
// DESTROY
	public function Destroy_Struct($id){
			if($id == 1){
				$status = 500; return $status;
			}else{
				//$del= \DB::table('department')->where('parent','=',$id)->get(['id']);
				$del = \DB::SELECT("select * from department where id = $id");
				$check = \DB::select("SELECT id from department where parent = $id");
				$uses = ['parent'=>$check,'uses'=>$del];
				
				if($uses['parent'] && $uses['uses'])
				{ 
					$status = 500; 
				}
				else{ 
					\DB::SELECT("UPDATE department SET parent = 0 where id = $id"); 
					$status = 200; 
				}
				return $status;	
			}
	}
}
