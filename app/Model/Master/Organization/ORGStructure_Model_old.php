<?php namespace Larasite\Model\Master\Organization;

use Illuminate\Database\Eloquent\Model;

class ORGStructure_Model extends Model {

	// Defined Eloquent Methods

	protected $table = 'sub_unit';
	protected $fillable = ['id','sub_unit','descript','parent_id'];
	protected $guarded = ['id', 'parent_id'];

	// Defined Function

// Count Department Employee
	public function count_department_emp(){
		$q = "SELECT 
		concat(t1.sub_unit,' (', (select concat(sub_unit) from sub_unit where parent_id =1 ) ,')') as parent_1,
		t2.sub_unit as parent_2,
		t3.sub_unit as parent_3
		FROM `sub_unit` as t1
		left join sub_unit as t2 on t2.parent_id = t1.id
		left join sub_unit as t3 on t3.parent_id = t2.id
		WHERE t1.sub_unit  = 'Leekie.ent.inc'";
		
		$count = \DB::select($q);
		if(isset($count)){ $data = $count; }
		else{ $data = null; }
		return $data;
	}
// SET INPUT
	public function Set_Input($input){
		return[ 'descript'=>$input['descript'],'sub_unit'=>$input['sub_unit'],'parent_id'=>$input['parent_id']];
	}
// GET ID
	public function GetID($sub_unit){
		$get = \DB::select("SELECT id from sub_unit where sub_unit = '$sub_unit' order by id desc limit 1");
		if(!$get){ return NULL; }
		foreach ($get as $key) { $data = $key->id; }
		return $data;
	}

	// Defined Model Methods
	
	private function getName($value)
	{
		$get = \DB::table('department')->where('id','=',$value)->get(['name']);
		if($get){ foreach($get as $key){ $data = $key->name; } }
		else{ $data = null; }
		return $data;
	}
// READ
	public function Read_Structure(){
		$i=1; $j=1; $k=0;
		$get_count = \DB::select("select count(distinct parent_id) as count_parent from sub_unit where parent_id != '' "); // count all parent
		$get = \DB::select("SELECT t1.id, t1.parent_id ,t1.sub_unit, count(t2.parent_id) as count 
							FROM sub_unit AS t1 
							LEFT JOIN sub_unit AS t2 ON t2.parent_id = t1.id
							WHERE t2.parent_id = $i");
				foreach ($get as $first) {
				$get = \DB::select("SELECT DISTINCT t2.id , t2.parent_id ,t2.sub_unit, t2.descript
									FROM sub_unit AS t1
									LEFT JOIN sub_unit AS t2 ON t2.parent_id = t1.id 
									WHERE t2.parent_id= $i");

					$get_count = \DB::select("select count(distinct parent_id) as count_parent from sub_unit where parent_id != $i");
					foreach ($get as $second) {
						$get3 = \DB::select("SELECT DISTINCT t2.id , t2.parent_id ,t2.sub_unit, t2.descript
										FROM sub_unit AS t1
										LEFT JOIN sub_unit AS t2 ON t2.parent_id = t1.id
										WHERE t2.parent_id= $second->id");
						$r_count = \DB::select("SELECT count(t2.parent_id) as count from sub_unit as t1 left join sub_unit as t2 on t2.parent_id = t1.id where t2.parent_id = $second->id");
						foreach ($r_count as $key) { $count = $key->count; }
						foreach ($get3 as $keyx) {
							$get3[$k] =  ['id'=>$keyx->id,'parent_id'=>$keyx->parent_id,'sub_unit'=>$this->getName($keyx->sub_unit),'descript'=>$keyx->descript];
							$k++;
						}
						$seconds = $this->getName($second->sub_unit);
						$firsts = $this->getName($first->sub_unit);

						$ger1[$j] = ['id'=>$second->id,'parent_id'=>$second->parent_id,'sub_unit'=>$seconds,'descript'=>$second->descript,'count'=>$count,'sub'=>$get3];
						$data = ['id'=>$first->id,'parent_id'=>$first->parent_id,'sub_unit'=>'Leekie Incoporated Inc.','count'=>$first->count,'sub'=>$ger1 ];
						$j++;
					 }
					$i++;
				}
		return $data;
	}
// STORE
	public function Store_Structure($input){
		if(isset($input) && $input != null){
			$store_input =  $this->set_input($input);
			$store = \DB::table($this->table)->insert($store_input);

			if($store){ $message = 'Store Successfully.'; $status=200; $data=$this->GetID($input['sub_unit']);}
			else{ $message ='Store Failed'; $status=500; $data=null; }
		}else{$message ='Input Null'; $status=500; $data=null;}
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
// UPDATE
	public function Update_Structure($id,$input){
		if(isset($input,$id) && $input != null && $id != null && $id != 'undefined'){
			$store = \DB::table($this->table)->where('id','=',$id)->update($this->set_input($input));
			if($store){ $message = 'Update Successfully.'; $status=200; $data=$this->GetID($input['sub_unit']);}
			else{ $message ='Update Failed'; $status=500; $data=null; }
		}else{$message ='Input Null'; $status=500; $data=null;}
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
// DESTROY
	public function Destroy_Struct($id){
			if($id == 1){
				$status = 500; return $status;
			}else{
				$del= \DB::table('job_history')->where('sub_unit','=',$id)->get(['id']);
				
				$check = \DB::select("SELECT id from sub_unit where parent_id = $id");
				$uses = ['parent'=>$check,'uses'=>$del];
				
				if($uses['parent'] && $uses['uses']){ $status = 500; }
				else{ \DB::table('sub_unit')->where('id','=',$id)->delete(); $status = 200; }
				return $status;	
			}
	}
}
