<?php namespace Larasite\Model\Master\PIM;

use Illuminate\Database\Eloquent\Model;

class DocumentTemplate_Model extends Model {

	// Defined Eloquent Methods
	protected $table = 'document_template';
	protected $fillable = ['id','template_name','description','size','link_to_section','margin_top','margin_left','margin_bottom','margin_right','content_text'];
	protected $guarded = ['id'];

	// Defined Model Methods

	// READ
	public function Read_Doc($id){
		if($id){
			$result = \DB::table($this->table)->where('id','=',$id)->get($this->fillable);
			if($result){ $data=$result; $status=200; }else{ $data=null; $status=500; }
		}else{
			$result = \DB::table($this->table)->get($this->fillable);
			if($result){ $data=$result; $status=200; }else{ $data=null; $status=500; }
		}
		return ['data'=>$data,'status'=>$status];
	}
	//STORE
	public function Store_Doc($input){
		$Get_Input = $this->SetInput($input);	
		$store = \DB::table($this->table)->insert($Get_Input);
		if($store){
			$data = $this->GetID($Get_Input['id']); $message='Store Successfully.'; $status=200;
		}else{$data=null; $message='Store Failed !'; $status=500;}	
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
	//UPDATE
	public function Update_Doc($input,$id){
		$Get_Input = $this->SetInput($input);
		$update = \DB::table($this->table)->where('id','=',$id)->update($Get_Input);
		
		if($update){ $message = 'Updated Successfully.'; $status=200; }
		else{ $message = 'Updated failed.'; $status=500; }
		return ['message'=>$message,'status'=>$status];
	}
	// DESTROY
	public function Destroy_Doc($id){
		foreach($id as $key){ $destroy = \DB::table($this->table)->where('id','=',$key)->delete(); }
			
		if($destroy){$msg = 'Delete Successfully.'; $status=200;} else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
	// GET ID
	public function GetID($id){
		$data = \DB::select("Select id from $this->table where id = '$id' order by id DESC limit 1");
		foreach ($data as $key) { $id_temp = $key->id; }
		return $id_temp;
	}
	// GET TYPE
	public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){ $data = null; }
		return $data;
	}
}
