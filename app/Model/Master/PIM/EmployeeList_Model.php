<?php namespace Larasite\Model\Master\PIM;

use Illuminate\Database\Eloquent\Model;

class EmployeeList_Model extends Model {

	protected $table = 'emp';
	protected $fillable = ['id','title'];
	protected $guarded = ['id'];


	protected $i =0;
    public function FecthData($local,$id){
    	$get1 = \DB::select("SELECT employee_id, CONCAT(first_name,if(middle_name != null or middle_name != '','',middle_name)) AS first_name,
								   last_name AS last_name, IFNULL(NULL,place_of_birth) AS date_hire FROM emp WHERE local_it = $local and employee_id != '$id' ORDER BY  employee_id ASC");
    	foreach ($get1 as $key) {
    		$get2 = \DB::select("SELECT
								IFNULL(NULL,j.title) AS job,
								IFNULL(NULL,a.title) AS `status`,
								IFNULL(NULL,d.sub_unit) AS sub_unit
								FROM emp x , job_history e , sub_unit d , job j , jobs_emp_status a
								WHERE
								x.employee_id = e.employee_id
								AND e.sub_unit = d.id
								AND e.job = j.id 
								AND e.status = a.id
								AND e.employee_id = '$key->employee_id' ");
	    		if($get2){ // check jika emp memiliki data job_history
	    			foreach ($get2 as $jobhis) {
	    				$job = $jobhis->job;
	    				$status = $jobhis->status;
	    				$sub_unit = $jobhis->sub_unit;
	    			}
	    		}else{ $job=null; $status=null; $sub_unit=null; }
    		
    		$get3 = \DB::select("SELECT
									(SELECT concat(a.first_name,if(a.middle_name != null or a.middle_name != '',a.middle_name,''),' ',a.last_name) 
										FROM emp a, emp_supervisor b where a.employee_id = b.supervisor ) AS `supervisor`
									 FROM emp a , emp_supervisor b
									 WHERE 
									 a.employee_id = b.employee_id and 
									 a.employee_id = '$key->employee_id' ");
    			if($get3){ // check jika emp memiliki supervisor
	    			foreach ($get3 as $emp_supervisor) {
	    				$supervisor = $emp_supervisor->supervisor;
	    			}
	    		}else{ $supervisor=null; }

    		$data['data'][$this->i++] = ['employee_id'=>$key->employee_id,
    							'first_name'=>$key->first_name,
    							'last_name'=>$key->last_name,
    							'date_hire'=>$key->date_hire,
    							'job'=>$job,
    							'status'=>$status,
    							'sub_unit'=>$sub_unit,
    							'supervisor'=>$supervisor];
    	} return $data;
    }
    private function QueryParamsFilter($inc,$opt,$opr,$employee_id)
    {
    	if($inc == 1){ $res = \DB::select("SELECT employee_id, CONCAT(first_name,IFNULL(middle_name,'')) AS first_name,
					last_name AS last_name FROM emp WHERE local_it in($param1) and substr(last_name,1,1) != '(' 
					and employee_id NOT in($employee_id,'2014999','2014888') ORDER BY  last_name ASC"); }
    	elseif($inc == 2) { $res = \DB::select("SELECT employee_id, CONCAT(first_name,IFNULL(middle_name,'')) AS first_name,
					last_name AS last_name FROM emp WHERE local_it in($param1) and substr(last_name,1,1) = '(' 
					and employee_id NOT in($employee_id,'2014999','2014888') ORDER BY  last_name ASC"); }
    	elseif($inc == 3){ $res = \DB::select("SELECT employee_id, CONCAT(first_name,IFNULL(middle_name,'')) AS first_name,
					last_name AS last_name FROM emp WHERE local_it in($param1) and substr(last_name,1,1) = '(' 
					and employee_id NOT in($employee_id,'2014999','2014888') ORDER BY  last_name ASC");}
    	return $res;
    }
    public function parameterFilter($local,$inc,$employee_id){
    	$res = null;
 		if($local == 1){
 			$param = '1,2,3';
 			$res = $this->QueryParamsFilter($inc,$param,$employee_id);
 		}elseif($local == 2){
 			$param = 1;
 			$res = $res = $this->QueryParamsFilter($inc,$param,$employee_id);
 		}elseif($local == 3){
 			$param = 3;
 			$res = $res = $this->QueryParamsFilter($inc,$param,$employee_id);
 		}elseif($local == 4){
 			$param = 2;
 			$res = $res = $this->QueryParamsFilter($inc,$param,$employee_id);
 		}elseif($local == 5){
 			$param = '1,2';
 			$res = $res = $this->QueryParamsFilter($inc,$param,$employee_id);
 		}
 		return $res;
    }
    public function FecthAllData($local,$employee_id,$param){
    	
    	if($param['lc'] && $param['inc']){
    		$get1 = $this->parameterFilter($param['lc'],$param['inc'],$employee_id);
    	}else{
    		$get1 = $this->parameterFilter(1,1,$employee_id);
    		
    	}
    	if($get1){
    		foreach ($get1 as $key) {
    		$get2 = \DB::select("SELECT
								IF(b.contract_start_date = null or b.contract_start_date = '',null,b.contract_start_date) AS date_hire,
								IF(d.title = null or d.title = '',null,d.title) AS job,
								IF(e.title = null or e.title = '',null,e.title) AS `status`,
								IF(c.sub_unit = null or c.sub_unit = '',null,c.sub_unit) AS sub_unit
								FROM emp a , job_history b , sub_unit c , job d , jobs_emp_status e
								WHERE
								a.employee_id = b.employee_id
								AND c.id = b.sub_unit
								AND d.id = b.job 
								AND e.id = b.status
								AND a.employee_id = '$key->employee_id' ");
	    		if($get2){ // check jika emp memiliki data job_history
	    			foreach ($get2 as $jobhis) {
	    				$job = $jobhis->job;
	    				$status = $jobhis->status;
	    				$sub_unit = $jobhis->sub_unit;
	    				$date_hire = $jobhis->date_hire;
	    			}
	    		}else{ $job=null; $status=null; $sub_unit=null; $date_hire=null;}
 
	    		$get3 = \DB::select("select if(a.first_name = null or a.first_name = '',null,concat(a.first_name,'',a.middle_name,' ',a.last_name)) AS supervisor
									from emp a , emp_supervisor b where b.supervisor = a.employee_id and b.employee_id = '$key->employee_id'");
	    		
    			if($get3){ // check jika emp memiliki supervisor
	    			foreach ($get3 as $emp_supervisor) {
	    				$supervisor = $emp_supervisor->supervisor;
	    			}
	    		}else{ $supervisor=null; }

    			$data['data'][$this->i++] = ['employee_id'=>$key->employee_id,
    							'first_name'=>$key->first_name,
    							'last_name'=>$key->last_name,
    							'date_hire'=>$date_hire,
    							'job'=>$job,
    							'status'=>$status,
    							'sub_unit'=>$sub_unit,
    							'supervisor'=>$supervisor];
    			
    		}// end for
    	}// endif
    	else{ $data =null; }
    	
    	return $data;
    }
	// PARAM 1(ALL) and 2(SORT)

	// EXPAT #######################################################
	public function ReadEmpInfoExpat($type,$employee_id,$id,$param){
		if($type == 1){ $data = $this->FecthAllData(1,$employee_id,$param);
			if(!$data){ $data = null; }
		}
		elseif($type == 2){ $data = \DB::select("select * from view_emp_expat where id = '$id' ");	
			if(!$data){ $data = null; }
		}
		return $data;
	}
	// LOCAL #######################################################
	public function ReadEmpInfoLocal($type,$employee_id,$id,$param){

		if($type == 1){ $data = $this->FecthAllData(2,$employee_id,$param);
			if(!$data){ $data = null; }
		}
		elseif($type == 2){ $data = \DB::select("select * from view_emp_local where id = '$id' ");	
			if(!$data){ $data = null; }
		}
		return $data;
	
	}
	// LOCALIT #######################################################
	public function ReadEmpInfoLocalit($type,$employee_id,$id,$param){
		if($type == 1){ $data = $this->FecthAllData(3,$employee_id,$param);
			if(!$data){ $data = null; }
		}
		elseif($type == 2){ $data = \DB::select("select * from view_emp_localit where id = '$id' ");	
			if(!$data){ $data = null; }
		}
		return $data;
	
	}

	// UPDATE
	public function UpdateEmpListInfo($id,$title){
		$update = \DB::table($this->tables)->where(['id'=>$id])->update(['title'=>$title]);
		if($update){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg; 
	}
	// FIX**
	public function StoreEmpListInfo($title){
		$store = \DB::table($this->tables)->insert(['title'=>$title]);
		if($store){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg;
	}

public function DestroyEMP($id){
		foreach ($id as $key) {
			$past_employee = $this->past_employee($key);
		}
				
		if($past_employee['status'] ==200){
			$data=['id'=>$id,'employee'=>$past_employee['message']];$message='Store Successfully.'; $status=200;
		}else{
				$data=null; $message='Failed Past Employee !'; $status=500;
		}
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
// DESTROY ################################################################
// PARAM ID(INT)
	public function DestroyEmpListInfo($id){
			foreach($id as $key){
				 $destroy = \DB::table($this->table)->where('id','=',$key)->delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################

	/* GET TYPE ID ##################################################
	* PARAM ROLE USER
	*/
	public function Get_Type($role,$employee_id){
		$data = \DB::select("SELECT c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE c.employee_id = b.employee_id and  
							b.role_id = '$role' and c.employee_id = '$employee_id'");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}// END GET TYPE ################################################

	// SEARCH JOB #######################################
	public function Search($string){ 
		$check_lastName = strpos($string," "); 
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));

		if($last_name){
			if(isset($string)){
				$first_name = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select employee_id, concat(first_name,' ',middle_name,' ',last_name) as name 
						from emp where first_name = '$first_name' and middle_name LIKE '$last_name%' limit 5");
			}else{ return ['data'=>null,'status'=>500]; }
		}else{
			$data = \DB::select("SELECT employee_id, concat(first_name,' ',middle_name,' ',last_name) as name 
						from emp where first_name LIKE '$string%' limit 10");
			$status=200;
		}
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################
		public function past_employee($id){
		//$name = \DB::select("select last_name, concat(first_name,' ',middle_name,' ',last_name,' (PAST EMPLOYEE).') as name from emp where employee_id = '$id' ");//
		$name = \DB::table('emp')->where('employee_id','=',$id)->get(['last_name']);
		
		if(isset($name) && $name != '[]'){
			foreach ($name as $key) {
				$last_name = $key->last_name;
			}
			\DB::table('emp')->where('employee_id','=',$id)->update(['last_name'=>"(DEL).".$last_name,'ad_username'=>null]);
			$message = '(DEL)'.$last_name; $status=200;
		}else{ $message='ID Undefined.'; $status=500; }
		return ['message'=>$message,'status'=>$status];
	}

	public function GetFilterEmp(){
		$i=1; $res['job'] = array(); $res['status'] = array(); $res['sub_unit'] = array();
		$job = \DB::table('job')->get(['id','title']);
		$status = \DB::table('jobs_emp_status')->get(['id','title']);
		$sub_unit = \DB::table('sub_unit')->get(['id','sub_unit']);
		//$res['job'] = array();
		foreach ($job as $jobs) {
			$t = ['id'=>$jobs->id,'title'=>$jobs->title];
			array_push($t,$res['job'][$i]);
			$i++;
		}
		foreach ($status as $statuss) {
			//$res['status'][$i] = ['id'=>$statuss->id,'title'=>$statuss->title];
			$t = ['id'=>$statuss->id,'title'=>$statuss->title];
			array_push($t,$res['status']);
			$i++;
		}
		foreach ($sub_unit as $sub_units) {
			//$res['sub_unit'][$i] = ['id'=>$sub_units->id,'title'=>$sub_units->sub_unit];
			$t = ['id'=>$sub_units->id,'title'=>$sub_units->sub_unit];
			array_push($t,$res['sub_unit']);
			$i++;
		}
		return $res;

	}

}
