<?php namespace Larasite\Model\Master\PIM;

use Illuminate\Database\Eloquent\Model;

class ImmigrationIssuer_Model extends Model {

	// Defined Eloquent Methods

	protected $table = 'immigration_issuer';
	protected $fillable = ['id','title'];
	protected $guarded = ['id'];
	protected $msg = ['success'=>['message'=>'Immigration Issuer : Show Records Data.','status'=>200],'error'=>['message'=>'Immigration Issuer : Empty Records Data.','status'=>200]];

	// Defined Model Methods

	// READ
	public function ReadImmigrationIssuer($type,$id){
		if($type == 'All'){
			$data = \DB::select("select id , title from ".$this->table." "); $message = $this->msg['success']['message']; $status = $this->msg['success']['status'];
			if(!$data){ $data = null; $message = $this->msg['error']['message']; $status = $this->msg['error']['status'];}
		}
		elseif($type == 'Sort'){
			$data = \DB::select("select id , title from ".$this->table." where id = '$id' "); $message = $this->msg['success']['message']; $status = $this->msg['success']['status'];
			if(!$data){ $data = null; $message = $this->msg['error']['message']; $status = $this->msg['error']['status'];}
		}
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
	// CHECK Title
	private function check_title($title)
	{
		$get = \DB::table($this->table)->where('title','=',$title)->get();
		if($get){ $data = ['message'=>'Value already in use.','status'=>500,'data'=>null]; }
		else{  $data = false; }
		return $data;
	}

	// UPDATE
	public function UpdateImmigrationIssuer($id,$input){
		$check = $this->check_title($input['title']);
		if($check){ $message = $check['message']; $status = $check['status']; $data = $check['data']; }
		else{
			$update = \DB::table($this->table)->where(['id'=>$id])->update($input); 
			if($update){ $message = 'Update Successfully.'; $status = 200; $data = $this->ReadImmigrationIssuer('Sort',$id)['data']; }
			else{ $message = 'Update Failed'; $status = 500; $data = NULL; }
		}
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
	// STORE
	public function StoreImmigrationIssuer($input){
		$check = $this->check_title($input['title']);
		if($check){ $message = $check['message']; $status = $check['status']; $data = $check['data']; }
		else{
			$store = \DB::table($this->table)->insert($input); 
			if($store){ $message = 'Store Successfully.'; $status = 200; $data = \DB::table($this->table)->where('title','=',$input['title'])->get(); }
			else{ $message = 'Store Failed'; $status = 500; $data = NULL; }
		}
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
	// DESTROY
	public function DestroyImmigrationIssuer($id){
		foreach ($id as $key) {
			$check = \DB::table('emp_immigration')->where('issued_by','=',$key)->get(['issued_by']);
			if($check){
				foreach ($check as $getTitle) {
					$title = \DB::table($this->table)->where('id','=',$getTitle->issued_by)->get(['title']);
					foreach ($title as $setTitle) {
						$message[] = $setTitle->title." : Data can not delete."; $status[] = 500;
					}
				}
			}else{
				$title = \DB::table($this->table)->where('id','=',$key)->get(['title']);
				foreach ($title as $setTitle) {
					$message[] = $setTitle->title." : Delete Successfully."; $status[] = 200;
					\DB::table($this->table)->where('id','=',$key)->delete();
				}
			}
		}
		
		$msgs = null;
		$i = 0;
		foreach ($message as $key) {
			$msgs .= $key."<br>";
		}
		if(in_array(500,$status)){
			$status = 500;
			$msgs .= "<br><br> Please Refresh Page for view effect.";
		}else{ $status = 200; }
		return ['message'=>$msgs,'status'=>$status];
	}
}
