<?php namespace Larasite\Model\Master\PIM;

use Illuminate\Database\Eloquent\Model;

class TerminationReason_Model extends Model {

	// Defined Eloquent Methods
	protected $table = 'termination_reasons';
	protected $fillable = ['id','title'];
	protected $guarded = ['id'];

	// Defined Model Methods

	// READ
	public function ReadTerminationReasons($type,$id){
		if($type == 'All'){
			$data = \DB::select("select id , title from $this->table ");
			if(!$data){ $data = null;}
		}
		elseif($type == 'Sort'){
			$data = \DB::select("select id , title from ".$this->table." where id = '$id' ");	
			if(!$data){ $data = null; }
		}
		return $data;
	}
	// UPDATE
	public function UpdateTerminationReasons($id,$input){
		$check = $this->check($input['title']);
		if($check != '[]' && $check){ $data = ['message'=> 'Value already in use.','status'=>500,'data'=>null]; }
		else{			
			$store = \DB::table($this->table)->where('id','=',$id)->update($input);
			if($store){ $data = ['message'=>'Update Successfully.','status'=>200,'data'=>$check = $this->check($input['title'])[0]]; }
			else{ $data = ['message'=>'Update Failed','status'=>500,'data'=>NULL]; }
		}
		return $data;
	}
	private function check($title)
	{
		$check = \DB::table($this->table)->where('title','=',$title)->get();
		if($check){ return $check; }
		else{ return false; }
	}
	// STORE
	public function StoreTerminationReasons($input){
		$check = $this->check($input['title']);
		if($check != '[]' && $check){ $data = ['message'=> 'Value already in use.','status'=>500,'data'=>null]; }
		else{			
			$store = \DB::table($this->table)->insert($input);
			if($store){ $data = ['message'=>'Store Successfully.','status'=>200,'data'=>$this->check($input['title'])[0]]; }
			else{ $data = ['message'=>'Store Failed','status'=>500,'data'=>NULL]; }
		}
		return $data;
	}
	// FIX**
	public function DestroyTerminationReasons($id){
		$i=0; $msgs = null; $check = null;
		foreach($id as $key){
				$C_title = \DB::table($this->table)->where('id','=',$key)->get(['title']);
				foreach ($C_title as $check_lang) {
				 	$check = \DB::table('emp_termination_reason')->where('termination','=',$check_lang->title)->get(['termination']);
					$title = $check_lang->title;
				}
				
				if($check){ $msg[$i] = $title.' : Data can not be deleted.'; }
				else{ 
					$destroy = \DB::table($this->table)->where('id','=',$key)->delete();
					$msg[$i] = $title.' : Delete Successfully.';
				}
				$i++;
		}
		foreach ($msg as $key) { $msgs .= $key." <br> "; }
		return ['message'=>$msgs,'status'=>200];
	}

}
