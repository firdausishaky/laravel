<?php namespace Larasite\Model\Master\Qualification;

use Illuminate\Database\Eloquent\Model;

class Education_Model extends Model {

	// Defined Eloquent Methods

	protected $table = 'education';
	protected $fillable = ['id','title'];
	protected $guarded = ['id'];

	// Defined Function

	// READ
	public function ReadEdu($type,$id){
		if($type == 1){ $data = \DB::select("CALL view_education('',0)");
			if(!$data){ $data = null; }
		}
		elseif($type == 2){
			$data = \DB::select("CALL view_education('',$id)");	
			if(!$data){ $data = null; }
		}
		return $data;
	}
	// UPDATE
	public function UpdateEdu($id,$input){
		$check = \DB::SELECT("CALL view_education('$input[title]',0)");
		if($check != null){
			$message = 'Value already in use.'; $status = 500; $data = null;
		}else{
			$update = \DB::SELECT("CALL update_education('$input[title]',$id)");
			if($update){ $message='Update Successfully.'; $status=200; $data=['id' => $id, 'title' => $input['title'] ]; }
			else{ $message = 'Update Failed.'; $status=500; $data=NULL; }
		}
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
	// STORE
	public function StoreEdu($input){
		$check = \DB::SELECT("CALL view_education('$input[title]',0)");
		if($check != null){
			$message = 'Value already in use.'; $status = 500; $data = null;
		}else{
			$store = \DB::SELECT("CALL insert_education('$input[title]')");
			$getid = \DB::select("Select id from education where title='$input[title]' order by id desc limit 1");
			if($store){ $message='Store Successfully.'; $status=200; $data=[]; } else{ $message = 'Update Failed.'; $status=500; $data=NULL;}
		}
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
	// DESTROY
	public function DestroyEdu($id){
		$i=0;
		foreach($id as $key){
				$C_title = \DB::table('education')->where('id','=',$key)->get(['title']);
				foreach ($C_title as $check_lang) {
				 	$check = \DB::table('view_emp_education')->where('level','=',$check_lang->title)->get(['level']);
					$title = $check_lang->title;
				}
				
				if($check){ $msg[$i] = "<b>$title</b> : Data can not be deleted."; }
				else{ 
					$destroy = \DB::table('education')->where('id','=',$key)->delete();
					$msg[$i] = "<b>$title</b> : Delete Successfully.";
				}
				$i++;
		}
		$msgs = null;
		foreach ($msg as $key) { $msgs .= "$key <br> "; }
		return ['message'=>$msgs,'status'=>200,'data'=>NULL];
	}

}
