<?php namespace Larasite\Model\Master\Qualification;

use Illuminate\Database\Eloquent\Model;

class Language_Model extends Model {

	// Defined Eloquent Methods

	protected $table = 'language';
	protected $fillable = ['id','title','descript'];
	protected $guarded = ['id'];

	// Defined Model Methods

	private function check($value)
	{
		$get = \DB::table($this->table)->where('title','=',$value)->get();
		return $get;
	}
	// READ
	public function ReadLang($type,$id){
		if($type == 1){ 
			$data = \DB::select("select id , title from language");
			if(!$data){ $data = null; }
		}
		elseif($type == 2){ 
			$data = \DB::select("select id , title from language where id = '$id' ");	
			if(!$data){ $data = null; }
		}
		return $data;
	}
	// UPDATE
	public function UpdateLang($id,$input){
		if($this->check($input)){ $data = ['message'=>'Value already in use.','status'=>500,'data'=>null]; }
		else{
			$update = \DB::table('language')->where(['id'=>$id])->update($input);
			if($update){ $data = ['message'=>'Update Successfully.','status'=>200,'data'=>$this->ReadLang(2,$id)]; }
			else{ $data = ['message'=>'Update Failed','status'=>500,'data'=>null]; }
		}
		return $data; 
	}
	// STORE
	public function StoreLang($input){
		if($this->check($input)){ $data = ['message'=>'Value already in use.','status'=>500,'data'=>null]; }
		else{			
			$store = \DB::table('language')->insert($input);
			$getid =  \DB::select("Select id,title from $this->table where title = '$input[title]' limit 1");
			if($store){	$data = ['message'=>'Store Successfully.','status'=>200,'data'=>$getid[0]]; }
			else{ $data = ['message'=>'Store Failed.','status'=>500,'data'=>NULL]; }
		}
		return $data;
	}
	// DESTROY
	public function DestroyLang($id){
		$i=0;
		$check = NULL;
		foreach($id as $key){
				$C_title = \DB::table('language')->where('id','=',$key)->get(['title']);
				foreach ($C_title as $check_lang) {
				 	$check = \DB::table('view_emp_lang')->where('language','=',$check_lang->title)->get(['language']);
					$title = $check_lang->title;
				}
				
				if($check){ $msg[$i] = $title.' Data can not be deleted.'; }
				else{ 
					$destroy = \DB::table('language')->where('id','=',$key)->delete();
					$msg[$i] = ' Delete Successfully.';
				}
				$i++;
		}
		$msgs = null;
		foreach ($msg as $key) { $msgs .= $key." \n "; }
		return ['message'=>$msgs,'status'=>200,'data'=>NULL];
	}
}
