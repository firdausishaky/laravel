<?php namespace Larasite\Model\Master\Qualification;

use Illuminate\Database\Eloquent\Model;

class Skills_Model extends Model {

	// Defined Eloquent Methods
	protected $table = 'skills';
	protected $fillable = ['id','title','descript'];
	protected $guarded = ['id'];

	// Defined Model Methods

	// READ
	public function ReadSkill($type,$id,$access){
		if ($access['create'] == 0 and $access['delete'] == 0 and $access['read'] == 0 and $access['update'] == 0 ){
					$data = null; $message = 'Unauthorized'; $status = 200; $access = $access;
		}else{
			if($type == 1){ 
				$data = \DB::select("select id , title, descript from skills");
				if(!$data){ $data = null; $message = 'Skill : Empty Records Data.'; $status = 200; $access = $access;}
				else{ $message = 'Skill : Show Records Data.'; $status=200; $access = $access;}
			}
			elseif($type == 2){
				$data = \DB::select("select id , title, descript from skills where id = '$id' ");	
				if(!$data){ $data = null; $message = 'Skill : Empty Records Data.'; $status = 200; $access = $access;}
				else{ $message = 'Skill : Show Records Data.'; $status=200; $access = $access;}
			}
		}
		return ['message'=>$message,'status'=>$status,'data'=>$data,'access'=>$access];
	}
	// UPDATE
	public function UpdateSkill($id,$input){
		$title = $input['title'];
		$data_fin = \DB::SELECT("CALL view_skill('$title')");
		if($data_fin != null ){
			$message = 'Value already in use.'; $status = 500; $data = null;
		}else{			
			$title = $input['title'];
			$descript = $input['descript'];
			$update = \DB::SELECT("CALL update_skills($id,'$title','$descript')");
			if(isset($update)){ $message = 'Update Successfully.'; $status=200; $data = ["descript" =>$descript, "id" => $id, "title" => $title]; }
			else{ $message = 'Update Failed.'; $status=500; $data=null;}
		}
		return ['message'=>$message,'status'=>$status,'data'=>$data]; 
	}
	// STORE
	public function StoreSkill($input){
		$title = $input['title'];
		$data_fin = \DB::SELECT("CALL view_skill('$title')");
		if($data_fin != null){
			$message = 'Value already in use.'; $status = 500; $data = null;
		}else{			
			$update = \DB::SELECT("CALL insert_skill('$input[title]','$input[descript]')");
			if($update){ $message = 'Update Successfully.'; $status=200; $data = \DB::table($this->table)->where('title','=',$input['title'])->get($this->fillable)[0]; }
			else{ $message = 'Update Failed.'; $status=500; $data=null;}
		}
		return ['message'=>$message,'status'=>$status,'data'=>$data]; 
	}
	// DESTROY
	public function DestroySkill($id){
		$i = 1;
		$del = array();
		foreach ($id as $key) {
			$del = \DB::select("delete from skills where id=$key");
		}
		if(empty($del)){ $msg='Status cannot delete.'; $status=500; }
		else{
			foreach($id as $key){
				 $destroy = \DB::select("delete from skills where id=$key");
			}
			if(!empty($destroy)){ $msg=$destroy[0]->message; $status=200; }
			else{ $msg='Error Code.'; $status=500; }
		}
		return ['message'=>$msg,'status'=>$status];
	}

}
