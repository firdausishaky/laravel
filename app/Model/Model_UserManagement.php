<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class Model_UserManagement extends Model {

/*
* Model_UserManagement : 
* table = system user and custom role user
*/
	Protected $table = ['employees','role','ldap'];

	 public function fields(){
		 $attr = ['emp_id'=>'employees.employee_id',
						'first_name'=>'employees.first_name',
						'last_name'=>'employees.last_name',
						'ldap_id'=>'ldap.id_ldap',
						'ldap_name'=>'ldap.ldap_name',
						'ldap_emp_id'=>'ldap.employee_id',
						'ldap_role_id'=>'ldap.role_id',
						'aktif_role'=>'role.active_role',
						'role_id'=>'role.role_id',
						'role_name'=>'role.role_name'];
		return $attr;
	}
	public function ReadSystemUser($type,$id){
		
		$attr = $this->fields();
		if($type == 'All')
		$data = \DB::select("select $attr[role_id] as id, 
							$attr[ldap_name] as Username ,
							$attr[role_name] as 'UserRole',
							concat($attr[first_name],' ',$attr[last_name]) AS 'EmployeeName',
							$attr[aktif_role] as Status
							from employees, ldap, role
							where $attr[ldap_emp_id] = $attr[emp_id] and $attr[ldap_role_id] = $attr[role_id]");
		elseif($type == 'Sort'){
			$data = \DB::select("select $attr[role_id] as id, 
				$attr[role_name] as 'UserRole',
				$attr[aktif_role] as Status
				from role
				where $attr[role_id] > 32");	
		}
		return $data;
	}

	public function StoreSystemUser($domain_name,$role_id,$status){
		$select_domain_name = \DB::select("select employee_id from ldap where ldap_name = '$domain_name' ");

		foreach ($select_domain_name as $temp_select) {
			$domain_name2 = $temp_select->employee_id;
		}

		$insert = \DB::table('ldap')->insert(['employee_id'=>$domain_name2,'role_id'=>$role_id,'active_role'=>$status]);
		if($insert){
			$msg = 'success';
			return \Response::json(['msg'=>$msg]);
		}
		else{
			$msg = 'failed';
			return \Response::json(['msg'=>$msg]); 
		}
	}

// // FORM JOBS #######################################################
// 	public function ReadJob($type,$id){
// 		if($type == 'All'){
// 			$data = \DB::select("select job_id as id , job_title as title 
// 					from jobs");
// 		}
// 		elseif($type == 'Sort'){
// 			$data = \DB::select("select job_id as id , job_title as title 
// 					from jobs where job_id = '$id' ");	
// 		}
// 		return $data;
	
// 	}
// 	public function JobUpdate($id,$title,$descript){
// 		$update = \DB::table('jobs')->where(['job_id'=>$id])->update(['job_title'=>$title,'job_descript'=>$descript]);
// 		if($update){
// 			$msg = 'success';
// 			return \Response::json(['msg'=>$msg]);
// 		}
// 		else{
// 			$msg = 'failed';
// 			return \Response::json(['msg'=>$msg]); 
// 		}
// 	}
// 	public function JobStore($id,$title,$descript){
// 		$update = \DB::table('jobs')->insert(['job_id'=>$id,'job_title'=>$title,'job_descript'=>$descript]);
// 		if($update){
// 			$msg = 'success';
// 			return \Response::json(['msg'=>$msg]);
// 		}
// 		else{
// 			$msg = 'failed';
// 			return \Response::json(['msg'=>$msg]); 
// 		}
// 	}
// 	public function JobDestroy($id){
// 		$update = \DB::table('jobs')->where('job_id','=',$id)->delete();
// 		if($update){
// 			$msg = 'success';
// 			return \Response::json(['msg'=>$msg]);
// 		}
// 		else{
// 			$msg = 'failed';
// 			return \Response::json(['msg'=>$msg]); 
// 		}
// 	}

	// END FORM JOBS
}
