<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class Nationalities_Model extends Model {

	//

	Protected $tables = 'nationality';
	Protected $fields = ['title'];
	Protected $hiddden = ['id'];


	// FORM NATIONALITIES #######################################################
	// FIX**
	public function ReadNationalities($type,$id){
		if($type == 'All'){
			$data = \DB::select("select id , title
					from ".$this->tables." ");
			if(!$data){
				$data = null;
			}
		}
		elseif($type == 'Sort'){
			$data = \DB::select("select id , title 
					from ".$this->tables." where id = '$id' ");	
			if(!$data){
				$data = null;
			}

		}
		return $data;
	
	}
	// FIX**
	public function UpdateNationalities($id,$title){
		if(\DB::table($this->table)->where('title','=',$title)->get()){
			$message = 'Value already in use.'; $status = 500; $data = null;
		}else{
			$update = \DB::table($this->tables)->where(['id'=>$id])->update(['title'=>$title]);
			if($update){
				$data =  \DB::select("Select id,title from nationality where title = '$title' limit 1");
				$message = 'Store Successfully.'; $status = 200;
			}else{
				$message = 'Store Failed.'; $status = 500; $data = NULL;
			}
		}
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
	// FIX**
	public function StoreNationalities($title){
		if(\DB::table($this->table)->where('title','=',$title)->get()){
			$message = 'Value already in use.'; $status = 500; $data = null;
		}else{
			$store = \DB::table($this->tables)->insert(['title'=>$title]);
			$data =  \DB::select("Select id,title from nationality where title = '$title' limit 1");
			$message = 'Store Successfully.'; $status = 200;
		}	
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
	// FIX**
	public function DestroyNationalities($id){
		$i=0;
		foreach($id as $key){
				$C_title = \DB::table($this->tables)->where('id','=',$key)->get(['title']);
				foreach ($C_title as $check_lang) {
				 	$check = \DB::table('emp')->where('nationality','=',$key)->get(['nationality']);
					$title = $check_lang->title;
				}
				if($check){ $msg[$i] = $title.', Data can not be deleted.'; $status[$i] = '500'; }
				else{ 
					$destroy = \DB::table($this->tables)->where('id','=',$key)->delete();
					$msg[$i] = $title.', Delete Successfully.';
					$status[$i] = '200';
				}
				$i++;
		}
		$msgs = null;
		foreach ($msg as $key) {
			$msgs .= $key." \n ";
		}
		if(in_array('200',$status)){
			$statuss = 200;
		}else{
			$statuss = 500;
		}
		return ['message'=>$msgs,'status'=>$statuss];

	}

	// END FORM NATIONALITIES


}
