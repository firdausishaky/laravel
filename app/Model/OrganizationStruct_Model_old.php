<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class OrganizationStruct_Model extends Model {

	//
	protected $table = 'sub_unit';


	public function count_department_emp(){
		$q = "SELECT 
		concat(t1.sub_unit,' (', (select concat(sub_unit) from sub_unit where parent_id =1 ) ,')') as parent_1,
		t2.sub_unit as parent_2,
		t3.sub_unit as parent_3
		FROM `sub_unit` as t1
		left join sub_unit as t2 on t2.parent_id = t1.id
		left join sub_unit as t3 on t3.parent_id = t2.id
		WHERE t1.sub_unit  = 'Leekie.ent.inc'";
		
		$count = \DB::select($q);
		if(isset($count)){
			$data = $count;
		}else{
			$data = null;
		}
		return $data;
	}
//####################
// READ ORGANIZATION
	public function Read_Structure(){
		$i=1;
		$j=1;
		$get_count = \DB::select("select count(distinct parent_id) as count_parent from sub_unit where parent_id != '' "); // count all parent
		$get = \DB::select("SELECT t1.id, t1.parent_id ,t1.sub_unit, count(t2.parent_id) as count 
							FROM sub_unit AS t1 
							LEFT JOIN sub_unit AS t2 ON t2.parent_id = t1.id 
							WHERE t2.parent_id = $i");
				foreach ($get as $first) {
				$get = \DB::select("SELECT DISTINCT t2.id , t2.parent_id ,t2.sub_unit, t2.descript
									FROM sub_unit AS t1
									LEFT JOIN sub_unit AS t2 ON t2.parent_id = t1.id 
									WHERE t2.parent_id= $i");

					$get_count = \DB::select("select count(distinct parent_id) as count_parent from sub_unit where parent_id != $i");
					foreach ($get as $second) {
						$get = \DB::select("SELECT DISTINCT t2.id , t2.parent_id ,t2.sub_unit, t2.descript
										FROM sub_unit AS t1
										LEFT JOIN sub_unit AS t2 ON t2.parent_id = t1.id
										WHERE t2.parent_id= $second->id");
						$r_count = \DB::select("SELECT count(t2.parent_id) as count from sub_unit as t1 left join sub_unit as t2 on t2.parent_id = t1.id where t2.parent_id = $second->id");
						foreach ($r_count as $key) { $count = $key->count; }
						$ger1[$j] = ['id'=>$second->id,'parent_id'=>$second->parent_id,'sub_unit'=>$second->sub_unit,'descript'=>$second->descript,'count'=>$count,'sub'=>$get];
						$data = ['id'=>$first->id,'parent_id'=>$first->parent_id,'sub_unit'=>$first->sub_unit,'count'=>$first->count,'sub'=>$ger1 ];
						$j++;
					 }
					$i++;
				}
		return $data;

	}
/* END READ STRUCTURE */
	/* STORE STRUCTURE*/
	public function Store_Structure($input){
		if(isset($input) && $input != null){
			$store_input =  $this->set_input($input);
			
			$store = \DB::table($this->table)->insert($store_input);

			if($store){ $message = 'Store Successfully.'; $status=200; $data=$this->GetID($input['sub_unit']);}
			else{ $message ='Store Failed'; $status=500; $data=null; }
		}else{$message ='Input Null'; $status=500; $data=null;}
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
/* END STORE STRUCTURE */
	/* UPDATE STRUCTURE*/
	public function Update_Structure($id,$input){
		if(isset($input,$id) && $input != null && $id != null && $id != 'undefined'){
			$store = \DB::table($this->table)->where('id','=',$id)->update($this->set_input($input));
			if($store){ $message = 'Update Successfully.'; $status=200; $data=$this->GetID($input['sub_unit']);}
			else{ $message ='Update Failed'; $status=500; $data=null; }
		}else{$message ='Input Null'; $status=500; $data=null;}
		return ['data'=>$data,'message'=>$message,'status'=>$status];
	}
/* END UPDATE STRUCTURE */


	/*  DESTROY */
	public function Destroy_Struct($id){
		//$destroy = null;
			if($id == 1){
				$status = 500;
				return $status;
			}else{
				$del= \DB::table('job_history')->where('sub_unit','=',$id)->get(['id']);
				$check = \DB::select("SELECT id from sub_unit where parent_id = $id");
				$uses = ['parent'=>$check,'uses'=>$del];
				
				if($uses['parent'] && $uses['uses']){
					$status = 500;
				}else{
					\DB::table('sub_unit')->where('id','=',$id)->delete();
					$status = 200;
				}
				return $status;	
			}
			
			//$name = \DB::select("select sub_unit from sub_unit where id = $key ");
			// foreach ($name as $key) {$title = $key->sub_unit;}
			
			// if($del == true && $check){ $msg = $title.' : Cannot Delete.';$status= 500;
			// }else{
			// 	foreach($id as $key){ $destroy = \DB::table('sub_unit')->where('id','=',$key)->delete();}
			// 	if($destroy){$msg = $title.' : Delete Successfully.'; $status=200;}
			// 	else{$msg =  'Delete Failed'; $status=500;}	
			// }
		
		// if($status == 200){ return ['message'=>$msg,'status'=>$status,'fix'=>200];	
		// }else{ return ['message'=>$msg,'status'=>$status,'fix'=>500];}
	}
/* END DESTROY */


	/* SETUP INPUT */
	public function Set_Input($input){
		return[ 'descript'=>$input['descript'],'sub_unit'=>$input['sub_unit'],'parent_id'=>$input['parent_id']];
	}
/* END SETUP INPUT */
	/* GET ID */
	public function GetID($sub_unit){
		$get = \DB::select("SELECT id from sub_unit where sub_unit = '$sub_unit' order by id desc limit 1");
		foreach ($get as $key) { $data = $key->id; }
		return $data;
	}
/* END GET ID */
}
