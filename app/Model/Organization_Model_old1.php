<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class Organization_Model extends Model {

	protected $table = 'general_information';
	protected $hidden = ['created_at','updated_at','path'];
	protected $field = "gi.id,gi.name,gi.phihealth_numb,gi.bir_numb,gi.sss_numb,gi.pag_big_numb,gi.phone,gi.rdo_numb,gi.email,gi.fax,gi.address1,gi.address2,gi.city,gi.state,gi.zip,gi.country,gi.filename";

// FORM GI
	public function Read_GI($access = array()){
			$local = \DB::select("select (SELECT COUNT(local_it) FROM (SELECT * FROM emp where local_it = 1 UNION ALL SELECT * FROM emp where local_it =3) as local) as temp limit 1");								
			$expat= \DB::select("select (SELECT COUNT(local_it) FROM (SELECT * FROM emp where local_it = 2 AND employee_id NOT IN ('2014888','2014999')) as local) as temp1 limit 1");								
			$local =  $local[0]->temp;
			$expat  = $expat[0]->temp1;
			$get = \DB::SELECT("select $local as local, $expat as expat,gi.id,gi.name,gi.phihealth_numb,gi.bir_numb,gi.sss_numb,gi.pag_big_numb,gi.phone,gi.rdo_numb,gi.email,gi.fax,gi.address1,gi.address2,gi.city,gi.state,gi.zip,gi.country,gi.filename from general_information gi ;");
			if($get == null){ $data = ['message'=>'General Information : Empty Data.','status'=>200,'data'=>NULL,'access'=>$access]; }
			else{ $data = ['message'=>'General Information : Show Records Data.','status'=>200,'data'=>$this->input2($get),'access'=>$access]; }
		return $data;
	
	}
// GET ID
	public function GetID($id){
		$r = \DB::select("select id from general_information where id = '$id'");
		$data = $r;
		return $data;
	}
// STORE
	public function Store_GI($input){
		$store = \DB::table('general_information')->insert($this->set_input($input));
		if($store){ $msg = 'success'; }else{ $msg = 'fail'; }
		return $msg;
	}
// USES Update
	public function Update_GI($id,$input){
		$store = \DB::table('general_information')->where('id','=',$id)->update($input);

		if($store){ $message="Update Successfully"; $status=200; $data=$this->Read_GI(); }else{ $message='Update Failed.'; $status=500; $data=null; }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
// CHECK ID
	public function check_file_id($id){
		$r = \DB::select("SELECT filename from general_information where id = '$id' ");
		if(!$r){ return NULL; }
		foreach ($r as $key) { $data = $key->filename; }
		return $data;
	}
// DESTROY JOB
	public function DestroyGI($id){

			$destroy = \DB::table('general_information')->where('id','=',$id)->delete();
			
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{ $msg = null; }	
			return $msg;
	}
// SET INPUT
	private function set_input($input)
	{
		$array = ['name'=>$input['name'],'phihealth_numb'=>$input['phihealth_numb'],'pag_big_numb'=>$input['pag_big_numb'],
				'phone'=>$input['phone'],'email'=>$input['email'],'bir_numb'=>$input['bir_numb'],'sss_numb'=>$input['sss_numb'],'rdo_numb'=>$input['rdo_numb'],
				'fax'=>$input['fax'],'address'=>$input['address']];

		return $array;
		if(isset($input['id'])){ $array['id'] = $input['id']; }
		return $array;
	}
	private function input2($array)
	{
		foreach ($array as $key) {
			$data['local'] = $key->local;
			$data['expat'] = $key->expat;
			$data['id'] = $key->id;
			$data['name'] = $key->name;
			$data['phihealth_numb'] = $key->phihealth_numb;
			$data['pag_big_numb'] = $key->pag_big_numb;
			$data['phone'] = $key->phone;
			$data['email'] = $key->email;
			$data['address1'] = $key->address1;
			$data['address2'] = $key->address2;
			$data['city'] = $key->city;
			$data['state'] = $key->state;
			$data['zip'] = $key->zip;
			$data['country'] = $key->country;
			$data['sss_numb'] = $key->sss_numb;
			$data['bir_numb'] = $key->bir_numb;
			$data['rdo_numb'] = $key->rdo_numb;
			$data['fax'] = $key->fax;
			$data['filename'] = $key->filename;
		}
		return $data;
	}



}
