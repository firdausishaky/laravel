<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class PersonalPict_Model extends Model {

	protected $table = 'emp_picture';
	//protected $hidden = ['employee_id','created_at','updated_at'];
	protected $guarded = ['id','employee_id'];


// GET READ #########################
	public function Read_Pict($employee_id,$id){
	
		if(isset($id) != null){
			$check = $this->Get_ID($id);
			if(isset($check) != null && $check['status'] == 200){
				$get_data = \DB::table($this->table)->where('id','=',$id)->get(['id','path','filename','type']);	
				foreach ($get_data as $key) {
					$get['path'] 		= $key->path;
					$get['filename']	= $key->filename;
					$get['id']			= $key->id;
				}
				$data = ['id'=>$get['id'],'path'=>$get['path'],'filename'=>$get['filename']];
				$message='Read Data.'; $status=200;
			}else{ $message=$check['message']; $status=$check['status']; $data=null; }
		}else{
			// untuk index
			$get_data = \DB::table($this->table)->where('employee_id','=',$employee_id)->get(['id','path','filename','type']);
			if(isset($get_data) != null){
				foreach ($get_data as $key) {
					$get['path'] 		= $key->path;
					$get['filename']	= $key->filename;
					$get['id']			= $key->id;
					$get['type']			= $key->type;
				}
				$data = ['id'=>$get['id'],'path'=>$get['path'],'filename'=>$get['filename'],'type'=>$get['type']];
				$message='Read Data.'; $status=200;
			}else{ $data =null; $message='Data not found.'; $status=404; }
		}
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
// END READ ###################

// GET PATH FILE #########################
	public function Get_Path($id){
		if(isset($id) != null){
			$check = $this->Get_ID($id);
			if(isset($check) != null && $check['status'] == 200){
				$get_data = \DB::table($this->table)->where('id','=',$id)->get(['path','filename']);	
				foreach ($get_data as $key) {
					$path = $key->path;
					$filename = $key->filename;
				}
				$data = ['path'=>$path,'filename'=>$filename];
				$message='Get Path.'; $status=200;
			}else{ $message=$check['message']; $status=$message['200']; $data=null; }
		}else{ $message='ID not null.'; $status=500; $data=null;}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
	}
// END GET PATH FILE ###################

// STORE ################################
	public function Store_Pict($input){
		if(isset($input) != null){
				\DB::table($this->table)->insert($this->Set_Input($input));	
				$message='Update Successfully.'; $status=200;
		}else{ $message='ID not null.'; $status=500; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
	}
// END STORE ###########################

// GET UPDATE ###########################
	public function Update_Picture($id,$filename,$path){
		
		if(isset($id,$filename,$path) != null){
			// $check = $this->Get_ID($id);
			// if(isset($check) != null && $check['status'] == 200){
				$update = \DB::table($this->table)->where('id','=',$id)->update(['filename'=>$filename,'path'=>$path]);	
				if($update){
					$message='Update Successfully.'; $status=200;	
				}else{ $message='Update failed.'; $status=500; }
				
			//}else{ $message=$check['message']; $status=$message['200']; }
		}else{ $message='ID not null.'; $status=500; }
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
	}
// END UPDATE ###########################

// GET CHECK ID ############################
	public function Get_ID($file){
		$get = \DB::table($this->table)->where('filename','=',$file)->get(['id','employee_id']);
		foreach ($get as $keys) {
			$employee_id = $keys->employee_id;
			$id = $keys->id;
		}
		if($employee_id){
			$get = \DB::table('emp')->where('employee_id','=',$employee_id)->get(['employee_id']);
			foreach ($get as $key) {
				$value = $key->employee_id;
			}
			$message=$id; $status=200;
		}else{ $message='ID not found.'; $status=500; }
		return ['data'=>$message,'status'=>$status];
	}
// END CHECK ID ###########################


public function check_file($id){
	$get = \DB::table($this->table)->where('id','=',$id)->get(['filename']);
	foreach ($get as $key) {
		$data = $key->filename;
	}
	return $data;
}

// GET DELETE ############################
	public function Destroy_Pict($id){
		if(isset($id)){
			$check = $this->Get_ID($id);	
			if(isset($check) != null && $check['status'] == 200 ){
				\DB::table($this->table)->where('id','=',$id)->delete();
				$message='Delete Successfully.'; $status=200;
			}else{ $message=$check['message']; $status=$check['status']; }
		}else{ $message='ID not found.'; $status=500; }
		return ['message'=>$message,'status'=>$status];
	}
// END DELETE ###########################

// SET INPUT ###########################
	public function Set_Input($input){
		return([
				'employee_id'	=> $input['employee_id'] ,
				'filename'		=> $input['filename'] ,
				'path'			=> $input['path']
			]);
	}
// END SET INPUT #######################

// GET TYPE ID ########################################
//PARAM ROLE USER
	public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id ");
		if(!$data){
			$data = null;
		}
		return $data;
	}
// END GET TYPE #######################################

// CHECK EMP ############################################
	public function Check_emp($id){
		$get = \DB::table('emp')->where('employee_id','=',$id)->get(['employee_id']);
		if($get != null && $get != '[]'){
			foreach ($get as $key){ $data = $key->employee_id; }
		}else{ $data=null; }
		return $data;
	}
// END CHECK EMP ########################################

// CHECK EMP ############################################
	public function check_file_id($id){
		$get = \DB::table($this->table)->where('id','=',$id)->get(['filename']);
		if($get != null && $get != '[]'){
			foreach ($get as $key){ $data = $key->filename; }
		}else{ $data=null; }
		return $data;
	}
// END CHECK EMP ########################################
}
