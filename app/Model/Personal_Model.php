<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class Personal_Model extends Model {

	// PERSONAL
	protected $table = 'emp';
	protected $table_attach = 'emp_attachment';
	protected $fill_attach = ['id','employee_id','filename','descript','size','type','added_by'];


	// READ ########################################################
		public function Read_Personal($id){
			$pic = null;
			//$result = \DB::table('emp')->where('employee_id','=',$id)->get();
			$result = \DB::select("
					select a.first_name, a.middle_name, a.last_name, a.ad_username, a.employee_id, a.biometric, a.gender, a.sss , a.philhealth, a.hdmf, a.maiden_name, a.hmo_account, a.hmo_numb, a.rtn, a.tin, a.mid, a.date_of_birth, a.place_of_birth,a.nationality,a.marital_status,a.religion, a.local_it
					from emp a
					where a.employee_id = $id
				");
			if($result){
				foreach ($result as $key) {
					$nation = $key->nationality;
					$datas['first_name'] = $key->first_name;
					$datas['middle_name'] = $key->middle_name;
					$datas['last_name'] = $key->last_name;
					$datas['ad_username'] = $key->ad_username;
					$datas['employee_id'] = $key->employee_id;
					$datas['gender'] = $key->gender;
					$datas['local_it'] = ($key->local_it == "3" ? "LOCAL" : ( $key->local_it == "2" ?  'LOCAL IT' : "EXPAT"));
					$datas['philhealth'] = $key->philhealth;
					$datas['hdmf'] = $key->hdmf;
					if(strtolower($key->gender) == 'female' && strtolower($key->marital_status) != 'single'){ $datas['maiden_name'] = $key->maiden_name; $datas['action_maiden'] = 1;}
					else{ $datas['maiden_name'] = NULL; $datas['action_maiden'] = 0; }
					$datas['hmo_account'] = $key->hmo_account;
					$datas['hmo_numb']  = $key->hmo_numb;
					$datas['rtn'] = $key->rtn;
					$datas['tin']  = $key->tin;
					$datas['mid'] = $key->mid;
					$datas['biometric'] = $key->biometric;
					$datas['date_of_birth'] = $key->date_of_birth;
					$datas['place_of_birth'] = $key->place_of_birth;
					$datas['marital_status'] = $key->marital_status;
					$datas['religion'] = $key->religion;
					$datas['sss'] = $key->sss;

					if($nation){
						$r = \DB::table('nationality')->where('id','=',$nation)->get(['title']);
						foreach ($r as $keys) {
							$datas['nationality'] = $keys->title;
						}
					}else{ $datas['nationality'] = null; }
				}
				$data=$datas;	$status=200;

				$result2 = \DB::select("select b.filename from emp a, emp_picture b where a.employee_id = b.employee_id and b.employee_id = '$id' ");
				if($result2){
					foreach ($result2 as $key) {
						$file = $key->filename;
					}
					$pic = $file;
				}else{ $pic = null;}

			}else{ $data='Data not found.';	$status=500; }
			return ['data'=>$data,'status'=>$status,'picture'=>$pic];
		}
	//END READ ######################################################


	// GET TYPE ID ##################################################
	//PARAM ROLE USER
	public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id
							FROM ldap b, emp c
							WHERE b.role_id = '$role' and c.employee_id = '$employee_id' ");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}// END GET TYPE ################################################

	//CHECK PICTURE #################################################
		public function check_picture($id){
			$result = \DB::select("select b.filename from emp a, emp_picture b where a.employee_id = b.employee_id and b.employee_id = '$id' ");
			if($result){
				foreach ($result as $key) {
					$file = $key->filename;
				}
				$data = $file; $status=200;
			}else{ $data = null; $status=404;}
			return ['data'=>$data,'status'=>$status];
		}
	// END PICTURE ##################################################

	// UPDATE #######################################################
		public function Update_Personal($input,$id,$maiden_name){
			if(isset($id) != null){
				$check = $this->Read_Personal($id);
				if(isset($check) != null){
					//$t = $this->Set_Input($input);
					$update = \DB::table($this->table)->where('employee_id','=',$id)
									->update($input);
					$message='Update Successfully.'; $status=200; $data = $this->Read_Personal($id)['data'];

				}else{ $message='ID not found.'; $status=500; $data = NULL;}
			}else{ $message='Update Failed.'; $status=500; $data = NULL;}
			return ['message'=>$message,'status'=>$status,'data'=>$data];
		}
	// END UPDATE ##################################################

	// SET INPUT###################################################
		public function Set_Input($input){
			return ([
				'first_name'	=> $input['first_name'],
				'middle_name'    => $input['middle_name'],
				'last_name'      => $input['last_name'],
				'ad_username'    => $input['ad_username'],
				'sss'            => $input['sss'],
				'philhealth'     => $input['philhealth'],
				'hdmf'           => $input['hdmf'],
				'maiden_name'    => $input['maiden_name'],
				'hmo_account'    => $input['hmo_account'],
				'hmo_numb'       => $input['hmo_numb'],
				'rtn'            => $input['rtn'],
				'tin'            => $input['tin'],
				'mid'            => $input['mid'],
				'gender'         => $input['gender'],
				'marital_status' => $input['marital_status'],
				'nationality'    => $input['nationality'],
				'date_of_birth'  => $input['date_of_birth'],
				'place_of_birth' => $input['place_of_birth'],
				'religion'       => $input['religion'],
				'biometric'       => $input['biometric']
			]);
		}
	// END #######################################################

//###################################################################################################

/* METHOD MODEL PERSONAL ATTACHMENT */

	/* READ ATTACH */
	public function Read_Personal_Attach($id, $access){
		//$i = 0;
		//foreach ($id as $key) { $check[$i++] = \DB::table($this->table_attach)->where('employee_id','=',$id)->get();}
		$check = \DB::table($this->table_attach)->where('employee_id','=',$id)->get();

		if($check !== null && $access == "Access Granted"){
			return ['message'=>'Read Data '.$this->table_attach,'status'=>200,'data'=>$check];
		}else{
			return ['message'=>'empty data', 'status'=>500,'data'=>null];
		}
		return ['message'=>null,'status'=>500,'data'=>null];
	}
/* END READ ATTACH */

	/* STORE ATTACH */
	public function Destroy_Personal_Attach($id){
	foreach($id as $key){
		if($this->check_file_id2($key) != null){
			$move = $this->move_file($this->check_file_id2($key));
			$destroy = \DB::table('emp_attachment')->where('id','=',$key)->delete();
		}else{ $destroy = \DB::table('emp_attachment')->where('id','=',$key)->delete();}
	}
	if($destroy){$msg='Delete Successfully.'; $status=200;}else{$msg = null; }
	return ['message'=>$msg,'status'=>$status];
	}
/* END DESTROY */

	/* UPDATE */
	public function Update_Personal_Attach($id){
	foreach($id as $key){
		if($this->check_file_id2($key) != null){
			$move = $this->move_file($this->check_file_id2($key));
			$destroy = \DB::table('emp_attachment')->where('id','=',$key)->delete();
		}else{ $destroy = \DB::table('emp_attachment')->where('id','=',$key)->delete();}
	}
	if($destroy){$msg = 'Delete Successfully.'; $status=200;}else{$msg = null; }
	return ['message'=>$msg,'status'=>$status];
	}
/* END UPDATE */

	public function Find_AddedBy($id){
		$data_added = \DB::table('emp')->where('employee_id','=',$id)->get(['ad_username']);
		foreach ($data_added as $key) {$ad_username = $key->ad_username;}
		return $ad_username;
	}

	// GET ID EMP ###############################################
	public function GetID($id){
		$data = \DB::select("Select id from emp_attachment where filename = '$id' order by id DESC limit 1");
		foreach ($data as $key) {
			$id = $key->id;
		}
		return $id;
	}
// END GET EMP ###########################################

	// CHECK ID ###############################################################
// PARAM ID
	public function check_file_id($id){
		$data = array();
		$r = \DB::select("SELECT filename from emp_attachment where id = '$id' ");
		foreach ($r as $key) {
			$data = $key->filename;
		}
		return $data;
	}
// END CHECK ID ###########################################################

// CHECK ID ###############################################################
// PARAM ID
	public function check_file_id2($id){
		$r = \DB::select("SELECT filename from emp_attachment where id = '$id'");
		foreach ($r as $key) {
			$data = $key->filename;
		}
		return $data;
	}
// END CHECK ID ###########################################################

	// MOVE FILE IN TRASH ########################################################################
	public function move_file($filename){
		$get_data = \DB::select("SELECT `filename`, `path` from emp_attachment where filename = '$filename' ");
		foreach ($get_data as $key) {
			$data['path'] = $key->path;
			$data['filename'] = $key->filename;
		}
		if(\Storage::disk('local')->exists($data['path']."/".$data['filename'])){
			\Storage::disk('local')->move($data['path']."/".$data['filename'], "/Trash/".$data['filename']);
			if( \Storage::disk('local')->exists("/Trash/".$data['filename']) ){
				$message = 'Update Successfully.';
				$status = 200;
			}else{
				\Storage::disk('local')->move("/Trash/".$data['filename'],$data['path']."/".$data['filename']);
				$message = 'gagal move file to trash';
				$status = 500;
			}
		}
		else{
			$message = 'File tidak ditemukan, atau file telah dihapus check folder trash';
			$status = 404;
		}
		return ['message'=>$message,'status'=>$status];
	}
// END MOVE FILE #################################################################################
}
