<?php namespace Larasite\Model\Qualification;

use Illuminate\Database\Eloquent\Model;

class EmpEducation_Model extends Model {


protected $table = 'emp_education';

// READ ###########################################
public function Read_Education($employee_id,$id){
	if(isset($id)){
		$result = \DB::table('emp_education')->where('id','=',$id)->get();
		if(isset($result) != null || isset($result) != '[]'){ $data=$result; $status=200; }else{ $data=null; $status=500; }
	}else{
		$result = \DB::table('view_emp_education')->where('emp','=',$employee_id)->get();
		if($result){ 
			foreach ($result as $key) {
				$key->from_year = Date("Y-m",strtotime($key->from_year));
				$key->to_year = Date("Y-m",strtotime($key->to_year));
			}
			$data=$result; $status=200; 
		}else{ $data=null; $status=500; }
	}
	return ['data'=>$data,'status'=>$status];
}
// END READ ###########################################


//STORE #################################################
public function Store_Education($input){
	// check job
			$Get_Input = $this->SetInput($input);
			$Get_Input['employee_id'] = $input['id'];
			$store = \DB::table('emp_education')->insert($Get_Input);
			if(isset($store) != null || isset($store) != '[]'){
				$data=$this->GetID($Get_Input['employee_id']); $message='Store Successfully.'; $status=200;
			}else{$data=null; $message='Store Failed !'; $status=500;}	
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
// END STORE ############################################

//UPDATE #################################################
public function Update_Education($input,$id){
	$employee_id = $this->Get_emp($id);
	$Get_Input = $this->SetInput($input);
	//$emp = $this->Get_emp();
	$Get_emp['employee_id'] = $input['employee_id'];
	$update = \DB::table('emp_education')
				->where('id','=',$id)
				->update($Get_Input);
	return $update;
	}
// END STORE ############################################

// DESTROY DEPENDENT ################################################################
// PARAM ID(INT)
	public function Destroy_Education($id){
		
			foreach($id as $key){
				 $destroy = \DB::table('emp_education')->where('id','=',$key)->delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################


// GET ID ###############################################
public function Get_emp($id){
		$data = \DB::select("Select employee_id from emp_education where id = $id");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID ###########################################


// GET ID EMP ###############################################
public function GetID($id){
		$data = \DB::select("Select id from emp_education where employee_id = '$id' order by id DESC limit 1");
		foreach ($data as $key) {
			$id_immigration = $key->id;
		}
		return $id_immigration;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
//PARAM ROLE USER
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################

public function SetInput($input){
	return([
		'education'=>$input['level'],
		'institution'=>$input['institution'],
		'major'=>$input['major'],
		'from_year'=>$input['from_year'],
		'to_year'=>$input['to_year']
		]);
}// end SETINPUT
public function check_job($id){
	$get = \DB::table('education')->where('id','=',$id)->get(['title']);
	if(isset($get) != null | isset($get) != '[]'){
		$status=200; $data=$get;
	}else{ $status=500; $data=null; }
	return $status; // response status
}//

// SEARCH JOB #######################################
	public function Search($string){ 
		$check_lastName = strpos($string," "); 
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));
		
		if($last_name){
			if(isset($string)){
				$start = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select id,title from job where title = '$start' limit 5");
			}
			else{ $data = null; $status=500;}
		}else{ $data = \DB::select("SELECT id,title from job where title LIKE '$string%' limit 5"); $status = 200;}
		if(isset($data) != null || isset($data) != '[]'){ $status=200; }else{ $data=null; $status=500; }
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################


}
