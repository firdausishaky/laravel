<?php namespace Larasite\Model\Qualification;

use Illuminate\Database\Eloquent\Model;

class EmpLanguage_Model extends Model {

protected $table = 'emp_lang';

/**
*	FIX CRUD, PERSONAL LANGUAGE
*/


// READ ###########################################
public function Read_Language($employee_id,$id){
	if(isset($id)){
		return 1;
	}else{
		$result = \DB::select(" select id, language , fluent_level as fluent from view_emp_lang where employee_id = '$employee_id' ");
		if(isset($result) && $result){ $data=$result; $status=200; }else{ $data=null; $status=500; }
	}
	return ['data'=>$data,'status'=>$status];
}
// END READ ###########################################


//STORE #################################################
public function Store_Language($input){
	// check job
		if($this->check_job($input['language']) == 200){
			$Get_Input = $this->SetInput($input);
			//;
			$Get_Input['employee_id'] = $input['id'];
			$id = $Get_Input['employee_id'];
			$lang = $Get_Input['language'];
			$fluent = $Get_Input['fluent_level'];
			$store = \DB::SELECT(" insert into emp_lang (employee_id, language, fluent_level,created_at) values ('$id',$lang,'$fluent',now()) ");
			//$store = \DB::table('emp_lang')->insert(['employee_id'=>$input['id'],'language'=>$input['language'],'fluent_level'=>$input['fluent']]);
			if(isset($store) == null ){
				$data=$this->GetID($Get_Input['employee_id']); $message='Store Successfully.'; $status=200;
			}else{$data=null; $message='Store Failed !'; $status=500;}	
		}else{ $data=null; $message='Job Undefined !'; $status=500; }
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
// END STORE ############################################

//UPDATE #################################################
public function Update_Language($input,$id){
	$data = \DB::select("Select employee_id from emp_lang where id = $id limit 1" );
	$employee_id = $data[0]->employee_id;
	$Get_Input = $this->SetInput($input);
	//$emp = $this->Get_emp();
	$Get_emp['employee_id'] = $input['employee_id'];
	$update = \DB::table('emp_lang')
				->where('id','=',$id)
				->update($Get_Input);
	return $update;
	}
// END STORE ############################################

// DESTROY DEPENDENT ################################################################
// PARAM ID(INT)
	public function Destroy_Language($id){
		
			foreach($id as $key){
				//$this->move_file($filename);
				$destroy = \DB::table('emp_lang')->where('id','=',$key)->delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################


// GET ID ###############################################
public function Get_emp($id){
		$data = \DB::select("Select employee_id from emp_lang where id = $id");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID ###########################################


// GET ID EMP ###############################################
public function GetID($id){
		$data = \DB::select("Select id from emp_lang where employee_id = '$id' order by id DESC limit 1");
		foreach ($data as $key) {
			$id_immigration = $key->id;
		}
		return $id_immigration;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
//PARAM ROLE USER
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################

public function SetInput($input){
	return([
		'language'=>$input['language'],
		'fluent_level'=>$input['fluent']
		]);
}// end SETINPUT
public function check_job($id){
	$get = \DB::table('language')->where('id','=',$id)->get(['title']);
	if(isset($get) != null | isset($get) != '[]'){
		$status=200; $data=$get;
	}else{ $status=500; $data=null; }
	return $status; // response status
}//

// SEARCH JOB #######################################
	public function Search($string){ 
		$check_lastName = strpos($string," "); 
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));
		
		if($last_name){
			if(isset($string)){
				$start = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select id,title from job where title = '$start' limit 5");
			}
			else{ $data = null; $status=500;}
		}else{ $data = \DB::select("SELECT id,title from job where title LIKE '$string%' limit 5"); $status = 200;}
		if(isset($data) != null || isset($data) != '[]'){ $status=200; }else{ $data=null; $status=500; }
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################

}
