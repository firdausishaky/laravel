<?php namespace Larasite\Model\Qualification;

use Illuminate\Database\Eloquent\Model;

class EmpSkill_Model extends Model {

protected $table = 'emp_skill';

// READ ###########################################
public function Read_Skill($employee_id,$id){
	if(isset($id)){
		$result = \DB::table('emp_skill')->where('id','=',$id)->get();
		if(isset($result) != null || isset($result) != '[]'){ $data=$result; $status=200; }else{ $data=null; $status=500; }
	}else{
		if(isset($employee_id) && $employee_id != 'Undefined'){
			$result = \DB::SELECT("select emp_skill.id,emp_skill.employee_id,skills.title as level,emp_skill.year_of_exp,comment from emp_skill,skills where emp_skill.employee_id =  '$employee_id' and  emp_skill.skill=skills.id ");
			if(isset($result) != null || isset($result) != '[]'){ $data=$result; $status=200; }else{ $data=null; $status=500; }	
		}else{ $data=null; $status=404; $message='ID Undefined.'; }
		
	}
	return ['data'=>$data,'status'=>$status];
}
// END READ ###########################################


//STORE #################################################
public function Store_Skill($input){
	// check job
		if($this->check_job($input['level']) == 200){
			$Get_Input = $this->SetInput($input);
			
			//;
			$Get_Input['employee_id'] = $input['id'];
			$id = $Get_Input['employee_id']; 
			$level = $Get_Input['skill'];
			$year = $Get_Input['year_of_exp'];
			$comment = $Get_Input['comment'];
			$insert = \DB::SELECT("insert into emp_skill(skill,year_of_exp,comment,employee_id,created_at) values ($level,'$year','$comment','$id',NOW()) ");
			//$store = \DB::table('emp_skill')->insert($Get_Input);
			if(isset($store) != null || isset($store) != '[]'){
				$data=$this->GetID($Get_Input['employee_id']); $message='Store Successfully.'; $status=200;
			}else{$data=null; $message='Store Failed !'; $status=500;}	
		}else{ $data=null; $message='Job Undefined !'; $status=500; }
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
// END STORE ############################################

//UPDATE #################################################
public function Update_Skill($input,$id){
	$employee_id = $this->Get_emp($id);
	$Get_Input = $this->SetInput($input);
	//$emp = $this->Get_emp();
	$Get_emp['employee_id'] = $input['employee_id'];
	$update = \DB::table('emp_skill')
				->where('id','=',$id)
				->update($Get_Input);
	return $update;
	}
// END STORE ############################################

// DESTROY DEPENDENT ################################################################
// PARAM ID(INT)
	public function Destroy_Skill($id){
		
			foreach($id as $key){
				 $destroy = \DB::table('emp_skill')->where('id','=',$key)->delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; $status = 500; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################


// GET ID ###############################################
public function Get_emp($id){
		$data = \DB::select("Select employee_id from emp_skill where id = $id limit 1");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID ###########################################


// GET ID EMP ###############################################
public function GetID($id){
		$data = \DB::select("Select id from emp_skill where employee_id = '$id' order by id DESC limit 1");
		foreach ($data as $key) {
			$id_immigration = $key->id;
		}
		return $id_immigration;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
//PARAM ROLE USER
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################

public function SetInput($input){
	return([
		'skill'=>$input['level'],
		'year_of_exp'=>$input['year_of_exp'],
		'comment'=>$input['comment']
		]);
}// end SETINPUT
public function check_job($id){
	$get = \DB::table('skills')->where('id','=',$id)->get(['title']);
	if(isset($get) != null | isset($get) != '[]'){
		$status=200; $data=$get;
	}else{ $status=500; $data=null; }
	return $status; // response status
}//

// SEARCH JOB #######################################
	public function Search($string){ 
		$check_lastName = strpos($string," "); 
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));
		
		if($last_name){
			if(isset($string)){
				$start = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select id,title from job where title = '$start' limit 5");
			}
			else{ $data = null; $status=500;}
		}else{ $data = \DB::select("SELECT id,title from job where title LIKE '$string%' limit 5"); $status = 200;}
		if(isset($data) != null || isset($data) != '[]'){ $status=200; }else{ $data=null; $status=500; }
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################


}
