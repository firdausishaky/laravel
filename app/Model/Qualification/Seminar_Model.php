<?php namespace Larasite\Model\Qualification;

use Illuminate\Database\Eloquent\Model;

class Seminar_Model extends Model {

// FIX ALL
 protected $table = 'emp_seminar';
 protected $fill = ['id','name','date','location','filename'];

 public function Read_Seminar($employee_id,$id){
 	if(isset($id)){
 		$result = \DB::table($this->table)->where('id','=',$id)->get($this->fill);
 		if($result){ $message = 'Read Data.'; $status=200; $data=$result;}
 		else{ $message = 'Data not found.'; $status=404; $data=null; }
 	}elseif(isset($employee_id)){ 
 		$result = \DB::table($this->table)->where('employee_id','=',$employee_id)->get($this->fill);
 		if($result){ $message = 'Read Data.'; $status=200; $data=$result;}
 		else{ $message = 'Data not found.'; $status=404; $data=null; }
 	}
 	else{ $message = 'ID not found.'; $status=500; $data=null; }
 	return ['message'=>$message,'status'=>$status,'data'=>$data];
 }


// STORE #####
	public function Store_Seminar($id,$input){
		if(isset($input)){
			$get_input = $this->set_input($input);
			$get_input['employee_id'] = $id;

			if(\DB::table($this->table)->insert($get_input)){ $message = 'Store Successfully.'; $status=200;}
			else{ $message = 'Store Failed.'; $status=500;}
		}else{ $message = 'Input null.'; $status=500;}
		
		return ['message'=>$message,'status'=>$status];
	}
// END STORE ###

// UPDATE #########
	public function Update_seminar($id,$input){
		$update = \DB::table($this->table)->where('id','=',$id)->update($input);
		if($update){
			$message='Update Successfully.'; $status=200;
		}
		else{
			$message='Update Failed.'; $status=500;
		}
		return ['message'=>$message,'status'=>$status];
	}
// END UPDATE #######


// CHECK ID ###############################################################
// PARAM ID
	public function check_file_id($id){
		$r = \DB::select("SELECT filename from emp_seminar where id = '$id' ");
		if(!$r){ return null; }
		foreach ($r as $key) {
			$data = $key->filename;
		}
		return $data;
	}
// END CHECK ID ###########################################################


// CHECK ID ###############################################################
// PARAM ID
	public function check_file_id2($id){
		$r = \DB::select("SELECT filename from emp_seminar where id = '$id'");
		if(!$r){ return null; }
		foreach ($r as $key) {
			$data = $key->filename;
		}
		return $data;
	}
// END CHECK ID ###########################################################

 // GET ID ###############################################
public function Get_emp($id){
		$data = \DB::select("Select employee_id from emp_seminar where id = $id");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID ###########################################


// GET ID EMP ###############################################
public function GetID($file){
		//$id = array();
		if(isset($file) && $file != null){
			$data =  \DB::select("SELECT id from emp_seminar order by id desc limit 1");
			foreach ($data as $key) {
				$id = $key->id;
			}	
		}
		$data =  \DB::table('emp_seminar')->where('filename','=',$file)->get(['id']);
		foreach ($data as $key) {
			$id = $key->id;
		}
		return $id;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
//PARAM ROLE USER
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################

// GET TYPE ID ##################################################
//PARAM ROLE USER
public function set_input($input){
	return([
		'name'=>$input['name'],
		'location'=>$input['location'],
		'date'=>$input['date']
		]);
}
// END GET TYPE ################################################


// DESTROY DEPENDENT ################################################################
// PARAM ID(INT)
	public function Destroy_Seminar($id){		
			foreach($id as $key){
				if($this->check_file_id2($key) != null){
					$move = $this->move_file($this->check_file_id2($key));
					$destroy = \DB::table('emp_seminar')->where('id','=',$key)->delete();	
				}else{
					$destroy = \DB::table('emp_seminar')->where('id','=',$key)->delete();	
				}
				
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################

// MOVE FILE IN TRASH ########################################################################
	public function move_file($filename){
		$get_data = \DB::select("SELECT `filename`, `path` from emp_seminar where filename = '$filename' ");
		foreach ($get_data as $key) {
			$data['path'] = $key->path;
			$data['filename'] = $key->filename;
		}
		if(\Storage::disk('local')->exists($data['path']."/".$data['filename'])){
			$data = array_map('unlink',glob(storage_path(($data['path']."/".$data['filename']))));
			if($data){
					$message = 'Update Successfully.';
					$status = 200;

			}else{
					$message = 'Failed to update data.';
					$status = 200;
			}
			
		}
		else{
			$message = 'File tidak ditemukan, atau file telah dihapus check folder trash';
			$status = 404;
		}
		return ['message'=>$message,'status'=>$status];
	}
// END MOVE FILE #################################################################################
//CHECK IMAGE ALREADY EXIST OR NOT
	public function chk_img_data($id){
		$db = \DB::SELECT("select * from emp_seminar where id =$id");
		if($db != null){
			$path = $db[0]->filename;
			$filename = $db[0]->path;
			if($path != null && $filename != null){
				 array_map('unlink',glob(storage_path(($path."/".$filename))));
			}
		}
	}
}
