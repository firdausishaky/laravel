<?php namespace Larasite\Model\Qualification;

use Illuminate\Database\Eloquent\Model;

class WorkExp_Model extends Model {

protected $table = 'emp_work_exp';

// READ ###########################################
public function Read_WorkExp($employee_id,$id){
	if(isset($id)){
		$result = \DB::table('emp_work_exp')->where('id','=',$id)->get();
		if(isset($result) != null || isset($result) != '[]'){ $data=$result; $status=200; }else{ $data=null; $status=500; }
	}else{
		$result = \DB::table('emp_work_exp')->where('employee_id','=',$employee_id)->get();
		if(isset($result) != null || isset($result) != '[]'){ $data=$result; $status=200; }else{ $data=null; $status=500; }
	}
	return ['data'=>$result,'status'=>$status];
}
// END READ ###########################################


//STORE #################################################
public function Store_WorkExp($input){
	// check job
		if($this->check_job($input['job']) == 200){
			$Get_Input = $this->SetInput($input);
			//;
			$Get_Input['employee_id'] = $input['id'];
			//return $Get_Input;
			$store = \DB::table('emp_work_exp')->insert($Get_Input);
			if(isset($store) != null || isset($store) != '[]'){
				$data['length_of_service'] = $Get_Input['length_of_service'];
				$data['id']=$this->GetID($Get_Input['employee_id']); $message='Store Successfully.'; $status=200;
			}else{$data=null; $message='Store Failed !'; $status=500;}	
		}else{ $data=null; $message='Job Undefined !'; $status=500; }
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
// END STORE ############################################

//UPDATE #################################################
public function Update_WorkExp($input,$id){
	$employee_id = $this->Get_emp($id);
	$Get_Input = $this->SetInput($input);
	//$emp = $this->Get_emp();
	$Get_emp['employee_id'] = $input['employee_id'];
	//return $Get_Input;
	$update = \DB::table('emp_work_exp')
				->where('id','=',$id)
				->update($Get_Input);
	if($update){
		//$response = $Get_Input['length_of_service']; 
		$response = \DB::SELECT("select * from emp_work_exp where  id = $id");
	}else{ $response = null; }
		return $response;
	}
// END STORE ############################################

// DESTROY DEPENDENT ################################################################
// PARAM ID(INT)
	public function Destroy_WorkExp($id){
		
			foreach($id as $key){
				 $destroy = \DB::table('emp_work_exp')->where('id','=',$key)->delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################


// GET ID ###############################################
public function Get_emp($id){
		$data = \DB::select("Select employee_id from emp_work_exp where id = $id limit 1");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID ###########################################


// GET ID EMP ###############################################
public function GetID($id){
		$data = \DB::select("Select id from emp_work_exp where employee_id = '$id' order by id DESC limit 1");
		foreach ($data as $key) {
			$id_immigration = $key->id;
		}
		return $id_immigration;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
//PARAM ROLE USER
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################

public function SetInput($input){
	return([
		'company'=>$input['company'],
		'location'=>$input['location'],
		'job'=>$input['job'],
		'from_date'=>$input['from_date'],
		'to_date'=>$input['to_date'],
		'length_of_service'=>$input['length_of_service'],
		'reason_of_leaving'=>$input['reason_of_leaving']
		]);
}// end SETINPUT
public function check_job($id){
	$get = \DB::table('job')->where('id','=',$id)->get(['title']);
	if(isset($get) != null | isset($get) != '[]'){
		$status=200; $data=$get;
	}else{ $status=500; $data=null; }
	return $status; // response status
}//

// SEARCH JOB #######################################
	public function Search($string){ 
		$check_lastName = strpos($string," "); 
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));
		
		if($last_name){
			if(isset($string)){
				$start = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select id,title from job where title = '$start' limit 5");
			}
			else{ $data = null; $status=500;}
		}else{ $data = \DB::select("SELECT id,title from job where title LIKE '$string%' limit 5"); $status = 200;}
		if(isset($data) != null || isset($data) != '[]'){ $status=200; }else{ $data=null; $status=500; }
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################

}
