<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class Qualification_Model extends Model {

// FORM SKILL #######################################################
	// FIX**
	public function ReadSkill($type,$id){
		if($type == 1){
			$data = \DB::select("select id , title , descript
					from skills");
			if(!$data){
				$data = null;
			}
		}
		elseif($type == 2){
			$data = \DB::select("select id , title, descript 
					from skills where id = '$id' ");	
			if(!$data){
				$data = null;
			}

		}
		return $data;
	
	}
	// FIX**
	public function UpdateSkill($id,$title,$descript){
		$update = \DB::table('skills')->where(['id'=>$id])->update(['title'=>$title,'descript'=>$descript]);
		if($update){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg; 
	}
	// FIX**
	public function StoreSkill($title,$descript){
		$title = mysql_real_escape_string($title);
		$descript = mysql_real_escape_string($descript);
		$store = \DB::table('skills')->insert(['title'=>$title,'descript'=>$descript]);
		if($store){
			$msg = 'success';
		}
		else{
			$msg = null;
		}
		return $msg;
	}
	// FIX**
	public function DestroySkill($id){
		$i=0;
		foreach($id as $key){
				$C_title = \DB::table('skills')->where('id','=',$key)->get(['title']);
				foreach ($C_title as $check_lang) {
				 	$check = \DB::table('view_emp_skill')->where('level','=',$check_lang->title)->get(['level']);
					$title = $check_lang->title;
				}
				
				if($check){ $msg[$i] = $title.' Data can not be deleted.'; }
				else{ 
					$destroy = \DB::table('skills')->where('id','=',$key)->delete();
					$msg[$i] = $title.' Delete Successfully.';
				}
				$i++;
		}
		$msgs = null;
		foreach ($msg as $key) {
			$msgs .= $key." \n ";
		}
		return ['message'=>$msgs,'status'=>200];
	}

	// END FORM SKILL


// FORM EDUCATION #######################################################
	// FIX**
	public function ReadEdu($type,$id){
		if($type == 1){
			$data = \DB::select("select id , title 
					from education");
			if(!$data){
				$data = null;
			}
		}
		elseif($type == 2){
			$data = \DB::select("select id , title
					from education where id = '$id' ");	
			if(!$data){
				$data = null;
			}
		}
		return $data;
	
	}
	// FIX**
	public function UpdateEdu($id,$input){
		return $data = $id;
	}
	// FIX**
	public function StoreEdu($input){
		$store = \DB::table('education')->insert($input);
		if($store){ $msg = 200; }
		else{ $msg = 500; }
		return $msg;
	}
	// FIX**
	public function DestroyEdu($id){
		$i=0;
		foreach($id as $key){
				$C_title = \DB::table('education')->where('id','=',$key)->get(['title']);
				foreach ($C_title as $check_lang) {
				 	$check = \DB::table('view_emp_education')->where('level','=',$check_lang->title)->get(['level']);
					$title = $check_lang->title;
				}
				
				if($check){ $msg[$i] = $title.' Data can not be deleted.'; }
				else{ 
					$destroy = \DB::table('education')->where('id','=',$key)->delete();
					$msg[$i] = $title.' Delete Successfully.';
				}
				$i++;
		}
		$msgs = null;
		foreach ($msg as $key) {
			$msgs .= $key." \n ";
		}
		return ['message'=>$msgs,'status'=>200];
	}

	// END FORM EDUCATION

// FORM LANGUAGE #######################################################
	// FIX**
	public function ReadLang($type,$id){
		if($type == 1){
			$data = \DB::select("select id , title
					from language");
			if(!$data){
				$data = null;
			}
		}
		elseif($type == 2){
			$data = \DB::select("select id , title
					from language where id = '$id' ");	
			if(!$data){
				$data = null;
			}

		}
		return $data;
	
	}
	// FIX**
	public function UpdateLang($id,$input){
		$update = \DB::table('language')->where(['id'=>$id])->update($input);
		if($update){
			$status = 200; }
		else{ $status = 500; }
		return $status; 
	}
	// FIX**
	public function StoreLang($input){
		$store = \DB::table('language')->insert($input);
		if($store){	$status = 200; }
		else{ $status = 500; }
		return $status;
	}
	// FIX**
	public function DestroyLang($id){
		$i=0;
		foreach($id as $key){
				$C_title = \DB::table('language')->where('id','=',$key)->get(['title']);
				foreach ($C_title as $check_lang) {
				 	$check = \DB::table('view_emp_lang')->where('language','=',$check_lang->title)->get(['language']);
					$title = $check_lang->title;
				}
				
				if($check){ $msg[$i] = $title.' Data can not be deleted.'; }
				else{ 
					$destroy = \DB::table('language')->where('id','=',$key)->delete();
					$msg[$i] = $title.' Delete Successfully.';
				}
				$i++;
		}
		$msgs = null;
		foreach ($msg as $key) {
			$msgs .= $key." \n ";
		}
		return ['message'=>$msgs,'status'=>200];
	}

	// END FORM LANGUAGE


}
