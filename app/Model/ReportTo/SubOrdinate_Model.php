<?php namespace Larasite\Model\ReportTo;

use Illuminate\Database\Eloquent\Model;

class SubOrdinate_Model extends Model {


	protected $table = 'emp_subordinate';
	protected $fill = ['id','employee_id','subordinate'];
	protected $fill2 = ['id','subordinate'];

// READ ###########################################

private function check($id)
{
	$get = \DB::table($this->table)->where('supervisor','=',$id)->get();
}

public function Read_Sub_Unit($employee_id,$id){
	
	if(isset($id)){
		$result = \DB::table('view_report_subordinate')->where('employee_id','=',$id)->get($this->fill2);
		if(isset($result) && $result != null){ $data=$result; $status=200; }else{ $data=null; $status=200; }
	}else{
		$result = \DB::table('view_report_subordinate')->where('employee_id','=',$employee_id)->get(['id','subordinate']);
		if(isset($result) && $result != null){ $data=$result; $status=200; }else{ $data=null; $status=200; }
	}
	return ['data'=>$data,'status'=>$status];
}
// END READ ###########################################


//STORE #################################################
public function Store_Sub_Unit($id,$input){
	// check job
		if($this->check_job($input['subordinate']) == 200){
			$Get_Input = $this->SetInput($input);
			$Get_Input['employee_id'] = $id;
			
			$store1 = \DB::table('emp_subordinate')->insert(['employee_id'=>$id,'subordinate'=>$input['subordinate']]);
			$store2 = \DB::table('emp_supervisor')->insert(['employee_id'=>$input['subordinate'],'supervisor'=>$id]);
			if(isset($store) != null || isset($store) != '[]'){
				$data=$this->GetID($Get_Input['employee_id']); $message='Store Successfully.'; $status=200;
			}else{$data=null; $message='Store Failed !'; $status=500;}	
		}else{ $data=null; $message='Job Undefined !'; $status=500;}
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
// END STORE ############################################

//UPDATE #################################################
public function Update_Sub_Unit($input,$id,$data){
	$employee_id = $this->Get_emp($id['id']);
	$Get_Input = $this->SetInput($input);
	//$emp = $this->Get_emp();
	$Get_Input['employee_id'] = $employee_id;
	$update = \DB::table('emp_subordinate')
				->where('id','=',$id['id'])
				->update($Get_Input);
	if($update){
		$data = \DB::SELECT("select * from view_report_subordinate where id=$data ");
		if(isset($data)){ return $update = ["message" => "success", "status" => 200, "data" => $data]; }else{ ["message" => "failed", "status" => 500, "data" => [] ];}
	}
	
	}
// END STORE ############################################

// DESTROY DEPENDENT ################################################################
// PARAM ID(INT)
	public function Destroy_Sub_Unit($id){
		
			foreach($id as $key){
				 $destroy = \DB::table('emp_subordinate')->where('id','=',$key)->delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################


// GET ID ###############################################
public function Get_emp($id){
		$data = \DB::select("Select employee_id from emp_subordinate where id = $id limit 1");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID ###########################################


// GET ID EMP ###############################################
public function GetID($id){
		$data = \DB::select("Select id from emp_subordinate where employee_id = '$id' order by id DESC limit 1");
		foreach ($data as $key) {
			$id_immigration = $key->id;
		}
		return $id_immigration;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
//PARAM ROLE USER
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = '$employee_id' ");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################

// set input
public function SetInput($input){
	return([
		'subordinate'=>$input
		]);
}// end SETINPUT

// CHECK SUPERVISOR
public function check_job($id){
	$get = \DB::table('emp')->where('employee_id','=',$id)->get(['employee_id']);
	if(isset($get) != null | isset($get) != '[]'){
		$status=200; $data=$get;
	}else{ $status=500; $data=null; }
	return $status; // response status
}//

// SEARCH JOB #######################################
	public function Search($id,$string){ 
		$check_lastName = strpos($string," "); 
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));
		$check = \DB::table('supervisor')->get(['employee_id']);
		if($last_name){
			if(isset($string)){
				$first_name = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select employee_id, concat(first_name,' ',middle_name,' ',last_name) as name 
						from emp where first_name = '$first_name' and middle_name LIKE '$last_name%' and employee_id != '$id' limit 5");
			}else{ return ['data'=>null,'status'=>500]; }
		}else{
			$data = \DB::select("SELECT employee_id, concat(first_name,' ',middle_name,' ',last_name) as name 
						from emp where first_name LIKE '$string%' limit 10");
			$status=200;
		}
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################


}
