<?php namespace Larasite\Model\ReportTo;

use Illuminate\Database\Eloquent\Model;

class Supervisor_Model extends Model {

	protected $table = 'emp_supervisor';
	protected $fill = ['id','employee_id','supervisor'];
	protected $fill2 = ['id','supervisor'];

// READ ###########################################
public function Read_Supervisor($employee_id,$id){
	
	if(isset($id)){
		$result = \DB::table('view_report_supervisor')->where('id','=',$id)->get($this->fill2);
		if(isset($result) != '[]'){ $data=$result; $status=200; }else{ $data=null; $status=500; }
	}else{
		$result = \DB::table('view_report_supervisor')->where('employee','=',$employee_id)->get(['id','supervisor']);
		if(isset($result) != null || isset($result) != '[]'){ $data=$result; $status=200; }else{ $data=null; $status=500; }
	}
	return ['data'=>$data,'status'=>$status];
}
// END READ ###########################################


//STORE #################################################
public function Store_Supervisor($id,$input){
	// check job
		if($this->check_job($input['supervisor']) == 200){
			$Get_Input = $this->SetInput($input);
			$Get_Input['employee_id'] = $id;
			$store1 = \DB::table('emp_supervisor')->insert($Get_Input);
			$supervisor = $input['supervisor'];
			//$subordinate = $id;
			$store2 = \DB::select("insert into emp_subordinate(employee_id,subordinate) values('$supervisor','$id')");
			//$store2 = \DB::table('emp_subordinate')->insert(['employee_id'=>$input['supervisor'],'subordinate'=>$id]);
			if(isset($store1,$store2) != null || isset($store1,$store2) != '[]'){
				$data=$this->GetID($Get_Input['employee_id']); $message='Store Successfully.'; $status=200;
			}else{$data=null; $message='Store Failed !'; $status=500;}	
		}else{ $data=null; $message='Job Undefined !'; $status=500; }
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
// END STORE ############################################

//UPDATE #################################################
public function Update_Supervisor($input,$id){
	$employee_id = $this->Get_emp($id);
	$Get_Input = $this->SetInput($input);
	//$emp = $this->Get_emp();
	$Get_Input['employee_id'] = $employee_id;
	$update = \DB::table('emp_supervisor')
				->where('id','=',$id)
				->update($Get_Input);
	return $update;
	}
// END STORE ############################################

// DESTROY DEPENDENT ################################################################
// PARAM ID(INT)
	public function Destroy_Supervisor($id,$supervisor){
		
			foreach($id as $key){
				 $destroy1 = \DB::select("delete from emp_supervisor where supervisor='$supervisor' ");
				 if($key != null){
				 	$destroy2 = \DB::select("delete from emp_subordinate where id=$key ");
				}
			}
			if($destroy1 && $destroy2){
				$msg = 'Delete Successfully.'; $status=200;
			}elseif($destroy1){
				$msg = 'Delete Successfully.'; $status=200;
			}else{$msg =  "Failed to delete data"; $status=500; }	
		return ['message'=>$msg,'status'=>$status];
	}

	// destroy only supervisor 
	public function Destroy_SupervisorOnly($supervisor){
		
			foreach($supervisor as $key){
				 $destroy1 = \DB::table('emp_supervisor')->where('supervisor','=',$key)->delete();
			}
			if($destroy1){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = "Error when delete data"; $status=200; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################


// GET ID ###############################################
public function Get_emp($id){
		
		$data = \DB::select("Select employee_id from emp_supervisor where  = $id limit 1");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID ###########################################


// GET ID EMP ###############################################
public function GetID($id){
		$data = \DB::select("Select emp_supervisor.id, concat(first_name,' ',middle_name,' ',last_name) as employee_name from emp,emp_supervisor where  emp.employee_id = '$id' and emp_supervisor.employee_id='$id' ");
		foreach ($data as $key) {
			$id_immigration = ["id" =>$key->id, "employee_name" => $key->employee_name];
		}
		return $id_immigration;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
//PARAM ROLE USER
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = '$employee_id' ");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################

// set input
public function SetInput($input){
	return([
		'supervisor'=>$input['supervisor']
		]);
}// end SETINPUT

// CHECK SUPERVISOR
public function check_job($id){
	$get = \DB::table('emp')->where('employee_id','=',$id)->get(['employee_id']);
	if(isset($get) != null | isset($get) != '[]'){
		$status=200; $data=$get;
	}else{ $status=500; $data=null; }
	return $status; // response status
}//

// SEARCH JOB #######################################
	public function Search($string){ 
		$check_lastName = strpos($string," "); 
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));

		if($last_name){
			if(isset($string)){
				$first_name = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select employee_id, concat(first_name,' ',middle_name,' ',last_name) as name 
						from emp where first_name = '$first_name%' and middle_name LIKE '$last_name%' limit 5");
			}else{ return ['data'=>null,'status'=>500]; }
		}else{
			$data = \DB::select("SELECT employee_id, concat(first_name,' ',middle_name,' ',last_name) as name 
						from emp where first_name LIKE '$string%' limit 10");
			$status=200;
		}
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################


}
