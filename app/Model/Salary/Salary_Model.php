<?php namespace Larasite\Model\Salary;

use Illuminate\Database\Eloquent\Model;

class Salary_Model extends Model {

  protected $tables = 'manage_salary';

  // READ ###########################################
public function Read_Salary($id){
	if(isset($id) && $id != 'undefined'){
		$result = \DB::table('view_manage_salary')->where('employee_id','=',$id)->get();
		if(isset($result) && $result){ 
			$current_data=$result[0]; 
			
			$result2 = \DB::select("select * from log_salary where employee_id = '$id' order by id desc ");
			if($result2){ 
				$log_data = $result2;
			}else{$log_data = null;}
			$status=200; $message='Salary : Show Records Data.';
		}else{ $current_data=null;$log_data=null; $status=200; $message='Salary : Empty Records Data.'; }

	}else{ $message='Data not found.'; $status=404; $log_data=null; $current_data=null; }
	return ['data'=>['current_data'=>$current_data,'log_data'=>$log_data],'status'=>$status,'message'=>$message];
}
// END READ ###########################################


//STORE #################################################
public function Store_Salary($id,$input){
	// check job
		if(isset($input) && $input != '[]'){
			$Get_Input = $this->set_input($input);
			$Get_Input['employee_id'] = $id;
			
			$store = \DB::table('manage_salary')->insert($Get_Input);
			if(isset($store) != null || isset($store) != '[]'){
				$data['id']=$this->GetID($Get_Input['atm_numb']); $message='Store Successfully.'; $status=200;
				$data['date'] = $this->GetDATE($data['id']);
			}else{$data=null; $message='Store Failed !'; $status=500;}

		}else{ $data=null; $dates=null; $message='Employee ID undefined !'; $status=500; }
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
// END STORE ############################################

//STORE #################################################
public function Store2_Salary($id,$input){
	// check job
		if(isset($input) && $input != '[]'){
			$Get_Input = $this->set_input($input);
			
			$store = \DB::table('manage_salary')->where('id','=',$id)->update($Get_Input);
			if(isset($store) != null || isset($store) != '[]'){
				//$data['date'] = $this->GetDATE($id);
				$data['id']=$id; $message='Store Successfully.'; $status=200;
				$data['date'] = $this->GetDATE($data['id']);
			}else{$data=null; $message='Store Failed !'; $status=500;}

		}else{ $data=null; $message='Employee ID undefined !'; $status=500; }
		return ['message'=>$message,'data'=>$data,'status'=>$status];
	}
// END STORE ############################################

//UPDATE #################################################
public function Update_Salary($id,$input){
	//$employee_id = $this->Get_emp($id);
	$Get_Input = $this->set_input($input);
	$check = \DB::table('manage_salary')->where('id','=',$id)->get();
	
	if(isset($check) && $check != '[]'){
		$update = \DB::table('manage_salary')->where('id','=',$id)->update($Get_Input);
		if($update){ $message='Updated Successfully.'; $status=200;}
		else{ $message='Updated Failed, Error code : '.$update; $status=500; }
	}else{  $message='ID not found, Error code : '.$check; $status=500; }
	return ['message'=>$message,'status'=>$status];
	}
// END STORE ############################################

// DESTROY DEPENDENT ################################################################
// PARAM ID(INT)
	public function Destroy_salary($id){
		
			foreach($id as $key){
				//$this->move_file($filename);
				$destroy = \DB::table('manage_salary')->where('id','=',$key)->delete();
			}
			if($destroy){$msg = 'Delete Successfully.'; $status=200;}
			else{$msg = null; }	
		return ['message'=>$msg,'status'=>$status];
	}
// END DESTROY ###############################################################


// GET ID #######################################################
public function Get_emp($id){
		$data = \DB::select("Select employee_id from manage_salary where id = $id");
		foreach ($data as $key) {
			$employee_id = $key->employee_id;
		}
		return $employee_id;
	}
// END GET ID ###########################################


// GET ID EMP ###############################################
public function GetID($atm_numb){
		$data = \DB::select("Select id from manage_salary where atm_numb = '$atm_numb' order by id DESC limit 1");
		foreach ($data as $key) {
			$id_immigration = $key->id;
		}
		return $id_immigration;
	}
public function GetDATE($employee_id){
		$data = \DB::select("Select dates from manage_salary where id = '$employee_id' order by id DESC limit 1");
		foreach ($data as $key) {
			$date = $key->dates;
		}
		return $date;
	}
// END GET EMP ###########################################


// GET TYPE ID ##################################################
//PARAM ROLE USER
public function Get_Type($role,$employee_id){
		$data = \DB::select("select c.local_it, c.employee_id 
							FROM ldap b, emp c 
							WHERE b.role_id = '$role' and c.employee_id = $employee_id");
		if(!isset($data)){
			$data = null;
		}
		return $data;
	}
// END GET TYPE ################################################

public function set_input($input){
	return([
		'salary'=>$input['salary'],
		'net_gross'=>$input['net_gross'],
		'atm_numb'=>$input['atm_numb'],
		'atm_account'=>$input['atm_account'],
		'hmo'=>$input['hmo'],
		'hdmf_loan'=>$input['hdmf_loan'],
		'sss_loan'=>$input['sss_loan']
		]);
}// end set_input

public function set_inputDataEmp($input){
	return([
		'salary'=>$input['salary'],
		'net_gross'=>$input['net_gross'],
		'atm_numb'=>$input['atm_numb'],
		'atm_account'=>$input['atm_account'],
		'hmo'=>$input['hmo'],
		'hdmf_loan'=>$input['hdmf_loan'],
		'sss_loan'=>$input['sss_loan']
		]);
}// end set_input
public function check_job($id){
	$get = \DB::table('language')->where('id','=',$id)->get(['title']);
	if(isset($get) != null | isset($get) != '[]'){
		$status=200; $data=$get;
	}else{ $status=500; $data=null; }
	return $status; // response status
}//

// SEARCH JOB #######################################
	public function Search($string){ 
		$check_lastName = strpos($string," "); 
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));
		
		if($last_name){
			if(isset($string)){
				$start = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select id,title from job where title = '$start' limit 5");
			}
			else{ $data = null; $status=500;}
		}else{ $data = \DB::select("SELECT id,title from job where title LIKE '$string%' limit 5"); $status = 200;}
		if(isset($data) != null || isset($data) != '[]'){ $status=200; }else{ $data=null; $status=500; }
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################

// SAVE LOG DATA ####
public function ambil_data_terbaru($id){
	$data = \DB::select("SELECT dates, salary, atm_numb, atm_account, net_gross,hmo,hdmf_loan,sss_loan from log_salary where employee_id = $id order by id desc ");
	return $data;
}

}
