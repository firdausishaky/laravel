<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class SystemUser_Model extends Model {

protected $table = 'ldap';
protected $fill = ['employee_id','role_id','active_access'];
protected $fill2 = ['id','name','domain_name','role_name','status'];

// READ ############################
	// FIX**
	public function Read_SystemUser($type,$id){
		if($type == 1){
			$result = \DB::table('view_system_user')->get($this->fill2);
			if(isset($result) != '[]' || isset($result) != null){
				$status=200; $data=$result;
			}else{ $status=500; $data=null; }
		}
		elseif($type == 2){
			$result = \DB::table('view_system_user')->where('id','=',$id)->get($this->fill2);
			if(isset($result) != '[]' || isset($result) != null){
				$status=200; $data=$result;
			}else{ $status=500; $data=null;}
		}
		return ['status'=>$status,'data'=>$data];
	
	}
// END READ ####################

// UPDATE ######################
	// FIX**
	public function Update_SystemUser($id,$inputx){
		//$input = $this->SetInput($inputx);
		//$input['role_id'] = $this->Get_RoleID($inputx['role_name']);
		$update = \DB::table($this->table)->where(['id_ldap'=>$id])->update($inputx);
		if($update){ $status=200;}
		else{ $status=500;}
		return $status; 
	}
// END UPDATE ##################

// STORE #######################
	// FIX**
	public function Store_SystemUser($inputx){
		//$input = $this->SetInput($inputx);
		//$input['role_id'] = $id;
		
		//$input['role_id'] = $this->Get_RoleID($inputx['role_name']);
		$store = \DB::table($this->table)->insert($inputx);
		if($store){ $status=200;}
		else{ $status=500; }
		return $status;
	}
// END STORE ###################
// DESTROY #####################
	// FIX**
	public function Destroy_SystemUser($id){
		foreach($id as $key){
		 $destroy =  \DB::table($this->table)->where('id_ldap','=',$key)->delete();
		}
		if($destroy){ $status=200; }
		else{ $status=500;}
		return $status;
	}
// END DESTROY #####################
// SET INPUT #######################
	public function SetInput($input){
		return(['employee_id'=>$input['employee_id'],'active_access'=>$input['status']]);
	}
// END SET INPUT ####################

// SEARCH DOMAIN NAME 
	public function Search_Domain($string){ 
		//$check_lastName = strpos($string," ")s; 
		//$last_name = substr($string, $check_lastName+1, strpos($string,' '));
		// $data = \DB::select("SELECT employee_id, ad_username as domain from emp where first_name LIKE '$string%' limit 10");
		// return $data;
		$check_lastName = strpos($string," "); 
		$last_name = substr($string, $check_lastName+1, strpos($string,' '));
		
		if($last_name){
			if(isset($string)){
				$first_name = substr($string, 0, strpos($string,' ')); $last = $last_name;
				$data = \DB::select("select ad_username as domain_name 
						from emp where first_name = '$first_name' and middle_name LIKE '$last_name%' limit 5");
			}
			else{ $data = null; $status=500;}
		}else{ $data = \DB::select("SELECT ad_username as domain from emp where first_name LIKE '$string%' limit 5"); $status = 200;}
		if(isset($data) != null || isset($data) != '[]'){ $status=200; }else{ $data=null; $status=500; }
		return ['data'=>$data,'status'=>$status];
	}
// END SEARCH DOMAIN NAME ###################################

// NEW GET_ID ####################
	public function Get_ID($id){
		$getid = array();
		$result = \DB::select("Select id_ldap from ldap where employee_id = $id order by id_ldap desc limit 1");
		foreach ($result as $key) {
			$getid = $key->id_ldap;
		}
		return $getid;
	}
// END NEW GETID #################


// GET EMP ######################
	public function Get_Emp($ad_username){
		$result = \DB::select("Select employee_id from emp where ad_username = '$ad_username'");
		$getemp = array();
		foreach ($result as $key) {
			$getemp = $key->employee_id;
		}
		return $getemp;
	}
// END GET EMP ##################
	// GET Name ######################
	public function Get_Name($ad_username){
		$result = \DB::select("Select concat(first_name,IFNULL('',middle_name),' ',last_name) as username from emp where ad_username = '$ad_username'");
		$getemp = array();
		foreach ($result as $key) {
			$getemp = $key->username;
		}
		return $getemp;
	}
// END GET Name##################

// GET EMP ######################
	public function Get_RoleName($id){
		$result = \DB::select("Select role_name from role where role_id = $id");
		$getemp = array();
		foreach ($result as $key) {
			$getemp = $key->role_name;
		}
		return $getemp;
	}
	public function Get_RoleID($id){
		$result = \DB::select("Select role_id from role where role_name = '$id'");
		$getemp = array();
		foreach ($result as $key) {
			$getemp = $key->role_id;
		}
		return $getemp;
	}
// END GET EMP ##################
}











// // SEARCH DOMAIN NAME #######################################
// 	public function Search($input){
// 		$string = "j"; 
// 		$check_lastName = strpos($string," "); 
// 		$last_name = substr($string, $check_lastName+1, strpos($string,' '));
		
// 		if($last_name){
// 			if(isset($string)){
// 				$first_name = substr($string, 0, strpos($string,' ')); $last = $last_name;
// 				$data = \DB::select("select employee_id, concat(first_name,' ',middle_name,' ',last_name) as name 
// 						from emp where first_name = '$first_name' and middle_name LIKE '$last_name%' limit 5");
// 			}
// 			else{ $data = "input kosong"; }
// 		}else{
// 			$data = \DB::select("SELECT employee_id, concat(first_name,' ',middle_name,' ',last_name) as name 
// 						from emp where first_name LIKE '$string%' limit 10");
// 			if()
// 		}
// 		return ['data'=>$data,'status'=>$status];
// 	}
// 	// END SEARCH DOMAIN NAME ###################################
