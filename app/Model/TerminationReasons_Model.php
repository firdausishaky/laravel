<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;

class TerminationReasons_Model extends Model {
	
	Protected $tables = 'termination_reasons';
	Protected $fields = ['title'];
	Protected $hiddden = ['id'];


	// FORM TerminationReasons #######################################################
	// FIX**
	public function ReadTerminationReasons($type,$id){
		if($type == 'All'){
			$data = \DB::select("select id , title
					from ".$this->tables." ");
			if(!$data){
				$data = null;
			}
		}
		elseif($type == 'Sort'){
			$data = \DB::select("select id , title 
					from ".$this->tables." where id = '$id' ");	
			if(!$data){
				$data = null;
			}

		}
		return $data;
	
	}
	// FIX**
	public function UpdateTerminationReasons($id,$title){
		$update = \DB::table($this->tables)->where(['id'=>$id])->update(['title'=>$title]);
		if($update){
			$msg = 200;
		}
		else{
			$msg = 500;
		}
		return $msg; 
	}
	// FIX**
	public function StoreTerminationReasons($title){
		$store = \DB::table($this->tables)->insert(['title'=>$title]);
		if($store){
			$getid = \DB::select("Select * from termination_reasons where title = '$title' limit 1");
			$msg = $getid[0];
		}
		else{
			$msg = null;
		}
		return $msg;
	}
	// FIX**
	public function DestroyTerminationReasons($id){
		$i=0;
		foreach($id as $key){
				$C_title = \DB::table('termination_reasons')->where('id','=',$key)->get(['title']);
				foreach ($C_title as $check_lang) {
				 	$check = \DB::table('emp_termination_reason')->where('termination','=',$check_lang->title)->get(['termination']);
					$title = $check_lang->title;
				}
				
				if($check){ $msg[$i] = $title.' Data can not be deleted.'; }
				else{ 
					$destroy = \DB::table('termination_reasons')->where('id','=',$key)->delete();
					$msg[$i] = $title.' Delete Successfully.';
				}
				$i++;
		}
		$msgs = null;
		foreach ($msg as $key) {
			$msgs .= $key." \n ";
		}
		return ['message'=>$msgs,'status'=>200];
	}

	// END FORM TerminationReasons
}
