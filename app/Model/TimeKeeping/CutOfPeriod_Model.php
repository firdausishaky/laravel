<?php namespace Larasite\Model\TimeKeeping;

use Illuminate\Database\Eloquent\Model;

class CutOfPeriod_Model extends Model {
	// define var
	protected $table = 'att_cutofperiod'; /*/*/ protected $fields = ['shift_code','from','to','hour_per_day']; /*/*/ protected $hidden = ['id'];
	
	private function set_input($input){
		return ['shift_code'=>$input['shift_code'],'from'=>$input['from'],'to'=>$input['to'],'hour_per_day'=>$input['hour_per_day']];
	}
	private function getData($inputan){
		if(isset($inputan['shift_code'])){
			$get = \DB::table($this->table)->where('shift_code','=',$inputan['shift_code'])->get($this->fields);	
		}else{ $get = \DB::table($this->table)->get($this->fields); }
		if($get){ foreach ($get as $input) { $data[] = $input; } }
		else{ $data=null; }
		return $data;
	}
	// CRUD FUNCTION ###
	public function Read($id)
	{	
		$get = $this->getData($id);
		if($get){ $data=['Show Records Data.',200,$get[0]]; }
		else{ $data=['Empty Records Data.',404,null]; }
		return $data;
	}
	public function Store($input)
	{	
		$store = \DB::table($this->table)->insert($this->set_input($input));
		if($store){ $data=['Store Successfully.',200,$this->getData($input)]; }
		else{ $data=['Store Failed.',500,null]; }
		return $data;
	}
	public function Destroy($id)
	{
		foreach($id as $key){ return $key; $destroy[] = \DB::table('att_cutofperiod')->where('id','=',$key)->get(); }
		if($destroy){ $msg = $destroy; $status=200;}
		else{ $msg = null; $status=404; }	
		return ['message'=>$msg,'status'=>$status];
	}
}	
