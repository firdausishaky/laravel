<?php namespace Larasite\Model\TimeKeeping;

use Illuminate\Database\Eloquent\Model;

class HolidaySetup_Model extends Model {

	// HRMS.emp_Holiday
	protected $table = 'attendance_holiday';
	protected $hidden = ['created_at','updated_at'];
	protected $guarded = ['id'];
	protected $fill = ['id','name','repeat_annually','type','date','month','day'];

	protected $typeCall = ['update'=>"CALL Update_Holiday",
							'store'=>"CALL Insert_Holiday",
							'view'=>"CALL View_holiday",
							'delete'=>"CALL Delete_Holiday"];

	public function getColumns($type){
		$data = NULL;  $i = 0;
		$get =  \DB::select("show columns from $this->table where Field != 'updated_at' and Field != 'created_at' and Field != 'id'");
		if($type == 'edit'){ $get =  \DB::select("show columns from $this->table where Field != 'updated_at' and Field != 'created_at'");  }

		while( $i < count($get)){
			foreach ($get[$i] as $key => $value) {
				if($key == 'Field'){ $rule[] = $value; }
				elseif($key == 'Type'){ $type[] = $value; }
			}
			$i++;
		}
		return ['rule'=>$rule,'type_data'=>$type];
	}
	public function ManiDat($data){
		$i = 0;
		if($data == '[]' || $data == false){
			$datax = ['Empty records data.',200,NULL];
		}else{
			foreach ($data as $key) {
				if($key->type == 2){
					$data[$i]['type'] = 'special';
				}
				else{
					$data[$i]['type'] = 'reguler';
				}

				if($key->repeat_annually == 0){
					$data[$i]['repeat_annually'] = 'no';
				}
				else{
					$data[$i]['repeat_annually'] = 'yes';
				}
				$i++;
			}
			$datax = ['Show records data.',200,$data];
		}
		return $datax;
	}

	public function CALL($type,$input)
	{
	
		if($type == 'update'){ $procedure = $this->typeCall['update']; $message = 'Update Successfully.'; $status = 200;}
		elseif( $type == 'store' ) { $procedure = $this->typeCall['store']; $message = 'Store Successfully.'; $status = 200;}
		elseif( $type == 'view' ) { $procedure = $this->typeCall['view']; $message = 'Show records data.'; $status = 200;}
		elseif( $type == 'delete' ) { $procedure = $this->typeCall['delete']; $message = 'Delete Successfully.'; $status = 200;}

		$get = \DB::select("$procedure(".$input.")");
		if($get){
			foreach ($get as $key) {
				if(isset($key->Message, $key->Status)){
					$message = $key->Message; $status = 500; $data = NULL;
				}else{ $message = $message; $status = $status; $data = $get;  }
			}
		}else{ $message = 'Empty records data.'; $status = 200; $data = NULL; }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
}
