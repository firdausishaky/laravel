<?php namespace Larasite\Model\TimeKeeping;

use Illuminate\Database\Eloquent\Model;

class WorkShift_Model extends Model {
	// define var
	protected $table = 'work_shifts'; /*/*/ protected $fields = ['shift_code','from','to','hour_per_day']; /*/*/ protected $hidden = ['id'];
	
	private function set_input($input){
		return ['shift_code'=>$input['shift_code'],'from'=>$input['from'],'to'=>$input['to'],'hour_per_day'=>$input['hour_per_day']];
	}
	private function getData($inputan){
		if(isset($inputan['shift_code'])){
			$shift_code = $inputan['shift_code'];
			$get = \DB::table($this->table)->where('shift_code','=',$inputan['shift_code'])->get($this->fields);	
		}else{ $get = \DB::table($this->table)->get($this->fields); }
		if($get){ foreach ($get as $input) { $data[] = $input; } }
		else{ $data=null; }
		return $data;
	}
	// CRUD FUNCTION ###
	public function Read($id)
	{	
		$get = \DB::SELECT("CALL 'View_WorkShift' ");
		if($get){ $data=['Show Records Data.',200,$get]; }
		else{ $data=['Empty Records Data.',404,null]; }
		return $data;
	}
	public function Store($input)
	{	$shift_code = $input['shift_code'];
		$from = $input['from'];
		$to = $input['to'];
		$hour_per_day = $input['hour_per_day'];
		$store = \DB::SELECT("CALL Insert_WorkShift('$shift_code','$from','$to','$hour_per_day')");
		if($store){ $data=['Store Successfully.',200,$this->getData($input)]; }
		else{ $data=['Store Failed.',500,null]; }
		return $data;
	}
	public function Destroy($id)
	{
		$i = 1;
		$del = array();
		foreach ($id as $key) {
			$del = \DB::select("CALL Delete_WorkShift('$key')");
		}
		if(empty($del)){ $msg='Status cannot delete.'; $status=500; }
		else{
			foreach($id as $key){
				 $destroy = \DB::select("CALL Delete_WorkShift('$key')");
			}
			if(!empty($destroy)){ $msg=$destroy[0]->message; $status=200; }
			else{ $msg='Error Code.'; $status=500; }
		}
		return ['message'=>$msg,'status'=>$status];
	}
}	
