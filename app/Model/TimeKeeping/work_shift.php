<?php namespace Larasite\Model\TimeKeeping;

use Illuminate\Database\Eloquent\Model;

class work_shift extends Model {

	protected $table = 'attendance_work_shifts';
	protected $fields = ['id','shift_code','_from','_to','hour_per_day'];
	
	protected $typeCall = ['update'=>"CALL Update_WorkShift",
							'store'=>"CALL Insert_WorkShift",
							'view'=>"CALL View_WorkShift",
							'delete'=>"CALL Delete_WorkShift"];
	
	private function getData($inputan){
		if(isset($inputan['shift_code'])){
			$get = \DB::table($this->table)->where('shift_code','=',$inputan['shift_code'])->get($this->fields);	
		}else{ $get = \DB::table($this->table)->get($this->fields); }
		if($get){ foreach ($get as $input) { $data[] = $input; } }
		else{ $data=null; }
		return $data;
	}

	public function getColumns($type){
		$data = NULL;  $i = 0;
		$get =  \DB::select("show columns from $this->table where Field != 'updated_at' and Field != 'created_at' and Field != 'shift_id'"); 
		if($type == 'edit'){ $get =  \DB::select("show columns from $this->table where Field != 'updated_at' and Field != 'created_at'");  }

		while( $i < count($get)){
			foreach ($get[$i] as $key => $value) {
				if($key == 'Field'){ $rule[] = $value; }
				elseif($key == 'Type'){ $type[] = $value; }
			}
			$i++;
		}
		return $rule;
	}
	
	// CRUD FUNCTION ###

	public function CALL($type,$input)
	{
		if($type == 'update'){ $procedure = $this->typeCall['update']; $message = 'Update Successfully.'; $status = 200;}
		elseif( $type == 'store' ) { $procedure = $this->typeCall['store']; $message = 'Store Successfully.'; $status = 200;}
		elseif( $type == 'view' ) { $procedure = $this->typeCall['view']; $message = 'Show records data.'; $status = 200;}
		elseif( $type == 'delete' ) { $procedure = $this->typeCall['delete']; $message = 'Delete Successfully.'; $status = 200;}

		$get = \DB::select("$procedure(".$input.")");	
		
		if($get){
			foreach ($get as $key) { 
				if(isset($key->Message, $key->Status)){
					$message = $key->Message; $status = 500; $data = NULL;
				}else{ $message = $message; $status = $status; $data = $get;  }
			}
		}else{ $message = 'Empty records data.'; $status = 200; $data = NULL; }
		return ['message'=>$message,'status'=>$status,'data'=>$data];
	}
}	
