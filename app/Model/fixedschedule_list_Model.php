<?php  namespace  Larasite\Model;

use Illuminate\database\eloquent\Model;

class fixedschedule_list_Model{

	public function break_date($data){
		foreach ($data as $key => $value) {
		 	$date = $value->schedule;
		 	$substring  =  substr($date,4,-1);

		 	$data_ex[] = [
		 			"date" => $value->date,
		 			"Last" => $value->last,
		 			"first_name" => $value->first_name,
		 			"department" => $value->department,
		 			"schedule" => $substring

		 		  ];
		}

		return $data_ex;
	}

	public function indexAcess($data = array(),$access = array()){
		if($data == null){
			return $this->getMessageAccess("Data not exist", 200, $data, $access);
		}else if($data != null){
			return $this->getMessageAccess("Success", 200, $data, $access);
		}else{
			return $this->getMessageAccess("Failed to load data", 500, $data, $access);
		}
	}

	public function indexUnauthorized($message = array(),$data = array(),$access = array()){
		if($data == null){
			return $this->getMessageAccess($message, 200, $data, $access);
		}else if($data != null){
			return $this->getMessageAccess($message, 200, $data, $access);
		}else{
			return $this->getMessageAccess($message, 500, $data, $access);
		}
	}

	public function getMessageAccess($message = array(), $status = array(), $data =  array(), $access = array()){
		return response()->json(['header' => ['message' => $message, 'status' => $status, 'access' => $access], 'data' => $data],$status);
	}

	function searchAcess($validator = array(),$data =  array(), $access = array()){
			if($validator->fails()){
				$check = $validator->errors()->all();
				return $this->getMessageAccess('Invalidate input format', 500, $check, $access);
			}else{
				return $this->indexAcess($data,$access);
			}
			return $this->getMessage("input data undefined",500,null,$access); 
		}




}