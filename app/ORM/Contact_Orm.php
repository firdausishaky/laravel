<?php namespace Larasite\ORM;

use Illuminate\Database\Eloquent\Model;

class Contact_Orm extends Model {

	protected $table = 'emp';
	//protected $fillable = ['id','title','descript'];
	protected $hidden = ['created_at','updated_at','employee_id'];
	protected $guarded = ['id'];


	public function ReadContact($type,$id){
		if(isset($type) && $type == 'all' && $id == null){
			$data = \DB::table('view_employee_contact')->get();
		}
		elseif(isset($type) && $type == 'sort'){
			$data =  \DB::table('view_employee_contact')->where('employee_id','=',$id)->get();
		}
		return $data; 
	}

	public function StoreContact($input,$id){
		if(isset($input,$id)){
			\DB::table($this->table)
			->update(['employee_id'=>$id,
					'permanent_address'=>$input['permanent_address'],
					'present_address'=>$input['present_address'],
					'home_telephone'=>$input['home_telephone'],
					'mobile'=>$input['mobile'],
					'work_email'=>$input['work_email'],
					'personal_email'=>$input['personal_email']
					]);
			$message = 'Update Successfully.';
		}else{
			$message = $input['personal_email'];
		}
		return $message;
	}

	public function UpdateContact($input,$id){
		if(isset($input,$id)){
			//$q = \DB::table('emp')->where('employee_id','=',$employee_id)->get(['permanent_address','present_address','home_telephone','mobile','work_email','personal_email']);
			$r = \DB::table('emp')->where('employee_id','=',$id)->update([
				'permanent_address' => $input['permanent_address'], 
				'present_address' => $input['present_address'], 
				'home_telephone' => $input['home_telephone'],
				'mobile' => $input['mobile'], 
				'work_email' => $input['work_email'] , 
				'personal_email' => $input['personal_email']
				]);
			// $pdo = \DB::connection()->getpdo();
			// $query = "update emp set 
			// 		permanent_address = $input[permanent_address],
			// 		present_address = $input[present_address],
			// 		home_telephone = $input[home_telephone],
			// 		mobile = $input[mobile],
			// 		work_email = $input[work_email],
			// 		personal_email $input[personal_email]
			// 		where employee_id = $employee_id";
			// $result = $pdo->exec($query);
			if(isset($r)){
				$message = 'ok';	
			}
			else{
				$message = null;
			}
		}
		else{
			$message = $input;
		}
		return $message;
	}

	public function GetID(){
		$data = \DB::select("Select id from contact order by id DESC limit 1");
		return $data;
	}

}

