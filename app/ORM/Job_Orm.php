<?php namespace Larasite\ORM;

use Illuminate\Database\Eloquent\Model;

class Job_Orm extends Model {

	// Database -> hrms.job

	protected $table = 'job';
	protected $fillable = ['id','title','descript','filename','path'];
	protected $hidden = ['create_at','updated_at'];
	protected $guarded = ['id', 'path'];
}
