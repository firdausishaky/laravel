<?php namespace Larasite;

use Illuminate\Database\Eloquent\Model;

class Privilege extends Model {

	//
	protected $table = ['employees','role','permission','form'];
	protected $fields = ['employee_id','employee_type','role_id','form_id','perm_id'];

	public function check_role($role,$form){
		$token_ori = 'test';
		//$user 		= base64_decode(\Cookie::get('id'));
		if($token_ori){
			
			$q = \DB::select("select a.role_name,d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
						from role a, permissions c, form d
						where 
						c.role_id = a.role_id and
						c.form_id = d.form_id and
						c.form_id =  '$form' and 
						a.role_id = '$role' ;");
			if($q){
				$userdata = array();
				foreach ($q as $key) {
					$userdata['role_name'] = $key->role_name;
					$userdata['form_name'] = $key->form_name;
					$userdata['create'] = $key->create;
					$userdata['read'] = $key->read;
					$userdata['update'] = $key->update;
					$userdata['delete'] = $key->delete;
				}
				return $userdata;
			}
			else{
				$msg['warning'] = 'Harap login terlebih dahulu (2)';
				$msg['status'] = 404;
				return $msg; 	
			}
			
		}
		else{
			$msg['warning'] = 'Harap login terlebih dahulu';
			$msg['status'] = 404;
			return $msg; 
		}
	}

	// check key in session
	public function session_key($key){
		$data = \DB::select("select session_key from session where session_key = '$key' ");
		return $data;
	}

	// check ldap_name in db
	public function session_role($employee_id){
		$data = \DB::select("select b.employee_id , b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = '$employee_id'");
		return $data;
	}

	// Check role personal #############################################################
	/*
	* PARAM ROLE AND FORM
	*/
		public function check_role_personal($role,$form){
		//$user 		= base64_decode(\Cookie::get('id'));
		if(isset($role,$form)){
			
			$q = \DB::select("select a.role_name,d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
						from role a, permissions c, form d
						where 
						c.role_id = a.role_id and
						c.form_id = d.form_id and
						c.form_id =  '$form' and 
						a.role_id = '$role' ;");
			$id = \DB::select("select a.employee_id , a.local_it from emp a, ldap b where a.employee_id = b.employee_id and b.role_id = $role");

			if(isset($q,$id)){
				$userdata = array();
				foreach ($q as $key) {
					$userdata['role_name'] = $key->role_name;
					$userdata['form_name'] = $key->form_name;
					$userdata['create'] = $key->create;
					$userdata['read'] = $key->read;
					$userdata['update'] = $key->update;
					$userdata['delete'] = $key->delete;
				}
				foreach ($id as $key) {
					$userdata['employee_id'] = $key->employee_id;
					$userdata['local_it'] = $key->local_it;
				}
				return $userdata;
			}
			else{
				$msg['warning'] = 'Harap login terlebih dahulu (2)';
				$msg['status'] = 404;
				return $msg; 	
			}
		}
		else{
			$msg['warning'] = 'Harap login terlebih dahulu';
			$msg['status'] = 404;
			return $msg; 
		}
	}
	// End Check role personal ########################################################
	public function check_role_personal2($role,$employee_id,$form){
		if($role && $employee_id && $form){
			
			$q = \DB::select("select a.role_name,d.form_name, c.`create`,c.`read`,c.`update`,c.`delete`
						from role a, permissions c, form d
						where 
						c.role_id = a.role_id and
						c.form_id = d.form_id and
						c.form_id =  '$form' and 
						a.role_id = '$role' ;");
			$id = \DB::select("select a.employee_id , a.local_it from emp a, ldap b where a.employee_id = b.employee_id  and a.employee_id = '$employee_id' and b.role_id = $role");

			if(isset($q,$id)){
				$userdata = array();
				foreach ($q as $key) {
					$userdata['role_name'] = $key->role_name;
					$userdata['form_name'] = $key->form_name;
					$userdata['create'] = $key->create;
					$userdata['read'] = $key->read;
					$userdata['update'] = $key->update;
					$userdata['delete'] = $key->delete;
				}
				foreach ($id as $key) {
					$userdata['employee_id'] = $key->employee_id;
					$userdata['local_it'] = $key->local_it;
				}
				return $userdata;
			}
			else{
				$msg['warning'] = 'Harap login terlebih dahulu (2)';
				$msg['status'] = 404;
				return $msg; 	
			}
		}
		else{
			$msg['warning'] = 'Harap login terlebih dahulu';
			$msg['status'] = 404;
			return $msg; 
		}
	}
	// End Check role personal ########################################################
}
