<?php namespace Larasite\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		// sebelum edit, boot tidak ada perintah
		view()->share('dynamic_storage', $this->storagePath());
		
	}

	/*
	* Fungsi storagePath
	*/
	public function storagePath(){
		switch (\Storage::getDefaultDriver()) {
			case 's3':
            		return Storage::getDriver()
               		 	->getAdapter()
                		->getClient()
                		->getObjectUrl(env('S3_BUCKET'), '').'/';

       		case 'local':
            		return URL::to('/').'/';
		}
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
