<?php namespace Larasite\Providers;

use Illuminate\Support\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider {

	/**
	 * Overwrite any vendor / package configuration.
	 *
	 * This service provider is intended to provide a convenient location for you
	 * to overwrite any "vendor" or package configuration that you may want to
	 * modify before the application handles the incoming request / command.
	 *
	 * @return void
	 */
	public function register()
	{
		 config([
            'laravel-cors' => [
                'defaults' => array(
                    'supportsCredentials' => false,
                    'allowedOrigins' => array("*"),
                    'allowedHeaders' => array("*"),
                    'allowedMethods' => array("*"),
                    'exposedHeaders' => array("*"),
                    'maxAge' => 0,
                    'hosts' => array(),
                ),
                'paths' => array(
                    'api/*' => array(
                        'allowedOrigins' => array('*'),
                        'allowedHeaders' => array('*'),
                        'allowedMethods' => array('*'),
                        'maxAge' => 3600,
                    ),
                    '*' => array(
                        'allowedOrigins' => array('*'),
                        'allowedHeaders' => array('Content-Type'),
                        'allowedMethods' => array('POST', 'PUT', 'GET', 'DELETE'),
                        'maxAge' => 3600,
                        'hosts' => array('api.*'),
                    ),
                ),
            ]
        ]);
	}

}
