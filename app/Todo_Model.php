<?php namespace Larasite;

use Illuminate\Database\Eloquent\Model;

class Todo_Model extends Model {

	protected $fillable = ['description','owner_id','is_done'];
 
    public function owner()
    {
        return $this->belongsTo('Larasite\UserTest','owner_id');
    }

}
