<?php namespace Larasite;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'emp';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['employee_id', 'ad_username'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function logout($key){
		$id = \DB::table('session')->where('session_key','=',$key)->get();
		foreach ($id as $val) {
			$data = $val->id;
		}
		if(isset($data)){
			\DB::table('session')->where('id','=',$data)->delete();

			$decode = base64_decode($key);
			$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
			//$key = substr($decode,0,strpos($decode,'-'));
			\DB::table('ldap')->where('employee_id','=',$employee_id)->update(['active'=>0]);
			$stat = 1;
		}
		else{
			$stat = null;	
		}
		return $stat;
	}

	public function auth_ldap($employee_id,$token,$req){
		
		if(isset($employee_id,$token)){

			// $check_permission = \DB::select("select concat(a.first_name,' ',a.last_name) as username, a.employee_id, a.ad_username from emp a, ldap b where a.employee_id = b.employee_id and a.ad_username = '$name' and b.active = 0");
			// if(isset($check_permission)){
				
			// 	$data = array();
			// 	foreach ($check_permission as $key) {
			// 		$data['username'] = $key->username;
			// 		$data['employee_id'] = $key->employee_id;
			// 		$data['ad_username'] = $key->ad_username;
			// 	}
			$client = $req->getClientIp();
				$a = \DB::table('session')->insert(['session_key'=>$token,"info_agent"=>(string)$req->header('user-agent'), "info_cookie"=>$req->header('cookie'),"clientIp"=>$client]);
				$b = \DB::table('ldap')->where(['employee_id'=>$employee_id])->update(['active'=>1]);
				$data = 1;
				return $data;					
			// }
			// else{
			// 	$msg = null;
			// 	return $msg;
			// }
			
		}
		else{
			$msg = 'Authentication Failed, Please Call administrator..';
			$status_header = 403;
			return \Response::json($msg,403);
		}
	}// end auth_ldap

}
