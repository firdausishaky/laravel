<?php namespace Larasite;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class UserTest extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	protected $table = 'users';

	protected $fillable = ['id','email'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function logout($key){
		$id = \DB::table('session')->where('session_key','=',$key)->get();
		foreach ($id as $val) {
			$data = $val->id;
		}
		if(isset($data)){
			\DB::table('session')->where('id','=',$data)->delete();

			$decode = base64_decode($key);
			$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
			//$key = substr($decode,0,strpos($decode,'-'));
			\DB::table('ldap')->where(['employee_id'=>$employee_id])->update(['active'=>0]);
			$stat = 1;
		}
		else{
			$stat = null;	
		}
		return $stat;
	}

	public function auth_ldap($employee_id,$token){
		
		if(isset($employee_id,$token)){

			// $check_permission = \DB::select("select concat(a.first_name,' ',a.last_name) as username, a.employee_id, a.ad_username from emp a, ldap b where a.employee_id = b.employee_id and a.ad_username = '$name' and b.active = 0");
			// if(isset($check_permission)){
				
			// 	$data = array();
			// 	foreach ($check_permission as $key) {
			// 		$data['username'] = $key->username;
			// 		$data['employee_id'] = $key->employee_id;
			// 		$data['ad_username'] = $key->ad_username;
			// 	}
				\DB::table('ldap')->where(['employee_id'=>$employee_id])->update(['active'=>1]);
				\DB::table('session')->insert(['session_key'=>$token]);
				$data = 1;
				return $data;					
			// }
			// else{
			// 	$msg = null;
			// 	return $msg;
			// }
			
		}
		else{
			$msg = 'Authentication Failed, Please Call administrator..';
			$status_header = 403;
			return \Response::json($msg,403);
		}
	}// end auth_ldap

	// public function auth_db($userdata){

	// 	//$rules = array('email'=>'required|email','password'=>'required|alphaNum|min:3');
	// 	$data = array('email'=>$userdata['email'],'password'=>$userdata['password']);
	// 	if(Auth::attempt($data)){

	// 		return \Response::json();
	// 	}
	// 	if(isset($name)){
	// 		if(\Cookie::get('XSRF-TOKEN') == $token){
	// 			$userdata = \DB::table($table)
	// 				->select('select email, password from users where id_ldap in 
	// 					(select id_ldap from ldap where name = '.$name.' )');
	// 			if($userdata != '[]'){
	// 				//$this->auth_db($userdata);
	// 				$msg = "sukses";
	// 				return \Response::json('msg'=>$msg);
	// 			}
	// 			else{
	// 				$msg = "maaf nama anda tidak dikenali, mohon hubungi administrator";
	// 				return \Response::json('msg'=>$msg);
	// 			}
			
	// 		}
	// 		else{
	// 			$msg = "maaf nama anda tidak dikenali, mohon hubungi administrator(check token)";
	// 			return \Response::json('msg'=>$msg);
	// 		}
	// 	}
	// 	else{
	// 		$msg = "Input null";
	// 		return \Response::json('msg'=>$msg);
	// 	}
	// }

}
