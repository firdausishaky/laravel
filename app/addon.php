<?php namespace Larasite\Http\Controllers\Leave\Entitlements;
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Library\FuncAccess;
use Larasite\Model\Leave\Entitlements\leaverequest_Model;
class Leaverequest_Ctrl extends Controller {

  protected $form = "72";

  // title first name file
  // path, path directory file after upload
  public function upload($title,$path){
      $input = \Input::file('file');
      $image = $file->getClientOriginalName();
      $ext = $file->getClientOriginalExtension();
      $size = $file->getSize();

      if($ext != 'jpg' || $ext = != 'JPG' || $ext = 'png' || $ext = 'PNG' ){
          return $data = ["message" => "unsupported extension file upload, only support jpg and png", "status" => 500,"data" => null];
      }elseif ($size >= 1048576 ) {
          return $data = ["message" => "file size too large please upload less tah 1MB ", "status" => 500,"data" => null);
      }else{
        $rename = $title.str_random(6).$ext;
        $file->move(storage_path($path),$rename);
        return $data = ["message" => "success", "rename" => $rename];
      }
  }


  public function input(){
    $json = \Input::get("data");
    $data = json_decode($json,1);

    //check leave type
    if(isset($data['data']['LeaveType'])){
      $type = $data['data']['LeaveType']);
    }elseif (isset($data['LeaveType'])) {
      $type = $data['LeaveType']);
    }else{
      return "errno"
    }

    //check leave_id
    $leave_type = \DB::select("select leave_type,id from leave_type where leave_type='$type' ");
    if($leave_type == null){
        return "errno";
    }else{
        $type_id = $leave_type[0]->id;
    }

    //day off between
    if(isset($data['data']['DayOffBetween'])){
      $dob = $data['data']['DayOffBetween'];
    }elseif (isset($data['DayOffBetween'])) {
      $dob = $data['DayOffBetween'];
    }else{
      return "errno"
    }

    //variable name
    if(isset($data['data']['name'])){
      $name = $data['data']['name'];
    }elseif (isset($data['name'])) {
      $name = $data['name'];
    }else{
      return "errno";
    }

    //variable from
    if(isset($data['data']['from'])){
      $from = $data['data']['From'];
    }elseif (isset($data['from'])){
      $from = $data['From'];
    }elseif ($type  == "Accumulation Day Off"){
      $from = NULL;
    }else{
      return "errno";
    }

    //include comment
    if(isset($data['data']['comment'])){
      $comment = $data['data']['comment'];
    }elseif (isset($data['comment'])){
      $comment = $data['comment'];
    }elseif (!isset($data['comment']) && !isset($data['data']['comment'])){
      $comment = NULL;
    }else{
      return "errno";
    }

    // variable aggred
    if(isset($data['data']['aggred'])){
      $agree = $data['data']['aggred'];
    }elseif (isset($data['aggred'])){
      $agree = $data['aggred'];
    }else{
      return "errno";
    }

    // variable include ado
    if(isset($data['data']['IncludeAdo'])){
      $inado = $data['data']['IncludeAdo'];
    }elseif (isset($data['IncludeAdo'])){
      $inado = $data['IncludeAdo'];
    }else{
      return "errno";
    }

    //varuiable to
    if(isset($data['data']['to'])){
      $to = $data['data']['To'];
    }elseif(isset($data['to'])){
      $to = $data['To'];
    }elseif ($type  == "Accumulation Day Off"){
      $to = NULL;
    }else{
      return "error"
    }

    if($leave_type == "Accumulation Day Off" ){
        $validation = \Validator::make(
        ['name' => $name,
        'LeaveType' => $type,
        'DayOffBetween' => $dob,
        "aggred"  => $aggree],

        ['name' => 'required|integer',
        'LeaveType' => "required|string",
        'DayOffBetween' => 'required|integer',
        "aggred"  => "required"]
        );
    }else{
    $validation = \Validator::make(
        ['name' => $name,
        'LeaveType' => $type,
        'From' => $from,
        'To'    => $to,
        'DayOffBetween' => $dob,
        "aggred"  => $aggree],


        ['name' => 'required|integer',
        'LeaveType' => "required|string",
        'From' => 'required|date',
        'To'    => 'required|date',
        'DayOffBetween' => 'required|integer',
        "aggred"  => "required"]
        );
    }

    if($validation->fails()){
        $check = $validation->errors()-all();
    }else{
      $check = null;
    }

    $input = [
      'type' => $type;
      'type_id' => $type_id;
      'dob' => $dob,
      'name' => $name,
      'from' => $from,
      'to' => $to,
      'i_ado' => $inado,
      'comment' => $comment,
      'agree' => $aggred
    ];

    return $fin = [ "input"  => $input, "check" => [$check] ];
  }

  public function ado(){
    $json = \Input::get("data");
    $data = json_decode($json,1);

    if(isset($data['data']['date1'])){$date1 =  $data['data']['date1'];}elseif(isset($data['date1'])){$date1 =  $data['date1'];}else{$date1 = null;}
    if(isset($data['data']['date2'])){$date2 =  $data['data']['date2'];}elseif(isset($data['date2'])){$date2 =  $data['date2'];}else{$date2 = null;}
    if(isset($data['data']['date3'])){$date3 =  $data['data']['date3'];}elseif(isset($data['date3'])){$date3 =  $data['date3'];}else{$date3 = null;}
    if(isset($data['data']['date4'])){$date4 =  $data['data']['date4'];}elseif(isset($data['date4'])){$date4 =  $data['date4'];}else{$date4 = null;}

    if(isset($data['data']['ado1'])){$ado1 =  $data['data']['ado1'];}elseif(isset($data['ado1'])){$ado1 =  $data['ado1'];}else{$ado1 = null;}
    if(isset($data['data']['ado2'])){$ado2 =  $data['data']['ado2'];}elseif(isset($data['ado2'])){$ado2 =  $data['ado2'];}else{$ado2 = null;}
    if(isset($data['data']['ado3'])){$ado3 =  $data['data']['ado3'];}elseif(isset($data['ado3'])){$ado3 =  $data['ado3'];}else{$ado3 = null;}
    if(isset($data['data']['ado4'])){$ado4 =  $data['data']['ado4'];}elseif(isset($data['ado4'])){$ado4 =  $data['ado4'];}else{$ado4 = null;}

    if(isset($data['data']['nextday'])){$nextday =  $data['data']['nextday'];}elseif(isset($data['nextday'])){$nextday =  $data['nextday'];}else{$nextday = null;}

    $ado = [
        "date1" => $date1;
        "date2" => $date1;
        "date1" => $date1;
        "date1" => $date1;
        "ado1" => $ado1;
        "ado2" => $ado2;
        "ado3" => $ado3;
        "ado4" => $ado4;
        "nextday" => $nextday;
    ];

    $ado_loop = [
          ["date" => $date1, "ado_name" => $ado1],
          ["date" => $date2, "ado_name" => $ado2],
          ["date" => $date3, "ado_name" => $ado3],
          ["date" => $date4, "ado_name" => $ado4]
      ];

    $validation = \Validator::make(
                  ['date1' => $date1,
                  'date2' => $date2,
                  'date3' => $date3,
                  'date4' => $date4,
                  'ado1' => $ado1,
                  'ado2' => $ado2,
                  'ado3' => $ado3,
                  'ado4' => $ado4],

                  ['date1' => 'required|date',
                  'date2' => 'required|date',
                  'date3' => 'required|date',
                  'date4' => 'required|date',
                  'ado1' => 'required|integer',
                  'ado2' => 'required|integer',
                  'ado3' => 'required|integer',
                  'ado4' => 'required|integer']
                  );

  if($validation->fails()){
    $check = $validation->errors()->all();
  }else{
    $check = null;
  }

  return $ado_data = ["ado" => $ado, "err_val" => $check, "loop" =>$ado_loop];

  }


  public function leave_request(){
    /*Access*/$FRA = new FuncAccess; $access = $FRA->Access(\Request::all(),$this->form,'read');
      if($access[1] == 200){
          $request = new leaverequest_Model;
          $file = \Input::file('file');
          $input = $this->input();
          if($input['input']['type'] == "Accumulation Day Off" ){
              if($input['check'][0] == null){
                if($input['input']['i_ado'] == "Yes"){
                  if(isset($file)  && $file != "undefined"){
                   $upload $this->upload("Acc_DoFF_","hrms_upload/request");
                   if($upload[0]['message'] == "success"){

                   }else{
                     return response()->json(['header' => ['message' => 'Error' , "status" => 500], 'data' => $input['check'][0]],500);
                   }
                  }else{

                  }
                }else{

                }
              }else{
                return response()->json(['header' => ['message' => 'Error' , "status" => 500], 'data' => $input['check'][0]],500);
              }
          }elseif ($input['input']['type']  == "Vacation Leave" || $input['input']['type']  == "Enhance Vacation Leave") {
              if($input['input']['i_ado'] == "Yes"){

              }
          }

      }else{ $message = $access[0]; $status = $access[1]; $data=$access[2];
          return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$data],$status);
      }
  }

}
