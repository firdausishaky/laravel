<?php namespace Larasite;

use Illuminate\Database\Eloquent\Model;

class config_ldap extends Model {

	//
	protected $table = 'config_ldap';

	public function read(){
		$temp = array();
		$data = \DB::table($this->table)->get();
		foreach ($data as $key) {
			$temp['ldap_domain'] = $key->ldap_domain;
			$temp['ldap_username'] = $key->ldap_username;
			$temp['ldap_password'] = $key->ldap_password;
			$temp['ldap_account_suffix'] = $key->ldap_account_suffix;
			$temp['ldap_basedn'] = $key->ldap_basedn;
			$temp['ldap_port'] = $key->ldap_port;
			if($key->ldap_protocol){
				$temp['ldap_protocol'] =  'true';		
			}
			$temp['ldap_protocol'] = $key->ldap_protocol;
			if($key->ldap_ssl){
				$temp['ldap_ssl'] =  'true'; 		
			}
			$tempp['ldap_ssl'] = $key->ldap_ssl;
			if($key->ldap_tls){
				$temp['ldap_tls'] =  'true'; 		
			}
			$temp['ldap_tls'] = $key->ldap_tls;
			if($key->ldap_sso){
				$temp['ldap_sso'] =  'true'; 	
			}
			$temp['ldap_sso'] = $key->ldap_sso;
			if($key->ldap_recursive_group){
				$temp['ldap_recursive_group'] = 'true';		
			}
			$temp['ldap_recursive_group'] = $key->ldap_recursive_group;
			if($key->ldap_primary_group){
				$temp['ldap_primary_group'] = 'true';	
			}
			$temp['ldap_primary_group'] = $key->ldap_primary_group;
		}

		return $temp;
	}
}
