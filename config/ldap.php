<?php

 return [

	'admin_username' => 'hr.ldap',

	'admin_password' => 'asdf1234*',

	'account_suffix' => "@inovasisolusi.com",
	
 	'domain_controllers' => 'dc01.inovasisolusi.com', // An array of domains may be provided for load balancing.

 	'base_dn'	=> 'cn=HR, DC=inovasisolusi, DC=com',

 	'real_primary_group' => true, // Returns the primary group (an educated guess).

 	'use_ssl' => false, // If TLS is true this MUST be false.
 	'use_tls' => false, // If SSL is true this MUST be false.

 	'recursive_groups' => true,
];

?>