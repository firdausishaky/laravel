<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Larasite\User;
class UserTableSeeder extends Seeder
{

public function run()
{
    DB::table('testauth')->delete();
    User::create(array(
        'email'    => 'chris@scotch.io',
        'password' => Hash::make('awesome'),
    ));
}

}